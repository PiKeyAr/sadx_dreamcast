#include "stdafx.h"
#include "Boss_E101R.h"

static bool ModelsLoaded_B_E101R;

static int E101REffectMode = 1; // 0 is orange, 1 is blue

// Trampoline to queue damage explosion
static Trampoline* spriteAutoExec_t = nullptr;
static void __cdecl spriteAutoExec_r(void *a1)
{
	const auto original = TARGET_DYNAMIC(spriteAutoExec);
	late_SetFunc((void(__cdecl*)(void*))original, (void*)a1, 22048.0f, LATE_LIG);
}

// Queue jet fumes
void E101R_Particle_Show(task *a1)
{
	late_SetFunc((void(__cdecl*)(void*))nozzleListDisp, (void*)a1, 22048.0f, LATE_LIG);
}

// Queue small balls
void E101R_SmallBalls_Show(task*a1)
{
	late_SetFunc((void(__cdecl*)(void*))control_disp, (void*)a1, 22048.0f, LATE_LIG);
}

// Color the aftereffect
void E101R_Effect_Render(float a, float r, float g, float b)
{
	if (!E101REffectMode)
		SetMaterial(1.0f, a * 1.5f, a * 0.75f, a * 0.5f);
	else
		SetMaterial(1.0f, a * 0.75f, a * 0.5f, a * 1.5f);
}

// Set aftereffect mode to orange
void E101R_Effect_Orange(int a1, int a2)
{
	E101REffectMode = 0;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	E101R_AfterImageParts(a1, a2);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

// Set aftereffect mode to blue
void E101R_Effect_Blue(int a1, int a2)
{
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	E101R_AfterImageParts(a1, a2);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	E101REffectMode = 1;
}

// Set constant attribute for the aftereffect
void E101R_AfterImageConstantAttr(Uint32 and_attr, Uint32 or_attr)
{
	OnConstantAttr(0, NJD_FLAG_USE_TEXTURE | NJD_FLAG_USE_ALPHA | NJD_FLAG_IGNORE_LIGHT | NJD_FLAG_USE_TEXTURE);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

// Set alpha for the aftereffect
void E101R_AfterImageMaterial(float a, float r, float g, float b)
{
	SetMaterial(a, 1.0f, 1.0f, 1.0f);
}

// Set constant attribute for the aftereffect (arm version)
void E101R_Effect_ConstantAttr(Uint32 and_attr, Uint32 or_attr)
{
	njSetConstantAttr(0x9999999u, 0x24100000u);
	OnConstantAttr(0, NJD_FLAG_IGNORE_LIGHT | NJD_FLAG_USE_TEXTURE);
}

// Queue the afterimage
void E101R_AfterImageQueue(NJS_ACTION *anim, float a2, int a3)
{
	late_z_ofs___ = 2000.0f;
	late_ActionEx(anim, a2, LATE_MAT);
	late_z_ofs___ = 0;
}

// Queue the afterimage (arm version)
void E101R_ArmsHook(NJS_OBJECT *a1, LATE a2)
{
	late_z_ofs___ = 2000.0f;
	late_DrawObjectClip(a1, a2, GlobalModelNodeScale);
	late_z_ofs___ = 0;
}

// Set blending mode for various effects drawn directly
void E101R_FVFShit(FVFStruct_H_B *a1, signed int count, int a3)
{
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	Direct3D_DrawFVF_H(a1, count);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

// Disable Z writing for various effects drawn directly
void E101R_FVFZWrite(FVFStruct_H_B* a1, signed int count, int a3)
{
	njSetZUpdateMode(0);
	SetOceanAlphaModeAndFVF(a3);
	Direct3D_DrawFVF_H(a1, count);
	njSetZUpdateMode(1u);
}

// Depth hook: explosion
void E101R_DrawExplosion(NJS_OBJECT *a1, LATE a2)
{
	late_z_ofs___ = 20000.0f;
	late_DrawObject(a1, LATE_LIG);
	late_z_ofs___ = 0.0f;
}

// Ocean display function
void E101ROceanHook(void(__cdecl* function)(void*), void* data, float depth, LATE queueflags)
{
	if (!loop_count)
	{
		___njFogDisable();
		njSetTexture(&texlist_ec_sea);
		njPushMatrix(0);
		njTranslate(0, camera_twp->pos.x, 0, camera_twp->pos.z);
		late_z_ofs___ = -40000.0f;
		late_DrawObjectClip(&_object_ecsc_s_hare_umi_s_hare_umi, LATE_WZ, 1.0f);
		njPopMatrix(1u);
		late_z_ofs___ = 0.0f;
	}
}

// Fix the motion callback in boss display function
void E101R_RenderWithCallback(NJS_ACTION* action, float frame)
{
	nj_motion_callback_ = 0x56AE40;
	njActionOld(action, frame);
	nj_motion_callback_ = 0;
}

// Rocket draw function with material flags
void E101R_Rocket_Render(NJS_OBJECT *a1, NJS_MOTION *a2, float a3)
{
	SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
	a1->child->basicdxmodel->mats[0].attrflags = 0x94002400;
	a1->child->sibling->basicdxmodel->mats[0].attrflags = 0x94002400;
	a1->child->sibling->sibling->basicdxmodel->mats[0].attrflags = 0x94002400;
	a1->child->sibling->sibling->sibling->basicdxmodel->mats[0].attrflags = 0x94002400;
	dsDrawMotion(a1, a2, a3);
	ResetMaterial();
}

// Verious fixes init function
void E101FVF_Init()
{
	WriteCall((void*)0x0056F839, E101R_Particle_Show);
	WriteCall((void*)0x004CC13F, E101R_SmallBalls_Show);
	WriteCall((void*)0x004CC1C7, E101R_SmallBalls_Show);
	WriteCall((void*)0x004CC82E, E101R_FVFZWrite);
	WriteCall((void*)0x0056FD59, E101R_FVFShit);
	WriteCall((void*)0x00571581, E101R_DrawExplosion);
	WriteCall((void*)0x005715AE, E101R_DrawExplosion);
}

// Loads the ocean PVM
void LoadE101ROceanPVM(const char* filename, NJS_TEXLIST* texlist)
{
	texLoadTexturePvmFile(filename, texlist);
	texLoadTexturePvmFile("EC_SEA", &texlist_ec_sea);
}

void B_E101_R_Init()
{
	ReplaceGeneric("E101R_GC.NB", "E101R_DC.NB");
	ReplaceSET("SETE101RE");
	ReplacePVM("E101R");
	ReplacePVM("E101R_BG");
	ReplacePVM("E101R_TIKEI"); // Shared with Zero
	for (int i = 0; i < 3; i++)
	{
		pFogTable_E101r[0][i].f32EndZ = -10000.0f;
		pFogTable_E101r[0][i].f32StartZ = -10000.0f;
		pClipMap_E101r[0][i].f32Far = -9500.0f;
	}
}

void B_E101_R_Load()
{
	LevelLoader(LevelAndActIDs_E101R, "SYSTEM\\data\\boss_e101_r\\landtable2400.c.sa1lvl", &texlist_e101r_tikei);
	if (!ModelsLoaded_B_E101R)
	{
		WriteData<1>((char*)0x00568D20, 0xC3u); // Disable SetClip_E101R
		spriteAutoExec_t = new Trampoline(0x004CC880, 0x004CC886, spriteAutoExec_r); // E101R hurt explosion
		// E-101R fixes
		E101FVF_Init();
		WriteCall((void*)0x00570DB1, E101R_Rocket_Render);
		WriteCall((void*)0x0056B018, E101R_RenderWithCallback);
		WriteCall((void*)0x005709CA, E101R_ArmsHook);
		WriteCall((void*)0x0057069D, E101R_AfterImageMaterial); // E101R afterimage
		WriteCall((void*)0x00570784, E101R_AfterImageConstantAttr); // E101R afterimage
		WriteCall((void*)0x0057072A, E101R_AfterImageQueue);
		WriteCall((void*)0x0056B07D, E101R_Effect_Orange); // Set arm effect to orange and render
		WriteCall((void*)0x0056B096, E101R_Effect_Blue); // Set arm effect to blue and render
		WriteCall((void*)0x0057098A, E101R_Effect_Render); // After effect on E101R's arms
		WriteCall((void*)0x00570952, E101R_Effect_ConstantAttr);
		// Ocean model
		if (!CheckSADXWater(LevelIDs_E101R))
		{
			WriteCall((void*)0x00569078, LoadE101ROceanPVM);
			WriteCall((void*)0x0056902A, E101ROceanHook);
			WriteCall((void*)0x005690C3, E101ROceanHook); // Nop another ocean functon
			_object_ecsc_s_hare_umi_s_hare_umi.basicdxmodel->mats[0].attr_texId = 4;
			_object_ecsc_s_hare_umi_s_hare_umi.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT;
			_object_ecsc_s_hare_umi_s_hare_umi.basicdxmodel->mats[0].diffuse.color = 0x7FFFFFFF;
			AddTextureAnimation(LevelIDs_E101R, 0, &_object_ecsc_s_hare_umi_s_hare_umi.basicdxmodel->mats[0], false, 4, 4, 13);
		}
		ModelsLoaded_B_E101R = true;
	}
}