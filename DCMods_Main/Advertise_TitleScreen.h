#pragma once

#include <SADXModLoader.h>

#define SQUARE(x) (x)*(x) 
#define PYTHAGORAS(x,y) sqrt(SQUARE(x) + SQUARE(y))

FunctionPointer(void, SetABCTextThingColor, (int a1, int a2, int a3, int a4), 0x00420A70); // Sets the colors for Ascii16x16FontInfo, not in X360?

void DrawTitleBack_r(float x, float y, float z, float width, float height); // Draws the green gradient

enum struct TitleScreenTransitionMode
{
	None = -1,
	Normal = 0,
	FadeOut = 1,
	FadeEnd = 2,
	FadeIn = 3
};