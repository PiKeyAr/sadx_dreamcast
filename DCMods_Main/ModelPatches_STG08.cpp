#include "stdafx.h"

void PatchModels_STG08()
{		
	// Crystal transparency... Maybe someday
	/*	
	// dxpc\stg08_icecap\common\models\obj_suisyou_b.nja.sa1mdl
	((NJS_MATERIAL*)0x00E758EC)->attrflags = 0x9471A400;
	((NJS_MATERIAL*)0x00E75900)->attrflags = 0x9471A400;
	((NJS_MATERIAL*)0x00E75914)->attrflags = 0x9438A400;

	// dxpc\stg08_icecap\common\models\obj_suisyou_bm.nja.sa1mdl
	((NJS_MATERIAL*)0x00E765CC)->attrflags = 0x9471A400;
	((NJS_MATERIAL*)0x00E765E0)->attrflags = 0x9471A400;
	((NJS_MATERIAL*)0x00E765F4)->attrflags = 0x9438A400;

	// dxpc\stg08_icecap\common\models\obj_suisyou_r.nja.sa1mdl
	((NJS_MATERIAL*)0x00E76EAC)->attrflags = 0x9471A400;
	((NJS_MATERIAL*)0x00E76EC0)->attrflags = 0x9471A400;
	*/

	// dxpc\stg08_icecap\bg\models\sora55_aida01a.nja.sa1mdl
	//((NJS_MATERIAL*)0x00E92294)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg08_icecap\bg\models\sora98_oya.nja.sa1mdl
	//((NJS_MATERIAL*)0x00E9395C)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x00E93970)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg08_icecap\bg\models\sora98_yuki_03kumo.nja.sa1mdl
	//((NJS_MATERIAL*)0x00E93EB0)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg08_icecap\common\models\cp_ic_futa_l.nja.sa1mdl
	// Full replacement

	// dxpc\stg08_icecap\common\models\cp_ic_objice_l.nja.sa1mdl
	// Full replacement

	// dxpc\stg08_icecap\common\models\green_moku_b.nja.sa1mdl
	// Rearranged model in DX, not replacing
	
	// dxpc\stg08_icecap\common\models\jpdai_all1_jpdai01.nja.sa1mdl
	*(NJS_TEX*)0x00E52AE0 = { 251, -1530 };
	*(NJS_TEX*)0x00E52AE8 = { 251, -1275 };
	*(NJS_TEX*)0x00E52AF0 = { 251, -1020 };
	*(NJS_TEX*)0x00E52AF8 = { 251, -764 };
	*(NJS_TEX*)0x00E52B00 = { 251, -509 };
	*(NJS_TEX*)0x00E52B08 = { 251, -254 };
	*(NJS_TEX*)0x00E52B10 = { 251, 0 };
	*(NJS_TEX*)0x00E52B18 = { 251, 255 };
	*(NJS_TEX*)0x00E52B24 = { 251, 255 };
	*(NJS_TEX*)0x00E52B2C = { 251, 0 };
	*(NJS_TEX*)0x00E52B34 = { 251, -254 };
	*(NJS_TEX*)0x00E52B3C = { 251, -510 };
	*(NJS_TEX*)0x00E52B44 = { 251, -765 };
	*(NJS_TEX*)0x00E52B4C = { 251, -1020 };
	*(NJS_TEX*)0x00E52B54 = { 251, -1275 };
	*(NJS_TEX*)0x00E52B5C = { 251, -1530 };
	*(NJS_TEX*)0x00E52B60 = { 251, 3 };
	*(NJS_TEX*)0x00E52B64 = { 251, 255 };
	*(NJS_TEX*)0x00E52B78 = { 251, 3 };
	*(NJS_TEX*)0x00E52B84 = { 251, 3 };
	*(NJS_TEX*)0x00E52B8C = { 510, 255 };
	*(NJS_TEX*)0x00E52B94 = { 510, 2 };
	*(NJS_TEX*)0x00E52B9C = { 510, -250 };
	*(NJS_TEX*)0x00E52BA4 = { 510, -504 };
	*(NJS_TEX*)0x00E52BAC = { 510, -758 };
	*(NJS_TEX*)0x00E52BB4 = { 510, -1015 };
	*(NJS_TEX*)0x00E52BBC = { 510, -1275 };
	*(NJS_TEX*)0x00E52BC0 = { 510, 3 };
	*(NJS_TEX*)0x00E52BC8 = { 510, 254 };
	*(NJS_TEX*)0x00E52BD4 = { 510, 255 };
	*(NJS_TEX*)0x00E52BDC = { 510, 3 };

	// dxpc\stg08_icecap\common\models\ki_all1_tree01.nja.sa1mdl
		//Model too different

	// dxpc\stg08_icecap\common\models\obj_ele.nja.sa1mdl
		//Model too different
		
	// dxpc\stg08_icecap\common\models\obj_mizuiwa_b.nja.sa1mdl
	*(NJS_TEX*)0x00E6DD50 = { 768, -637 };
	*(NJS_TEX*)0x00E6DD54 = { 648, -637 };
	*(NJS_TEX*)0x00E6DD58 = { 720, -765 };
	*(NJS_TEX*)0x00E6DD5C = { 386, 251 };
	*(NJS_TEX*)0x00E6DD60 = { 737, 251 };
	*(NJS_TEX*)0x00E6DD64 = { 386, -99 };
	*(NJS_TEX*)0x00E6DD68 = { 737, -99 };
	*(NJS_TEX*)0x00E6DD6C = { 386, -354 };
	*(NJS_TEX*)0x00E6DD70 = { 737, -354 };
	*(NJS_TEX*)0x00E6DD74 = { 386, -641 };
	*(NJS_TEX*)0x00E6DD78 = { 737, -641 };
	*(NJS_TEX*)0x00E6DD7C = { 3, 251 };
	*(NJS_TEX*)0x00E6DD80 = { 386, 251 };
	*(NJS_TEX*)0x00E6DD84 = { 3, -99 };
	*(NJS_TEX*)0x00E6DD88 = { 386, -99 };
	*(NJS_TEX*)0x00E6DD8C = { 3, -354 };
	*(NJS_TEX*)0x00E6DD90 = { 386, -354 };
	*(NJS_TEX*)0x00E6DD94 = { 3, -641 };
	*(NJS_TEX*)0x00E6DD98 = { 386, -641 };
	*(NJS_TEX*)0x00E6DD9C = { 513, -761 };
	*(NJS_TEX*)0x00E6DDA0 = { 737, 251 };
	*(NJS_TEX*)0x00E6DDA4 = { 1016, 251 };
	*(NJS_TEX*)0x00E6DDA8 = { 737, -99 };
	*(NJS_TEX*)0x00E6DDAC = { 1016, -99 };
	*(NJS_TEX*)0x00E6DDB0 = { 737, -354 };
	*(NJS_TEX*)0x00E6DDB4 = { 1016, -354 };
	*(NJS_TEX*)0x00E6DDB8 = { 737, -641 };
	*(NJS_TEX*)0x00E6DDBC = { 1016, -641 };
	*(NJS_TEX*)0x00E6DDC0 = { 513, -761 };
	*(NJS_TEX*)0x00E6DDC4 = { 720, -765 };
	*(NJS_TEX*)0x00E6DDC8 = { 768, -637 };
	*(NJS_TEX*)0x00E6DDCC = { 648, -637 };
	*(NJS_TEX*)0x00E6DDD0 = { 386, -641 };
	*(NJS_TEX*)0x00E6DDD4 = { 737, -641 };
	*(NJS_TEX*)0x00E6DDD8 = { 386, -354 };
	*(NJS_TEX*)0x00E6DDDC = { 737, -354 };
	*(NJS_TEX*)0x00E6DDE0 = { 386, -99 };
	*(NJS_TEX*)0x00E6DDE4 = { 737, -99 };
	*(NJS_TEX*)0x00E6DDE8 = { 386, 251 };
	*(NJS_TEX*)0x00E6DDEC = { 737, 251 };
	*(NJS_TEX*)0x00E6DDF0 = { 513, -761 };
	*(NJS_TEX*)0x00E6DDF4 = { 1016, -641 };
	*(NJS_TEX*)0x00E6DDF8 = { 737, -641 };
	*(NJS_TEX*)0x00E6DDFC = { 1016, -354 };
	*(NJS_TEX*)0x00E6DE00 = { 737, -354 };
	*(NJS_TEX*)0x00E6DE04 = { 1016, -99 };
	*(NJS_TEX*)0x00E6DE08 = { 737, -99 };
	*(NJS_TEX*)0x00E6DE0C = { 1016, 251 };
	*(NJS_TEX*)0x00E6DE10 = { 737, 251 };
	*(NJS_TEX*)0x00E6DE14 = { 386, 251 };
	*(NJS_TEX*)0x00E6DE18 = { 3, 251 };
	*(NJS_TEX*)0x00E6DE1C = { 386, -99 };
	*(NJS_TEX*)0x00E6DE20 = { 3, -99 };
	*(NJS_TEX*)0x00E6DE24 = { 386, -354 };
	*(NJS_TEX*)0x00E6DE28 = { 3, -354 };
	*(NJS_TEX*)0x00E6DE2C = { 386, -641 };
	*(NJS_TEX*)0x00E6DE30 = { 3, -641 };
	*(NJS_TEX*)0x00E6DE34 = { 513, -761 };

	// dxpc\stg08_icecap\common\models\obj_mizuiwa_c.nja.sa1mdl
	*(NJS_TEX*)0x00E6E1C8 = { 250, 255 };
	*(NJS_TEX*)0x00E6E1CC = { 292, 255 };
	*(NJS_TEX*)0x00E6E1D0 = { 243, 202 };
	*(NJS_TEX*)0x00E6E1D4 = { 301, 202 };
	*(NJS_TEX*)0x00E6E1D8 = { 255, 152 };
	*(NJS_TEX*)0x00E6E1DC = { 294, 152 };
	*(NJS_TEX*)0x00E6E1E0 = { 250, 89 };
	*(NJS_TEX*)0x00E6E1E4 = { 282, 89 };
	*(NJS_TEX*)0x00E6E1E8 = { 250, 45 };
	*(NJS_TEX*)0x00E6E1EC = { 291, 45 };
	*(NJS_TEX*)0x00E6E1F0 = { 250, 1 };
	*(NJS_TEX*)0x00E6E1F4 = { 292, 1 };
	*(NJS_TEX*)0x00E6E1F8 = { 250, 1 };
	*(NJS_TEX*)0x00E6E1FC = { 207, 1 };
	*(NJS_TEX*)0x00E6E200 = { 250, 45 };
	*(NJS_TEX*)0x00E6E204 = { 208, 45 };
	*(NJS_TEX*)0x00E6E208 = { 250, 89 };
	*(NJS_TEX*)0x00E6E20C = { 217, 89 };
	*(NJS_TEX*)0x00E6E210 = { 255, 152 };
	*(NJS_TEX*)0x00E6E214 = { 212, 152 };
	*(NJS_TEX*)0x00E6E218 = { 243, 202 };
	*(NJS_TEX*)0x00E6E21C = { 195, 202 };
	*(NJS_TEX*)0x00E6E220 = { 250, 255 };
	*(NJS_TEX*)0x00E6E224 = { 207, 255 };
	*(NJS_TEX*)0x00E6E228 = { 81, 1 };
	*(NJS_TEX*)0x00E6E22C = { 39, 1 };
	*(NJS_TEX*)0x00E6E230 = { 82, 45 };
	*(NJS_TEX*)0x00E6E234 = { 38, 45 };
	*(NJS_TEX*)0x00E6E238 = { 70, 89 };
	*(NJS_TEX*)0x00E6E23C = { 29, 89 };
	*(NJS_TEX*)0x00E6E240 = { 79, 152 };
	*(NJS_TEX*)0x00E6E244 = { 41, 152 };
	*(NJS_TEX*)0x00E6E248 = { 92, 202 };
	*(NJS_TEX*)0x00E6E24C = { 48, 202 };
	*(NJS_TEX*)0x00E6E250 = { 81, 255 };
	*(NJS_TEX*)0x00E6E254 = { 39, 255 };
	*(NJS_TEX*)0x00E6E258 = { 207, 1 };
	*(NJS_TEX*)0x00E6E25C = { 165, 1 };
	*(NJS_TEX*)0x00E6E260 = { 208, 45 };
	*(NJS_TEX*)0x00E6E264 = { 164, 45 };
	*(NJS_TEX*)0x00E6E268 = { 217, 89 };
	*(NJS_TEX*)0x00E6E26C = { 177, 89 };
	*(NJS_TEX*)0x00E6E270 = { 212, 152 };
	*(NJS_TEX*)0x00E6E274 = { 161, 152 };
	*(NJS_TEX*)0x00E6E278 = { 195, 202 };
	*(NJS_TEX*)0x00E6E27C = { 160, 202 };
	*(NJS_TEX*)0x00E6E280 = { 207, 255 };
	*(NJS_TEX*)0x00E6E284 = { 165, 255 };
	*(NJS_TEX*)0x00E6E288 = { 81, 255 };
	*(NJS_TEX*)0x00E6E28C = { 123, 255 };
	*(NJS_TEX*)0x00E6E290 = { 92, 202 };
	*(NJS_TEX*)0x00E6E294 = { 127, 202 };
	*(NJS_TEX*)0x00E6E298 = { 79, 152 };
	*(NJS_TEX*)0x00E6E29C = { 117, 152 };
	*(NJS_TEX*)0x00E6E2A0 = { 70, 89 };
	*(NJS_TEX*)0x00E6E2A4 = { 123, 89 };
	*(NJS_TEX*)0x00E6E2A8 = { 82, 45 };
	*(NJS_TEX*)0x00E6E2AC = { 123, 45 };
	*(NJS_TEX*)0x00E6E2B0 = { 81, 1 };
	*(NJS_TEX*)0x00E6E2B4 = { 123, 1 };
	*(NJS_TEX*)0x00E6E2B8 = { 123, 255 };
	*(NJS_TEX*)0x00E6E2BC = { 165, 255 };
	*(NJS_TEX*)0x00E6E2C0 = { 127, 202 };
	*(NJS_TEX*)0x00E6E2C4 = { 160, 202 };
	*(NJS_TEX*)0x00E6E2C8 = { 117, 152 };
	*(NJS_TEX*)0x00E6E2CC = { 161, 152 };
	*(NJS_TEX*)0x00E6E2D0 = { 123, 89 };
	*(NJS_TEX*)0x00E6E2D4 = { 177, 89 };
	*(NJS_TEX*)0x00E6E2D8 = { 123, 45 };
	*(NJS_TEX*)0x00E6E2DC = { 164, 45 };
	*(NJS_TEX*)0x00E6E2E0 = { 123, 1 };
	*(NJS_TEX*)0x00E6E2E4 = { 165, 1 };

	// dxpc\stg08_icecap\common\models\obj_turara.nja.sa1mdl
		//Model too different

	// dxpc\stg08_icecap\common\models\saku_all1_saku01.nja.sa1mdl
		//Model too different

	// dxpc\stg08_icecap\common\models\saku_all1_saku01_b.nja.sa1mdl
		//Model too different

	// dxpc\stg08_icecap\common\models\saku_all1_saku02.nja.sa1mdl
		//Model too different

	// dxpc\stg08_icecap\common\models\saku_all1_saku02_b.nja.sa1mdl
		//Model too different
}