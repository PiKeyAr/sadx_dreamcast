#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_MODEL_SADX, model_itembox_boxbody_boxbody, 0x008BF260); // Item box base
DataPointer(NJS_MODEL_SADX, model_itembox_boxbody_item_panel, 0x008BE270); // Item box icon
DataPointer(NJS_MODEL_SADX, model_itembox_boxbody_cyl2, 0x008BEAA8); // Item box capsule part
DataPointer(NJS_MODEL_SADX, model_itembox_boxbody_cyl3, 0x8BE620); // Item box top

DataArray(CCL_INFO, itembox_colli_info0, 0x009BF0C0, 2);
DataArray(CCL_INFO, itembox_colli_info1, 0x009BF120, 2);