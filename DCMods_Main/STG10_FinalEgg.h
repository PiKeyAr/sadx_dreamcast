#pragma once

#include <SADXModLoader.h>

void ScaleEnvironmentMap(float value1 = -1, float value2 = -1, float value3 = -1, float value4 = -1);
void RestoreEnvironmentMap();
TEX_PVMTABLE* ExpandPVMList(TEX_PVMTABLE* sourcepvmlist, const TEX_PVMTABLE& newpvmentry);

DataPointer(NJS_ACTION, action_enemyout_hokoz, 0x01A1F944);
DataPointer(NJS_MODEL_SADX, model_cam_bo_c_redlgt, 0x019FBDAC);
DataPointer(NJS_MODEL_SADX, model_setasiba_lv1_lv1, 0x019D7530);
DataPointer(NJS_MODEL_SADX, model_contena_box_box, 0x019D77F0);
DataPointer(NJS_MODEL_SADX, model_ugokuhari_dodai_dodai, 0x019D6A20);
DataPointer(NJS_MODEL_SADX, model_ugokuhari_dodai_waku, 0x019D607C);
DataPointer(NJS_MODEL_SADX, model_ugokuhari_dodai_hari, 0x019D493C);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_big_big, 0x019E8D88);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_big_kaiten_ami1a, 0x019E2D30);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_big_kaiten_waku1a, 0x019E4588);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_midium_midium, 0x019F0408);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_midium_kaiten_ami2a, 0x019EA370);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_midium_kaiten_waku2a, 0x019EBBE0);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_small_small, 0x019F7480);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_small_kaiten_ami3a, 0x019F1400);
DataPointer(NJS_MODEL_SADX, model_kaitenasiba_small_kaiten_waku3a, 0x019F2C58);
DataPointer(NJS_MODEL_SADX, model_hammer_bmerge2_hammer, 0x019FA5DC);
DataPointer(NJS_OBJECT, object_boyon_henkeigo_saku_gomug, 0x01A46568);
DataPointer(NJS_OBJECT, object_ue_aamu_ue_cube50a, 0x019DB01C);
DataPointer(NJS_OBJECT, object_ue_aamu_ue_cyl51a, 0x019DC0EC);
DataPointer(NJS_OBJECT, object_ue_aamu_ue_cube51a, 0x019DB43C);
DataPointer(NJS_OBJECT, object_ue_aamu_ue_cube52a, 0x019DB85C);
DataPointer(NJS_OBJECT, object_hammer_bmerge2_bmergex, 0x019FBC64);
DataPointer(NJS_OBJECT, object_aamu05_cyl40_cyl51, 0x019DF364);
DataPointer(NJS_OBJECT, object_aamu05_cyl40_cube50, 0x019DE2AC);
DataPointer(NJS_OBJECT, object_aamu05_cyl40_cube51, 0x019DE6CC);
DataPointer(NJS_OBJECT, object_aamu05_cyl40_cube52, 0x019DEAEC);

void OStandLight_Main_F(task* obj);
void OStandLight_F(task* a1);
void OStandLight_Display_F(task* a1);
void OStandLight_Init();