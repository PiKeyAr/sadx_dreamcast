#include "stdafx.h"
#include "ADV02_MysticRuins.h"

static bool ModelsLoaded_ADV02;

NJS_VECTOR TempleVector = { -515.99f, 90.0f, -1137.45f }; // Center of the temple, used for dynamic fog
static bool AmyMissionCollision = false; // Add extra collision (used in Amy's mission) if true

// Model pointers
NJS_OBJECT* MROcean = nullptr; // Act 1 ocean model that doesn't exist in DX
NJS_OBJECT* OFinalEggModel_A = nullptr; // OFinalEgg opaque parts
NJS_OBJECT* OFinalEggModel_B = nullptr; // OFinalEgg transparent parts
NJS_OBJECT* OFinalEggModel_C = nullptr; // OFinalEgg colored dots
NJS_OBJECT* OFinalEggModel_D = nullptr; // OFinalEgg moving lights, background light
NJS_OBJECT* MRJungle_Propeller = nullptr; // Propeller in the jungle 1
NJS_OBJECT* MRJungle_Propeller2 = nullptr; // Propeller in the jungle 1
NJS_OBJECT* MRJungle_LanternTop = nullptr; // Top of the lantern in Big's house

// Renders the Mystic Ruins ocean, SA1 version
void __cdecl MRWater_Display(void(__cdecl* function)(void*), void* data, float depth, LATE queueflags)
{
	if (!IsLevelLoaded(LevelIDs_MysticRuins, 0) || ssStageNumber != LevelIDs_MysticRuins || ssActNumber != 0)
		return;
	if (!loop_count)
	{
		___njFogDisable();
		njSetTexture(ADV02_TEXLISTS[38]);
		njPushMatrix(0);
		njTranslate(0, 0, 0, 0);
		late_z_ofs___ = -47952.0f;
		if (MROcean)
			ds_DrawObjectClip(MROcean, 1.0f);
		late_z_ofs___ = 0.0f;
		njPopMatrix(1u);
		___njFogEnable();
	}
}

// Renders Eggman's base object (OFinalEgg) with the pathway (OFinalWay)
void FinalEggDisplayer_r(task *tp)
{
	taskwk* twp = tp->twp;
	___njFogDisable();
	njSetTexture(ADV02_TEXLISTS[1]);
	njPushMatrix(0);
	njTranslateV(0, &twp->pos);
	late_z_ofs___ = -30000.0f;
	Angle rot_y = twp->ang.y;
	if (rot_y)
		njRotateY(0, (unsigned __int16)rot_y);
	// Render the animation without the lights
	late_DrawMotionClip(OFinalEggModel_A, ADV02_ACTIONS[0]->motion, twp->scl.x, LATE_WZ, 1.0f);
	// Render the transparent part of the animation without the lights
	late_DrawMotionClip(OFinalEggModel_B, ADV02_ACTIONS[0]->motion, twp->scl.x, LATE_WZ, 1.0f);
	// Render the colored dots
	late_z_ofs___ = -20000.0f;
	late_DrawMotionClip(OFinalEggModel_C, ADV02_ACTIONS[0]->motion, twp->scl.x, LATE_WZ, 1.0f);
	// Render the OFinalWay car
	late_z_ofs___ = -30000.0f;
	late_ActionEx(ADV02_ACTIONS[30], twp->scl.y, LATE_WZ);
	// Render the FinalWay without the car
	njPushMatrix(0);
	njTranslateV(0, (NJS_VECTOR*)ADV02_ACTIONS[30]->object->pos);
	late_z_ofs___ = -20000.0f;
	late_DrawModelEx(ADV02_ACTIONS[30]->object->basicdxmodel, LATE_WZ);
	late_DrawModelEx(ADV02_ACTIONS[30]->object->child->basicdxmodel, LATE_WZ);
	njPopMatrix(1u);
	// Render the lights
	late_z_ofs___ = -30000.0f;
	late_DrawMotionClipMesh(OFinalEggModel_D, ADV02_ACTIONS[0]->motion, twp->scl.x, LATE_LIG, 1.0f);
	// Render the background light
	late_DrawModel(OFinalEggModel_D->child->sibling->sibling->basicdxmodel, LATE_LIG);
	njPopMatrix(1u);
	___njFogEnable();
	late_z_ofs___ = 0.0f;
}

// Trampoline to set offset material for OBlockEntry glow effect
static Trampoline* DispATask_23_t = nullptr;
static void __cdecl DispATask_23_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(DispATask_23);
	SaveControl3D();
	OnControl3D(NJD_CONTROL_3D_OFFSET_MATERIAL);
	original(tp);
	LoadControl3D();
}

// Trampoline to set offset material for Master Emerald glow
static Trampoline* DispATask_26_t = nullptr;
static void __cdecl DispATask_26_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(DispATask_26);
	SaveControl3D();
	OnControl3D(NJD_CONTROL_3D_OFFSET_MATERIAL);
	original(tp);
	LoadControl3D();
}

// Hack to draw the Master Emerald with depth
void MasterEmeraldFix(NJS_OBJECT* obj, float scale)
{
	if (current_event != 128)
		late_z_ofs___ = 2000.0f;
	late_DrawObjectClipMesh(obj, LATE_WZ, 1.0f);
	late_z_ofs___ = 0.0f;
}

// Hack to draw the Master Emerald glow with depth
void DrawMasterEmeraldGlow(NJS_MODEL_SADX* model, LATE blend, float scale)
{
	if (current_event != 128)
		late_z_ofs___ = 4000.0f;
	late_DrawModelClip(model, blend, scale);
	late_z_ofs___ = 0.0f;
}

// Depth hack: Grass (animation)
void RustlingGrassDepthFix1(NJS_ACTION* a1, float a2, LATE a3, float a4)
{
	if (current_event != -1)
		late_z_ofs___ = -27000.0f;
	late_ActionClipEx(a1, a2, a3, a4);
	late_z_ofs___ = 0.0f;
}

// Depth hack: Grass (object)
void RustlingGrassDepthFix2(NJS_OBJECT* a1, LATE a2, float a3)
{
	if (current_event != -1)
		late_z_ofs___ = -27000.0f;
	late_DrawObjectClipEx(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Depth hack: Tree
void OTreeFix(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_z_ofs___ = -25000.0f;
	late_DrawObjectClipEx(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Depth/hierarchy hack: OHandKey
void OHandKeyFix(NJS_ACTION* action, float frame, LATE flags, float scale)
{
	if (action->object == ADV02_OBJECTS[76] || action->object == ADV02_OBJECTS[88])
	{
		late_z_ofs___ = -28000.0f;
		late_ActionClipMesh(action, frame, LATE_WZ, scale);
		late_z_ofs___ = -27000.0f;
		late_DrawModelClipMesh(action->object->basicdxmodel, LATE_WZ, scale);
		late_z_ofs___ = 0.0f;
	}
	else
		late_ActionClipEx(action, frame, flags, scale);
}

// Sets Master Emerald shard color in Knuckles' cutscene
void RenderEmeraldShard(NJS_OBJECT *a1, int blend_mode, float scale)
{
	NJS_ARGB argb = { _nj_constant_material_.a, _nj_constant_material_.r, _nj_constant_material_.g, _nj_constant_material_.b };
	SetMaterial(1.0f, argb.r, 0.5f, argb.b);
	late_z_ofs___ = -4000.0f;
	late_DrawObjectClipMesh(a1, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
	SetMaterial(argb.a, argb.r, argb.g, argb.b);
}

// Emerald shard glow in Knuckles' cutscene
void RenderEmeraldShardGlow(NJS_MODEL_SADX* a1, int a2, float a3)
{
	float radius; // ST04_4
	if (a3 == 0.0f || a1->r == 0.0f || (radius = a3 * a1->r, dsCheckViewV(&a1->center, radius)))
	{
		if (!loop_count && !VerifyTexList(njds_texList))
		{
			if (a1)
			{
				if (a3 == 0.0f || a1->r == 0.0f || (radius = a3 * a1->r, dsCheckViewV(&a1->center, radius)))
				{
					late_z_ofs___ = current_event == 145 ? -2000.0f : 6000.0f;
					DrawModelMesh(a1, LATE_MAT);
					late_z_ofs___ = 0.0f;
				}
			}
		}
	}
}

// Calls the callback function to draw the emerald shards
void EmeraldShardCutscene_DrawLightRays_Callback(task *a1)
{
	late_SetFunc((void(__cdecl*)(void*))dispCrushLight, (void*)a1, -27000.0f, LATE_LIG);
}

// Draw the Ice Cap door with hierarchy and depth hacks
void IceCapDoorFix(NJS_MODEL_SADX *a1, LATE a2, float a3)
{
	// Draw the solid stuff underneath the wall
	dsDrawModel(ADV02_OBJECTS[25]->basicdxmodel);
	late_z_ofs___ = -27000.0f;
	// Draw the snowflake
	late_DrawModelClipMesh(ADV02_OBJECTS[25]->child->basicdxmodel, LATE_WZ, a3);
	late_z_ofs___ = -20000.0f;
	// Draw the icy surface
	late_DrawModelClipMesh(ADV02_OBJECTS[25]->child->sibling->basicdxmodel, LATE_WZ, a3);
	late_z_ofs___ = 0.0f;
}

// Hack to draw windows in Tails' workshop with a different function
void TailsWindowsHack(NJS_MODEL_SADX* model, LATE flags, float scale)
{
	late_DrawModelMesh(model, flags);
}

// Super ugly hack to single out landtable items for Big's house animation
// TODO: Maybe this is not necessary anymore with the interpolation fix?
void ParseMRColFlags()
{
	// Jungle area
	_OBJ_LANDTABLE* landtable = ___LANDTABLEMR[2];
	for (int j = 0; j < landtable->ssCount; j++)
	{
		if (landtable->pLandEntry[j].xWidth == 19.45961f)
			MRJungle_LanternTop = landtable->pLandEntry[j].pObject;
	}
	MRJungle_Propeller = landtable->pMotLandEntry->pObject->child->sibling->sibling->sibling;
	MRJungle_Propeller2 = landtable->pMotLandEntry->pObject->child->sibling->sibling;
}

// Hack to enable extra collision in Amy's Mission Mode
// TODO: Make it dynamic collision instead of landtable
void AmyMissionModeCollision()
{
	if (!IsLevelLoaded(LevelAndActIDs_MysticRuins1))
		return;

	if (!AmyMissionCollision && usPlayer == Characters_Amy && ulGlobalMode == MD_MISSION)
	{
		if (___LANDTABLEMR[0]->pLandEntry[0].xWidth > 252 && ___LANDTABLEMR[0]->pLandEntry[0].xWidth < 253)
			___LANDTABLEMR[0]->pLandEntry[0].slAttribute = 0x1;
		if (___LANDTABLEMR[0]->pLandEntry[1].xWidth > 260 && ___LANDTABLEMR[0]->pLandEntry[1].xWidth < 261)
			___LANDTABLEMR[0]->pLandEntry[1].slAttribute = 0x1;
		AmyMissionCollision = true;
	}
	if (AmyMissionCollision && (usPlayer != Characters_Amy || ulGlobalMode != MD_MISSION))
	{
		if (___LANDTABLEMR[0]->pLandEntry[0].xWidth > 252 && ___LANDTABLEMR[0]->pLandEntry[0].xWidth < 253)
			___LANDTABLEMR[0]->pLandEntry[0].slAttribute = 0;
		if (___LANDTABLEMR[0]->pLandEntry[1].xWidth > 260 && ___LANDTABLEMR[0]->pLandEntry[1].xWidth < 261)
			___LANDTABLEMR[0]->pLandEntry[1].slAttribute = 0;
		AmyMissionCollision = false;
	}
}

// Animate rotating stuff in MR Jungle
void JungleAnimations()
{
	if (!IsLevelLoaded(LevelAndActIDs_MysticRuins3))
		return;

	if (!ChkPause())
	{
		// These were originally animated in the landtable's motion but the animation is kinda broken so I rotate them manually
		MRJungle_Propeller->ang[0] += (0x444 * g_CurrentFrame) % 65535;
		MRJungle_Propeller2->ang[0] += (0x222 * g_CurrentFrame) % 65535;
		MRJungle_LanternTop->ang[1] += (0x111 * g_CurrentFrame) % 65535;
	}
}

// Expand and shrink fog dynamically in the jungle area
void JungleDynamicFog()
{
	if (!IsLevelLoaded(LevelAndActIDs_MysticRuins3) || !camera_twp || ChkPause())
		return;

	bool InsideTemple = CheckCollisionP(&TempleVector, 480.0f) != 0;

	// Force disable fog in two cutscenes in Big's story
	if (current_event != -1 && usPlayer == Characters_Big && (current_event == 208 || current_event == 226))
		InsideTemple = 1;

	// Make the fog thicker when in the jungle but outside temple
	if (camera_twp->pos.y < 300.0f && !InsideTemple)
	{
		gFog.f32StartZ = min(-1.0f, gFog.f32StartZ + 64.0f);
		gFog.f32EndZ = min(-3200.0f, gFog.f32EndZ + 128.0f);
	}
	// Reduce the fog when above the jungle or around the temple
	else
	{
		bool day = SeqGetTime() == SEQ_DAYLIGHT;
		gFog.f32StartZ = max(day ? -4000.0f : -1000.0f, gFog.f32StartZ - 64.0f);
		gFog.f32EndZ = max(day ? -16000.0f : -14000.0f, gFog.f32EndZ - 128.0f);
	}
}

void ADV02_Init()
{
	ReplaceSET("SETMR00A");
	ReplaceSET("SETMR00B");
	ReplaceSET("SETMR00E");
	ReplaceSET("SETMR00K");
	ReplaceSET("SETMR00L");
	ReplaceSET("SETMR00M");
	ReplaceSET("SETMR00S");
	ReplaceSET("SETMR01A");
	ReplaceSET("SETMR01B");
	ReplaceSET("SETMR01E");
	ReplaceSET("SETMR01K");
	ReplaceSET("SETMR01L");
	ReplaceSET("SETMR01M");
	ReplaceSET("SETMR01S");
	ReplaceSET("SETMR02S");
	ReplaceSET("SETMR03S");
	ReplaceCAM("CAMMR00S");
	ReplaceCAM("CAMMR01S");
	ReplaceCAM("CAMMR02S");
	ReplaceCAM("CAMMR03S");
	ReplacePVM("ADV_MR00");
	ReplacePVM("ADV_MR01");
	ReplacePVM("ADV_MR02");
	ReplacePVM("ADV_MR03");
	ReplacePVM("MROBJ");
	ReplacePVM("MR_SKY00");
	ReplacePVM("MR_SKY01");
	ReplacePVM("MR_SKY02");
	ReplacePVM("MR_TRAIN");
	ReplacePVM("TANKEN");
	ReplacePVM("MROBJ_MAST");
	ReplacePVM("MROBJ_MASTLDAM");
	ReplacePVM("MROBJ_MASTSDAM");
	ReplacePVM("MR_EGG");
	ReplacePVM("MR_PYRAMID");
	ReplacePVM("MR_TORNADO2");
	ReplacePVM("MR_FINALEGG");
	ReplaceGeneric("SL_X0B.BIN", "SL_X0B_DC.BIN"); // Day light direction override
	ReplaceGeneric("SL_X1B.BIN", "SL_X1B_DC.BIN"); // Evening light direction override
	ReplaceGeneric("SL_X2B.BIN", "SL_X2B_DC.BIN"); // Night light direction override
	// Fog/draw distance data
	for (int i = 0; i < 3; i++)
	{
		// Act 1 day
		pFogTable_Adv02[0][i].f32StartZ = -2000.0f;
		pFogTable_Adv02[0][i].f32EndZ = -14000.0f;
		pFogTable_Adv02[0][i].u8Enable = 1;
		pFogTable_Adv02[0][i].Col = 0xFFA0A0A0;
		// Act 2 day
		pFogTable_Adv02[1][i].f32StartZ = -2000.0f;
		pFogTable_Adv02[1][i].f32EndZ = -16000.0f;
		pFogTable_Adv02[1][i].Col = 0xFFA0A0A0;
		// Act 3 day
		pFogTable_Adv02[2][i].f32StartZ = -4000.0f;
		pFogTable_Adv02[2][i].f32EndZ = -16000.0f;
		pFogTable_Adv02[2][i].Col = 0xFFA0A0A0;
		// Act 1 evening
		pFogTable_Adv02[4][i].f32StartZ = -3500.0f;
		pFogTable_Adv02[4][i].f32EndZ = -9000.0f;
		pFogTable_Adv02[4][i].Col = 0xFF907858;
		// Act 2/3 evening
		pFogTable_Adv02[5][i].f32StartZ = -5000.0f;
		pFogTable_Adv02[5][i].f32EndZ = -12000.0f;
		pFogTable_Adv02[5][i].Col = 0xFF907858;
		// Act 1 night
		pFogTable_Adv02[6][i].f32StartZ = -2000.0f;
		pFogTable_Adv02[6][i].f32EndZ = -10000.0f;
		pFogTable_Adv02[6][i].Col = 0xFF001058;
		// Act 2/3 night
		pFogTable_Adv02[7][i].f32StartZ = -5000;
		pFogTable_Adv02[7][i].f32EndZ = -12000;
		pFogTable_Adv02[7][i].Col = 0xFF001058;
		// Draw distance
		pClipMap_Adv02[0][i].f32Far = -10000.0f;
		pClipMap_Adv02[1][i].f32Far = -10000.0f;
		pClipMap_Adv02[2][i].f32Far = -16000.0f; // Required for OFinalEgg
		pClipMap_Adv02[3][i].f32Far = -4000.0f;
	}
}

void ADV02_Load()
{
	// This is done every time the function is called
	LevelLoader(LevelAndActIDs_MysticRuins1, "SYSTEM\\data\\adv02_mysticruin\\landtablemr00.c.sa1lvl", ADV02_TEXLISTS[38], ___LANDTABLEMR[0]);
	LevelLoader(LevelAndActIDs_MysticRuins2, "SYSTEM\\data\\adv02_mysticruin\\landtablemr01.c.sa1lvl", ADV02_TEXLISTS[39], ___LANDTABLEMR[1]);
	LevelLoader(LevelAndActIDs_MysticRuins3, "SYSTEM\\data\\adv02_mysticruin\\landtablemr02.c.sa1lvl", ADV02_TEXLISTS[40], ___LANDTABLEMR[2]);
	LevelLoader(LevelAndActIDs_MysticRuins4, "SYSTEM\\data\\adv02_mysticruin\\landtablemr03.c.sa1lvl", ADV02_TEXLISTS[41], ___LANDTABLEMR[3]);
	ParseMRColFlags();
	// This is done only once
	if (!ModelsLoaded_ADV02)
	{
		// Enable SA1 water
		if (!CheckSADXWater(LevelIDs_MysticRuins))
			WriteCall((void*)0x0052FDC3, MRWater_Display);
		// Enable MR light direction adjustment code
		WriteData<6>((char*)0x00412536, 0x90u);
		WriteData<6>((char*)0x00412544, 0x90u);
		// Master Emerald fixes
		WriteCall((void*)0x0053CCD2, MasterEmeraldFix); // Always use the more expensive function to render the Master Emerald
		WriteCall((void*)0x0053CE7D, DrawMasterEmeraldGlow); // Depth hack
		WriteCall((void*)0x0053CEDC, DrawMasterEmeraldGlow); // Depth hack
		WriteData<2>((char*)0x0053CD1F, 0x90u); // Nop fabs to get the real values for RGB
		WriteData<2>((char*)0x0053CD0B, 0x90u); // Nop fabs to get the real values for RGB
		WriteData<2>((char*)0x0053CD31, 0x90u); // Nop fabs to get the real values for RGB
		DispATask_26_t = new Trampoline(0x0053CC10, 0x0053CC17, DispATask_26_r); // Set offset material
		// MR base stuff
		WriteJump((void*)0x00538430, FinalEggDisplayer_r);
		ADV02_ACTIONS[30]->object = LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_feturoa.nja.sa1mdl"); // OFinalWay (edited)
		OFinalEggModel_A = LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_fe_dodai_a.nja.sa1mdl"); // OFinalEgg (split into 4 models)
		OFinalEggModel_B = LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_fe_dodai_b.nja.sa1mdl");
		OFinalEggModel_C = LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_fe_dodai_c.nja.sa1mdl");
		OFinalEggModel_D = LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_fe_dodai_d.nja.sa1mdl");
		AddWhiteDiffuseMaterial(&OFinalEggModel_B->basicdxmodel->mats[7]); // Grass stuff
		AddWhiteDiffuseMaterial(&OFinalEggModel_A->basicdxmodel->mats[13]); // Pathways in the middle
		AddWhiteDiffuseMaterial(&OFinalEggModel_B->child->basicdxmodel->mats[5]); // City 1
		AddWhiteDiffuseMaterial(&OFinalEggModel_B->child->basicdxmodel->mats[6]); // City 2
		AddWhiteDiffuseMaterial(&OFinalEggModel_B->child->basicdxmodel->mats[7]); // City 3
		// Depth hacks
		WriteCall((void*)0x00538896, OTreeFix); // OTree depth
		WriteData<5>((void*)0x0052FC73, 0x90u); // Disable MR jungle callback
		WriteCall((void*)0x0053816F, RustlingGrassDepthFix1); // Rustling grass depth bias for Knuckles' cutscene
		WriteCall((void*)0x0053818F, RustlingGrassDepthFix2); // Rustling grass depth bias for Knuckles' cutscene
		// Code fixes
		DispATask_23_t = new Trampoline(0x0053B560, 0x0053B566, DispATask_23_r); // Set offset material for OBlockEntry
		WriteData((float*)0x005343BE, 239.0f); // Windows in Tails' workshop
		WriteData((float*)0x005343EF, 239.0f); // Windows in Tails' workshop 
		WriteData<1>((char*)0x005370E0, 0x01); // Fix blending mode on floating bricks in WV entrance
		WriteData<1>((char*)0x00537038, 0x01); // Fix blending mode on floating bricks in WV entrance
		WriteData<1>((char*)0x00537181, 0x01); // Fix blending mode on floating bricks in WV entrance
		WriteData<1>((char*)0x00537354, 0x01); // Fix blending mode on floating bricks in WV entrance
		// Emerald shard fixes
		WriteCall((void*)0x006F4AF3, RenderEmeraldShard); // Emerald shard (cutscene)
		WriteCall((void*)0x006F4BF3, RenderEmeraldShardGlow); // Emerald shard (cutscene) glow
		WriteCall((void*)0x006F4F02, RenderEmeraldShardGlow); // Emerald shard (cutscene) glow (big)
		WriteCall((void*)0x007A69FA, EmeraldShardCutscene_DrawLightRays_Callback); // Draw light rays behind glow
		WriteCall((void*)0x007A6A8F, EmeraldShardCutscene_DrawLightRays_Callback); // Draw light rays behind glow
		// Cutscene after Lost World
		WriteData((float*)0x006D2537, 16.0f); // Y1
		WriteData((float*)0x006D2507, 16.0f); // Y2
		WriteData((float*)0x006D1CF6, 14.52f); // Y after cutscene
		WriteData((int*)0x006D1D13, 0); // X rotation after cutscene
		WriteData((int*)0x006D1D1D, 0); // Z rotation after cutscene
		WriteData((float*)0x006BA1EC, 200.5f); // Fix Tails' position in "The time has come at last"
		WriteData((float*)0x006BA24E, 200.5f); // Fix Tails' position in "The time has come at last"
		WriteData((float*)0x006BA326, 200.5f); // Fix Tails' position in "The time has come at last"
		// Palm trees near Tails' house
		LoadModel_ReplaceMeshes(ADV02_OBJECTS[67], "system\\data\\adv02_mysticruin\\common\\models\\mrobj_s_kasoyasi.nja.sa1mdl");
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->sibling->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->sibling->sibling->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[67]->child->sibling->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
		ForceLevelSpecular_Object(ADV02_OBJECTS[84], false); // Windows and the light above the door of Tails' house
		AddWhiteDiffuseMaterial(&ADV02_OBJECTS[85]->basicdxmodel->mats[4]); // Same as above but lit up
		ForceLevelSpecular_Object(ADV02_OBJECTS[85], false);
		// Material fixes
		ADV02_OBJECTS[90]->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR; // Palm trees
		ADV02_OBJECTS[91]->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR; // Palm trees
		ForceLevelSpecular_Object(ADV02_OBJECTS[96], true); // Silver statue (queued so flags get messed up)
		ForceLevelSpecular_Object(ADV02_OBJECTS[97], true); // Gold statue (queued so flags get messed up)
		// Ice Cap door fix
		*ADV02_OBJECTS[23] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrb_f_doorice.nja.sa1mdl"); // Ice Cap door (edited hierarchy)
		*ADV02_OBJECTS[25] = *ADV02_OBJECTS[23]->child->sibling;
		WriteCall((void*)0x0053E0B2, IceCapDoorFix);
		// OHandKey fixes
		ADV02_OBJECTS[76]->evalflags |= NJD_EVAL_HIDE;
		ADV02_OBJECTS[88]->evalflags |= NJD_EVAL_HIDE;
		WriteCall((void*)0x005322C3, OHandKeyFix);
		// Other objects
		WriteCall((void*)0x0053430D, TailsWindowsHack);
		object_tanken_c_tanken_c = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\tanken_c.nja.sa1mdl"); // TANKEN
		object_tanken_d_tanken_d = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\tanken_d.nja.sa1mdl"); // TANKEN 2
		object_tanken_a_tanken_a = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\tanken_a.nja.sa1mdl"); // TANKEN 3
		*ADV02_OBJECTS[90] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_treea.nja.sa1mdl"); // Tree
		*ADV02_OBJECTS[91] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrc_s_yasi.nja.sa1mdl"); // Palm tree
		MROcean = LoadModel("system\\data\\adv02_mysticruin\\sky\\mra_s_umi.nja.sa1mdl"); // Ocean in Act 1
		AddTextureAnimation_Permanent(LevelIDs_MysticRuins, 0, &MROcean->basicdxmodel->mats[0], false, 5, 130, 139);
		*ADV02_OBJECTS[64] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrj_n_kakusicolib.nja.sa1mdl"); // Angel Island rock
		*ADV02_OBJECTS[68] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrobj_s_egggdum.nja.sa1mdl"); // That thing that pushes the Chao Egg out
		*ADV02_OBJECTS[100] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrj_n_bigkusanul.nja.sa1mdl"); // Grass
		*ADV02_ACTIONS[32]->object = *ADV02_OBJECTS[100]; // Rustling grass
		*ADV02_ACTIONS[18]->object = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrobj_s_bliwb.nja.sa1mdl"); // Wall in Tails' house
		*ADV02_OBJECTS[61] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mra_o_door_2.nja.sa1mdl"); // OIslandDoor
		*ADV02_OBJECTS[60] = *ADV02_OBJECTS[61]->child->sibling; // OIslandDoor right
		ADV02_OBJECTS[60]->pos[2] = 0.05f; // Fix Z fighting
		*ADV02_OBJECTS[59] = *ADV02_OBJECTS[61]->child; // OIslandDoor left
		ADV02_OBJECTS[59]->pos[2] = 0.05f; // Fix Z fighting
		*ADV02_OBJECTS[71] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mri_k_uraguti.nja.sa1mdl"); // The gate for Tails' Tornado
		*ADV02_OBJECTS[69] = *ADV02_OBJECTS[71]->child;
		*ADV02_OBJECTS[70] = *ADV02_OBJECTS[71]->child->sibling;
		*ADV02_OBJECTS[72] = *ADV02_OBJECTS[71]->child->sibling->sibling; // I have no idea why it's set up this way
		*ADV02_ACTIONS[10]->object = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrobj_train.nja.sa1mdl"); // Train
		AddWhiteDiffuseMaterial(&ADV02_ACTIONS[10]->object->child->sibling->sibling->sibling->basicdxmodel->mats[9]);
		*ADV02_ACTIONS[21]->object = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrobj_s_kassort.nja.sa1mdl"); // Plane platform
		*ADV02_MODELS[14] = *LoadModel("system\\data\\adv02_mysticruin\\common\\models\\mrobj_nc_iwaent_b.nja.sa1mdl")->basicdxmodel; // Rock
		AddWhiteDiffuseMaterial(&ADV02_MODELS[9]->mats[2]);
		ModelsLoaded_ADV02 = true;
	}
}

void ADV02_OnFrame()
{
	if (ssStageNumber != LevelIDs_MysticRuins)
		return;

	switch (ssActNumber)
	{
		case 0:
			AmyMissionModeCollision();
			break;
		case 2:
			JungleAnimations();
			JungleDynamicFog();
			break;
		case 3:
			// Prevent dynamic light direction from being adjusted in Eggman's base
			c_roty = 0;
			c_rotz = 0;
			break;
	}
}