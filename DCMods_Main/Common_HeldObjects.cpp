#include "stdafx.h"
#include "Common_HeldObjects.h"

float SnowboardOffset1 = 2.72f;
float SnowboardOffset2 = -2.72f;
float HeldOffset_Sonic_1 = 0.0f; // 3.7
float HeldOffset_Sonic_2 = 0.0f; // 2.7
float HeldOffset_Sonic_3 = 0.0f; // 1.7
float HeldOffset_Sonic_4 = 0.0f; // 1.0
float HeldOffset_Tails_1 = -0.5f; // 3.3
float HeldOffset_Tails_2 = 0.0f; // 2.2
float HeldOffset_Tails_3 = 0.0f; // 1.7
float HeldOffset_Tails_4 = 0.0f; // 1.0
float HeldOffset_Knuckles_1 = 0.0f; // 1.5
float HeldOffset_Knuckles_2 = 0.0f; // 1.2
float HeldOffset_Knuckles_3 = 0.0f; // 2.7
float HeldOffset_Knuckles_4 = 0.0f; // 0.0
float HeldOffset_Big = 0.0f; // 2.0
double PutDownZOff = 1000.0f;
float HotShelterPuzzleDistance = -2.0f;
float StationSquarePuzzleDistance = 3.5f;
float MRPuzzleDistance1 = 2.6f; // 0.1
float MRPuzzleDistance2 = 9.5f; // 7.0
float Chaos6FreezerOffset = 1.25f;
float SampleSOffset = 0.0f;

// Trampoline to fix the burger shop statue shadow and floor collision
static Trampoline* ObjectSSMannequin_t = nullptr;
static void __cdecl ObjectSSMannequin_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectSSMannequin);
	short flag = tp->twp->flag;
	if (flag & 0x1000)
	{
		tp->twp->counter.b[1] = 1;
	}
	if (!(flag & 0x1))
		tp->twp->counter.b[0]++;
	else
		tp->twp->counter.b[0] = 0;
	if (tp->twp->counter.b[0] > 100)
		tp->twp->counter.b[0] = 10;
	original(tp);
}

// Trampoline to fix the mission statue shadow and floor collision
static Trampoline* Exec_0_MissionStatue_t = nullptr;
static void __cdecl Exec_0_MissionStatue_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(Exec_0_MissionStatue);
	short flag = tp->twp->flag;
	if (flag & 0x1000)
	{
		tp->twp->counter.b[1] = 1;
	}
	if (!(flag & 0x1))
		tp->twp->counter.b[0]++;
	else
		tp->twp->counter.b[0] = 0;
	if (tp->twp->counter.b[0] > 100)
		tp->twp->counter.b[0] = 10;
	original(tp);
}

// Statue display function
void Statue_Display(task* tp, bool mission)
{
	taskwk* twp; // esi
	Angle yang; // eax
	float YDist; // [esp+0h] [ebp-Ch]

	twp = tp->twp;
	if (GetPlayerTaskPointer(0)->exec)
	{
		njPushMatrix(0);
		njSetTexture(mission ? &texlist_mi_3dasu : ADV00_TEXLISTS[6]);
		float yoff = twp->counter.b[1] ? 2.1f : 0.0f;
		YDist = twp->pos.y + twp->value.f - yoff;
		njTranslate(0, twp->pos.x, YDist, twp->pos.z);
		yang = twp->ang.y;
		if (yang)
		{
			njRotateY(0, (unsigned __int16)yang);
		}
		SaveControl3D();
		njControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
		SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
		if (mission)
			late_DrawObjectClip(&object_mi_3dasu_fat_d, LATE_WZ, 1.0f);
		else
			late_DrawMotionClip(MODEL_SS_PEOPLE_OBJECTS[12], MODEL_SS_PEOPLE_MOTIONS[19], 0.0f, LATE_WZ, 1.0f);
		ResetMaterial();
		LoadControl3D();
		if (twp->counter.b[0] < 8 && !(twp->flag & 0x1000))
		{
			SetTextureToCommon();
			njTranslate(0, 0.0f, 0.1f - twp->value.f, 0.0f);
			late_DrawShadowObject(&object_shadow, 1.0f);
		}
		njPopMatrix(1u);
	}
}

// Mission statue display function hook
void __cdecl Disp_0_r(task* tp)
{
	Statue_Display(tp, true);
}

// Burger shop statue display function hook
void __cdecl MannequinDisplayer_r(task* tp)
{
	Statue_Display(tp, false);
}

// Trampoline for Mystic Ruins lock
static Trampoline* HandKey_Display_t = nullptr;
static void __cdecl HandKey_Display_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(HandKey_Display);
	task* lock = MRLockInfo[tp->twp->btimer];
	NJS_VECTOR* pos_lock = nullptr;
	NJS_VECTOR* pos_key = &tp->twp->pos;	
	if (lock)
	{
		pos_lock = &lock->twp->pos;
		// Offset the key when it's inside the lock
		if (pos_lock && pos_key->x == pos_lock->x && pos_key->z == pos_lock->z)
			pos_key->y = max(pos_key->y, pos_lock->y + 2.5f);
	}
	original(tp);
}

// njTranslateV hack to add Y offsets to held objects
void __fastcall njTranslateVHack(NJS_MATRIX_PTR m, const NJS_VECTOR* v)
{
	njTranslate(m, v->x, v->y - 2.5f, v->z);
}

void HeldObjects_Init()
{
	if (ModConfig::FixHeldObjects)
	{
		ObjectSSMannequin_t = new Trampoline(0x00630780, 0x00630785, ObjectSSMannequin_r);
		Exec_0_MissionStatue_t = new Trampoline(0x00593510, 0x00593515, Exec_0_MissionStatue_r);
		HandKey_Display_t = new Trampoline(0x00532240, 0x00532245, HandKey_Display_r);
		WriteData((float**)0x00495975, &SnowboardOffset1); // Sonic
		WriteData((float**)0x0045E56D, &SnowboardOffset1); // Tails
		WriteData((float**)0x00495989, &SnowboardOffset2); // Sonic
		WriteData((float**)0x0045E581, &SnowboardOffset2); // Tails
		WriteData((double**)0x0044232A, &PutDownZOff); // Z offset check when putting objects down
		//WriteData((float*)0x0049D867, 0.0f); // Doesn't do shit
		WriteData((float*)0x0049D902, 0.0f); // Y offset after being put down
		WriteData((float*)0x0049D957, 0.01f); // Y speed after hitting ceiling
		WriteData((float**)0x0053C96E, &MRPuzzleDistance1); // Wind/Ice/Statue key distance check
		WriteData((float**)0x0053C919, &MRPuzzleDistance2); // Wind/Ice/Statue key rotation check
		WriteData((float**)0x0059A93B, &HotShelterPuzzleDistance); // Hot Shelter cube puzzle distance check
		WriteData((float**)0x006367CE, &StationSquarePuzzleDistance); // Station Square cube puzzle distance check
		WriteData((float**)0x007AC559, &SampleSOffset); // SampleS
		WriteData((float**)0x0055BBEA, &Chaos6FreezerOffset); // Chaos 6 freezer
		// Hook njTranslate calls to add a vertical offset for all objects involved
		for (unsigned int i = 0; i < LengthOfArray(njTranslateVCalls); i++)
		{
			WriteCall((void*)njTranslateVCalls[i], njTranslateVHack);
		}
		WriteJump(MannequinDisplayer, MannequinDisplayer_r); // Statue shadow/Y position fix
		WriteJump(Disp_0, Disp_0_r); // Statue shadow/Y position fix
		// Collision adjustments
		WriteData((float**)0x0049577F, &HeldOffset_Sonic_1);
		WriteData((float**)0x00495777, &HeldOffset_Sonic_2);
		WriteData((float**)0x00495787, &HeldOffset_Sonic_3);
		WriteData((float**)0x0049578F, &HeldOffset_Sonic_4);
		WriteData((float**)0x0045D5F5, &HeldOffset_Tails_1);
		WriteData((float**)0x0045D5ED, &HeldOffset_Tails_2);
		WriteData((float**)0x0045D5FD, &HeldOffset_Tails_3);
		WriteData((float**)0x0045D605, &HeldOffset_Tails_4);
		WriteData((float**)0x00477892, &HeldOffset_Knuckles_1);
		WriteData((float**)0x004778A2, &HeldOffset_Knuckles_2);
		WriteData((float**)0x0047789A, &HeldOffset_Knuckles_3);
		WriteData((float**)0x004778AA, &HeldOffset_Knuckles_4);
		WriteData((float**)0x0048E250, &HeldOffset_Big);
		cinfo_mannequin_0[0].center.y = 0.0f;
		cinfo_mannequin_0[1].center.y = 0.0f;
		obj_c_info_1[0].center.y = 0.0f;
		obj_c_info_1[1].center.y = -0.5f;
		ColliInfo_6[0].center.y = 0.0f;
		ColliInfo_6[1].center.y = 0.0f;
		ColliInfo_6[1].a = 1.0f;
		obj_c_info_3[0].center.y = 0.0f;
		obj_c_info_3[1].center.y = -0.5f;
		ColliInfo_9[0].center.y = 0.0f;
		ColliInfo_9[1].center.y = -2.2f;
		ccinfo_8[0].center.y = 0.0f;
		ccinfo_8[1].center.y = 0.0f;
		ColliInfo_51[0].center.y = 0.5f;
		ColliInfo_51[1].center.y = 0.5f;
		obj_c_info_0[0].center.y = 0.0f;
		obj_c_info_0[1].center.x = -1.0f;
		obj_c_info_0[1].center.y = 0.0f;
		key_c_info[0].center.y = 2.0f;
		key_c_info[1].center.y = 0.0f;
		key_c_info[1].a = 0.0f;
		obj_c_info_2[0].center.y = 0.0f;
		obj_c_info_2[0].center.x = -1.0f;
		obj_c_info_2[1].center.y = 0.0f;
		cci_sscard_0[0].center.y = 0.0f;
		cci_sscard_0[0].center.x = -1.0f;
		cci_sscard_0[1].center.y = 0.0f;
		cci_sscard[0].center.y = 0.0f;
		cci_sscard[1].center.y = 0.0f;
		cci_sscard[1].a = 1.0f;
		ccinfo_25[0].center.y = 0.0f;
		ccinfo_25[1].a = 1.0f;
		alm_colli_info[0].center.y = 1.0f;
		fruit_colli_info[0].center.y = 1.0f;
		bomb1_info[1].a = 2.8f; // Z offset after putting down		
	}
}