#pragma once

#include <SADXModLoader.h>

DataArray(HintMessageTable* [5], HintMessageStageTable, 0x009BED60, 43); // Hint message table for all levels, 5 languages per level
DataArray(HintMessageTable, stage_al_garden_00_ss_hint_tbl_j, 0x008811A0, 7); // SS Garden hint messages in Japanese
DataArray(HintMessageTable, stage_al_garden_00_ss_hint_tbl_e, 0x008811D8, 7); // SS Garden hint messages in English
DataArray(HintMessageTable, stage_al_garden_00_ss_hint_tbl_f, 0x00881210, 7); // SS Garden hint messages in French
DataArray(HintMessageTable, stage_al_garden_00_ss_hint_tbl_s, 0x00881248, 7); // SS Garden hint messages in Spanish
DataArray(HintMessageTable, stage_al_garden_00_ss_hint_tbl_g, 0x00881280, 7); // SS Garden hint messages in German
DataArray(HintMessageTable*, stage_al_race_hint_tbl_pointer, 0x0342372C, 5); // Chao Race hint messages in 5 languages (unused in DX, message ID 8 is actual message)

TaskFunc(FDisp, 0x0071B210); // DX X/Y button prompts
TaskFunc(ParamNameplateDisplayer, 0x00738260); // Chao stat panel display function
VoidFunc(DrawSaltWater3D_39, 0x00728E20); // Draws the water in SS and MR gardens

void Garden00_Init();
void Garden01_Init();
void Garden02_Init();

void Garden00_Load();
void Garden01_Load();
void Garden02_Load();
void AL_ChaoRace_Load();

void AL_Egg_Load();
void AL_Fruit_Load();
void AL_Tree_Load();
void AL_Transporter_Load();
void AL_VMU_Load();

void Garden00_OnFrame();
void AL_VMU_OnFrame();