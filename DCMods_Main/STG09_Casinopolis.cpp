#include "stdafx.h"
#include "STG09_Casinopolis.h"

static bool ModelsLoaded_STG09;

// Part of OSlG, OSlB etc. without the light
NJS_OBJECT* OSlRBase = nullptr;
NJS_OBJECT* OSlGBase = nullptr;
NJS_OBJECT* OSlBBase = nullptr;
NJS_OBJECT* OSlYBase = nullptr;

NJS_OBJECT* ODento_1 = nullptr;
NJS_OBJECT* ODento_2 = nullptr;
NJS_OBJECT* ODento_3 = nullptr;

NJS_OBJECT* IdeyaCap1_1 = nullptr;
NJS_OBJECT* IdeyaCap1_2 = nullptr;
NJS_OBJECT* IdeyaCap1_3 = nullptr;
NJS_OBJECT* IdeyaCap2_1 = nullptr;
NJS_OBJECT* IdeyaCap2_2 = nullptr;
NJS_OBJECT* IdeyaCap2_3 = nullptr;
NJS_OBJECT* IdeyaCap3_1 = nullptr;
NJS_OBJECT* IdeyaCap3_2 = nullptr;
NJS_OBJECT* IdeyaCap3_3 = nullptr;
NJS_OBJECT* IdeyaCap4_1 = nullptr;
NJS_OBJECT* IdeyaCap4_2 = nullptr;
NJS_OBJECT* IdeyaCap4_3 = nullptr;

NJS_OBJECT* LhtR_1 = nullptr;
NJS_OBJECT* LhtR_2 = nullptr;

NJS_OBJECT* LhtG_1 = nullptr;
NJS_OBJECT* LhtG_2 = nullptr;

static bool TailsPinballSoundPlayed = false;

static Trampoline* OTikeiAnim_Load_t = nullptr;
static Trampoline* OTikeiAnim_Main_t = nullptr;
static Trampoline* Loop_Main_t = nullptr;

// Draws the water that was moved to the landtable in DX
void OTikeiAnim_Display(task* a1)
{
	if (ssActNumber == 0 && !loop_count)
	{
		___njFogDisable();
		njSetTexture(&texlist_casino01);
		njPushMatrix(0);
		njTranslate(0, 0, 0, 0);
		late_z_ofs___ = -47960.0f;
		late_DrawObjectClip(&object_cas_wt_2f_mizu_2f_mizu, LATE_LIG, 1.0f);
		late_z_ofs___ = 0.0f;
		njPopMatrix(1u);
	}
}

// Hook to add water that was moved to the landtable in DX (Load function)
static void __cdecl OTikeiAnim_Load_r(task* a1)
{
	const auto original = TARGET_DYNAMIC(OTikeiAnim_Load);
	a1->disp = OTikeiAnim_Display;
	original(a1);
}

// Hook to add water that was moved to the landtable in DX
static void __cdecl OTikeiAnim_Main_r(task* a1)
{
	const auto original = TARGET_DYNAMIC(OTikeiAnim_Main);
	original(a1);
	if (ssActNumber == 0)
	{
		OTikeiAnim_Display(a1);
	}
}

// Trampoline to fix the angle for gears and ceiling screen
void FixGear(task* tp, NJS_POINT3* vec, int angY)
{		
	ObjCasino_MobileObj(tp, vec, tp->twp->ang.y + angY);
}

// Hook to play a sound when Sonic goes through NiGHTS' rings
static void __cdecl Loop_Main_r(task* a1)
{
	taskwk* v1 = a1->twp;
	const auto original = TARGET_DYNAMIC(Loop_Main);
	if (v1->cwp->flag & 1 && v1->counter.f > 38.0f)
	{
		dsPlay_oneshot(SE_CA_NIGHTSRING, 0, 0, 0);
	}
	original(a1);
}

// LhtG display
void __cdecl Disp_76_OLhtg(task* tp)
{
	taskwk* twp = tp->twp;
	int z = twp->ang.z;
	int x = twp->ang.x;
	int y = twp->ang.y;

	if (!CheckRangeOutWithR(tp, 360010.0f) && dsCheckViewV(&twp->pos, 105.0f))
	{
		SetObjectTexture2(12);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		if (z)
			njRotateZ(0, z);
		if (x)
			njRotateX(0, x);
		if (y)
			njRotateY(0, y);
		late_DrawObjectClip(twp->mode == 0 ? LhtG_2 : LhtG_1, LATE_LIG, 1.0f);
		njPopMatrix(1);
	}
}

// LhtR display
void __cdecl Disp_77_OLhtr(task *tp)
{
	taskwk* twp = tp->twp;
	int z = twp->ang.z;
	int x = twp->ang.x;
	int y = twp->ang.y;

	if (!CheckRangeOutWithR(tp, 360010.0f) && dsCheckViewV(&twp->pos, 30.0f))
	{
		ObjCasino_SetTexAnimInfo(&texanim_5 + (twp->mode == 0), 1);
		SetObjectTexture2(12);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		if (z)
			njRotateZ(0, z);
		if (x)
			njRotateX(0, x);
		if (y)
			njRotateY(0, y);
		late_DrawObjectClip(twp->mode == 0 ? LhtR_2 : LhtR_1, LATE_LIG, 1.0f);
		njPopMatrix(1u);
	}
}

// Returns the model for OSlX without the light part and hides everything but the light in the original model
NJS_OBJECT* CreateOSlXBaseModel(NJS_OBJECT* original)
{
	// Result is the model without the light
	NJS_OBJECT* result = CloneObject(original);
	original->evalflags |= NJD_EVAL_HIDE;
	HideMesh_Object(original->child, 0, 1);
	HideMesh_Object(result->child, 2);
	return result;
}

// Renders OSlX without the light and then the light separately
void OSlxDisplayNew(NJS_ACTION* action, float frame, int flags, float scale)
{
	NJS_OBJECT* BaseObject = OSlYBase;
	if (action->object == &object_cas_obj_s_light_r_s_light_r)
		BaseObject = OSlRBase;
	else if (action->object == &object_cas_obj_s_light_g_s_light_g)
		BaseObject = OSlGBase;
	else if (action->object == &object_cas_obj_s_light_b_s_light_b)
		BaseObject = OSlBBase;
	dsDrawMotionClip(BaseObject, action->motion, frame, scale);
	late_z_ofs___ = 8000.0f;
	late_ActionClipMesh(action, frame, LATE_MAT, scale);
	late_z_ofs___ = 0;
}

void RenderLightA(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = 8000.0f;
	late_DrawObjectClip(a1, LATE_LIG, a3);
	late_z_ofs___ = 0;
}

void RenderOKBSText(NJS_OBJECT *obj, float scale)
{
	DrawModel(obj->basicdxmodel);
	late_z_ofs___ = 1000.0f;
	late_DrawObjectClip(obj->child, LATE_LIG, 1.0f);
	late_z_ofs___ = 0;
}

void RenderOKBCText(NJS_ACTION *a1, float frame, float scale)
{
	late_z_ofs___ = 1000.0f;
	late_ActionClipMesh(a1, frame, LATE_WZ, scale);
	late_z_ofs___ = 2000.0f;
	late_DrawModel(a1->object->child->sibling->sibling->sibling->sibling->basicdxmodel, LATE_LIG);
	late_z_ofs___ = 0;
}

void NeonKFix(NJS_MODEL_SADX* model, float scale)
{
	late_DrawModel(model, LATE_WZ);
}

void JackPotFix1(NJS_MODEL_SADX *a1)
{
	late_z_ofs___ = 10000.0f;
	late_DrawModel(a1, LATE_LIG);
	late_z_ofs___ = 0;
}

void JackPotFix2(NJS_MODEL_SADX *a1)
{
	late_z_ofs___ = 20000.0f;
	late_DrawModel(a1, LATE_LIG);
	late_z_ofs___ = 0;
}

void cNormal_7_PinballJackpotSprite(task *ctp)
{
	taskwk *twp; // esi
	char mode; // edi
	char btimer = 0; // al
	twp = ctp->twp;
	bool currFrame = ((g_CurrentFrame == 1 && ulGlobalTimer % 2 == 0) || g_CurrentFrame >= 2);
	if (currFrame)
	{
		if (twp->wtimer)
			--twp->wtimer;
		else
			goto render;
	}
render:
	mode = ctp->ptp->twp->mode;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	anim_name[mode]->p.x = (posi_table[11 * mode + twp->mode].x * 2.8f + 320.0f) * ScreenRaitoX;
	anim_name[mode]->p.y = (240.0f - posi_table[11 * mode + twp->mode].y * 2.8f) * ScreenRaitoY;
	anim_name[mode]->sy = (posi_table[11 * mode + twp->mode].z) * ScreenRaitoY;
	anim_name[mode]->sx = anim_name[mode]->sy;
	late_z_ofs___ = 10000.0f;
	late_DrawSprite2D(anim_name[mode], twp->btimer, 22047.0f, NJD_SPRITE_ALPHA, LATE_NCOMP);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	late_z_ofs___ = 0;
	if (currFrame)
	{
		btimer = twp->btimer + 1;
		twp->btimer = btimer;
		if (btimer == 8)
		{
			FreeTask(ctp);
		}
		if (twp->btimer == 3)
		{
			++ctp->ptp->twp->smode;
		}
	}
}

void IdeyaCapFix(NJS_OBJECT *a1, NJS_MOTION *a2, float animframe, float scale)
{
	NJS_OBJECT* Object1 = IdeyaCap1_1;
	NJS_OBJECT* Object2 = IdeyaCap1_2;
	NJS_OBJECT* Object3 = IdeyaCap1_3;
	if (a1 == &object_card_obj_ideacp_ideacp)
	{
		Object1 = IdeyaCap1_1;
		Object2 = IdeyaCap1_2;
		Object3 = IdeyaCap1_3;
	}
	else if (a1 == &object_card_obj_g_ideacp_ideacp)
	{
		Object1 = IdeyaCap2_1;
		Object2 = IdeyaCap2_2;
		Object3 = IdeyaCap2_3;
	}
	else if (a1 == &object_card_obj_y_ideacp_ideacp)
	{
		Object1 = IdeyaCap3_1;
		Object2 = IdeyaCap3_2;
		Object3 = IdeyaCap3_3;
	}
	else if (a1 == &object_card_obj_b_ideacp_ideacp)
	{
		Object1 = IdeyaCap4_1;
		Object2 = IdeyaCap4_2;
		Object3 = IdeyaCap4_3;
	}
	// Process non-transparent parts
	dsDrawMotionClip(Object1, a2, animframe, scale);
	late_z_ofs___ = 2000.0f;
	// Process transparent parts
	late_DrawMotionClipMesh(Object2, a2, animframe, LATE_MAT, scale);
	late_DrawMotionClipMesh(Object3, a2, animframe, LATE_MAT, scale);
	late_z_ofs___ = 0.0f;
}

void TDenkiFix(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawObjectClip(a1, LATE_MAT, 1.0f);
}

void TDenkiFix2(void(__cdecl* function)(task*), task* data, float depth, LATE queueflags)
{
	late_SetFunc((void(__cdecl*)(void*))function, (void*)data, 4000.0f, 0);
}

void MizuAFix(void(__cdecl* function)(task*), task* data, float depth, LATE queueflags)
{
	late_SetFunc((void(__cdecl*)(void*))function, (void*)data, 2000.0f, LATE_WZ);
}

void ODentoFix(NJS_OBJECT* obj, float scale)
{
	if (obj == &object_csn_dentou_dentou)
	{
		ds_DrawModelClip(object_csn_dentou_dentou.basicdxmodel, scale);
		late_DrawModelClip(object_csn_dentou_dentou.child->basicdxmodel, LATE_WZ, scale);
		late_DrawModelClip(object_csn_dentou_dentou.child->sibling->basicdxmodel, LATE_WZ, scale);
		late_DrawModelClip(object_csn_dentou_dentou.child->sibling->sibling->basicdxmodel, LATE_LIG, scale); 
	}
	else
		ds_DrawObjectClip(obj, scale);
}

void SetUpIdeyaCapModels(NJS_OBJECT* Object1, NJS_OBJECT* Object2, NJS_OBJECT* Object3)
{
	// Hide transparent meshes in the first object
	Object1->evalflags |= NJD_EVAL_HIDE; // Root node (transparent base)
	Object1->child->sibling->evalflags |= NJD_EVAL_HIDE; // Transparent sphere
	// Hide everything but the first transparent mesh (root node) in the second object
	Object2->child->evalflags |= NJD_EVAL_HIDE; // Non-transparent base
	Object2->child->sibling->evalflags |= NJD_EVAL_HIDE; // Transparent sphere
	Object2->child->child->evalflags |= NJD_EVAL_HIDE; // Horns
	Object2->child->child->sibling->evalflags |= NJD_EVAL_HIDE; // Horns
	Object2->child->child->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Horns
	Object2->child->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
	Object2->child->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
	Object2->child->sibling->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
	// Hide everything but the second transparent mesh (sphere) in the third object
	Object3->evalflags |= NJD_EVAL_HIDE; // Root node(transparent base)
	Object3->child->evalflags |= NJD_EVAL_HIDE; // Non-transparent base
	Object3->child->child->evalflags |= NJD_EVAL_HIDE; // Horns
	Object3->child->child->sibling->evalflags |= NJD_EVAL_HIDE; // Horns
	Object3->child->child->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Horns
	Object3->child->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
	Object3->child->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
	Object3->child->sibling->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE; // Wings
}

void Casinopolis_Init()
{
	ReplaceCAM("CAM0900K");
	ReplaceCAM("CAM0900S");
	ReplaceCAM("CAM0901M");
	ReplaceCAM("CAM0901S");
	ReplaceCAM("CAM0902S");
	ReplaceCAM("CAM0903S");
	ReplaceSET("SET0900K");
	ReplaceSET("SET0900S");
	ReplaceSET("SET0901M");
	ReplaceSET("SET0901S");
	ReplaceSET("SET0902S");
	ReplaceSET("SET0903S");
	ReplaceGeneric("SETMI0900K.BIN", "SETMI0900K_DC.BIN"); // Fixed Casino mission objects not spawning (?)
	ReplacePVM("CASINO01");
	ReplacePVM("CASINO02");
	ReplacePVM("CASINO03");
	ReplacePVM("CASINO04");
	ReplacePVM("OBJ_CASINO2");
	ReplacePVM("OBJ_CASINO8");
	ReplacePVM("OBJ_CASINO9");
	ReplacePVM("OBJ_CASINO_E");
	// Fog and draw distance stuff 
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg09[0][i].Col = 0xFF000000;
		pFogTable_Stg09[0][i].f32StartZ = 800.0f;
		pFogTable_Stg09[0][i].u8Enable = 1;
		pFogTable_Stg09[0][i].f32EndZ = 2000.0f;
		pFogTable_Stg09[1][i].Col = 0xFF707000;
		pFogTable_Stg09[1][i].f32StartZ = 1.0f;
		pFogTable_Stg09[1][i].f32EndZ = 4000.0f;
		pFogTable_Stg09[1][i].u8Enable = 1;
		pClipMap_Stg09[1][i].f32Far = -2000.0;
	}
}

void Casinopolis_Load()
{
	LevelLoader(LevelAndActIDs_Casinopolis1, "system\\data\\stg09_casino\\landtable0900.c.sa1lvl", &texlist_casino01);
	LevelLoader(LevelAndActIDs_Casinopolis2, "system\\data\\stg09_casino\\landtable0901.c.sa1lvl", &texlist_casino02);
	LevelLoader(LevelAndActIDs_Casinopolis3, "system\\data\\stg09_casino\\landtable0902.c.sa1lvl", &texlist_casino03);
	LevelLoader(LevelAndActIDs_Casinopolis4, "system\\data\\stg09_casino\\landtable0903.c.sa1lvl", &texlist_casino04);
	if (!ModelsLoaded_STG09)
	{
		WriteData<1>((char*)0x005D4454, 0x01); // OTutuA queue flag fix 1
		WriteData<1>((char*)0x005D4489, 0x01); // OTutuA queue flag fix 2
		WriteData<1>((char*)0x005D0A9D, 0x85); // Reverse the loop_count check to fix the animation for the pumps in the gears room
		OTikeiAnim_Load_t = new Trampoline(0x005CB160, 0x005CB165, OTikeiAnim_Load_r);
		OTikeiAnim_Main_t = new Trampoline(0x005CB0A0, 0x005CB0A6, OTikeiAnim_Main_r);
		if (ModConfig::DLLLoaded_SoundOverhaul)
			Loop_Main_t = new Trampoline(0x005D5F50, 0x005D5F56, Loop_Main_r); // Only with Sound Overhaul because the DX sound is broken
		WriteCall((void*)0x005D3C0C, FixGear);
		// Code fixes
		WriteData((float*)0x005C0CA4, 10000.0f); // BALLS depth
		object_cas_wt_2f_mizu_2f_mizu = *LoadModel("system\\data\\stg09_casino\\act01\\models\\cas_wt_2f_mizu.nja.sa1mdl");
		AddTextureAnimation_Permanent(9, 0, &object_cas_wt_2f_mizu_2f_mizu.basicdxmodel->mats[0], true, 3, 75, 68, 69, 70, 71, 72, 73, 74, 67, 76, 77, 78, 79, 80, -1, -1);
		WriteCall((void*)0x005DF170, TDenkiFix2); // TDenki rendering 1
		WriteCall((void*)0x005DF08A, TDenkiFix); // TDenki blending
		WriteData<5>((void*)0x005DF073, 0x90u); // Disable ZFunc set in TDenki
		WriteData<5>((void*)0x005DF092, 0x90u); // Disable ZFunc set in TDenki
		WriteCall((void*)0x005DE7A0, MizuAFix); // To make TDenki fix work
		WriteData<1>((char*)0x008135F2, 0x0i8); // Reala thing UV fix
		// Ideya cap fixes
		WriteCall((void*)0x005D79A9, IdeyaCapFix);
		IdeyaCap1_1 = CloneObject(&object_card_obj_ideacp_ideacp);
		IdeyaCap1_2 = CloneObject(IdeyaCap1_1);
		IdeyaCap1_3 = CloneObject(IdeyaCap1_1);
		SetUpIdeyaCapModels(IdeyaCap1_1, IdeyaCap1_2, IdeyaCap1_3);
		IdeyaCap2_1 = CloneObject(&object_card_obj_g_ideacp_ideacp);
		IdeyaCap2_2 = CloneObject(IdeyaCap2_1);
		IdeyaCap2_3 = CloneObject(IdeyaCap2_1);
		SetUpIdeyaCapModels(IdeyaCap2_1, IdeyaCap2_2, IdeyaCap2_3);
		IdeyaCap3_1 = CloneObject(&object_card_obj_y_ideacp_ideacp);
		IdeyaCap3_2 = CloneObject(IdeyaCap3_1);
		IdeyaCap3_3 = CloneObject(IdeyaCap3_1);
		SetUpIdeyaCapModels(IdeyaCap3_1, IdeyaCap3_2, IdeyaCap3_3);
		IdeyaCap4_1 = CloneObject(&object_card_obj_b_ideacp_ideacp);
		IdeyaCap4_2 = CloneObject(IdeyaCap4_1);
		IdeyaCap4_3 = CloneObject(IdeyaCap4_1);
		SetUpIdeyaCapModels(IdeyaCap4_1, IdeyaCap4_2, IdeyaCap4_3);
		// Jackpot sprite fixes
		WriteCall((void*)0x005E1144, JackPotFix1);
		WriteCall((void*)0x005E1187, JackPotFix2);
		WriteJump(cNormal_7, cNormal_7_PinballJackpotSprite);
		// Act 2 lights
		WriteJump((void*)0x005C9980, Disp_76_OLhtg);
		WriteJump((void*)0x005C9BA0, Disp_77_OLhtr);
		// OLightA stuff
		AddWhiteDiffuseMaterial(&object_csn_t_light_a_t_light_a.basicdxmodel->mats[1]);
		WriteCall((void*)0x005DDBE4, RenderLightA); // OLightA
		// OSlX objects
		OSlRBase = CreateOSlXBaseModel(&object_cas_obj_s_light_r_s_light_r);
		OSlGBase = CreateOSlXBaseModel(&object_cas_obj_s_light_g_s_light_g);
		OSlBBase = CreateOSlXBaseModel(&object_cas_obj_s_light_b_s_light_b);
		OSlYBase = CreateOSlXBaseModel(&object_cas_obj_s_light_y_s_light_y);
		WriteCall((void*)0x005CE630, OSlxDisplayNew);
		WriteCall((void*)0x005CAB20, NeonKFix);
		if (ModConfig::EnableCowgirl)
			Cowgirl_Init();
		// Card pinball entrance OTensC
		object_cas_obj_tens_card_tens_card = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_tens_card.nja.sa1mdl");
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_tens_card_tens_card.basicdxmodel->meshsets[0], 12, 65, 0); // Main object UV animation 1
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_tens_card_tens_card.basicdxmodel->meshsets[5], 12, 65, 0); // Main object UV animation 1
		WriteData((NJS_MESHSET_SADX**)0x1E76D30, &object_cas_obj_tens_card_tens_card.child->basicdxmodel->meshsets[1]); // Child UV animation 1
		WriteData((NJS_MESHSET_SADX**)0x1E76D40, &object_cas_obj_tens_card_tens_card.child->basicdxmodel->meshsets[0]); // Child UV animation 2
		// Card pinball entrance top animation OKBC
		WriteCall((void*)0x005CEA2A, RenderOKBCText);
		object_cas_obj_card_kanban_card_kanban.child->sibling->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE;
		AddAlphaRejectMaterial(&object_cas_obj_card_kanban_card_kanban.child->sibling->sibling->sibling->sibling->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_cas_obj_card_kanban_card_kanban.child->sibling->sibling->sibling->sibling->basicdxmodel->mats[1]);
		// Card pinball entrance side animation OCardKan
		object_csn_kanban_card_kanban_card = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\csn_kanban_card.nja.sa1mdl");
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_card_kanban_card.basicdxmodel->meshsets[1], 8, 65, 0); // Meshset order changed after sorting
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_card_kanban_card.child->basicdxmodel->meshsets[0], 8, 65, 0);
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_card_kanban_card.child->sibling->basicdxmodel->meshsets[0], 8, 65, 0);
		ForceLevelSpecular_Object(&object_csn_kanban_card_kanban_card, false);
		AddWhiteDiffuseMaterial(&object_csn_kanban_card_kanban_card.basicdxmodel->mats[0]); // Material order changed after sorting
		AddWhiteDiffuseMaterial(&object_csn_kanban_card_kanban_card.basicdxmodel->mats[2]); // Material order changed after sorting
		AddWhiteDiffuseMaterial(&object_csn_kanban_card_kanban_card.basicdxmodel->mats[3]); // Material order changed after sorting
		AddAlphaRejectMaterial(&object_csn_kanban_card_kanban_card.basicdxmodel->mats[4]);
		AddAlphaRejectMaterial(&object_csn_kanban_card_kanban_card.basicdxmodel->mats[5]);
		AddAlphaRejectMaterial(&object_csn_kanban_card_kanban_card.child->sibling->basicdxmodel->mats[5]);
		// Slot pinball entrance OTensS
		object_cas_obj_tens_slot_tens_slot = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_tens_slot.nja.sa1mdl"); // Main object
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_tens_slot_tens_slot.basicdxmodel->meshsets[0], 12, 65, 0); // Main object UV animation 1
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_tens_slot_tens_slot.basicdxmodel->meshsets[11], 12, 65, 0); // Main object UV animation 2
		WriteData((NJS_MESHSET_SADX**)0x1E76F18, &object_cas_obj_tens_slot_tens_slot.child->basicdxmodel->meshsets[0]); // Child UV animation (meshes merged in the DC version so just one)
		// Slot pinball entrance top animation OKBS
		WriteCall((void*)0x005CE84B, RenderOKBSText);
		AddAlphaRejectMaterial(&object_csn_kanban_slot_kanban_slot.child->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_csn_kanban_slot_kanban_slot.child->basicdxmodel->mats[1]);
		// Slot pinball entrance side animation OSlotKan
		object_csn_kanban_slot_kanban_slot = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\csn_kanban_slot.nja.sa1mdl");
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_slot_kanban_slot.basicdxmodel->meshsets[2], 8, 65, 0);
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_slot_kanban_slot.child->basicdxmodel->meshsets[1], 8, 65, 0);
		AddUVAnimation_Permanent(9, 0, &object_csn_kanban_slot_kanban_slot.child->sibling->basicdxmodel->meshsets[2], 8, 65, 0); // Meshset order changed after sorting
		AddWhiteDiffuseMaterial(&object_csn_kanban_slot_kanban_slot.basicdxmodel->mats[3]); // Material order changed after sorting
		AddWhiteDiffuseMaterial(&object_csn_kanban_slot_kanban_slot.child->sibling->basicdxmodel->mats[0]); // Material order changed after sorting
		// ODento
		object_csn_dentou_dentou = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\csn_dentou.nja.sa1mdl"); // Sorted model
		AddAlphaRejectMaterial(&object_csn_dentou_dentou.child->sibling->sibling->basicdxmodel->mats[4]);
		WriteCall((void*)0x005DDADF, ODentoFix);
		// OLhtR
		LhtR_1 = CloneObject(&object_cas_rt_lht_rt_lht);
		LhtR_2 = CloneObject(&object_cas_rt_lht_rt_lht);
		LhtR_2->basicdxmodel->mats[2].attr_texId = 15; // Faded light
		// OLhtG
		LhtG_1 = CloneObject(&object_cas_gb_lht_gb_lht);		
		LhtG_2 = CloneObject(&object_cas_gb_lht_gb_lht);
		LhtG_2->basicdxmodel->mats[4].attr_texId = 15; // Faded light
		// Models
		AddWhiteDiffuseMaterial(&object_csn_sr_light_sr_light.basicdxmodel->mats[2]); // OShwrl
		model_casi_banp_banp = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\casi_banp.nja.sa1mdl")->basicdxmodel; // Bumper1 pressed
		model_casi_banp_k_banp_k = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\casi_banp_k.nja.sa1mdl")->basicdxmodel; // Bumper1
		model_card_obj_yajirusi_a_yajirusi_a = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\card_obj_yajirusi_a.nja.sa1mdl")->basicdxmodel; // Kazariyaji
		model_card_obj_yajirusi_b_yajirusi_b = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\card_obj_yajirusi_b.nja.sa1mdl")->basicdxmodel; // Kazariyaji2
		model_card_obj_yajirusi_c_yajirusi_c = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\card_obj_yajirusi_c.nja.sa1mdl")->basicdxmodel; // Kazariyaji green 2
		model_card_obj_yajirusi_d_yajirusi_d = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\card_obj_yajirusi_d.nja.sa1mdl")->basicdxmodel; // Kazariyaji pink 2
		model_csn_kinka_kinka = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\csn_kinka.nja.sa1mdl")->basicdxmodel; // Sonic token
		model_cas_obj_ten_monitor_ten_monitor = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_ten_monitor.nja.sa1mdl")->basicdxmodel; // OCfO rotating thing
		AddTextureAnimation_Permanent(9, 0, &object_cas_obj_ten_monitor_ten_monitor.basicdxmodel->mats[3], false, 120, 140, 147);
		object_cas_fan_fan = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_fan.nja.sa1mdl"); // OFanFan
		object_cas_obj_6slot_a_6slot_a = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_6slot_a.nja.sa1mdl"); // Slot red
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_6slot_a_6slot_a.basicdxmodel->meshsets[8], 16, 65, 0);
		object_cas_obj_6slot_6slot = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_6slot.nja.sa1mdl"); // Slot blue
		AddUVAnimation_Permanent(9, 0, &object_cas_obj_6slot_6slot.basicdxmodel->meshsets[8], 16, 65, 0);
		object_casino_lion_lion = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\casino_lion.nja.sa1mdl"); // Lion
		AddUVAnimation_Permanent(9, 0, &object_casino_lion_lion.child->sibling->sibling->basicdxmodel->meshsets[5], 8, -127, 0);
		AddUVAnimation_Permanent(9, 0, &object_casino_lion_lion.child->sibling->sibling->basicdxmodel->meshsets[10], 8, -127, 0);
		object_alegg_body_o_body_o = *LoadModel("system\\data\\stg09_casino\\common\\models\\waka_models\\alegg_body_o.nja.sa1mdl"); // Flying cock
		ModelsLoaded_STG09 = true;
	}
}

void Casinopolis_OnFrame()
{
	if (ssStageNumber != LevelIDs_Casinopolis)
		return;
	EVL_KazariRotY += 256;
	// Tails' sound thing
	if (ssActNumber != 1)
		TailsPinballSoundPlayed = false;
	else
	{
		auto entity = playertwp[0];
		if (!TailsPinballSoundPlayed && entity != nullptr && entity->pos.y > -1698.0f)
		{
			dsPlay_oneshot(SE_CA_OUTFALL, 0, 0, 0);
			TailsPinballSoundPlayed = true;
		}
	}
}