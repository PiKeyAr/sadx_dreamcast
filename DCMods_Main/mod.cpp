#include "stdafx.h"
#include "mod.h"

static bool InitError = false; // Stop all code if true
static int LanternErrorMessageTimer = 0; // Timer used to display "Lantern Engine not found"
static int PauseHideErrorMessageTimer = 0;  // Timer used to display Pause Hide incompatibility warning
static bool InitFinished = false; // If true, it means the mod has run all initialization functions

const HelperFunctions* helperFunctionsGlobal;
bool UpdatePaletteAtlas = false;

// Clean up and load general stuff when level data loads
static Trampoline* LoadLevelFiles_t = nullptr;
static void __cdecl LoadLevelFiles_r()
{
	const auto original = TARGET_DYNAMIC(LoadLevelFiles);
	RemoveLateDrawLandtable();	
	ClearTextureAnimationData();
	ClearLoadedLevels(ssStageNumber << 8 | ssActNumber);
	Animals_Load();
	Characters_Load();
	original();
}

// Trampoline that loads landtable items to be rendered after the skybox
static Trampoline* LoadSkyboxObject_t = nullptr;
static void __cdecl LoadSkyboxObject_r()
{
	const auto original = TARGET_DYNAMIC(LoadSkyboxObject);
	original();
	LoadLateDrawLand();
}

static void __declspec(naked) land_DrawObjectHacc()
{
	__asm
	{
		push[esp + 04h] // a2
		push edi // a1

		call land_DrawObject_New

		pop edi // a1
		add esp, 4 // a2
		retn
	}
}

// Checks if any incompatible mods are loaded, or if any of them replace the nullsub calls
void CheckCompatibleMods()
{
	// Autodemo mods check
	if (GetModuleHandle(L"AutoDemo_TestLevels") != nullptr)
		ModConfig::EnabledLevels[LevelIDs_HedgehogHammer] = false;
	if (GetModuleHandle(L"AutoDemo_WindyValley") != nullptr)
		ModConfig::EnabledLevels[LevelIDs_WindyValley] = false;
	if (GetModuleHandle(L"AutoDemo_SpeedHighway") != nullptr)
		ModConfig::EnabledLevels[LevelIDs_SpeedHighway] = false;
	if (GetModuleHandle(L"AutoDemo_RedMountain") != nullptr)
		ModConfig::EnabledLevels[LevelIDs_RedMountain] = false;
	// Nullsub check
	if (nullsub_STG00 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_HedgehogHammer] = false;
	if (nullsub_STG01 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EmeraldCoast] = false;
	if (nullsub_STG02 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_WindyValley] = false;
	if (nullsub_STG03 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_TwinklePark] = false;
	if (nullsub_STG04 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_SpeedHighway] = false;
	if (nullsub_STG05 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_RedMountain] = false;
	if (nullsub_STG06 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_SkyDeck] = false;
	if (nullsub_STG07 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_LostWorld] = false;
	if (nullsub_STG08 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_IceCap] = false;
	if (nullsub_STG09 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Casinopolis] = false;
	if (nullsub_STG10 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_FinalEgg] = false;
	if (nullsub_STG12 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_HotShelter] = false;
	if (nullsub_B_CHAOS0 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Chaos0] = false;
	if (nullsub_B_CHAOS2 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Chaos2] = false;
	if (nullsub_B_CHAOS4 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Chaos4] = false;
	if (nullsub_B_CHAOS6 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Chaos6] = false;
	if (nullsub_B_CHAOS7 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_PerfectChaos] = false;
	if (nullsub_B_EGM1 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EggHornet] = false;
	if (nullsub_B_EGM2 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EggWalker] = false;
	if (nullsub_B_EGM3 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EggViper] = false;
	if (nullsub_B_ROBO == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Zero] = false;
	if (nullsub_B_E101 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_E101] = false;
	if (nullsub_B_E101R == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_E101R] = false;
	if (nullsub_ADV00 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_StationSquare] = false;
	if (nullsub_ADV01 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EggCarrierOutside] = false;
	if (nullsub_ADV01C == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_EggCarrierInside] = false;
	if (nullsub_ADV02 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_MysticRuins] = false;
	if (nullsub_ADV03 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_Past] = false;
	if (nullsub_SHOOTING1 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_SkyChase1] = false;
	if (nullsub_SHOOTING2 == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_SkyChase2] = false;
	if (nullsub_SBOARD == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_SandHill] = false;
	if (nullsub_MINICART == 0x90u)
		ModConfig::EnabledLevels[LevelIDs_TwinkleCircuit] = false;
	if (nullsub_Chao == 0x90u)
	{
		ModConfig::DisableChao = true;
		ModConfig::EnabledLevels[LevelIDs_SSGarden] = false;
		ModConfig::EnabledLevels[LevelIDs_MRGarden] = false;
		ModConfig::EnabledLevels[LevelIDs_ECGarden] = false;
		ModConfig::EnabledLevels[LevelIDs_ChaoRace] = false;
	}
	// Pause hide check
	if (GetModuleHandle(L"pause-hide") != nullptr)
	{
		if (ModConfig::RemoveMap || ModConfig::RemoveCamera || ModConfig::RemoveSetUpPad)
			PauseHideErrorMessageTimer = 300;
	}
}

// Runs the mod's main initialization functions
void InitFinal()
{
	Fish_Init();
	Animals_Init();
	General_Init();
	Characters_Init();
	Enemies_Init();
	Event_Init();
	HeldObjects_Init();
	Objects_Init();
	if (ModConfig::EnableDCBranding)
	{
		AdvertiseTutorials_Init();
		AdvertiseFeatures_Init();
		AdvertiseGUI_Init();
		AdvertiseCredits_Init();
	}
	else
		ModConfig::TitleScreenLogoMode = TitleScreenConfig::SADX;
	if (ModConfig::TitleScreenLogoMode != TitleScreenConfig::SADX)
		AdvertiseTitleScreen_Init();
	if (ModConfig::RestoreDemos != DemosConfig::Off)
		AdvertiseDemos_Init();
	if (ModConfig::EnabledLevels[LevelIDs_StationSquare])
	{
		ADV00_Init();
		WriteCall((void*)0x004231E6, ADV00_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EggCarrierOutside])
	{
		ADV01_Init();
		WriteCall((void*)0x004232C9, ADV01_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EggCarrierOutside])
	{
		ADV01C_Init();
		WriteCall((void*)0x004233BB, ADV01C_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_MysticRuins])
	{
		ADV02_Init();
		WriteCall((void*)0x004234AD, ADV02_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Past])
	{
		ADV03_Init();
		WriteCall((void*)0x00423554, ADV03_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EmeraldCoast])
	{
		EmeraldCoast_Init();
		WriteCall((void*)0x00422B68, EmeraldCoast_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley])
	{
		WindyValley_Init();
		WriteCall((void*)0x00422BD3, WindyValley_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_TwinklePark])
	{
		TwinklePark_Init();
		WriteCall((void*)0x00422C3E, TwinklePark_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_SpeedHighway])
	{
		SpeedHighway_Init();
		WriteCall((void*)0x00422CA9, SpeedHighway_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_RedMountain])
	{
		RedMountain_Init();
		WriteCall((void*)0x00422D14, RedMountain_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_SkyDeck])
	{
		SkyDeck_Init();
		WriteCall((void*)0x00422D84, SkyDeck_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_LostWorld])
	{
		LostWorld_Init();
		WriteCall((void*)0x00422DEF, LostWorld_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_IceCap])
	{
		IceCap_Init();
		WriteCall((void*)0x00422E5A, IceCap_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Casinopolis])
	{
		Casinopolis_Init();
		WriteCall((void*)0x00422EE8, Casinopolis_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_FinalEgg])
	{
		FinalEgg_Init();
		WriteCall((void*)0x00422F71, FinalEgg_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_HotShelter])
	{
		HotShelter_Init();
		WriteCall((void*)0x00422FFF, HotShelter_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Chaos0])
	{
		B_CHAOS0_Init();
		WriteCall((void*)0x00423088, B_CHAOS0_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Chaos2])
	{
		B_CHAOS2_Init();
		WriteCall((void*)0x004230B7, B_CHAOS2_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Chaos4])
	{
		B_CHAOS4_Init();
		WriteCall((void*)0x004230CD, B_CHAOS4_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Chaos6])
	{
		B_CHAOS6_Init();
		WriteCall((void*)0x004230E3, B_CHAOS6_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_PerfectChaos])
	{
		B_CHAOS7_Init();
		WriteCall((void*)0x00423108, B_CHAOS7_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EggHornet])
	{
		B_EGM1_Init();
		WriteCall((void*)0x00423146, B_EGM1_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EggWalker])
	{
		B_EGM2_Init();
		WriteCall((void*)0x0042315F, B_EGM2_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_EggViper])
	{
		B_EGM3_Init();
		WriteCall((void*)0x00423178, B_EGM3_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_E101])
	{
		B_E101_Init();
		WriteCall((void*)0x004231AF, B_E101_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_Zero])
	{
		B_ROBO_Init();
		WriteCall((void*)0x00423196, B_ROBO_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_E101R])
	{
		B_E101_R_Init();
		WriteCall((void*)0x004231CD, B_E101_R_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_SkyChase1] || ModConfig::EnabledLevels[LevelIDs_SkyChase2])
		SkyChase_Init();
	if (ModConfig::EnabledLevels[LevelIDs_SkyChase1])
	{
		WriteCall((void*)0x004236B1, SkyChase_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_SkyChase2])
	{
		WriteCall((void*)0x004236E0, SkyChase_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_HedgehogHammer])
	{
		HedgehogHammer_Init();
		WriteCall((void*)0x00422B2A, HedgehogHammer_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_TwinkleCircuit])
	{
		TwinkleCircuit_Init();
		WriteCall((void*)0x004235EC, TwinkleCircuit_Load);
	}
	if (ModConfig::EnabledLevels[LevelIDs_SandHill])
	{
		SandHill_Init();
		WriteCall((void*)0x0042370F, SandHill_Load);
	}
	if (!ModConfig::DisableChao)
	{
		AL_Main_Init();
		AL_ChaoRace_Init();
		WriteCall((void*)0x00423795, AL_Main_Load);
	}
	if (!ModConfig::DisableAllVideoStuff)
		Videos_Init();
	if (ModConfig::EnableSpeedFixes)
		SpeedFixes_Init();
	UpdateExportVariables();
	InitFinished = true;
}

extern "C"
{
	__declspec(dllexport) void __cdecl Init(const char *path, const HelperFunctions &helperFunctions)
	{
		helperFunctionsGlobal = &helperFunctions;
		// Check which DLLs are loaded
		HMODULE LanternDLL = GetModuleHandle(L"sadx-dc-lighting");
		ModConfig::DLLLoaded_HDGUI = (GetModuleHandle(L"HD_GUI") != nullptr);
		ModConfig::DLLLoaded_SA1Chars = (GetModuleHandle(L"SA1_Chars") != nullptr);
		ModConfig::DLLLoaded_Lantern = LanternDLL != nullptr;
		ModConfig::DLLLoaded_DLCs = (GetModuleHandle(L"DLCs_Main") != nullptr);
		ModConfig::DLLLoaded_SADXFE = (GetModuleHandle(L"sadx-fixed-edition") != nullptr);
		ModConfig::DLLLoaded_SoundOverhaul = (GetModuleHandle(L"sadx-sound-overhaul") != nullptr);
		ModConfig::DLLLoaded_Korean = (GetModuleHandle(L"sadx-korean-mod") != nullptr);
		// Error messages
		if (!ModConfig::DLLLoaded_Lantern)
			LanternErrorMessageTimer = 300;
		if (ModConfig::DLLLoaded_Lantern && GetProcAddress(LanternDLL, "palette_from_mix") == nullptr)
		{
			MessageBox(WindowHandle,
				L"Please update the Lantern Engine mod. Dreamcast Conversion requires Lantern Engine 1.5.5 or newer.",
				L"DC Conversion error: Lantern Engine out of date", MB_OK | MB_ICONERROR);
			InitError = true;
		}
		if (helperFunctions.Version < 7)
		{
			MessageBox(WindowHandle,
				L"Please update SADX Mod Loader. Dreamcast Conversion requires API version 7 or newer.",
				L"DC Conversion error: Mod Loader out of date", MB_OK | MB_ICONERROR);
			InitError = true;
		}
		// Check for old mod DLLs
		std::wstring OldModsMessage = L"Old/incompatible mods detected!\n\n"
			L"The following mods are outdated and will cause "
			L"problems if you leave them enabled. These mods are "
			L"no longer needed because they are built into the "
			L"main Dreamcast Conversion mod.\n\n"
			L"Please uninstall the following mods in the Mod Manager:\n\n";
		bool OldModsFound = false;
		for (unsigned int i = 0; i < LengthOfArray(OldModDLLs); i++)
		{
			if (GetModuleHandle(OldModDLLs[i]) != nullptr)
			{
				// Found a known incompatible mod
				OldModsMessage += OldModDLLs[i];
				OldModsMessage += '\n';
				OldModsFound = true;
			}
		}
		if (OldModsFound)
		{
			MessageBox(WindowHandle, OldModsMessage.c_str(),
				L"DC Conversion error: incompatible mods detected",
				MB_OK | MB_ICONERROR);
			InitError = true;
		}
		if (InitError)
			return;
		LoadLevelFiles_t = new Trampoline(0x00422AD0, 0x00422AD8, LoadLevelFiles_r);
		LoadSkyboxObject_t = new Trampoline(0x00414420, 0x00414427, LoadSkyboxObject_r);
		LoadModConfig(path);
		WriteCall((void*)0x0043A757, land_DrawObjectHacc);
		WriteCall((void*)0x0043A7CD, land_DrawObjectHacc);
	}

	__declspec(dllexport) void __cdecl OnFrame()
	{
		if (InitError)
			return;
		if (!InitFinished)
		{
			CheckCompatibleMods();
			InitFinal();
		}
		Objects_OnFrame();
		Event_OnFrame();
		Characters_OnFrame();
		General_OnFrame();
		// Display error messages
		if (!ModConfig::SuppressWarnings && LanternErrorMessageTimer && (IsIngame() || ulGlobalMode == MD_TITLE2))
		{
			BackupDebugFontSettings();
			SetDebugFontSize(Uint16(10.0f * (float)VerticalResolution / 480.0f));
			SetDebugFontColor(0xFFBFBFBF);
			DisplayDebugString(NJM_LOCATION(2, 1), "Failed to detect the Lantern Engine mod.");
			DisplayDebugString(NJM_LOCATION(2, 2), "Dreamcast levels will have no lighting,");
			DisplayDebugString(NJM_LOCATION(2, 3), "and alpha rejection fixes will not be applied.");
			DisplayDebugString(NJM_LOCATION(2, 4), "Please install and enable Lantern Engine for correct visuals.");
			LanternErrorMessageTimer--;
			RestoreDebugFontSettings();
		}
		if (!ModConfig::SuppressWarnings && PauseHideErrorMessageTimer && (IsIngame() || ulGlobalMode == MD_TITLE2))
		{
			BackupDebugFontSettings();
			SetDebugFontSize(Uint16(10.0f * (float)VerticalResolution / 480.0f));
			SetDebugFontColor(0xFFBFBFBF);
			DisplayDebugString(NJM_LOCATION(2, 6), "The Pause Hide mod interferes with");
			DisplayDebugString(NJM_LOCATION(2, 7), "some options in Dreamcast Conversion.");
			DisplayDebugString(NJM_LOCATION(2, 8), "Dreamcast Conversion already includes");
			DisplayDebugString(NJM_LOCATION(2, 9), "the functionality of the Pause Hide mod.");
			DisplayDebugString(NJM_LOCATION(2, 10), "Please disable or remove it.");
			PauseHideErrorMessageTimer--;
			RestoreDebugFontSettings();
		}
		// Animate materials and UVs
		if (!ChkPause() && Camera_Data1)
		{
			RunCustomAnimations();
		}
		// Config stuff
		if (ModConfig::EnableDCBranding)
		{
			AdvertiseTutorials_OnFrame();
			AdvertiseFeatures_OnFrame();
		}
		if (ModConfig::TitleScreenLogoMode != TitleScreenConfig::SADX)
			AdvertiseTitleScreen_OnFrame();
		if (ModConfig::RestoreDemos != DemosConfig::Off)
			AdvertiseDemos_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_StationSquare])
			ADV00_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_EggCarrierOutside])
			ADV01_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_EggCarrierInside])
			//ADV01C_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_MysticRuins])
			ADV02_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_Past]) ADV03_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_EggHornet]) B_EGM1_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_EggViper]) B_EGM3_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_Zero]) B_ROBO_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_E101R])	B_E101_R_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_PerfectChaos]) B_CHAOS7_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_EmeraldCoast])
			EmeraldCoast_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_WindyValley])
			WindyValley_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_TwinklePark]) TwinklePark_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_TwinklePark] || EnabledLevels[LevelIDs_TwinkleCircuit]) ShareObj_OnFrame();
		//if (ModConfig::EnabledLevels[LevelIDs_SpeedHighway]) SpeedHighway_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_RedMountain])
			RedMountain_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_SkyDeck])
			SkyDeck_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_LostWorld])
			LostWorld_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_IceCap])
			IceCap_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_Casinopolis])
			Casinopolis_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_FinalEgg])
			FinalEgg_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_HotShelter])
			HotShelter_OnFrame();
		//Subgames_OnFrame();
		if (!ModConfig::DisableChao)
			AL_Main_OnFrame();
		if (ModConfig::EnabledLevels[LevelIDs_ChaoRace])
			AL_ChaoRace_OnFrame();
		if (!ModConfig::DisableAllVideoStuff) 
			Videos_OnFrame();
		if (ModConfig::EnableSpeedFixes) 
			SpeedFixes_OnFrame();
	}

	__declspec(dllexport) void __cdecl OnInput()
	{
		if (InitError)
			return;
		if (ModConfig::CutsceneSkipFix != CutsceneSkipConfig::DefaultDX)
			Event_OnInput();
		if (ModConfig::RestoreDemos != DemosConfig::Off)
			AdvertiseDemos_OnInput();
	}

	__declspec(dllexport) void __cdecl OnRenderSceneStart()
	{
		if (InitError)
			return;
		// Updates the palette atlas for Lantern Engine
		if (UpdatePaletteAtlas)
		{
			if (ModConfig::DLLLoaded_Lantern)
				generate_atlas();
			UpdatePaletteAtlas = false;
		}
	}

	__declspec(dllexport) void __cdecl OnRenderDeviceReset()
	{
		if (InitError) 
			return;
		AdvertiseTitleScreen_OnReset();
	}

	__declspec(dllexport) ModInfo SADXModInfo = { ModLoaderVer };
}