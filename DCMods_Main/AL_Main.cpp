#include "stdafx.h"
#include "AL_Main.h"
#include "FunctionHook.h"

// TODO: Rewrite all functions, restore proper object lists and load SET files

static bool ModelsLoaded_Chao;

FunctionHook<void> DrawSaltWater3D_39_h(DrawSaltWater3D_39);

// Expands an array of HintMessageTable, useful for modifying Chao hint messages etc.
HintMessageTable* ExpandHintMessageTable(HintMessageTable* sourcetable, int oldsize, HintMessageTable &newentry)
{
	std::vector<HintMessageTable> hintlistvector;

	// Add original and new data to vector
	const HintMessageTable* oldlist = &sourcetable[0];
	for (int i = 0; i < oldsize;i++)
	{
		//PrintDebug("Adding entry %d\n", i);
		hintlistvector.push_back(oldlist[i]);
	}
	hintlistvector.push_back(newentry);
	// Create a new HintMEssageTable list
	auto size = hintlistvector.size();
	auto newlist = new HintMessageTable[size];
	memcpy(newlist, hintlistvector.data(), sizeof(HintMessageTable) * size);
	return newlist;
}

 // Restores Chao Race hints from an unused messages array
void LoadChaoGardenHintMessages()
{
	for (int l = 0; l < 4; l++)
	{
		for (int la = 0;la < 5;la++)
		{
			// Since the arrays could be replaced by another mod, retrieve the pointers to them instead of using static pointers
			HintMessageTable* pointer_table = (HintMessageTable*)HintMessageStageTable[39 + l][la];
			HintMessageTable* pointer_race = (HintMessageTable*)stage_al_race_hint_tbl_pointer[la];
			//PrintDebug("Pointer: %X\n", pointer);
			HintMessageStageTable[39 + l][la] = ExpandHintMessageTable(pointer_table, 7, pointer_race[8]);
		}
	}
}

// Queues XY button prompts with a callback
void FixChaoButtonPrompts(task *a1)
{
	late_SetFunc((void(__cdecl*)(void*))FDisp, (void*)a1, 22952.0f, LATE_LIG);
}

// Queues Chao stat panel with a callback
void FixChaoStatPanel(task *a1)
{
	late_SetFunc((void(__cdecl*)(void*))ParamNameplateDisplayer, (void*)a1, 22952.0f, LATE_LIG);
}

// Hook to draw DX water in SS and MR gardens
static void __cdecl DrawSaltWater3D_39_r()
{
	if (ChaoStageNumber == CHAO_STG_SS && ModConfig::EnabledLevels[LevelIDs_SSGarden])
		return;
	else if (ChaoStageNumber == CHAO_STG_MR && ModConfig::EnabledLevels[LevelIDs_MRGarden])
		return;
	DrawSaltWater3D_39_h.Original();
}

void AL_Main_Init()
{
	ReplacePVM("CHAO");
	ReplacePVM("CHAO_OBJECT");
	ReplacePVM("CHAO_HYOUJI");
	ReplacePVM("CHAO_HYOUJI_E");
	ReplacePVM("CHAO_HYOUJI_F");
	ReplacePVM("CHAO_HYOUJI_G");
	ReplacePVM("CHAO_HYOUJI_S");
	ReplacePVM("EC_ALIFE");
	if (ModConfig::EnabledLevels[LevelIDs_SSGarden])
		Garden00_Init();
	if (ModConfig::EnabledLevels[LevelIDs_ECGarden])
		Garden01_Init();
	if (ModConfig::EnabledLevels[LevelIDs_MRGarden])
		Garden02_Init();
}

void AL_Main_Load()
{
	// This stuff is done only once
	if (!ModelsLoaded_Chao)
	{
		if (ModConfig::ReplaceEggs)
			AL_Egg_Load();
		if (ModConfig::ReplaceFruits != FruitConfig::DefaultDX)
			AL_Fruit_Load();
		if (ModConfig::ReplaceTrees)
			AL_Tree_Load();
		if (ModConfig::ReplaceNameMachine)
			AL_VMU_Load();
		if (ModConfig::DisableChaoButtonPrompts)
		{
			WriteData<5>((void*)0x007195AE, 0x90); // Don't load SADX button prompts in SS garden
			WriteData<5>((void*)0x00718E20, 0x90); // Don't load SADX button prompts in MR garden
			WriteData<5>((void*)0x00719181, 0x90); // Don't load SADX button prompts in EC garden
		}
		else 
			WriteData((TaskFuncPtr*)0x0071B3D3, FixChaoButtonPrompts);
		WriteData((TaskFuncPtr*)0x007382A4, FixChaoStatPanel);
		LoadChaoGardenHintMessages(); // Restore Chao Race hints
		DrawSaltWater3D_39_h.Hook(DrawSaltWater3D_39_r); // Render DX water in SS/MR gardens if their DC versions are disabled
		AL_Transporter_Load();
		ModelsLoaded_Chao = true;
	}
}

void AL_Main_OnFrame()
{
	if (ModConfig::EnabledLevels[LevelIDs_SSGarden])
		Garden00_OnFrame();
	if (ModConfig::ReplaceNameMachine)
		AL_VMU_OnFrame();
}