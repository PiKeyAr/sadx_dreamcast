#pragma once

#include <SADXModLoader.h>

FunctionPointer(void, dispCrushLight, (task* tp), 0x007A6950); // Displays light rays in explosions etc.