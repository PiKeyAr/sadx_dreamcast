#include "stdafx.h"
#include "AL_Object_Egg.h"

NJS_OBJECT* ChaoEgg_BlackTwoTone = nullptr;
NJS_OBJECT* ChaoEgg_Normal = nullptr;
NJS_OBJECT* ChaoEgg_Gold = nullptr;
NJS_OBJECT* ChaoEgg_Silver = nullptr;
NJS_OBJECT* ChaoEgg_Broken_BlackTwoTone = nullptr; // The root object of this is egg top (hat), child is bottom
NJS_OBJECT* ChaoEgg_Broken_Normal = nullptr;
NJS_OBJECT* ChaoEgg_Broken_Gold = nullptr;
NJS_OBJECT* ChaoEgg_Broken_Silver = nullptr;

// Retrieves the SA1 version of a Chao Egg model, nullptr if not available
NJS_OBJECT* GetSA1EggModel(int color)
{
	switch (color)
	{
	case EggColor_Normal:
		return ChaoEgg_Normal;
	case EggColor_BlendBlack:
		return ChaoEgg_BlackTwoTone;
	case EggColor_Gold:
		return ChaoEgg_Gold;
	case EggColor_Silver:
		return ChaoEgg_Silver;
	default:
		return nullptr;
	}
}

// Retrieves the SA1 version of a Chao Broken Egg (full or bottom) model, nullptr if not available
NJS_OBJECT* GetSA1BrokenEggModel(int color, bool bottom)
{
	NJS_OBJECT* result;
	switch (color)
	{
	case EggColor_Normal:
		result = ChaoEgg_Broken_Normal;
		break;
	case EggColor_BlendBlack:
		result = ChaoEgg_Broken_BlackTwoTone;
		break;
	case EggColor_Gold:
		result = ChaoEgg_Broken_Gold;
		break;
	case EggColor_Silver:
		result = ChaoEgg_Broken_Silver;
		break;
	default:
		result = nullptr;
		break;
	}
	if (result != nullptr && bottom)
		return result->child;
	else
		return result;
}

// Hook to draw SA1 Chao eggs (object version)
void chRareEggDrawObject_r(NJS_CNK_OBJECT* model, int color)
{
	NJS_OBJECT* SA1Model = nullptr;
	// Full egg
	if (model == &object_alo_dummyegg_dummyegg)
		SA1Model = GetSA1EggModel(color);
	// Broken egg with both parts
	else if (model == &object_alm_egg_egg)
		SA1Model = GetSA1BrokenEggModel(color, false);
	// Broken egg bottom
	else if (model == &object_alm_egg_eggbottom)
		SA1Model = GetSA1BrokenEggModel(color, true);
	// If a relevant SA1 model is found, draw with it
	if (SA1Model != nullptr)
	{
		njScale(0, 0.9f, 0.9f, 0.9f);
		njSetTexture(&texlist_chao);
		late_DrawObjectClip(SA1Model, LATE_WZ, 1.0f);
		njSetTexture(&texlist_al_body);
	}
	// Otherwise draw regular SADX stuff
	else
	{
		NJS_CNK_MODEL* chunkmodel = model->chunkmodel;
		if (chunkmodel)
		{
			SetChunkParametersModel(chunkmodel, color);
			njCnkDrawObject(model);
			SetChunkMaterialFlags(0);
			DisableChunkMaterialFlags();
		}
		else
			njCnkDrawObject(model);
	}
}

// Hook to draw SA1 Chao eggs (motion version)
void chRareEggDrawMotion_r(NJS_CNK_OBJECT* model, int color, NJS_MOTION* motion, float frame)
{
	NJS_CNK_OBJECT* child;
	NJS_OBJECT* SA1Model = nullptr;
	// Full egg
	if (model == &object_alo_dummyegg_dummyegg)
		SA1Model = GetSA1EggModel(color);
	// Broken egg with both parts
	else if (model == &object_alm_egg_egg)
		SA1Model = GetSA1BrokenEggModel(color, false);
	// Broken egg bottom
	else if (model == &object_alm_egg_eggbottom)
		SA1Model = GetSA1BrokenEggModel(color, true);
	// If a relevant SA1 model is found, draw with it
	if (SA1Model != nullptr)
	{
		njScale(0, 0.9f, 0.9f, 0.9f);
		njSetTexture(&texlist_chao);
		late_DrawMotionClip(SA1Model, motion, frame, LATE_WZ, 1.0f);
		njSetTexture(&texlist_al_body);
	}
	// Otherwise draw regular SADX stuff
	else
	{
		NJS_CNK_MODEL* chunkmodel = model->chunkmodel;
		if (chunkmodel || (child = model->child) != 0 && (chunkmodel = (NJS_CNK_MODEL*)child->model) != 0) // What the fuck is this?
		{
			SetChunkParametersModel(chunkmodel, color);
			chCnkDrawMotion(model, motion, frame);
			SetChunkMaterialFlags(0);
			DisableChunkMaterialFlags();
		}
		else
		{
			chCnkDrawMotion(model, motion, frame);
		}
	}
}

// Render the Black Market preview with SA1 DC egg models
void ChaoEggHook_BlackMarket(NJS_CNK_MODEL* model, EggColor color)
{
	NJS_OBJECT* SA1Model = GetSA1EggModel(color);
	if (SA1Model != nullptr)
	{
		njSetTexture(&texlist_chao);
		dsDrawModel(SA1Model->basicdxmodel);
		njSetTexture(&texlist_al_body);
	}
	else
		chRareEggDrawModel(model, color);
}

// Repositions Chao eyes to fit SA1 egg hats
void RenderChaoHatEyes(NJS_CNK_OBJECT* a1)
{
	njTranslate(0, 0.0f, 0.0f, 0.5f);
	njCnkDrawObject(a1);
	njTranslate(0, 0.0f, 0.0f, -0.5f);
}

// Draws SA1 egg shells
void ChaoEggshellHatHook(NJS_CNK_MODEL* model, int color)
{
	NJS_OBJECT* SA1Model = GetSA1BrokenEggModel(color, false);
	if (SA1Model != nullptr)
	{
		njTranslate(0, 0.0f, -2.5f, 0.0f);
		njScale(0, 0.9f, 0.9f, 0.9f);
		njSetTexture(&texlist_chao);
		dsDrawModel(SA1Model->basicdxmodel);
		njSetTexture(&texlist_al_body);
	}
	else
		chRareEggDrawModel(model, color);
}

// Hook to not draw the egg shell hat if using SA1 models
void ChaoEggshellHatHook_Empty(NJS_CNK_MODEL* model, int color)
{
	switch (color)
	{
	case EggColor_BlendBlack:
	case EggColor_Normal:
	case EggColor_Gold:
	case EggColor_Silver:
		break;
	default:
		chRareEggDrawModel(model, color);
		break;
	}
}

void AL_Egg_Load()
{
	ChaoEgg_BlackTwoTone = LoadModel("system\\data\\a_life\\al_model\\alm_egg_black.nja.sa1mdl");
	ChaoEgg_Normal = LoadModel("system\\data\\a_life\\al_model\\alm_egg.nja.sa1mdl");
	ChaoEgg_Gold = LoadModel("system\\data\\a_life\\al_model\\alm_egg_gold.nja.sa1mdl");
	ChaoEgg_Silver = LoadModel("system\\data\\a_life\\al_model\\alm_egg_silver.nja.sa1mdl");
	ChaoEgg_Broken_BlackTwoTone = LoadModel("system\\data\\a_life\\al_model\\alm_egg_black_broken.nja.sa1mdl");
	ChaoEgg_Broken_Normal = LoadModel("system\\data\\a_life\\al_model\\alm_egg_bottom.nja.sa1mdl"); // Added child object (egg shell bottom) like in other 3 models
	ChaoEgg_Broken_Gold = LoadModel("system\\data\\a_life\\al_model\\alm_egg_gold_broken.nja.sa1mdl");
	ChaoEgg_Broken_Silver = LoadModel("system\\data\\a_life\\al_model\\alm_egg_silver_broken.nja.sa1mdl");
	WriteData<1>((char*)0x007151D3, (char)EggColor_BlendBlack); // The secret EC egg is a two-tone black egg
	WriteJump(chRareEggDrawObject, chRareEggDrawObject_r); // Egg object
	WriteJump(chRareEggDrawMotion, chRareEggDrawMotion_r); // Egg motion
	WriteCall((void*)0x0073EA48, RenderChaoHatEyes); // Offset eyes in the Chao hat
	WriteCall((void*)0x007264C3, ChaoEggshellHatHook_Empty); // Avoid rendering stuff twice in Black Market
	WriteCall((void*)0x007264D7, ChaoEggshellHatHook); // Black Market preview
	WriteCall((void*)0x0073EA2F, ChaoEggshellHatHook); // In DrawChao
	WriteCall((void*)0x007235D9, ChaoEggshellHatHook); // In the garden
	WriteCall((void*)0x007277C9, ChaoEggHook_BlackMarket); // Item list
	WriteCall((void*)0x00725EAE, ChaoEggHook_BlackMarket); // Preview
}