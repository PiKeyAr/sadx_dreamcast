#include "stdafx.h"
#include "STG02_WindyValley.h"

static bool ModelsLoaded_STG02;

static NJS_VECTOR TornadoSpawnPosition = { 3254, -421, -1665 };
static float Windy1SkyAlphaOffset = 0.0f; // DX adds 1.0 to constant material's alpha but in SA1DC it's offset material.

TornadoFogMode TornadoFogVar; // Variable to control fog state as Sonic gets closer to the tornado.

void RenderWindy1SkyPart1(NJS_OBJECT *obj)
{
	late_z_ofs___ = -30000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f); // Main
}

void RenderWindy1SkyPart2(NJS_OBJECT* obj)
{
	late_z_ofs___ = -28000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f); // Bottom non-trans
}

void RenderWindy1SkyPart3(NJS_OBJECT* obj)
{
	late_z_ofs___ = -25000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f); // Bottom trans
}

void RenderWindy1SkyPart4(NJS_OBJECT* obj)
{
	late_z_ofs___ = -20000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f); // Cloud 1
}

void RenderWindy1SkyPart5(NJS_OBJECT* obj)
{
	late_z_ofs___ = -18000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f); // Cloud 2
	late_z_ofs___ = 0;
}

void OHasieFix(NJS_MODEL_SADX *model, float scale)
{
	DrawModelMesh(model, LATE_MAT);
}

void DrawTransparentBrokenBlocks(NJS_MODEL_SADX *model, LATE blend)
{
	late_z_ofs___ = 5000.0f;
	late_DrawModel(model, blend);
	late_z_ofs___ = 0.0f;
}

void DrawTransparentBrokenBlocksExplosion(NJS_VECTOR *a1, float a2)
{
	sp_zoffset = 22048.0f;
	CreateBomb(a1, a2);
	sp_zoffset = 0.0f;
}

static Trampoline* ObjectWatageDisplay_t = nullptr;
static void __cdecl ObjectWatageDisplay_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectWatageDisplay);
	taskwk* v1; // r31
	taskwk* v2; // r30
	v1 = tp->twp;
	v2 = tp->ptp->twp;
	float dif_x = abs(v1->pos.x - v2->pos.x);
	float dif_y = abs(v1->pos.x - v2->pos.x);
	float dif_z = abs(v1->pos.x - v2->pos.x);
	if (dif_x > 0.5f || dif_y > 0.5f || dif_z > 0.5f)
	{
		original(tp);
	}
}

void OTuriBr2_Particle(NJS_VECTOR *a1, NJS_VECTOR *a2, float a3)
{
	sp_zoffset = 20000.0f;
	CreateSmoke(a1, a2, a3);
	sp_zoffset = 0.0f;
}

void OHaneAFix(NJS_MODEL_SADX *model, LATE blend, float scale)
{
	late_z_ofs___ = -5000.0f;
	late_DrawModelClipEx(model, blend, scale);
	late_z_ofs___ = 0.0f;
}

void Windy1SkyNjControl3D(NJD_CONTROL_3D a1)
{
	OnControl3D(NJD_CONTROL_3D_OFFSET_MATERIAL);
}

void WindyValley_Init()
{
	ReplaceSET("SET0200S");
	ReplaceSET("SET0200E");
	ReplaceSET("SET0201S");
	ReplaceSET("SET0202M");
	ReplaceSET("SET0202S");
	ReplaceCAM("CAM0200E");
	ReplaceCAM("CAM0200S");
	ReplaceCAM("CAM0201S");
	ReplaceCAM("CAM0202M");
	ReplaceCAM("CAM0202S");
	ReplacePVM("OBJ_WINDY");
	ReplacePVM("WINDY01");
	ReplacePVM("WINDY02");
	ReplacePVM("WINDY03");
	ReplacePVM("WINDY_BACK");
	ReplacePVM("WINDY_BACK2");
	ReplacePVM("WINDY_BACK3");
	ReplacePVM("WINDY_E103");
	ReplacePVR("SORA60");
	ReplacePVR("SW_NBG2");
	ReplacePVR("S_WT28");
	ReplacePVR("S_WT32");
	ReplacePVR("SEA");
	ReplacePVR("WINDSEA001");
	ReplacePVR("WINDY2_NBG1");
	ReplacePVR("WINDY2_NBG2");
	ReplacePVR("WINDY2_NBG5");
	ReplacePVR("WINDY3_NBG2");
	// Skybox/fog data stuff
	for (int i = 0; i < 3; i++)
	{
		pScale_Stg02[i]->x = pScale_Stg02[i]->y = pScale_Stg02[i]->z = 1.0f; // All 3 acts
		pClipMap_Stg02[0][i].f32Far = -8000.0f;
		pFogTable_Stg02[0][i].f32EndZ = 12000.0f;
		pFogTable_Stg02[0][i].f32StartZ = 1000.0f;
		pFogTable_Stg02[0][i].Col = 0xFFFFFFFF;
		pFogTable_Stg02[1][i].f32EndZ = 2500.0f;
		pFogTable_Stg02[1][i].f32StartZ = 50.0f;
		pFogTable_Stg02[1][i].Col = 0xFFFFFFFF;
		pFogTable_Stg02[1][i].u8Enable = 1;
		pFogTable_Stg02[2][i].f32EndZ = 6000.0f;
		pFogTable_Stg02[2][i].f32StartZ = 200.0f;
		pFogTable_Stg02[2][i].Col = 0xFFFFFFFF;
	}
}

void WindyValley_Load()
{
	LevelLoader(LevelAndActIDs_WindyValley1, "SYSTEM\\data\\stg02_windy\\landtable0200.c.sa1lvl", &texlist_windy01);
	LevelLoader(LevelAndActIDs_WindyValley2, "SYSTEM\\data\\stg02_windy\\landtable0201.c.sa1lvl", &texlist_windy02);
	LevelLoader(LevelAndActIDs_WindyValley3, "SYSTEM\\data\\stg02_windy\\landtable0202.c.sa1lvl", &texlist_windy03);
	AddLateDrawLandtable(objLandTable[2][2]);
	if (!ModelsLoaded_STG02)
	{
		// Skybox stuff
		WriteData((float**)0x004DD77A, &Windy1SkyAlphaOffset);
		WriteCall((void*)0x004DD770, Windy1SkyNjControl3D);
		WriteCall((void*)0x004DD7D1, RenderWindy1SkyPart1);
		WriteCall((void*)0x004DD7DB, RenderWindy1SkyPart2);
		WriteCall((void*)0x004DD7E5, RenderWindy1SkyPart3);
		WriteCall((void*)0x004DD7EF, RenderWindy1SkyPart4);
		WriteCall((void*)0x004DD7F9, RenderWindy1SkyPart5);
		// Object fixes
		WriteCall((void*)0x004E1E35, late_DrawModelClipMesh); // Wind gate rendering function
		WriteCall((void*)0x004E1E9A, late_DrawModelClipMesh); // Wind gate rendering function
		WriteCall((void*)0x004E1F08, late_DrawModelClipMesh); // Wind gate rendering function
		WriteCall((void*)0x004E1F77, late_DrawModelClipMesh); // Wind gate rendering function
		WriteCall((void*)0x004E126F, OHasieFix);
		WriteCall((void*)0x004E12D2, OHasieFix);
		WriteCall((void*)0x004E133E, OHasieFix);
		WriteCall((void*)0x004E108A, OHaneAFix);
		WriteCall((void*)0x004E11C1, OHaneAFix);
		WriteCall((void*)0x004E282D, DrawTransparentBrokenBlocksExplosion);
		WriteCall((void*)0x004E2703, DrawTransparentBrokenBlocksExplosion);
		WriteCall((void*)0x004E2262, DrawTransparentBrokenBlocks);
		WriteCall((void*)0x007A525C, OTuriBr2_Particle);
		WriteCall((void*)0x004E09FB, OTuriBr2_Particle);
		ObjectWatageDisplay_t = new Trampoline(0x004DFA60, 0x004DFA65, ObjectWatageDisplay_r); // Fix hanging dandelion seed
		model_windobj_hasib_hasib = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobsj_hasib.nja.sa1mdl")->basicdxmodel; // Bridge piece
		model_windobj_tatehimo_tatehimo = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_tatehimo.nja.sa1mdl")->basicdxmodel; // Fixed bridge rope
		NJS_OBJECT* PopoModel = LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_popo.nja.sa1mdl"); // OPopo
		model_wvobj_popo_popo = *PopoModel->basicdxmodel; // OPopo base
		object_wvobj_popo_hana2 = *PopoModel->child; // OPopo part 2
		object_wvobj_popo_hana1 = *PopoModel->child->sibling; // OPopo part 1 (I swapped these because SADX renders them in an incorrect order)
		AddWhiteDiffuseMaterial(&object_wvobj_popo_hana2.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_wvobj_popo_hana1.basicdxmodel->mats[1]);
		NJS_OBJECT* TanpopoModel = LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_tampopo.nja.sa1mdl"); 
		model_wvobj_tampopo_tampopo = *TanpopoModel->basicdxmodel; // OTanpopo base
		object_wvobj_tampopo_wata = *TanpopoModel->child; // OTanpopo fuzz
		object_wvobj_watage_watage = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_watage.nja.sa1mdl"); // OTanpopo seed
		object_wvobj_tree_tree = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_tree.nja.sa1mdl"); // OTreeM
		object_wvobj_busshu_busshu = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_busshu.nja.sa1mdl"); // Grassy rock
		object_wvobj_tateiwa_l_tateiwa_l = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_tateiwa_l.nja.sa1mdl"); // OTatel (rock fencing)
		object_wvobj_cube_s_cube_s = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_cube_s.nja.sa1mdl"); // OCubeS
		object_wvobj_cube_m_cube_m = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_cube_m.nja.sa1mdl"); // OCubeM
		object_wvobj_cube_l_cube_l = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_cube_l.nja.sa1mdl"); // OCubeL
		object_wvobj_flower_a_flower_a = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_flower_a.nja.sa1mdl"); // OGrFlowerA / OHanaA
		object_wvobj_flower_b_flower_b = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_flower_b.nja.sa1mdl"); // OGrFlowerB / OHanaB
		object_wvobj_tateiwa_s_tateiwa_s = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_tateiwa_s.nja.sa1mdl"); // OTateS
		object_windobj_wasi_wasi = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_wasi.nja.sa1mdl"); // OWasi
		object_windobj_haneobj_c_haneobj_c = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_haneobj_c.nja.sa1mdl"); // OBroobj (breakable fan)
		object_windobj_hane_cb1_hane_cb1 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb1.nja.sa1mdl"); // OBroobj broken 1
		object_windobj_hane_cb2_hane_cb2 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb2.nja.sa1mdl"); // OBroobj broken 2
		object_windobj_hane_cb3_hane_cb3 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb3.nja.sa1mdl"); // OBroobj broken 3
		object_windobj_hane_cb4_hane_cb4 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb4.nja.sa1mdl"); // OBroobj broken 4
		object_windobj_hane_cb5_hane_cb5 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb5.nja.sa1mdl"); // OBroobj broken 5
		object_windobj_hane_cb6_hane_cb6 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb6.nja.sa1mdl"); // OBroobj broken 6
		object_windobj_hane_cb7_hane_cb7 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb7.nja.sa1mdl"); // OBroobj broken 7
		object_windobj_hane_cb8_hane_cb8 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb8.nja.sa1mdl"); // OBroobj broken 8
		object_windobj_hane_cb9_hane_cb9 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hane_cb9.nja.sa1mdl"); // OBroobj broken 9
		object_windobj_hasib_break_hasib_break = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hasib_break.nja.sa1mdl"); // Yure
		object_windobj_haneobj_a_haneobj_a = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_haneobj_a.nja.sa1mdl"); // HaneA
		object_windobj_isibasi_isibasi = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_isibasi.nja.sa1mdl"); // OStBrid
		object_windobj_bridge_a4_bridge_a4 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_a4.nja.sa1mdl"); // Bridge D
		object_windobj_bridge_a3_bridge_a3 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_a3.nja.sa1mdl"); // Bridge C
		object_windobj_bridge_a2_bridge_a2 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_a2.nja.sa1mdl"); // Bridge B
		object_windobj_bridge_a1_bridge_a1 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_a1.nja.sa1mdl"); // Bridge A
		object_windobj_saku_c_saku_c = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_saku_c.nja.sa1mdl"); // OSaku C
		object_windobj_saku_b_saku_b = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_saku_b.nja.sa1mdl"); // OSaku B
		object_windobj_saku_a_saku_a = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_saku_a.nja.sa1mdl"); // OSaku A
		object_kazeroop_kazegate_kazegate = *LoadModel("system\\data\\stg02_windy\\common\\models\\kazeroop_kazegate.nja.sa1mdl"); // Wind gate 1
		object_kazeroop_kazegate_gatehane_a = *object_kazeroop_kazegate_kazegate.child; // Wind gate 2
		object_kazeroop_kazegate_gatehane_b = *object_kazeroop_kazegate_kazegate.child->sibling; // Wind gate 3
		object_kazeroop_kazegate_gatehane_c = *object_kazeroop_kazegate_kazegate.child->sibling->sibling; // Wind gate 4
		object_windobj_haneobj_b_haneobj_b = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_haneobj_b.nja.sa1mdl"); // OHaneA
		object_wvobj_ukisima_ukisima = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_ukisima.nja.sa1mdl"); // OUkisim
		object_windobj_hasib_end_hasib_end = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_hasib_end.nja.sa1mdl");
		object_windobj_hasib_end_hasi_hane1 = *object_windobj_hasib_end_hasib_end.child; // OHasiE 2
		object_windobj_hasib_end_hasi_hane2 = *object_windobj_hasib_end_hasib_end.child->sibling; // OHasiE 3
		LoadModel_ReplaceMeshes(&object_wvobj_ha_green_ha_green, "system\\data\\stg02_windy\\common\\models\\wvobj_ha_green.nja.sa1mdl"); // PuWind 1
		LoadModel_ReplaceMeshes(&object_wvobj_ha_yellow_ha_yellow, "system\\data\\stg02_windy\\common\\models\\wvobj_ha_yellow.nja.sa1mdl"); // PuWind 2
		LoadModel_ReplaceMeshes(&object_wvobj_ha_red_ha_red, "system\\data\\stg02_windy\\common\\models\\wvobj_ha_red.nja.sa1mdl"); // PuWind 3
		model_wvobj_ha_green_ha_green = *object_wvobj_ha_green_ha_green.basicdxmodel; // Tornado leaves 1
		model_wvobj_ha_red_ha_red = *object_wvobj_ha_red_ha_red.basicdxmodel; // Tornado leaves 2
		model_wvobj_ha_yellow_ha_yellow = *object_wvobj_ha_yellow_ha_yellow.basicdxmodel; // Tornado leaves 3
		object_windobj_tramp_tramp = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_tramp.nja.sa1mdl"); // OPoline 1
		object_windobj_tramp_ami_a = *object_windobj_tramp_tramp.child; // OPoline 3
		object_windobj_tramp_ami_b = *object_windobj_tramp_tramp.child->sibling; // OPoline 4
		object_wvobj_wara_wara = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_wara.nja.sa1mdl"); // Tumbleweed (unused normally)
		object_windobj_bigfloot_bigfloot = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bigfloot.nja.sa1mdl"); // OBigfla
		model_wvobj_iwaita_iwaita = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_iwaita.nja.sa1mdl")->basicdxmodel; // OVcRock 1
		model_wvobj_brock_moto_brock_moto = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_moto.nja.sa1mdl")->basicdxmodel; // OVcRock 2
		object_wvobj_brock_a_brock_a = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_a.nja.sa1mdl"); // OVcRock 3
		object_wvobj_brock_b_brock_b = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_b.nja.sa1mdl"); // OVcRock 4
		object_wvobj_brock_c_brock_c = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_c.nja.sa1mdl"); // OVcRock 5
		object_wvobj_brock_d_brock_d = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_d.nja.sa1mdl"); // OVcRock 6
		object_wvobj_brock_e_brock_e = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_brock_e.nja.sa1mdl"); // OVcRock 7
		model_windobj_yokohimo_yokohimo = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_yokohimo.nja.sa1mdl")->basicdxmodel; // OTuriBr2 top rope
		model_windobj_haneobj_c_haneobj_c = *object_windobj_haneobj_c_haneobj_c.basicdxmodel; // OSetiff 1
		model_windobj_hane_cb1_hane_cb1 = *object_windobj_hane_cb1_hane_cb1.basicdxmodel; // OSetiff 2
		model_windobj_hane_cb2_hane_cb2 = *object_windobj_hane_cb2_hane_cb2.basicdxmodel; // OSetiff 3 
		model_windobj_hane_cb3_hane_cb3 = *object_windobj_hane_cb3_hane_cb3.basicdxmodel; // OSetiff 4
		model_windobj_hane_cb4_hane_cb4 = *object_windobj_hane_cb4_hane_cb4.basicdxmodel; // OSetiff 5
		model_windobj_hane_cb5_hane_cb5 = *object_windobj_hane_cb5_hane_cb5.basicdxmodel; // OSetiff 6
		model_windobj_hane_cb6_hane_cb6 = *object_windobj_hane_cb6_hane_cb6.basicdxmodel; // OSetiff 7
		model_windobj_hane_cb7_hane_cb7 = *object_windobj_hane_cb7_hane_cb7.basicdxmodel; // OSetiff 8
		model_windobj_hane_cb8_hane_cb8 = *object_windobj_hane_cb8_hane_cb8.basicdxmodel; // OSetiff 9
		model_windobj_bridge_ab07_bridge_ab07 = *object_windobj_bridge_ab07_bridge_ab07.basicdxmodel; // OSetiff 10
		object_windobj_bridge_ab20_bridge_ab20 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab20.nja.sa1mdl"); // Broken bridge pieces 1
		object_windobj_bridge_ab21_bridge_ab21 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab21.nja.sa1mdl"); // Broken bridge pieces 2
		object_windobj_bridge_ab22_bridge_ab22 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab22.nja.sa1mdl"); // Broken bridge pieces 3
		object_windobj_bridge_ab23_bridge_ab23 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab23.nja.sa1mdl"); // Broken bridge pieces 4
		object_windobj_bridge_ab24_bridge_ab24 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab24.nja.sa1mdl"); // Broken bridge pieces 5
		object_windobj_bridge_ab25_bridge_ab25 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab25.nja.sa1mdl"); // Broken bridge pieces 6
		object_windobj_bridge_ab26_bridge_ab26 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab26.nja.sa1mdl"); // Broken bridge pieces 7
		object_windobj_bridge_ab27_bridge_ab27 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab27.nja.sa1mdl"); // Broken bridge pieces 8
		object_windobj_bridge_ab28_bridge_ab28 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab28.nja.sa1mdl"); // Broken bridge pieces 9
		object_windobj_bridge_ab29_bridge_ab29 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab29.nja.sa1mdl"); // Broken bridge pieces 10
		object_windobj_bridge_ab30_bridge_ab30 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab30.nja.sa1mdl"); // Broken bridge pieces 11
		object_windobj_bridge_ab31_bridge_ab31 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab31.nja.sa1mdl"); // Broken bridge pieces 12
		object_windobj_bridge_ab32_bridge_ab32 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab32.nja.sa1mdl"); // Broken bridge pieces 13
		object_windobj_bridge_ab33_bridge_ab33 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab33.nja.sa1mdl"); // Broken bridge pieces 14
		object_windobj_bridge_ab01_bridge_ab01 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab01.nja.sa1mdl"); // Broken bridge pieces 15
		object_windobj_bridge_ab02_bridge_ab02 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab02.nja.sa1mdl"); // Broken bridge pieces 16
		object_windobj_bridge_ab03_bridge_ab03 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab03.nja.sa1mdl"); // Broken bridge pieces 17
		object_windobj_bridge_ab04_bridge_ab04 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab04.nja.sa1mdl"); // Broken bridge pieces 18
		object_windobj_bridge_ab05_bridge_ab05 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab05.nja.sa1mdl"); // Broken bridge pieces 19
		object_windobj_bridge_ab06_bridge_ab06 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab06.nja.sa1mdl"); // Broken bridge pieces 20
		object_windobj_bridge_ab07_bridge_ab07 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab07.nja.sa1mdl"); // Broken bridge pieces 21
		object_windobj_bridge_ab08_bridge_ab08 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab08.nja.sa1mdl"); // Broken bridge pieces 22
		object_windobj_bridge_ab09_bridge_ab09 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab09.nja.sa1mdl"); // Broken bridge pieces 23
		object_windobj_bridge_ab10_bridge_ab10 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab10.nja.sa1mdl"); // Broken bridge pieces 24
		object_windobj_bridge_ab11_bridge_ab11 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab11.nja.sa1mdl"); // Broken bridge pieces 25
		object_windobj_bridge_ab12_bridge_ab12 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab12.nja.sa1mdl"); // Broken bridge pieces 26
		object_windobj_bridge_ab13_bridge_ab13 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab13.nja.sa1mdl"); // Broken bridge pieces 27
		object_windobj_bridge_ab14_bridge_ab14 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab14.nja.sa1mdl"); // Broken bridge pieces 28
		object_windobj_bridge_ab15_bridge_ab15 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab15.nja.sa1mdl"); // Broken bridge pieces 29
		object_windobj_bridge_ab16_bridge_ab16 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab16.nja.sa1mdl"); // Broken bridge pieces 30
		object_windobj_bridge_ab17_bridge_ab17 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab17.nja.sa1mdl"); // Broken bridge pieces 31
		object_windobj_bridge_ab18_bridge_ab18 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab18.nja.sa1mdl"); // Broken bridge pieces 32
		object_windobj_bridge_ab19_bridge_ab19 = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bridge_ab19.nja.sa1mdl"); // Broken bridge pieces 33
		object_windobj_bigflootb_bigflootb = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_bigflootb.nja.sa1mdl"); // OBigflo
		object_wvobj_sekichu_a_sekichu_a = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_sekichu_a.nja.sa1mdl"); // OIshiA
		object_wvobj_sekichu_b_sekichu_b = *LoadModel("system\\data\\stg02_windy\\common\\models\\wvobj_sekichu_b.nja.sa1mdl"); // OIshiB
		object_windobj_kazami_kazami = *LoadModel("system\\data\\stg02_windy\\common\\models\\windobj_kazami.nja.sa1mdl"); // OKazami 1
		object_windobj_kazami_kubi = *object_windobj_kazami_kazami.child; // OKazami 2
		object_windobj_kazami_hane = *object_windobj_kazami_kazami.child->child; // OKazami 3
		ModelsLoaded_STG02 = true;
	}
};

void WindyValley_OnFrame()
{
	// WV Act 1 only
	if (ssStageNumber != LevelIDs_WindyValley || ssActNumber)
		return;
	// Tornado fog stuff
	switch (ssGameMode)
	{
	case MD_GAME_REINIT2:
	case MD_GAME_FADEIN:
	case MD_GAME_FADEOUT_MISS_RESTART:
	case MD_GAME_STAGENAME_INIT:
		TornadoFogVar = TornadoFogMode::NoAction;
		break;
	}
	auto entity = playertwp[0];
	if (GetPlayerNumber() != Characters_Gamma && entity != nullptr)
	{
		float TornadoDistance = squareroot(
			(entity->pos.x - TornadoSpawnPosition.x) * (entity->pos.x - TornadoSpawnPosition.x) + 
			(entity->pos.y - TornadoSpawnPosition.y) * (entity->pos.y - TornadoSpawnPosition.y) + 
			(entity->pos.z - TornadoSpawnPosition.z) * (entity->pos.z - TornadoSpawnPosition.z));
		if (TornadoFogVar != TornadoFogMode::Shrink)
		{
			if (TornadoDistance < 680)
				TornadoFogVar = TornadoFogMode::ExpandLow;
			if (TornadoDistance < 310)
				TornadoFogVar = TornadoFogMode::ExpandMid;
			if (TornadoDistance < 170)
				TornadoFogVar = TornadoFogMode::Maintain;
			if (TornadoDistance < 150)
				TornadoFogVar = TornadoFogMode::Shrink;
		}
		switch (TornadoFogVar)
		{
		case TornadoFogMode::NoAction:
		case TornadoFogMode::Shrink:
			if (gFog.f32EndZ < 2200)
				gFog.f32EndZ += 32.0f;
			if (gFog.f32StartZ < 400)
				gFog.f32StartZ += 16.0f;
			break;
		case TornadoFogMode::ExpandLow:
			if (gFog.f32EndZ > 5000)
				gFog.f32EndZ -= 128.0f;
			if (gFog.f32StartZ >= 100)
				gFog.f32StartZ -= 128.0f;
			break;
		case TornadoFogMode::ExpandMid:
			if (gFog.f32EndZ > 450)
				gFog.f32EndZ -= 64.0f;
			if (gFog.f32StartZ >= 64)
				gFog.f32StartZ -= 64.0f;
			break;
		}
	}
	else
		TornadoFogVar = TornadoFogMode::NoAction;
}