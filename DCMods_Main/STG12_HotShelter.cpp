#include "stdafx.h"
#include "STG12_HotShelter.h"

static bool ModelsLoaded_STG12;

static bool ReduceHotShelterFog = false;

// Model pointers
NJS_OBJECT* BrokenGlass = nullptr;

// Depth hacks for OLight1
void OLight1_Display(task *a1)
{
	taskwk* v1; // esi
	Angle v2; // eax
	v1 = a1->twp;
	bool IsUnderwater = true;
	// Underwater check lol
	if (v1->pos.x > 1050 || (current_event == -1 && (((v1->pos.x > 700 && v1->pos.x < 790) && (v1->pos.y > 1 && v1->pos.y < 3) && (v1->pos.z > -690 && v1->pos.z < -610))))) 
		IsUnderwater = false;
	if (ssActNumber) 
		IsUnderwater = false;
	if (!loop_count)
	{
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		v2 = v1->ang.y;
		if (v2)
		{
			njRotateY(0, v2);
		}
		// Act 1 stuff
		if (!ssActNumber)
		{
			// Draw opaque model first
			dsDrawModel(object_light1h_dai_dai.basicdxmodel);
			SaveControl3D();
			OnControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
			if (IsUnderwater)
			{
				if (!IsCameraUnderwater)
				{
					SetMaterial(1.0f, 0.4f, 0.53f, 0.5f);
					late_z_ofs___ = 0.0f;
				}
				else
				{
					late_z_ofs___ = 0.0f;
					SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
				}
			}
			else
			{
				late_z_ofs___ = 0.0f;
				SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
			}
			if (v1->pos.x > 1050)
				late_z_ofs___ = -48000.0f;
			late_DrawObjectClipMesh(object_light1h_dai_dai.child, LATE_LIG, 1.0f);
			njPopMatrix(1u);
			ResetMaterial();
			LoadControl3D();
			late_z_ofs___ = 0.0f;
		}
		// Acts 2 and 3
		else
		{
			DrawModel(object_light1h_dai_dai.basicdxmodel);
			late_z_ofs___ = -20000.0f;
			late_DrawObjectClip(object_light1h_dai_dai.child, LATE_MAT, 1.0f);
			njPopMatrix(1u);
			late_z_ofs___ = 0.0f;
		}
	}
}

// Depth hacks for for OLight3
void OLight3_Display(NJS_OBJECT* a1, LATE a2, float a3)
{
	ds_DrawModelClip(a1->basicdxmodel, a3);
	ds_DrawModelClip(a1->child->basicdxmodel, a3);
	late_DrawModelClipEx(a1->child->child->basicdxmodel, a2, a3);
	late_DrawModelClip(a1->child->child->sibling->basicdxmodel, LATE_MAT, a3);
	late_DrawModelClip(a1->child->child->sibling->sibling->basicdxmodel, LATE_MAT, a3);
}

// Depth hacks for OKowareSuisou
void OKowareSuisou_Display(task* a1)
{
	taskwk* twp; // esi
	Angle v2; // eax
	Angle v3; // eax
	float a4a; // [esp+8h] [ebp+4h]

	twp = a1->twp;
	if (!loop_count)
	{
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);

		njTranslate(0, 0.0f, twp->value.f, 0.0f);
		v3 = twp->ang.x;
		if (v3)
		{
			njRotateY(0, v3);
		}
		njScale(0, twp->scl.y, 1.0f, twp->scl.y);
		if (twp->scl.y <= 1.0f)
		{
			a4a = 1.0f;
		}
		else
		{
			a4a = twp->scl.y;
		}
		late_z_ofs___ = 2900.0f;
		// Thing in the middle
		late_ActionMesh(&action_kowaresuisou_hikari_sin, twp->counter.f, LATE_MAT);
		njPopMatrix(1u);

		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		v2 = twp->ang.y;
		if (v2)
		{
			njRotateY(0, v2);
		}
		// Top
		dsDrawObject(&object_kowaresuisou_dai_dai);
		late_z_ofs___ = 3000.0f;
		if (twp->mode)
		{
			// Broken glass
			DrawObjectClipMesh(&object_kowaresuisou_br_br, LATE_MAT, 1.0f);
		}
		else
		{
			// Glass
			DrawObjectClipMesh(&object_kowaresuisou_grass_grass, LATE_LIG, 1.0f);
		}
		njPopMatrix(1u);
	}
}

// Fix material not being reset after drawing the sprite for OKowareSuisou
void njDrawSprite3D_TheyForgotToClampAgain(NJS_SPRITE* _sp, Int n, NJD_SPRITE attr)
{
	late_DrawSprite3D(_sp, n, attr, LATE_MAT);
	ResetMaterial();
}

// Depth hack for for OEntotsu
void OEntotsuParticleFix(NJS_VECTOR* a1, NJS_VECTOR* a2, float a3)
{
	if (ModConfig::EnabledLevels[LevelIDs_HotShelter] && ssStageNumber == LevelIDs_HotShelter)
	{
		sp_zoffset = -2000.0f;
		CreateSmoke(a1, a2, a3);
		sp_zoffset = 0.0f;
	}
}

// Disable collision on hatch in Amy's Act 2 if DC SET files are used
void AmyHatchFix(task *obj, CCL_INFO *collisionArray, int count, unsigned __int8 list)
{
	if (GetPlayerNumber() != Characters_Amy)
		CCL_Init(obj, collisionArray, count, list);
}

// Fix OSuimen flickering
void RenderSuimen(NJS_OBJECT *a1, LATE a2, float a3)
{
	SetMaterial(0.0f, 0.5f, 0.5f, 0.5f);
	//late_z_ofs___ = 000.0f;
	late_DrawObjectClip(a1, a2, a3);
	//late_z_ofs___ = 0.0f;
	ResetMaterial();
}

// Restore light in OEfHikari
void RenderOHikari(NJS_OBJECT *a1, LATE a2, float a3)
{
	ds_DrawModelClip(a1->basicdxmodel, a3);
	late_z_ofs___ = 1000.0f;
	late_DrawModelClip(a1->child->basicdxmodel, LATE_LIG, 1.0f);
	late_DrawModelClip(a1->child->sibling->basicdxmodel, LATE_LIG, 1.0f);
	late_z_ofs___ = 0.0f;
}

// Aquariums in drainage room
void RenderFourWaterThings(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_z_ofs___ = -4000.0f;
	late_DrawObjectClip(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Broken RoboTV part 1
void RenderRoboTVBrokenGlass(NJS_MODEL_SADX* model, LATE blend, float scale)
{
	late_DrawModel(BrokenGlass->basicdxmodel, LATE_MAT);
	late_DrawModel(model, LATE_MAT);
}

// Broken RoboTV part 2
void RenderRoboTVGlass2(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawModel(BrokenGlass->basicdxmodel, LATE_MAT);
	late_DrawObjectClip(a1, a2, a3);
}

// Hook to set custom fog in E105's room
void PlayMusicHook_ReduceE105Fog(MusicIDs song)
{
	BGM_Play(song);
	ReduceHotShelterFog = true;
}

void HotShelter_Init()
{
	ReplaceSET("SET1200A");
	ReplaceSET("SET1200B");
	ReplaceSET("SET1200S");
	ReplaceSET("SET1201A");
	ReplaceSET("SET1201S");
	ReplaceSET("SET1202E");
	ReplaceSET("SET1202S");
	ReplaceSET("SET1203S");
	ReplaceCAM("CAM1200A");
	ReplaceCAM("CAM1200B");
	ReplaceCAM("CAM1200S");
	ReplaceCAM("CAM1201A");
	ReplaceCAM("CAM1201S");
	ReplaceCAM("CAM1202E");
	ReplaceCAM("CAM1202S");
	ReplaceCAM("CAM1203S");
	ReplacePVM("HOTSHELTER0");
	ReplacePVM("HOTSHELTER1");
	ReplacePVM("HOTSHELTER2");
	ReplacePVM("HOTSHELTER3");
	ReplacePVM("HOTSHELTER4");
	ReplacePVM("SHELTER_COLUMN");
	ReplacePVM("SHELTER_SUIMEN");
	ReplacePVM("HOT_E105");
	ReplacePVM("ROBOTV");
	ReplacePVM("LIGHTNING");
	ReplacePVR("TM32KURAGE");
	// Fog/draw distance data
	for (int i = 0; i < 3; i++)
	{
		pClipMap_Stg12[0][i].f32Far = -3000.0;
		pFogTable_Stg12[0][i].Col = 0xFF000000;
		pFogTable_Stg12[0][i].f32StartZ = 600.0f;
		pFogTable_Stg12[0][i].f32EndZ = 1500.0f;

		pClipMap_Stg12[1][i].f32Far = -3000.0;
		pFogTable_Stg12[1][i].Col = 0xFF000000;
		pFogTable_Stg12[1][i].u8Enable = 1;
		pFogTable_Stg12[1][i].f32StartZ = 800.0f;
		pFogTable_Stg12[1][i].f32EndZ = 2000.0f;

		pFogTable_Stg12[2][i].Col = 0xFF000000;
		pFogTable_Stg12[2][i].u8Enable = 1;
		pFogTable_Stg12[2][i].f32StartZ = 500.0f;
		pFogTable_Stg12[2][i].f32EndZ = 1800.0f;
	}
}

void HotShelter_Load()
{
	LevelLoader(LevelAndActIDs_HotShelter1, "system\\data\\stg12_hotshelter\\landtable1201.c.sa1lvl", &texlist_HotShelter1);
	LevelLoader(LevelAndActIDs_HotShelter2, "system\\data\\stg12_hotshelter\\landtable1202.c.sa1lvl", &texlist_HotShelter2);
	LevelLoader(LevelAndActIDs_HotShelter3, "system\\data\\stg12_hotshelter\\landtable1203.c.sa1lvl", &texlist_HotShelter3);
	if (!ModelsLoaded_STG12)
	{
		// Code fixes
		WriteData((float*)0x005AB2F0, -1000.0f); // Four glass things in drainage room (depth bias)
		WriteCall((void*)0x005A93BF, njDrawSprite3D_TheyForgotToClampAgain); // OKowareSuisou related
		WriteJump((void*)0x005A91E0, OKowareSuisou_Display);
		WriteJump((void*)0x005A30F0, OLight1_Display);
		WriteCall((void*)0x005A3A03, PlayMusicHook_ReduceE105Fog); // Hook to disable fog in E105 room
		WriteCall((void*)0x0059F75C, AmyHatchFix); // Don't make the ventilation hatch solid when playing as Amy
		WriteCall((void*)0x005ADAFE, RenderFourWaterThings); // Four aquariums in drainage room
		// Queue flag for waterfalls
		WriteData<1>((char*)0x005AD472, 0i8);
		WriteData<1>((char*)0x005AD4B9, 0i8);
		WriteData<1>((char*)0x005AD500, 0i8);
		WriteData<1>((char*)0x005AD547, 0i8);
		// Materials
		AddWhiteDiffuseMaterial(&object_suisoukazari2a_hontai_hontai.basicdxmodel->mats[5]); // OKazari2
		AddWhiteDiffuseMaterial(&object_boxana_boxana_boxana.basicdxmodel->mats[7]); // OBoxAna
		// Model replacements
		object_hikari01a_light_light = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\hikari01a_light.nja.sa1mdl"); // Edited hierarchy
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[3]);
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[4]);
		AddWhiteDiffuseMaterial(&object_hikari01a_light_light.basicdxmodel->mats[5]);
		WriteCall((void*)0x0059D444, RenderOHikari); // Add back OHikari green light
		// E105 boss missile
		object_e105_misil_e105_misil_e105_misil.child->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		object_e105_misil_e105_misil_e105_misil.child->sibling->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		object_e105_misil_e105_misil_e105_misil.child->sibling->sibling->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		object_e105_misil_e105_misil_e105_misil.child->sibling->sibling->sibling->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		// OLight3
		object_lightdw_daiv_daiv = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\lightdw_daiv.nja.sa1mdl"); // Edited hierarchy
		WriteCall((void*)0x005A2EF4, OLight3_Display);
		// OEntotsu particle fix
		WriteCall((void*)0x005A33C0, OEntotsuParticleFix);
		// OBridge
		object_hasi_enhasi_enhasi = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\hasi_enhasi.nja.sa1mdl");
		object_hasi_enhasi_e_enhasi_e = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\hasi_enhasi_e.nja.sa1mdl");
		model_hasi_enhasi_obj4 = *object_hasi_enhasi_enhasi.child->sibling->basicdxmodel; // OBridge moving bit
		model_hasi_enhasi_bmerge1 = *object_hasi_enhasi_enhasi.child->basicdxmodel; // OBridge static bit 1 (support)
		model_hasi_enhasi_enhasi = *object_hasi_enhasi_enhasi.basicdxmodel; // OBridge static bit 2 (round thing)
		// OSuimen0
		WriteCall((void*)0x005A8FE1, RenderSuimen); // Fix OSuimen0 flickering
		// Other models
		object_kaitendai_daidai_daidai = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\kaitenkey_dai.nja.sa1mdl"); // OKaitenKey
		object_kaitenkey_daionly_daionly = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\kaitenkey_daionly.nja.sa1mdl"); // Unused?
		model_kaitenkey_dai_dai = *object_kaitendai_daidai_daidai.basicdxmodel;
		model_kaitenkey_dai_totte = *object_kaitendai_daidai_daidai.child->basicdxmodel; // OKaitenKey handle
		// ORoboTV broken glass
		WriteCall((void*)0x005A5D6C, RenderRoboTVBrokenGlass);
		WriteCall((void*)0x005A5CFC, RenderRoboTVGlass2);
		BrokenGlass = CloneObject(&object_tv_clash_base_tv_clash_base);
		HideMesh_Object(BrokenGlass, 0, 1);
		HideMesh_Object(&object_tv_clash_base_tv_clash_base, 2); // Hide the black part to render separately
		object_clain_clnull_clnull = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\clain_clnull.nja.sa1mdl"); // OCarne
		ForceLevelSpecular_Object(object_clain_clnull_clnull.child->sibling->child, false);
		ForceLevelSpecular_Object(object_clain_clnull_clnull.child->sibling->child->sibling, false);
		object_cargo_crgfront_crgfront = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_crgfront.nja.sa1mdl"); // OCargoTop
		object_enecrg_enemycont_enemycont = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\enecrg_enemycont.nja.sa1mdl"); // OEnemyContainer
		ForceLevelSpecular_Object(&object_enecrg_enemycont_enemycont, false);
		object_gyakubj_hashira_hashira = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\gyakubj_hashira.nja.sa1mdl"); // OBanji 1
		object_gyakubj_dai_dai = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\gyakubj_dai.nja.sa1mdl"); // OBanji 2
		object_cargo_tnnlami_tnnlami = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_tnnlami.nja.sa1mdl"); // Gamma train tracks 1
		object_cargo_tnnlblue_tnnlblue = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_tnnlblue.nja.sa1mdl"); // Gamma train tracks 2
		object_cargo_renketubo_renketubo = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_renketubo.nja.sa1mdl"); // Gamma train chain
		object_cargo_contenaa_contenaa = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_contenaa.nja.sa1mdl"); // OCargoContainer related
		object_clcargo_cl_contenaa_cl_contenaa = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\clcargo_cl_contenaa.nja.sa1mdl"); // OCargoContainer related
		object_fens_nc_fen_nc_fen = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\fens_nc_fen.nja.sa1mdl"); // OFens
		object_biribiria_br_br = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\biribiria_br.nja.sa1mdl"); // OBiriBiri
		object_blhs_hotblbase_hotblbase = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\blhs_hotblbase.nja.sa1mdl"); // OLight4
		object_cargo_crg_crg = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_crg.nja.sa1mdl"); // OCargoStart 1, OCargo, Gamma train platform
		object_cargo_midcontena_midcontena = *LoadModel("system\\data\\stg12_hotshelter\\act04\\models\\cargo_midcontena.nja.sa1mdl"); // OCargoStart 2
		object_gt_gateyoko_gateyoko = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\gt_gateyoko.nja.sa1mdl"); // OGateSide
		object_g_sasae1a_sasae1a_sasae1a = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\g_sasae1a_sasae1a.nja.sa1mdl"); // OSasae1A
		object_g_sasae1b_sasae1b_sasae1b = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\g_sasae1b_sasae1b.nja.sa1mdl"); // OSasae1B
		object_g_sasae1c_sasae1c_sasae1c = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\g_sasae1c_sasae1c.nja.sa1mdl"); // OSasae1C
		object_g_sasae1d_sasae1d_sasae1d = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\g_sasae1d_sasae1d.nja.sa1mdl"); // OSasae1D
		object_redkem_ent_a_ent_a = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\redkem_ent_a.nja.sa1mdl"); // OEntotsu
		object_drmcan_drmcan_drmcan = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\drmcan_drmcan.nja.sa1mdl"); // ODrumcan
		object_kaidan_hontai_hontai = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\kaidan_hontai.nja.sa1mdl"); // OKaidan
		object_ukidoor_ukidoor_ukidoor = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\ukidoor_ukidoor.nja.sa1mdl"); // Floodgates
		object_eel1_elvater_elvater = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\eel1_elvater.nja.sa1mdl"); // OElevator inside
		object_eel1_doa_b_doa_b = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\eel1_doa_b.nja.sa1mdl"); // OElevator door 1
		object_eel1_doa_a_doa_a = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\eel1_doa_a.nja.sa1mdl"); // OElevator door 2
		object_syoumei_syoumei1h_syoumei1h = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\syoumei_syoumei1h.nja.sa1mdl"); // OSyoumei
		object_suisou_taki_taki_taki = *LoadModel("system\\data\\stg12_hotshelter\\act01\\models\\suisou_taki_taki.nja.sa1mdl"); // Waterfall thing
		//object_suimen_wt_big_wt_big = *LoadModel("system\\data\\stg12_hotshelter\\act01\\models\\suimen_wt_big.nja.sa1mdl"); // Water surface in drainage room (restoring this break fishing collision)
		matlist_suimen_wt_big_wt_big.attrflags = 0x94B02400; // Instead, change only the material to match the DC version
		matlist_suimen_wt_big_wt_big.specular.color = 0xFFFFFFFF;
		object_suimen_wt_bigcircle_wt_bigcircle = *LoadModel("system\\data\\stg12_hotshelter\\act01\\models\\suimen_wt_bigcircle.nja.sa1mdl"); // Water surface in Big's room
		object_suisou_taki_wt_o_water_wt_o_water = *LoadModel("system\\data\\stg12_hotshelter\\act01\\models\\suisou_taki_wt_o_water.nja.sa1mdl"); // Water objects behind glass in drainage room
		object_kaitenhyouji_meter_meter = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\kaitenhyouji_meter.nja.sa1mdl"); // OKaitenmeter
		object_bluebox____ = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\bluebox__.nja.sa1mdl"); // Colored cube 1
		AddWhiteDiffuseMaterial(&object_bluebox____.basicdxmodel->mats[8]);
		object_orangebox____ = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\orangebox__.nja.sa1mdl"); // Colored cube 2
		AddWhiteDiffuseMaterial(&object_orangebox____.basicdxmodel->mats[8]);
		object_purplebox____ = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\purplebox__.nja.sa1mdl"); // Colored cube 3
		AddWhiteDiffuseMaterial(&object_purplebox____.basicdxmodel->mats[8]);
		object_yellowbox____ = *LoadModel("system\\data\\stg12_hotshelter\\act02\\models\\yellowbox__.nja.sa1mdl"); // Colored cube 4
		AddWhiteDiffuseMaterial(&object_yellowbox____.basicdxmodel->mats[8]);
		object_wc_doa_doa_doa = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\wc_doa_doa.nja.sa1mdl"); // Bathroom door
		object_computer_bmerge6 = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\computer.nja.sa1mdl"); // OComputer
		object_breakkabe_before_before = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\breakkabe_before.nja.sa1mdl"); // Broken wall (full)
		object_breakkabe_after_after = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\breakkabe_after.nja.sa1mdl"); // Broken wall (broken)
		object_breakkabe_br_part_br_part = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\breakkabe_br_part.nja.sa1mdl"); // Broken wall (pieces)
		object_tobira_hot_door1h_door1h = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\tobira_hot_door1h.nja.sa1mdl"); // Egghead door 1
		NJS_OBJECT* object_gt_bgatecente = LoadModel("system\\data\\stg12_hotshelter\\common\\models\\gt_bgatecente.nja.sa1mdl"); // Egghead door 2
		*object_gt_bgatecente_bgate01.basicdxmodel = *object_gt_bgatecente->basicdxmodel;
		object_gt_bgatecente_bgate01.sibling->evalflags |= NJD_EVAL_HIDE; // DX has the logo as sibling
		*object_gt_bgatecente_bgate02.basicdxmodel = *object_gt_bgatecente->sibling->basicdxmodel;
		object_gt_bgatecente_bgate02.sibling->evalflags |= NJD_EVAL_HIDE;
		object_kaitendai_daidai_daidai = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\kaitendai_daidai.nja.sa1mdl"); // OKaitenashiba
		object_daruma_ue_ue = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\daruma_ue.nja.sa1mdl"); // Daruma (unused lightning box)
		object_ukijima_ukiasiba_ukiasiba = *LoadModel("system\\data\\stg12_hotshelter\\act01\\models\\ukijima_ukiasiba.nja.sa1mdl"); // OUkijima
		object_suisoukazari1a_hontai_hontai = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\suisoukazari1a_hontai.nja.sa1mdl"); // Underwater decoration 1
		object_suisoukazari2a_hontai_hontai = *LoadModel("system\\data\\stg12_hotshelter\\common\\models\\suisoukazari2a_hontai.nja.sa1mdl"); // Underwater decoration 2
		ModelsLoaded_STG12 = true;
	}
}

void HotShelter_OnFrame()
{
	if (ssStageNumber == !LevelIDs_HotShelter)
		return;

	// Fog in E105 room
	if (ssActNumber == 2 && !ChkPause())
	{
		if (ReduceHotShelterFog)
		{
			gFog.f32EndZ = min(6000, gFog.f32StartZ + 32.0f);
			gFog.f32StartZ = min(2000, gFog.f32StartZ + 16.0f);
		}
	}
	// Reset if leaving the level
	switch (ssGameMode)
	{
	case MD_GAME_REINIT2:
	case MD_GAME_FADEIN:
	case MD_GAME_FADEOUT_MISS_RESTART:
	case MD_GAME_STAGENAME_INIT:
		ReduceHotShelterFog = false;
		break;
	}
}