#include "stdafx.h"

void E101FVF_Init();

void B_E101_Init()
{
	ReplaceSET("SETE101E");
	ReplacePVM("E101");
	ReplacePVM("E101_TIKEI");
	E101FVF_Init();
	// E101 rocket (SL object)
	object_01_misile_01_misile_01_misile.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT;
	object_01_misile_01_misile_01_misile.basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT;
	object_01_misile_01_misile_01_misile.basicdxmodel->mats[2].attrflags |= NJD_FLAG_IGNORE_LIGHT;
	object_01_misile_01_misile_01_misile.basicdxmodel->mats[3].attrflags |= NJD_FLAG_IGNORE_LIGHT;
}

void B_E101_Load()
{
	LevelLoader(LevelAndActIDs_E101, "SYSTEM\\data\\boss_e101\\landtable2300.c.sa1lvl", &texlist_e101_tikei);
}