#include "stdafx.h"

void PatchModels_STG01()
{
	// dxpc\stg01_beach\bg\models\sea_nbg.nja.sa1mdl
	// Ignores lighting
	//((NJS_MATERIAL*)0x0103A978)->diffuse.color = 0xFF0039A8;
	//((NJS_MATERIAL*)0x0103A98C)->diffuse.color = 0xFF7F7F7F;

	// dxpc\stg01_beach\common\models\sea02_taki.nja.sa1mdl
	// Ignores lighting
	//((NJS_MATERIAL*)0x010AC478)->diffuse.color = 0xFF00FFFF;

	// dxpc\stg01_beach\common\models\seaobj_bungalow.nja.sa1mdl
	*(NJS_TEX*)0x01040F20 = { 5, -249 };
	*(NJS_TEX*)0x01040F24 = { 1512, -249 };
	*(NJS_TEX*)0x01040F28 = { 5, 253 };
	*(NJS_TEX*)0x01040F2C = { 1512, 253 };
	*(NJS_TEX*)0x01040F30 = { 770, -249 };
	*(NJS_TEX*)0x01040F34 = { 1512, 253 };
	*(NJS_TEX*)0x01040F38 = { 5, 253 };
	*(NJS_TEX*)0x01040F3C = { 770, -249 };
	*(NJS_TEX*)0x01040F40 = { 1512, 253 };
	*(NJS_TEX*)0x01040F44 = { 5, 253 };
	*(NJS_TEX*)0x01040F48 = { 1512, 253 };
	*(NJS_TEX*)0x01040F4C = { 5, 253 };
	*(NJS_TEX*)0x01040F50 = { 770, -249 };
	*(NJS_TEX*)0x01040F54 = { 1512, 253 };
	*(NJS_TEX*)0x01040F58 = { 5, 253 };
	*(NJS_TEX*)0x01040F5C = { 770, -249 };

	// dxpc\stg01_beach\common\models\seaobj_dolphin.nja.sa1mdl
		//Replaced manually (transparency sorting)

	// dxpc\stg01_beach\common\models\seaobj_guillotine.nja.sa1mdl
	*(NJS_TEX*)0x01043508 = { 252, 254 };
	*(NJS_TEX*)0x0104350C = { 128, 254 };
	*(NJS_TEX*)0x01043510 = { 252, 2 };
	*(NJS_TEX*)0x01043514 = { 128, 2 };
	*(NJS_TEX*)0x01043518 = { 128, 2 };
	*(NJS_TEX*)0x0104351C = { 252, 2 };
	*(NJS_TEX*)0x01043520 = { 128, 254 };
	*(NJS_TEX*)0x01043524 = { 252, 254 };
	*(NJS_TEX*)0x01042BC8 = { 120, 161 };
	*(NJS_TEX*)0x01042BCC = { 2, 161 };
	*(NJS_TEX*)0x01042BD0 = { 120, -249 };
	*(NJS_TEX*)0x01042BD4 = { 2, -249 };
	*(NJS_TEX*)0x01042BD8 = { 120, -249 };
	*(NJS_TEX*)0x01042BDC = { 120, 161 };
	*(NJS_TEX*)0x01042BE0 = { 2, -249 };
	*(NJS_TEX*)0x01042BE4 = { 2, 161 };
	*(NJS_TEX*)0x01042BE8 = { 120, 161 };
	*(NJS_TEX*)0x01042BEC = { 2, 161 };
	*(NJS_TEX*)0x01042BF0 = { 120, -249 };
	*(NJS_TEX*)0x01042BF4 = { 2, -249 };
	*(NJS_TEX*)0x01042BF8 = { 2, 161 };
	*(NJS_TEX*)0x01042BFC = { 2, -249 };
	*(NJS_TEX*)0x01042C00 = { 120, 161 };
	*(NJS_TEX*)0x01042C04 = { 120, -249 };
	*(NJS_TEX*)0x01042C08 = { 2, 161 };
	*(NJS_TEX*)0x01042C0C = { 2, -249 };
	*(NJS_TEX*)0x01042C10 = { 120, 161 };
	*(NJS_TEX*)0x01042C14 = { 120, -249 };
	*(NJS_TEX*)0x01042C18 = { 2, 161 };
	*(NJS_TEX*)0x01042C1C = { 2, -249 };
	*(NJS_TEX*)0x01042C20 = { 120, 161 };
	*(NJS_TEX*)0x01042C24 = { 120, -249 };
	*(NJS_TEX*)0x01042C28 = { 2, 161 };
	*(NJS_TEX*)0x01042C2C = { 2, -249 };
	*(NJS_TEX*)0x01042C30 = { 120, 161 };
	*(NJS_TEX*)0x01042C34 = { 120, -249 };
	*(NJS_TEX*)0x01042C38 = { 2, 161 };
	*(NJS_TEX*)0x01042C3C = { 2, -249 };
	*(NJS_TEX*)0x01042C40 = { 120, 161 };
	*(NJS_TEX*)0x01042C44 = { 120, -249 };

	// dxpc\stg01_beach\common\models\seaobj_hasi_b.nja.sa1mdl
	*(NJS_TEX*)0x01049018 = { 1020, -255 };
	*(NJS_TEX*)0x01049020 = { 0, -255 };
	*(NJS_TEX*)0x01049028 = { 0, -255 };
	*(NJS_TEX*)0x01049030 = { 1020, -255 };
	*(NJS_TEX*)0x01049038 = { 11, 127 };
	*(NJS_TEX*)0x0104903C = { 1008, 127 };
	*(NJS_TEX*)0x01049040 = { 11, 208 };
	*(NJS_TEX*)0x01049044 = { 1008, 208 };
	*(NJS_TEX*)0x01049048 = { 11, 127 };
	*(NJS_TEX*)0x0104904C = { 1008, 127 };
	*(NJS_TEX*)0x01049050 = { 11, 208 };
	*(NJS_TEX*)0x01049054 = { 1008, 208 };
	*(NJS_TEX*)0x010491C0 = { 61, -253 };
	*(NJS_TEX*)0x010491C4 = { 61, 253 };
	*(NJS_TEX*)0x010491C8 = { 126, -253 };
	*(NJS_TEX*)0x010491CC = { 126, 253 };
	*(NJS_TEX*)0x010491D0 = { 61, -253 };
	*(NJS_TEX*)0x010491D4 = { 61, 253 };
	*(NJS_TEX*)0x010491D8 = { 126, -253 };
	*(NJS_TEX*)0x010491DC = { 126, 253 };

	// dxpc\stg01_beach\common\models\seaobj_jumpdai.nja.sa1mdl
	*(NJS_TEX*)0x010A1F6C = { 509, 255 };
	*(NJS_TEX*)0x010A1F74 = { 509, 7 };
	*(NJS_TEX*)0x010A1240 = { 128, -249 };
	*(NJS_TEX*)0x010A1244 = { 126, 253 };
	*(NJS_TEX*)0x010A1248 = { 250, -249 };
	*(NJS_TEX*)0x010A124C = { 250, 253 };
	*(NJS_TEX*)0x010A1250 = { 126, 253 };
	*(NJS_TEX*)0x010A1254 = { 250, 253 };
	*(NJS_TEX*)0x010A1258 = { 128, -249 };
	*(NJS_TEX*)0x010A125C = { 250, -249 };
	*(NJS_TEX*)0x010A1260 = { 126, 253 };
	*(NJS_TEX*)0x010A1264 = { 250, 253 };
	*(NJS_TEX*)0x010A1268 = { 128, -249 };
	*(NJS_TEX*)0x010A126C = { 250, -249 };
	*(NJS_TEX*)0x010A1270 = { 189, -77 };
	*(NJS_TEX*)0x010A1274 = { 250, -249 };
	*(NJS_TEX*)0x010A1278 = { 128, -249 };
	*(NJS_TEX*)0x010A127C = { 128, -249 };
	*(NJS_TEX*)0x010A1280 = { 126, 253 };
	*(NJS_TEX*)0x010A1284 = { 250, -249 };
	*(NJS_TEX*)0x010A1288 = { 250, 253 };
	*(NJS_TEX*)0x010A128C = { 250, -249 };
	*(NJS_TEX*)0x010A1290 = { 128, -249 };
	*(NJS_TEX*)0x010A1294 = { 250, 253 };
	*(NJS_TEX*)0x010A1298 = { 126, 253 };
	*(NJS_TEX*)0x010A129C = { 250, -249 };
	*(NJS_TEX*)0x010A12A0 = { 128, -249 };
	*(NJS_TEX*)0x010A12A4 = { 250, 253 };
	*(NJS_TEX*)0x010A12A8 = { 126, 253 };
	*(NJS_TEX*)0x010A12AC = { 189, -77 };
	*(NJS_TEX*)0x010A12B0 = { 250, -249 };
	*(NJS_TEX*)0x010A12B4 = { 128, -249 };

	// dxpc\stg01_beach\common\models\seaobj_kage.nja.sa1mdl
	*(NJS_TEX*)0x010C0640 = { 0, -255 };
	*(NJS_TEX*)0x010C0648 = { 510, -255 };

	// dxpc\stg01_beach\common\models\seaobj_kusa7.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_kusa8.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_oruka.nja.sa1mdl
	((NJS_MATERIAL*)0x010693B8)->attrflags = 0x9439A400;
	((NJS_MATERIAL*)0x010693CC)->attrflags = 0x9469A400;
	((NJS_MATERIAL*)0x010693E0)->attrflags = 0x9469A400;
	((NJS_MATERIAL*)0x010693F4)->attrflags = 0x9429A400;
	((NJS_MATERIAL*)0x01069408)->attrflags = 0x94292400;

	// dxpc\stg01_beach\common\models\seaobj_parasol01.nja.sa1mdl
	*(NJS_TEX*)0x01083A10 = { 7, 251 };
	*(NJS_TEX*)0x01083A14 = { 502, 251 };
	*(NJS_TEX*)0x01083A18 = { 247, 11 };
	*(NJS_TEX*)0x01083A1C = { 7, 251 };
	*(NJS_TEX*)0x01083A20 = { 502, 251 };
	*(NJS_TEX*)0x01083A24 = { 247, 11 };
	*(NJS_TEX*)0x01083A28 = { 7, 251 };
	*(NJS_TEX*)0x01083A2C = { 502, 251 };
	*(NJS_TEX*)0x01083A30 = { 247, 11 };
	*(NJS_TEX*)0x01083A34 = { 7, 251 };
	*(NJS_TEX*)0x01083A38 = { 502, 251 };
	*(NJS_TEX*)0x01083A3C = { 247, 11 };
	*(NJS_TEX*)0x01083A40 = { 502, 11 };
	*(NJS_TEX*)0x01083A44 = { 7, 11 };
	*(NJS_TEX*)0x01083A48 = { 502, 81 };
	*(NJS_TEX*)0x01083A4C = { 7, 81 };
	*(NJS_TEX*)0x01083A50 = { 7, 251 };
	*(NJS_TEX*)0x01083A54 = { 502, 251 };
	*(NJS_TEX*)0x01083A58 = { 7, 11 };
	*(NJS_TEX*)0x01083A5C = { 502, 11 };
	*(NJS_TEX*)0x01083A60 = { 502, 11 };
	*(NJS_TEX*)0x01083A64 = { 7, 11 };
	*(NJS_TEX*)0x01083A68 = { 502, 81 };
	*(NJS_TEX*)0x01083A6C = { 7, 81 };
	*(NJS_TEX*)0x01083A70 = { 7, 251 };
	*(NJS_TEX*)0x01083A74 = { 502, 251 };
	*(NJS_TEX*)0x01083A78 = { 7, 11 };
	*(NJS_TEX*)0x01083A7C = { 502, 11 };
	*(NJS_TEX*)0x01083A80 = { 502, 11 };
	*(NJS_TEX*)0x01083A84 = { 7, 11 };
	*(NJS_TEX*)0x01083A88 = { 502, 81 };
	*(NJS_TEX*)0x01083A8C = { 7, 81 };
	*(NJS_TEX*)0x01083A90 = { 7, 251 };
	*(NJS_TEX*)0x01083A94 = { 502, 251 };
	*(NJS_TEX*)0x01083A98 = { 7, 11 };
	*(NJS_TEX*)0x01083A9C = { 502, 11 };
	*(NJS_TEX*)0x01083AA0 = { 502, 11 };
	*(NJS_TEX*)0x01083AA4 = { 7, 11 };
	*(NJS_TEX*)0x01083AA8 = { 502, 81 };
	*(NJS_TEX*)0x01083AAC = { 7, 81 };
	*(NJS_TEX*)0x01083AB0 = { 7, 251 };
	*(NJS_TEX*)0x01083AB4 = { 502, 251 };
	*(NJS_TEX*)0x01083AB8 = { 7, 11 };
	*(NJS_TEX*)0x01083ABC = { 502, 11 };
	*(NJS_TEX*)0x01083AC0 = { 7, 251 };
	*(NJS_TEX*)0x01083AC4 = { 502, 251 };
	*(NJS_TEX*)0x01083AC8 = { 247, 11 };
	*(NJS_TEX*)0x01083ACC = { 7, 251 };
	*(NJS_TEX*)0x01083AD0 = { 502, 251 };
	*(NJS_TEX*)0x01083AD4 = { 247, 11 };
	*(NJS_TEX*)0x01083AD8 = { 7, 251 };
	*(NJS_TEX*)0x01083ADC = { 502, 251 };
	*(NJS_TEX*)0x01083AE0 = { 7, 11 };
	*(NJS_TEX*)0x01083AE4 = { 502, 11 };
	*(NJS_TEX*)0x01083AE8 = { 7, 251 };
	*(NJS_TEX*)0x01083AEC = { 502, 251 };
	*(NJS_TEX*)0x01083AF0 = { 7, 11 };
	*(NJS_TEX*)0x01083AF4 = { 502, 11 };
	*(NJS_TEX*)0x01083AF8 = { 502, 11 };
	*(NJS_TEX*)0x01083AFC = { 7, 11 };
	*(NJS_TEX*)0x01083B00 = { 502, 96 };
	*(NJS_TEX*)0x01083B04 = { 7, 96 };
	*(NJS_TEX*)0x01083B08 = { 502, 11 };
	*(NJS_TEX*)0x01083B0C = { 7, 11 };
	*(NJS_TEX*)0x01083B10 = { 502, 96 };
	*(NJS_TEX*)0x01083B14 = { 7, 96 };
	*(NJS_TEX*)0x01083B18 = { 7, 251 };
	*(NJS_TEX*)0x01083B1C = { 502, 251 };
	*(NJS_TEX*)0x01083B20 = { 231, 11 };
	*(NJS_TEX*)0x01083B24 = { 7, 251 };
	*(NJS_TEX*)0x01083B28 = { 502, 251 };
	*(NJS_TEX*)0x01083B2C = { 231, 11 };
	*(NJS_TEX*)0x01083B30 = { 7, 89 };
	*(NJS_TEX*)0x01083B34 = { 502, 89 };
	*(NJS_TEX*)0x01083B38 = { 7, 11 };
	*(NJS_TEX*)0x01083B3C = { 502, 11 };
	*(NJS_TEX*)0x01083B40 = { 7, 251 };
	*(NJS_TEX*)0x01083B44 = { 502, 251 };
	*(NJS_TEX*)0x01083B48 = { 7, 11 };
	*(NJS_TEX*)0x01083B4C = { 502, 11 };
	*(NJS_TEX*)0x01083B50 = { 502, 11 };
	*(NJS_TEX*)0x01083B54 = { 7, 11 };
	*(NJS_TEX*)0x01083B58 = { 502, 89 };
	*(NJS_TEX*)0x01083B5C = { 7, 89 };
	*(NJS_TEX*)0x01083B60 = { 7, 251 };
	*(NJS_TEX*)0x01083B64 = { 502, 251 };
	*(NJS_TEX*)0x01083B68 = { 7, 11 };
	*(NJS_TEX*)0x01083B6C = { 502, 11 };

	// dxpc\stg01_beach\common\models\seaobj_parasol02.nja.sa1mdl
	*(NJS_TEX*)0x01084E20 = { 7, 251 };
	*(NJS_TEX*)0x01084E24 = { 502, 251 };
	*(NJS_TEX*)0x01084E28 = { 262, 11 };
	*(NJS_TEX*)0x01084E2C = { 7, 251 };
	*(NJS_TEX*)0x01084E30 = { 502, 251 };
	*(NJS_TEX*)0x01084E34 = { 262, 11 };
	*(NJS_TEX*)0x01084E38 = { 7, 251 };
	*(NJS_TEX*)0x01084E3C = { 502, 251 };
	*(NJS_TEX*)0x01084E40 = { 262, 11 };
	*(NJS_TEX*)0x01084E44 = { 7, 251 };
	*(NJS_TEX*)0x01084E48 = { 502, 251 };
	*(NJS_TEX*)0x01084E4C = { 262, 11 };
	*(NJS_TEX*)0x01084E50 = { 502, 11 };
	*(NJS_TEX*)0x01084E54 = { 7, 11 };
	*(NJS_TEX*)0x01084E58 = { 502, 96 };
	*(NJS_TEX*)0x01084E5C = { 7, 96 };
	*(NJS_TEX*)0x01084E60 = { 7, 251 };
	*(NJS_TEX*)0x01084E64 = { 502, 251 };
	*(NJS_TEX*)0x01084E68 = { 7, 11 };
	*(NJS_TEX*)0x01084E6C = { 502, 11 };
	*(NJS_TEX*)0x01084E70 = { 502, 11 };
	*(NJS_TEX*)0x01084E74 = { 7, 11 };
	*(NJS_TEX*)0x01084E78 = { 502, 96 };
	*(NJS_TEX*)0x01084E7C = { 7, 96 };
	*(NJS_TEX*)0x01084E80 = { 7, 251 };
	*(NJS_TEX*)0x01084E84 = { 502, 251 };
	*(NJS_TEX*)0x01084E88 = { 7, 11 };
	*(NJS_TEX*)0x01084E8C = { 502, 11 };
	*(NJS_TEX*)0x01084E90 = { 502, 11 };
	*(NJS_TEX*)0x01084E94 = { 7, 11 };
	*(NJS_TEX*)0x01084E98 = { 502, 96 };
	*(NJS_TEX*)0x01084E9C = { 7, 96 };
	*(NJS_TEX*)0x01084EA0 = { 7, 251 };
	*(NJS_TEX*)0x01084EA4 = { 502, 251 };
	*(NJS_TEX*)0x01084EA8 = { 7, 11 };
	*(NJS_TEX*)0x01084EAC = { 502, 11 };
	*(NJS_TEX*)0x01084EB0 = { 502, 11 };
	*(NJS_TEX*)0x01084EB4 = { 7, 11 };
	*(NJS_TEX*)0x01084EB8 = { 502, 96 };
	*(NJS_TEX*)0x01084EBC = { 7, 96 };
	*(NJS_TEX*)0x01084EC0 = { 7, 251 };
	*(NJS_TEX*)0x01084EC4 = { 502, 251 };
	*(NJS_TEX*)0x01084EC8 = { 7, 11 };
	*(NJS_TEX*)0x01084ECC = { 502, 11 };
	*(NJS_TEX*)0x01084ED0 = { 7, 251 };
	*(NJS_TEX*)0x01084ED4 = { 502, 251 };
	*(NJS_TEX*)0x01084ED8 = { 278, 11 };
	*(NJS_TEX*)0x01084EDC = { 7, 251 };
	*(NJS_TEX*)0x01084EE0 = { 502, 251 };
	*(NJS_TEX*)0x01084EE4 = { 278, 11 };
	*(NJS_TEX*)0x01084EE8 = { 7, 251 };
	*(NJS_TEX*)0x01084EEC = { 502, 251 };
	*(NJS_TEX*)0x01084EF0 = { 278, 11 };
	*(NJS_TEX*)0x01084EF4 = { 7, 251 };
	*(NJS_TEX*)0x01084EF8 = { 502, 251 };
	*(NJS_TEX*)0x01084EFC = { 278, 11 };
	*(NJS_TEX*)0x01084F00 = { 7, 112 };
	*(NJS_TEX*)0x01084F04 = { 502, 112 };
	*(NJS_TEX*)0x01084F08 = { 7, 11 };
	*(NJS_TEX*)0x01084F0C = { 502, 11 };
	*(NJS_TEX*)0x01084F10 = { 7, 251 };
	*(NJS_TEX*)0x01084F14 = { 502, 251 };
	*(NJS_TEX*)0x01084F18 = { 7, 11 };
	*(NJS_TEX*)0x01084F1C = { 502, 11 };
	*(NJS_TEX*)0x01084F20 = { 502, 11 };
	*(NJS_TEX*)0x01084F24 = { 7, 11 };
	*(NJS_TEX*)0x01084F28 = { 502, 112 };
	*(NJS_TEX*)0x01084F2C = { 7, 112 };
	*(NJS_TEX*)0x01084F30 = { 7, 251 };
	*(NJS_TEX*)0x01084F34 = { 502, 251 };
	*(NJS_TEX*)0x01084F38 = { 7, 11 };
	*(NJS_TEX*)0x01084F3C = { 502, 11 };
	*(NJS_TEX*)0x01084F40 = { 502, 11 };
	*(NJS_TEX*)0x01084F44 = { 7, 11 };
	*(NJS_TEX*)0x01084F48 = { 502, 112 };
	*(NJS_TEX*)0x01084F4C = { 7, 112 };
	*(NJS_TEX*)0x01084F50 = { 7, 251 };
	*(NJS_TEX*)0x01084F54 = { 502, 251 };
	*(NJS_TEX*)0x01084F58 = { 7, 11 };
	*(NJS_TEX*)0x01084F5C = { 502, 11 };
	*(NJS_TEX*)0x01084F60 = { 502, 11 };
	*(NJS_TEX*)0x01084F64 = { 7, 11 };
	*(NJS_TEX*)0x01084F68 = { 502, 112 };
	*(NJS_TEX*)0x01084F6C = { 7, 112 };
	*(NJS_TEX*)0x01084F70 = { 7, 251 };
	*(NJS_TEX*)0x01084F74 = { 502, 251 };
	*(NJS_TEX*)0x01084F78 = { 7, 11 };
	*(NJS_TEX*)0x01084F7C = { 502, 11 };

	// dxpc\stg01_beach\common\models\seaobj_parasol03.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_pier_a.nja.sa1mdl
	*(NJS_TEX*)0x010922E0 = { 96, -251 };
	*(NJS_TEX*)0x010922E8 = { 34, -251 };
	*(NJS_TEX*)0x010922F4 = { 34, -251 };
	*(NJS_TEX*)0x010922FC = { 96, -251 };
	*(NJS_TEX*)0x01092304 = { 34, -251 };
	*(NJS_TEX*)0x0109230C = { 96, -251 };
	*(NJS_TEX*)0x01092314 = { 34, -251 };
	*(NJS_TEX*)0x0109231C = { 96, -251 };
	*(NJS_TEX*)0x01092324 = { 34, -251 };
	*(NJS_TEX*)0x0109232C = { 96, -251 };
	*(NJS_TEX*)0x01092334 = { 34, -251 };
	*(NJS_TEX*)0x0109233C = { 96, -251 };
	*(NJS_TEX*)0x01092344 = { 0, -255 };
	*(NJS_TEX*)0x01092348 = { 20, 253 };
	*(NJS_TEX*)0x0109234C = { 20, -253 };
	*(NJS_TEX*)0x01092354 = { 0, -255 };
	*(NJS_TEX*)0x01092358 = { 20, 253 };
	*(NJS_TEX*)0x0109235C = { 20, -253 };
	*(NJS_TEX*)0x01092368 = { 125, -255 };
	*(NJS_TEX*)0x0109236C = { 0, -255 };
	*(NJS_TEX*)0x01092370 = { 127, 1 };
	*(NJS_TEX*)0x01092374 = { 253, 1 };
	*(NJS_TEX*)0x01092380 = { 127, 1 };
	*(NJS_TEX*)0x01092384 = { 253, 1 };
	*(NJS_TEX*)0x01092390 = { 127, 1 };
	*(NJS_TEX*)0x01092394 = { 253, 1 };
	*(NJS_TEX*)0x010923A0 = { 253, 1 };
	*(NJS_TEX*)0x010923A8 = { 127, 1 };
	*(NJS_TEX*)0x010923B0 = { 127, 1 };
	*(NJS_TEX*)0x010923B4 = { 253, 1 };
	*(NJS_TEX*)0x010923C0 = { 127, 1 };
	*(NJS_TEX*)0x010923C4 = { 253, 1 };
	*(NJS_TEX*)0x010923D8 = { 253, 1 };
	*(NJS_TEX*)0x010923DC = { 127, 1 };
	*(NJS_TEX*)0x010923E8 = { 253, 1 };
	*(NJS_TEX*)0x010923EC = { 127, 1 };
	*(NJS_TEX*)0x010923F8 = { 253, 1 };
	*(NJS_TEX*)0x010923FC = { 127, 1 };
	*(NJS_TEX*)0x01092404 = { 127, 1 };
	*(NJS_TEX*)0x0109240C = { 253, 1 };
	*(NJS_TEX*)0x01092418 = { 253, 1 };
	*(NJS_TEX*)0x0109241C = { 127, 1 };
	*(NJS_TEX*)0x01092428 = { 253, 1 };
	*(NJS_TEX*)0x0109242C = { 127, 1 };
	*(NJS_TEX*)0x0109273C = { 127, -255 };
	*(NJS_TEX*)0x01092744 = { 143, -253 };
	*(NJS_TEX*)0x0109274C = { 127, -255 };
	*(NJS_TEX*)0x01092754 = { 143, -253 };
	*(NJS_TEX*)0x01092760 = { 253, -255 };
	*(NJS_TEX*)0x01092764 = { 127, -255 };
	*(NJS_TEX*)0x0109276C = { 0, -255 };
	*(NJS_TEX*)0x01092770 = { 19, 257 };
	*(NJS_TEX*)0x01092774 = { 19, -255 };
	*(NJS_TEX*)0x0109277C = { 0, -255 };
	*(NJS_TEX*)0x01092780 = { 19, 257 };
	*(NJS_TEX*)0x01092784 = { 19, -255 };
	*(NJS_TEX*)0x01092790 = { 125, -255 };
	*(NJS_TEX*)0x01092794 = { 0, -255 };
	*(NJS_TEX*)0x0109279C = { 127, -255 };
	*(NJS_TEX*)0x010927A4 = { 143, -253 };
	*(NJS_TEX*)0x010927AC = { 127, -255 };
	*(NJS_TEX*)0x010927B4 = { 143, -253 };
	*(NJS_TEX*)0x010927C0 = { 253, -255 };
	*(NJS_TEX*)0x010927C4 = { 127, -255 };
	*(NJS_TEX*)0x010927CC = { 0, -255 };
	*(NJS_TEX*)0x010927D0 = { 19, 257 };
	*(NJS_TEX*)0x010927D4 = { 19, -255 };
	*(NJS_TEX*)0x010927DC = { 0, -255 };
	*(NJS_TEX*)0x010927E0 = { 19, 257 };
	*(NJS_TEX*)0x010927E4 = { 19, -255 };
	*(NJS_TEX*)0x010927F0 = { 125, -255 };
	*(NJS_TEX*)0x010927F4 = { 0, -255 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a01.nja.sa1mdl
	*(NJS_TEX*)0x01093838 = { 124, -121 };
	*(NJS_TEX*)0x0109383C = { 0, -121 };
	*(NJS_TEX*)0x01093840 = { 124, -253 };
	*(NJS_TEX*)0x01093844 = { 0, -253 };
	*(NJS_TEX*)0x01093848 = { 126, -253 };
	*(NJS_TEX*)0x0109384C = { 126, 253 };
	*(NJS_TEX*)0x01093850 = { 106, -253 };
	*(NJS_TEX*)0x01093854 = { 106, 253 };
	*(NJS_TEX*)0x01093858 = { 124, 125 };
	*(NJS_TEX*)0x0109385C = { 0, 129 };
	*(NJS_TEX*)0x01093860 = { 122, -21 };
	*(NJS_TEX*)0x01093864 = { 0, -21 };
	*(NJS_TEX*)0x01093868 = { 124, 253 };
	*(NJS_TEX*)0x0109386C = { 0, 253 };
	*(NJS_TEX*)0x01093870 = { 0, -253 };
	*(NJS_TEX*)0x01093874 = { 22, 253 };
	*(NJS_TEX*)0x01093878 = { 22, -253 };
	*(NJS_TEX*)0x0109387C = { 124, 253 };
	*(NJS_TEX*)0x01093880 = { 124, -253 };
	*(NJS_TEX*)0x01093884 = { 0, -253 };
	*(NJS_TEX*)0x01093888 = { 124, -253 };
	*(NJS_TEX*)0x0109388C = { 0, -253 };
	*(NJS_TEX*)0x01093890 = { 124, 253 };
	*(NJS_TEX*)0x01093894 = { 0, 253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a02.nja.sa1mdl
	*(NJS_TEX*)0x01093A50 = { 0, 127 };
	*(NJS_TEX*)0x01093A54 = { 0, 254 };
	*(NJS_TEX*)0x01093A58 = { 20, 127 };
	*(NJS_TEX*)0x01093A5C = { 20, 254 };
	*(NJS_TEX*)0x01093A60 = { 0, 254 };
	*(NJS_TEX*)0x01093A64 = { 0, 127 };
	*(NJS_TEX*)0x01093A68 = { 122, 254 };
	*(NJS_TEX*)0x01093A6C = { 104, 165 };
	*(NJS_TEX*)0x01093A70 = { 0, 208 };
	*(NJS_TEX*)0x01093A74 = { 124, 208 };
	*(NJS_TEX*)0x01093A78 = { 0, 127 };
	*(NJS_TEX*)0x01093A7C = { 124, 127 };
	*(NJS_TEX*)0x01093A80 = { 122, 254 };
	*(NJS_TEX*)0x01093A84 = { 73, 153 };
	*(NJS_TEX*)0x01093A88 = { 0, 254 };
	*(NJS_TEX*)0x01093A8C = { 0, 93 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a03.nja.sa1mdl
	*(NJS_TEX*)0x01093CC0 = { 0, 2 };
	*(NJS_TEX*)0x01093CC4 = { 48, 127 };
	*(NJS_TEX*)0x01093CC8 = { 124, 44 };
	*(NJS_TEX*)0x01093CCC = { 124, 139 };
	*(NJS_TEX*)0x01093CD0 = { 57, 70 };
	*(NJS_TEX*)0x01093CD4 = { 0, 2 };
	*(NJS_TEX*)0x01093CD8 = { 124, 93 };
	*(NJS_TEX*)0x01093CDC = { 124, 2 };
	*(NJS_TEX*)0x01093CE0 = { 124, 2 };
	*(NJS_TEX*)0x01093CE4 = { 124, 93 };
	*(NJS_TEX*)0x01093CE8 = { 100, 2 };
	*(NJS_TEX*)0x01093CEC = { 100, 123 };
	*(NJS_TEX*)0x01093CF0 = { 61, 2 };
	*(NJS_TEX*)0x01093CF4 = { 69, 91 };
	*(NJS_TEX*)0x01093CF8 = { 38, 2 };
	*(NJS_TEX*)0x01093CFC = { 61, 93 };
	*(NJS_TEX*)0x01093D00 = { 50, 2 };
	*(NJS_TEX*)0x01093D04 = { 112, 109 };
	*(NJS_TEX*)0x01093D08 = { 112, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a04.nja.sa1mdl
	*(NJS_TEX*)0x01093F20 = { 124, 40 };
	*(NJS_TEX*)0x01093F24 = { 102, 20 };
	*(NJS_TEX*)0x01093F28 = { 102, 103 };
	*(NJS_TEX*)0x01093F2C = { 124, 93 };
	*(NJS_TEX*)0x01093F30 = { 0, 16 };
	*(NJS_TEX*)0x01093F34 = { 124, 40 };
	*(NJS_TEX*)0x01093F38 = { 0, 170 };
	*(NJS_TEX*)0x01093F3C = { 124, 186 };
	*(NJS_TEX*)0x01093F40 = { 2, 115 };
	*(NJS_TEX*)0x01093F44 = { 0, 170 };
	*(NJS_TEX*)0x01093F48 = { 22, 74 };
	*(NJS_TEX*)0x01093F4C = { 22, 194 };
	*(NJS_TEX*)0x01093F50 = { 0, 12 };
	*(NJS_TEX*)0x01093F54 = { 0, 170 };
	*(NJS_TEX*)0x01093F58 = { 124, 40 };
	*(NJS_TEX*)0x01093F5C = { 124, 186 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a05.nja.sa1mdl
	*(NJS_TEX*)0x01094100 = { 124, 68 };
	*(NJS_TEX*)0x01094104 = { 0, 2 };
	*(NJS_TEX*)0x01094108 = { 0, 184 };
	*(NJS_TEX*)0x0109410C = { 28, 22 };
	*(NJS_TEX*)0x01094110 = { 0, 184 };
	*(NJS_TEX*)0x01094114 = { 124, 68 };
	*(NJS_TEX*)0x01094118 = { 0, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a06.nja.sa1mdl
	*(NJS_TEX*)0x01094288 = { 122, 28 };
	*(NJS_TEX*)0x0109428C = { 122, 147 };
	*(NJS_TEX*)0x01094290 = { 101, 82 };
	*(NJS_TEX*)0x01094294 = { 101, 147 };
	*(NJS_TEX*)0x01094298 = { 0, 143 };
	*(NJS_TEX*)0x0109429C = { 0, 2 };
	*(NJS_TEX*)0x010942A0 = { 101, 182 };
	*(NJS_TEX*)0x010942A4 = { 53, 184 };
	*(NJS_TEX*)0x010942A8 = { 124, 2 };
	*(NJS_TEX*)0x010942AC = { 124, 172 };
	*(NJS_TEX*)0x010942B0 = { 72, 2 };
	*(NJS_TEX*)0x010942B4 = { 93, 174 };
	*(NJS_TEX*)0x010942B8 = { 109, 2 };
	*(NJS_TEX*)0x010942BC = { 109, 174 };
	*(NJS_TEX*)0x010942C0 = { 109, 127 };
	*(NJS_TEX*)0x010942C4 = { 60, 147 };
	*(NJS_TEX*)0x010942C8 = { 109, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a07.nja.sa1mdl
	*(NJS_TEX*)0x01094480 = { 124, -129 };
	*(NJS_TEX*)0x01094484 = { 0, -129 };
	*(NJS_TEX*)0x01094488 = { 124, 25 };
	*(NJS_TEX*)0x0109448C = { 0, 25 };
	*(NJS_TEX*)0x01094490 = { 108, 253 };
	*(NJS_TEX*)0x01094494 = { 108, -253 };
	*(NJS_TEX*)0x01094498 = { 124, 253 };
	*(NJS_TEX*)0x0109449C = { 108, -253 };
	*(NJS_TEX*)0x010944A0 = { 124, -253 };
	*(NJS_TEX*)0x010944A4 = { 124, 253 };
	*(NJS_TEX*)0x010944A8 = { 0, -253 };
	*(NJS_TEX*)0x010944AC = { 0, 253 };
	*(NJS_TEX*)0x010944B0 = { 22, 253 };
	*(NJS_TEX*)0x010944B4 = { 124, -29 };
	*(NJS_TEX*)0x010944B8 = { 0, -29 };
	*(NJS_TEX*)0x010944BC = { 124, 129 };
	*(NJS_TEX*)0x010944C0 = { 0, 129 };
	*(NJS_TEX*)0x010944C4 = { 22, -253 };
	*(NJS_TEX*)0x010944C8 = { 22, 253 };
	*(NJS_TEX*)0x010944CC = { 0, -253 };
	*(NJS_TEX*)0x010944D0 = { 124, -253 };
	*(NJS_TEX*)0x010944D4 = { 0, -253 };
	*(NJS_TEX*)0x010944D8 = { 124, 253 };
	*(NJS_TEX*)0x010944DC = { 0, 253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a08.nja.sa1mdl
	*(NJS_TEX*)0x01094688 = { 69, 82 };
	*(NJS_TEX*)0x0109468C = { 122, 212 };
	*(NJS_TEX*)0x01094690 = { 122, 113 };
	*(NJS_TEX*)0x01094694 = { 48, 93 };
	*(NJS_TEX*)0x01094698 = { 122, 254 };
	*(NJS_TEX*)0x0109469C = { 122, 93 };
	*(NJS_TEX*)0x010946A0 = { 104, 254 };
	*(NJS_TEX*)0x010946A4 = { 104, 123 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a09.nja.sa1mdl
	*(NJS_TEX*)0x010948A0 = { 0, 127 };
	*(NJS_TEX*)0x010948A4 = { 0, 254 };
	*(NJS_TEX*)0x010948A8 = { 20, 127 };
	*(NJS_TEX*)0x010948AC = { 20, 254 };
	*(NJS_TEX*)0x010948B0 = { 0, 254 };
	*(NJS_TEX*)0x010948B4 = { 0, 127 };
	*(NJS_TEX*)0x010948B8 = { 122, 254 };
	*(NJS_TEX*)0x010948BC = { 104, 165 };
	*(NJS_TEX*)0x010948C0 = { 0, 208 };
	*(NJS_TEX*)0x010948C4 = { 124, 208 };
	*(NJS_TEX*)0x010948C8 = { 0, 127 };
	*(NJS_TEX*)0x010948CC = { 124, 127 };
	*(NJS_TEX*)0x010948D0 = { 122, 254 };
	*(NJS_TEX*)0x010948D4 = { 73, 153 };
	*(NJS_TEX*)0x010948D8 = { 0, 254 };
	*(NJS_TEX*)0x010948DC = { 0, 93 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a10.nja.sa1mdl
	*(NJS_TEX*)0x01094AD0 = { 124, 68 };
	*(NJS_TEX*)0x01094AD4 = { 0, 2 };
	*(NJS_TEX*)0x01094AD8 = { 0, 184 };
	*(NJS_TEX*)0x01094ADC = { 28, 22 };
	*(NJS_TEX*)0x01094AE0 = { 0, 184 };
	*(NJS_TEX*)0x01094AE4 = { 124, 68 };
	*(NJS_TEX*)0x01094AE8 = { 0, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a11.nja.sa1mdl
	*(NJS_TEX*)0x01094C58 = { 122, 28 };
	*(NJS_TEX*)0x01094C5C = { 122, 147 };
	*(NJS_TEX*)0x01094C60 = { 101, 82 };
	*(NJS_TEX*)0x01094C64 = { 101, 147 };
	*(NJS_TEX*)0x01094C68 = { 0, 143 };
	*(NJS_TEX*)0x01094C6C = { 0, 2 };
	*(NJS_TEX*)0x01094C70 = { 101, 182 };
	*(NJS_TEX*)0x01094C74 = { 53, 184 };
	*(NJS_TEX*)0x01094C78 = { 124, 2 };
	*(NJS_TEX*)0x01094C7C = { 124, 172 };
	*(NJS_TEX*)0x01094C80 = { 72, 2 };
	*(NJS_TEX*)0x01094C84 = { 93, 174 };
	*(NJS_TEX*)0x01094C88 = { 109, 2 };
	*(NJS_TEX*)0x01094C8C = { 109, 174 };
	*(NJS_TEX*)0x01094C90 = { 109, 127 };
	*(NJS_TEX*)0x01094C94 = { 60, 147 };
	*(NJS_TEX*)0x01094C98 = { 109, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a12.nja.sa1mdl
	*(NJS_TEX*)0x01094E40 = { 124, 103 };
	*(NJS_TEX*)0x01094E44 = { 124, 2 };
	*(NJS_TEX*)0x01094E48 = { 0, 2 };
	*(NJS_TEX*)0x01094E4C = { 0, 2 };
	*(NJS_TEX*)0x01094E50 = { 124, 80 };
	*(NJS_TEX*)0x01094E54 = { 124, 2 };
	*(NJS_TEX*)0x01094E58 = { 100, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a13.nja.sa1mdl
	*(NJS_TEX*)0x01094FC8 = { 124, 202 };
	*(NJS_TEX*)0x01094FCC = { 0, 202 };
	*(NJS_TEX*)0x01094FD0 = { 124, 127 };
	*(NJS_TEX*)0x01094FD4 = { 0, 127 };
	*(NJS_TEX*)0x01094FD8 = { 0, 254 };
	*(NJS_TEX*)0x01094FDC = { 0, 184 };
	*(NJS_TEX*)0x01094FE0 = { 124, 254 };
	*(NJS_TEX*)0x01094FE4 = { 124, 68 };
	*(NJS_TEX*)0x01094FE8 = { 104, 254 };
	*(NJS_TEX*)0x01094FEC = { 106, 68 };
	*(NJS_TEX*)0x01094FF0 = { 124, 254 };
	*(NJS_TEX*)0x01094FF4 = { 124, 68 };
	*(NJS_TEX*)0x01094FF8 = { 20, 184 };
	*(NJS_TEX*)0x01094FFC = { 20, 254 };
	*(NJS_TEX*)0x01095000 = { 0, 184 };
	*(NJS_TEX*)0x01095004 = { 0, 254 };
	*(NJS_TEX*)0x01095008 = { 124, 44 };
	*(NJS_TEX*)0x0109500C = { 124, 254 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a14.nja.sa1mdl
	*(NJS_TEX*)0x010951E0 = { 124, 40 };
	*(NJS_TEX*)0x010951E4 = { 102, 20 };
	*(NJS_TEX*)0x010951E8 = { 102, 103 };
	*(NJS_TEX*)0x010951EC = { 124, 93 };
	*(NJS_TEX*)0x010951F0 = { 0, 16 };
	*(NJS_TEX*)0x010951F4 = { 124, 40 };
	*(NJS_TEX*)0x010951F8 = { 0, 170 };
	*(NJS_TEX*)0x010951FC = { 124, 186 };
	*(NJS_TEX*)0x01095200 = { 2, 115 };
	*(NJS_TEX*)0x01095204 = { 0, 170 };
	*(NJS_TEX*)0x01095208 = { 22, 74 };
	*(NJS_TEX*)0x0109520C = { 22, 194 };
	*(NJS_TEX*)0x01095210 = { 0, 12 };
	*(NJS_TEX*)0x01095214 = { 0, 170 };
	*(NJS_TEX*)0x01095218 = { 124, 40 };
	*(NJS_TEX*)0x0109521C = { 124, 186 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a15.nja.sa1mdl
	*(NJS_TEX*)0x010953D0 = { 95, 198 };
	*(NJS_TEX*)0x010953D4 = { 95, 4 };
	*(NJS_TEX*)0x010953D8 = { 33, 198 };
	*(NJS_TEX*)0x010953DC = { 33, 4 };
	*(NJS_TEX*)0x010953E0 = { 95, 4 };
	*(NJS_TEX*)0x010953E4 = { 33, 4 };
	*(NJS_TEX*)0x010953E8 = { 95, 198 };
	*(NJS_TEX*)0x010953EC = { 33, 198 };
	*(NJS_TEX*)0x010953F0 = { 110, 130 };
	*(NJS_TEX*)0x010953F4 = { 11, 130 };
	*(NJS_TEX*)0x010953F8 = { 60, 208 };
	*(NJS_TEX*)0x010953FC = { 95, 4 };
	*(NJS_TEX*)0x01095400 = { 33, 4 };
	*(NJS_TEX*)0x01095404 = { 95, 198 };
	*(NJS_TEX*)0x01095408 = { 33, 198 };
	*(NJS_TEX*)0x0109540C = { 33, 251 };
	*(NJS_TEX*)0x01095410 = { 61, 251 };
	*(NJS_TEX*)0x01095414 = { 33, 4 };
	*(NJS_TEX*)0x01095418 = { 61, 4 };
	*(NJS_TEX*)0x0109541C = { 89, 251 };
	*(NJS_TEX*)0x01095420 = { 61, 251 };
	*(NJS_TEX*)0x01095424 = { 89, 4 };
	*(NJS_TEX*)0x01095428 = { 61, 4 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a17.nja.sa1mdl
	*(NJS_TEX*)0x01095760 = { 231, 202 };
	*(NJS_TEX*)0x01095764 = { 232, 4 };
	*(NJS_TEX*)0x01095768 = { 147, 202 };
	*(NJS_TEX*)0x0109576C = { 147, 4 };
	*(NJS_TEX*)0x01095770 = { 147, 202 };
	*(NJS_TEX*)0x01095774 = { 231, 202 };
	*(NJS_TEX*)0x01095778 = { 147, 4 };
	*(NJS_TEX*)0x0109577C = { 232, 4 };
	*(NJS_TEX*)0x01095780 = { 231, 125 };
	*(NJS_TEX*)0x01095784 = { 149, 125 };
	*(NJS_TEX*)0x01095788 = { 187, 203 };
	*(NJS_TEX*)0x0109578C = { 147, 202 };
	*(NJS_TEX*)0x01095790 = { 231, 202 };
	*(NJS_TEX*)0x01095794 = { 147, 4 };
	*(NJS_TEX*)0x01095798 = { 232, 4 };
	*(NJS_TEX*)0x0109579C = { 147, 202 };
	*(NJS_TEX*)0x010957A0 = { 185, 202 };
	*(NJS_TEX*)0x010957A4 = { 147, 4 };
	*(NJS_TEX*)0x010957A8 = { 185, 4 };
	*(NJS_TEX*)0x010957AC = { 191, 203 };
	*(NJS_TEX*)0x010957B0 = { 231, 202 };
	*(NJS_TEX*)0x010957B4 = { 190, 4 };
	*(NJS_TEX*)0x010957B8 = { 232, 4 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a20.nja.sa1mdl
	*(NJS_TEX*)0x01095CA0 = { 104, 4 };
	*(NJS_TEX*)0x01095CA4 = { 104, 202 };
	*(NJS_TEX*)0x01095CA8 = { 13, 4 };
	*(NJS_TEX*)0x01095CAC = { 13, 202 };
	*(NJS_TEX*)0x01095CB0 = { 104, 202 };
	*(NJS_TEX*)0x01095CB4 = { 13, 202 };
	*(NJS_TEX*)0x01095CB8 = { 104, 4 };
	*(NJS_TEX*)0x01095CBC = { 13, 4 };
	*(NJS_TEX*)0x01095CC0 = { 56, 202 };
	*(NJS_TEX*)0x01095CC4 = { 108, 129 };
	*(NJS_TEX*)0x01095CC8 = { 12, 130 };
	*(NJS_TEX*)0x01095CCC = { 104, 202 };
	*(NJS_TEX*)0x01095CD0 = { 13, 202 };
	*(NJS_TEX*)0x01095CD4 = { 104, 4 };
	*(NJS_TEX*)0x01095CD8 = { 13, 4 };
	*(NJS_TEX*)0x01095CDC = { 150, 4 };
	*(NJS_TEX*)0x01095CE0 = { 150, 202 };
	*(NJS_TEX*)0x01095CE4 = { 233, 4 };
	*(NJS_TEX*)0x01095CE8 = { 233, 202 };
	*(NJS_TEX*)0x01095CEC = { 150, 202 };
	*(NJS_TEX*)0x01095CF0 = { 233, 202 };
	*(NJS_TEX*)0x01095CF4 = { 150, 4 };
	*(NJS_TEX*)0x01095CF8 = { 233, 4 };
	*(NJS_TEX*)0x01095CFC = { 190, 202 };
	*(NJS_TEX*)0x01095D00 = { 241, 122 };
	*(NJS_TEX*)0x01095D04 = { 139, 121 };
	*(NJS_TEX*)0x01095D08 = { 150, 202 };
	*(NJS_TEX*)0x01095D0C = { 233, 202 };
	*(NJS_TEX*)0x01095D10 = { 150, 4 };
	*(NJS_TEX*)0x01095D14 = { 233, 4 };
	*(NJS_TEX*)0x01095D18 = { 150, 202 };
	*(NJS_TEX*)0x01095D1C = { 185, 202 };
	*(NJS_TEX*)0x01095D20 = { 150, 4 };
	*(NJS_TEX*)0x01095D24 = { 186, 4 };
	*(NJS_TEX*)0x01095D28 = { 220, 202 };
	*(NJS_TEX*)0x01095D2C = { 185, 202 };
	*(NJS_TEX*)0x01095D30 = { 220, 4 };
	*(NJS_TEX*)0x01095D34 = { 186, 4 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a21.nja.sa1mdl
	*(NJS_TEX*)0x01095FA8 = { 253, -255 };
	*(NJS_TEX*)0x01095FB0 = { 127, -255 };
	*(NJS_TEX*)0x01095FBC = { 127, -255 };
	*(NJS_TEX*)0x01095FC0 = { 253, -255 };
	*(NJS_TEX*)0x01095FCC = { 127, -255 };
	*(NJS_TEX*)0x01095FD0 = { 253, -255 };
	*(NJS_TEX*)0x01095FD4 = { 185, 29 };
	*(NJS_TEX*)0x01095FD8 = { 134, 249 };
	*(NJS_TEX*)0x01095FDC = { 248, 249 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a22.nja.sa1mdl
	*(NJS_TEX*)0x0109612C = { 126, 253 };
	*(NJS_TEX*)0x01096130 = { 252, 253 };
	*(NJS_TEX*)0x01096134 = { 126, -253 };
	*(NJS_TEX*)0x01096138 = { 252, -253 };
	*(NJS_TEX*)0x0109613C = { 126, 253 };
	*(NJS_TEX*)0x01096140 = { 252, 253 };
	*(NJS_TEX*)0x01096144 = { 126, -253 };
	*(NJS_TEX*)0x01096148 = { 252, -253 };
	*(NJS_TEX*)0x0109614C = { 126, 253 };
	*(NJS_TEX*)0x01096150 = { 252, 253 };
	*(NJS_TEX*)0x01096154 = { 126, -253 };
	*(NJS_TEX*)0x01096158 = { 252, -253 };
	*(NJS_TEX*)0x0109615C = { 246, -253 };
	*(NJS_TEX*)0x01096160 = { 132, -253 };
	*(NJS_TEX*)0x01096164 = { 189, -65 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a23.nja.sa1mdl
	*(NJS_TEX*)0x010962B4 = { 127, -255 };
	*(NJS_TEX*)0x010962BC = { 253, -255 };
	*(NJS_TEX*)0x010962C4 = { 253, -255 };
	*(NJS_TEX*)0x010962C8 = { 127, -255 };
	*(NJS_TEX*)0x010962D4 = { 253, -255 };
	*(NJS_TEX*)0x010962D8 = { 127, -255 };
	*(NJS_TEX*)0x010962E4 = { 134, 245 };
	*(NJS_TEX*)0x010962E8 = { 244, 245 };
	*(NJS_TEX*)0x010962EC = { 189, 57 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a24.nja.sa1mdl
	*(NJS_TEX*)0x0109642C = { 251, -253 };
	*(NJS_TEX*)0x01096430 = { 252, 253 };
	*(NJS_TEX*)0x01096434 = { 127, -253 };
	*(NJS_TEX*)0x01096438 = { 127, 253 };
	*(NJS_TEX*)0x0109643C = { 188, -253 };
	*(NJS_TEX*)0x01096440 = { 189, 253 };
	*(NJS_TEX*)0x01096444 = { 127, 253 };
	*(NJS_TEX*)0x01096448 = { 127, -253 };
	*(NJS_TEX*)0x0109644C = { 252, 253 };
	*(NJS_TEX*)0x01096450 = { 251, -253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_a25.nja.sa1mdl
	*(NJS_TEX*)0x010965E0 = { 0, 2 };
	*(NJS_TEX*)0x010965E4 = { 48, 127 };
	*(NJS_TEX*)0x010965E8 = { 124, 44 };
	*(NJS_TEX*)0x010965EC = { 124, 139 };
	*(NJS_TEX*)0x010965F0 = { 57, 70 };
	*(NJS_TEX*)0x010965F4 = { 0, 2 };
	*(NJS_TEX*)0x010965F8 = { 124, 93 };
	*(NJS_TEX*)0x010965FC = { 124, 2 };
	*(NJS_TEX*)0x01096600 = { 124, 2 };
	*(NJS_TEX*)0x01096604 = { 124, 93 };
	*(NJS_TEX*)0x01096608 = { 100, 2 };
	*(NJS_TEX*)0x0109660C = { 100, 123 };
	*(NJS_TEX*)0x01096610 = { 61, 2 };
	*(NJS_TEX*)0x01096614 = { 69, 91 };
	*(NJS_TEX*)0x01096618 = { 38, 2 };
	*(NJS_TEX*)0x0109661C = { 61, 93 };
	*(NJS_TEX*)0x01096620 = { 50, 2 };
	*(NJS_TEX*)0x01096624 = { 112, 109 };
	*(NJS_TEX*)0x01096628 = { 112, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b.nja.sa1mdl
	*(NJS_TEX*)0x01096E50 = { 96, -251 };
	*(NJS_TEX*)0x01096E58 = { 34, -251 };
	*(NJS_TEX*)0x01096E64 = { 34, -251 };
	*(NJS_TEX*)0x01096E6C = { 96, -251 };
	*(NJS_TEX*)0x01096E74 = { 34, -251 };
	*(NJS_TEX*)0x01096E7C = { 96, -251 };
	*(NJS_TEX*)0x01096E84 = { 34, -251 };
	*(NJS_TEX*)0x01096E8C = { 96, -251 };
	*(NJS_TEX*)0x01096E94 = { 34, -251 };
	*(NJS_TEX*)0x01096E9C = { 96, -251 };
	*(NJS_TEX*)0x01096EA4 = { 34, -251 };
	*(NJS_TEX*)0x01096EAC = { 96, -251 };
	*(NJS_TEX*)0x01096EB0 = { 0, -255 };
	*(NJS_TEX*)0x01096EB4 = { 125, -255 };
	*(NJS_TEX*)0x01096EC4 = { 0, -255 };
	*(NJS_TEX*)0x01096EC8 = { 20, 253 };
	*(NJS_TEX*)0x01096ECC = { 20, -253 };
	*(NJS_TEX*)0x01096ED4 = { 0, -255 };
	*(NJS_TEX*)0x01096ED8 = { 20, 253 };
	*(NJS_TEX*)0x01096EDC = { 20, -253 };
	*(NJS_TEX*)0x01096EE0 = { 127, 1 };
	*(NJS_TEX*)0x01096EE4 = { 253, 1 };
	*(NJS_TEX*)0x01096EF0 = { 127, 1 };
	*(NJS_TEX*)0x01096EF4 = { 253, 1 };
	*(NJS_TEX*)0x01096F00 = { 127, 1 };
	*(NJS_TEX*)0x01096F04 = { 253, 1 };
	*(NJS_TEX*)0x01096F10 = { 253, 1 };
	*(NJS_TEX*)0x01096F18 = { 127, 1 };
	*(NJS_TEX*)0x01096F20 = { 127, 1 };
	*(NJS_TEX*)0x01096F24 = { 253, 1 };
	*(NJS_TEX*)0x01096F30 = { 127, 1 };
	*(NJS_TEX*)0x01096F34 = { 253, 1 };
	*(NJS_TEX*)0x01096F48 = { 253, 1 };
	*(NJS_TEX*)0x01096F4C = { 127, 1 };
	*(NJS_TEX*)0x01096F58 = { 253, 1 };
	*(NJS_TEX*)0x01096F5C = { 127, 1 };
	*(NJS_TEX*)0x01096F68 = { 253, 1 };
	*(NJS_TEX*)0x01096F6C = { 127, 1 };
	*(NJS_TEX*)0x01096F74 = { 127, 1 };
	*(NJS_TEX*)0x01096F7C = { 253, 1 };
	*(NJS_TEX*)0x01096F88 = { 253, 1 };
	*(NJS_TEX*)0x01096F8C = { 127, 1 };
	*(NJS_TEX*)0x01096F98 = { 253, 1 };
	*(NJS_TEX*)0x01096F9C = { 127, 1 };
	*(NJS_TEX*)0x01096FA0 = { 127, -255 };
	*(NJS_TEX*)0x01096FA4 = { 253, -255 };
	*(NJS_TEX*)0x01096FB4 = { 127, -255 };
	*(NJS_TEX*)0x01096FB8 = { 147, 257 };
	*(NJS_TEX*)0x01096FBC = { 147, -255 };
	*(NJS_TEX*)0x01096FC4 = { 127, -255 };
	*(NJS_TEX*)0x01096FC8 = { 147, 257 };
	*(NJS_TEX*)0x01096FCC = { 147, -255 };
	*(NJS_TEX*)0x01096FD0 = { 0, -255 };
	*(NJS_TEX*)0x01096FD4 = { 125, -255 };
	*(NJS_TEX*)0x01096FE4 = { 0, -255 };
	*(NJS_TEX*)0x01096FEC = { 15, -253 };
	*(NJS_TEX*)0x01096FF4 = { 0, -255 };
	*(NJS_TEX*)0x01096FFC = { 15, -253 };
	*(NJS_TEX*)0x01097000 = { 127, -255 };
	*(NJS_TEX*)0x01097004 = { 253, -255 };
	*(NJS_TEX*)0x01097014 = { 127, -255 };
	*(NJS_TEX*)0x01097018 = { 147, 257 };
	*(NJS_TEX*)0x0109701C = { 147, -255 };
	*(NJS_TEX*)0x01097024 = { 127, -255 };
	*(NJS_TEX*)0x01097028 = { 147, 257 };
	*(NJS_TEX*)0x0109702C = { 147, -255 };
	*(NJS_TEX*)0x01097034 = { 0, -255 };
	*(NJS_TEX*)0x0109703C = { 15, -253 };
	*(NJS_TEX*)0x01097044 = { 0, -255 };
	*(NJS_TEX*)0x0109704C = { 15, -253 };
	*(NJS_TEX*)0x01097050 = { 0, -255 };
	*(NJS_TEX*)0x01097054 = { 125, -255 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b01.nja.sa1mdl
	*(NJS_TEX*)0x01098010 = { 124, -121 };
	*(NJS_TEX*)0x01098014 = { 0, -121 };
	*(NJS_TEX*)0x01098018 = { 124, -253 };
	*(NJS_TEX*)0x0109801C = { 0, -253 };
	*(NJS_TEX*)0x01098020 = { 126, -253 };
	*(NJS_TEX*)0x01098024 = { 126, 253 };
	*(NJS_TEX*)0x01098028 = { 106, -253 };
	*(NJS_TEX*)0x0109802C = { 106, 253 };
	*(NJS_TEX*)0x01098030 = { 124, 125 };
	*(NJS_TEX*)0x01098034 = { 0, 129 };
	*(NJS_TEX*)0x01098038 = { 122, -21 };
	*(NJS_TEX*)0x0109803C = { 0, -21 };
	*(NJS_TEX*)0x01098040 = { 124, 253 };
	*(NJS_TEX*)0x01098044 = { 0, 253 };
	*(NJS_TEX*)0x01098048 = { 0, -253 };
	*(NJS_TEX*)0x0109804C = { 22, 253 };
	*(NJS_TEX*)0x01098050 = { 22, -253 };
	*(NJS_TEX*)0x01098054 = { 124, 253 };
	*(NJS_TEX*)0x01098058 = { 124, -253 };
	*(NJS_TEX*)0x0109805C = { 0, -253 };
	*(NJS_TEX*)0x01098060 = { 124, -253 };
	*(NJS_TEX*)0x01098064 = { 0, -253 };
	*(NJS_TEX*)0x01098068 = { 124, 253 };
	*(NJS_TEX*)0x0109806C = { 0, 253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b02.nja.sa1mdl
	*(NJS_TEX*)0x01098228 = { 0, 127 };
	*(NJS_TEX*)0x0109822C = { 0, 254 };
	*(NJS_TEX*)0x01098230 = { 20, 127 };
	*(NJS_TEX*)0x01098234 = { 20, 254 };
	*(NJS_TEX*)0x01098238 = { 0, 254 };
	*(NJS_TEX*)0x0109823C = { 0, 127 };
	*(NJS_TEX*)0x01098240 = { 122, 254 };
	*(NJS_TEX*)0x01098244 = { 104, 165 };
	*(NJS_TEX*)0x01098248 = { 0, 208 };
	*(NJS_TEX*)0x0109824C = { 124, 208 };
	*(NJS_TEX*)0x01098250 = { 0, 127 };
	*(NJS_TEX*)0x01098254 = { 124, 127 };
	*(NJS_TEX*)0x01098258 = { 122, 254 };
	*(NJS_TEX*)0x0109825C = { 73, 153 };
	*(NJS_TEX*)0x01098260 = { 0, 254 };
	*(NJS_TEX*)0x01098264 = { 0, 93 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b03.nja.sa1mdl
	*(NJS_TEX*)0x01098498 = { 0, 2 };
	*(NJS_TEX*)0x0109849C = { 48, 127 };
	*(NJS_TEX*)0x010984A0 = { 124, 44 };
	*(NJS_TEX*)0x010984A4 = { 124, 139 };
	*(NJS_TEX*)0x010984A8 = { 57, 70 };
	*(NJS_TEX*)0x010984AC = { 0, 2 };
	*(NJS_TEX*)0x010984B0 = { 124, 93 };
	*(NJS_TEX*)0x010984B4 = { 124, 2 };
	*(NJS_TEX*)0x010984B8 = { 124, 2 };
	*(NJS_TEX*)0x010984BC = { 124, 93 };
	*(NJS_TEX*)0x010984C0 = { 100, 2 };
	*(NJS_TEX*)0x010984C4 = { 100, 123 };
	*(NJS_TEX*)0x010984C8 = { 61, 2 };
	*(NJS_TEX*)0x010984CC = { 69, 91 };
	*(NJS_TEX*)0x010984D0 = { 38, 2 };
	*(NJS_TEX*)0x010984D4 = { 61, 93 };
	*(NJS_TEX*)0x010984D8 = { 50, 2 };
	*(NJS_TEX*)0x010984DC = { 112, 109 };
	*(NJS_TEX*)0x010984E0 = { 112, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b04.nja.sa1mdl
	*(NJS_TEX*)0x010986F8 = { 124, 40 };
	*(NJS_TEX*)0x010986FC = { 102, 20 };
	*(NJS_TEX*)0x01098700 = { 102, 103 };
	*(NJS_TEX*)0x01098704 = { 124, 93 };
	*(NJS_TEX*)0x01098708 = { 0, 16 };
	*(NJS_TEX*)0x0109870C = { 124, 40 };
	*(NJS_TEX*)0x01098710 = { 0, 170 };
	*(NJS_TEX*)0x01098714 = { 124, 186 };
	*(NJS_TEX*)0x01098718 = { 2, 115 };
	*(NJS_TEX*)0x0109871C = { 0, 170 };
	*(NJS_TEX*)0x01098720 = { 22, 74 };
	*(NJS_TEX*)0x01098724 = { 22, 194 };
	*(NJS_TEX*)0x01098728 = { 0, 12 };
	*(NJS_TEX*)0x0109872C = { 0, 170 };
	*(NJS_TEX*)0x01098730 = { 124, 40 };
	*(NJS_TEX*)0x01098734 = { 124, 186 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b05.nja.sa1mdl
	*(NJS_TEX*)0x010988D8 = { 124, 68 };
	*(NJS_TEX*)0x010988DC = { 0, 2 };
	*(NJS_TEX*)0x010988E0 = { 0, 184 };
	*(NJS_TEX*)0x010988E4 = { 28, 22 };
	*(NJS_TEX*)0x010988E8 = { 0, 184 };
	*(NJS_TEX*)0x010988EC = { 124, 68 };
	*(NJS_TEX*)0x010988F0 = { 0, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b06.nja.sa1mdl
	*(NJS_TEX*)0x01098A60 = { 122, 28 };
	*(NJS_TEX*)0x01098A64 = { 122, 147 };
	*(NJS_TEX*)0x01098A68 = { 101, 82 };
	*(NJS_TEX*)0x01098A6C = { 101, 147 };
	*(NJS_TEX*)0x01098A70 = { 0, 143 };
	*(NJS_TEX*)0x01098A74 = { 0, 2 };
	*(NJS_TEX*)0x01098A78 = { 101, 182 };
	*(NJS_TEX*)0x01098A7C = { 53, 184 };
	*(NJS_TEX*)0x01098A80 = { 124, 2 };
	*(NJS_TEX*)0x01098A84 = { 124, 172 };
	*(NJS_TEX*)0x01098A88 = { 72, 2 };
	*(NJS_TEX*)0x01098A8C = { 93, 174 };
	*(NJS_TEX*)0x01098A90 = { 109, 2 };
	*(NJS_TEX*)0x01098A94 = { 109, 174 };
	*(NJS_TEX*)0x01098A98 = { 109, 127 };
	*(NJS_TEX*)0x01098A9C = { 60, 147 };
	*(NJS_TEX*)0x01098AA0 = { 109, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b07.nja.sa1mdl
	*(NJS_TEX*)0x01098C58 = { 124, -129 };
	*(NJS_TEX*)0x01098C5C = { 0, -129 };
	*(NJS_TEX*)0x01098C60 = { 124, 25 };
	*(NJS_TEX*)0x01098C64 = { 0, 25 };
	*(NJS_TEX*)0x01098C68 = { 108, 253 };
	*(NJS_TEX*)0x01098C6C = { 108, -253 };
	*(NJS_TEX*)0x01098C70 = { 124, 253 };
	*(NJS_TEX*)0x01098C74 = { 108, -253 };
	*(NJS_TEX*)0x01098C78 = { 124, -253 };
	*(NJS_TEX*)0x01098C7C = { 124, 253 };
	*(NJS_TEX*)0x01098C80 = { 0, -253 };
	*(NJS_TEX*)0x01098C84 = { 0, 253 };
	*(NJS_TEX*)0x01098C88 = { 22, 253 };
	*(NJS_TEX*)0x01098C8C = { 124, -29 };
	*(NJS_TEX*)0x01098C90 = { 0, -29 };
	*(NJS_TEX*)0x01098C94 = { 124, 129 };
	*(NJS_TEX*)0x01098C98 = { 0, 129 };
	*(NJS_TEX*)0x01098C9C = { 22, -253 };
	*(NJS_TEX*)0x01098CA0 = { 22, 253 };
	*(NJS_TEX*)0x01098CA4 = { 0, -253 };
	*(NJS_TEX*)0x01098CA8 = { 124, -253 };
	*(NJS_TEX*)0x01098CAC = { 0, -253 };
	*(NJS_TEX*)0x01098CB0 = { 124, 253 };
	*(NJS_TEX*)0x01098CB4 = { 0, 253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b08.nja.sa1mdl
	*(NJS_TEX*)0x01098E60 = { 69, 82 };
	*(NJS_TEX*)0x01098E64 = { 122, 212 };
	*(NJS_TEX*)0x01098E68 = { 122, 113 };
	*(NJS_TEX*)0x01098E6C = { 48, 93 };
	*(NJS_TEX*)0x01098E70 = { 122, 254 };
	*(NJS_TEX*)0x01098E74 = { 122, 93 };
	*(NJS_TEX*)0x01098E78 = { 104, 254 };
	*(NJS_TEX*)0x01098E7C = { 104, 123 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b09.nja.sa1mdl
	*(NJS_TEX*)0x01099078 = { 0, 127 };
	*(NJS_TEX*)0x0109907C = { 0, 254 };
	*(NJS_TEX*)0x01099080 = { 20, 127 };
	*(NJS_TEX*)0x01099084 = { 20, 254 };
	*(NJS_TEX*)0x01099088 = { 0, 254 };
	*(NJS_TEX*)0x0109908C = { 0, 127 };
	*(NJS_TEX*)0x01099090 = { 122, 254 };
	*(NJS_TEX*)0x01099094 = { 104, 165 };
	*(NJS_TEX*)0x01099098 = { 0, 208 };
	*(NJS_TEX*)0x0109909C = { 124, 208 };
	*(NJS_TEX*)0x010990A0 = { 0, 127 };
	*(NJS_TEX*)0x010990A4 = { 124, 127 };
	*(NJS_TEX*)0x010990A8 = { 122, 254 };
	*(NJS_TEX*)0x010990AC = { 73, 153 };
	*(NJS_TEX*)0x010990B0 = { 0, 254 };
	*(NJS_TEX*)0x010990B4 = { 0, 93 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b10.nja.sa1mdl
	*(NJS_TEX*)0x010992A8 = { 124, 68 };
	*(NJS_TEX*)0x010992AC = { 0, 2 };
	*(NJS_TEX*)0x010992B0 = { 0, 184 };
	*(NJS_TEX*)0x010992B4 = { 28, 22 };
	*(NJS_TEX*)0x010992B8 = { 0, 184 };
	*(NJS_TEX*)0x010992BC = { 124, 68 };
	*(NJS_TEX*)0x010992C0 = { 0, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b11.nja.sa1mdl
	*(NJS_TEX*)0x01099430 = { 122, 28 };
	*(NJS_TEX*)0x01099434 = { 122, 147 };
	*(NJS_TEX*)0x01099438 = { 101, 82 };
	*(NJS_TEX*)0x0109943C = { 101, 147 };
	*(NJS_TEX*)0x01099440 = { 0, 143 };
	*(NJS_TEX*)0x01099444 = { 0, 2 };
	*(NJS_TEX*)0x01099448 = { 101, 182 };
	*(NJS_TEX*)0x0109944C = { 53, 184 };
	*(NJS_TEX*)0x01099450 = { 124, 2 };
	*(NJS_TEX*)0x01099454 = { 124, 172 };
	*(NJS_TEX*)0x01099458 = { 72, 2 };
	*(NJS_TEX*)0x0109945C = { 93, 174 };
	*(NJS_TEX*)0x01099460 = { 109, 2 };
	*(NJS_TEX*)0x01099464 = { 109, 174 };
	*(NJS_TEX*)0x01099468 = { 109, 127 };
	*(NJS_TEX*)0x0109946C = { 60, 147 };
	*(NJS_TEX*)0x01099470 = { 109, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b12.nja.sa1mdl
	*(NJS_TEX*)0x01099618 = { 124, 103 };
	*(NJS_TEX*)0x0109961C = { 124, 2 };
	*(NJS_TEX*)0x01099620 = { 0, 2 };
	*(NJS_TEX*)0x01099624 = { 0, 2 };
	*(NJS_TEX*)0x01099628 = { 124, 80 };
	*(NJS_TEX*)0x0109962C = { 124, 2 };
	*(NJS_TEX*)0x01099630 = { 100, 2 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b13.nja.sa1mdl
	*(NJS_TEX*)0x010997A0 = { 124, 202 };
	*(NJS_TEX*)0x010997A4 = { 0, 202 };
	*(NJS_TEX*)0x010997A8 = { 124, 127 };
	*(NJS_TEX*)0x010997AC = { 0, 127 };
	*(NJS_TEX*)0x010997B0 = { 0, 254 };
	*(NJS_TEX*)0x010997B4 = { 0, 184 };
	*(NJS_TEX*)0x010997B8 = { 124, 254 };
	*(NJS_TEX*)0x010997BC = { 124, 68 };
	*(NJS_TEX*)0x010997C0 = { 104, 254 };
	*(NJS_TEX*)0x010997C4 = { 106, 68 };
	*(NJS_TEX*)0x010997C8 = { 124, 254 };
	*(NJS_TEX*)0x010997CC = { 124, 68 };
	*(NJS_TEX*)0x010997D0 = { 20, 184 };
	*(NJS_TEX*)0x010997D4 = { 20, 254 };
	*(NJS_TEX*)0x010997D8 = { 0, 184 };
	*(NJS_TEX*)0x010997DC = { 0, 254 };
	*(NJS_TEX*)0x010997E0 = { 124, 44 };
	*(NJS_TEX*)0x010997E4 = { 124, 254 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b14.nja.sa1mdl
	*(NJS_TEX*)0x010999B8 = { 124, 40 };
	*(NJS_TEX*)0x010999BC = { 102, 20 };
	*(NJS_TEX*)0x010999C0 = { 102, 103 };
	*(NJS_TEX*)0x010999C4 = { 124, 93 };
	*(NJS_TEX*)0x010999C8 = { 0, 16 };
	*(NJS_TEX*)0x010999CC = { 124, 40 };
	*(NJS_TEX*)0x010999D0 = { 0, 170 };
	*(NJS_TEX*)0x010999D4 = { 124, 186 };
	*(NJS_TEX*)0x010999D8 = { 2, 115 };
	*(NJS_TEX*)0x010999DC = { 0, 170 };
	*(NJS_TEX*)0x010999E0 = { 22, 74 };
	*(NJS_TEX*)0x010999E4 = { 22, 194 };
	*(NJS_TEX*)0x010999E8 = { 0, 12 };
	*(NJS_TEX*)0x010999EC = { 0, 170 };
	*(NJS_TEX*)0x010999F0 = { 124, 40 };
	*(NJS_TEX*)0x010999F4 = { 124, 186 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b15.nja.sa1mdl
	*(NJS_TEX*)0x01099BA8 = { 95, 198 };
	*(NJS_TEX*)0x01099BAC = { 95, 4 };
	*(NJS_TEX*)0x01099BB0 = { 33, 198 };
	*(NJS_TEX*)0x01099BB4 = { 33, 4 };
	*(NJS_TEX*)0x01099BB8 = { 95, 4 };
	*(NJS_TEX*)0x01099BBC = { 33, 4 };
	*(NJS_TEX*)0x01099BC0 = { 95, 198 };
	*(NJS_TEX*)0x01099BC4 = { 33, 198 };
	*(NJS_TEX*)0x01099BC8 = { 110, 130 };
	*(NJS_TEX*)0x01099BCC = { 11, 130 };
	*(NJS_TEX*)0x01099BD0 = { 60, 208 };
	*(NJS_TEX*)0x01099BD4 = { 95, 4 };
	*(NJS_TEX*)0x01099BD8 = { 33, 4 };
	*(NJS_TEX*)0x01099BDC = { 95, 198 };
	*(NJS_TEX*)0x01099BE0 = { 33, 198 };
	*(NJS_TEX*)0x01099BE4 = { 33, 251 };
	*(NJS_TEX*)0x01099BE8 = { 61, 251 };
	*(NJS_TEX*)0x01099BEC = { 33, 4 };
	*(NJS_TEX*)0x01099BF0 = { 61, 4 };
	*(NJS_TEX*)0x01099BF4 = { 89, 251 };
	*(NJS_TEX*)0x01099BF8 = { 61, 251 };
	*(NJS_TEX*)0x01099BFC = { 89, 4 };
	*(NJS_TEX*)0x01099C00 = { 61, 4 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b16.nja.sa1mdl
	*(NJS_TEX*)0x01099DC4 = { 124, -253 };
	*(NJS_TEX*)0x01099DC8 = { 124, 253 };
	*(NJS_TEX*)0x01099DCC = { 59, -253 };
	*(NJS_TEX*)0x01099DD0 = { 59, 253 };
	*(NJS_TEX*)0x01099DD4 = { 0, -253 };
	*(NJS_TEX*)0x01099DD8 = { 0, 253 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b17.nja.sa1mdl
	*(NJS_TEX*)0x01099F38 = { 231, 202 };
	*(NJS_TEX*)0x01099F3C = { 232, 4 };
	*(NJS_TEX*)0x01099F40 = { 147, 202 };
	*(NJS_TEX*)0x01099F44 = { 147, 4 };
	*(NJS_TEX*)0x01099F48 = { 147, 202 };
	*(NJS_TEX*)0x01099F4C = { 231, 202 };
	*(NJS_TEX*)0x01099F50 = { 147, 4 };
	*(NJS_TEX*)0x01099F54 = { 232, 4 };
	*(NJS_TEX*)0x01099F58 = { 231, 125 };
	*(NJS_TEX*)0x01099F5C = { 149, 125 };
	*(NJS_TEX*)0x01099F60 = { 187, 203 };
	*(NJS_TEX*)0x01099F64 = { 147, 202 };
	*(NJS_TEX*)0x01099F68 = { 231, 202 };
	*(NJS_TEX*)0x01099F6C = { 147, 4 };
	*(NJS_TEX*)0x01099F70 = { 232, 4 };
	*(NJS_TEX*)0x01099F74 = { 147, 202 };
	*(NJS_TEX*)0x01099F78 = { 185, 202 };
	*(NJS_TEX*)0x01099F7C = { 147, 4 };
	*(NJS_TEX*)0x01099F80 = { 185, 4 };
	*(NJS_TEX*)0x01099F84 = { 191, 203 };
	*(NJS_TEX*)0x01099F88 = { 231, 202 };
	*(NJS_TEX*)0x01099F8C = { 190, 4 };
	*(NJS_TEX*)0x01099F90 = { 232, 4 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b19.nja.sa1mdl
	*(NJS_TEX*)0x0109A180 = { 108, 4 };
	*(NJS_TEX*)0x0109A184 = { 106, 202 };
	*(NJS_TEX*)0x0109A188 = { 22, 4 };
	*(NJS_TEX*)0x0109A18C = { 22, 202 };
	*(NJS_TEX*)0x0109A190 = { 106, 202 };
	*(NJS_TEX*)0x0109A194 = { 22, 202 };
	*(NJS_TEX*)0x0109A198 = { 108, 4 };
	*(NJS_TEX*)0x0109A19C = { 22, 4 };
	*(NJS_TEX*)0x0109A1A0 = { 60, 170 };
	*(NJS_TEX*)0x0109A1A4 = { 19, 90 };
	*(NJS_TEX*)0x0109A1A8 = { 110, 91 };
	*(NJS_TEX*)0x0109A1AC = { 106, 202 };
	*(NJS_TEX*)0x0109A1B0 = { 22, 202 };
	*(NJS_TEX*)0x0109A1B4 = { 108, 4 };
	*(NJS_TEX*)0x0109A1B8 = { 22, 4 };
	*(NJS_TEX*)0x0109A1BC = { 54, 202 };
	*(NJS_TEX*)0x0109A1C0 = { 22, 202 };
	*(NJS_TEX*)0x0109A1C4 = { 55, 4 };
	*(NJS_TEX*)0x0109A1C8 = { 22, 4 };
	*(NJS_TEX*)0x0109A1CC = { 86, 4 };
	*(NJS_TEX*)0x0109A1D0 = { 55, 4 };
	*(NJS_TEX*)0x0109A1D4 = { 86, 202 };
	*(NJS_TEX*)0x0109A1D8 = { 54, 202 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b20.nja.sa1mdl
	*(NJS_TEX*)0x0109A3E0 = { 233, 2 };
	*(NJS_TEX*)0x0109A3E4 = { 233, 205 };
	*(NJS_TEX*)0x0109A3E8 = { 149, 2 };
	*(NJS_TEX*)0x0109A3EC = { 149, 204 };
	*(NJS_TEX*)0x0109A3F0 = { 233, 205 };
	*(NJS_TEX*)0x0109A3F4 = { 149, 204 };
	*(NJS_TEX*)0x0109A3F8 = { 233, 2 };
	*(NJS_TEX*)0x0109A3FC = { 149, 2 };
	*(NJS_TEX*)0x0109A400 = { 191, 190 };
	*(NJS_TEX*)0x0109A404 = { 143, 112 };
	*(NJS_TEX*)0x0109A408 = { 239, 112 };
	*(NJS_TEX*)0x0109A40C = { 233, 205 };
	*(NJS_TEX*)0x0109A410 = { 149, 204 };
	*(NJS_TEX*)0x0109A414 = { 233, 2 };
	*(NJS_TEX*)0x0109A418 = { 149, 2 };
	*(NJS_TEX*)0x0109A41C = { 223, 2 };
	*(NJS_TEX*)0x0109A420 = { 186, 2 };
	*(NJS_TEX*)0x0109A424 = { 223, 205 };
	*(NJS_TEX*)0x0109A428 = { 185, 205 };
	*(NJS_TEX*)0x0109A42C = { 149, 2 };
	*(NJS_TEX*)0x0109A430 = { 186, 2 };
	*(NJS_TEX*)0x0109A434 = { 149, 204 };
	*(NJS_TEX*)0x0109A438 = { 185, 205 };
	*(NJS_TEX*)0x0109A43C = { 149, 2 };
	*(NJS_TEX*)0x0109A440 = { 186, 2 };
	*(NJS_TEX*)0x0109A444 = { 149, 204 };
	*(NJS_TEX*)0x0109A448 = { 185, 205 };
	*(NJS_TEX*)0x0109A44C = { 223, 2 };
	*(NJS_TEX*)0x0109A450 = { 186, 2 };
	*(NJS_TEX*)0x0109A454 = { 223, 205 };
	*(NJS_TEX*)0x0109A458 = { 185, 205 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b21.nja.sa1mdl
	*(NJS_TEX*)0x0109A6C8 = { 253, -255 };
	*(NJS_TEX*)0x0109A6D0 = { 127, -255 };
	*(NJS_TEX*)0x0109A6DC = { 127, -255 };
	*(NJS_TEX*)0x0109A6E0 = { 253, -255 };
	*(NJS_TEX*)0x0109A6EC = { 127, -255 };
	*(NJS_TEX*)0x0109A6F0 = { 253, -255 };
	*(NJS_TEX*)0x0109A6F4 = { 185, 29 };
	*(NJS_TEX*)0x0109A6F8 = { 134, 249 };
	*(NJS_TEX*)0x0109A6FC = { 248, 249 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b22.nja.sa1mdl
	*(NJS_TEX*)0x0109A84C = { 126, 253 };
	*(NJS_TEX*)0x0109A850 = { 252, 253 };
	*(NJS_TEX*)0x0109A854 = { 126, -253 };
	*(NJS_TEX*)0x0109A858 = { 252, -253 };
	*(NJS_TEX*)0x0109A85C = { 126, 253 };
	*(NJS_TEX*)0x0109A860 = { 252, 253 };
	*(NJS_TEX*)0x0109A864 = { 126, -253 };
	*(NJS_TEX*)0x0109A868 = { 252, -253 };
	*(NJS_TEX*)0x0109A86C = { 126, 253 };
	*(NJS_TEX*)0x0109A870 = { 252, 253 };
	*(NJS_TEX*)0x0109A874 = { 126, -253 };
	*(NJS_TEX*)0x0109A878 = { 252, -253 };
	*(NJS_TEX*)0x0109A87C = { 246, -253 };
	*(NJS_TEX*)0x0109A880 = { 132, -253 };
	*(NJS_TEX*)0x0109A884 = { 189, -65 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b23.nja.sa1mdl
	*(NJS_TEX*)0x0109A9D4 = { 127, -255 };
	*(NJS_TEX*)0x0109A9DC = { 253, -255 };
	*(NJS_TEX*)0x0109A9E4 = { 253, -255 };
	*(NJS_TEX*)0x0109A9E8 = { 127, -255 };
	*(NJS_TEX*)0x0109A9F4 = { 253, -255 };
	*(NJS_TEX*)0x0109A9F8 = { 127, -255 };
	*(NJS_TEX*)0x0109AA04 = { 134, 245 };
	*(NJS_TEX*)0x0109AA08 = { 244, 245 };
	*(NJS_TEX*)0x0109AA0C = { 189, 57 };

	// dxpc\stg01_beach\common\models\seaobj_pier_b25.nja.sa1mdl
	*(NJS_TEX*)0x0109AD00 = { 0, 2 };
	*(NJS_TEX*)0x0109AD04 = { 48, 127 };
	*(NJS_TEX*)0x0109AD08 = { 124, 44 };
	*(NJS_TEX*)0x0109AD0C = { 124, 139 };
	*(NJS_TEX*)0x0109AD10 = { 57, 70 };
	*(NJS_TEX*)0x0109AD14 = { 0, 2 };
	*(NJS_TEX*)0x0109AD18 = { 124, 93 };
	*(NJS_TEX*)0x0109AD1C = { 124, 2 };
	*(NJS_TEX*)0x0109AD20 = { 124, 2 };
	*(NJS_TEX*)0x0109AD24 = { 124, 93 };
	*(NJS_TEX*)0x0109AD28 = { 100, 2 };
	*(NJS_TEX*)0x0109AD2C = { 100, 123 };
	*(NJS_TEX*)0x0109AD30 = { 61, 2 };
	*(NJS_TEX*)0x0109AD34 = { 69, 91 };
	*(NJS_TEX*)0x0109AD38 = { 38, 2 };
	*(NJS_TEX*)0x0109AD3C = { 61, 93 };
	*(NJS_TEX*)0x0109AD40 = { 50, 2 };
	*(NJS_TEX*)0x0109AD44 = { 112, 109 };
	*(NJS_TEX*)0x0109AD48 = { 112, 2 };

	// dxpc\stg01_beach\common\models\seaobj_rock.nja.sa1mdl
	((NJS_MATERIAL*)0x0109C700)->attrflags = 0x94282400;
	((NJS_MATERIAL*)0x0109C714)->attrflags = 0x9429A400;
	((NJS_MATERIAL*)0x0109BE18)->attrflags = 0x94B9A400;

	// dxpc\stg01_beach\common\models\seaobj_yashi00.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_yashi01.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_yashi02.nja.sa1mdl
		// Not replaced (DX sorted model)

	// dxpc\stg01_beach\common\models\seaobj_yashi04.nja.sa1mdl
		// Not replaced (DX sorted model)
}