#include "stdafx.h"

void SGeneHook(NJS_ACTION* action, Float frame)
{
	if (IsCameraUnderwater)
		late_z_ofs___ = 100.0f;
	else
		late_z_ofs___ = -28000.0f;
	late_ActionEx(action, frame, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

void Fish_Init()
{
	ReplacePVM("ANKO");
	ReplacePVM("ANKO_T");
	ReplacePVM("COERA");
	ReplacePVM("HAMMER");
	ReplacePVM("UTSUBO");
	ReplacePVM("ISHIDAI");
	ReplacePVM("KAJIKI");
	ReplacePVM("MECHA");
	ReplacePVM("NEW_BB");
	ReplacePVM("RYUGU");
	ReplacePVM("SAKE");
	ReplacePVM("SAME");
	ReplacePVM("SEA_BASS");
	ReplacePVM("PIRANIA");
	ReplacePVM("UNAGI");
	ReplacePVM("TAI");
	WriteCall((void*)0x00596C7C, SGeneHook); // Render mecha fish with depth
	// White diffuse (Mecha Fish)
	AddWhiteDiffuseMaterial(&object_mi_meka_sakana_ms_hontai.child->child->child->sibling->basicdxmodel->mats[3]); // Belly 1
	AddWhiteDiffuseMaterial(&object_mi_meka_sakana_ms_hontai.child->child->child->sibling->basicdxmodel->mats[4]); // Belly 2
	AddWhiteDiffuseMaterial(&object_mi_meka_sakana_ms_hontai.child->child->child->sibling->basicdxmodel->mats[5]); // Belly 3
	// Tail
	AddWhiteDiffuseMaterial(&object_mi_meka_sakana_ms_hontai.child->child->child->child->sibling->basicdxmodel->mats[1]); // Tail 1
	AddWhiteDiffuseMaterial(&object_mi_meka_sakana_ms_hontai.child->child->child->child->sibling->basicdxmodel->mats[2]); // Tail 2	
	// White diffuse (Mecha Fish Mission)
	AddWhiteDiffuseMaterial(&object_meka_sakana_ms_hontai_ms_hontai.child->child->child->sibling->basicdxmodel->mats[3]); // Belly 1
	AddWhiteDiffuseMaterial(&object_meka_sakana_ms_hontai_ms_hontai.child->child->child->sibling->basicdxmodel->mats[4]); // Belly 2
	AddWhiteDiffuseMaterial(&object_meka_sakana_ms_hontai_ms_hontai.child->child->child->sibling->basicdxmodel->mats[5]); // Belly 3
	// Tail
	AddWhiteDiffuseMaterial(&object_meka_sakana_ms_hontai_ms_hontai.child->child->child->child->sibling->basicdxmodel->mats[1]); // Tail 1
	AddWhiteDiffuseMaterial(&object_meka_sakana_ms_hontai_ms_hontai.child->child->child->child->sibling->basicdxmodel->mats[2]); // Tail 2
}