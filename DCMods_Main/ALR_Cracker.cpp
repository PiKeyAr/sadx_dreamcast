#include "stdafx.h"

// This draws the SA1 cracker models while also loading the DX crackers which are used for the confetti effect

 // Load the SADX confetti effect with custom positions
void ObjectAlrCracker_Init_r()
{
	task* obj;
	taskwk* ent;

	flagCrackerStart = 0;
	pLevel = 0; // Reset confetti status
	for (int i = 0;i < 5;i++)
	{
		obj = CreateElementalTask((LoadObj)2, 3, ObjectAlrCracker);
		if (!obj)
			continue;
		ent = obj->twp;
		switch (i)
		{
		case 0:
			ent->pos.x = -103.82f;
			ent->pos.y = 10;
			ent->pos.z = 370.34f;
			ent->ang.x = 0xFFFFFFEE;
			ent->ang.z = 0xFFFFF33D;
			ent->scl.x = 1.5f;
			ent->scl.y = 15;
			break;
		case 1:
			ent = obj->twp;
			ent->pos.x = -86.57f;
			ent->pos.y = 10;
			ent->pos.z = 392.97f;
			ent->ang.x = 0xFFFFEAAB;
			ent->ang.z = 0xFFFFF8E4;
			ent->scl.x = 1.5f;
			ent->scl.y = 10.8f;
			break;
		case 2:
			ent = obj->twp;
			ent->pos.x = -52.01f;
			ent->pos.y = 10;
			ent->pos.z = 384.09f;
			ent->ang.x = 0xFFFFF3F8;
			ent->ang.z = 0x11C7;
			ent->scl.x = 1.6f;
			ent->scl.y = 20.8f;
			break;
		case 3:
			ent = obj->twp;
			ent->pos.x = -67.99f;
			ent->pos.y = 10;
			ent->pos.z = 337.77f;
			ent->ang.x = 0xD77;
			ent->ang.z = 0x11C7;
			ent->scl.x = 1.6f;
			ent->scl.y = 20.8f;
			break;
		case 4:
			ent = obj->twp;
			ent->pos.x = -97.47f;
			ent->pos.y = 10;
			ent->pos.z = 343.87f;
			ent->ang.x = 0x180E;
			ent->ang.y = 0x180E;
			ent->ang.z = 0xFFFFF404;
			ent->scl.x = 1.5f;
			ent->scl.y = 10.8f;
			break;
		}
	}
}

void ChaoRaceCracker_Display(task* a1)
{
	taskwk* v1 = a1->twp;
	njSetTexture(&texlist_obj_al_race);
	njPushMatrix(0);
	njTranslateV(0, &v1->pos);
	njRotateZYX(0, v1->ang.x, v1->ang.y, v1->ang.z);
	njScale(0, 1.0f, 1.0f, 1.0f);
	late_z_ofs___ = -37000;
	late_DrawObjectClip(&object_cr_crak_crak_crak, LATE_WZ, 1.0f);
	njPopMatrix(1u);
	late_z_ofs___ = 0;
}

void ChaoRaceCracker_Main(task* a1)
{
	ChaoRaceCracker_Display(a1);
}

void ChaoRaceCracker_Load(task* a1)
{
	a1->exec = ChaoRaceCracker_Main;
	a1->disp = ChaoRaceCracker_Display;
	a1->dest = FreeTask;
}

void ALR_Cracker_Init()
{
	WriteJump(ObjectAlrCracker_Init, ObjectAlrCracker_Init_r);
}