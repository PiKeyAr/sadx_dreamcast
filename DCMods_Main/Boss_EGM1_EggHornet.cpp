#include "stdafx.h"
#include "Boss_EGM1_EggHornet.h"
#include "FunctionHook.h"

static bool ModelsLoaded_B_EGM1;
static bool SADXWaterDetected;
static bool EggHornetEnvMap = false;

FunctionHook<void> njBeginModel_h(njBeginModel); // Hook to set environment map
FunctionHook<void> njEndModel_h(njEndModel); // Hook to restore environment map
FunctionHook<void> setBoss_egm1_h(setBoss_egm1); // Hook to enable njBeginModel_h/njEndModel_h
FunctionHook<void, task*> TaskBossEnd_h(TaskBossEnd); // Hook to disable njBeginModel_h/njEndModel_h

// Trampoline to fix Egg Hornet rotation
static Trampoline* Egm1Fly_t = nullptr;
static void __cdecl Egm1Fly_r(task* tp, taskwk* twp)
{
	const auto original = TARGET_DYNAMIC(Egm1Fly);
	original(tp, twp);
	if (tp->awp[3].work.ul[2] == 15)
		twp->ang.y = tp->awp[8].work.ul[1] + unsigned int(1820 * njCos(tp->awp[7].work.sl[2]));
}

// Hooked function: set environment map
void njBeginModel_r()
{
	njBeginModel_h.Original();
	if (EggHornetEnvMap && njds_texList == &texlist_egm1)
		ScaleEnvironmentMap(2.0f, 2.0f, 0.5f, 0.5f);
}

// Hooked function: restore environment map
void njEndModel_r()
{
	njEndModel_h.Original();
	if (EggHornetEnvMap)
		ScaleEnvironmentMap(0.5f, 0.5f, 0.5f, 0.5f);
}

// Enable the environment map hooks when the Egg Hornet task is created
void setBoss_egm1_r()
{
	setBoss_egm1_h.Original();
	EggHornetEnvMap = true;
}

// Disable the environment map hooks when the Egg Hornet task is deleted
void TaskBossEnd_r(task* tp)
{
	TaskBossEnd_h.Original(tp);
	EggHornetEnvMap = false;
}

void B_EGM1_Init()
{
	ReplaceSET("SETEGM1S");
	ReplacePVM("EGM1");
	ReplacePVM("EGM1LAND");
	ReplacePVM("EGM1BARRIER");
	ReplacePVM("EGM1EGGMAN");
	ReplacePVM("EGM1JET");
	ReplacePVM("EGM1JETB");
	ReplacePVM("EGM1MIS");
	ReplacePVM("EGM1SORA");
	ReplacePVM("EGM1TSUCHI");
	ReplacePVR("MRASC_016S_HIRUUMI");
	ReplacePVR("MRASC_256S_HIRUSORAA");
	for (int i = 0; i < 3; i++)
	{
		pClipMap_Egm01[0][i].f32Far = -12500.0f;
		pFogTable_Egm01[0][i].f32EndZ = -20000.0f;
		pFogTable_Egm01[0][i].f32StartZ = -6000.0f;
		pFogTable_Egm01[0][i].u8Enable = true;
		pFogTable_Egm01[0][i].Col = 0xFFA0A0A0;
	}
	njBeginModel_h.Hook(njBeginModel_r);
	njEndModel_h.Hook(njEndModel_r);
}

void B_EGM1_Load()
{
	LevelLoader(LevelAndActIDs_EggHornet, "SYSTEM\\data\\bossegm1\\landtableegm1.c.sa1lvl", &texlist_egm1land);
	if (!ModelsLoaded_B_EGM1)
	{
		Egm1Fly_t = new Trampoline(0x00573790, 0x00573795, Egm1Fly_r); // Fix rotation
		setBoss_egm1_h.Hook(setBoss_egm1_r); // Enable hook to enable env map hooks
		TaskBossEnd_h.Hook(TaskBossEnd_r); // Enable hook to disable env map hooks
		if (!CheckSADXWater(LevelIDs_EggHornet)) // Disable SADX water
			WriteData<5>((void*)0x0057192A, 0x90u);
		// Egg Hornet model stuff
		ForceLevelSpecular_Object(&object_egm01_egm01_m1_bw01, false);
		ForceLevelSpecular_Object(&object_egm01_egm01_m1_m02, false);
		ForceLevelSpecular_Object(&object_egm01_egm01_m1_m01, false);
		object_egm01_egm01_hari2.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT; // Drills
		object_egm01_egm01_hari1.basicdxmodel->mats[2].attrflags |= NJD_FLAG_IGNORE_LIGHT; // Drills
		object_egm01_egm01_egm01.basicdxmodel->mats[18].attrflags |= NJD_FLAG_IGNORE_LIGHT; // Front light
		object_egm01_egm01_egm01.basicdxmodel->mats[8].attrflags |= NJD_FLAG_IGNORE_LIGHT; // Computer
		ForceObjectSpecular_Object(&object_egm01_egm01_egm01, false);
		AddWhiteDiffuseMaterial(&object_egm01_egm01_hari2.basicdxmodel->mats[2]); // Drill white diffuse
		ModelsLoaded_B_EGM1 = true;
	}
}