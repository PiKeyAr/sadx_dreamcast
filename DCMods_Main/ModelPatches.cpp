#include "stdafx.h"

void PatchModels_STG01();
void PatchModels_STG02();
void PatchModels_STG03();
void PatchModels_STG04();
void PatchModels_STG05();
void PatchModels_STG06();
void PatchModels_STG07();
void PatchModels_STG08();
void PatchModels_STG09();
void PatchModels_STG10();
void PatchModels_STG12();
void PatchModels_ADV00();
void PatchModels_ADV0100();
void PatchModels_ADV0130();
void PatchModels_ADV02();
void PatchModels_ADV03();
void PatchModels_ShareObj();
void PatchModels_B_EGM3();

void PatchModels(LevelIDs level)
{
	// Sky Chase is separate because it doesn't have a LandTable so this function is not called
	switch (level)
	{
	case LevelIDs_EmeraldCoast:
		PatchModels_STG01();
		break;
	case LevelIDs_WindyValley:
		PatchModels_STG02();
		break;
	case LevelIDs_TwinklePark:
		PatchModels_STG03();
		PatchModels_ShareObj();
		break;
	case LevelIDs_TwinkleCircuit:
		PatchModels_ShareObj();
		break;
	case LevelIDs_SpeedHighway:
		PatchModels_STG04();
		break;
	case LevelIDs_RedMountain:
		PatchModels_STG05();
		break;
	case LevelIDs_SkyDeck:
		PatchModels_STG06();
		break;
	case LevelIDs_LostWorld:
		PatchModels_STG07();
		break;
	case LevelIDs_IceCap:
		PatchModels_STG08();
		break;
	case LevelIDs_Casinopolis:
		PatchModels_STG09();
		break;
	case LevelIDs_FinalEgg:
		PatchModels_STG10();
		break;
	case LevelIDs_HotShelter:
		PatchModels_STG12();
		break;
	case LevelIDs_EggViper:
		PatchModels_B_EGM3();
		break;
	case LevelIDs_StationSquare:
		PatchModels_ADV00();
		break;
	case LevelIDs_EggCarrierOutside:
		PatchModels_ADV0100();
		break;
	case LevelIDs_EggCarrierInside:
		PatchModels_ADV0130();
		break;
	case LevelIDs_MysticRuins:
		PatchModels_ADV02();
		break;
	case LevelIDs_Past:
		PatchModels_ADV03();
		break;
	}
}