#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_MODEL_SADX, model_casi_banp_banp, 0x01D98CE8);
DataPointer(NJS_MODEL_SADX, model_casi_banp_k_banp_k, 0x01D99790);
DataPointer(NJS_MODEL_SADX, model_card_obj_yajirusi_a_yajirusi_a, 0x01DCF690);
DataPointer(NJS_MODEL_SADX, model_card_obj_yajirusi_b_yajirusi_b, 0x01DCF970);
DataPointer(NJS_MODEL_SADX, model_card_obj_yajirusi_c_yajirusi_c, 0x01DCFC50);
DataPointer(NJS_MODEL_SADX, model_card_obj_yajirusi_d_yajirusi_d, 0x01DCFF30);
DataPointer(NJS_MODEL_SADX, model_csn_kinka_kinka, 0x01E0D048);
DataPointer(NJS_MODEL_SADX, model_cas_obj_ten_monitor_ten_monitor, 0x01E46F30);
DataPointer(NJS_OBJECT, object_cas_wt_2f_mizu_2f_mizu, 0x01D798D4);

FunctionPointer(void, cNormal_7, (task* ctp), 0x005E11A0); // Pinball jackpot sprite

void Cowgirl_Init();