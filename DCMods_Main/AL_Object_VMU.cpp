#include "stdafx.h"

NJS_OBJECT* ChaoVMU = nullptr;

static int VMUFrame = 0; // Chao VMU animation

DataArray(CCL_INFO, colli_info_21, 0x033D0B50, 9); // Name Machine collision
TaskFunc(ALO_OdekakeMachine, 0x00729EE0); // Chao Name Machine load function

void SetChaoObjectTexlist();

// Chao Name Machine load function
void __cdecl SetChaoNameMachineCollision(task* tp, CCL_INFO* collisionArray, int count, unsigned __int8 list)
{
	CCL_Init(tp, collisionArray, count, list);
	taskwk* twp = tp->twp;
	NJS_OBJECT* v5 = SetModelCollision(ChaoVMU, tp, (SurfaceFlags)0x20001001);
	v5->scl[0] = 1.0f;
	v5->scl[1] = 1.0f;
	v5->scl[2] = 1.0f;
}

// Hook to create the Name Machine at specific coordinates
void ALO_OdekakeMachineCreate_r(NJS_VECTOR* position, int yrotation)
{
	taskwk* ent = CreateElementalTask(LoadObj_Data1, 2, ALO_OdekakeMachine)->twp;
	switch (ChaoStageNumber)
	{
	case CHAO_STG_SS:
		ent->pos.x = 178.03f;
		ent->pos.y = 8.56f;
		ent->pos.z = -128.44f;
		ent->ang.y = 0xD7B8;
		break;
	case CHAO_STG_EC:
		ent->pos.x = 131.67f;
		ent->pos.y = ModConfig::EnabledLevels[LevelIDs_ECGarden] ? 2.6f : 0.0f;
		ent->pos.z = -204.28f;
		ent->ang.x = 0xFFB0;
		ent->ang.y = 0xAFD6;
		ent->ang.z = 0xFFDE;
		break;
	case CHAO_STG_MR:
		ent->pos.x = 239.4137f;
		ent->pos.y = 15.10273f;
		ent->pos.z = -45.98477f;
		ent->ang.x = 0xFFDC;
		ent->ang.y = 0xC1A8;
		ent->ang.z = 0xFFF2;
		break;
	default:
		break;
	}
}

void AL_VMU_Load()
{
	ChaoVMU = LoadModel("system\\data\\chao\\data\\dx\\object_common\\gneut_obj_vms_body.nja.sa1mdl"); // Edited model
	ChaoVMU->child->sibling->basicdxmodel->nbMeshset = 0; // Disable the button because it needs to be excluded from collision
	object_gneut_obj_vms_button_button.basicdxmodel = LoadModel("system\\data\\chao\\data\\dx\\object_common\\gneut_obj_vms_body.nja.sa1mdl")->child->sibling->basicdxmodel; // Button model
	object_gneut_obj_vms_button_button.pos[0] = 0.003933f;
	object_gneut_obj_vms_button_button.pos[1] = 4.179999f;
	object_gneut_obj_vms_button_button.pos[2] = 5.605592f;
	object_gneut_obj_vms_button_button.scl[0] = 1.0f;
	object_gneut_obj_vms_button_button.scl[1] = 1.0f;
	object_gneut_obj_vms_button_button.scl[2] = 1.0f;
	WriteData((NJS_OBJECT**)0x33D0D0C, ChaoVMU);
	WriteData((NJS_OBJECT**)0x33D0D10, ChaoVMU);
	WriteData((NJS_OBJECT**)0x33D0D14, ChaoVMU);
	// Prevent endless jumping in MR garden with the DC model for the Name Machine (required for DX MR garden too)
	AL_StartPosMR_Odekake.pos.x = 219;
	AL_StartPosMR_Odekake.pos.y = 15.45f;
	AL_StartPosMR_Odekake.pos.z = -48.5f;
	WriteCall((void*)0x00729EEF, SetChaoNameMachineCollision);
	WriteCall((void*)0x00729DE9, SetChaoObjectTexlist);
	WriteData<5>((void*)0x00729EBB, 0x90); // Kill SADX Name Machine screen thing
	WriteJump((void*)0x00729F40, ALO_OdekakeMachineCreate_r);
	colli_info_21[0].center.z = 5;
	for (int i = 1; i < 9; i++)
	{
		colli_info_21[i].a = 0;
		colli_info_21[i].b = 0;
		colli_info_21[i].c = 0;
	}
}

void AL_VMU_OnFrame()
{
	if (ChaoStageNumber >= CHAO_STG_SS && ChaoStageNumber <= CHAO_STG_MR)
	{
		if (!ChkPause())
		{
			if (VMUFrame > 4)
				VMUFrame = 0;
			ChaoVMU->child->basicdxmodel->mats[1].attr_texId = VMUFrame;
			if (ulGlobalTimer % 120 == 0)
				VMUFrame++;
		}
	}
}