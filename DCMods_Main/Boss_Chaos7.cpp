#include "stdafx.h"
#include "Boss_Chaos7.h"

static bool ModelsLoaded_B_CHAOS7;

static float TornadoAlpha = 1.0f;
static int TornadoTrigger = 0;

NJS_OBJECT *TornadoAttack1 = nullptr;
NJS_OBJECT *TornadoAttack2 = nullptr;
NJS_OBJECT *TornadoAttack3 = nullptr;

NJS_TEXANIM anim_Chaos7Damage_copy[32];
NJS_TEXANIM anim_Chaos7SDamage_copy[32];

// Initialize the arrays used in the damage sprite fix
void PerfectChaosDamageFix_Init()
{
	for (int i = 0; i < 32; i++)
	{
		memcpy(&anim_Chaos7Damage_copy[i], &anim_Chaos7Damage, sizeof(NJS_TEXANIM));
		memcpy(&anim_Chaos7SDamage_copy[i], &anim_Chaos7SDamage, sizeof(NJS_TEXANIM));
		anim_Chaos7Damage_copy[i].texid = i;
		anim_Chaos7SDamage_copy[i].texid = i;
	}
}

// Draw the explosion sprite using a static array of NJS_TEXANIM instead of using the sprite's
void Chaos7Explosion_DisplayX(task* a1)
{
	taskwk* twp = a1->twp;
	int id = (int)twp->counter.f;
	if (!loop_count)
	{
		___njFogDisable();
		SetMaterialAndSpriteColor(&argb_18);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_ONE);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		sprite_Chaos7Damage.tanim = anim_Chaos7Damage_copy;
		late_DrawSprite3D(&sprite_Chaos7Damage, id, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE, LATE_LIG);
		njPopMatrix(1u);
		___njFogEnable();
	}
}

// Draw the damage sprite using a static array of NJS_TEXANIM instead of using the sprite's
void Chaos7Damage_DisplayX(task* a1)
{
	taskwk* twp = a1->twp;
	__int16 id = twp->timer.w[1] + 16;
	if (!loop_count)
	{
		___njFogDisable();
		SetMaterialAndSpriteColor(&argb_19);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_ONE);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		sprite_Chaos7SDamage.tanim = anim_Chaos7SDamage_copy;
		late_DrawSprite3D(&sprite_Chaos7SDamage, id, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE, LATE_LIG);
		njPopMatrix(1u);
		___njFogEnable();
	}
}

// Queue hack: Waterfall
void PerfectChaosWaterfallHook(NJS_OBJECT *a1, LATE a2, float a3)
{
	DrawObjectClipMesh(a1, LATE_MAT, a3);
}

// Draw a sorted tornado model
void Chaos7TornadoAttackHook(NJS_ACTION *action, float frame, LATE flags, float scale)
{
	late_z_ofs___ = 2000.0f;
	late_DrawMotionClipMesh(TornadoAttack1, action->motion, frame, LATE_MAT, scale);
	late_z_ofs___ = 3000.0f;
	late_DrawMotionClipMesh(TornadoAttack2, action->motion, frame, LATE_MAT, scale);
	late_z_ofs___ = 4000.0f;
	late_DrawMotionClipMesh(TornadoAttack3, action->motion, frame, LATE_MAT, scale);
	late_z_ofs___ = 0.0f;
}

// Depth hack: Breath attack
void PerfectChaosBreathFix2(NJS_OBJECT *a1, float scale)
{
	late_z_ofs___ = 50.0f;
	late_DrawObjectClip(a1, LATE_MAT, scale);
}

void B_CHAOS7_Init()
{
	ReplaceSET("SET1900S");
	ReplaceCAM("CAM1900S");
	ReplacePVM("CHAOS7_0");
	ReplacePVM("CHAOS7_0BREATH");
	ReplacePVM("CHAOS7_0BREATH2ND");
	ReplacePVM("CHAOS7_0DAMAGE");
	ReplacePVM("CHAOS7_0DEAD_PTCHG");
	ReplacePVM("CHAOS7_0SURFACE");
	ReplacePVM("CHAOS7_0WATEREXP");
	ReplacePVM("CHAOS7_0WEXP_PTCHG");
	ReplacePVM("LM_CHAOS7_0");
	ReplacePVM("OBJ_CHAOS7");
	ReplacePVM("LAST1A_HIGHWAY_A");
	ReplacePVM("EFF_WT_COLUMN");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Chaos07[0][i].f32StartZ = -6000.0f;
		pFogTable_Chaos07[0][i].f32EndZ = -15000.0f;
		pFogTable_Chaos07[0][i].u8Enable = true;
		pFogTable_Chaos07[0][i].Col = 0xFF19CED3;
		pFogTable_Chaos07[1][i].f32StartZ = -6000.0f;
		pFogTable_Chaos07[1][i].f32EndZ = -15000.0f;
		pFogTable_Chaos07[1][i].u8Enable = true;
		pFogTable_Chaos07[1][i].Col = 0xFF19CED3;
		pClipMap_Chaos07[0][i].f32Far = -6500.0;
		pClipMap_Chaos07[1][i].f32Far = -6500.0;
	}
}

void B_CHAOS7_Load()
{
	LevelLoader(LevelAndActIDs_PerfectChaos, "SYSTEM\\data\\boss_chaos7\\landtable1900.c.sa1lvl", &texlist_lm_chaos7_0);
	if (!ModelsLoaded_B_CHAOS7)
	{
		PerfectChaosDamageFix_Init();
		// Fix material flags on Chaos' models for palette selection
		ForceObjectSpecular_Object(&object_c7_kihon_chaos7_chaos7, true);
		ForceObjectSpecular_Object(&object_c7_shape_c7shape_body_c7shape_body, true);
		WriteCall((void*)0x56463B, PerfectChaosWaterfallHook);
		// Fix fade-in for tornadoes, eyes and brain
		WriteData<5>((char*)0x00562229, 0x90u); // Disable Control3D
		WriteData<5>((char*)0x00562242, 0x90u); // Disable constant material
		// Tornado attack fix
		WriteCall((void*)0x00566673, Chaos7TornadoAttackHook);
		TornadoAttack1 = LoadModel("system\\data\\boss_chaos7\\act01\\chaos\\c7_c7_tatumaki.nja.sa1mdl");
		TornadoAttack2 = CloneObject(TornadoAttack1);
		TornadoAttack3 = CloneObject(TornadoAttack1);
		// 1 is the middle layer
		HideMesh_Object(TornadoAttack1, 0);
		HideMesh_Object(TornadoAttack1->child, 0, 1);
		HideMesh_Object(TornadoAttack1->child->sibling->sibling, 0);
		// 2 is the top layer
		HideMesh_Object(TornadoAttack2, 0);
		HideMesh_Object(TornadoAttack2->child->sibling, 0, 1);
		HideMesh_Object(TornadoAttack2->child->sibling->sibling, 0);
		// 3 is the front layer
		HideMesh_Object(TornadoAttack3, 0);
		HideMesh_Object(TornadoAttack3->child, 0, 1);
		HideMesh_Object(TornadoAttack3->child->sibling, 0, 1);		
		// Perfect Chaos breath fix
		WriteData((float*)0x00566A03, 1.0f); // Z Scale?
		WriteCall((void*)0x005660BE, PerfectChaosBreathFix2);
		// Perfect Chaos damage functions
		WriteJump((void*)0x005632F0, Chaos7Explosion_DisplayX);
		WriteJump((void*)0x005633C0, Chaos7Damage_DisplayX);
		// Perfect Chaos misc
		object_ecff_bf_s_fbody_bf_s_fbodyc.basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR; // Egg Carrier 2
		// Objects
		object_last1a_m_way1a_m_way1a = *LoadModel("system\\data\\boss_chaos7\\object\\models\\last1a_m_way1a.nja.sa1mdl"); // ORoad1
		object_last1a_m_way2a_m_way2a = *LoadModel("system\\data\\boss_chaos7\\object\\models\\last1a_m_way2a.nja.sa1mdl"); // ORoad2
		object_last1a_m_way3a_m_way3a = *LoadModel("system\\data\\boss_chaos7\\object\\models\\last1a_m_way3a.nja.sa1mdl"); // ORoad3
		object_last1a_m_way4a_m_way4a = *LoadModel("system\\data\\boss_chaos7\\object\\models\\last1a_m_way4a.nja.sa1mdl"); // ORoad4
		ModelsLoaded_B_CHAOS7 = true;
	}
}