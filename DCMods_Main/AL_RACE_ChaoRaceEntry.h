#pragma once

#include <SADXModLoader.h>

extern bool SkipSA1Entry;
void LoadObjects_RaceEntry();
void ChaoRaceNumbers_Load(task* tp);

struct RaceLettersParam
{
	PatChngObj* pattern;
	int param;
	void* offset_texid;
	void* offset_pattern;
	unsigned __int16 num_pattern;
	unsigned __int16 speedmaybe;
};

TaskFunc(dispObject_12, 0x0071CE80); // Chao Race Entry button display function

DataArray(NJS_MATERIAL, matlist_entranse_nc_neon_a_nc_neon_a, 0x033AD2B0, 8);
DataArray(NJS_MATERIAL, matlist_entranse_nc_neon_b_nc_neon_b, 0x033AD750, 8);
DataArray(NJS_MATERIAL, matlist_entranse_nc_neon_c_nc_neon_c, 0x033ADBF0, 8);
DataPointer(NJS_ACTION, action_holl_open_hg_center, 0x033B6EC8);
DataArray(unsigned __int16, NeonTexId_aa, 0x0088A720, 16);
DataArray(unsigned __int8, NeonPattern_a, 0x0088A740, 71);

PatChngObj PatternRaceEntry[] = {
{
	&object_entranse_nc_neon_a_nc_neon_a, {
	&matlist_entranse_nc_neon_a_nc_neon_a[0],
	&matlist_entranse_nc_neon_a_nc_neon_a[1],
	&matlist_entranse_nc_neon_a_nc_neon_a[2],
	&matlist_entranse_nc_neon_a_nc_neon_a[3],
	&matlist_entranse_nc_neon_a_nc_neon_a[6],
	&matlist_entranse_nc_neon_a_nc_neon_a[7],
	&matlist_entranse_nc_neon_a_nc_neon_a[5],
	&matlist_entranse_nc_neon_a_nc_neon_a[4]}
},
{	&object_entranse_nc_neon_b_nc_neon_b, {
	&matlist_entranse_nc_neon_b_nc_neon_b[0],
	&matlist_entranse_nc_neon_b_nc_neon_b[1],
	&matlist_entranse_nc_neon_b_nc_neon_b[2],
	&matlist_entranse_nc_neon_b_nc_neon_b[3],
	&matlist_entranse_nc_neon_b_nc_neon_b[6],
	&matlist_entranse_nc_neon_b_nc_neon_b[7],
	&matlist_entranse_nc_neon_b_nc_neon_b[5],
	&matlist_entranse_nc_neon_b_nc_neon_b[4]}
},
{
	&object_entranse_nc_neon_c_nc_neon_c, {
	&matlist_entranse_nc_neon_c_nc_neon_c[0],
	&matlist_entranse_nc_neon_c_nc_neon_c[1],
	&matlist_entranse_nc_neon_c_nc_neon_c[2],
	&matlist_entranse_nc_neon_c_nc_neon_c[3],
	&matlist_entranse_nc_neon_c_nc_neon_c[6],
	&matlist_entranse_nc_neon_c_nc_neon_c[7],
	&matlist_entranse_nc_neon_c_nc_neon_c[5],
	&matlist_entranse_nc_neon_c_nc_neon_c[4]}
} };

RaceLettersParam RaceEntryParam = { PatternRaceEntry, 3, &NeonTexId_aa, &NeonPattern_a, 71, 10 };