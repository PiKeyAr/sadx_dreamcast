#include "stdafx.h"

NJS_MODEL_SADX* ChaoGardenTransporterEffect = nullptr;

DataArray(CCL_INFO, WarpColliInfo, 0x007F8B90, 4); // EC transporter collision
TaskFunc(EffectWarpDisplayer, 0x00729390);
TaskFunc(EffectWarpExecutor, 0x00729300);

// Sets the CHAO_OBJECT texlist instead of AL_DX_OBJ_CMN for the transporter model
void SetChaoObjectTexlist()
{
	njSetTexture(&texlist_chao_object);
}

// Rainbow effect display function (DX has the code but doesn't render the model)
void EffectWarpDraw_r(taskwk* twp)
{
	Angle angy; // eax
	float alpha; // ST18_4
	float sx; // [esp+0h] [ebp-Ch]
	float sy; // [esp+4h] [ebp-8h]
	alpha = twp->scl.z;
	sx = twp->scl.x * 0.7f;
	sy = twp->scl.y * 4.5f;
	SaveConstantAttr();
	njSetTexture(&texlist_chao_object);
	OnConstantAttr(0, NJD_FLAG_USE_ALPHA);
	njPushMatrix(0);
	SetMaterial(alpha, alpha, alpha, alpha);
	njTranslateV(0, &twp->pos);
	angy = twp->ang.y;
	if (angy)
		njRotateY(0, (unsigned __int16)angy);
	njScale(0, sx, sy, sx);
	late_z_ofs___ = 3000.0f;
	DrawModelMesh(ChaoGardenTransporterEffect, LATE_LIG);
	late_z_ofs___ = 0;
	njPopMatrix(1u);
	LoadConstantAttr();
	ResetMaterial();
}

// Rainbow effect main function
void EffectWarpExecutor_r(task* tp)
{
	taskwk* twp=tp->twp;

	if (!twp->mode)
	{
		twp->ang.y += 384;
		twp->scl.x = twp->scl.x - 0.002f;
		twp->scl.y = njSin(twp->ang.x);
		twp->scl.z = twp->scl.z - 0.018181818f;
		twp->ang.x += 546;
		if (twp->ang.x >= 0x4000)
			twp->pos.y = 2.0f - twp->scl.y * 9.0f + twp->counter.f;
		else
			twp->pos.y = twp->scl.y * 9.0f + twp->counter.f;
		EffectWarpDraw_r(twp);
	}
	if (twp->ang.x > 0x8000)
		FreeTask(tp);
}

// Depth hack for transporter label
void RenderChaoTransporterLabel_Fix(NJS_OBJECT* a1)
{
	late_z_ofs___ = -1000.0f;
	late_DrawObjectClip(a1, LATE_LIG, 1.0f);
	late_z_ofs___ = 0;
}

void AL_Transporter_Load()
{
	ChaoGardenTransporterEffect = CloneAttach(ADV01C_MODELS[32]);
	ChaoGardenTransporterEffect->mats[0].attr_texId = 68;
	WriteData((NJS_TEXLIST**)0x007290FB, &texlist_chao_object); // Transporter model texlist
	WriteJump(EffectWarpExecutor, EffectWarpExecutor_r); // Transporter effect in gardens 1
	WriteJump(EffectWarpDisplayer, EffectWarpDraw_r); // Transporter effect in gardens 2
	WriteCall((void*)0x00729289, SetChaoObjectTexlist); // Transporter effect in EC
	object_warp_model_ec_base_ec_base = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\warp_model_ec_base.nja.sa1mdl"); // EC garden to EC transporter
	object_warp_model_base_base = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\warp_model_base.nja.sa1mdl"); // All other transporters
	object_mr_placebeam_b_placebeam_b = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\mr_placebeam_b.nja.sa1mdl");
	object_mr_placebeam_a_placebeam_a = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\mr_placebeam_a.nja.sa1mdl");
	object_ec_placebeam_b_placebeam_b = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\ec_placebeam_b.nja.sa1mdl");
	object_ec_placebeam_a_placebeam_a = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\ec_placebeam_a.nja.sa1mdl");
	object_ss_placebeam_b_placebeam_b = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\ss_placebeam_b.nja.sa1mdl");
	object_ss_placebeam_a_placebeam_a = *LoadModel("system\\data\\chao\\data\\dx\\object_common\\ss_placebeam_a.nja.sa1mdl");
	WriteCall((void*)0x007291B1, RenderChaoTransporterLabel_Fix); // Label transparency fix
	WriteCall((void*)0x007291E0, RenderChaoTransporterLabel_Fix); // Label transparency fix
	WriteCall((void*)0x00729217, RenderChaoTransporterLabel_Fix); // Label transparency fix
	WriteData((CCL_INFO**)0x00729576, &WarpColliInfo); // Collision struct pointer to match the EC transporter one
	WriteData<1>((char*)0x00729574, 0x04u); // Collision parameter for InitCollision
}