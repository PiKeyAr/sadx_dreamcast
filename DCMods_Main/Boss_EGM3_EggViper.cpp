#include "stdafx.h"
#include "Boss_EGM3_EggViper.h"

static bool ModelsLoaded_B_EGM3 = false;
static bool BluePaletteIsGray = true;

NJS_OBJECT* object_kemuri_boss_boss2; // Cloned object for the dust effect
NJS_TEX vuvS_0_kemuri_boss_boss2[12]; // Cloned UVs for the dust effect

// Fixes Sonic's position in the cutscene
void EggViperCutsceneFix1(task *a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{	
	taskwk* twp = a1->twp;
	EV_SetPos(a1, twp->pos.x + 16, twp->pos.y, twp->pos.z + 4);
	EV_SetAction(a1, a2, a3, a4, a5, a6);
}

// Fixes Sonic's animation in the cutscene
void EggViperCutsceneFix2(int time)
{
	task *a1 = EV_GetPlayer(0);
	EV_Wait(18);
	EV_SetAction(a1, &action_s_s0009_sonic, &texlist_sonic, 1.0f, 1, 8);
	EV_Wait(62);
	EV_ClrAction(a1);
}

// Dust effect draw function
void Egm3_iroiroDraw(task* tp)
{
	int idx; // esi MAPDST
	NJS_TEX v2; // edx
	int v3; // eax
	int v4; // ecx
	float YDist; // [esp+0h] [ebp-14h]

	if (!loop_count)
	{
		SaveConstantAttr();
		___njFogDisable();
		njSetTexture(&texlist_egm3mdl);
		njPushMatrix(0);
		njTranslate(0, 0.0f, -170.0f, 0.0f);
		idx = 0;
		for (idx = 0; idx < 2; ++idx)
		{
			v2 = kemuri_uv_bias[idx];
			v3 = 0;
			v4 = 12;
			do
			{
				vuvS_0_kemuri_boss_boss[v3].u = v2.u + egm3DamageNow[v3].u;
				vuvS_0_kemuri_boss_boss[v3].v = v2.v + egm3DamageNow[v3].v;
				vuvS_0_kemuri_boss_boss2[v3].u = v2.v + egm3DamageNow[v3].u;
				vuvS_0_kemuri_boss_boss2[v3].v = v2.v + egm3DamageNow[v3].v;
				++v3;
				--v4;
			} while (v4);
			YDist = idx * 10.0f;
			njTranslate(0, 0.0f, YDist, 0.0f);
			late_DrawObjectClip(idx ? object_kemuri_boss_boss2 : &object_kemuri_boss_boss, LATE_MAT, 1.0f);
		}
		njPopMatrix(1u);
		___njFogEnable();
		LoadConstantAttr();
	}

	// Toggle between blue and gray palette replacement
	if (FrameCounter % ((g_CurrentFrame == 2) ? 2 : 4) == 0)
		BluePaletteIsGray = !BluePaletteIsGray;
	if (egm3_taskptr && egm3_taskptr->twp)
	{
		// The energy charge background color is always blue during the fadeout
		if (egm3_taskptr->twp->smode >= 15)
			BluePaletteIsGray = false;
	}
}

// Depth hack: Explosion 1
void EggViperExplosionFix(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = 8000.0f;
	late_DrawObjectClip(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Depth hack: Explosion 2
void EggViperLastExplosionFix(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = 8000.0f;
	DrawObjectClipMesh(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Blends the level diffuse palette with red
void PaletteColorRed(float r, float g, float b)
{
	addColorRGB(r, g, b);
	int dest = (int)(r * 255.0f);
	for (int c = 0; c < 256; c++)
	{
		LSPAL_0[0][c][0].argb.r = max(LSPAL_0[8][c][0].argb.r, dest);
		LSPAL_0[0][c][0].argb.g = LSPAL_0[8][c][0].argb.g;
		LSPAL_0[0][c][0].argb.b = LSPAL_0[8][c][0].argb.b;
	}
	UpdatePaletteAtlas = true;
}

// Blends // Blends the level diffuse palette with redwith cyan/grey
void PaletteColorBlue(float r, float g, float b)
{
	addColorRGB(r, g, b);
	if (!egm3_taskptr || !egm3_taskptr->twp)
		return;

	char smode = egm3_taskptr->twp->smode;
	if (smode < 14 || smode > 15)
		return;

	int dest = (int)(b * 255.0f);
	for (int c = 0; c < 256; c++)
	{
		LSPAL_0[0][c][0].argb.r = BluePaletteIsGray ? max(LSPAL_0[8][c][0].argb.r, dest) : LSPAL_0[8][c][0].argb.r;
		LSPAL_0[0][c][0].argb.g = max(LSPAL_0[8][c][0].argb.g, dest);
		LSPAL_0[0][c][0].argb.b = max(LSPAL_0[8][c][0].argb.b, dest);
	}
	UpdatePaletteAtlas = true;
}

// Blends the level diffuse palette back to the original palette
void RestoreFromBlue()
{
	for (int c = 0; c < 256; c++)
	{
		LSPAL_0[0][c][0].argb.r = max(LSPAL_0[8][c][0].argb.r, LSPAL_0[0][c][0].argb.r - 8);
		LSPAL_0[0][c][0].argb.g = max(LSPAL_0[8][c][0].argb.g, LSPAL_0[0][c][0].argb.g - 4);
		LSPAL_0[0][c][0].argb.b = max(LSPAL_0[8][c][0].argb.b, LSPAL_0[0][c][0].argb.b - 4);
	}
	if (FrameCounter % 2 == 0)
		UpdatePaletteAtlas = true;
}

// Hook for the palette fadeout
static void __cdecl Egm3Energy_r(task* tp)
{
	Egm3Energy(tp);

	char smode = tp->twp->smode;
	if (smode >= 15 && smode < 20)
	{
		RestoreFromBlue();
	}
}

// Hook the boss init function to create palette backup
void Egm3Init_r(task* tp)
{
	Egm3Init(tp);
	lig_cpyPalette(8, 0);
	UpdatePaletteAtlas = true;
}

// Hook the dust effect destroy function to restore palette from backup
void Egm3_iroiroExit_r(task* a1)
{
	memcpy(vuvS_0_kemuri_boss_boss, egm3DamageNow, sizeof(vuvS_0_kemuri_boss_boss));
	lig_cpyPalette(0, 8);
	UpdatePaletteAtlas = true;
}

void B_EGM3_Init()
{
	ReplaceSET("SETEGM3S");
	ReplacePVM("EGM3CHIKEI");
	ReplacePVM("EGM3MDL");
	ReplacePVM("EGM3SPR");
	WriteData((TaskFuncPtr*)0x0165EDB0, Egm3Init_r);
	WriteData((TaskFuncPtr*)0x0165EDC4, Egm3Energy_r);
	WriteJump((void*)0x0057E540, Egm3_iroiroExit_r);
	WriteCall((void*)0x00581A08, PaletteColorBlue); // Energytame
	WriteCall((void*)0x00581B96, PaletteColorRed); // Hit
	WriteCall((void*)0x00581DA1, PaletteColorRed); // Explosion in
	WriteCall((void*)0x00581DD6, PaletteColorRed); // Explosion out
	WriteCall((void*)0x00581C7F, PaletteColorRed); // Spinning after running out of lives
	WriteCall((void*)0x00581CE4, PaletteColorRed); // Dead
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Egm03[0][i].f32EndZ = -10000.0f;
		pFogTable_Egm03[0][i].f32StartZ = -10000.0f;
	}
}

void B_EGM3_Load()
{
	LevelLoader(LevelAndActIDs_EggViper, "SYSTEM\\data\\bossegm3\\landtableegm3.c.sa1lvl", &texlist_egm3chikei);
	if (!ModelsLoaded_B_EGM3)
	{
		// Replace function calls for better cockpit transparency sorting
		WriteCall((void*)0x0057E297, (void*)late_DrawMotionClipMesh);
		WriteCall((void*)0x0057E35B, (void*)late_DrawMotionClipMesh);
		WriteCall((void*)0x007B596C, (void*)late_DrawMotionClipMesh);
		// Explosions sorting fix
		WriteCall((void*)0x00584F46, EggViperExplosionFix);
		WriteCall((void*)0x00584F6C, EggViperExplosionFix);
		WriteCall((void*)0x0057E0C1, EggViperLastExplosionFix);
		WriteCall((void*)0x0057E13C, EggViperLastExplosionFix);
		// Dust effect UV fix + misc code for palette effects
		WriteJump((void*)0x0057E470, Egm3_iroiroDraw);
		object_kemuri_boss_boss2 = CloneObject(&object_kemuri_boss_boss);
		object_kemuri_boss_boss2->basicdxmodel->meshsets[0].vertuv = vuvS_0_kemuri_boss_boss2;
		// Objects
		object_boss_br_uki_uki = *LoadModel("system\\data\\bossegm3\\allmodel\\boss_br_uki.nja.sa1mdl"); // Platform 1
		object_boss_br_ukiside_ukiside = *LoadModel("system\\data\\bossegm3\\allmodel\\boss_br_ukiside.nja.sa1mdl"); // Platform 2
		object_boss_uki_nc_uki_nc_uki = *LoadModel("system\\data\\bossegm3\\allmodel\\boss_uki_nc_uki.nja.sa1mdl"); // Platform 3
		object_boss_uki_nc_ukiside_nc_ukiside = *LoadModel("system\\data\\bossegm3\\allmodel\\boss_uki_nc_ukiside.nja.sa1mdl"); // Platform 4
		AddWhiteDiffuseMaterial(&object_mobil03_egm3_cockpit_egm3_cockpit.basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_mobil03_egm3_cockpit_egm3_cockpit.basicdxmodel->mats[3]);
		AddWhiteDiffuseMaterial(&object_mobil03_egm3_cockpit_egm3_cockpit.basicdxmodel->mats[5]);
		AddWhiteDiffuseMaterial(&object_mobil03_egm3_cockpit_egm3_cockpit.basicdxmodel->mats[6]);
		// Cutscene fixes
		WriteCall((void*)0x006D04E3, EggViperCutsceneFix1); // supercoolsonic's position fix
		WriteCall((void*)0x006D04EA, EggViperCutsceneFix2); // Cutscene pose fix
		ModelsLoaded_B_EGM3 = true;
	}
}