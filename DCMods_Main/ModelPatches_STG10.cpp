#include "stdafx.h"

void PatchModels_STG10()
{
	// dxpc\stg10_finalegg\common\models\aamu05_cyl40.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\biglights_dai.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\bl_nc_bllight.nja.sa1mdl
		// Sorted model in DX

	// dxpc\stg10_finalegg\common\models\boyon_saku.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\b_con_conbere.nja.sa1mdl
	*(NJS_TEX*)0x019CDF10 = { 510, -4400 };
	*(NJS_TEX*)0x019CDF18 = { 510, -3864 };
	*(NJS_TEX*)0x019CDF20 = { 510, -3328 };
	*(NJS_TEX*)0x019CDF28 = { 510, -2791 };
	*(NJS_TEX*)0x019CDF30 = { 510, -2255 };
	*(NJS_TEX*)0x019CDF38 = { 510, -1719 };
	*(NJS_TEX*)0x019CDF3C = { 510, -1477 };
	*(NJS_TEX*)0x019CDF40 = { 510, -877 };
	*(NJS_TEX*)0x019CDF48 = { 510, -338 };
	*(NJS_TEX*)0x019CDF50 = { 510, 255 };
	*(NJS_TEX*)0x019CDF5C = { 510, -877 };
	*(NJS_TEX*)0x019CDF64 = { 510, -1119 };
	*(NJS_TEX*)0x019CDF6C = { 510, -1298 };
	*(NJS_TEX*)0x019CDF74 = { 510, -1477 };
	*(NJS_TEX*)0x019CDF7C = { 510, -5781 };
	*(NJS_TEX*)0x019CDF80 = { 510, -5242 };
	*(NJS_TEX*)0x019CDF88 = { 510, -5000 };
	*(NJS_TEX*)0x019CDF90 = { 510, -4821 };
	*(NJS_TEX*)0x019CDF98 = { 510, -4642 };
	*(NJS_TEX*)0x019CDFA0 = { 510, -4400 };
	*(NJS_TEX*)0x019CDFA8 = { 510, -5781 };
	*(NJS_TEX*)0x019CDFB0 = { 510, -6375 };

	// dxpc\stg10_finalegg\common\models\b_con_harikabe.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\cam_lig_bo_cl.nja.sa1mdl
		// Manually sorted

	// dxpc\stg10_finalegg\common\models\contena_box.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\elevator_hontai.nja.sa1mdl
		// Full replacement
	
	// dxpc\stg10_finalegg\common\models\elevator_tobira.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\enemyout2_enmout2.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\enemyout_hokoz.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\fun_funflot.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\haheneffect_hahen1a.nja.sa1mdl
	*(NJS_TEX*)0x01A1D088 = { 126, 118 };
	*(NJS_TEX*)0x01A1D08C = { 97, 138 };
	*(NJS_TEX*)0x01A1D090 = { 126, 99 };
	*(NJS_TEX*)0x01A1D094 = { 126, 125 };
	*(NJS_TEX*)0x01A1D098 = { 155, 150 };
	*(NJS_TEX*)0x01A1D09C = { 97, 138 };
	*(NJS_TEX*)0x01A1D0A0 = { 126, 157 };
	*(NJS_TEX*)0x01A1D0A4 = { 126, 118 };
	*(NJS_TEX*)0x01A1D0A8 = { 155, 150 };
	*(NJS_TEX*)0x01A1D0AC = { 126, 99 };

	// dxpc\stg10_finalegg\common\models\haheneffect_hahen2a.nja.sa1mdl
	*(NJS_TEX*)0x01A1D1E8 = { 108, 149 };
	*(NJS_TEX*)0x01A1D1EC = { 95, 118 };
	*(NJS_TEX*)0x01A1D1F0 = { 126, 97 };
	*(NJS_TEX*)0x01A1D1F4 = { 157, 138 };
	*(NJS_TEX*)0x01A1D1F8 = { 108, 149 };
	*(NJS_TEX*)0x01A1D1FC = { 122, 159 };
	*(NJS_TEX*)0x01A1D200 = { 95, 118 };
	*(NJS_TEX*)0x01A1D204 = { 157, 138 };

	// dxpc\stg10_finalegg\common\models\haheneffect_hahen3a.nja.sa1mdl
	*(NJS_TEX*)0x01A1D320 = { 127, 89 };
	*(NJS_TEX*)0x01A1D324 = { 124, 167 };
	*(NJS_TEX*)0x01A1D328 = { 165, 141 };
	*(NJS_TEX*)0x01A1D32C = { 87, 154 };
	*(NJS_TEX*)0x01A1D330 = { 127, 89 };
	*(NJS_TEX*)0x01A1D334 = { 124, 167 };

	// dxpc\stg10_finalegg\common\models\haheneffect_hahen4a.nja.sa1mdl
	*(NJS_TEX*)0x01A1D438 = { 127, 106 };
	*(NJS_TEX*)0x01A1D43C = { 113, 150 };
	*(NJS_TEX*)0x01A1D440 = { 104, 142 };
	*(NJS_TEX*)0x01A1D444 = { 148, 136 };
	*(NJS_TEX*)0x01A1D448 = { 127, 106 };
	*(NJS_TEX*)0x01A1D44C = { 113, 150 };

	// dxpc\stg10_finalegg\common\models\hammer_bmerge2.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\houden1a_hontai.nja.sa1mdl
	*(NJS_TEX*)0x019D78A4 = { 12, 36 };
	*(NJS_TEX*)0x019D78A8 = { 10, 117 };
	*(NJS_TEX*)0x019D78AC = { 252, 91 };
	*(NJS_TEX*)0x019D78B0 = { 252, 109 };
	*(NJS_TEX*)0x019D78B4 = { 12, 36 };
	*(NJS_TEX*)0x019D78B8 = { 10, 117 };
	*(NJS_TEX*)0x019D78BC = { 252, 91 };
	*(NJS_TEX*)0x019D78C0 = { 252, 109 };

	// dxpc\stg10_finalegg\common\models\houden1br_hontai.nja.sa1mdl
	*(NJS_TEX*)0x019D8F70 = { 252, 91 };
	*(NJS_TEX*)0x019D8F74 = { 12, 36 };
	*(NJS_TEX*)0x019D8F78 = { 252, 109 };
	*(NJS_TEX*)0x019D8F7C = { 10, 117 };
	*(NJS_TEX*)0x019D8F80 = { 252, 91 };
	*(NJS_TEX*)0x019D8F84 = { 12, 36 };
	*(NJS_TEX*)0x019D8F88 = { 252, 109 };
	*(NJS_TEX*)0x019D8F8C = { 10, 117 };

	// dxpc\stg10_finalegg\common\models\hsgo_hasigo.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\kaitenasiba_big.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\kaitenasiba_midium.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\kaitenasiba_small.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\kanban_eggkanban.nja.sa1mdl
		// Sorted model in DX; UVs patched manually, no model replacement
	*(NJS_TEX*)0x01C271F4 = { 510, 3 };
	*(NJS_TEX*)0x01C271FC = { 510, 255 };

	// dxpc\stg10_finalegg\common\models\kowarekabe_koware.nja.sa1mdl
		// Full replacement
		
	// dxpc\stg10_finalegg\common\models\kowarekabe_mae.nja.sa1mdl
		// Full replacement
		
	// dxpc\stg10_finalegg\common\models\ligpfr_light01.nja.sa1mdl
		// Model sorted in DX

	// dxpc\stg10_finalegg\common\models\lig_blue_lig_bou.nja.sa1mdl
		// Manually sorted

	// dxpc\stg10_finalegg\common\models\mdlelv_nc_elvtr.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\setasiba_lv1.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\shatter_breakparts.nja.sa1mdl
		//Model too different

	// dxpc\stg10_finalegg\common\models\shatter_br_nokori.nja.sa1mdl
	*(NJS_TEX*)0x01A13BE8 = { 127, 223 };
	*(NJS_TEX*)0x01A13BEC = { 127, 254 };
	*(NJS_TEX*)0x01A13BF0 = { 95, 254 };
	*(NJS_TEX*)0x01A13BF4 = { 191, 1 };
	*(NJS_TEX*)0x01A13BF8 = { 191, 65 };
	*(NJS_TEX*)0x01A13BFC = { 159, 33 };
	*(NJS_TEX*)0x01A13C00 = { 286, 33 };
	*(NJS_TEX*)0x01A13C04 = { 255, 1 };
	*(NJS_TEX*)0x01A13C08 = { 318, 1 };
	*(NJS_TEX*)0x01A13C0C = { 255, 254 };
	*(NJS_TEX*)0x01A13C10 = { 223, 254 };
	*(NJS_TEX*)0x01A13C14 = { 223, 223 };
	*(NJS_TEX*)0x01A13C18 = { 318, 1 };
	*(NJS_TEX*)0x01A13C1C = { 318, 65 };
	*(NJS_TEX*)0x01A13C20 = { 286, 33 };
	*(NJS_TEX*)0x01A13C24 = { 382, 1 };
	*(NJS_TEX*)0x01A13C28 = { 382, 65 };
	*(NJS_TEX*)0x01A13C2C = { 350, 33 };
	*(NJS_TEX*)0x01A13C30 = { 63, 254 };
	*(NJS_TEX*)0x01A13C34 = { 63, 128 };
	*(NJS_TEX*)0x01A13C38 = { 95, 191 };
	*(NJS_TEX*)0x01A13C3C = { 414, 191 };
	*(NJS_TEX*)0x01A13C40 = { 446, 128 };
	*(NJS_TEX*)0x01A13C44 = { 446, 254 };
	*(NJS_TEX*)0x01A13C48 = { 95, 191 };
	*(NJS_TEX*)0x01A13C4C = { 95, 254 };
	*(NJS_TEX*)0x01A13C50 = { 63, 254 };
	*(NJS_TEX*)0x01A13C54 = { 223, 33 };
	*(NJS_TEX*)0x01A13C58 = { 191, 1 };
	*(NJS_TEX*)0x01A13C5C = { 255, 1 };
	*(NJS_TEX*)0x01A13C60 = { 382, 65 };
	*(NJS_TEX*)0x01A13C64 = { 350, 65 };
	*(NJS_TEX*)0x01A13C68 = { 350, 33 };
	*(NJS_TEX*)0x01A13C6C = { 446, 254 };
	*(NJS_TEX*)0x01A13C70 = { 414, 254 };
	*(NJS_TEX*)0x01A13C74 = { 414, 191 };
	*(NJS_TEX*)0x01A13C78 = { 286, 254 };
	*(NJS_TEX*)0x01A13C7C = { 255, 254 };
	*(NJS_TEX*)0x01A13C80 = { 255, 223 };
	*(NJS_TEX*)0x01A13C84 = { 318, 223 };
	*(NJS_TEX*)0x01A13C88 = { 318, 254 };
	*(NJS_TEX*)0x01A13C8C = { 286, 254 };
	*(NJS_TEX*)0x01A13C90 = { 223, 33 };
	*(NJS_TEX*)0x01A13C94 = { 191, 65 };
	*(NJS_TEX*)0x01A13C98 = { 191, 1 };
	*(NJS_TEX*)0x01A13C9C = { 191, 254 };
	*(NJS_TEX*)0x01A13CA0 = { 159, 254 };
	*(NJS_TEX*)0x01A13CA4 = { 159, 223 };
	*(NJS_TEX*)0x01A13CA8 = { 286, 33 };
	*(NJS_TEX*)0x01A13CAC = { 255, 65 };
	*(NJS_TEX*)0x01A13CB0 = { 255, 1 };
	*(NJS_TEX*)0x01A13CB4 = { 414, 191 };
	*(NJS_TEX*)0x01A13CB8 = { 414, 128 };
	*(NJS_TEX*)0x01A13CBC = { 446, 128 };
	*(NJS_TEX*)0x01A13CC0 = { 414, 128 };
	*(NJS_TEX*)0x01A13CC4 = { 414, 191 };
	*(NJS_TEX*)0x01A13CC8 = { 382, 160 };
	*(NJS_TEX*)0x01A13CCC = { 159, 33 };
	*(NJS_TEX*)0x01A13CD0 = { 127, 65 };
	*(NJS_TEX*)0x01A13CD4 = { 127, 1 };
	*(NJS_TEX*)0x01A13CD8 = { 127, 1 };
	*(NJS_TEX*)0x01A13CDC = { 191, 1 };
	*(NJS_TEX*)0x01A13CE0 = { 159, 33 };
	*(NJS_TEX*)0x01A13CE4 = { 63, 128 };
	*(NJS_TEX*)0x01A13CE8 = { 95, 128 };
	*(NJS_TEX*)0x01A13CEC = { 95, 191 };
	*(NJS_TEX*)0x01A13CF0 = { 159, 33 };
	*(NJS_TEX*)0x01A13CF4 = { 159, 65 };
	*(NJS_TEX*)0x01A13CF8 = { 127, 65 };
	*(NJS_TEX*)0x01A13CFC = { 382, 191 };
	*(NJS_TEX*)0x01A13D00 = { 382, 223 };
	*(NJS_TEX*)0x01A13D04 = { 350, 223 };
	*(NJS_TEX*)0x01A13D08 = { 286, 33 };
	*(NJS_TEX*)0x01A13D0C = { 286, 65 };
	*(NJS_TEX*)0x01A13D10 = { 255, 65 };
	*(NJS_TEX*)0x01A13D14 = { 95, 191 };
	*(NJS_TEX*)0x01A13D18 = { 95, 128 };
	*(NJS_TEX*)0x01A13D1C = { 127, 160 };
	*(NJS_TEX*)0x01A13D20 = { 159, 223 };
	*(NJS_TEX*)0x01A13D24 = { 159, 254 };
	*(NJS_TEX*)0x01A13D28 = { 127, 254 };
	*(NJS_TEX*)0x01A13D2C = { 318, 1 };
	*(NJS_TEX*)0x01A13D30 = { 382, 1 };
	*(NJS_TEX*)0x01A13D34 = { 350, 33 };
	*(NJS_TEX*)0x01A13DE8 = { 255, 128 };
	*(NJS_TEX*)0x01A13DEC = { 191, 191 };
	*(NJS_TEX*)0x01A13DF0 = { 95, 191 };
	*(NJS_TEX*)0x01A13DF4 = { 414, 191 };
	*(NJS_TEX*)0x01A13DF8 = { 318, 191 };
	*(NJS_TEX*)0x01A13DFC = { 255, 128 };
	*(NJS_TEX*)0x01A13E00 = { 318, 254 };
	*(NJS_TEX*)0x01A13E04 = { 255, 254 };
	*(NJS_TEX*)0x01A13E08 = { 318, 191 };
	*(NJS_TEX*)0x01A13E0C = { 255, 254 };
	*(NJS_TEX*)0x01A13E10 = { 191, 191 };
	*(NJS_TEX*)0x01A13E14 = { 318, 191 };
	*(NJS_TEX*)0x01A13E18 = { 255, 254 };
	*(NJS_TEX*)0x01A13E1C = { 191, 254 };
	*(NJS_TEX*)0x01A13E20 = { 191, 191 };
	*(NJS_TEX*)0x01A13E24 = { 318, 191 };
	*(NJS_TEX*)0x01A13E28 = { 382, 254 };
	*(NJS_TEX*)0x01A13E2C = { 318, 254 };
	*(NJS_TEX*)0x01A13E30 = { 191, 254 };
	*(NJS_TEX*)0x01A13E34 = { 127, 254 };
	*(NJS_TEX*)0x01A13E38 = { 191, 191 };
	*(NJS_TEX*)0x01A13E3C = { 191, 191 };
	*(NJS_TEX*)0x01A13E40 = { 255, 128 };
	*(NJS_TEX*)0x01A13E44 = { 318, 191 };

	// dxpc\stg10_finalegg\common\models\shatter_shatter1a.nja.sa1mdl
	*(NJS_TEX*)0x01A17390 = { 510, 254 };
	*(NJS_TEX*)0x01A17394 = { 510, 1 };
	*(NJS_TEX*)0x01A173A8 = { 510, 254 };
	*(NJS_TEX*)0x01A173AC = { 510, 1 };
	*(NJS_TEX*)0x01A173B8 = { 510, 1 };
	*(NJS_TEX*)0x01A173BC = { 510, 254 };
	*(NJS_TEX*)0x01A173C0 = { 510, 1 };
	*(NJS_TEX*)0x01A173C4 = { 510, 254 };
	*(NJS_TEX*)0x01A173D0 = { 508, 220 };
	*(NJS_TEX*)0x01A173D4 = { 1, 220 };
	*(NJS_TEX*)0x01A173D8 = { 508, 246 };
	*(NJS_TEX*)0x01A173DC = { 1, 246 };

	// dxpc\stg10_finalegg\common\models\shatter_shatter1shape.nja.sa1mdl
		// Model too different
	
	// dxpc\stg10_finalegg\common\models\standlig_daig.nja.sa1mdl
	((NJS_MATERIAL*)0x01C28018)->attrflags = 0x32A9A440;
	((NJS_MATERIAL*)0x01C28068)->attrflags = 0x32B9A440;

	// dxpc\stg10_finalegg\common\models\ue_aamu_ue.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\ugokuhari_dodai.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\common\models\uki_uki.nja.sa1mdl
		// Full replacement

	// dxpc\stg10_finalegg\killcolli\act2\poly18.nja.sa1mdl
	*(NJS_VECTOR*)0x01A48470 = { 808.3194f, 0.000916f, -374.57834f };
}