#include "stdafx.h"
#include "Boss_Chaos4.h"

// TODO: Original waterfall particles

static bool ModelsLoaded_B_CHAOS4;

// Chaos 4 water and splash display function
void Chaos4CleanWater_Display(task* tp)
{
	float xshift = -145.92953f;
	float zshift = -27.3004f;
	NJS_VECTOR splashpos = { 0, 0, 0 };
	NJS_VECTOR zerovec = { 0, 0, 0 };
	// Swamp water
	if (first_alpha > -1.0f && !loop_count)
	{
		njSetTexture(&texlist_numa_f);
		object_c4_wt_s_numamoto_wt_s_numamoto.basicdxmodel->mats[0].diffuse.argb.a = (int)(153.0f * (1.0f + first_alpha));
		njPushMatrix(0);
		if (camera_twp->pos.y <= 0)
			late_z_ofs___ = -17900.0f;
		if (camera_twp->pos.y > 0)
		{
			// Camera above water and Chaos above water - move to back
			if (chaos_objpv[0].pos.y >= 15)
				late_z_ofs___ = -17900.0f;
			// Camera above water and Chaos below water - move to front
			else
				late_z_ofs___ = 8000.0f;
		}
		late_DrawObjectClipMesh(&object_c4_wt_s_numamoto_wt_s_numamoto, LATE_MAT, 1.0f);
		njPopMatrix(1u);
		late_z_ofs___ = 0.0f;
	}
	// Waterfall effect
	if (ssGameMode == MD_GAME_MAIN)
	{
		splashpos.y = 20.0f;
		sp_zoffset = 100.0f;
		switch (gu32GameCnt & 3)
		{
			case 0:
				splashpos.x = 9.3f;
				splashpos.z = -280.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 12.3f;
				splashpos.z = -275.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 15.3f;
				splashpos.z = -270.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				break;
			case 1:
				splashpos.x = 23.3f;
				splashpos.z = -267.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 31.3f;
				splashpos.z = -268.5f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 37.3f;
				splashpos.z = -269.5f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				break;
			case 2:
				splashpos.x = 43.3f;
				splashpos.z = -270.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 42.5f;
				splashpos.z = -275.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				splashpos.x = 48.3f;
				splashpos.z = -280.0f;
				splashpos.x += xshift;
				splashpos.z += zshift;
				CreateWater(&splashpos, &zerovec, 1.5f);
				break;
		}
		sp_zoffset = 0.0f;
	}
}

// Chaos 4 attack display function
void __cdecl Dsp_Chaos4KamaTame_r(task *a1)
{
	taskwk *v1; // esi

	v1 = a1->twp;
	if (!loop_count)
	{
		njSetTexture(&texlist_chaos4_kama);
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		late_ActionMS(&action_c4_tailattame01, v1->value.f, LATE_LIG);
		late_ActionMS(&action_c4_tailattame02, v1->value.f, LATE_LIG);
		njPopMatrix(1u);
	}
}

// Depth fix: Chaos 4 attack
void Chaos4TailAttackFix(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = 12000.0f;
	late_DrawObjectClip(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

// Material fix: Chaos 4 wave effect
void SetMaterial_Chaos4Wave(float a, float r, float g, float b)
{
	SetMaterial(max(0.0f, r), r, g, b);
}

// Hook to draw Chaos brain sprite with depth
void Chaos4BrainHook(NJS_SPRITE *sp, Int n, NJD_SPRITE attr, LATE zfunc_type)
{
	if (ssStageNumber == LevelIDs_Chaos4)
	{
		// Camera below water - Chaos above water
		if (camera_twp->pos.y <= 0)
			late_z_ofs___ = 8500.0f;
		// Camera above water
		else
		{
			// Chaos above water - Draw Chaos above water
			// Chaos below water - Draw Chaos below water
			late_z_ofs___ = (chaos_objpv[0].pos.y >= 15) ? 8500.0f : -19500.0f;
		}
		late_DrawSprite3D(sp, n, attr, zfunc_type);
	}
	else
		late_DrawSprite3D(sp, n, attr, zfunc_type);
}

// Hook to draw Chaos with depth
void Chaos4Action(NJS_ACTION *a1, float frameNumber)
{
	// Camera below water - Chaos above water
	if (camera_twp->pos.y <= 0)
		late_z_ofs___ = 8000.0f;
	// Camera above water
	else
	{
		// Chaos above water - Draw Chaos above water
		// Chaos below water - Draw Chaos below water
		late_z_ofs___ =  (chaos_objpv[0].pos.y >= 15) ? 8000.0f : -20000.0f;
	}
	late_ActionMesh(a1, frameNumber, LATE_MAT);
	late_z_ofs___ = 0.0f;
}

// Hook to queue the swamp
void Chaos4NumaFix(NJS_OBJECT *a1, int blend_mode, float scale)
{
	late_DrawObjectClipMesh(a1, LATE_MAT, scale);
}

// Hook to queue the ball attack with per mesh sorting
void Chaos4Ball(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_DrawObjectMesh(a1, LATE_MAT);
}

// Chaos 4 lilypad display function with depth hack
void __cdecl Dsp_Leaf_r(task *pTask)
{
	taskwk* v1; // esi
	Angle v3; // eax
	Angle v4; // eax
	Angle v5; // eax
	v1 = pTask->twp;
	if (!loop_count)
	{
		ResetMaterial();
		njSetTexture(&texlist_chaos4_object);
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		v3 = v1->ang.z;
		if (v3)
		{
			njRotateZ(0, v3);
		}
		v4 = v1->ang.x;
		if (v4)
		{
			njRotateX(0, v4);
		}
		v5 = v1->ang.y;
		if (v5)
		{
			njRotateY(0, v5);
		}
		if (v1->pos.y < 10.0f)
			late_z_ofs___ = -17952.0f;
		else
			late_z_ofs___ = 20;
		late_DrawObjectClipMS(&object_c4_nc_s_asiba_nc_s_asiba, LATE_WZ, 1.0f);
		late_z_ofs___ = 0.0f;
		njPopMatrix(1u);
	}
}

// Trampoline for small balls attack with depth hack
static Trampoline* Dsp_damage_ball_t = nullptr;
static void __cdecl Dsp_damage_ball_r(task *a1)
{
	const auto original = TARGET_DYNAMIC(Dsp_damage_ball);
	taskwk *v1 = a1->twp;
	// Chaos above or below water
	late_z_ofs___ = v1->pos.y >= 10 ? 8500.0f : -28500.0f;
	original(a1);
}

// Trampoline for big ball attack with depth hack
static Trampoline* Dsp_separete_t;
static void __cdecl Dsp_separete_r(task* a1)
{
	const auto original = TARGET_DYNAMIC(Dsp_separete);
	taskwk* v1 = a1->twp;
	// Chaos above or below water
	late_z_ofs___ = v1->pos.y >= 10 ? 8500.0f : -28500.0f;
	original(a1);
}

// Depth hack for Chaos morph model
void Chaos4_Transform(NJS_OBJECT *object)
{
	late_z_ofs___ = -17000.0f;
	late_DrawObjectMesh(object, LATE_MAT);
	late_z_ofs___ = 0;
}

void B_CHAOS4_Init()
{
	ReplaceSET("SET1700S");
	ReplacePVM("CHAOS4_COMMON");
	ReplacePVM("CHAOS4_HASHIRA");
	ReplacePVM("CHAOS4_KAMA");
	ReplacePVM("CHAOS4_NUMA");
	ReplacePVM("CHAOS4_OBJECT");
	ReplacePVM("CHAOS4_SHIBUKI");
	ReplacePVM("CHAOS4_TIKEI");
	ReplacePVM("CHAOS4_WAVE");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Chaos04[0][i].Col = 0xFF000000;
		pFogTable_Chaos04[0][i].f32StartZ = 1.0f;
		pFogTable_Chaos04[0][i].f32EndZ = 2000.0f;
		pFogTable_Chaos04[0][i].u8Enable = 0;
	}
}

void B_CHAOS4_Load()
{
	LevelLoader(LevelAndActIDs_Chaos4, "SYSTEM\\data\\boss_chaos4\\landtable1700.c.sa1lvl", &texlist_chaos4_tikei);
	if (!ModelsLoaded_B_CHAOS4)
	{
		// Various fixes
		WriteData<1>((char*)0x00555A42, NJD_COLOR_BLENDING_INVSRCALPHA); // Fix wrong material flags on ball model
		WriteData<1>((char*)0x00555B3F, 0x08); // Chaos 4 bubble blending mode SA_SRC instead of SA_ONE
		object_c4_wave_wave.basicdxmodel->mats[0].attr_texId = 0; // Fix wave texture ID
		WriteCall((void*)0x005539E6, SetMaterial_Chaos4Wave); // Make wave visible
		// Objects		
		object_c4_wt_s_numa_wt_s_numa = *LoadModel("SYSTEM\\data\\boss_chaos4\\common\\models\\obj\\c4_wt_s_numa.nja.sa1mdl"); // Swamp water
		object_c4_ar_s_kabeb_ar_s_kabeb = *LoadModel("SYSTEM\\data\\boss_chaos4\\common\\models\\obj\\c4_ar_s_kabeb.nja.sa1mdl"); // Wall
		object_c4_wt_s_numab_wt_s_numab = *LoadModel("SYSTEM\\data\\boss_chaos4\\common\\models\\obj\\c4_wt_s_numab.nja.sa1mdl"); // Unused?
		object_c4_nc_s_asiba_nc_s_asiba = *LoadModel("SYSTEM\\data\\boss_chaos4\\common\\models\\obj\\c4_nc_s_asiba.nja.sa1mdl"); // Lilypad
		matlist_c4_s_skusa_nc_s_hasb[0].attrflags = 0x94BCA400; // Restore missing alpha on small lilypads
		WriteJump((void*)0x00552F80, Chaos4CleanWater_Display);
		WriteJump((void*)0x00553F60, Dsp_Leaf_r); // Fix lilypad
		// Depth hacks
		WriteData<10>((char*)0x0055507D, 0x90u); // Disable depth bias setting for balls
		WriteData<10>((char*)0x00555AB1, 0x90u); // Disable depth bias setting for balls attack
		Dsp_damage_ball_t = new Trampoline(0x00554FF0, 0x00554FF5, Dsp_damage_ball_r);
		Dsp_separete_t = new Trampoline(0x00555A10, 0x00555A15, Dsp_separete_r);
		WriteCall((void*)0x005528ED, Chaos4Action); // Main model
		WriteCall((void*)0x00555096, Chaos4Ball);
		WriteCall((void*)0x007ADC1E, Chaos4BrainHook);
		WriteCall((void*)0x00553380, Chaos4NumaFix);
		WriteCall((void*)0x00552918, Chaos4_Transform); // Chaos' model formed from balls
		WriteJump((void*)0x00556420, Dsp_Chaos4KamaTame_r); // Attack effect fix
		WriteCall((void*)0x0055667C, Chaos4TailAttackFix);
		ModelsLoaded_B_CHAOS4 = true;
	}
}