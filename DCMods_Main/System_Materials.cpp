#include "stdafx.h"

// Fixes specular flags for objects that aren't picked up by Lantern Engine.
void CorrectSpecular(NJS_OBJECT* obj)
{
	if (!obj || !obj->basicdxmodel)
		return;

	CorrectSpecular(obj->basicdxmodel);

	if (obj->child)
		CorrectSpecular(obj->child);
	if (obj->sibling)
		CorrectSpecular(obj->sibling);
}

// Fixes specular flags for objects that aren't picked up by Lantern Engine (NJS_MODEL_SADX version).
void CorrectSpecular(NJS_MODEL_SADX* mdl)
{
	if (!mdl || !mdl->nbMat || !mdl->mats)
		return;

	bool ignorespecular = ((mdl->mats[0].attrflags & NJD_FLAG_IGNORE_SPECULAR) || (mdl->mats[0].specular.argb.a == 0));

	for (int k = 1; k < mdl->nbMat; ++k)
	{
		mdl->mats[k].specular.color = ignorespecular ? 0x00000000 : 0xFFFFFFFF;
		if (!ignorespecular && (mdl->mats[k].attrflags & NJD_FLAG_IGNORE_SPECULAR))
		{
			mdl->mats[k].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
		}
		else if (ignorespecular && !(mdl->mats[k].attrflags & NJD_FLAG_IGNORE_SPECULAR))
		{
			mdl->mats[k].attrflags |= NJD_FLAG_IGNORE_SPECULAR;
		}
	}
}

// Sets NJD_FLAG_IGNORE_SPECULAR on all materials
void ForceLevelSpecular_Object(NJS_OBJECT* obj, bool recursive)
{
	if (obj)
	{
		if (obj->basicdxmodel)
		{
			for (int k = 0; k < obj->basicdxmodel->nbMat; ++k)
			{
				if (!(obj->basicdxmodel->mats[k].attrflags & NJD_FLAG_IGNORE_SPECULAR))
					obj->basicdxmodel->mats[k].attrflags |= NJD_FLAG_IGNORE_SPECULAR;
			}
		}
		if (recursive && obj->child)
			ForceLevelSpecular_Object(obj->child, true);
		if (recursive && obj->sibling)
			ForceLevelSpecular_Object(obj->sibling, true);
	}

}

// Removes NJD_FLAG_IGNORE_SPECULAR on all materials
void ForceObjectSpecular_Object(NJS_OBJECT* obj, bool recursive)
{
	if (obj)
	{
		if (obj->basicdxmodel)
		{
			for (int k = 0; k < obj->basicdxmodel->nbMat; ++k)
			{
				obj->basicdxmodel->mats[k].specular.color = 0xFFFFFFFF;
				if (obj->basicdxmodel->mats[k].attrflags & NJD_FLAG_IGNORE_SPECULAR)
				{
					obj->basicdxmodel->mats[k].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
				}
			}
		}
		if (recursive && obj->child)
			ForceObjectSpecular_Object(obj->child, true);
		if (recursive && obj->sibling)
			ForceObjectSpecular_Object(obj->sibling, true);
	}
}

// Removes NJD_FLAG_USE_ALPHA in all materials
void RemoveTransparency_Object(NJS_OBJECT* obj, bool recursive)
{
	NJS_MATERIAL* material;
	if (obj->basicdxmodel)
	{
		for (int k = 0; k < obj->basicdxmodel->nbMat; ++k)
		{
			material = &obj->basicdxmodel->mats[k];
			material->diffuse.argb.a = 255;
			if (material->attrflags & NJD_FLAG_USE_ALPHA)
				material->attrflags &= ~NJD_FLAG_USE_ALPHA;
		}
	}
	if (recursive)
	{
		if (obj->child)
			RemoveTransparency_Object(obj->child, true);
		if (obj->sibling)
			RemoveTransparency_Object(obj->sibling, true);
	}
}

// Removes material colors from a model
void RemoveMaterialColors(NJS_OBJECT* obj)
{
	if (obj == nullptr)
		return;
	if (obj->basicdxmodel != nullptr)
	{
		for (int k = 0; k < obj->basicdxmodel->nbMat; ++k)
		{
			obj->basicdxmodel->mats[k].diffuse.argb.r = 0xFF;
			obj->basicdxmodel->mats[k].diffuse.argb.g = 0xFF;
			obj->basicdxmodel->mats[k].diffuse.argb.b = 0xFF;
		}
	}
	if (obj->child != nullptr)
		RemoveMaterialColors(obj->child);
	if (obj->sibling != nullptr)
		RemoveMaterialColors(obj->sibling);
}

// Removes material colors from all models in a LandTable
void RemoveMaterialColors(_OBJ_LANDTABLE* landtable)
{
	for (int j = 0; j < landtable->ssCount; j++)
		RemoveMaterialColors(landtable->pLandEntry[j].pObject);
	for (int j = 0; j < landtable->ssMotCount; j++)
		RemoveMaterialColors(landtable->pMotLandEntry[j].pObject);
}

// Lantern callback: white diffuse
bool ForceWhiteDiffuse(NJS_MATERIAL* material, uint32_t flags)
{
	diffuse_override_rgb(1, 1, 1, false);
	diffuse_override(true);
	return true;
}

// Lantern callback: white diffuse (Station Square night version)
bool ForceWhiteDiffuse_Night(NJS_MATERIAL* material, uint32_t flags)
{
	bool night = SeqGetTime() == SEQ_NIGHT;
	diffuse_override_rgb(1, 1, 1, false);
	diffuse_override(night);
	return true;
}

// Lantern callback: disable alha rejection
bool DisableAlphaRejection(NJS_MATERIAL* material, uint32_t flags)
{
	set_alpha_reject(0.0f, false);
	return true;
}

// Registers a material with alpha rejection to disable
void AddAlphaRejectMaterial(const NJS_MATERIAL* material)
{
	if (ModConfig::DLLLoaded_Lantern)
		material_register(&material, 1, DisableAlphaRejection);
}

// Registers a material with white diffuse
void AddWhiteDiffuseMaterial(const NJS_MATERIAL* material)
{
	if (ModConfig::DLLLoaded_Lantern && ModConfig::EnableWhiteDiffuse)
		material_register(&material, 1, ForceWhiteDiffuse);
}