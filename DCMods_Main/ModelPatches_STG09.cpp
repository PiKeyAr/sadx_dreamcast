#include "stdafx.h"

void PatchModels_STG09()
{
	// dxpc\stg09_casino\act01\models\cas_dead_cube.nja.sa1mdl
	//((NJS_MATERIAL*)0x01D7E0E8)->attrflags = 0x94002400;
	//((NJS_MATERIAL*)0x01D7E0E8)->exponent = 6;
	//((NJS_MATERIAL*)0x01D7E0E8)->diffuse.color = 0xFFBFBFBF;
	//((NJS_MATERIAL*)0x01D7E0E8)->specular.color = 0xFFFFFFFF;

	// dxpc\stg09_casino\common\models\ogino_model\casino_lion.nja.sa1mdl
		//Full replacement

	// dxpc\stg09_casino\common\models\ogino_model\cas_fan.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\cas_fook.nja.sa1mdl
	*(NJS_TEX*)0x01E51260 = { 1020, 255 };
	*(NJS_TEX*)0x01E51264 = { 1020, -765 };
	*(NJS_TEX*)0x01E51268 = { 0, 255 };
	*(NJS_TEX*)0x01E5126C = { 0, -765 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_lioncol.nja.sa1mdl
		// Collision model not replaced

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj1_sdai_futi_b.nja.sa1mdl
	*(NJS_TEX*)0x01E471CC = { 0, -255 };
	*(NJS_TEX*)0x01E471D4 = { 3060, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_6slot.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_6slot_a.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_cow_girl.nja.sa1mdl
	*(NJS_TEX*)0x01E51660 = { 0, -255 };
	*(NJS_TEX*)0x01E51668 = { 2588, -255 };
	*(NJS_TEX*)0x01E51670 = { 5100, -255 };
	*(NJS_TEX*)0x01E51678 = { 2588, -255 };
	*(NJS_TEX*)0x01E51680 = { 0, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_kadan_large.nja.sa1mdl
	*(NJS_TEX*)0x01E5D688 = { 0, -255 };
	*(NJS_TEX*)0x01E5D690 = { 255, -255 };
	*(NJS_TEX*)0x01E5D694 = { 255, 255 };
	*(NJS_TEX*)0x01E5D69C = { 255, 255 };
	*(NJS_TEX*)0x01E5D6A0 = { 0, -255 };
	*(NJS_TEX*)0x01E5D6A4 = { 255, -255 };
	*(NJS_TEX*)0x01E5D6A8 = { 0, -255 };
	*(NJS_TEX*)0x01E5D6B0 = { 510, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_kadan_mini.nja.sa1mdl
	*(NJS_TEX*)0x01E5DA94 = { 510, -255 };
	*(NJS_TEX*)0x01E5DA98 = { 0, -255 };
	*(NJS_TEX*)0x01E5DA9C = { 510, 255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_mizu_a.nja.sa1mdl
	((NJS_MATERIAL*)0x01E47330)->attrflags = 0x94302400;
	((NJS_MATERIAL*)0x01E47344)->attrflags = 0x94302400;

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_mizu_b.nja.sa1mdl
	((NJS_MATERIAL*)0x01E47B50)->attrflags = 0x94302400;

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_neon_k.nja.sa1mdl
	*(NJS_TEX*)0x01E6D7C8 = { 2550, -255 };
	*(NJS_TEX*)0x01E6D7D0 = { 1974, -255 };
	*(NJS_TEX*)0x01E6D7D8 = { 1405, -255 };
	*(NJS_TEX*)0x01E6D7E0 = { 842, -255 };
	*(NJS_TEX*)0x01E6D7E8 = { 280, -255 };
	*(NJS_TEX*)0x01E6D7F0 = { 0, -255 };
	*(NJS_TEX*)0x01E6D7F8 = { 280, -255 };
	*(NJS_TEX*)0x01E6D800 = { 842, -255 };
	*(NJS_TEX*)0x01E6D808 = { 1405, -255 };
	*(NJS_TEX*)0x01E6D810 = { 1974, -255 };
	*(NJS_TEX*)0x01E6D818 = { 2550, -255 };
	*(NJS_TEX*)0x01E6D820 = { 2550, -255 };
	*(NJS_TEX*)0x01E6D828 = { 1974, -255 };
	*(NJS_TEX*)0x01E6D830 = { 1405, -255 };
	*(NJS_TEX*)0x01E6D838 = { 842, -255 };
	*(NJS_TEX*)0x01E6D840 = { 280, -255 };
	*(NJS_TEX*)0x01E6D848 = { 0, -255 };
	*(NJS_TEX*)0x01E6D850 = { 280, -255 };
	*(NJS_TEX*)0x01E6D858 = { 842, -255 };
	*(NJS_TEX*)0x01E6D860 = { 1405, -255 };
	*(NJS_TEX*)0x01E6D868 = { 1974, -255 };
	*(NJS_TEX*)0x01E6D870 = { 2550, -255 };
	*(NJS_TEX*)0x01E6D900 = { 249, -255 };
	*(NJS_TEX*)0x01E6D908 = { 701, -255 };
	*(NJS_TEX*)0x01E6D910 = { 1275, -255 };
	*(NJS_TEX*)0x01E6D918 = { 701, -255 };
	*(NJS_TEX*)0x01E6D920 = { 1275, -255 };
	*(NJS_TEX*)0x01E6D928 = { 701, -255 };
	*(NJS_TEX*)0x01E6D92C = { 249, -255 };
	*(NJS_TEX*)0x01E6D934 = { 0, -255 };
	*(NJS_TEX*)0x01E6D93C = { 249, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_neon_kabe.nja.sa1mdl
	*(NJS_TEX*)0x01E4BD38 = { 0, -255 };
	*(NJS_TEX*)0x01E4BD40 = { 7650, -255 };
	*(NJS_TEX*)0x01E4BD48 = { 15300, -255 };
	*(NJS_TEX*)0x01E4BD50 = { 0, -255 };
	*(NJS_TEX*)0x01E4BD58 = { 7650, -255 };
	*(NJS_TEX*)0x01E4BD60 = { 15300, -255 };
	*(NJS_TEX*)0x01E4BD68 = { 0, -255 };
	*(NJS_TEX*)0x01E4BD70 = { 7650, -255 };
	*(NJS_TEX*)0x01E4BD78 = { 15300, -255 };
	*(NJS_TEX*)0x01E4BD80 = { 0, -255 };
	*(NJS_TEX*)0x01E4BD88 = { 7650, -255 };
	*(NJS_TEX*)0x01E4BD90 = { 15300, -255 };
	*(NJS_TEX*)0x01E4BD9C = { 15300, -255 };
	*(NJS_TEX*)0x01E4BDA4 = { 7649, -255 };
	*(NJS_TEX*)0x01E4BDAC = { 0, -255 };
	*(NJS_TEX*)0x01E4BDB4 = { 15300, -255 };
	*(NJS_TEX*)0x01E4BDBC = { 7649, -255 };
	*(NJS_TEX*)0x01E4BDC4 = { 0, -255 };
	*(NJS_TEX*)0x01E4BDCC = { 15300, -255 };
	*(NJS_TEX*)0x01E4BDD4 = { 7649, -255 };
	*(NJS_TEX*)0x01E4BDDC = { 0, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_nkanban_b.nja.sa1mdl
	*(NJS_TEX*)0x01E3C3C8 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C3CC = { 3060, -255 };
	*(NJS_TEX*)0x01E3C3D4 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C3D8 = { 3060, -255 };
	*(NJS_TEX*)0x01E3C3E0 = { 0, -255 };
	*(NJS_TEX*)0x01E3C3E4 = { 1051, -255 };
	*(NJS_TEX*)0x01E3C3EC = { 2008, -255 };
	*(NJS_TEX*)0x01E3C400 = { 0, -255 };
	*(NJS_TEX*)0x01E3C404 = { 0, -255 };
	*(NJS_TEX*)0x01E3C408 = { 1051, -255 };
	*(NJS_TEX*)0x01E3C410 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C424 = { 0, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_nkanban_r.nja.sa1mdl
	*(NJS_TEX*)0x01E3C868 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C86C = { 3060, -255 };
	*(NJS_TEX*)0x01E3C874 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C878 = { 3060, -255 };
	*(NJS_TEX*)0x01E3C880 = { 0, -255 };
	*(NJS_TEX*)0x01E3C884 = { 1051, -255 };
	*(NJS_TEX*)0x01E3C88C = { 2008, -255 };
	*(NJS_TEX*)0x01E3C8A0 = { 0, -255 };
	*(NJS_TEX*)0x01E3C8A4 = { 0, -255 };
	*(NJS_TEX*)0x01E3C8A8 = { 1051, -255 };
	*(NJS_TEX*)0x01E3C8B0 = { 2008, -255 };
	*(NJS_TEX*)0x01E3C8C4 = { 0, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_roolet_ita.nja.sa1mdl
	*(NJS_TEX*)0x01E3BBD0 = { 0, -255 };
	*(NJS_TEX*)0x01E3BBD4 = { 3060, -255 };
	*(NJS_TEX*)0x01E3BBE0 = { 0, -255 };
	*(NJS_TEX*)0x01E3BBE4 = { 3060, -255 };
	*(NJS_TEX*)0x01E3BBF0 = { 0, -255 };
	*(NJS_TEX*)0x01E3BBF4 = { 3060, -255 };
	*(NJS_TEX*)0x01E3BC00 = { 0, -255 };
	*(NJS_TEX*)0x01E3BC04 = { 3060, -255 };
	*(NJS_TEX*)0x01E3BC10 = { 0, -254 };
	*(NJS_TEX*)0x01E3BC14 = { 3059, -255 };
	*(NJS_TEX*)0x01E3BC20 = { 0, -254 };
	*(NJS_TEX*)0x01E3BC24 = { 3059, -255 };
	*(NJS_TEX*)0x01E3BC30 = { 0, -254 };
	*(NJS_TEX*)0x01E3BC34 = { 3059, -255 };
	*(NJS_TEX*)0x01E3BC40 = { 0, -254 };
	*(NJS_TEX*)0x01E3BC44 = { 3059, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_sdai_futi_r.nja.sa1mdl
	*(NJS_TEX*)0x01E46FFC = { 0, -255 };
	*(NJS_TEX*)0x01E47004 = { 3060, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_tens_card.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_tens_slot.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\cas_obj_ten_monitor.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\csn_kanban_card.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\ogino_model\csn_kanban_slot.nja.sa1mdl
		//Sorted model
	//*(NJS_TEX*)0x01E4DFD8 = { 1020, -255 };
	//*(NJS_TEX*)0x01E4DFE0 = { 510, -255 };
	//*(NJS_TEX*)0x01E4DFE8 = { 0, -255 };

	// dxpc\stg09_casino\common\models\ogino_model\csn_kinka.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\waka_models\alegg_body_o.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\waka_models\card_doa_a.nja.sa1mdl
	*(NJS_TEX*)0x01DDE80C = { 1020, -255 };
	*(NJS_TEX*)0x01DDE814 = { 0, -255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_b_ideacp.nja.sa1mdl
	*(NJS_TEX*)0x01DC779C = { 509, 255 };
	*(NJS_TEX*)0x01DC77A4 = { 510, 0 };
	*(NJS_TEX*)0x01DC77A8 = { 506, 253 };
	*(NJS_TEX*)0x01DC77B0 = { 258, 125 };
	*(NJS_TEX*)0x01DC77B8 = { 509, 255 };
	*(NJS_TEX*)0x01DC77C0 = { 510, 0 };
	*(NJS_TEX*)0x01DC77C4 = { 506, 253 };
	*(NJS_TEX*)0x01DC77CC = { 258, 125 };
	*(NJS_TEX*)0x01DC77D0 = { 506, 253 };
	*(NJS_TEX*)0x01DC77D8 = { 258, 125 };
	*(NJS_TEX*)0x01DC77E0 = { 509, 255 };
	*(NJS_TEX*)0x01DC77E8 = { 510, 0 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_g_ideacp.nja.sa1mdl
	*(NJS_TEX*)0x01DC41FC = { 509, 255 };
	*(NJS_TEX*)0x01DC4204 = { 510, 0 };
	*(NJS_TEX*)0x01DC4208 = { 506, 253 };
	*(NJS_TEX*)0x01DC4210 = { 258, 125 };
	*(NJS_TEX*)0x01DC4218 = { 509, 255 };
	*(NJS_TEX*)0x01DC4220 = { 510, 0 };
	*(NJS_TEX*)0x01DC4224 = { 506, 253 };
	*(NJS_TEX*)0x01DC422C = { 258, 125 };
	*(NJS_TEX*)0x01DC4230 = { 506, 253 };
	*(NJS_TEX*)0x01DC4238 = { 258, 125 };
	*(NJS_TEX*)0x01DC4240 = { 509, 255 };
	*(NJS_TEX*)0x01DC4248 = { 510, 0 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_ideacp.nja.sa1mdl
	*(NJS_TEX*)0x01DC29A8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC29B0 = { 1019, 255 };
	*(NJS_TEX*)0x01DC29B8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC29C0 = { 1019, 255 };
	*(NJS_TEX*)0x01DC29C8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC29D0 = { 1019, 255 };
	*(NJS_TEX*)0x01DC29D8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC29E0 = { 1019, 255 };
	*(NJS_TEX*)0x01DC29E8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC29F0 = { 1019, 255 };
	*(NJS_TEX*)0x01DC29F8 = { 1020, 0 };
	*(NJS_TEX*)0x01DC2A00 = { 1019, 255 };
	*(NJS_TEX*)0x01DC272C = { 509, 255 };
	*(NJS_TEX*)0x01DC2734 = { 510, 0 };
	*(NJS_TEX*)0x01DC2738 = { 506, 253 };
	*(NJS_TEX*)0x01DC2740 = { 258, 125 };
	*(NJS_TEX*)0x01DC2748 = { 509, 255 };
	*(NJS_TEX*)0x01DC2750 = { 510, 0 };
	*(NJS_TEX*)0x01DC2754 = { 506, 253 };
	*(NJS_TEX*)0x01DC275C = { 258, 125 };
	*(NJS_TEX*)0x01DC2760 = { 506, 253 };
	*(NJS_TEX*)0x01DC2768 = { 258, 125 };
	*(NJS_TEX*)0x01DC2770 = { 509, 255 };
	*(NJS_TEX*)0x01DC2778 = { 510, 0 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_obj_xi.nja.sa1mdl
	*(NJS_TEX*)0x01DCE15C = { 255, 0 };
	*(NJS_TEX*)0x01DCE160 = { 255, 255 };
	*(NJS_TEX*)0x01DCE164 = { 0, 255 };
	*(NJS_TEX*)0x01DCE168 = { 255, 0 };
	*(NJS_TEX*)0x01DCE16C = { 255, 255 };
	*(NJS_TEX*)0x01DCE170 = { 0, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_xi_b.nja.sa1mdl
	*(NJS_TEX*)0x01DCE4DC = { 255, 0 };
	*(NJS_TEX*)0x01DCE4E0 = { 255, 255 };
	*(NJS_TEX*)0x01DCE4E8 = { 255, 0 };
	*(NJS_TEX*)0x01DCE4EC = { 255, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_xi_c.nja.sa1mdl
	*(NJS_TEX*)0x01DCE804 = { 255, 0 };
	*(NJS_TEX*)0x01DCE808 = { 255, 255 };
	*(NJS_TEX*)0x01DCE810 = { 255, 0 };
	*(NJS_TEX*)0x01DCE814 = { 255, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_xi_d.nja.sa1mdl
	*(NJS_TEX*)0x01DCEB1C = { 255, 0 };
	*(NJS_TEX*)0x01DCEB20 = { 255, 255 };
	*(NJS_TEX*)0x01DCEB28 = { 255, 0 };
	*(NJS_TEX*)0x01DCEB2C = { 255, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_xi_e.nja.sa1mdl
	*(NJS_TEX*)0x01DCEE9C = { 255, 0 };
	*(NJS_TEX*)0x01DCEEA0 = { 255, 255 };
	*(NJS_TEX*)0x01DCEEA4 = { 0, 255 };
	*(NJS_TEX*)0x01DCEEA8 = { 255, 0 };
	*(NJS_TEX*)0x01DCEEAC = { 255, 255 };
	*(NJS_TEX*)0x01DCEEB0 = { 0, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_xi_f.nja.sa1mdl
	*(NJS_TEX*)0x01DCF1C4 = { 255, 0 };
	*(NJS_TEX*)0x01DCF1C8 = { 255, 255 };
	*(NJS_TEX*)0x01DCF1CC = { 0, 255 };
	*(NJS_TEX*)0x01DCF1D0 = { 255, 0 };
	*(NJS_TEX*)0x01DCF1D4 = { 255, 255 };
	*(NJS_TEX*)0x01DCF1D8 = { 0, 255 };

	// dxpc\stg09_casino\common\models\waka_models\card_obj_yajirusi_a.nja.sa1mdl
		//Model too different
	// dxpc\stg09_casino\common\models\waka_models\card_obj_yajirusi_b.nja.sa1mdl
		//Model too different
	// dxpc\stg09_casino\common\models\waka_models\card_obj_yajirusi_c.nja.sa1mdl
		//Model too different
	// dxpc\stg09_casino\common\models\waka_models\card_obj_yajirusi_d.nja.sa1mdl
		//Model too different
	// dxpc\stg09_casino\common\models\waka_models\card_obj_y_ideacp.nja.sa1mdl
	*(NJS_TEX*)0x01DC5CCC = { 509, 255 };
	*(NJS_TEX*)0x01DC5CD4 = { 510, 0 };
	*(NJS_TEX*)0x01DC5CD8 = { 506, 253 };
	*(NJS_TEX*)0x01DC5CE0 = { 258, 125 };
	*(NJS_TEX*)0x01DC5CE8 = { 509, 255 };
	*(NJS_TEX*)0x01DC5CF0 = { 510, 0 };
	*(NJS_TEX*)0x01DC5CF4 = { 506, 253 };
	*(NJS_TEX*)0x01DC5CFC = { 258, 125 };
	*(NJS_TEX*)0x01DC5D00 = { 506, 253 };
	*(NJS_TEX*)0x01DC5D08 = { 258, 125 };
	*(NJS_TEX*)0x01DC5D10 = { 509, 255 };
	*(NJS_TEX*)0x01DC5D18 = { 510, 0 };

	// dxpc\stg09_casino\common\models\waka_models\card_t_yaji_a.nja.sa1mdl
	((NJS_MATERIAL*)0x01DDB524)->attrflags = 0x9634A400;
	*(NJS_TEX*)0x01DDB544 = { 509, 0 };
	*(NJS_TEX*)0x01DDB548 = { 510, 254 };

	// dxpc\stg09_casino\common\models\waka_models\casi_banp.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\waka_models\casi_banp_k.nja.sa1mdl
		//Model too different

	// dxpc\stg09_casino\common\models\waka_models\casi_fulip01a.nja.sa1mdl
	*(NJS_TEX*)0x01D8BA00 = { 399, 248 };
	*(NJS_TEX*)0x01D8BA04 = { 399, 248 };
	*(NJS_TEX*)0x01D8BA08 = { 510, 230 };
	*(NJS_TEX*)0x01D8BA0C = { 510, 230 };
	*(NJS_TEX*)0x01D8BA10 = { 325, 6 };
	*(NJS_TEX*)0x01D8BA14 = { 325, 6 };
	*(NJS_TEX*)0x01D8BA18 = { 245, 0 };
	*(NJS_TEX*)0x01D8BA1C = { 245, 0 };
	*(NJS_TEX*)0x01D8BA20 = { 255, 255 };
	*(NJS_TEX*)0x01D8BA24 = { 399, 248 };
	*(NJS_TEX*)0x01D8BA28 = { 255, 255 };
	*(NJS_TEX*)0x01D8BA2C = { 399, 248 };
	*(NJS_TEX*)0x01D8BA30 = { 96, 247 };
	*(NJS_TEX*)0x01D8BA34 = { 510, 230 };
	*(NJS_TEX*)0x01D8BA3C = { 325, 6 };
	*(NJS_TEX*)0x01D8BA40 = { 182, 8 };
	*(NJS_TEX*)0x01D8BA44 = { 245, 0 };
	*(NJS_TEX*)0x01D8BA48 = { 182, 8 };
	*(NJS_TEX*)0x01D8BA4C = { 245, 0 };
	*(NJS_TEX*)0x01D8BA50 = { 182, 8 };
	*(NJS_TEX*)0x01D8BA54 = { 182, 8 };
	*(NJS_TEX*)0x01D8BA60 = { 96, 247 };
	*(NJS_TEX*)0x01D8BA64 = { 96, 247 };
	*(NJS_TEX*)0x01D8BA68 = { 255, 255 };
	*(NJS_TEX*)0x01D8BA6C = { 255, 255 };

	// dxpc\stg09_casino\common\models\waka_models\casi_fulip02a.nja.sa1mdl
	*(NJS_TEX*)0x01D8BCC8 = { 254, 255 };
	*(NJS_TEX*)0x01D8BCCC = { 254, 255 };
	*(NJS_TEX*)0x01D8BCD0 = { 110, 248 };
	*(NJS_TEX*)0x01D8BCD4 = { 110, 248 };
	*(NJS_TEX*)0x01D8BCE0 = { 182, 6 };
	*(NJS_TEX*)0x01D8BCE4 = { 182, 6 };
	*(NJS_TEX*)0x01D8BCE8 = { 254, 255 };
	*(NJS_TEX*)0x01D8BCEC = { 413, 246 };
	*(NJS_TEX*)0x01D8BCF0 = { 254, 255 };
	*(NJS_TEX*)0x01D8BCF4 = { 413, 246 };
	*(NJS_TEX*)0x01D8BCF8 = { 110, 248 };
	*(NJS_TEX*)0x01D8BCFC = { 510, 230 };
	*(NJS_TEX*)0x01D8BD04 = { 326, 8 };
	*(NJS_TEX*)0x01D8BD08 = { 182, 6 };
	*(NJS_TEX*)0x01D8BD0C = { 262, 0 };
	*(NJS_TEX*)0x01D8BD10 = { 182, 6 };
	*(NJS_TEX*)0x01D8BD14 = { 262, 0 };
	*(NJS_TEX*)0x01D8BD18 = { 413, 246 };
	*(NJS_TEX*)0x01D8BD1C = { 413, 246 };
	*(NJS_TEX*)0x01D8BD20 = { 510, 230 };
	*(NJS_TEX*)0x01D8BD24 = { 510, 230 };
	*(NJS_TEX*)0x01D8BD28 = { 326, 8 };
	*(NJS_TEX*)0x01D8BD2C = { 326, 8 };
	*(NJS_TEX*)0x01D8BD30 = { 262, 0 };
	*(NJS_TEX*)0x01D8BD34 = { 262, 0 };

	// dxpc\stg09_casino\common\models\waka_models\casi_h_ita01a.nja.sa1mdl
	*(NJS_TEX*)0x01D8E098 = { 509, 0 };
	*(NJS_TEX*)0x01D8E09C = { 509, 254 };
	*(NJS_TEX*)0x01D8E0A0 = { 509, 0 };
	*(NJS_TEX*)0x01D8E0B4 = { 509, 0 };
	*(NJS_TEX*)0x01D8E0BC = { 509, 0 };
	*(NJS_TEX*)0x01D8E0C0 = { 509, 254 };
	*(NJS_TEX*)0x01D8E0C4 = { 509, 254 };
	*(NJS_TEX*)0x01D8E0D4 = { 509, 254 };

	// dxpc\stg09_casino\common\models\waka_models\casi_stop01a.nja.sa1mdl
	*(NJS_TEX*)0x01D9BA90 = { 199, 254 };
	*(NJS_TEX*)0x01D9BA98 = { 199, 0 };
	*(NJS_TEX*)0x01D9BAAC = { 199, 254 };

	// dxpc\stg09_casino\common\models\waka_models\casi_stop01a_k.nja.sa1mdl
	*(NJS_TEX*)0x01D9BBE8 = { 199, 254 };
	*(NJS_TEX*)0x01D9BBF0 = { 199, 0 };
	*(NJS_TEX*)0x01D9BC04 = { 199, 254 };

	// dxpc\stg09_casino\common\models\waka_models\casi_stop01b.nja.sa1mdl
	*(NJS_TEX*)0x01D9C3D0 = { 435, 37 };
	*(NJS_TEX*)0x01D9C3D4 = { 510, 127 };
	*(NJS_TEX*)0x01D9C3D8 = { 510, 127 };
	*(NJS_TEX*)0x01D9C3DC = { 435, 217 };
	*(NJS_TEX*)0x01D9C3E0 = { 435, 217 };
	*(NJS_TEX*)0x01D9C3E4 = { 254, 255 };
	*(NJS_TEX*)0x01D9C3E8 = { 254, 255 };
	*(NJS_TEX*)0x01D9C3EC = { 254, 127 };
	*(NJS_TEX*)0x01D9C3F0 = { 510, 127 };
	*(NJS_TEX*)0x01D9C3F4 = { 435, 37 };
	*(NJS_TEX*)0x01D9C3F8 = { 435, 37 };
	*(NJS_TEX*)0x01D9C3FC = { 254, 0 };
	*(NJS_TEX*)0x01D9C400 = { 435, 37 };
	*(NJS_TEX*)0x01D9C404 = { 435, 37 };
	*(NJS_TEX*)0x01D9C408 = { 510, 127 };
	*(NJS_TEX*)0x01D9C40C = { 510, 127 };
	*(NJS_TEX*)0x01D9C410 = { 435, 217 };
	*(NJS_TEX*)0x01D9C414 = { 254, 127 };
	*(NJS_TEX*)0x01D9C418 = { 254, 255 };
	*(NJS_TEX*)0x01D9C41C = { 74, 217 };
	*(NJS_TEX*)0x01D9C420 = { 254, 255 };
	*(NJS_TEX*)0x01D9C424 = { 74, 217 };
	*(NJS_TEX*)0x01D9C428 = { 254, 255 };
	*(NJS_TEX*)0x01D9C42C = { 74, 217 };
	*(NJS_TEX*)0x01D9C430 = { 254, 127 };
	*(NJS_TEX*)0x01D9C434 = { 254, 0 };
	*(NJS_TEX*)0x01D9C438 = { 435, 37 };
	*(NJS_TEX*)0x01D9C43C = { 254, 0 };
	*(NJS_TEX*)0x01D9C440 = { 435, 37 };
	*(NJS_TEX*)0x01D9C444 = { 254, 0 };
	*(NJS_TEX*)0x01D9C448 = { 74, 217 };
	*(NJS_TEX*)0x01D9C44C = { 74, 217 };
	*(NJS_TEX*)0x01D9C458 = { 74, 37 };
	*(NJS_TEX*)0x01D9C45C = { 74, 37 };
	*(NJS_TEX*)0x01D9C460 = { 74, 217 };
	*(NJS_TEX*)0x01D9C46C = { 74, 37 };
	*(NJS_TEX*)0x01D9C470 = { 74, 37 };
	*(NJS_TEX*)0x01D9C474 = { 74, 217 };
	*(NJS_TEX*)0x01D9C478 = { 74, 217 };
	*(NJS_TEX*)0x01D9C480 = { 254, 127 };
	*(NJS_TEX*)0x01D9C484 = { 74, 37 };
	*(NJS_TEX*)0x01D9C488 = { 254, 0 };
	*(NJS_TEX*)0x01D9C48C = { 74, 37 };
	*(NJS_TEX*)0x01D9C490 = { 254, 0 };
	*(NJS_TEX*)0x01D9C494 = { 74, 37 };
	*(NJS_TEX*)0x01D9C498 = { 254, 0 };
	*(NJS_TEX*)0x01D9C49C = { 510, 127 };
	*(NJS_TEX*)0x01D9C4A0 = { 435, 217 };
	*(NJS_TEX*)0x01D9C4A4 = { 435, 217 };
	*(NJS_TEX*)0x01D9C4A8 = { 254, 255 };
	*(NJS_TEX*)0x01D9C4AC = { 254, 255 };

	// dxpc\stg09_casino\common\models\waka_models\casi_stop01b_k.nja.sa1mdl
	*(NJS_TEX*)0x01D9C820 = { 435, 37 };
	*(NJS_TEX*)0x01D9C824 = { 510, 127 };
	*(NJS_TEX*)0x01D9C828 = { 510, 127 };
	*(NJS_TEX*)0x01D9C82C = { 435, 217 };
	*(NJS_TEX*)0x01D9C830 = { 435, 217 };
	*(NJS_TEX*)0x01D9C834 = { 254, 255 };
	*(NJS_TEX*)0x01D9C838 = { 254, 255 };
	*(NJS_TEX*)0x01D9C83C = { 254, 127 };
	*(NJS_TEX*)0x01D9C840 = { 510, 127 };
	*(NJS_TEX*)0x01D9C844 = { 435, 37 };
	*(NJS_TEX*)0x01D9C848 = { 435, 37 };
	*(NJS_TEX*)0x01D9C84C = { 254, 0 };
	*(NJS_TEX*)0x01D9C850 = { 435, 37 };
	*(NJS_TEX*)0x01D9C854 = { 435, 37 };
	*(NJS_TEX*)0x01D9C858 = { 510, 127 };
	*(NJS_TEX*)0x01D9C85C = { 510, 127 };
	*(NJS_TEX*)0x01D9C860 = { 435, 217 };
	*(NJS_TEX*)0x01D9C864 = { 254, 127 };
	*(NJS_TEX*)0x01D9C868 = { 254, 255 };
	*(NJS_TEX*)0x01D9C86C = { 74, 217 };
	*(NJS_TEX*)0x01D9C870 = { 254, 255 };
	*(NJS_TEX*)0x01D9C874 = { 74, 217 };
	*(NJS_TEX*)0x01D9C878 = { 254, 255 };
	*(NJS_TEX*)0x01D9C87C = { 74, 217 };
	*(NJS_TEX*)0x01D9C880 = { 254, 127 };
	*(NJS_TEX*)0x01D9C884 = { 254, 0 };
	*(NJS_TEX*)0x01D9C888 = { 435, 37 };
	*(NJS_TEX*)0x01D9C88C = { 254, 0 };
	*(NJS_TEX*)0x01D9C890 = { 435, 37 };
	*(NJS_TEX*)0x01D9C894 = { 254, 0 };
	*(NJS_TEX*)0x01D9C898 = { 74, 217 };
	*(NJS_TEX*)0x01D9C89C = { 74, 217 };
	*(NJS_TEX*)0x01D9C8A8 = { 74, 37 };
	*(NJS_TEX*)0x01D9C8AC = { 74, 37 };
	*(NJS_TEX*)0x01D9C8B0 = { 74, 217 };
	*(NJS_TEX*)0x01D9C8BC = { 74, 37 };
	*(NJS_TEX*)0x01D9C8C0 = { 74, 37 };
	*(NJS_TEX*)0x01D9C8C4 = { 74, 217 };
	*(NJS_TEX*)0x01D9C8C8 = { 74, 217 };
	*(NJS_TEX*)0x01D9C8D0 = { 254, 127 };
	*(NJS_TEX*)0x01D9C8D4 = { 74, 37 };
	*(NJS_TEX*)0x01D9C8D8 = { 254, 0 };
	*(NJS_TEX*)0x01D9C8DC = { 74, 37 };
	*(NJS_TEX*)0x01D9C8E0 = { 254, 0 };
	*(NJS_TEX*)0x01D9C8E4 = { 74, 37 };
	*(NJS_TEX*)0x01D9C8E8 = { 254, 0 };
	*(NJS_TEX*)0x01D9C8EC = { 510, 127 };
	*(NJS_TEX*)0x01D9C8F0 = { 435, 217 };
	*(NJS_TEX*)0x01D9C8F4 = { 435, 217 };
	*(NJS_TEX*)0x01D9C8F8 = { 254, 255 };
	*(NJS_TEX*)0x01D9C8FC = { 254, 255 };

	// dxpc\stg09_casino\common\models\waka_models\egg_moto.nja.sa1mdl
	*(NJS_TEX*)0x01DD18D8 = { 1487, -930 };
	*(NJS_TEX*)0x01DD18DC = { 1275, -930 };
	*(NJS_TEX*)0x01DD18E0 = { 1487, -752 };
	*(NJS_TEX*)0x01DD18E4 = { 1275, -752 };
	*(NJS_TEX*)0x01DD18E8 = { 1487, -416 };
	*(NJS_TEX*)0x01DD18EC = { 1275, -416 };
	*(NJS_TEX*)0x01DD18F0 = { 1486, -80 };
	*(NJS_TEX*)0x01DD18F4 = { 1275, -80 };
	*(NJS_TEX*)0x01DD18F8 = { 1487, 165 };
	*(NJS_TEX*)0x01DD18FC = { 1275, 165 };
	*(NJS_TEX*)0x01DD1900 = { 1275, 255 };
	*(NJS_TEX*)0x01DD1904 = { 212, 165 };
	*(NJS_TEX*)0x01DD1908 = { 0, 255 };
	*(NJS_TEX*)0x01DD190C = { 425, 165 };
	*(NJS_TEX*)0x01DD1910 = { 637, 165 };
	*(NJS_TEX*)0x01DD1914 = { 425, -80 };
	*(NJS_TEX*)0x01DD1918 = { 637, -80 };
	*(NJS_TEX*)0x01DD191C = { 425, -416 };
	*(NJS_TEX*)0x01DD1920 = { 637, -416 };
	*(NJS_TEX*)0x01DD1924 = { 425, -752 };
	*(NJS_TEX*)0x01DD1928 = { 637, -752 };
	*(NJS_TEX*)0x01DD192C = { 425, -930 };
	*(NJS_TEX*)0x01DD1930 = { 637, -930 };
	*(NJS_TEX*)0x01DD1934 = { 0, -1020 };
	*(NJS_TEX*)0x01DD1938 = { 850, -930 };
	*(NJS_TEX*)0x01DD193C = { 0, -1020 };
	*(NJS_TEX*)0x01DD1940 = { 425, -930 };
	*(NJS_TEX*)0x01DD1944 = { 212, -930 };
	*(NJS_TEX*)0x01DD1948 = { 425, -752 };
	*(NJS_TEX*)0x01DD194C = { 212, -752 };
	*(NJS_TEX*)0x01DD1950 = { 425, -416 };
	*(NJS_TEX*)0x01DD1954 = { 212, -416 };
	*(NJS_TEX*)0x01DD1958 = { 425, -80 };
	*(NJS_TEX*)0x01DD195C = { 211, -80 };
	*(NJS_TEX*)0x01DD1960 = { 425, 165 };
	*(NJS_TEX*)0x01DD1964 = { 212, 165 };
	*(NJS_TEX*)0x01DD1968 = { 0, 255 };
	*(NJS_TEX*)0x01DD196C = { 637, 165 };
	*(NJS_TEX*)0x01DD1970 = { 850, 165 };
	*(NJS_TEX*)0x01DD1974 = { 637, -80 };
	*(NJS_TEX*)0x01DD1978 = { 849, -80 };
	*(NJS_TEX*)0x01DD197C = { 637, -416 };
	*(NJS_TEX*)0x01DD1980 = { 850, -416 };
	*(NJS_TEX*)0x01DD1984 = { 637, -752 };
	*(NJS_TEX*)0x01DD1988 = { 850, -752 };
	*(NJS_TEX*)0x01DD198C = { 637, -930 };
	*(NJS_TEX*)0x01DD1990 = { 850, -930 };
	*(NJS_TEX*)0x01DD1994 = { 850, 165 };
	*(NJS_TEX*)0x01DD1998 = { 1275, 255 };
	*(NJS_TEX*)0x01DD199C = { 1062, 165 };
	*(NJS_TEX*)0x01DD19A0 = { 1275, 165 };
	*(NJS_TEX*)0x01DD19A4 = { 1063, -80 };
	*(NJS_TEX*)0x01DD19A8 = { 1275, -80 };
	*(NJS_TEX*)0x01DD19AC = { 1062, -416 };
	*(NJS_TEX*)0x01DD19B0 = { 1275, -416 };
	*(NJS_TEX*)0x01DD19B4 = { 1062, -752 };
	*(NJS_TEX*)0x01DD19B8 = { 1275, -752 };
	*(NJS_TEX*)0x01DD19BC = { 1062, -930 };
	*(NJS_TEX*)0x01DD19C0 = { 1275, -930 };
	*(NJS_TEX*)0x01DD19C4 = { 1275, -1020 };
	*(NJS_TEX*)0x01DD19C8 = { 1487, -930 };
	*(NJS_TEX*)0x01DD19CC = { 850, 165 };
	*(NJS_TEX*)0x01DD19D0 = { 1062, 165 };
	*(NJS_TEX*)0x01DD19D4 = { 849, -80 };
	*(NJS_TEX*)0x01DD19D8 = { 1063, -80 };
	*(NJS_TEX*)0x01DD19DC = { 850, -416 };
	*(NJS_TEX*)0x01DD19E0 = { 1062, -416 };
	*(NJS_TEX*)0x01DD19E4 = { 850, -752 };
	*(NJS_TEX*)0x01DD19E8 = { 1062, -752 };
	*(NJS_TEX*)0x01DD19EC = { 850, -930 };
	*(NJS_TEX*)0x01DD19F0 = { 1062, -930 };
	*(NJS_TEX*)0x01DD19F4 = { 1275, -1020 };

	// dxpc\stg09_casino\common\models\waka_models\egg_sita.nja.sa1mdl
	*(NJS_TEX*)0x01DD2168 = { 1486, -80 };
	*(NJS_TEX*)0x01DD216C = { 1275, -80 };
	*(NJS_TEX*)0x01DD2170 = { 1487, 165 };
	*(NJS_TEX*)0x01DD2174 = { 1275, 165 };
	*(NJS_TEX*)0x01DD2178 = { 1275, 255 };
	*(NJS_TEX*)0x01DD217C = { 1062, 165 };
	*(NJS_TEX*)0x01DD2180 = { 850, 165 };
	*(NJS_TEX*)0x01DD2184 = { 849, -80 };
	*(NJS_TEX*)0x01DD2188 = { 637, -80 };
	*(NJS_TEX*)0x01DD218C = { 1275, -80 };
	*(NJS_TEX*)0x01DD2190 = { 1275, 165 };
	*(NJS_TEX*)0x01DD2194 = { 1063, -80 };
	*(NJS_TEX*)0x01DD2198 = { 1062, 165 };
	*(NJS_TEX*)0x01DD219C = { 849, -80 };
	*(NJS_TEX*)0x01DD21A0 = { 425, -80 };
	*(NJS_TEX*)0x01DD21A4 = { 211, -80 };
	*(NJS_TEX*)0x01DD21A8 = { 425, 165 };
	*(NJS_TEX*)0x01DD21AC = { 212, 165 };
	*(NJS_TEX*)0x01DD21B0 = { 211, -80 };
	*(NJS_TEX*)0x01DD21B4 = { 212, -416 };
	*(NJS_TEX*)0x01DD21B8 = { 425, -80 };
	*(NJS_TEX*)0x01DD21BC = { 425, -416 };
	*(NJS_TEX*)0x01DD21C0 = { 637, -80 };
	*(NJS_TEX*)0x01DD21C4 = { 637, -416 };
	*(NJS_TEX*)0x01DD21C8 = { 849, -80 };
	*(NJS_TEX*)0x01DD21CC = { 850, -416 };
	*(NJS_TEX*)0x01DD21D0 = { 1063, -80 };
	*(NJS_TEX*)0x01DD21D4 = { 1062, -416 };
	*(NJS_TEX*)0x01DD21D8 = { 1275, -80 };
	*(NJS_TEX*)0x01DD21DC = { 1275, -416 };
	*(NJS_TEX*)0x01DD21E0 = { 1486, -80 };
	*(NJS_TEX*)0x01DD21E4 = { 1487, -416 };
	*(NJS_TEX*)0x01DD21E8 = { 637, -80 };
	*(NJS_TEX*)0x01DD21EC = { 425, -80 };
	*(NJS_TEX*)0x01DD21F0 = { 637, 165 };
	*(NJS_TEX*)0x01DD21F4 = { 425, 165 };
	*(NJS_TEX*)0x01DD21F8 = { 0, 255 };
	*(NJS_TEX*)0x01DD21FC = { 212, 165 };
	*(NJS_TEX*)0x01DD2200 = { 637, -80 };
	*(NJS_TEX*)0x01DD2204 = { 637, 165 };
	*(NJS_TEX*)0x01DD2208 = { 850, 165 };
	*(NJS_TEX*)0x01DD220C = { 0, 255 };

	// dxpc\stg09_casino\common\models\waka_models\egg_ue.nja.sa1mdl
	*(NJS_TEX*)0x01DD1DF8 = { 1275, -416 };
	*(NJS_TEX*)0x01DD1DFC = { 1062, -416 };
	*(NJS_TEX*)0x01DD1E00 = { 1062, -752 };
	*(NJS_TEX*)0x01DD1E04 = { 850, -752 };
	*(NJS_TEX*)0x01DD1E08 = { 850, -930 };
	*(NJS_TEX*)0x01DD1E0C = { 637, -930 };
	*(NJS_TEX*)0x01DD1E10 = { 0, -1020 };
	*(NJS_TEX*)0x01DD1E14 = { 425, -930 };
	*(NJS_TEX*)0x01DD1E18 = { 212, -930 };
	*(NJS_TEX*)0x01DD1E1C = { 425, -752 };
	*(NJS_TEX*)0x01DD1E20 = { 212, -752 };
	*(NJS_TEX*)0x01DD1E24 = { 425, -416 };
	*(NJS_TEX*)0x01DD1E28 = { 212, -416 };
	*(NJS_TEX*)0x01DD1E2C = { 1275, -416 };
	*(NJS_TEX*)0x01DD1E30 = { 1062, -752 };
	*(NJS_TEX*)0x01DD1E34 = { 1275, -752 };
	*(NJS_TEX*)0x01DD1E38 = { 1062, -930 };
	*(NJS_TEX*)0x01DD1E3C = { 1275, -930 };
	*(NJS_TEX*)0x01DD1E40 = { 1275, -1020 };
	*(NJS_TEX*)0x01DD1E44 = { 1487, -930 };
	*(NJS_TEX*)0x01DD1E48 = { 1487, -930 };
	*(NJS_TEX*)0x01DD1E4C = { 1275, -930 };
	*(NJS_TEX*)0x01DD1E50 = { 1487, -752 };
	*(NJS_TEX*)0x01DD1E54 = { 1275, -752 };
	*(NJS_TEX*)0x01DD1E58 = { 1487, -416 };
	*(NJS_TEX*)0x01DD1E5C = { 1275, -416 };
	*(NJS_TEX*)0x01DD1E60 = { 1062, -752 };
	*(NJS_TEX*)0x01DD1E64 = { 1062, -930 };
	*(NJS_TEX*)0x01DD1E68 = { 850, -930 };
	*(NJS_TEX*)0x01DD1E6C = { 1275, -1020 };
	*(NJS_TEX*)0x01DD1E70 = { 850, -416 };
	*(NJS_TEX*)0x01DD1E74 = { 1062, -416 };
	*(NJS_TEX*)0x01DD1E78 = { 850, -752 };
	*(NJS_TEX*)0x01DD1E7C = { 637, -930 };
	*(NJS_TEX*)0x01DD1E80 = { 850, -752 };
	*(NJS_TEX*)0x01DD1E84 = { 637, -752 };
	*(NJS_TEX*)0x01DD1E88 = { 850, -416 };
	*(NJS_TEX*)0x01DD1E8C = { 637, -416 };
	*(NJS_TEX*)0x01DD1E90 = { 637, -930 };
	*(NJS_TEX*)0x01DD1E94 = { 425, -930 };
	*(NJS_TEX*)0x01DD1E98 = { 637, -752 };
	*(NJS_TEX*)0x01DD1E9C = { 425, -752 };
	*(NJS_TEX*)0x01DD1EA0 = { 637, -416 };
	*(NJS_TEX*)0x01DD1EA4 = { 425, -416 };

	// dxpc\stg09_casino\common\models\waka_models\pian_fishing_karada.nja.sa1mdl
	*(NJS_TEX*)0x01DD8C1C = { 0, -255 };
	*(NJS_TEX*)0x01DD8C20 = { 510, 255 };
	*(NJS_TEX*)0x01DD8C24 = { 510, -254 };

	// dxpc\stg09_casino\common\models\waka_models\pian_fly_karada.nja.sa1mdl
	*(NJS_TEX*)0x01DD50C4 = { 0, -255 };
	*(NJS_TEX*)0x01DD50C8 = { 510, 255 };
	*(NJS_TEX*)0x01DD50CC = { 510, -254 };

	// dxpc\stg09_casino\common\models\waka_models\pian_pach_karada.nja.sa1mdl
	*(NJS_TEX*)0x01DD34CC = { 0, -255 };
	*(NJS_TEX*)0x01DD34D0 = { 510, 255 };
	*(NJS_TEX*)0x01DD34D4 = { 510, -254 };

	// dxpc\stg09_casino\common\models\waka_models\pian_sleep_karada.nja.sa1mdl
	*(NJS_TEX*)0x01DDA78C = { 0, -255 };
	*(NJS_TEX*)0x01DDA790 = { 510, 255 };
	*(NJS_TEX*)0x01DDA794 = { 510, -254 };

	// dxpc\stg09_casino\common\models\waka_models\pian_walk_karada.nja.sa1mdl
	*(NJS_TEX*)0x01DD6CBC = { 0, -255 };
	*(NJS_TEX*)0x01DD6CC0 = { 510, 255 };
	*(NJS_TEX*)0x01DD6CC4 = { 510, -254 };

	// dxpc\stg09_casino\common\models\waka_models\uv_sita_hutiuv.nja.sa1mdl
	*(NJS_TEX*)0x01DDE990 = { 502, 175 };
	*(NJS_TEX*)0x01DDE994 = { 502, -4765 };
	*(NJS_TEX*)0x01DDE998 = { 7, 175 };
	*(NJS_TEX*)0x01DDE99C = { 7, -4765 };
	*(NJS_TEX*)0x01DDE9A0 = { 7, -1737 };
	*(NJS_TEX*)0x01DDE9A4 = { 502, -1737 };
	*(NJS_TEX*)0x01DDE9A8 = { 7, -462 };
	*(NJS_TEX*)0x01DDE9AC = { 502, -462 };
	*(NJS_TEX*)0x01DDE9B0 = { 7, 175 };
	*(NJS_TEX*)0x01DDE9B4 = { 502, 175 };
	*(NJS_TEX*)0x01DDE9B8 = { 502, 175 };
	*(NJS_TEX*)0x01DDE9BC = { 502, -4765 };
	*(NJS_TEX*)0x01DDE9C0 = { 7, 175 };
	*(NJS_TEX*)0x01DDE9C4 = { 7, -4765 };
	*(NJS_TEX*)0x01DDE9C8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDE9CC = { 502, -4765 };
	*(NJS_TEX*)0x01DDE9D0 = { 7, -4287 };
	*(NJS_TEX*)0x01DDE9D4 = { 502, -4287 };
	*(NJS_TEX*)0x01DDE9D8 = { 7, -462 };
	*(NJS_TEX*)0x01DDE9DC = { 502, -462 };
	*(NJS_TEX*)0x01DDE9E0 = { 7, 175 };
	*(NJS_TEX*)0x01DDE9E4 = { 502, 175 };
	*(NJS_TEX*)0x01DDE9E8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDE9EC = { 7, -4287 };
	*(NJS_TEX*)0x01DDE9F0 = { 502, -4765 };
	*(NJS_TEX*)0x01DDE9F4 = { 502, -4287 };
	*(NJS_TEX*)0x01DDE9F8 = { 502, 175 };
	*(NJS_TEX*)0x01DDE9FC = { 502, -462 };
	*(NJS_TEX*)0x01DDEA00 = { 7, 175 };
	*(NJS_TEX*)0x01DDEA04 = { 7, -462 };
	*(NJS_TEX*)0x01DDEA08 = { 502, 175 };
	*(NJS_TEX*)0x01DDEA0C = { 502, -462 };
	*(NJS_TEX*)0x01DDEA10 = { 7, 175 };
	*(NJS_TEX*)0x01DDEA14 = { 7, -462 };
	*(NJS_TEX*)0x01DDEA18 = { 502, -4287 };
	*(NJS_TEX*)0x01DDEA1C = { 502, -4765 };
	*(NJS_TEX*)0x01DDEA20 = { 7, -4287 };
	*(NJS_TEX*)0x01DDEA24 = { 7, -4765 };
	*(NJS_TEX*)0x01DDEA28 = { 502, -2374 };
	*(NJS_TEX*)0x01DDEA2C = { 7, -2374 };
	*(NJS_TEX*)0x01DDEA30 = { 502, -4287 };
	*(NJS_TEX*)0x01DDEA34 = { 7, -4287 };
	*(NJS_TEX*)0x01DDEA38 = { 502, -4765 };
	*(NJS_TEX*)0x01DDEA3C = { 7, -4765 };

	// dxpc\stg09_casino\common\models\waka_models\uv_sita_utiuv.nja.sa1mdl
	*(NJS_TEX*)0x01DDEDC0 = { 502, -4765 };
	*(NJS_TEX*)0x01DDEDC4 = { 502, -3968 };
	*(NJS_TEX*)0x01DDEDC8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDEDCC = { 7, -4765 };
	*(NJS_TEX*)0x01DDEDD0 = { 7, -3968 };
	*(NJS_TEX*)0x01DDEDD4 = { 502, -3968 };
	*(NJS_TEX*)0x01DDEDD8 = { 7, -462 };
	*(NJS_TEX*)0x01DDEDDC = { 502, -462 };
	*(NJS_TEX*)0x01DDEDE0 = { 502, 175 };
	*(NJS_TEX*)0x01DDEDE4 = { 7, 175 };
	*(NJS_TEX*)0x01DDEDE8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDEDEC = { 502, 175 };
	*(NJS_TEX*)0x01DDEDF0 = { 502, -4765 };
	*(NJS_TEX*)0x01DDEDF4 = { 7, 175 };
	*(NJS_TEX*)0x01DDEDF8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDEDFC = { 502, 175 };
	*(NJS_TEX*)0x01DDEE00 = { 502, -4765 };
	*(NJS_TEX*)0x01DDEE04 = { 502, 175 };
	*(NJS_TEX*)0x01DDEE08 = { 7, 175 };
	*(NJS_TEX*)0x01DDEE0C = { 7, -462 };

	// dxpc\stg09_casino\common\models\waka_models\uv_ue_huti.nja.sa1mdl
	*(NJS_TEX*)0x01DDF000 = { 7, -4765 };
	*(NJS_TEX*)0x01DDF004 = { 502, -4765 };
	*(NJS_TEX*)0x01DDF008 = { 7, 175 };
	*(NJS_TEX*)0x01DDF00C = { 502, 175 };
	*(NJS_TEX*)0x01DDF010 = { 7, 175 };
	*(NJS_TEX*)0x01DDF014 = { 502, 175 };
	*(NJS_TEX*)0x01DDF018 = { 7, -462 };
	*(NJS_TEX*)0x01DDF01C = { 502, -462 };
	*(NJS_TEX*)0x01DDF020 = { 7, -4287 };
	*(NJS_TEX*)0x01DDF024 = { 502, -4287 };
	*(NJS_TEX*)0x01DDF028 = { 7, -4765 };
	*(NJS_TEX*)0x01DDF02C = { 502, -4765 };
	*(NJS_TEX*)0x01DDF030 = { 502, 175 };
	*(NJS_TEX*)0x01DDF034 = { 7, 175 };
	*(NJS_TEX*)0x01DDF038 = { 502, -4765 };
	*(NJS_TEX*)0x01DDF03C = { 7, -4765 };

	// dxpc\stg09_casino\common\models\waka_models\uv_ue_sinen.nja.sa1mdl
	*(NJS_TEX*)0x01DDF21C = { 2040, 254 };
	*(NJS_TEX*)0x01DDF220 = { 0, 254 };
	*(NJS_TEX*)0x01DDF224 = { 2040, -2295 };
	*(NJS_TEX*)0x01DDF228 = { 0, -2295 };
	*(NJS_TEX*)0x01DDF22C = { 0, 254 };
	*(NJS_TEX*)0x01DDF230 = { 2040, 254 };
	*(NJS_TEX*)0x01DDF234 = { 0, -2295 };
	*(NJS_TEX*)0x01DDF238 = { 2040, -2295 };

	// dxpc\stg09_casino\common\models\waka_models\uv_ue_uti.nja.sa1mdl
	*(NJS_TEX*)0x01DDF3B4 = { 7, -4765 };
	*(NJS_TEX*)0x01DDF3B8 = { 7, 175 };
	*(NJS_TEX*)0x01DDF3BC = { 502, -4765 };
	*(NJS_TEX*)0x01DDF3C0 = { 502, 175 };
	*(NJS_TEX*)0x01DDF3C4 = { 7, 175 };
	*(NJS_TEX*)0x01DDF3C8 = { 7, -4765 };
	*(NJS_TEX*)0x01DDF3CC = { 502, 175 };
	*(NJS_TEX*)0x01DDF3D0 = { 502, -4765 };
}