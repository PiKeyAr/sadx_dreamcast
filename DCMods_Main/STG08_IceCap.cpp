#include "stdafx.h"
#include "STG08_IceCap.h"

static bool ModelsLoaded_STG08;

void RenderSnowboardEffect(NJS_OBJECT* obj, float scale)
{
	late_z_ofs___ = 2000.0f;
	late_DrawObjectClip(obj, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
}

void FixSnowflake(NJS_SPRITE *sp, Int n, NJD_SPRITE attr, LATE zfunc_type)
{
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	late_DrawSprite3D(sp, n, attr, zfunc_type);
}

void Obj_Icecap_DoColFlagThingsX(int some_flags)
{
	Uint32 *ptr; // ecx
	int count; // edx
	Uint32 flags; // eax
	unsigned __int32 _flags; // eax
	if (IsLevelLoaded(LevelAndActIDs_IceCap4))
	{
		if (GetLevelLandtable(LevelAndActIDs_IceCap4)->ssCount - 1 >= 0)
		{
			ptr = (Uint32*)&GetLevelLandtable(LevelAndActIDs_IceCap4)->pLandEntry->slAttribute;
			count = GetLevelLandtable(LevelAndActIDs_IceCap4)->ssCount;
			do
			{
				flags = *ptr;
				if (*ptr & 0x60000000)
				{
					if (some_flags & *(ptr - 1))
					{
						//PrintDebug("Solid\n");
						_flags = flags | SurfaceFlags_Solid;
					}
					else
					{
						//PrintDebug("Not solid\n");
						_flags = flags & ~SurfaceFlags_Solid;
					}
					*ptr = _flags;
				}
				ptr += 9;
				--count;
			} while (count);
		}
	}
}

static void __declspec(naked) Obj_Icecap_DoColFlagThings_a()
{
	__asm
	{
		push esi // some_flags

		// Call your __cdecl function here:
		call Obj_Icecap_DoColFlagThingsX

		pop esi // some_flags
		retn
	}
}

void IceCapTransparentSkybox1(NJS_OBJECT* obj)
{
	late_z_ofs___ = -40000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f);
	late_z_ofs___ = 0;
}

void IceCapTransparentSkybox2(NJS_OBJECT* obj)
{
	late_z_ofs___ = -30000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f);
	late_z_ofs___ = 0;
}

void IceCapTransparentSkybox3(NJS_OBJECT* obj)
{
	late_z_ofs___ = -20000.0f;
	late_DrawObjectClip(obj, LATE_MAT, 1.0f);
	late_z_ofs___ = 0;
}

void IceCap_Init()
{
	ReplaceCAM("CAM0800S");
	ReplaceCAM("CAM0801S");
	ReplaceCAM("CAM0802S");
	ReplaceCAM("CAM0803B");
	ReplaceSET("SET0800S");
	ReplaceSET("SET0801S");
	ReplaceSET("SET0802M");
	ReplaceSET("SET0802S");
	ReplaceSET("SET0803B");
	ReplacePVM("BG_ICECAP");
	ReplacePVM("ICECAP01");
	ReplacePVM("ICECAP02");
	ReplacePVM("ICECAP03");
	ReplacePVM("OBJ_ICECAP");
	ReplacePVM("OBJ_ICECAP2");
	ReplacePVR("MIW_B001");
	ReplacePVR("MTX_BOARD0");
	ReplacePVR("SB_BOARD1");
	for (int i = 0; i < 3; i++)
	{
		pClipMap_Stg08[0][i].f32Far = -3000.0;
		pClipMap_Stg08[1][i].f32Far = -4000.0;
		pClipMap_Stg08[2][i].f32Far = -8000.0;

		pFogTable_Stg08[0][i].Col = 0xFFFFFFFF;
		pFogTable_Stg08[0][i].f32StartZ = 1000.0f;
		pFogTable_Stg08[0][i].f32EndZ = 2500.0f;
		pFogTable_Stg08[0][i].u8Enable = 1;

		pFogTable_Stg08[1][i].Col = 0xFF000060;
		pFogTable_Stg08[1][i].f32StartZ = 1600.0f;
		pFogTable_Stg08[1][i].f32EndZ = 4000.0f;
		pFogTable_Stg08[1][i].u8Enable = 1;

		pFogTable_Stg08[2][i].f32StartZ = 2500.0f;
		pFogTable_Stg08[2][i].Col = 0xFFFFFFFF;
		pFogTable_Stg08[2][i].u8Enable = 1;

		pFogTable_Stg08[3][i].Col = 0xFF000060;
		pFogTable_Stg08[3][i].f32StartZ = 800.0f;
		pFogTable_Stg08[3][i].f32EndZ = 3800.0f;
		pFogTable_Stg08[3][i].u8Enable = 1;

		pScale_Stg08[2][i].x = 1.0f;
		pScale_Stg08[2][i].x = 1.0f;
		pScale_Stg08[2][i].x = 1.0f;
	}
}

void IceCap_Load()
{
	LevelLoader(LevelAndActIDs_IceCap1, "system\\data\\stg08_icecap\\landtable0800.c.sa1lvl", &texlist_icecap01);
	LevelLoader(LevelAndActIDs_IceCap2, "system\\data\\stg08_icecap\\landtable0801.c.sa1lvl", &texlist_icecap02);
	LevelLoader(LevelAndActIDs_IceCap3, "system\\data\\stg08_icecap\\landtable0802.c.sa1lvl", &texlist_icecap03);
	LevelLoader(LevelAndActIDs_IceCap4, "system\\data\\stg08_icecap\\landtable0803.c.sa1lvl", &texlist_icecap02);
	if (!ModelsLoaded_STG08)
	{
		// Texlists
		texlist_piece.textures = (NJS_TEXNAME*)&texlist_obj_icecap.textures[58]; // Ice fragment (availanche)
		texlist_yuki.textures = (NJS_TEXNAME*)&texlist_obj_icecap.textures[96]; // Snowflake (avalanche)
		WriteJump((void*)0x004E91C0, Obj_Icecap_DoColFlagThings_a); // Part of Rd_Snow in X360
		// Objects		
		object_snowball_m_atama_atama.basicdxmodel->mats[7].attrflags &= ~NJD_FLAG_USE_ALPHA; // Unnecessary alpha causing errors
		object_snowball_m_atama_atama.child->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA; // Unnecessary alpha causing errors
		object_snowball_m_atama_atama.child->sibling->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA; // Unnecessary alpha causing errors
		object_snow_saku01_saku01 = *LoadModel("system\\data\\stg08_icecap\\common\\models\\saku_all1_saku01_b.nja.sa1mdl"); // OSaku01
		object_snow_saku01_b_saku01_b = *LoadModel("system\\data\\stg08_icecap\\common\\models\\saku_all1_saku02_b.nja.sa1mdl"); // OSaku01b
		object_snow_saku02_saku02 = *LoadModel("system\\data\\stg08_icecap\\common\\models\\saku_all1_saku02.nja.sa1mdl"); // OSaku02
		object_snow_saku02_b_saku02_b = *LoadModel("system\\data\\stg08_icecap\\common\\models\\saku_all1_saku01.nja.sa1mdl"); // OSaku02b
		object_cp_ic_futa_l_futa_l = *LoadModel("system\\data\\stg08_icecap\\common\\models\\cp_ic_futa_l.nja.sa1mdl"); // OFutaL
		object_cp_ic_objice_l_objice_l = *LoadModel("system\\data\\stg08_icecap\\common\\models\\cp_ic_objice_l.nja.sa1mdl"); // OFutaL broken
		object_obj_turara_obj_turara = *LoadModel("system\\data\\stg08_icecap\\common\\models\\obj_turara.nja.sa1mdl"); // Icicle inner part
		object_jpdai_all1_jpdai01_jpdai01 = *LoadModel("system\\data\\stg08_icecap\\common\\models\\jpdai_all1_jpdai01.nja.sa1mdl"); // OIceJmp
		object_bigturara_under_a_under_a = *LoadModel("system\\data\\stg08_icecap\\common\\models\\bigturara_under_a.nja.sa1mdl"); // Giant icicle (edited meshsets)
		// Various effect fixes
		WriteCall((void*)0x004EA454, RenderSnowboardEffect);
		WriteCall((void*)0x004EB7D0, FixSnowflake); // Avalanche blending mode
		WriteCall((void*)0x004F5B6F, FixSnowflake); // Snowflake blending fix
		WriteCall((void*)0x004EB948, FixSnowflake); // Snowflake blending fix 2
		// Skybox effect
		WriteCall((void*)0x004E9A5E, IceCapTransparentSkybox1); // Act 1
		WriteCall((void*)0x004E9C24, IceCapTransparentSkybox1); // Act 3
		WriteCall((void*)0x004E9A68, IceCapTransparentSkybox2); // Act 1
		WriteCall((void*)0x004E9C2E, IceCapTransparentSkybox2); // Act 3
		WriteCall((void*)0x004E9A72, IceCapTransparentSkybox3); // Act 1
		WriteCall((void*)0x004E9C5D, IceCapTransparentSkybox3); // Act 3
		// Replace model pointers in rendering functions to change their draw order
		WriteData((NJS_OBJECT**)0x004E9A5A, &object_sora98_oya_oya); // Act 1
		WriteData((NJS_OBJECT**)0x004E9C20, &object_sora98_oya_oya); // Act 3
		WriteData((NJS_OBJECT**)0x004E9A64, &object_sora55_aida01a_aida01a); // Act 1
		WriteData((NJS_OBJECT**)0x004E9C2A, &object_sora55_aida01a_aida01a); // Act 3
		ModelsLoaded_STG08 = true;
	}
}

void IceCap_OnFrame()
{
	if (ssStageNumber != LevelIDs_IceCap)
		return;
	if (ssActNumber != 2)
		return;
	// Dynamic fog in snowboard section
	if (playertwp[0] && !ChkPause())
	{
		if (playertwp[0]->pos.x <= -8000)
		{
			gFog.f32StartZ = max(800, gFog.f32StartZ - 16);
			gFog.f32EndZ = max(3000, gFog.f32EndZ - 8);
		}
		else if (playertwp[0]->pos.x >= -5000)
		{
			gFog.f32StartZ = min(pFogTable_Stg08[2][0].f32StartZ, gFog.f32StartZ + 8);
			gFog.f32EndZ = min(pFogTable_Stg08[2][0].f32EndZ, gFog.f32EndZ + 16);
		}
	}
}