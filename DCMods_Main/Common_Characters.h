#pragma once

#include <SADXModLoader.h>

void ScaleEnvironmentMap(float value1 = -1, float value2 = -1, float value3 = -1, float value4 = -1);
void RestoreEnvironmentMap();
void ChaosFixes_Init();

DataPointer(unsigned __int8, flagSuperSonicMode, 0x03C55D45); // 1 if playing as Super Sonic

DataPointer(NJS_ACTION, action_tr2henkei_tr2_tf_event_s_tr2rt, 0x028988FC);
DataPointer(NJS_ACTION, action_g_g0001_eggman, 0x0089E254);
DataPointer(NJS_ACTION, action_gm_gm0004_eggmoble, 0x02CD393C);
DataPointer(NJS_OBJECT, object_er_9000_eggmanrobo_er_head, 0x031A4DFC);

DataArray(TEX_PVMTABLE, evpvm_12, 0x02BD5FD0, 12); // "Chaos is... - Is what?" cutscene PVM list

// SADX NPC body parts
DataPointer(NJS_OBJECT, object_tikal_a_l_a4, 0x008CE058); // Tikal fingers DX
DataPointer(NJS_OBJECT, object_tikal_a_l_a7, 0x008CC658); // Tikal fingers DX
DataPointer(NJS_OBJECT, object_g_g0000_eggman_r_a, 0x008961E0); // Eggman fingers DX
DataPointer(NJS_OBJECT, object_g_g0000_eggman_r_b, 0x008964CC); // Eggman fingers DX
DataPointer(NJS_OBJECT, object_g_g0000_eggman_l_b, 0x008980DC); // Eggman fingers DX
DataPointer(NJS_OBJECT, object_g_g0000_eggman_l_a, 0x00897DE0); // Eggman fingers DX
DataPointer(NJS_OBJECT, object_gm_gm0000_eggmoble_r_a, 0x02EE22C0); // Eggman (Eggmobile) fingers DX
DataPointer(NJS_OBJECT, object_gm_gm0000_eggmoble_r_b, 0x02EE25AC); // Eggman (Eggmobile) fingers DX
DataPointer(NJS_OBJECT, object_gm_gm0000_eggmoble_l_b, 0x02EE4194); // Eggman (Eggmobile) fingers DX
DataPointer(NJS_OBJECT, object_gm_gm0000_eggmoble_l_a, 0x02EE3E98); // Eggman (Eggmobile) fingers DX

DataPointer(NJS_OBJECT, object_e101old_kihon0_e101old_e_reye, 0x014D857C);
DataPointer(NJS_OBJECT, object_e101old_kihon0_e101old_e_leye, 0x014D887C);
DataPointer(NJS_OBJECT, object_e101old_kihon0_e101old_e_right, 0x014D6504);
DataPointer(NJS_OBJECT, object_e101old_kihon0_e101old_e_head, 0x014D943C);
DataPointer(NJS_OBJECT, _object_e101old_kihon0_e101old_e_head, 0x0312F714);
DataPointer(NJS_OBJECT, object_e103_kihon0_e103_e_head, 0x0030AB08C);
DataPointer(NJS_OBJECT, object_e104_kihon0_e104_e_head, 0x030A290C);
DataPointer(NJS_OBJECT, object_e105old_kihon0_e105old_e_head, 0x0309A21C);