#include "stdafx.h"
#include "AL_Object_Fruit.h"

NJS_OBJECT* ChaoTree = nullptr;
NJS_OBJECT* ChaoFruit_Chaonut = nullptr;
NJS_OBJECT* ChaoFruit_Starnut = nullptr;
NJS_OBJECT* ChaoFruit_Hastnut = nullptr;
NJS_OBJECT* ChaoFruit_Lifenut = nullptr;
NJS_OBJECT* ChaoFruit_Normal_25 = nullptr;
NJS_OBJECT* ChaoFruit_Normal_50 = nullptr;
NJS_OBJECT* ChaoFruit_Normal_75 = nullptr;
NJS_OBJECT* ChaoFruit_Normal_Ripe = nullptr;

// Retrieves the SA1 version of a Chao Fruit model, nullptr if not available
NJS_OBJECT* GetSA1ChaoFruit(NJS_CNK_MODEL* src)
{
	if (src == object_ali_mi_chao_chao.chunkmodel)
		return ChaoFruit_Chaonut;
	else if (src == object_ali_mi_hero_mi_hero.chunkmodel)
		return ChaoFruit_Starnut;
	else if (src == object_ali_mi_dark_mi_dark.chunkmodel)
		return ChaoFruit_Hastnut;
	else if (src == object_ali_mi_heart_heart.chunkmodel)
		return ChaoFruit_Lifenut;
	return nullptr;
}

// Hook to draw SA1 Chao fruits (object version)
void RenderSA1ChaoFruits_Object(NJS_CNK_OBJECT* src)
{
	NJS_OBJECT* SA1Fruit = GetSA1ChaoFruit(src->chunkmodel);
	if (SA1Fruit != nullptr)
	{
		njSetTexture(&texlist_chao_object);
		njTranslate(0, 0, 0, 0);
		njScale(0, 0.7f, 0.7f, 0.7f);
		dsDrawObject(SA1Fruit);
	}
	else
	{
		njSetTexture(&texlist_al_object);
		njCnkDrawObject(src);
	}
}

// Hook to draw SA1 Chao fruits (attach version)
void RenderSA1ChaoFruits_Model(NJS_CNK_MODEL* src)
{
	NJS_OBJECT* SA1Fruit = GetSA1ChaoFruit(src);
	if (SA1Fruit != nullptr)
	{
		njSetTexture(&texlist_chao_object);
		njTranslate(0, 0, 0, 0);
		njScale(0, 0.5f, 0.5f, 0.5f);
		dsDrawObject(SA1Fruit);
	}
	else
	{
		njSetTexture(&texlist_al_object);
		DrawCnkModel(src);
	}
}

// Render the Black Market preview with SA1 DC fruit models
void RenderSA1ChaoFruits_Model_BlackMarket(NJS_CNK_MODEL* src)
{
	// Fruits are scaled and positioned differently
	float scale = 0.5f;
	float yoff = 0.0f;
	NJS_OBJECT* SA1Fruit = GetSA1ChaoFruit(src);
	
	if (SA1Fruit != nullptr)
	{
		if (SA1Fruit == ChaoFruit_Chaonut)
		{
			yoff = -0.7f;
		}
		else if (SA1Fruit == ChaoFruit_Starnut)
		{
			scale = 0.6f;
			yoff = 0.5f;
		}
		else if (SA1Fruit == ChaoFruit_Hastnut)
		{
			scale = 0.6f;
			yoff = -0.3f;
		}
		else if (SA1Fruit == ChaoFruit_Lifenut)
		{
			yoff = -0.5f;
		}
		njSetTexture(&texlist_chao_object);
		njTranslate(0, 0.0f, yoff, 0.0f);
		njScale(0, scale, scale, scale);
		dsDrawObject(SA1Fruit);
	}
	else
	{
		njSetTexture(&texlist_al_object);
		DrawCnkModel(src);
	}
}

// Render the SA1 normal fruit model in the garden
void RenderSA1ChaoNormalFruitWithScale()
{
	njScale(0, 0.7f, 0.7f, 0.7f);
	njSetTexture(&texlist_chao_object);
	dsDrawObject(ChaoFruit_Normal_Ripe);
}

// Render the SA1 normal fruit model in the Black Market
void RenderSA1ChaoNormalFruit_BlackMarket(NJS_MODEL_SADX* a1)
{
	njSetTexture(&texlist_chao_object);
	dsDrawModel(ChaoFruit_Normal_Ripe->basicdxmodel);
}

// Tree display sub. Decompiled crap but will do for now.
void AL_DrawTreeSub_r(TREE_WORK* pTree, NJS_OBJECT* pObject)
{
	unsigned __int64 v3; // rax
	int FruitDispNum; // ecx
	float* p_growth; // ebx
	double v6; // st7
	NJS_VECTOR v; // [esp+Ch] [ebp-Ch] BYREF
	float sxa; // [esp+20h] [ebp+8h]

	do
	{
		njPushMatrixEx();
		njTranslateV(0, (NJS_VECTOR*)pObject->pos);
		njRotateXYZ(0, pObject->ang[0], pObject->ang[1], pObject->ang[2]);
		if (!pObject->model)
		{
			FruitDispNum = pTree->FruitDispNum;
			p_growth = &pTree->fruit[FruitDispNum].growth;
			v.x = 0.0f;
			v.y = -1.8f;
			v.z = 0.0f;
			sxa = *p_growth;
			if (*p_growth <= 1.0f)
			{
				if (sxa <= 0.0f)
				{
				LABEL_17:
					++pTree->FruitDispNum;
					goto LABEL_18;
				}
			}
			else
			{
				sxa = 1.0f;
			}
			if (*p_growth <= 0.9f)
			{
				if (!(njSin(pTree->FruitRotAng + FruitPhaseList[FruitDispNum])
					* pTree->FruitWidth
					* 0.2f
					* 65536.0f
					* 0.002777777777777778f))
				{
					goto LABEL_14;
				}
				v6 = njSin(pTree->FruitRotAng + FruitPhaseList[pTree->FruitDispNum]) * pTree->FruitWidth * 0.2f;
			}
			else
			{
				njPushMatrixEx();
				njGetTranslation(0, (NJS_VECTOR*)p_growth + 1);
				njPopMatrixEx();
				if (!(njSin(pTree->FruitRotAng + FruitPhaseList[pTree->FruitDispNum])
					* pTree->FruitWidth
					* 65536.0f
					* 0.002777777777777778f))
				{
					goto LABEL_14;
				}
				v6 = njSin(pTree->FruitRotAng + FruitPhaseList[pTree->FruitDispNum]) * pTree->FruitWidth;
			}
			njRotateX(0, (v6 * 65536.0f * 0.002777777777777778f));
		LABEL_14:
			njScale(0, sxa, sxa, sxa);
			njTranslateEx(&v);
			njSetTexture(&texlist_chao_object);
			// HACK
			if (sxa < 0.5f)
				dsDrawObject(ChaoFruit_Normal_25);
			if (sxa >= 0.5f && sxa < 0.75f)
				dsDrawObject(ChaoFruit_Normal_50);
			if (sxa >= 0.75f && sxa < 1.0f)
				dsDrawObject(ChaoFruit_Normal_75);
			if (sxa >= 1.0f)
				dsDrawObject(ChaoFruit_Normal_Ripe);
			// HACK
			goto LABEL_17;
		}
		if ((njSin(pTree->LeafRotAng + LeafPhaseList[pTree->LeafDispNum])
			* pTree->LeafWidth
			* 65536.0f
			* 0.002777777777777778f))
		{
			v3 = (njSin(pTree->LeafRotAng + LeafPhaseList[pTree->LeafDispNum])
				* pTree->LeafWidth
				* 65536.0f
				* 0.002777777777777778f);
			njRotateX(0, v3);
		}
		dsDrawModel(pObject->basicdxmodel);
		++pTree->LeafDispNum;
	LABEL_18:
		if (pObject->child)
		{
			AL_DrawTreeSub_r(pTree, pObject->child);
		}
		njPopMatrixEx();
		pObject = pObject->sibling;
	} while (pObject);
}

// Loads SA1 tree replacement, separate from fruit models
void AL_Tree_Load()
{
	ChaoTree = LoadModel("system\\data\\chao\\data\\dx\\object_common\\yasiyure_miki.nja.sa1mdl");
	WriteCall((void*)0x0072110F, SetChaoObjectTexlist);
	WriteCall((void*)0x0072109C, SetChaoObjectTexlist);
	model_yasiyure_miki_miki = *ChaoTree->basicdxmodel;
	model_yasiyure_miki_ha1 = *ChaoTree->child->basicdxmodel;
	model_yasiyure_miki_ha2 = *ChaoTree->child->sibling->basicdxmodel;
}

void AL_Fruit_Load()
{
	ChaoFruit_Chaonut = LoadModel("system\\data\\chao\\data\\dx\\object_common\\al_mi_chaonut.sa1mdl");
	ChaoFruit_Starnut = LoadModel("system\\data\\chao\\data\\dx\\object_common\\al_mi_starnut.sa1mdl");
	ChaoFruit_Hastnut = LoadModel("system\\data\\chao\\data\\dx\\object_common\\al_mi_hastnut.sa1mdl");
	ChaoFruit_Lifenut = LoadModel("system\\data\\chao\\data\\dx\\object_common\\al_mi_lifenut.sa1mdl");
	ChaoFruit_Normal_25 = LoadModel("system\\data\\chao\\data\\dx\\object_common\\yasiyure_mi00.nja.sa1mdl");
	ChaoFruit_Normal_25->pos[0] = 0;
	ChaoFruit_Normal_25->pos[1] = 0;
	ChaoFruit_Normal_25->pos[2] = 0;
	ChaoFruit_Normal_50 = LoadModel("system\\data\\chao\\data\\dx\\object_common\\yasiyure_mi01.nja.sa1mdl");
	ChaoFruit_Normal_50->pos[0] = 0;
	ChaoFruit_Normal_50->pos[1] = 0;
	ChaoFruit_Normal_50->pos[2] = 0;
	ChaoFruit_Normal_75 = LoadModel("system\\data\\chao\\data\\dx\\object_common\\yasiyure_mi02.nja.sa1mdl");
	ChaoFruit_Normal_75->pos[0] = 0;
	ChaoFruit_Normal_75->pos[1] = 0;
	ChaoFruit_Normal_75->pos[2] = 0;
	ChaoFruit_Normal_Ripe = LoadModel("system\\data\\chao\\data\\dx\\object_common\\yasiyure_mi03.nja.sa1mdl");
	ChaoFruit_Normal_Ripe->pos[0] = 0;
	ChaoFruit_Normal_Ripe->pos[1] = 0;
	ChaoFruit_Normal_Ripe->pos[2] = 0;
	if (ModConfig::ReplaceFruits == FruitConfig::CommonFruits)
	{
		WriteCall((void*)0x00722D3B, RenderSA1ChaoFruits_Object);
		WriteCall((void*)0x00726106, RenderSA1ChaoFruits_Model);
		WriteCall((void*)0x00726138, RenderSA1ChaoFruits_Model_BlackMarket); // Black Market preview
		WriteCall((void*)0x00727722, RenderSA1ChaoFruits_Model_BlackMarket); // Black Market item list
	}
	if (ModConfig::ReplaceFruits == FruitConfig::NormalFruitOnly || ModConfig::ReplaceFruits == FruitConfig::CommonFruits)
	{
		WriteJump(AL_DrawTreeSub, AL_DrawTreeSub_r); // Tree display sub replacement with SA1 fruits
		WriteCall((void*)0x007260D9, RenderSA1ChaoNormalFruit_BlackMarket); // Black market preview
		WriteCall((void*)0x00722D59, RenderSA1ChaoNormalFruitWithScale); // Scale normal fruit in garden
	}
}