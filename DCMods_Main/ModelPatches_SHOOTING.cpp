#include "stdafx.h"

void PatchModels_Shooting0()
{
	// dxpc\shooting\common\models\edv_f_futa.nja.sa1mdl
	*(NJS_TEX*)0x0298A4B0 = { 453, 235 };
	*(NJS_TEX*)0x0298A4B4 = { 56, 235 };
	*(NJS_TEX*)0x0298A4B8 = { 510, -255 };
	*(NJS_TEX*)0x0298A4BC = { 0, -255 };
	*(NJS_TEX*)0x0298A4C0 = { 453, 235 };
	*(NJS_TEX*)0x0298A4C4 = { 56, 235 };
	*(NJS_TEX*)0x0298A4C8 = { 510, -255 };
	*(NJS_TEX*)0x0298A4CC = { 0, -255 };
	*(NJS_TEX*)0x0298A4D0 = { 453, 235 };
	*(NJS_TEX*)0x0298A4D4 = { 56, 235 };
	*(NJS_TEX*)0x0298A4D8 = { 510, -255 };
	*(NJS_TEX*)0x0298A4DC = { 0, -255 };
	*(NJS_TEX*)0x0298A4E0 = { 453, 235 };
	*(NJS_TEX*)0x0298A4E4 = { 56, 235 };
	*(NJS_TEX*)0x0298A4E8 = { 510, -255 };
	*(NJS_TEX*)0x0298A4EC = { 0, -255 };
	*(NJS_TEX*)0x0298A4F0 = { 453, 235 };
	*(NJS_TEX*)0x0298A4F4 = { 56, 235 };
	*(NJS_TEX*)0x0298A4F8 = { 510, -255 };
	*(NJS_TEX*)0x0298A4FC = { 0, -255 };
	*(NJS_TEX*)0x0298A500 = { 453, 235 };
	*(NJS_TEX*)0x0298A504 = { 56, 235 };
	*(NJS_TEX*)0x0298A508 = { 510, -255 };
	*(NJS_TEX*)0x0298A50C = { 0, -255 };
	*(NJS_TEX*)0x0298A510 = { 109, 255 };
	*(NJS_TEX*)0x0298A514 = { 400, 255 };
	*(NJS_TEX*)0x0298A518 = { 0, 0 };
	*(NJS_TEX*)0x0298A51C = { 510, 0 };
	*(NJS_TEX*)0x0298A520 = { 109, -254 };
	*(NJS_TEX*)0x0298A524 = { 400, -254 };

	// dxpc\shooting\common\models\stene_s_kibody.nja.sa1mdl
	*(NJS_TEX*)0x02941538 = { 249, 131 };
	*(NJS_TEX*)0x0294153C = { 1, 237 };
	*(NJS_TEX*)0x02941540 = { 1, 29 };
	*(NJS_TEX*)0x02941544 = { 1, 237 };
	*(NJS_TEX*)0x02941548 = { 1, 29 };
	*(NJS_TEX*)0x0294154C = { 249, 131 };
	*(NJS_TEX*)0x02941550 = { 1, 237 };
	*(NJS_TEX*)0x02941554 = { 1, 29 };
	*(NJS_TEX*)0x02941558 = { 1, 237 };
	*(NJS_TEX*)0x0294155C = { 1, 29 };
	*(NJS_TEX*)0x02941560 = { 249, 131 };
	*(NJS_TEX*)0x02941564 = { 1, 237 };
	*(NJS_TEX*)0x02941568 = { 1, 29 };
	*(NJS_TEX*)0x0294156C = { 249, 131 };
	*(NJS_TEX*)0x02941570 = { 1, 237 };
	*(NJS_TEX*)0x02941574 = { 1, 29 };
	*(NJS_TEX*)0x02941578 = { 249, 131 };
	*(NJS_TEX*)0x0294157C = { 1, 29 };
	*(NJS_TEX*)0x02941580 = { 1, 237 };
	*(NJS_TEX*)0x02941584 = { 1, 237 };
	*(NJS_TEX*)0x02941588 = { 1, 29 };
	*(NJS_TEX*)0x0294158C = { 249, 131 };
	*(NJS_TEX*)0x02941590 = { 1, 237 };
	*(NJS_TEX*)0x02941594 = { 1, 29 };
	*(NJS_TEX*)0x02941598 = { 1, 237 };
	*(NJS_TEX*)0x0294159C = { 1, 29 };
	*(NJS_TEX*)0x029415A0 = { 249, 131 };
	*(NJS_TEX*)0x029415A4 = { 1, 237 };
	*(NJS_TEX*)0x029415A8 = { 1, 29 };
	*(NJS_TEX*)0x029415AC = { 249, 131 };
	*(NJS_TEX*)0x029415B0 = { 1, 29 };
	*(NJS_TEX*)0x029415B4 = { 1, 237 };
}

void PatchModels_Shooting1()
{
	// dxpc\shooting\act1\models\beam2_beamev_s_beam.nja.sa1mdl
	*(NJS_TEX*)0x028E121C = { 0, 0 };
	*(NJS_TEX*)0x028E1220 = { 0, -255 };
	*(NJS_TEX*)0x028E1224 = { 255, 0 };
	*(NJS_TEX*)0x028E1228 = { 255, -254 };
	*(NJS_TEX*)0x028E122C = { 509, 0 };
	*(NJS_TEX*)0x028E1230 = { 510, -254 };
	*(NJS_TEX*)0x028E1234 = { 509, 0 };
	*(NJS_TEX*)0x028E1238 = { 509, 255 };
	*(NJS_TEX*)0x028E123C = { 255, 0 };
	*(NJS_TEX*)0x028E1240 = { 254, 254 };
	*(NJS_TEX*)0x028E1244 = { 0, 0 };
	*(NJS_TEX*)0x028E1248 = { 0, 254 };

	// dxpc\shooting\act1\models\beam_beam_tr1.nja.sa1mdl
		// Character model

	// dxpc\shooting\act1\models\ecsc_hare_s_sora_hare.nja.sa1mdl
	//((NJS_MATERIAL*)0x028DFD68)->diffuse.color = 0xFF006AFF;
	//((NJS_MATERIAL*)0x028DFD7C)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x028DFD90)->diffuse.color = 0xFFB2B2B2;

	// dxpc\shooting\act1\models\tr1_s_t1_body.nja.sa1mdl
		// Character model

	// dxpc\shooting\act2\models\tr2a_s_t2b_body.nja.sa1mdl
		// Character model

	// dxpc\shooting\act2\models\tr2b_s_tru2_body.nja.sa1mdl
		// Character model

	// dxpc\shooting\act2\models\tr2_tf_event_s_tr2rt.nja.sa1mdl
		// Character model

	// dxpc\shooting\common\models\shot_bf_s_bodya.nja.sa1mdl
		// Full replacement

	// dxpc\shooting\common\models\shot_s_ecfire.nja.sa1mdl
		// Full replacement

	// dxpc\shooting\common\models\stec_s_shadow.nja.sa1mdl
		// Unused
}

void PatchModels_Shooting2()
{
	// dxpc\shooting\act2\models\ecsc_s_sora.nja.sa1mdl
	//((NJS_MATERIAL*)0x02817628)->diffuse.color = 0xFF006AFF;
	//((NJS_MATERIAL*)0x0281763C)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x02817650)->diffuse.color = 0xFFB2B2B2;

	// dxpc\shooting\common\models\beamev_s_beam.nja.sa1mdl
	*(NJS_TEX*)0x0298D94C = { 0, 0 };
	*(NJS_TEX*)0x0298D950 = { 0, -255 };
	*(NJS_TEX*)0x0298D954 = { 255, 0 };
	*(NJS_TEX*)0x0298D958 = { 255, -254 };
	*(NJS_TEX*)0x0298D95C = { 509, 0 };
	*(NJS_TEX*)0x0298D960 = { 510, -254 };
	*(NJS_TEX*)0x0298D964 = { 509, 0 };
	*(NJS_TEX*)0x0298D968 = { 509, 255 };
	*(NJS_TEX*)0x0298D96C = { 255, 0 };
	*(NJS_TEX*)0x0298D970 = { 254, 254 };
	*(NJS_TEX*)0x0298D974 = { 0, 0 };
	*(NJS_TEX*)0x0298D978 = { 0, 254 };
}