#include "stdafx.h"
#include "Advertise_Credits.h"

// Really dumb hack to make credits logos fade out properly. Maybe I'll go back and redo it someday.
void DrawBG_CreditsLogoHack(int texnum, float x, float y, float z, float scaleX, float scaleY)
{
	NJS_COLOR vtxcolor = { 0xFFFFFFFF };
	if (y < 0.0f)
		vtxcolor.argb.a = max(0, 255 + (int)(y * 4.0f)); // Lower alpha after reaching the center of the screen
	ghSetPvrTexMaterial(vtxcolor.color);
	njSetZCompare(7u);
	njSetZUpdateMode(0);
	PvrAlphaFlag = true;
	ghDrawPvrTextureS(texnum, x, y, z, scaleX, scaleX);
	njSetZUpdateMode(1);
	njSetZCompare(3u);
	PvrAlphaFlag = false;
}

void AdvertiseCredits_Init()
{
	// Credits
	WriteData((float*)0x006415DA, 1.5f); // EngBG X scale (DC images are smaller so they're scaled up a bit)
	WriteData((float*)0x006415DF, 1.5f); // EngBG Y scale
	WriteCall((void*)0x00640ACC, DrawBG_CreditsLogoHack);
	StaffRollData.nbStaff = 449;
	StaffRollData.StaffTbl = (STAFF_DATA*)&SA1Credits;
}