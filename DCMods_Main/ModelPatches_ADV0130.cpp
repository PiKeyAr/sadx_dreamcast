#include "stdafx.h"

void PatchModels_ADV0130()
{
	HMODULE ADV01CMODELS = GetModuleHandle(L"ADV01CMODELS");

	// dxpc\adv01_eggcarrierc\mogobj\tataki1_dai1mt.nja.sa1mdl
		//Full replacement
		
	// dxpc\adv01_eggcarrierc\common\models\e102_itemgun_e102_itemgun.nja.sa1mdl
	//Wow, something that was actually fixed in DX!
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x001371F8)->attr_texId = 5;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x0013720C)->attr_texId = 4;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00137220)->attr_texId = 1;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00137234)->attr_texId = 3;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00137248)->attr_texId = 0;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x0013725C)->attr_texId = 0;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00137270)->attr_texId = 2;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00136EF0)->attr_texId = 3;

	// dxpc\adv01_eggcarrierc\common\models\edc_f_tarai.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112160) = { 382, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112164) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112168) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0011216C) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112170) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112174) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112178) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0011217C) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112180) = { 382, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112184) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112188) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0011218C) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112190) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112194) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00112198) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0011219C) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121A0) = { 382, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121A4) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121A8) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121AC) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121B0) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121B4) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121B8) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001121BC) = { 382, 254 };

	// dxpc\adv01_eggcarrierc\common\models\edv_f_dodai.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00105210) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00105214) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00105218) = { 509, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0010521C) = { 0, 254 };

	// dxpc\adv01_eggcarrierc\common\models\edv_f_moji.nja.sa1mdl
		//Model too different but whatever
	
	// dxpc\adv01_eggcarrierc\common\models\edv_k_hlift.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierc\common\models\edv_k_monorb.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1DE8) = { 3, 5 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1DEC) = { 506, 5 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1DF0) = { 3, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1DF4) = { 506, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1AE0) = { 3, -504 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1AE4) = { 3, 249 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1AE8) = { 506, -504 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000F1AEC) = { 506, 249 };

	// dxpc\adv01_eggcarrierc\common\models\edv_k_monorf.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDDC8) = { 3, 5 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDDCC) = { 506, 5 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDDD0) = { 3, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDDD4) = { 506, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDAC0) = { 3, -504 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDAC4) = { 3, 249 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDAC8) = { 506, -504 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EDACC) = { 506, 249 };

	// dxpc\adv01_eggcarrierc\common\models\edv_nc_k_ekis.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3C8) = { 352, -253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3CC) = { 352, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3D0) = { 1, -253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3D4) = { 1, 253 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3D8) = { 253, 251 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3DC) = { 1, 251 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3E0) = { 253, -251 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000EC3E4) = { 1, -251 };

	// dxpc\adv01_eggcarrierc\common\models\e_itemfun_e_itemfun.nja.sa1mdl
	//Wow, something that was actually fixed in DX!
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x00136978)->attr_texId = 0;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x0013698C)->attr_texId = 1;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x001369A0)->attr_texId = 5;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x001369B4)->attr_texId = 4;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x001369C8)->attr_texId = 2;
	//((NJS_MATERIAL*)(size_t)ADV01CMODELS + 0x001369DC)->attr_texId = 3;

	// dxpc\adv01_eggcarrierc\common\models\n_eggtobira_n_hontai.nja.sa1mdl
		//Full replacement

	// dxpc\adv01_eggcarrierc\common\models\n_elv_nc_n_lift_2.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028C8) = { 0, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028CC) = { 0, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028D0) = { 40, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028D4) = { 40, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028D8) = { 214, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028DC) = { 214, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028E0) = { 510, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028E4) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028E8) = { 469, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028EC) = { 469, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028F0) = { 295, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028F4) = { 295, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028F8) = { 214, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x001028FC) = { 214, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102900) = { 40, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102904) = { 40, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102908) = { 0, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0010290C) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102910) = { 214, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102914) = { 214, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102918) = { 295, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0010291C) = { 295, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102920) = { 469, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102924) = { 469, -764 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x00102928) = { 510, -306 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x0010292C) = { 510, -764 };

	// dxpc\adv01_eggcarrierc\common\models\n_osuji1_edv.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEED0) = { 10, 145 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEED4) = { 4, 93 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEED8) = { 7, 98 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEE0) = { 507, 88 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEE4) = { 499, 3 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEE8) = { 509, 135 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEEC) = { 504, 83 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEF0) = { 509, 135 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEF4) = { 448, 247 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEF8) = { 507, 88 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEEFC) = { 474, 208 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF00) = { 60, 217 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF04) = { 7, 98 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF08) = { 10, 145 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF0C) = { 7, 98 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF10) = { 507, 88 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF14) = { 474, 208 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF18) = { 448, 247 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF1C) = { 60, 217 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF20) = { 91, 255 };
	*(NJS_TEX*)((size_t)ADV01CMODELS + 0x000FEF24) = { 10, 145 };

	// dxpc\adv01_eggcarrierc\common\models\n_switch_n_switcha.nja.sa1mdl
		// Different evalflags?

	// dxpc\adv01_eggcarrierc\common\models\n_switch_n_switchb.nja.sa1mdl
		// Different evalflags?
}