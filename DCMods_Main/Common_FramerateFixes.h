#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_POINT3COL, spark_point3col, 0x009894C0); // Bomb fuse polygons
DataPointer(NJS_SPRITE, sprite_o_bubble_0, 0x038CD540); // Underwater bubbles
DataArray(NJS_VECTOR, l_point, 0x03C5C484, 2); // Bomb fuse points

FastcallFunctionPointer(int, PtnChgEgm1Jet, (Egm1JetWk* wkp), 0x00572620); // Animates Egg Hornet jet stuff
TaskFunc(Object_beach_osakana_Draw, 0x004FC770); // Emerald Coast 2D fishes decoration
TaskFunc(ObjectCasinoHaeDraw, 0x005C8A20); // Casino flies display function
TaskFunc(DrawFire, 0x005E81E0); // Draws Lost World fire particles
TaskFunc(drawEffectKiran2, 0x004BAE50); // Draws sparks such as those used for character upgrades
TaskFunc(DisplayDrop, 0x00546090); // Draws Chaos 0 rain
TaskFunc(disp_Emission, 0x0058B5B0); // Draws Zero sparks
TaskFunc(Disp_113_PinballJackpot, 0x005E1020); // Draws the pinball jackpot sprite

// Stuff that isn't in the symbols

DataPointer(int, PerfectChaosTornadoSpeed1, 0x01426CA0);
DataPointer(int, PerfectChaosTornadoSpeed2, 0x01426CA4);
DataPointer(int, PerfectChaosTornadoSpeed3, 0x01426CA8);
DataPointer(int, PerfectChaosTornadoSpeed4, 0x01426CAC);