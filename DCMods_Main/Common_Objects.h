#pragma once

#include <SADXModLoader.h>

TaskFunc(ef_barrier_DrawModel, 0x004B9C90); // Barrier display function

DataPointer(NJS_SPRITE, sprite_kaos_eme_eff, 0x03C5E32C); // Goal emerald effect Ice Cap
DataPointer(NJS_SPRITE, sprite_kaos_eme_eff_0, 0x03C750F4); // Goal emerald effect Casino
DataPointer(NJS_SPRITE, sprite_kaos_eme_eff_1, 0x03C5D680); // Goal emerald effect Windy Valley
DataPointer(NJS_MODEL_SADX, model_switch_body_button2, 0x00989384); // Switch (unpressed)
DataPointer(NJS_MODEL_SADX, model_switch_body_button, 0x008BBD84); // Switch (pressed)

void PatchModels_Objects();
void ItemBox_Init();