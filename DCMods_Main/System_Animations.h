#pragma once
#include <SADXModLoader.h>

struct TextureAnimation // Struct used to animate textures
{
	int level;
	int act;
	NJS_MATERIAL* material;
	int Speed;
	bool NonSequential;
	int Frames[16];
};

struct UVAnimation // Struct used to animate UVs
{
	int level;
	int act;
	NJS_TEX* uv_pointer;
	int uv_count;
	int timer;
	int u_speed;
	int v_speed;
	int u_shift;
	int v_shift;
};