#pragma once

#include <SADXModLoader.h>

DataArray(NJS_MATERIAL, matlist_hw_obj_lmpa_lmpalght, 0x02696920, 1);
DataArray(NJS_MATERIAL, matlist_st_lamp_lamppost_1_lamp02, 0x026877CC, 1);
DataArray(NJS_MATERIAL, matlist_st_lamp_lamppost_1_lamp01, 0x026878D8, 1);
DataArray(NJS_MATERIAL, matlist_nisepat_nisepat_nisepat, 0x009719B0, 17);
DataPointer(NJS_OBJECT, object_turnasi_oya_koa, 0x0267D3B4);
DataPointer(NJS_OBJECT, object_turnasi_oya_kob, 0x0267C7AC);
DataPointer(NJS_MODEL_SADX, model_glass_cube_1_cube_1, 0x0267A1A0);
DataArray(NJS_TEXLIST, texlist_board, 0x026B2B90, 4); // OSignboard partial texlists