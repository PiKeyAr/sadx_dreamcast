#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_ARGB, argb_18, 0x01494114);
DataPointer(NJS_ARGB, argb_19, 0x01494124);
DataPointer(NJS_SPRITE, sprite_Chaos7Damage, 0x01494030);
DataPointer(NJS_SPRITE, sprite_Chaos7SDamage, 0x01494064);
DataPointer(NJS_TEXANIM, anim_Chaos7Damage, 0x0149401C);
DataPointer(NJS_TEXANIM, anim_Chaos7SDamage, 0x01494050);