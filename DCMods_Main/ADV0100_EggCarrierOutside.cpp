#include "stdafx.h"
#include "ADV0100_EggCarrierOutside.h"

static bool ModelsLoaded_ADV0100;

// Hook to draw EC NPCs with depth
void RenderEggCarrier0NPC(NJS_ACTION* action, Float frame)
{
	if (action == &action_c4_wait0_c4_kihon0_chaos4) // Chaos 4
		CHAOS_Action(action, frame);
	else if (action->object == E102_OBJECTS[0]) // Gamma
	{
		OffControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
		OnControl3D(NJD_CONTROL_3D_ENABLE_ALPHA);
		late_ActionEx(action, frame, LATE_WZ);
	}
	else
		ds_ActionClipNoScale(action, frame);
}

// Disable the SetClip function in Egg Carrier
void SetClip_EggCarrier(signed int cliplevel)
{
	return;
}

// Skybox (top) display function
void EggCarrierSkyBox_Top(taskwk* twp, float yoff)
{
	if (!loop_count)
	{
		if (ssActNumber != 6)
			___njClipZ(gClipSky.f32Near, gClipSky.f32Far);
		___njFogDisable();
		njSetTexture(&texlist_ec_sky);
		njPushMatrix(0);
		njTranslate(0, camera_twp->pos.x, yoff, camera_twp->pos.z);
		njScaleV(0, &gSkyScale);
		ds_DrawObjectClip(ADV01_OBJECTS[65], VectorMaxAbs(&gSkyScale));
		njScale(0, 1.0f, 1.0f, 1.0f);
		njPopMatrix(1u);
		___njFogEnable();
		if (ssActNumber != 6)
			___njClipZ(gClipMap.f32Near, gClipMap.f32Far);
	}
}

// Skybox (bottom) display function
void __cdecl EggCarrierSkyBox_Bottom(taskwk* twp, float yoff)
{
	NJS_OBJECT* model; // esi
	NJS_TEX* uvs; // eax
	Sint16 uvshift; // cx
	model = ADV01_OBJECTS[66];
	uvs = model->basicdxmodel->meshsets->vertuv;
	uvshift = twp->value.w[0];
	uvs[0].v = uvshift + 2040;
	uvs[1].v = uvshift;
	uvs[2].v = uvshift + 2040;
	uvs[3].v = uvshift;
	___njFogDisable();
	njSetTexture(&texlist_ec_sky);
	njPushMatrix(0);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	njTranslate(0, camera_twp->pos.x, yoff, camera_twp->pos.z);
	njScale(0, 3.0f, 1.0f, 3.0f);
	njScaleV(0, &gSkyScale);
	ds_DrawObjectClip(model, VectorMaxAbs(&gSkyScale));
	njScale(0, 1.0f, 1.0f, 1.0f);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	njPopMatrix(1u);
	___njFogEnable();
}

// Egg Carrier sea display function
void EggCarrierSea()
{
	if (!loop_count)
	{
		___njFogDisable();
		njSetTexture(&texlist_ec_sea);
		njPushMatrix(0);
		njTranslate(0, camera_twp->pos.x, 0, camera_twp->pos.z);
		late_DrawObjectClip(&_object_ecsc_s_hare_umi_s_hare_umi, LATE_NO, 1.0f);
		njPopMatrix(1u);
	}
}

// Material fix for rotating planet decoration (ODosei)
void ODoseiFix(NJS_MODEL_SADX* model, float scale)
{
	SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
	ds_DrawModelClip(model, scale);
	ResetMaterial();
}

// Material fix for the light in Private Room (OLivingLight)
void OLivingOSLightFix(NJS_OBJECT* a1, LATE blend_mode, float scale)
{
	SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
	late_DrawObjectClipMesh(a1, blend_mode, scale);
	ResetMaterial();
}

// Draws the Tornado 2 by mesh
void DrawTornado2WithQueue(NJS_OBJECT* obj, float scale)
{
	DrawModelMesh(obj->basicdxmodel, LATE_WZ);
}

// Depth hack for rotating light (OSLight)
void OSLightFix(NJS_MODEL_SADX* model, LATE blend, float scale)
{
	late_z_ofs___ = 1000.0f;
	late_DrawModelClip(model, blend, scale);
	late_z_ofs___ = 0.0f;
}

// This dirty hack is needed to prevent the DX collision object from interfering with Gamma's hover animation in cutscenes
// TODO: Make it dynamic collision instead of landtable
void EC00CollisionHack()
{
	if (current_event != -1)
	{

		if (___LANDTABLEEC0[2]->pLandEntry[0].slAttribute == 0x40000001 && ___LANDTABLEEC0[2]->pLandEntry[0].xWidth < 53.0f)
		{
			if (___LANDTABLEEC0[2]->pLandEntry[0].pObject->pos[1] >= 1524.0f)
			{
				//PrintDebug("Disabling collision\n");
				___LANDTABLEEC0[2]->pLandEntry[0].pObject->pos[1] = -1500.0f;
				___LANDTABLEEC0[2]->pLandEntry[0].xCenter = 0;
				___LANDTABLEEC0[2]->pLandEntry[0].yCenter = 0;
				___LANDTABLEEC0[2]->pLandEntry[0].zCenter = 0;
				___LANDTABLEEC0[2]->pLandEntry[0].xWidth = 0;
				___LANDTABLEEC0[2]->pLandEntry[0].slAttribute = 0;
			}
		}
	}
	else if (___LANDTABLEEC0[2]->pLandEntry[0].pObject->pos[1] == -1500.0f)
	{
		//PrintDebug("Enabling collision\n");
		___LANDTABLEEC0[2]->pLandEntry[0].pObject->pos[1] = 1525.692f;
		___LANDTABLEEC0[2]->pLandEntry[0].xCenter = 1.572876f;
		___LANDTABLEEC0[2]->pLandEntry[0].yCenter = 1527.742f;
		___LANDTABLEEC0[2]->pLandEntry[0].zCenter = 3462.564f;
		___LANDTABLEEC0[2]->pLandEntry[0].xWidth = 51.01946f;
		___LANDTABLEEC0[2]->pLandEntry[0].slAttribute = 0x40000001;
	}

}

void ADV01_Init()
{
	ReplaceSET("SETEC00S");
	ReplaceSET("SETEC00M");
	ReplaceSET("SETEC00K");
	ReplaceSET("SETEC00A");
	ReplaceSET("SETEC00E");
	ReplaceSET("SETEC00B");
	ReplaceSET("SETEC01S");
	ReplaceSET("SETEC01M");
	ReplaceSET("SETEC01K");
	ReplaceSET("SETEC01A");
	ReplaceSET("SETEC01E");
	ReplaceSET("SETEC01B");
	ReplaceSET("SETEC02S");
	ReplaceSET("SETEC02M");
	ReplaceSET("SETEC02K");
	ReplaceSET("SETEC02A");
	ReplaceSET("SETEC02E");
	ReplaceSET("SETEC02B");
	ReplaceSET("SETEC03S");
	ReplaceSET("SETEC03M");
	ReplaceSET("SETEC04S");
	ReplaceSET("SETEC04M");
	ReplaceSET("SETEC04K");
	ReplaceSET("SETEC04A");
	ReplaceSET("SETEC04E");
	ReplaceSET("SETEC04B");
	ReplaceSET("SETEC05S");
	ReplaceSET("SETEC05M");
	ReplaceCAM("CAMEC00S");
	ReplaceCAM("CAMEC01S");
	ReplaceCAM("CAMEC02S");
	ReplaceCAM("CAMEC03S");
	ReplaceCAM("CAMEC04S");
	ReplaceCAM("CAMEC05S");
	ReplacePVM("ADV_EC00");
	ReplacePVM("ADV_EC01");
	ReplacePVM("ADV_EC02");
	ReplacePVM("ADV_EC03");
	ReplacePVM("ADV_EC04");
	ReplacePVM("ADV_EC05");
	ReplacePVM("BG_EC00");
	ReplacePVM("EC_ACTDOOR");
	ReplacePVM("EC_BG");
	ReplacePVM("EC_BOAT");
	ReplacePVM("EC_CLOUDS");
	ReplacePVM("EC_IKADA");
	ReplacePVM("EC_LIGHT");
	ReplacePVM("EC_SKY");
	ReplacePVM("EC_TORNADO");
	ReplacePVM("EC_TRANSFORM");
	ReplacePVM("EC_WATER");
	ReplacePVM("EV_ECCLOUD");
	ReplacePVM("EC_SEA");
	ReplacePVM("OBJ_EC00");
	// Fog/draw distance data
	for (int i = 0; i < 3; i++)
	{
		pScale_Adv01AB[3][i].x = 1.0f;
		pScale_Adv01AB[3][i].y = 1.0f;
		pScale_Adv01AB[3][i].z = 1.0f;
		pClipSky_Adv01AB[3]->f32Far = -10000.0f;
		pClipSky_Adv01AB[2]->f32Far = -9000.0f;

		pClipMap_Adv01AB[0][i].f32Far = -11000.0f;
		pClipMap_Adv01AB[1][i].f32Far = -11000.0f;
		pClipMap_Adv01AB[2][i].f32Far = -11000.0f;
		pClipMap_Adv01AB[6][i].f32Far = -28000.0f;

		pFogTable_Adv01AB[1][i].f32EndZ = -12000.0f;
		pFogTable_Adv01AB[1][i].f32StartZ = -12000.0f;
		pFogTable_Adv01AB[2][i].f32EndZ = -12000.0f;
		pFogTable_Adv01AB[2][i].f32StartZ = -12000.0f;
		pFogTable_Adv01AB[3][i].f32EndZ = -12000.0f;
		pFogTable_Adv01AB[3][i].f32StartZ = -12000.0f;
		pFogTable_Adv01AB[4][i].f32EndZ = -12000.0f;
		pFogTable_Adv01AB[4][i].f32StartZ = -12000.0f;
		pFogTable_Adv01AB[5][i].f32EndZ = -12000.0f;
		pFogTable_Adv01AB[5][i].f32StartZ = -12000.0f;

		pFogTable_Adv01AB[6][i].u8Enable = 1;
		pFogTable_Adv01AB[6][i].f32StartZ = -6500.0f;
		pFogTable_Adv01AB[6][i].f32EndZ = -17000.0f;
		pFogTable_Adv01AB[6][i].Col = 0xFF000000;
	}
}

void ADV01_Load()
{
	// This is done every time the function is called
	LevelLoader(LevelAndActIDs_EggCarrierOutside1, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec00.c.sa1lvl", ADV01_TEXLISTS[0], ___LANDTABLEEC0[0]);
	LevelLoader(LevelAndActIDs_EggCarrierOutside2, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec01.c.sa1lvl", ADV01_TEXLISTS[1], ___LANDTABLEEC0[1]);
	LevelLoader(LevelAndActIDs_EggCarrierOutside3, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec02.c.sa1lvl", ADV01_TEXLISTS[2], ___LANDTABLEEC0[2]);
	LevelLoader(LevelAndActIDs_EggCarrierOutside4, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec03.c.sa1lvl", ADV01_TEXLISTS[3], ___LANDTABLEEC0[3]);
	LevelLoader(LevelAndActIDs_EggCarrierOutside5, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec04.c.sa1lvl", ADV01_TEXLISTS[4], ___LANDTABLEEC0[4]);
	LevelLoader(LevelAndActIDs_EggCarrierOutside6, "SYSTEM\\data\\adv01_eggcarrierab\\landtableec05.c.sa1lvl", ADV01_TEXLISTS[5], ___LANDTABLEEC0[5]);
	if (!ModelsLoaded_ADV0100)
	{
		if (!CheckSADXWater(LevelIDs_EggCarrierOutside))
		{
			AddTextureAnimation_Permanent(LevelIDs_EggCarrierOutside, -1, &_object_ecsc_s_hare_umi_s_hare_umi.basicdxmodel->mats[0], false, 4, 4, 13);
			WriteJump((void*)0x0051C440, EggCarrierSea);
		}
		// Code fixes
		WriteCall((void*)0x0051AB88, RenderEggCarrier0NPC); // Chaos 4 glitch
		WriteData<5>((void*)0x005244D6, 0x90u); // Disable light flickering
		WriteCall((void*)0x00524509, OSLightFix); // Make light render of top of glass
		WriteCall((void*)0x00522B49, DrawTornado2WithQueue);
		WriteCall((void*)0x0051F637, ODoseiFix);
		WriteCall((void*)0x0051F669, ODoseiFix);
		WriteCall((void*)0x0051EB2C, OLivingOSLightFix);
		WriteJump((void*)0x0051B210, EggCarrierSkyBox_Top);
		WriteJump((void*)0x0051B3B0, EggCarrierSkyBox_Bottom);
		// Swap EC skybox draw calls to render the outer part last in Acts 1/2
		WriteCall((void*)0x0051B717, EggCarrierSkyBox_Bottom);
		WriteCall((void*)0x0051B71F, EggCarrierSkyBox_Top);
		WriteCall((void*)0x0051B76F, EggCarrierSkyBox_Bottom); // Transformation cutscene
		WriteCall((void*)0x0051B77A, EggCarrierSkyBox_Top); // Transformation cutscene
		// Fix Sonic jumps in "Come back here" cutscene
		WriteData((float*)0x006D5227, 2.0f); // Sonic's speed before the first jump
		WriteData((float*)0x006D5371, -420.0f); // Sonic's X position before the last jump
		// Fix camera in Amy-Gamma prison cutscene
		WriteData((float*)0x006A4EBE, -134.0f); // X1
		WriteData((float*)0x006A4EB9, 15.0f); // Y1
		WriteData((float*)0x006A4EB4, 54.0f); // Z1
		WriteData((float*)0x006A4F41, -143.85f); // X2
		WriteData((float*)0x006A4F3C, 15.93f); // Y2
		WriteData((float*)0x006A4F37, 80.25f); // Z2
		// Fix camera in Gamma-Amy prison cutscene
		WriteData((float*)0x00678C48, -134.0f); // X1
		WriteData((float*)0x00678C43, 15.0f); // Y1
		WriteData((float*)0x00678C3E, 54.0f); // Z1
		WriteData((float*)0x00678CCB, -143.85f); // X2
		WriteData((float*)0x00678CC6, 15.93f); // Y2
		WriteData((float*)0x00678CC1, 80.25f); // Z2
		// Material fixes
		AddAlphaRejectMaterial((NJS_MATERIAL*)((size_t)GetModuleHandle(L"ADV01MODELS") + 0x00209B6C)); // Rotating lights outside
		AddAlphaRejectMaterial((NJS_MATERIAL*)((size_t)GetModuleHandle(L"ADV01MODELS") + 0x001F7A58)); // Monorail sign (outside)
		// Fix materials on elevator buttons
		WriteData((float*)0x0051E818, 1.0f);
		WriteData((float*)0x0051E81D, 1.0f);
		WriteData((float*)0x0051E88F, 1.0f);
		WriteData((float*)0x0051E894, 1.0f);
		WriteJump((char*)GetProcAddress(GetModuleHandle(L"ADV01MODELS"), "SetClip_EC00"), SetClip_EggCarrier);
		WriteJump((char*)GetProcAddress(GetModuleHandle(L"ADV01MODELS"), "SetClip_EC01"), SetClip_EggCarrier);
		WriteData<5>((void*)0x0051BB8C, 0x90); // Don't disable fog in EC transformation cutscene
		// Model replacements
		*ADV01_ACTIONS[2]->object = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\era_s_seatrt.nja.sa1mdl"); // OEggChair
		*ADV01_ACTIONS[2]->motion = *LoadAnimation("system\\data\\adv01_eggcarrierab\\common\\models\\era_s_seatrt.nam.saanim"); // OEggChair
		ADV01_OBJECTS[21] = ADV01_ACTIONS[2]->object->child->child;
		ADV01_OBJECTS[22] = ADV01_ACTIONS[2]->object->child->child->sibling->sibling->sibling;
		ADV01_OBJECTS[23] = ADV01_ACTIONS[2]->object->child->child->sibling->sibling->sibling->sibling;
		ADV01_OBJECTS[24] = ADV01_ACTIONS[2]->object->child->child->sibling->sibling->sibling->sibling->sibling;
		*ADV01_ACTIONS[6]->object = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edv_s_stdoor.nja.sa1mdl"); // OSkyDeck
		*ADV01_OBJECTS[64] = *LoadModel("system\\data\\adv01_eggcarrierab\\transform\\ecf_bf_s_bodya.nja.sa1mdl"); // EC transform
		*ADV01_ACTIONS[7]->object = *ADV01_OBJECTS[64]; // EC transform
		*ADV01_OBJECTS[0] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\eda_o_lift.nja.sa1mdl"); // SideLift
		*ADV01_OBJECTS[1] = *ADV01_OBJECTS[0]->child; // SideLift
		*ADV01_OBJECTS[2] = *ADV01_OBJECTS[0]->child->sibling; // SideLift
		*ADV01_OBJECTS[29] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\gun_s_guna.nja.sa1mdl"); // OGunSight
		*ADV01_ACTIONS[3]->object = *ADV01_OBJECTS[29];
		*ADV01_OBJECTS[6] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edb_o_isu.nja.sa1mdl"); // OBChair
		*ADV01_OBJECTS[8] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edb_o_lbed.nja.sa1mdl"); // OEggmanBed
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[55]->child->child->basicdxmodel->mats[3]);
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[55]->child->child->basicdxmodel->mats[4]);
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[55]->child->child->basicdxmodel->mats[5]);
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[55]->child->child->basicdxmodel->mats[6]);
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[55]->child->child->basicdxmodel->mats[7]);
		*ADV01_OBJECTS[58] = *ADV01_OBJECTS[55]->child->child->child; // OSLight
		*ADV01_OBJECTS[61] = *LoadModel("system\\data\\adv01_eggcarrierab\\sky\\tr2a_s_tr2_mgbody.nja.sa1mdl"); // OTornado2
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[61]->basicdxmodel->mats[24]);
		AddWhiteDiffuseMaterial(&ADV01_OBJECTS[61]->basicdxmodel->mats[25]);
		*ADV01_OBJECTS[5] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edb_o_daiza.nja.sa1mdl"); // This thing is stupid
		*ADV01_ACTIONS[0]->object = *ADV01_OBJECTS[5];		
		*ADV01_OBJECTS[13] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edb_o_kasa.nja.sa1mdl"); // OParasol
		*ADV01_OBJECTS[27] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edv_s_dorup.nja.sa1mdl"); // Door top
		LoadModel_ReplaceMeshes(ADV01_OBJECTS[14], "system\\data\\adv01_eggcarrierab\\common\\models\\taihou_s_taiho.nja.sa1mdl"); // OTaihou (Cannon)
		*ADV01_OBJECTS[28] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edv_s_ermdoor.nja.sa1mdl"); // Door 2
		*ADV01_OBJECTS[18] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\futa_s_rmfuta.nja.sa1mdl"); // Eggcap
		*ADV01_OBJECTS[19] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\edv_k_hlift.nja.sa1mdl"); // Egglift
		*ADV01_OBJECTS[34] = *LoadModel("system\\data\\adv01_eggcarrierab\\common\\models\\eca_s_msto.nja.sa1mdl"); // OMast
		ADV01_OBJECTS[51]->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Pool water
		ADV01_OBJECTS[51]->basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Pool water
		ADV01_OBJECTS[51]->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Pool water
		AddAlphaRejectMaterial(&ADV01_OBJECTS[69]->child->sibling->basicdxmodel->mats[0]);
		ModelsLoaded_ADV0100 = true;
	}
}

void ADV01_OnFrame()
{
	if (ssStageNumber != LevelIDs_EggCarrierOutside || ssActNumber != 2 || !IsLevelLoaded(LevelAndActIDs_EggCarrierOutside1))
		return;
	EC00CollisionHack();
}