#pragma once

#include <SADXModLoader.h>

void PatchModels_Shooting0();
void PatchModels_Shooting1();
void PatchModels_Shooting2();

TaskFunc(BeamDisp, 0x0062A8C0); // Displays the Egg Carrier cannon beam in Sky Chase Act 1

DataPointer(int, RoundMode, 0x03C81290); // Sky Chase level mode

DataPointer(NJS_ACTION, action_beam2_beamev_s_beam, 0x028E596C);
DataPointer(NJS_ACTION, action_beam2_stef_s_tamari, 0x0290761C);
DataPointer(NJS_ACTION, action_beam2_shot_bf_s_bodya, 0x0290A414);
DataPointer(NJS_ACTION, action_shot_s_ecfire, 0x02985984);

DataPointer(NJS_OBJECT, object_beam_beam_tr1_s_l_f4_3, 0x02916F9C);
DataPointer(NJS_OBJECT, object_beam_beam_tr1_s_l_f5_3, 0x02916ADC);
DataPointer(NJS_OBJECT, object_beam_beam_tr1_s_r_f4_3, 0x02918404);
DataPointer(NJS_OBJECT, object_beam_beam_tr1_s_r_f5_3, 0x02917F34);