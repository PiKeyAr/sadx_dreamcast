#pragma once

#include <SADXModLoader.h>

void SetChaoObjectTexlist();

DataPointer(NJS_CNK_OBJECT, object_ali_mi_mori01_mori01, 0x033B9064); // Strong Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_paku01_mi_paku01, 0x033B9A44); // Tasty Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_hero_mi_hero, 0x033B9E5C); // Hero Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_dark_mi_dark, 0x033BA304); // Dark Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_maru01_maru01, 0x033BAC54); // Round Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_sankaku01_sankaku01, 0x033BB514); // Triangle Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_sikaku01_sikaku01, 0x033BBFF4); // Square Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_heart_heart, 0x033BC574); // Heart Fruit
DataPointer(NJS_CNK_OBJECT, object_ali_mi_chao_chao, 0x033BCBC4); // Chao Fruit

DataPointer(NJS_MODEL_SADX, model_yasiyure_miki_miki, 0x036087C0); // Chao Tree trunk
DataPointer(NJS_MODEL_SADX, model_yasiyure_miki_ha1, 0x03608064); // Chao Tree leaves 1
DataPointer(NJS_MODEL_SADX, model_yasiyure_miki_ha2, 0x036076E4); // Chao Tree leaves 2

DataArray(int, FruitPhaseList, 0x033B87E0, 4);
DataArray(int, LeafPhaseList, 0x033B87B4, 11);