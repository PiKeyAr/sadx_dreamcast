#include "stdafx.h"

// Some stuff here is commented out for future use in the full lobby restoration

// List of number doors
NJS_OBJECT* GateObjects[] = {
	&object_opengate_a_nc_gate_a_nc_gate_a,
	&object_opengate_b_nc_gate_b_nc_gate_b,
	&object_opengate_c_nc_gate_c_nc_gate_c,
	&object_opengate_d_nc_gate_d_nc_gate_d,
	&object_opengate_e_nc_gate_e_nc_gate_e,
	&object_opengate_f_nc_gate_f_nc_gate_f,
	&object_opengate_g_nc_gate_g_nc_gate_g,
	&object_opengate_h_nc_gate_h_nc_gate_h
};

// List of dynamic collision for number doors
NJS_OBJECT* GateColliObjects[8];

void ChaoNumbers_Display(task* tp)
{
	taskwk* twp = tp->twp;
	njPushMatrix(0);
	njSetTexture(&texlist_obj_al_race);
	//njTranslate(0, 0, tp->awp->work.f[0], 0);
	for (int i = 0; i < 8;i++)
		DrawObjectClip(GateObjects[i], 1.0f);
	njPopMatrix(1u);
}

void ChaoNumbers_Main(task* tp)
{
	/*
	anywk* awp = tp->awp;
	if (awp->work.ub[15])
	{
		float frame = min(30, awp->work.f[0] + 0.5f);
		awp->work.f[0] = frame;
	}
	*/
	ChaoNumbers_Display(tp);
}

void ChaoRaceNumbers_Load(task* tp)
{
	anywk* awp = tp->awp;
	taskwk* twp = tp->twp;
	//awp->work.ub[15] = 0; // Resets the gate open status
	//awp->work.f[0] = 0; // Resets the gate Y offset
	tp->exec = ChaoNumbers_Main;
	tp->disp = ChaoNumbers_Display;
	tp->dest = B_Destructor; // Deletes collision
	// Create collision
	for (int i = 0; i < 8;i++)
	{
		GateColliObjects[i] = SetModelCollision(GateObjects[i], tp, SurfaceFlags_Solid);
		GateColliObjects[i]->pos[0] = GateObjects[i]->pos[0];
		GateColliObjects[i]->pos[1] = GateObjects[i]->pos[1];
		GateColliObjects[i]->pos[2] = GateObjects[i]->pos[2];
		GateColliObjects[i]->scl[0] = 1.0f;
		GateColliObjects[i]->scl[1] = 1.0f;
		GateColliObjects[i]->scl[2] = 1.0f;
	}
}