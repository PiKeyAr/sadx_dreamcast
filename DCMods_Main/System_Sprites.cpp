#include "stdafx.h"

FunctionPointer(float, nlateBegin2D, (LATE flags, float pri), 0x404290);
 
// njDrawSprite2D with point filtering force enabled (for subtitle font).
void __cdecl njDrawSprite2D_DrawNow_Point(NJS_SPRITE* sp, Int n, Float pri, NJD_SPRITE attr)
{
	NJS_TEXLIST* texlist; // ecx
	NJS_TEXANIM* texanim; // esi
	Sint16 _cx; // st7
	Sint16 cy; // st6
	float sy; // ST14_4
	float v9; // ST2C_4
	float v10; // st7
	float v11; // ST30_4
	float v12; // st7
	float v13; // st7
	float v14; // eax
	float v15; // ecx
	NJS_COLOR v16; // edi
	NJS_COLOR color_; // ST20_4
	int texid; // esi
	float result_x; // [esp+4h] [ebp-74h]
	float v20; // [esp+8h] [ebp-70h]
	float result_y; // [esp+Ch] [ebp-6Ch]
	float u1; // [esp+10h] [ebp-68h]
	float v1; // [esp+14h] [ebp-64h]
	float u2; // [esp+18h] [ebp-60h]
	float v2; // [esp+1Ch] [ebp-5Ch]
	float v26; // [esp+28h] [ebp-50h]
	NJS_VECTOR vs; // [esp+2Ch] [ebp-4Ch]
	NJS_QUAD_TEXTURE_EX points; // [esp+38h] [ebp-40h]

	if (sp)
	{
		texlist = sp->tlist;
		if (texlist)
		{
			Direct3D_SetTexList(texlist);
			texanim = &sp->tanim[n];
			u1 = texanim->u1 * 0.0039215689f;
			v1 = texanim->v1 * 0.0039215689f;
			u2 = texanim->u2 * 0.0039215689f;
			v2 = texanim->v2 * 0.0039215689f;
			_cx = -texanim->cx;
			cy = -texanim->cy;
			sy = texanim->sy + (float)cy;
			result_x = _cx * sp->sx;
			v26 = (texanim->sx + _cx) * sp->sx;
			result_y = cy * sp->sy;
			v20 = sy * sp->sy;
			if (attr & NJD_SPRITE_ANGLE)
			{
				njPushMatrix(nj_unit_matrix_);
				njRotateZ(0, sp->ang);
				vs.x = result_x;
				vs.y = result_y;
				vs.z = 0.0;
				njCalcVector(0, &vs, &vs);
				points.x = vs.x + sp->p.x;
				v9 = vs.x;
				v10 = vs.y + sp->p.y;
				v11 = vs.y;
				vs.y = result_y;
				points.y = v10;
				vs.x = v26;
				vs.z = 0.0;
				points.z = -1.0f / pri;
				njCalcVector(0, &vs, &vs);
				points.vx1 = vs.x - v9;
				vs.x = result_x;
				v12 = vs.y;
				vs.y = v20;
				vs.z = 0.0;
				points.vy1 = v12 - v11;
				njCalcVector(0, &vs, &vs);
				points.vx2 = vs.x - v9;
				points.vy2 = vs.y - v11;
				njPopMatrixEx();
			}
			else
			{
				points.vy1 = 0.0;
				v13 = result_x + sp->p.x;
				points.vx2 = 0.0;
				points.x = v13;
				points.y = result_y + sp->p.y;
				points.z = -1.0f / pri;
				points.vx1 = v26 - result_x;
				points.vy2 = v20 - result_y;
			}
			if (attr & NJD_SPRITE_HFLIP)
			{
				v14 = u2;
				u2 = u1;
				u1 = v14;
			}
			if (attr & NJD_SPRITE_VFLIP)
			{
				v15 = v2;
				v2 = v1;
				v1 = v15;
			}
			points.u = u1;
			points.vu1 = u2 - u1;
			points.v = v1;
			points.vv1 = 0.0;
			points.vu2 = 0.0;
			points.vv2 = v2 - v1;
			if (attr & NJD_SPRITE_COLOR)
			{
				color_.argb.b = (Uint8)(nj_constant_material_.b * 255.0);
				color_.argb.g = (Uint8)(nj_constant_material_.g * 255.0);
				color_.argb.r = (Uint8)(nj_constant_material_.r * 255.0);
				color_.argb.a = (Uint8)(nj_constant_material_.a * 255.0);
				v16.color = color_.color;
			}
			else
			{
				v16.color = -1;
			}
			Direct3D_EnableHudAlpha((attr & NJD_SPRITE_ALPHA) == NJD_SPRITE_ALPHA);
			Direct3D_TextureFilterPoint();
			texid = texanim->texid;
			CurrentHUDColor = v16.color;
			Direct3D_SetTextureNum(texid);
			Direct3D_DrawQuad(&points);
			Direct3D_TextureFilterLinear();
		}
	}
}

// Queued njDrawSprite2D with point filtering force enabled (for subtitle font).
void late_DrawSprite2D_Point(NJS_SPRITE* sp, Int n, Float pri, NJD_SPRITE attr, LATE queue_flags)
{
	QueuedModelSprite* v5; // eax
	QueuedModelSprite* v6; // ebx
	float pria; // [esp+0h] [ebp-4h]
	float v8; // [esp+10h] [ebp+Ch]

	if (!loop_count && (unsigned int)sp >= 0x100000 && !VerifyTexList(sp->tlist))
	{
		pria = pri;
		if (pri >= -2.0f && pri < 10000.0f)
		{
			pria = pri + 12048.0f;
		}
		v5 = (QueuedModelSprite*)AllocateQueuedModel(n, 0x98, QueuedModelType_Sprite2D, (QueuedModelFlagsB)queue_flags);
		v6 = v5;
		if (v5)
		{
			v5->SpritePri = pria;
			v5->SpriteFlags = attr;
			memcpy(&v5->Sprite, sp, sizeof(v5->Sprite));
			njGetMatrix(v5->Transform);
			AddToQueue(&v6->Node, pria, 1);
		}
		else
		{
			v8 = nlateBegin2D(queue_flags, pria);
			njDrawSprite2D_DrawNow_Point(sp, n, v8, attr);
			npSetZCompare();
			njSetZUpdateMode(1u);
			ResetMaterial();
		}
	}
}