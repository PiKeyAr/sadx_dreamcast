#pragma once

#include <SADXModLoader.h>

FunctionPointer(void, CreateWater, (NJS_VECTOR* position, NJS_VECTOR* scale_v, float scale), 0x004B9540); // Creates a water splash (small)
DataPointer(float, first_alpha, 0x03C688D4); // Swamp transparency

DataPointer(NJS_ACTION, action_c4_tailattame01, 0x011C117C);
DataPointer(NJS_ACTION, action_c4_tailattame02, 0x011C13CC);
DataArray(CHAOS_OBJPV, chaos_objpv, 0x03C5A358, 0x32); // Chaos position table
DataArray(NJS_MATERIAL, matlist_c4_s_skusa_nc_s_hasb, 0x011E3344, 2); // Lilypad material