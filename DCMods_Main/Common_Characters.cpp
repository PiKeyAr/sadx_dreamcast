#include "stdafx.h"
#include "Common_Characters.h"

static bool ModelsLoaded_Characters;

float LSDMinimumSet1 = 16.0f;

// All characters

void DrawNPCShadowFix(NJS_MODEL_SADX* a1)
{
	late_DrawModel(a1, LATE_WZ);
}

void CharBubbleHook(NJS_SPRITE* a1, Int n, NJD_SPRITE attr)
{
	if (!IsCameraUnderwater)
		late_z_ofs___ = -20000.0f;
	late_DrawSprite3D(a1, n, attr, LATE_LIG);
	late_z_ofs___ = 0.0f;
}

// Sonic

void SonicDashTrailFix(NJS_OBJECT* a1, LATE a2)
{
	late_z_ofs___ = 2500.0f;
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
		late_z_ofs___ = 3500.0f;
	if (ModConfig::EnabledLevels[LevelIDs_Chaos4] && ssStageNumber == LevelIDs_Chaos4)
		late_z_ofs___ = 4500.0f;
	if (!ChkPause())
		a1->basicdxmodel->mats->attr_texId = rand() % 2;
	late_DrawObjectClip(a1, LATE_MAT, 1.0f);
	if (!ChkPause())
		a1->basicdxmodel->mats->attr_texId = 0;
	late_z_ofs___ = 0.0f;
}

void SonicDashTrailFix2(NJS_OBJECT* a1, LATE a2)
{
	late_z_ofs___ = 2500.0f;
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
		late_z_ofs___ = 3500.0f;
	if (ModConfig::EnabledLevels[LevelIDs_Chaos4] && ssStageNumber == LevelIDs_Chaos4)
		late_z_ofs___ = 4500.0f;
	late_DrawObject(a1, a2);
	late_z_ofs___ = 0.0f;
}

void FixLightDashModel(NJS_OBJECT* obj)
{
	if (obj->basicdxmodel != NULL)
	{
		obj->basicdxmodel->mats[0].diffuse.color = 0xFF000000;
		obj->basicdxmodel->mats[0].specular.color = 0xFFFFFFFF;
		obj->basicdxmodel->mats[0].attrflags = 0x26182400;
		obj->basicdxmodel->mats[0].exponent = 6;
	}
	if (obj->child != NULL)
		FixLightDashModel(obj->child);
	if (obj->sibling != NULL)
		FixLightDashModel(obj->sibling);
}

void Sonic_DisplayLightDashModelX(taskwk* data1, motionwk** mwp, playerwk* pwp)
{
	// Don't render the Light Speed Dash model in Super Sonic mode - for compatibility with the Super Sonic mod.
	if (flagSuperSonicMode)
		return;
	NJS_ACTION action;
	NJS_ARGB color;
	float timer;
	float basedepth = 8000.0f;
	if (ModConfig::EnabledLevels[LevelIDs_SpeedHighway] && ssStageNumber == LevelIDs_SpeedHighway && ssActNumber == 2) 
		basedepth = 1000.0f;
	if (ModConfig::EnabledLevels[LevelIDs_StationSquare] && ssStageNumber == LevelIDs_StationSquare && ssActNumber == 3) 
		basedepth = 1000.0f;
	if (!loop_count)
	{
		// Main stuff
		unsigned short reqaction = pwp->mj.reqaction;
		action.object = SONIC_OBJECTS[54];
		if (pwp->mj.mtnmode == 2)
			action.motion = pwp->mj.actwkptr->motion;
		else
			action.motion = pwp->mj.plactptr[reqaction].actptr->motion;

		timer = (float)(gu32LocalCnt & 0x7F);
		if (timer >= 64.0f)
			timer = 128.0f - timer;
		float v6x = timer * 0.015625f;
		float v5x = v6x * 0.18f;
		color.r = 0;
		color.g = 0.28f - v5x;
		color.b = 1;
		color.a = 0;
		// Render
		njControl3D(NJD_CONTROL_3D_OFFSET_MATERIAL);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_ONE);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njPushMatrixEx();
		// Main
		___njSetConstantMaterial(&color);
		late_z_ofs___ = basedepth;
		late_ActionEx(&action, pwp->mj.nframe, LATE_WZ);
		// Outer 1
		njScale(0, 1.05f, 1.05f, 1.05f);
		color.r = 0.05f;
		color.g -= 0.15f;
		___njSetConstantMaterial(&color);
		late_z_ofs___ = basedepth + 300.0f;
		late_ActionEx(&action, pwp->mj.nframe, LATE_WZ);
		// Outer 2
		color.g -= 0.05f;
		color.b = 0.3f;
		njScale(0, 1.05f, 1.05f, 1.05f);
		___njSetConstantMaterial(&color);
		late_z_ofs___ = basedepth + 600.0f;
		late_ActionEx(&action, pwp->mj.nframe, LATE_WZ);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		njPopMatrixEx();
		late_z_ofs___ = 0;
	}
}

void SpindashChargeLinesHook(NJS_POINT3COL* a1, int a2, NJD_DRAW attr, LATE a4)
{
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
		late_z_ofs___ = 4000.0f;
	if (ModConfig::EnabledLevels[LevelIDs_StationSquare] && ssStageNumber == LevelIDs_StationSquare && ssActNumber == 3)
		late_z_ofs___ = 1000.0f;
	late_DrawTriangle3D(a1, a2, attr, a4);
	late_z_ofs___ = 0.0f;
}

void SpindashChargeSpriteHook(NJS_SPRITE* sp, Int n, NJD_SPRITE attr, LATE zfunc_type)
{
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
		late_z_ofs___ = 4000.0f;
	if (ModConfig::EnabledLevels[LevelIDs_StationSquare] && ssStageNumber == LevelIDs_StationSquare && ssActNumber == 3)
		late_z_ofs___ = 1000.0f;
	late_DrawSprite3D(sp, n, attr, zfunc_type);
	late_z_ofs___ = 0.0f;
}

void SonicFrozenCubeFix(NJS_OBJECT* a1)
{
	DrawObjectClipMesh(a1, LATE_MAT, 1.0f);
}

static Trampoline* SonicSnowboard_t = nullptr;
static void __cdecl SonicSnowboard_r(task* tp)
{
	// The snowboard is actually a non-paletted model (SL)
	const auto original = TARGET_DYNAMIC(SonicSnowboard);
	___dsSetPalette(2);
	original(tp);
	___dsSetPalette(0);
}

// Tails

void TailWhipEffect(NJS_POINT3COL* p, int n, NJD_DRAW atr, LATE flg)
{
	// Something isn't looking bright enough? Just draw it twice lol!
	late_DrawPolygon3D(p, n, atr, flg);
	late_DrawPolygon3D(p, n, atr, flg);
}

static Trampoline* TailsSnowboard_t = nullptr;
static void __cdecl TailsSnowboard_r(task* tp)
{
	// The snowboard is actually a non-paletted model (SL)
	const auto original = TARGET_DYNAMIC(TailsSnowboard);
	___dsSetPalette(2);
	original(tp);
	___dsSetPalette(0);
}

// Knuckles

void DrawMaximumHeatAuraCallback(void* rate_p)
{
	float rate = *(float*)&rate_p;
	KNUCKLES_OBJECTS[47]->basicdxmodel->mats->attr_texId = 1;
	float envmap = 2.0f - 1.4f * rate * rate;
	ScaleEnvironmentMap(envmap, envmap);
	SetMaterial((float)(rate * -0.6000000238418579), 1.0f, 1.0f, 1.0f);
	dsDrawModel_S(KNUCKLES_OBJECTS[47]->basicdxmodel);
	RestoreEnvironmentMap();
	ResetMaterial();
}

void DrawMaximumHeatChargeCallback(taskwk* twp)
{
	KNUCKLES_OBJECTS[47]->basicdxmodel->mats->attr_texId = 2;
	float scale = (((twp->scl.z - twp->scl.y) * twp->timer.f) + twp->scl.y);
	float alpha = (((twp->counter.f - twp->value.f) * twp->timer.f) + twp->value.f);
	float envmap = scale * scale / 2;
	ScaleEnvironmentMap(envmap, envmap);
	SetMaterial(alpha, 1.0f, 1.0f, 1.0f);
	dsDrawModel_S(KNUCKLES_OBJECTS[47]->basicdxmodel);
	//PrintDebug("Rate: %f Alpha: %f E1: %f E2: %f\n", scale, alpha, EnvMap1, EnvMap2);
	RestoreEnvironmentMap();
	ResetMaterial();
}

// Maximum Heat aura
void __cdecl KnuEffectPutChargeComp_r(NJS_VECTOR* position, float rate)
{
	if (!loop_count)
	{
		njPushMatrix(0);
		njTranslateV(0, position);
		float scale = 2.0f - rate * rate;
		njScale(0, scale, scale, scale);
		if ((rate * 262144.0f))
			njRotateY(0, (Angle)(rate * 262144.0f));
		njSetTexture(&texlist_knu_eff);
		late_SetFunc(DrawMaximumHeatAuraCallback, (void*)(*(int*)&rate), 0.0f, LATE_LIG);
		njPopMatrix(1u);
	}
}

// Maximum Heat charge circle
void dispKnuEffectChargeScl_r(task* tsk)
{
	taskwk* v1 = tsk->twp;
	taskwk* player = playertwp[0];
	if (player)
	{
		if (!loop_count)
		{
			float scale = (((v1->scl.z - v1->scl.y) * v1->timer.f) + v1->scl.y);
			njPushMatrix(0);
			njTranslate(0, player->cwp->info->center.x, player->cwp->info->center.y, player->cwp->info->center.z);
			njScale(0, scale, scale, scale);
			njSetTexture(&texlist_knu_eff);
			late_SetFunc((void(__cdecl*)(void*))DrawMaximumHeatChargeCallback, (void*)v1, 0.0f, LATE_LIG);
			njPopMatrix(1u);
		}
	}
}

void KnucklesPunch_AddConstantMaterial(float a, float r, float g, float b)
{
	SaveControl3D();
	OnControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
	SetMaterial(a, r, g, b);
}

void KnucklesPunch_RemoveConstantMaterial(NJS_OBJECT* a1, LATE a2)
{
	late_DrawObject(a1, a2);
	LoadControl3D();
}

// Gamma

void GammaSetMaterial(float a, float r, float g, float b)
{
	bool chest = true;
	// Disable in Character Select
	if (ulGlobalMode == MD_TITLE2)
		chest = false;
	// Disable in dying animation
	if (usPlayer == Characters_Gamma && playertwp[0] && playertwp[0]->mode == 51)
		chest = false;
	SetMaterial(chest ? 0.847f : a, r, g, b);
}

void GammaNjControl3D(NJD_CONTROL_3D a1)
{
	bool chest = true;
	// Disable in Character Select
	if (ulGlobalMode == MD_TITLE2)
		chest = false;
	// Disable in dying animation
	if (usPlayer == Characters_Gamma && playertwp[0] && playertwp[0]->mode == 51)
		chest = false;
	njControl3D(chest ? NJD_CONTROL_3D_ENABLE_ALPHA : NJD_CONTROL_3D_CONSTANT_MATERIAL);
}

// Big

void BigFishingThingFix(NJS_OBJECT* a1)
{
	late_DrawObjectClip(a1, LATE_MAT, 1.0f);
}

void BigDisplayFix(NJS_ACTION* action, float frame, float scale)
{
	late_z_ofs___ = -52952.0f;
	DrawAction(action, frame, LATE_WZ, scale, (void(__cdecl*)(NJS_MODEL_SADX*, int, int))late_DrawModelEx);
	late_z_ofs___ = 0.0f;
}

// Super Sonic

void SuperSonicAuraHook(NJS_OBJECT* a1, LATE a2)
{
	// Same deal as the barriers basically
	if ((unsigned __int16)(ssActNumber | (ssStageNumber << 8)) >> 8 == 3 && ssActNumber == 2) 
		late_z_ofs___ = 0; 
	else 
		late_z_ofs___ = 20048.0f;
	if (ModConfig::EnabledLevels[LevelIDs_SpeedHighway] && ssStageNumber == LevelIDs_SpeedHighway && ssActNumber == 2) 
		late_z_ofs___ = 500.0f;
	late_DrawObject(a1, a2);
	late_z_ofs___ = 0;
}

// Metal Sonic

// Version of DrawSonicMotion for Metal Sonic stuff
void DrawSonicMotion_r_AfterImage(int action, playerwk* pwk)
{
	NJS_OBJECT* obj; // eax
	NJS_ACTION* act; // ecx

	if (!gu8flgPlayingMetalSonic)
		return;

	act = pwk->mj.plactptr[action].actptr;
	obj = act->object;

	if (playerpwp[0]->breathtimer)
		late_z_ofs___ = -35000.0f;
	else
		late_z_ofs___ = 5000.0f;

	if (obj == SONIC_OBJECTS[0])
	{
		njSetTexture(&texlist_metalsonic);
		if (act == SONIC_ACTIONS[0]) // Metal Sonic takes a shit
		{
			late_DrawMotionClip(SONIC_OBJECTS[68], SONIC_MOTIONS[1], pwk->mj.nframe, LATE_MAT, 0);
		}
		else
		{
			late_DrawMotionClip(SONIC_OBJECTS[68], pwk->mj.plactptr[action].actptr->motion, pwk->mj.nframe, LATE_MAT, 0);
		}
	}
	else if (obj == SONIC_OBJECTS[66])
	{
		njSetTexture(&texlist_metalsonic);
		late_DrawMotionClip(SONIC_OBJECTS[69], pwk->mj.plactptr[action].actptr->motion, pwk->mj.nframe, LATE_MAT, 0);
	}
	else if (obj == SONIC_OBJECTS[67])
	{
		njSetTexture(&texlist_metalsonic);
		late_DrawMotionClip(SONIC_OBJECTS[70], pwk->mj.plactptr[action].actptr->motion, pwk->mj.nframe, LATE_MAT, 0);
	}
	else
	{
		late_ActionClip(act, pwk->mj.nframe, LATE_MAT, 0);
	}
	late_z_ofs___ = 0;
}

static void __declspec(naked) DrawSonicMotion_asm()
{
	__asm
	{
		push esi // a2
		push eax // a1

		// Call your __cdecl function here:
		call DrawSonicMotion_r_AfterImage

		pop eax // a1
		pop esi // a2
		retn
	}
}

void Characters_Load()
{
	if (ModelsLoaded_Characters)
		return;
	if (ModConfig::RestoreHumpAnimations)
	{
		sonic_action[37].racio = 0.5f; // Sonic holding item
		miles_action[62].racio = 0.5f; // Tails holding item
		knuckles_action[74].racio = 1.0f; // Knuckles holding item
		amy_action[47].racio = 0.25f; // Amy holding item; half the original value because it looks stupid	
		// Other fixes
		WriteData<1>((char*)0x0045C08B, 0x50u); // Correct animation index for Tails' "jump while holding something"
		miles_action[80].actptr = MILES_ACTIONS[21]; // Tails jumping while holding something
		miles_action[80].racio = 0.5f;
		miles_action[81].actptr = MILES_ACTIONS[22]; // Tails transition jumping while holding something
		miles_action[81].racio = 0.5f;
		miles_action[82].actptr = MILES_ACTIONS[23]; // Tails falling while holding something
		knuckles_action[24].racio = 1.0f; // Fix wrong speed for Knuckles' push animation (same value as pull animation)
		knuckles_action[12].racio = 0.5f; // Fix weird animation speed for Knuckles' running
		knuckles_action[13].racio = 0.5f; // Fix weird animation speed for Knuckles' running
	}
	ModelsLoaded_Characters = true;
}

void LSDStupidHack(taskwk* twp, motionwk2* mwp, playerwk* pwp)
{
	pwp->spd.x = pwp->spd.x * 0.5f;
	pwp->spd.z = pwp->spd.z * 0.5f;
	PGetInertia(twp, mwp, pwp);
}

void Characters_Init()
{
	ReplacePVR("SSTX_BODY");
	ReplacePVR("STX_ICE0");
	ReplacePVM("VER1_WING");
	ReplacePVM("VER2_WING");
	ReplacePVM("VER3_WING");
	ReplacePVM("VER4_WING");
	ReplacePVM("WING_P");
	ReplacePVM("WING_T");
	ReplacePVM("ANCIENT_LIGHT");
	ReplacePVM("A_MASTER");
	ReplacePVM("BIG_KAERU");
	ReplacePVM("E102");
	ReplacePVM("E102BEAM");
	ReplacePVM("E102EFFECT");
	ReplacePVM("E102HIT");
	ReplacePVM("KNU_EFF");
	ReplacePVM("EGGROB");
	// General stuff
	FixLightDashModel(SONIC_OBJECTS[54]); // Resets material color to 0xFF000000 to make it work with offset material
	WriteData((int*)0x007802D2, 0x03AB9908); // Redirect the old motion callback to fix missing animations
	WriteCall((void*)0x005252E8, DrawNPCShadowFix);
	WriteCall((void*)0x004A07D9, CharBubbleHook); // Draw bubbles behind water
	// Stupid hacks for Windy Valley 3 and other stages
	WriteCall((void*)0x004A1827, SpindashChargeLinesHook);
	WriteCall((void*)0x004A1E55, SpindashChargeSpriteHook);
	// Snow/sandboard "fixes"
	SonicSnowboard_t = new Trampoline(0x00494140, 0x00494145, SonicSnowboard_r);
	TailsSnowboard_t = new Trampoline(0x0045BC40, 0x0045BC45, TailsSnowboard_r);
	// Character effects
	// Sonic
	WriteJump((void*)0x004A1630, Sonic_DisplayLightDashModelX);
	WriteCall((void*)0x004A0F56, SonicDashTrailFix);
	WriteCall((void*)0x004A1233, SonicDashTrailFix2);
	WriteData((float*)0x004A1216, 2500.0f); // Long dash trail depth bias
	WriteData<1>((char*)0x004A1220, 0i8); // Spindash trail queued flags
	WriteCall((void*)0x004A22A6, SonicFrozenCubeFix);
	// Light Speed Dash distance fix - not accurate but seems close enough
	if (ModConfig::EnableLSDFix)
	{
		WriteData((float*)0x0049306A, 16.0f); // Initial X speed
		WriteData((float*)0x00492FEB, 16.0f); // Initial Y speed
		WriteData((float*)0x00492CBF, 16.0f); // Minimum speed to be set
		WriteData((float**)0x00492CB0, &LSDMinimumSet1); // Minimum speed to be compared
		// With original DC speed values the character slips down on Twinkle Park roofs.
		// The exact cause of this is currently unknown, but this can be prevented by halving horizontal speed... Pretty hacky
		WriteCall((void*)0x00492ADB, LSDStupidHack);
	}
	// Tail attacks
	WriteCall((void*)0x004BC6BB, TailWhipEffect);
	WriteCall((void*)0x004BCAF1, TailWhipEffect);
	// Knuckles
	WriteJump((void*)0x004C1330, KnuEffectPutChargeComp_r); // Maximum Heat aura
	WriteJump((void*)0x004C1410, dispKnuEffectChargeScl_r); // Maximum Heat charge
	WriteCall((void*)0x004C1258, KnucklesPunch_AddConstantMaterial);
	WriteCall((void*)0x004C1305, KnucklesPunch_RemoveConstantMaterial);
	// Amy
	AMY_OBJECTS[1]->child->child->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Barrel
	AMY_OBJECTS[1]->child->child->basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Barrel
	object_er_9000_eggmanrobo_er_head.basicdxmodel->mats[11].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
	object_er_9000_eggmanrobo_er_head.basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT;
	// Big
	WriteCall((void*)0x0046C547, BigFishingThingFix);
	if (ModConfig::BigBeltFix)
		WriteCall((void*)0x0048BA29, BigDisplayFix);
	// Gamma
	WriteData<1>((char*)0x00480080, 0x0i8); // Light type for Gamma's upgrades
	E102_OBJECTS[5]->basicdxmodel->mats[0].attr_texId = 10; 
	E102_OBJECTS[5]->basicdxmodel->mats[0].attrflags |= NJD_FLAG_USE_TEXTURE;
	*E102_OBJECTS[6] = *E102_OBJECTS[5]; // Gamma's projectile fix. I have no idea why this works, but ok I guess
	WriteCall((void*)0x0047FDFA, GammaNjControl3D);
	WriteCall((void*)0x0047FE13, GammaSetMaterial);
	// E102 unnecessary alpha (must check if the model hasn't been replaced)
	if (E102_OBJECTS[0]->getnode(54) != nullptr && E102_OBJECTS[0]->getnode(54)->basicdxmodel != nullptr && E102_OBJECTS[0]->getnode(54)->basicdxmodel->nbMat == 9)
		E102_OBJECTS[0]->getnode(54)->basicdxmodel->mats[8].attrflags &= ~NJD_FLAG_USE_ALPHA;
	// Super Sonic
	WriteCall((void*)0x0055E782, SuperSonicAuraHook);
	WriteCall((void*)0x0055E76A, SuperSonicAuraHook);
	WriteCall((void*)0x0055E72A, SuperSonicAuraHook);
	object_question_question_question.basicdxmodel->mats[0].attr_texId = 10; // Fix wrong texture on question mark
	// Metal Sonic
	WriteCall((void*)0x004947B7, DrawSonicMotion_asm);
	// Tikal
	object_tikal_a_l_a4.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_tikal_a_l_a7.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	// Eggman
	object_g_g0000_eggman_r_a.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_g_g0000_eggman_r_b.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_g_g0000_eggman_l_a.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_g_g0000_eggman_l_b.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_gm_gm0000_eggmoble_r_a.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_gm_gm0000_eggmoble_r_b.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_gm_gm0000_eggmoble_l_a.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	object_gm_gm0000_eggmoble_l_b.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA;
	chos6_object_egm01_egm00_egm00.basicdxmodel->mats[15].diffuse.color = 0xFFFFFFFF; // Chaos6 Eggmobile
	evpvm_12[2].ptexlist = &_texlist_ev_eggmoble0; // Fixes wrong texlist on the crashed Eggmobile in Super Sonic story
	// Chaos
	ChaosFixes_Init();
	// Robot chest stuff
	WriteData<1>((char*)0x004CFC05, 0x08); // Zero constant material thing
	WriteData<1>((char*)0x004CFC99, 0x08); // Zero constant material thing
	WriteData<1>((char*)0x00567CF2, 0x08); // E101 Beta (boss) constant material
	object_e101old_kihon0_e101old_e_head.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // E101 unnecessary alpha (boss model)
	_object_e101old_kihon0_e101old_e_head.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // E101 unnecessary alpha (cutscene model)
	object_e103_kihon0_e103_e_head.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // E103 unnecessary alpha (cutscene model)
	object_e104_kihon0_e104_e_head.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // E104 unnecessary alpha (cutscene model)
	object_e105old_kihon0_e105old_e_head.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // E105 unnecessary alpha (cutscene model)
	WriteData((float*)0x00567D08, 0.847f); // E101 (boss model)
	WriteData((float*)0x004E7BFD, 0.847f); // E103 (boss model)
	WriteData((float*)0x004E7C40, 0.847f); // E103 (boss model/other)
	WriteData((float*)0x00605813, 0.847f); // E104 (boss model)
	WriteData((float*)0x006F4718, 0.847f); // E101 (cutscene model)
	WriteData((float*)0x006F3F94, 0.847f); // E103 (cutscene model)
	WriteData((float*)0x006F3D54, 0.847f); // E104 (cutscene model)
	WriteData((float*)0x006F3B24, 0.847f); // E105 (cutscene model)
	// Material/vertex color fixes
	// Eggman's eyes (UV-less / SA1 model only)
	if (ModConfig::DLLLoaded_SA1Chars)
	{
		// Regular model
		action_g_g0001_eggman.object->child->child->basicdxmodel->mats[4].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		// Event Eggmobile 0
		action_gm_gm0004_eggmoble.object->child->child->basicdxmodel->mats[4].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		// Tornado 2 white diffuse during transformation custcene
		AddWhiteDiffuseMaterial(&action_tr2henkei_tr2_tf_event_s_tr2rt.object->child->child->sibling->sibling->sibling->child->basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&action_tr2henkei_tr2_tf_event_s_tr2rt.object->child->child->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&action_tr2henkei_tr2_tf_event_s_tr2rt.object->child->child->sibling->sibling->sibling->child->basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&action_tr2henkei_tr2_tf_event_s_tr2rt.object->child->child->sibling->sibling->sibling->child->basicdxmodel->mats[3]);
	}
}

void Characters_OnFrame()
{
	// Fix broken welds after playing as Metal Sonic
	if (!ModConfig::DLLLoaded_SADXFE)
	{
		if (ulGlobalMode == MD_TITLE2_INIT) 
			gu8flgPlayingMetalSonic = false;
	}
}