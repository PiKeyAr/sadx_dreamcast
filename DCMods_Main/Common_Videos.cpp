#include "stdafx.h"
#include "FileSystem.h"
#include "Common_Videos.h"

static VideoFadeoutModes VideoFadeMode_Mod; // 0 - fade in, 1 - play, 2 - fade out, 3 - fade out finish
static int VideoFadeoutRectOpacity = 0; // Fadeout opacity
static bool VideoCancel = false; // True if the player has pressed Start/A on a video
static bool HDVideosCheck = false; // Set to true after the check for AI HD FMV is done
static NJS_COLOR VideoOverlayColor = { 0x13FFFFFF }; // Colorization effect color

// Replacement for the movie_file array with SA1 Sonic Team logo
_m_file VideoDataArray[] = {
	{ "system\\intro.mpg", 0, 6420, 640, 448 },
	{ "system\\Sa1.mpg", 0, 5760, 640, 480 },
	{ "system\\Sa2.mpg", 0, 786, 640, 480 },
	{ "system\\Sa3.mpg", 0, 1750, 640, 480 },
	{ "system\\Sa4.mpg", 0, 1120, 640, 480 },
	{ "system\\Sa5.mpg", 0, 830, 640, 480 },
	{ "system\\Sa6.mpg", 0, 1250, 640, 480 },
	{ "system\\Sa7.mpg", 0, 630, 640, 480 },
	{ "system\\Sa8.mpg", 0, 1110, 640, 480 },
	{ "system\\sonicteam.mpg", 0, 300, 640, 480 },
};

// Centers the video frame vertically for SA1 videos that are 448 in height rather than 480
void AdjustVideoFrame()
{
	float centering = 0.0f;
	// Get proper width/height from the array because SADX gets confused for some reason
	int height_new = VideoDataArray[CurrentVideoID].s16H;
	// If playing the logo or SA1 intro, adjust the center of the video frame
	if (height_new < 480.0f)
	{
		centering = (480.0f - (float)height_new) / 2.0f;
	}
	VideoFrame.p.y = VideoFrame.p.y + centering;
}

// Input hook. This is not in OnInput because the movie player polls input too early for that to work
void InputHookForVideos()
{
	if (CurrentVideoID != -1 && ulGlobalMode != MD_LOGO && ulGlobalMode != MD_TITLE2_INIT)
	{
		// Check for A/Start press to initiate fadeout
		if (per[0]->press & (Buttons_Start | Buttons_A))
		{
			VideoCancel = true;
			movie_pack[0][1] = -1; // Set the second video in the video list to -1 to end playback
			if (VideoFadeMode_Mod < VideoFadeoutModes::FadeOut)
			{
				VideoFadeMode_Mod = VideoFadeoutModes::FadeOut;
			}
		}
		// If the fadeout has finished and the skip button was pressed, simulate a D-Pad Up+Down press to make original code end playback
		if (VideoFadeMode_Mod == VideoFadeoutModes::FadeOutFinish)
		{
			if (VideoCancel)
				per[0]->press |= (Buttons_Down | Buttons_Up);
			else
				VideoFadeMode_Mod = VideoFadeoutModes::FadeIn;
		}
	}
	CheckReset();
}

// A colored version of the DrawFadePoly function that draws a color overlay on top of the video
void DrawFadePolyColor(NJS_COLOR color)
{
	NJS_POINT2COL VideoFrameRect; // Rectangle to draw over the video for colorization effect
	NJS_POINT2 PointsVideoframeRect[4]; // Points for the video colorization effect
	NJS_COLOR ColorsVideoFrameRect[4]; // Colors array for the video colorization effect
	PointsVideoframeRect[0] = { 0.0f, 0.0f };
	PointsVideoframeRect[1] = { (float)HorizontalResolution, 0.0f };
	PointsVideoframeRect[2] = { 0.0f, (float)VerticalResolution };
	PointsVideoframeRect[3] = { (float)HorizontalResolution, (float)VerticalResolution };
	for (int c = 0; c < 4; c++)
		ColorsVideoFrameRect[c].color = color.color;
	VideoFrameRect.p = PointsVideoframeRect;
	VideoFrameRect.col = ColorsVideoFrameRect;
	VideoFrameRect.num = 4;
	VideoFrameRect.tex = NULL;
	njColorBlendingMode(0, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	njSetZCompare(7u);
	njSetZUpdateMode(0);
	___SAnjDrawPolygon2D(&VideoFrameRect, 4, -1000.0f, NJD_TRANSPARENT | NJD_FILL | NJD_DRAW_CONNECTED);
	njSetZCompare(3u);
	njSetZUpdateMode(1);
	njColorBlendingMode(0, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

void DrawMovieTex_r(int width, int height)
{
	// Set background color
	___njSetBackColor(0, 0, 0);

	// Display the video frame
	// TODO: Figure out what I broke so that it draws the previous video for a few frames
	if (VideoDataArray[CurrentVideoID].efade == -1 || PreviousVideoFrameCopy < VideoDataArray[CurrentVideoID].efade + 51)
		DrawMovieTex(VideoDataArray[CurrentVideoID].s16W, max(480, VideoDataArray[CurrentVideoID].s16H));

	// Display the "colorized" overlay
	if (ModConfig::ColorizeVideos)
		DrawFadePolyColor(VideoOverlayColor);

	// Fade out when fadeout start frame is reached
	if (PreviousVideoFrameCopy > VideoDataArray[CurrentVideoID].efade)
		VideoFadeMode_Mod = VideoFadeoutModes::FadeOut;

	// Draw the fadeout rectangle
	if (VideoFadeoutRectOpacity != 255)
		DrawFadePoly(VideoFadeoutRectOpacity);

	// Update fadeout alpha
	switch (VideoFadeMode_Mod)
	{
	case VideoFadeoutModes::FadeIn:
		VideoFadeoutRectOpacity = min(255, VideoFadeoutRectOpacity + 5);
		break;
	case VideoFadeoutModes::FadeOut:
		VideoFadeoutRectOpacity = max(0, VideoFadeoutRectOpacity - 5);
		if (VideoFadeoutRectOpacity <= 1)
			VideoFadeMode_Mod = VideoFadeoutModes::FadeOutFinish;
		break;
	}
}

void Videos_Init()
{
	bool SA1DCOriginalFiles = Exists(helperFunctionsGlobal->GetReplaceablePath("system\\SLH448_1.MPG"));

	switch (ModConfig::SonicTeamLogo)
	{
		case SonicTeamLogoConfig::Animated:
			movie_pack[0][0] = 9;
			movie_pack[0][1] = 0;
			movie_pack[0][2] = -1;
			// Check for the original SA1 video (320x448)
			VideoDataArray[9].fname = SA1DCOriginalFiles ? "system\\SLH448_1.MPG" : "system\\SONICTEAM.MPG";
			VideoDataArray[9].s16W = SA1DCOriginalFiles ? 320 : 640;
		case SonicTeamLogoConfig::Off:
			WriteData<1>((void*)0x0042CCF3, 0x0F); // Disable the SADX Sonic Team logo image
			break;
		case SonicTeamLogoConfig::Static:
		default:
			break;
	}
	if (ModConfig::ColorizeVideos)
	{
		DefaultVideoColor.r = 0.6627450980392157f;
		DefaultVideoColor.g = 0.6627450980392157f;
		DefaultVideoColor.b = 0.6627450980392157f;
	}

	// Kill original DrawFadePoly calls to prevent interference with my fadeout
	WriteData<5>((void*)0x00512E81, 0x90u);
	WriteData<5>((void*)0x00512EC5, 0x90u);
	WriteData<5>((void*)0x00512FB7, 0x90u);
	WriteData<5>((void*)0x00512F59, 0x90u);
	WriteData<5>((void*)0x0051307D, 0x90u);
	WriteData<5>((void*)0x00513162, 0x90u);

	// Video hacks
	WriteCall((void*)0x00513A88, AdjustVideoFrame); // Center video frame vertically if playing Sonic Team logo/SA1 intro
	WriteCall((void*)0x0051330A, DrawMovieTex_r); // DisplayVideoFrame hook to draw everything
	WriteCall((void*)0x00513271, InputHookForVideos); // Wait to cancel videos
	WriteData<1>((char*)0x005132B9, (Buttons_Up | Buttons_Down)); // Wait for D-Pad Up+Down instead of Button_A or Button_Start

	// Write pointers to get data from DC Conversion's video array
	WriteData((_m_file**)0x00513182, (_m_file*)&VideoDataArray);
	WriteData((int**)0x00512EE6, (int*)&VideoDataArray->sfade);
	WriteData((int**)0x00513143, (int*)&VideoDataArray->sfade);
	WriteData((int**)0x00512E9D, (int*)&VideoDataArray->efade);
	WriteData((int16_t**)0x005130D2, (int16_t*)&VideoDataArray->s16W);
	WriteData((int16_t**)0x005130D9, (int16_t*)&VideoDataArray->s16H);
	
	// Check for the intro
	if (ModConfig::SA1Intro)
	{
		bool SA1DCOriginalFiles = Exists(helperFunctionsGlobal->GetReplaceablePath("system\\SAH448_6.MPG"));
		VideoDataArray[0].fname = SA1DCOriginalFiles ? "system\\SAH448_6.MPG" : "system\\INTRO.MPG";
		VideoDataArray[0].s16W = SA1DCOriginalFiles ? 320 : 640;
		VideoDataArray[0].s16H = SA1DCOriginalFiles ? 448 : 480;
	}
	else
	{
		VideoDataArray[0].fname = "system\\RE-US.MPG";
		VideoDataArray[0].sfade = 0;
		VideoDataArray[0].efade = 0x7FFFFFFF; // Broken intentionally in DX because the video has a built-in fadeout
		VideoDataArray[0].s16W = 640;
		VideoDataArray[0].s16H = 480;
	}
}

void Videos_OnFrame()
{
	// Load width and height from HD Videos
	if (!HDVideosCheck && GetModuleHandle(L"sadx-hd-videos") != nullptr)
	{
		for (unsigned int i = 0; i < LengthOfArray(VideoDataArray); i++)
		{
			VideoDataArray[i].s16W = movie_file[0].s16W;
			VideoDataArray[i].s16H = movie_file[0].s16H;
		}
		HDVideosCheck = true;
	}
	// Reset fadeouts in story mode
	if (CurrentVideoID == -1 && VideoFadeMode_Mod != VideoFadeoutModes::FadeIn)
	{
		VideoFadeMode_Mod = VideoFadeoutModes::FadeIn;
		VideoFadeoutRectOpacity = 0;
		VideoCancel = false;
		movie_pack[0][1] = ModConfig::SonicTeamLogo == SonicTeamLogoConfig::Animated ? 0 : -1;
	}
}