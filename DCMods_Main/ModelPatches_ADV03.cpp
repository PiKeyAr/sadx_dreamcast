#include "stdafx.h"

void PatchModels_ADV03()
{
	HMODULE ADV03MODELS = GetModuleHandle(L"ADV03MODELS");

	// dxpc\adv03_past\bg\mra_s_sora_hare.nja.sa1mdl
	/*
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00114EE8)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00114EEC)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00114EF0)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\bg\mra_s_sora_yoru.nja.sa1mdl
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x0011600C)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00116010)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00116014)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\bg\mra_s_sora_yuu.nja.sa1mdl
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x0011712C)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00117130)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00117134)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\bg\mrc_bf_s_skyhiru.nja.sa1mdl
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x0011824C)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00118250)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\bg\mrc_bf_s_skyyoru.nja.sa1mdl
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00119F8C)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x00119F90)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\bg\mrc_bf_s_skyyuu.nja.sa1mdl
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x001190EC)->diffuse.color = 0xFFB2B2B2;
	((NJS_MATERIAL*)(size_t)ADV03MODELS + 0x001190F0)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv03_past\common\models\k_fat.nja.sa1mdl
		//Material colors only

	// dxpc\adv03_past\common\models\k_long.nja.sa1mdl
		//Material colors only

	// dxpc\adv03_past\common\models\k_normal.nja.sa1mdl
		//Material colors only

	// dxpc\adv03_past\common\models\mra_n_sindenyasib.nja.sa1mdl
		//Hierarchy different in DX

	// dxpc\adv03_past\common\models\mra_n_treeaa.nja.sa1mdl
		//Model too different

	// dxpc\adv03_past\common\models\mra_n_treeab.nja.sa1mdl
		//Hierarchy different in DX

	// dxpc\adv03_past\common\models\mrj_n_ido.nja.sa1mdl
		//Model too different

	// dxpc\adv03_past\common\models\mrj_n_sotetubody.nja.sa1mdl
		//Model too different
	*/
	// dxpc\adv03_past\common\models\mrk_k_maruikage.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x0013967C) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x00139680) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x00139684) = { 0, -255 };

	// dxpc\adv03_past\common\models\mrk_k_objtreeb.nja.sa1mdl
		//Model too different

	// dxpc\adv03_past\common\models\mrk_k_objtreeblow.nja.sa1mdl
		//Model too different

	// dxpc\adv03_past\common\models\mrk_k_shikakukage.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x00139570) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x00139574) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV03MODELS + 0x00139578) = { 0, -254 };

	// dxpc\adv03_past\common\models\mrk_nc_k_gmdaikaidan.nja.sa1mdl
		//Model too different

	// dxpc\adv03_past\common\models\mrk_nc_k_gmkaidan.nja.sa1mdl
		//Model too different
}