#include "stdafx.h"

void PatchModels_STG06()
{
	// dxpc\stg06_skydeck\common\models\cloud_kumo_b.nja.sa1mdl
	((NJS_MATERIAL*)0x0214CA18)->diffuse.color = 0xFFFFFFFF;

	// dxpc\stg06_skydeck\common\models\ecsc_sky.nja.sa1mdl
	((NJS_MATERIAL*)0x0214D334)->diffuse.color = 0xFFFFFFFF;
	((NJS_MATERIAL*)0x0214D348)->diffuse.color = 0xFFFFFFFF;
	((NJS_MATERIAL*)0x0214D35C)->diffuse.color = 0xFFFFFFFF;

	// dxpc\stg06_skydeck\common\models\ecsc_skykumo.nja.sa1mdl
	((NJS_MATERIAL*)0x0214E2D4)->diffuse.color = 0xB2FFFFFF;

	// dxpc\stg06_skydeck\common\models\eg_main_a.nja.sa1mdl
	*(NJS_TEX*)0x021D33B0 = { 2550, 255 };
	*(NJS_TEX*)0x021D33B4 = { 2550, -255 };
	*(NJS_TEX*)0x021D33BC = { 0, -254 };
	*(NJS_TEX*)0x021D33C0 = { 2550, 255 };
	*(NJS_TEX*)0x021D33C4 = { 2550, -255 };
	*(NJS_TEX*)0x021D33CC = { 0, -254 };
	*(NJS_TEX*)0x021D33D0 = { 0, -254 };
	*(NJS_TEX*)0x021D33D8 = { 2550, -255 };
	*(NJS_TEX*)0x021D33DC = { 2550, 255 };
	*(NJS_TEX*)0x021D33E0 = { 0, -254 };
	*(NJS_TEX*)0x021D33E8 = { 2550, -255 };
	*(NJS_TEX*)0x021D33EC = { 2550, 255 };
	*(NJS_TEX*)0x021D29D4 = { 510, 255 };
	*(NJS_TEX*)0x021D29D8 = { 510, -2295 };
	*(NJS_TEX*)0x021D29DC = { 0, 255 };
	*(NJS_TEX*)0x021D29E0 = { 0, -2295 };
	*(NJS_TEX*)0x021D29E4 = { 510, 255 };
	*(NJS_TEX*)0x021D29E8 = { 510, -2295 };
	*(NJS_TEX*)0x021D29EC = { 0, 255 };
	*(NJS_TEX*)0x021D29F0 = { 0, -2295 };
	*(NJS_TEX*)0x021D29F4 = { 510, 255 };
	*(NJS_TEX*)0x021D29F8 = { 510, -2295 };
	*(NJS_TEX*)0x021D29FC = { 0, 255 };
	*(NJS_TEX*)0x021D2A00 = { 0, -2295 };
	*(NJS_TEX*)0x021D2A04 = { 7649, 255 };
	*(NJS_TEX*)0x021D2A08 = { 7649, -2295 };
	*(NJS_TEX*)0x021D2A0C = { 0, 255 };
	*(NJS_TEX*)0x021D2A10 = { 0, -2295 };
	*(NJS_TEX*)0x021D2A14 = { 7649, 255 };
	*(NJS_TEX*)0x021D2A18 = { 7649, -2295 };
	*(NJS_TEX*)0x021D2A1C = { 0, 255 };
	*(NJS_TEX*)0x021D2A20 = { 0, -2295 };
	*(NJS_TEX*)0x021D2A24 = { 7649, 255 };
	*(NJS_TEX*)0x021D2A28 = { 7649, -2295 };
	*(NJS_TEX*)0x021D2A2C = { 0, 255 };
	*(NJS_TEX*)0x021D2A30 = { 0, -2295 };
	*(NJS_TEX*)0x021D1FCC = { 0, 255 };
	*(NJS_TEX*)0x021D1FD0 = { 0, -2295 };
	*(NJS_TEX*)0x021D1FD4 = { 510, 255 };
	*(NJS_TEX*)0x021D1FD8 = { 510, -2295 };
	*(NJS_TEX*)0x021D1FDC = { 0, 255 };
	*(NJS_TEX*)0x021D1FE0 = { 0, -2295 };
	*(NJS_TEX*)0x021D1FE4 = { 510, 255 };
	*(NJS_TEX*)0x021D1FE8 = { 510, -2295 };
	*(NJS_TEX*)0x021D1FEC = { 0, 255 };
	*(NJS_TEX*)0x021D1FF0 = { 0, -2295 };
	*(NJS_TEX*)0x021D1FF4 = { 510, 255 };
	*(NJS_TEX*)0x021D1FF8 = { 510, -2295 };
	*(NJS_TEX*)0x021D1FFC = { 0, 255 };
	*(NJS_TEX*)0x021D2000 = { 0, -2295 };
	*(NJS_TEX*)0x021D2004 = { 7649, 255 };
	*(NJS_TEX*)0x021D2008 = { 7649, -2295 };
	*(NJS_TEX*)0x021D200C = { 0, 255 };
	*(NJS_TEX*)0x021D2010 = { 0, -2295 };
	*(NJS_TEX*)0x021D2014 = { 7649, 255 };
	*(NJS_TEX*)0x021D2018 = { 7649, -2295 };
	*(NJS_TEX*)0x021D201C = { 0, 255 };
	*(NJS_TEX*)0x021D2020 = { 0, -2295 };
	*(NJS_TEX*)0x021D2024 = { 7649, 255 };
	*(NJS_TEX*)0x021D2028 = { 7649, -2295 };

	// dxpc\stg06_skydeck\common\models\eg_top_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\eg_under_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\hdai5_l_houdai.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa01v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa02v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa03v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa04v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa05v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa06v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa07v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa08v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa09v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_koa10v.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa01p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa02p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa03p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa04p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa05p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa06p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa07p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa08p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kosa09p.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu01q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu01y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu02q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu02y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu03q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu03y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu04q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu04y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu05q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu05y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu06q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu06y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu07q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu07y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu08q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu08y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu09q.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu09y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu10y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu11y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu12y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu13y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu14y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu15y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu16y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu17y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu18y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu19y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu20y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu21y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu22y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu23y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu24y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu25y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu26y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu27y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu28y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu29y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu30y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu31y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu32y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu33y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\koware04_obj_kotu34y.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skymain_eskwa_d.nja.sa1mdl
	*(NJS_TEX*)0x021FB8B0 = { 508, 2 };
	*(NJS_TEX*)0x021FB8B4 = { 1, 2 };
	*(NJS_TEX*)0x021FB8B8 = { 508, 32 };
	*(NJS_TEX*)0x021FB8BC = { 1, 32 };
	*(NJS_TEX*)0x021FB8C0 = { 508, 64 };
	*(NJS_TEX*)0x021FB8C4 = { 1, 64 };
	*(NJS_TEX*)0x021FB8C8 = { 508, 95 };
	*(NJS_TEX*)0x021FB8CC = { 1, 95 };
	*(NJS_TEX*)0x021FB8D0 = { 508, 127 };
	*(NJS_TEX*)0x021FB8D4 = { 1, 127 };
	*(NJS_TEX*)0x021FB8D8 = { 508, 159 };
	*(NJS_TEX*)0x021FB8DC = { 1, 159 };
	*(NJS_TEX*)0x021FB8E0 = { 508, 190 };
	*(NJS_TEX*)0x021FB8E4 = { 1, 190 };
	*(NJS_TEX*)0x021FB8E8 = { 508, 222 };
	*(NJS_TEX*)0x021FB8EC = { 1, 222 };
	*(NJS_TEX*)0x021FB8F0 = { 508, 254 };
	*(NJS_TEX*)0x021FB8F4 = { 1, 254 };
	*(NJS_TEX*)0x021FB8F8 = { 508, 2 };
	*(NJS_TEX*)0x021FB8FC = { 1, 2 };
	*(NJS_TEX*)0x021FB900 = { 508, 32 };
	*(NJS_TEX*)0x021FB904 = { 1, 32 };
	*(NJS_TEX*)0x021FB908 = { 508, 64 };
	*(NJS_TEX*)0x021FB90C = { 1, 64 };
	*(NJS_TEX*)0x021FB910 = { 508, 95 };
	*(NJS_TEX*)0x021FB914 = { 1, 95 };
	*(NJS_TEX*)0x021FB918 = { 508, 127 };
	*(NJS_TEX*)0x021FB91C = { 1, 127 };
	*(NJS_TEX*)0x021FB920 = { 508, 159 };
	*(NJS_TEX*)0x021FB924 = { 1, 159 };
	*(NJS_TEX*)0x021FB928 = { 508, 190 };
	*(NJS_TEX*)0x021FB92C = { 1, 190 };
	*(NJS_TEX*)0x021FB930 = { 508, 222 };
	*(NJS_TEX*)0x021FB934 = { 1, 222 };
	*(NJS_TEX*)0x021FB938 = { 508, 254 };
	*(NJS_TEX*)0x021FB93C = { 1, 254 };

	// dxpc\stg06_skydeck\common\models\skymain_eskwa_u.nja.sa1mdl
	*(NJS_TEX*)0x021FBC68 = { 508, 2 };
	*(NJS_TEX*)0x021FBC6C = { 1, 2 };
	*(NJS_TEX*)0x021FBC70 = { 508, 32 };
	*(NJS_TEX*)0x021FBC74 = { 1, 32 };
	*(NJS_TEX*)0x021FBC78 = { 508, 64 };
	*(NJS_TEX*)0x021FBC7C = { 1, 64 };
	*(NJS_TEX*)0x021FBC80 = { 508, 95 };
	*(NJS_TEX*)0x021FBC84 = { 1, 95 };
	*(NJS_TEX*)0x021FBC88 = { 508, 127 };
	*(NJS_TEX*)0x021FBC8C = { 1, 127 };
	*(NJS_TEX*)0x021FBC90 = { 508, 159 };
	*(NJS_TEX*)0x021FBC94 = { 1, 159 };
	*(NJS_TEX*)0x021FBC98 = { 508, 190 };
	*(NJS_TEX*)0x021FBC9C = { 1, 190 };
	*(NJS_TEX*)0x021FBCA0 = { 508, 222 };
	*(NJS_TEX*)0x021FBCA4 = { 1, 222 };
	*(NJS_TEX*)0x021FBCA8 = { 508, 254 };
	*(NJS_TEX*)0x021FBCAC = { 1, 254 };
	*(NJS_TEX*)0x021FBCB0 = { 508, 2 };
	*(NJS_TEX*)0x021FBCB4 = { 1, 2 };
	*(NJS_TEX*)0x021FBCB8 = { 508, 32 };
	*(NJS_TEX*)0x021FBCBC = { 1, 32 };
	*(NJS_TEX*)0x021FBCC0 = { 508, 64 };
	*(NJS_TEX*)0x021FBCC4 = { 1, 64 };
	*(NJS_TEX*)0x021FBCC8 = { 508, 95 };
	*(NJS_TEX*)0x021FBCCC = { 1, 95 };
	*(NJS_TEX*)0x021FBCD0 = { 508, 127 };
	*(NJS_TEX*)0x021FBCD4 = { 1, 127 };
	*(NJS_TEX*)0x021FBCD8 = { 508, 159 };
	*(NJS_TEX*)0x021FBCDC = { 1, 159 };
	*(NJS_TEX*)0x021FBCE0 = { 508, 190 };
	*(NJS_TEX*)0x021FBCE4 = { 1, 190 };
	*(NJS_TEX*)0x021FBCE8 = { 508, 222 };
	*(NJS_TEX*)0x021FBCEC = { 1, 222 };
	*(NJS_TEX*)0x021FBCF0 = { 508, 254 };
	*(NJS_TEX*)0x021FBCF4 = { 1, 254 };

	// dxpc\stg06_skydeck\common\models\skymain_kr.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skymain_tkr_off.nja.sa1mdl
	*(NJS_TEX*)0x021B9D78 = { 249, -757 };
	*(NJS_TEX*)0x021B9D7C = { 249, 247 };
	*(NJS_TEX*)0x021B9D80 = { 1, -757 };
	*(NJS_TEX*)0x021B9D84 = { 1, 247 };
	*(NJS_TEX*)0x021B9D88 = { 249, -757 };
	*(NJS_TEX*)0x021B9D8C = { 249, 247 };
	*(NJS_TEX*)0x021B9D90 = { 1, -517 };
	*(NJS_TEX*)0x021B9D94 = { 1, 247 };
	*(NJS_TEX*)0x021B9D98 = { 1, 247 };
	*(NJS_TEX*)0x021B9D9C = { 1, -757 };
	*(NJS_TEX*)0x021B9DA0 = { 249, 247 };
	*(NJS_TEX*)0x021B9DA4 = { 249, -757 };
	*(NJS_TEX*)0x021B9DA8 = { 1, 247 };
	*(NJS_TEX*)0x021B9DAC = { 1, -757 };
	*(NJS_TEX*)0x021B9DB0 = { 249, 247 };
	*(NJS_TEX*)0x021B9DB4 = { 249, -517 };

	// dxpc\stg06_skydeck\common\models\skymain_tkr_on.nja.sa1mdl
	*(NJS_TEX*)0x021BA168 = { 249, -757 };
	*(NJS_TEX*)0x021BA16C = { 249, 247 };
	*(NJS_TEX*)0x021BA170 = { 1, -757 };
	*(NJS_TEX*)0x021BA174 = { 1, 247 };
	*(NJS_TEX*)0x021BA178 = { 249, -757 };
	*(NJS_TEX*)0x021BA17C = { 249, 247 };
	*(NJS_TEX*)0x021BA180 = { 1, -517 };
	*(NJS_TEX*)0x021BA184 = { 1, 247 };
	*(NJS_TEX*)0x021BA188 = { 1, 247 };
	*(NJS_TEX*)0x021BA18C = { 1, -757 };
	*(NJS_TEX*)0x021BA190 = { 249, 247 };
	*(NJS_TEX*)0x021BA194 = { 249, -757 };
	*(NJS_TEX*)0x021BA198 = { 1, 247 };
	*(NJS_TEX*)0x021BA19C = { 1, -757 };
	*(NJS_TEX*)0x021BA1A0 = { 249, 247 };
	*(NJS_TEX*)0x021BA1A4 = { 249, -517 };

	// dxpc\stg06_skydeck\common\models\skytop_buhin_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_buhin_ana.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_buhin_c.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_buhin_d.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_buhin_e.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_buhin_f.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\skytop_nejia.nja.sa1mdl
	*(NJS_TEX*)0x02193258 = { 252, 24 };
	*(NJS_TEX*)0x0219325C = { 252, 2 };
	*(NJS_TEX*)0x02193260 = { 198, 24 };
	*(NJS_TEX*)0x02193264 = { 198, 2 };
	*(NJS_TEX*)0x02193268 = { 151, 24 };
	*(NJS_TEX*)0x0219326C = { 151, 2 };
	*(NJS_TEX*)0x02193270 = { 95, 24 };
	*(NJS_TEX*)0x02193274 = { 151, 24 };
	*(NJS_TEX*)0x02193278 = { 151, 127 };
	*(NJS_TEX*)0x0219327C = { 198, 24 };
	*(NJS_TEX*)0x02193280 = { 198, 127 };
	*(NJS_TEX*)0x02193284 = { 252, 24 };
	*(NJS_TEX*)0x02193288 = { 252, 127 };
	*(NJS_TEX*)0x0219328C = { 2, 129 };
	*(NJS_TEX*)0x02193290 = { 123, 129 };
	*(NJS_TEX*)0x02193294 = { 2, 252 };
	*(NJS_TEX*)0x02193298 = { 123, 252 };
	*(NJS_TEX*)0x0219329C = { 252, 24 };
	*(NJS_TEX*)0x021932A0 = { 252, 2 };
	*(NJS_TEX*)0x021932A4 = { 198, 24 };
	*(NJS_TEX*)0x021932A8 = { 198, 2 };
	*(NJS_TEX*)0x021932AC = { 151, 24 };
	*(NJS_TEX*)0x021932B0 = { 151, 2 };
	*(NJS_TEX*)0x021932B4 = { 111, 252 };
	*(NJS_TEX*)0x021932B8 = { 2, 252 };
	*(NJS_TEX*)0x021932BC = { 2, 149 };
	*(NJS_TEX*)0x021932C0 = { 111, 252 };
	*(NJS_TEX*)0x021932C4 = { 2, 252 };
	*(NJS_TEX*)0x021932C8 = { 2, 149 };
	*(NJS_TEX*)0x021932CC = { 2, 129 };
	*(NJS_TEX*)0x021932D0 = { 123, 129 };
	*(NJS_TEX*)0x021932D4 = { 2, 252 };
	*(NJS_TEX*)0x021932D8 = { 123, 252 };
	*(NJS_TEX*)0x021932DC = { 2, 129 };
	*(NJS_TEX*)0x021932E0 = { 123, 129 };
	*(NJS_TEX*)0x021932E4 = { 2, 252 };
	*(NJS_TEX*)0x021932E8 = { 123, 252 };
	*(NJS_TEX*)0x021932EC = { 252, 2 };
	*(NJS_TEX*)0x021932F0 = { 0, 2 };
	*(NJS_TEX*)0x021932F4 = { 252, 22 };
	*(NJS_TEX*)0x021932F8 = { 0, 22 };
	*(NJS_TEX*)0x021932FC = { 0, 22 };
	*(NJS_TEX*)0x02193300 = { 252, 22 };
	*(NJS_TEX*)0x02193304 = { 0, 2 };
	*(NJS_TEX*)0x02193308 = { 252, 2 };
	*(NJS_TEX*)0x0219330C = { 95, 24 };
	*(NJS_TEX*)0x02193310 = { 95, 2 };
	*(NJS_TEX*)0x02193314 = { 48, 24 };
	*(NJS_TEX*)0x02193318 = { 48, 2 };
	*(NJS_TEX*)0x0219331C = { 0, 24 };
	*(NJS_TEX*)0x02193320 = { 0, 2 };
	*(NJS_TEX*)0x02193324 = { 95, 24 };
	*(NJS_TEX*)0x02193328 = { 151, 24 };
	*(NJS_TEX*)0x0219332C = { 151, 127 };
	*(NJS_TEX*)0x02193330 = { 198, 24 };
	*(NJS_TEX*)0x02193334 = { 198, 127 };
	*(NJS_TEX*)0x02193338 = { 252, 24 };
	*(NJS_TEX*)0x0219333C = { 252, 127 };
	*(NJS_TEX*)0x02193340 = { 95, 24 };
	*(NJS_TEX*)0x02193344 = { 95, 2 };
	*(NJS_TEX*)0x02193348 = { 48, 24 };
	*(NJS_TEX*)0x0219334C = { 48, 2 };
	*(NJS_TEX*)0x02193350 = { 0, 24 };
	*(NJS_TEX*)0x02193354 = { 0, 2 };
	*(NJS_TEX*)0x02193358 = { 0, 24 };
	*(NJS_TEX*)0x0219335C = { 0, 127 };
	*(NJS_TEX*)0x02193360 = { 48, 24 };
	*(NJS_TEX*)0x02193364 = { 48, 127 };
	*(NJS_TEX*)0x02193368 = { 95, 24 };
	*(NJS_TEX*)0x0219336C = { 95, 127 };
	*(NJS_TEX*)0x02193370 = { 151, 127 };
	*(NJS_TEX*)0x02193374 = { 0, 24 };
	*(NJS_TEX*)0x02193378 = { 0, 127 };
	*(NJS_TEX*)0x0219337C = { 48, 24 };
	*(NJS_TEX*)0x02193380 = { 48, 127 };
	*(NJS_TEX*)0x02193384 = { 95, 24 };
	*(NJS_TEX*)0x02193388 = { 95, 127 };
	*(NJS_TEX*)0x0219338C = { 151, 127 };

	// dxpc\stg06_skydeck\common\models\sky_bg_sora.nja.sa1mdl
	((NJS_MATERIAL*)0x0214BF58)->diffuse.color = 0xFFFFFFFF;
	((NJS_MATERIAL*)0x0214BF6C)->diffuse.color = 0xFFFFFFFF;
	((NJS_MATERIAL*)0x0214BF80)->diffuse.color = 0xFFFFFFFF;
	((NJS_MATERIAL*)0x0214BF94)->diffuse.color = 0xFFFFFFFF;

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_kr.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_marukado_c.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_maruyoko.nja.sa1mdl
	((NJS_MATERIAL*)0x021DAC20)->attr_texId = 0;
	((NJS_MATERIAL*)0x021DAC20)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x021DAC20)->exponent = 11;
	((NJS_MATERIAL*)0x021DAC20)->specular.color = 0xFFFFFFFF;
	*(NJS_VECTOR*)0x021DAC90 = { -21, -30, -137 };
	*(NJS_VECTOR*)0x021DAC9C = { -25.5f, -49, -7 };
	*(NJS_VECTOR*)0x021DACA8 = { -21, 0, -137 };
	*(NJS_VECTOR*)0x021DACB4 = { -25.5f, 0, 0 };
	*(NJS_VECTOR*)0x021DACC0 = { 21, -30, -137 };
	*(NJS_VECTOR*)0x021DACCC = { 25.5f, -49, -7 };
	*(NJS_VECTOR*)0x021DACD8 = { 21, 0, -137 };
	*(NJS_VECTOR*)0x021DACE4 = { 25.5f, 0, 0 };
	*(NJS_VECTOR*)0x021DACF0 = { -17.003653f, 0, -63.000004f };
	*(NJS_VECTOR*)0x021DACFC = { -17.003653f, 20.000008f, -76.5f };
	*(NJS_VECTOR*)0x021DAD08 = { -17.003654f, 20.000008f, -137 };
	*(NJS_VECTOR*)0x021DAD14 = { -17.003654f, 0, -137 };
	*(NJS_VECTOR*)0x021DAD20 = { 16.99634f, 0, -63.000004f };
	*(NJS_VECTOR*)0x021DAD2C = { 16.99634f, 20.000008f, -76.5f };
	*(NJS_VECTOR*)0x021DAD38 = { 16.996338f, 20.000008f, -137 };
	*(NJS_VECTOR*)0x021DAD44 = { 16.996338f, 0, -137 };

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_tlp.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_tunagi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_turokado.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_turo_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_cl_obj_turo_b.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_duct.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_mark_a.nja.sa1mdl
	*(NJS_TEX*)0x021FEC64 = { 510, 255 };
	*(NJS_TEX*)0x021FEC68 = { 510, 0 };

	// dxpc\stg06_skydeck\common\models\sky_mark_b.nja.sa1mdl
	*(NJS_TEX*)0x021FED70 = { 510, 255 };
	*(NJS_TEX*)0x021FED74 = { 510, 0 };

	// dxpc\stg06_skydeck\common\models\sky_mark_c.nja.sa1mdl
	*(NJS_TEX*)0x021FEE7C = { 255, 255 };
	*(NJS_TEX*)0x021FEE80 = { 255, 0 };
	*(NJS_TEX*)0x021FEE84 = { 0, 255 };
	*(NJS_TEX*)0x021FEE88 = { 0, 0 };

	// dxpc\stg06_skydeck\common\models\sky_m_houdai.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex35d.nja.sa1mdl
	*(NJS_TEX*)0x02226EB0 = { 2569, -5225 };
	*(NJS_TEX*)0x02226EB4 = { 19, -5459 };
	*(NJS_TEX*)0x02226EB8 = { 1116, -4429 };
	*(NJS_TEX*)0x02226EBC = { 19, -4431 };
	*(NJS_TEX*)0x02226EC0 = { 844, 238 };
	*(NJS_TEX*)0x02226EC4 = { 28, 238 };
	*(NJS_TEX*)0x02226EC8 = { 844, 238 };
	*(NJS_TEX*)0x02226ECC = { 3676, 236 };
	*(NJS_TEX*)0x02226ED0 = { 1116, -4429 };
	*(NJS_TEX*)0x02226ED4 = { 3816, -4424 };
	*(NJS_TEX*)0x02226ED8 = { 2569, -5225 };
	*(NJS_TEX*)0x02226EDC = { 3555, -4985 };
	*(NJS_TEX*)0x02226EE0 = { 914, 253 };
	*(NJS_TEX*)0x02226EE4 = { 34, 253 };
	*(NJS_TEX*)0x02226EE8 = { 804, -4415 };
	*(NJS_TEX*)0x02226EEC = { 20, -4418 };
	*(NJS_TEX*)0x02226EF0 = { 2296, -5125 };
	*(NJS_TEX*)0x02226EF4 = { 20, -5445 };
	*(NJS_TEX*)0x02226EF8 = { 2296, -5125 };
	*(NJS_TEX*)0x02226EFC = { 3801, -4625 };
	*(NJS_TEX*)0x02226F00 = { 804, -4415 };
	*(NJS_TEX*)0x02226F04 = { 3805, -4306 };
	*(NJS_TEX*)0x02226F08 = { 914, 253 };
	*(NJS_TEX*)0x02226F0C = { 3793, 251 };
	*(NJS_TEX*)0x022268F0 = { 2716, 1963 };
	*(NJS_TEX*)0x022268F4 = { 2730, 2010 };
	*(NJS_TEX*)0x022268F8 = { 1595, 2505 };
	*(NJS_TEX*)0x022268FC = { 1293, 2805 };
	*(NJS_TEX*)0x02226900 = { 1415, 2020 };
	*(NJS_TEX*)0x02226904 = { 240, 2109 };
	*(NJS_TEX*)0x02226908 = { 1449, 577 };
	*(NJS_TEX*)0x0222690C = { 506, 561 };
	*(NJS_TEX*)0x02226910 = { 1470, 317 };
	*(NJS_TEX*)0x02226914 = { 527, 214 };
	*(NJS_TEX*)0x02226918 = { 2703, 297 };
	*(NJS_TEX*)0x0222691C = { 2703, 0 };
	*(NJS_TEX*)0x02226920 = { 1415, 2020 };
	*(NJS_TEX*)0x02226924 = { 1595, 2505 };
	*(NJS_TEX*)0x02226928 = { 2716, 1963 };
	*(NJS_TEX*)0x0222692C = { 1470, 317 };
	*(NJS_TEX*)0x02226930 = { 2703, 297 };
	*(NJS_TEX*)0x02226934 = { 1449, 577 };
	*(NJS_TEX*)0x02226938 = { 2703, 588 };
	*(NJS_TEX*)0x0222693C = { 1415, 2020 };
	*(NJS_TEX*)0x02226940 = { 2714, 1869 };
	*(NJS_TEX*)0x02226944 = { 2716, 1963 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex36d.nja.sa1mdl
	*(NJS_TEX*)0x02227D60 = { 9, 15 };
	*(NJS_TEX*)0x02227D64 = { 9, 245 };
	*(NJS_TEX*)0x02227D68 = { 545, 44 };
	*(NJS_TEX*)0x02227D6C = { 1211, 232 };
	*(NJS_TEX*)0x02227D70 = { 917, 71 };
	*(NJS_TEX*)0x02227D74 = { 1159, 129 };
	*(NJS_TEX*)0x02227D78 = { 9, 11 };
	*(NJS_TEX*)0x02227D7C = { 8, 245 };
	*(NJS_TEX*)0x02227D80 = { 832, 61 };
	*(NJS_TEX*)0x02227D84 = { 1227, 232 };
	*(NJS_TEX*)0x02227D88 = { 933, 71 };
	*(NJS_TEX*)0x02227D8C = { 1177, 129 };
	*(NJS_TEX*)0x02227D90 = { 9, 7 };
	*(NJS_TEX*)0x02227D94 = { 773, 55 };
	*(NJS_TEX*)0x02227D98 = { 9, 245 };
	*(NJS_TEX*)0x02227D9C = { 932, 71 };
	*(NJS_TEX*)0x02227DA0 = { 1226, 232 };
	*(NJS_TEX*)0x02227DA4 = { 1177, 129 };
	*(NJS_TEX*)0x02227DA8 = { 1251, 185 };
	*(NJS_TEX*)0x02227DAC = { 1211, 232 };
	*(NJS_TEX*)0x02227DB0 = { 1236, 185 };
	*(NJS_TEX*)0x02227DB4 = { 1159, 129 };
	*(NJS_TEX*)0x02227DB8 = { 0, 255 };
	*(NJS_TEX*)0x02227DC0 = { 2040, 255 };
	*(NJS_TEX*)0x02227DC8 = { 1227, 232 };
	*(NJS_TEX*)0x02227DCC = { 1251, 185 };
	*(NJS_TEX*)0x02227DD0 = { 1177, 129 };
	*(NJS_TEX*)0x02227DD4 = { 0, 255 };
	*(NJS_TEX*)0x02227DDC = { 762, 255 };
	*(NJS_TEX*)0x02227DE4 = { 1050, 255 };
	*(NJS_TEX*)0x02227DF4 = { 765, 151 };
	*(NJS_TEX*)0x02227DF8 = { 0, 151 };
	*(NJS_TEX*)0x02227DFC = { 765, 254 };
	*(NJS_TEX*)0x02227E00 = { 0, 254 };
	*(NJS_TEX*)0x02227E04 = { 271, 255 };
	*(NJS_TEX*)0x02227E08 = { 510, 255 };
	*(NJS_TEX*)0x02227E0C = { 271, 151 };
	*(NJS_TEX*)0x02227E10 = { 510, 151 };
	*(NJS_TEX*)0x02227E24 = { 271, 151 };
	*(NJS_TEX*)0x02227E28 = { 0, 151 };
	*(NJS_TEX*)0x02227E2C = { 271, 255 };
	*(NJS_TEX*)0x02227E30 = { 0, 255 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex37d.nja.sa1mdl
	*(NJS_TEX*)0x02229240 = { 2899, -6503 };
	*(NJS_TEX*)0x02229244 = { 264, -8007 };
	*(NJS_TEX*)0x02229248 = { 2560, -5911 };
	*(NJS_TEX*)0x0222924C = { 37, -6826 };
	*(NJS_TEX*)0x02229250 = { 2716, -4912 };
	*(NJS_TEX*)0x02229254 = { 37, -5133 };
	*(NJS_TEX*)0x02229258 = { 2283, -4828 };
	*(NJS_TEX*)0x0222925C = { 46, -5131 };
	*(NJS_TEX*)0x02229260 = { 2086, -5908 };
	*(NJS_TEX*)0x02229264 = { 62, -6822 };
	*(NJS_TEX*)0x02229268 = { 2474, -6588 };
	*(NJS_TEX*)0x0222926C = { 268, -7991 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex38d.nja.sa1mdl
	*(NJS_TEX*)0x02229AE8 = { 1676, -9513 };
	*(NJS_TEX*)0x02229AEC = { 768, -9257 };
	*(NJS_TEX*)0x02229AF0 = { 1959, -9245 };
	*(NJS_TEX*)0x02229AF4 = { 739, -9165 };
	*(NJS_TEX*)0x02229AF8 = { 2841, -6473 };
	*(NJS_TEX*)0x02229AFC = { 254, -7972 };
	*(NJS_TEX*)0x02229B00 = { 1073, -1020 };
	*(NJS_TEX*)0x02229B04 = { 0, -1020 };
	*(NJS_TEX*)0x02229B08 = { 1030, -763 };
	*(NJS_TEX*)0x02229B0C = { 0, -741 };
	*(NJS_TEX*)0x02229B10 = { 999, -450 };
	*(NJS_TEX*)0x02229B14 = { 0, -428 };
	*(NJS_TEX*)0x02229B18 = { 1030, -120 };
	*(NJS_TEX*)0x02229B1C = { 0, -107 };
	*(NJS_TEX*)0x02229B20 = { 1076, 255 };
	*(NJS_TEX*)0x02229B24 = { 0, 255 };
	*(NJS_TEX*)0x02229B28 = { 1076, 255 };
	*(NJS_TEX*)0x02229B2C = { 3315, 254 };
	*(NJS_TEX*)0x02229B30 = { 1030, -120 };
	*(NJS_TEX*)0x02229B34 = { 3315, -161 };
	*(NJS_TEX*)0x02229B38 = { 999, -450 };
	*(NJS_TEX*)0x02229B3C = { 3315, -478 };
	*(NJS_TEX*)0x02229B40 = { 1030, -763 };
	*(NJS_TEX*)0x02229B44 = { 3315, -758 };
	*(NJS_TEX*)0x02229B48 = { 1073, -1020 };
	*(NJS_TEX*)0x02229B4C = { 3315, -1020 };
	*(NJS_TEX*)0x02229B50 = { 2841, -6473 };
	*(NJS_TEX*)0x02229B54 = { 3623, -6722 };
	*(NJS_TEX*)0x02229B58 = { 1959, -9245 };
	*(NJS_TEX*)0x02229B5C = { 3631, -9366 };
	*(NJS_TEX*)0x02229B60 = { 1676, -9513 };
	*(NJS_TEX*)0x02229B64 = { 3659, -9901 };
	*(NJS_TEX*)0x02229BF0 = { 2456, -6592 };
	*(NJS_TEX*)0x02229BF4 = { 234, -7996 };
	*(NJS_TEX*)0x02229BF8 = { 1601, -9285 };
	*(NJS_TEX*)0x02229BFC = { 735, -9205 };
	*(NJS_TEX*)0x02229C00 = { 1698, -9510 };
	*(NJS_TEX*)0x02229C04 = { 767, -9275 };
	*(NJS_TEX*)0x02229C08 = { 1698, -9510 };
	*(NJS_TEX*)0x02229C0C = { 3729, -9855 };
	*(NJS_TEX*)0x02229C10 = { 1601, -9285 };
	*(NJS_TEX*)0x02229C14 = { 3721, -9407 };
	*(NJS_TEX*)0x02229C18 = { 2456, -6592 };
	*(NJS_TEX*)0x02229C1C = { 3721, -6964 };
	*(NJS_TEX*)0x02229740 = { 2220, -1631 };
	*(NJS_TEX*)0x02229744 = { 2220, -1819 };
	*(NJS_TEX*)0x02229748 = { 919, -1459 };
	*(NJS_TEX*)0x0222974C = { -69, -1686 };
	*(NJS_TEX*)0x02229750 = { 922, -1259 };
	*(NJS_TEX*)0x02229754 = { -91, -1356 };
	*(NJS_TEX*)0x02229758 = { 952, -465 };
	*(NJS_TEX*)0x0222975C = { 77, -154 };
	*(NJS_TEX*)0x02229760 = { 1058, 9 };
	*(NJS_TEX*)0x02229764 = { 917, 237 };
	*(NJS_TEX*)0x02229768 = { 2225, -717 };
	*(NJS_TEX*)0x0222976C = { 2171, -650 };
	*(NJS_TEX*)0x02229770 = { 2224, -822 };
	*(NJS_TEX*)0x02229774 = { 922, -1259 };
	*(NJS_TEX*)0x02229778 = { 2220, -1354 };
	*(NJS_TEX*)0x0222977C = { 919, -1459 };
	*(NJS_TEX*)0x02229780 = { 2220, -1631 };
	*(NJS_TEX*)0x02229784 = { 2225, -717 };
	*(NJS_TEX*)0x02229788 = { 1058, 9 };
	*(NJS_TEX*)0x0222978C = { 2224, -822 };
	*(NJS_TEX*)0x02229790 = { 952, -465 };
	*(NJS_TEX*)0x02229794 = { 922, -1259 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex40d.nja.sa1mdl
	*(NJS_TEX*)0x0222A714 = { 1050, 255 };
	*(NJS_TEX*)0x0222A71C = { 2040, 255 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex41d.nja.sa1mdl
	*(NJS_TEX*)0x0222AE40 = { 3825, 151 };
	*(NJS_TEX*)0x0222AE44 = { 3824, 255 };
	*(NJS_TEX*)0x0222AE48 = { 3215, 151 };
	*(NJS_TEX*)0x0222AE4C = { 3212, 255 };
	*(NJS_TEX*)0x0222AE50 = { 1702, 138 };
	*(NJS_TEX*)0x0222AE54 = { 1690, 255 };
	*(NJS_TEX*)0x0222AE58 = { 0, 110 };
	*(NJS_TEX*)0x0222AE5C = { 0, 255 };
	*(NJS_TEX*)0x0222AE60 = { 0, 110 };
	*(NJS_TEX*)0x0222AE68 = { 1702, 138 };
	*(NJS_TEX*)0x0222AE70 = { 3215, 151 };
	*(NJS_TEX*)0x0222AE78 = { 3825, 151 };
	*(NJS_TEX*)0x0222AE80 = { 6, 4 };
	*(NJS_TEX*)0x0222AE84 = { 507, 36 };
	*(NJS_TEX*)0x0222AE88 = { 6, 247 };
	*(NJS_TEX*)0x0222AE8C = { 761, 53 };
	*(NJS_TEX*)0x0222AE90 = { 1054, 237 };
	*(NJS_TEX*)0x0222AE94 = { 1004, 119 };
	*(NJS_TEX*)0x0222AE98 = { 1078, 183 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex49c.nja.sa1mdl
	*(NJS_TEX*)0x0222C9F0 = { 0, 255 };
	*(NJS_TEX*)0x0222C9F8 = { 255, 255 };
	*(NJS_TEX*)0x0222CA00 = { 0, 255 };
	*(NJS_TEX*)0x0222CA08 = { 255, 255 };
	*(NJS_TEX*)0x0222CA10 = { 0, 255 };
	*(NJS_TEX*)0x0222CA18 = { 255, 255 };
	*(NJS_TEX*)0x0222CA20 = { 0, 255 };
	*(NJS_TEX*)0x0222CA28 = { 255, 255 };
	*(NJS_TEX*)0x0222CA30 = { 0, 255 };
	*(NJS_TEX*)0x0222CA38 = { 255, 255 };
	*(NJS_TEX*)0x0222CA40 = { 0, 255 };
	*(NJS_TEX*)0x0222CA48 = { 255, 255 };

	// dxpc\stg06_skydeck\common\models\sky_nc_h_hanex50c.nja.sa1mdl
	*(NJS_TEX*)0x0222DD58 = { 0, 255 };
	*(NJS_TEX*)0x0222DD60 = { 255, 255 };
	*(NJS_TEX*)0x0222DD68 = { 0, 255 };
	*(NJS_TEX*)0x0222DD70 = { 255, 255 };
	*(NJS_TEX*)0x0222DD78 = { 0, 255 };
	*(NJS_TEX*)0x0222DD80 = { 255, 255 };
	*(NJS_TEX*)0x0222DD88 = { 0, 255 };
	*(NJS_TEX*)0x0222DD90 = { 255, 255 };
	*(NJS_TEX*)0x0222DD98 = { 0, 255 };
	*(NJS_TEX*)0x0222DDA0 = { 255, 255 };
	*(NJS_TEX*)0x0222DDA8 = { 0, 255 };
	*(NJS_TEX*)0x0222DDB0 = { 255, 255 };

	// dxpc\stg06_skydeck\common\models\sky_obj_45asi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_6jiasi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_9jiasi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_erea.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_erec.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_ered.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_etc.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_hasira_l.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_hasira_s.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_h_ub01a.nja.sa1mdl
	*(NJS_TEX*)0x021BD2D0 = { 253, 1 };
	*(NJS_TEX*)0x021BD2D4 = { 253, 255 };
	*(NJS_TEX*)0x021BD2D8 = { 0, 1 };
	*(NJS_TEX*)0x021BD2DC = { 0, 255 };
	*(NJS_TEX*)0x021BD2E0 = { 253, 1 };
	*(NJS_TEX*)0x021BD2E4 = { 253, 255 };
	*(NJS_TEX*)0x021BD2E8 = { 0, 1 };
	*(NJS_TEX*)0x021BD2EC = { 0, 255 };
	*(NJS_TEX*)0x021BD2F0 = { 253, 1 };
	*(NJS_TEX*)0x021BD2F4 = { 253, 255 };
	*(NJS_TEX*)0x021BD2F8 = { 0, 1 };
	*(NJS_TEX*)0x021BD2FC = { 0, 255 };
	*(NJS_TEX*)0x021BD300 = { 253, 1 };
	*(NJS_TEX*)0x021BD304 = { 253, 255 };
	*(NJS_TEX*)0x021BD308 = { 0, 1 };
	*(NJS_TEX*)0x021BD30C = { 0, 255 };
	*(NJS_TEX*)0x021BD310 = { 253, 1 };
	*(NJS_TEX*)0x021BD314 = { 253, 255 };
	*(NJS_TEX*)0x021BD318 = { 0, 1 };
	*(NJS_TEX*)0x021BD31C = { 0, 255 };
	*(NJS_TEX*)0x021BD320 = { 253, 1 };
	*(NJS_TEX*)0x021BD324 = { 253, 255 };
	*(NJS_TEX*)0x021BD328 = { 0, 1 };
	*(NJS_TEX*)0x021BD32C = { 0, 255 };

	// dxpc\stg06_skydeck\common\models\sky_obj_h_ub02a.nja.sa1mdl
	*(NJS_TEX*)0x021BE5E0 = { 253, 1 };
	*(NJS_TEX*)0x021BE5E4 = { 253, 255 };
	*(NJS_TEX*)0x021BE5E8 = { 0, 1 };
	*(NJS_TEX*)0x021BE5EC = { 0, 255 };
	*(NJS_TEX*)0x021BE5F0 = { 253, 1 };
	*(NJS_TEX*)0x021BE5F4 = { 253, 255 };
	*(NJS_TEX*)0x021BE5F8 = { 0, 1 };
	*(NJS_TEX*)0x021BE5FC = { 0, 255 };
	*(NJS_TEX*)0x021BE600 = { 253, 1 };
	*(NJS_TEX*)0x021BE604 = { 253, 255 };
	*(NJS_TEX*)0x021BE608 = { 0, 1 };
	*(NJS_TEX*)0x021BE60C = { 0, 255 };
	*(NJS_TEX*)0x021BE610 = { 253, 1 };
	*(NJS_TEX*)0x021BE614 = { 253, 255 };
	*(NJS_TEX*)0x021BE618 = { 0, 1 };
	*(NJS_TEX*)0x021BE61C = { 0, 255 };
	*(NJS_TEX*)0x021BE620 = { 253, 1 };
	*(NJS_TEX*)0x021BE624 = { 253, 255 };
	*(NJS_TEX*)0x021BE628 = { 0, 1 };
	*(NJS_TEX*)0x021BE62C = { 0, 255 };
	*(NJS_TEX*)0x021BE630 = { 253, 1 };
	*(NJS_TEX*)0x021BE634 = { 253, 255 };
	*(NJS_TEX*)0x021BE638 = { 0, 1 };
	*(NJS_TEX*)0x021BE63C = { 0, 255 };

	// dxpc\stg06_skydeck\common\models\sky_obj_kazarikon.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_kontena.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_maru2asi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_maruasi_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_marukado_c.nja.sa1mdl
	*(NJS_TEX*)0x02171994 = { 2, 254 };
	*(NJS_TEX*)0x0217199C = { 762, 254 };
	*(NJS_TEX*)0x021719A0 = { 762, 254 };
	*(NJS_TEX*)0x021719A8 = { 2, 254 };
	*(NJS_TEX*)0x021719B4 = { 2, 254 };
	*(NJS_TEX*)0x021719BC = { 762, 254 };
	*(NJS_TEX*)0x021719C0 = { 762, 254 };
	*(NJS_TEX*)0x021719C8 = { 2, 254 };
	*(NJS_TEX*)0x021719D0 = { 0, 254 };
	*(NJS_TEX*)0x021719D4 = { 254, 254 };
	*(NJS_TEX*)0x021719E4 = { 0, 254 };
	*(NJS_TEX*)0x021719EC = { 254, 254 };
	*(NJS_TEX*)0x021719F0 = { 254, 254 };
	*(NJS_TEX*)0x021719F8 = { 0, 254 };
	*(NJS_TEX*)0x02171A00 = { 0, 254 };
	*(NJS_TEX*)0x02171A04 = { 254, 254 };
	*(NJS_TEX*)0x02171A54 = { 510, -255 };
	*(NJS_TEX*)0x02171A5C = { 510, 255 };
	*(NJS_TEX*)0x02171A64 = { 510, -255 };
	*(NJS_TEX*)0x02171A6C = { 510, 255 };
	*(NJS_TEX*)0x02171A70 = { 3, 253 };
	*(NJS_TEX*)0x02171A74 = { 506, 253 };
	*(NJS_TEX*)0x02171A78 = { 506, 1 };
	*(NJS_TEX*)0x02171A7C = { 506, 253 };
	*(NJS_TEX*)0x02171A80 = { 506, 1 };
	*(NJS_TEX*)0x02171A84 = { 3, 253 };
	*(NJS_TEX*)0x02171A88 = { 3, 253 };
	*(NJS_TEX*)0x02171A8C = { 506, 253 };
	*(NJS_TEX*)0x02171A90 = { 506, 1 };
	*(NJS_TEX*)0x02171A94 = { 506, 253 };
	*(NJS_TEX*)0x02171A98 = { 506, 1 };
	*(NJS_TEX*)0x02171A9C = { 3, 253 };
	*(NJS_TEX*)0x02171720 = { 509, -5100 };
	*(NJS_TEX*)0x02171728 = { 509, 255 };
	*(NJS_TEX*)0x02171730 = { 510, 255 };
	*(NJS_TEX*)0x02171738 = { 510, 0 };

	// dxpc\stg06_skydeck\common\models\sky_obj_marumoto.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_maruyoko.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_maru_c.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_radar.nja.sa1mdl
	*(NJS_TEX*)0x021BB180 = { 277, -255 };
	*(NJS_TEX*)0x021BB184 = { 510, -255 };
	*(NJS_TEX*)0x021BB188 = { 251, 255 };
	*(NJS_TEX*)0x021BB18C = { 510, 255 };
	*(NJS_TEX*)0x021BB190 = { 510, 255 };
	*(NJS_TEX*)0x021BB194 = { 251, 255 };
	*(NJS_TEX*)0x021BB198 = { 510, -255 };
	*(NJS_TEX*)0x021BB19C = { 277, -255 };
	*(NJS_TEX*)0x021BB1A0 = { 510, -255 };
	*(NJS_TEX*)0x021BB1A4 = { 510, 255 };
	*(NJS_TEX*)0x021BB1A8 = { 277, -255 };
	*(NJS_TEX*)0x021BB1AC = { 251, 255 };
	*(NJS_TEX*)0x021BB1B0 = { 251, 255 };
	*(NJS_TEX*)0x021BB1B8 = { 277, -255 };
	*(NJS_TEX*)0x021BB1BC = { 0, -255 };
	*(NJS_TEX*)0x021BB1C0 = { 510, -255 };
	*(NJS_TEX*)0x021BB1C4 = { 510, 255 };
	*(NJS_TEX*)0x021BB1C8 = { 277, -255 };
	*(NJS_TEX*)0x021BB1CC = { 251, 255 };
	*(NJS_TEX*)0x021BB1D0 = { 510, 255 };
	*(NJS_TEX*)0x021BB1D4 = { 251, 255 };
	*(NJS_TEX*)0x021BB1D8 = { 510, -255 };
	*(NJS_TEX*)0x021BB1DC = { 277, -255 };
	*(NJS_TEX*)0x021BB1E0 = { 510, 255 };
	*(NJS_TEX*)0x021BB1E4 = { 510, -255 };
	*(NJS_TEX*)0x021BB1E8 = { 251, 255 };
	*(NJS_TEX*)0x021BB1EC = { 277, -255 };
	*(NJS_TEX*)0x021BB1F4 = { 0, -255 };
	*(NJS_TEX*)0x021BB1F8 = { 510, -255 };
	*(NJS_TEX*)0x021BB1FC = { 510, 255 };
	*(NJS_TEX*)0x021BB200 = { 277, -255 };
	*(NJS_TEX*)0x021BB204 = { 251, 255 };
	*(NJS_TEX*)0x021BB208 = { 510, 255 };
	*(NJS_TEX*)0x021BB20C = { 251, 255 };
	*(NJS_TEX*)0x021BB210 = { 510, -255 };
	*(NJS_TEX*)0x021BB214 = { 277, -255 };

	// dxpc\stg06_skydeck\common\models\sky_obj_robo_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_tlp.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_tunagi.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_turokado.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_turo_a.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_turo_b.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_obj_t_la.nja.sa1mdl
	*(NJS_TEX*)0x021A2D44 = { 509, 209 };
	*(NJS_TEX*)0x021A2D48 = { 509, 32 };
	*(NJS_TEX*)0x021A2D4C = { 509, 32 };
	*(NJS_TEX*)0x021A2D50 = { 509, 209 };

	// dxpc\stg06_skydeck\common\models\sky_sasae.nja.sa1mdl
	*(NJS_TEX*)0x0217C978 = { 3, 183 };
	*(NJS_TEX*)0x0217C97C = { 3, 247 };
	*(NJS_TEX*)0x0217C980 = { 147, 183 };
	*(NJS_TEX*)0x0217C984 = { 147, 247 };
	*(NJS_TEX*)0x0217C988 = { 227, -757 };
	*(NJS_TEX*)0x0217C98C = { 378, -454 };
	*(NJS_TEX*)0x0217C990 = { 3, -454 };
	*(NJS_TEX*)0x0217C994 = { 147, -294 };
	*(NJS_TEX*)0x0217C998 = { 3, -294 };
	*(NJS_TEX*)0x0217C99C = { 147, 247 };
	*(NJS_TEX*)0x0217C9A0 = { 3, 247 };
	*(NJS_TEX*)0x0217C9A4 = { 3, -581 };
	*(NJS_TEX*)0x0217C9A8 = { 227, -581 };
	*(NJS_TEX*)0x0217C9AC = { 3, -757 };
	*(NJS_TEX*)0x0217C9B0 = { 227, -757 };
	*(NJS_TEX*)0x0217C9B4 = { 3, -454 };
	*(NJS_TEX*)0x0217C9B8 = { 3, 247 };
	*(NJS_TEX*)0x0217C9BC = { 147, 247 };
	*(NJS_TEX*)0x0217C9C0 = { 3, -294 };
	*(NJS_TEX*)0x0217C9C4 = { 147, -294 };
	*(NJS_TEX*)0x0217C9C8 = { 163, -454 };
	*(NJS_TEX*)0x0217C9CC = { 330, -454 };
	*(NJS_TEX*)0x0217C9D0 = { 163, -294 };
	*(NJS_TEX*)0x0217C9D4 = { 330, -294 };
	*(NJS_TEX*)0x0217C9D8 = { 3, -294 };
	*(NJS_TEX*)0x0217C9DC = { 147, -294 };
	*(NJS_TEX*)0x0217C9E0 = { 3, -454 };
	*(NJS_TEX*)0x0217C9E4 = { 378, -454 };
	*(NJS_TEX*)0x0217C9E8 = { 3, -757 };
	*(NJS_TEX*)0x0217C9EC = { 227, -757 };
	*(NJS_TEX*)0x0217C9F0 = { 163, 247 };
	*(NJS_TEX*)0x0217C9F4 = { 330, 247 };
	*(NJS_TEX*)0x0217C9F8 = { 163, -294 };
	*(NJS_TEX*)0x0217C9FC = { 330, -294 };
	*(NJS_TEX*)0x0217CA00 = { 147, 183 };
	*(NJS_TEX*)0x0217CA04 = { 147, 247 };
	*(NJS_TEX*)0x0217CA08 = { 3, 183 };
	*(NJS_TEX*)0x0217CA0C = { 3, 247 };
	*(NJS_TEX*)0x0217CA10 = { 3, -757 };
	*(NJS_TEX*)0x0217CA14 = { 3, -454 };
	*(NJS_TEX*)0x0217CA18 = { 378, -454 };
	*(NJS_TEX*)0x0217CA1C = { 3, -294 };
	*(NJS_TEX*)0x0217CA20 = { 147, -294 };
	*(NJS_TEX*)0x0217CA24 = { 3, 247 };
	*(NJS_TEX*)0x0217CA28 = { 147, 247 };
	*(NJS_TEX*)0x0217CA2C = { 227, -581 };
	*(NJS_TEX*)0x0217CA30 = { 3, -581 };
	*(NJS_TEX*)0x0217CA34 = { 227, -757 };
	*(NJS_TEX*)0x0217CA38 = { 3, -757 };
	*(NJS_TEX*)0x0217CA3C = { 378, -454 };
	*(NJS_TEX*)0x0217CA40 = { 3, -294 };
	*(NJS_TEX*)0x0217CA44 = { 147, -294 };
	*(NJS_TEX*)0x0217CA48 = { 3, 247 };
	*(NJS_TEX*)0x0217CA4C = { 147, 247 };
	*(NJS_TEX*)0x0217CA50 = { 163, -294 };
	*(NJS_TEX*)0x0217CA54 = { 330, -294 };
	*(NJS_TEX*)0x0217CA58 = { 163, -454 };
	*(NJS_TEX*)0x0217CA5C = { 330, -454 };
	*(NJS_TEX*)0x0217CA60 = { 147, -294 };
	*(NJS_TEX*)0x0217CA64 = { 3, -294 };
	*(NJS_TEX*)0x0217CA68 = { 378, -454 };
	*(NJS_TEX*)0x0217CA6C = { 3, -454 };
	*(NJS_TEX*)0x0217CA70 = { 227, -757 };
	*(NJS_TEX*)0x0217CA74 = { 3, -757 };
	*(NJS_TEX*)0x0217CA78 = { 163, -294 };
	*(NJS_TEX*)0x0217CA7C = { 330, -294 };
	*(NJS_TEX*)0x0217CA80 = { 163, 247 };
	*(NJS_TEX*)0x0217CA84 = { 330, 247 };

	// dxpc\stg06_skydeck\common\models\sky_scope_a.nja.sa1mdl
	*(NJS_TEX*)0x0214E400 = { 508, 249 };
	*(NJS_TEX*)0x0214E404 = { 5, 249 };
	*(NJS_TEX*)0x0214E408 = { 508, -253 };
	*(NJS_TEX*)0x0214E40C = { 5, -253 };

	// dxpc\stg06_skydeck\common\models\sky_stop.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_s_levar.nja.sa1mdl
	*(NJS_TEX*)0x021AF110 = { 127, 255 };
	*(NJS_TEX*)0x021AF114 = { 0, 63 };
	*(NJS_TEX*)0x021AF118 = { 382, 255 };
	*(NJS_TEX*)0x021AF11C = { 382, 255 };
	*(NJS_TEX*)0x021AF120 = { 510, 63 };
	*(NJS_TEX*)0x021AF124 = { 0, 63 };
	*(NJS_TEX*)0x021AF128 = { 510, -510 };
	*(NJS_TEX*)0x021AF12C = { 0, -510 };
	*(NJS_TEX*)0x021AF1E8 = { 1, 127 };
	*(NJS_TEX*)0x021AF1EC = { 1, 2 };
	*(NJS_TEX*)0x021AF1F0 = { 508, 127 };
	*(NJS_TEX*)0x021AF1F4 = { 508, 2 };
	*(NJS_TEX*)0x021AF1F8 = { 1, 127 };
	*(NJS_TEX*)0x021AF1FC = { 1, 2 };
	*(NJS_TEX*)0x021AF200 = { 508, 127 };
	*(NJS_TEX*)0x021AF204 = { 508, 2 };
	*(NJS_TEX*)0x021AF27C = { 510, 255 };
	*(NJS_TEX*)0x021AF284 = { 510, 3 };
	*(NJS_TEX*)0x021AF288 = { 322, -168 };
	*(NJS_TEX*)0x021AF28C = { 187, -168 };
	*(NJS_TEX*)0x021AF290 = { 322, -224 };
	*(NJS_TEX*)0x021AF294 = { 187, -224 };
	*(NJS_TEX*)0x021AF298 = { 86, -67 };
	*(NJS_TEX*)0x021AF29C = { 86, 67 };
	*(NJS_TEX*)0x021AF2A0 = { 30, -67 };
	*(NJS_TEX*)0x021AF2A4 = { 30, 67 };
	*(NJS_TEX*)0x021AF2A8 = { 479, 224 };
	*(NJS_TEX*)0x021AF2AC = { 322, 224 };
	*(NJS_TEX*)0x021AF2B0 = { 479, 67 };
	*(NJS_TEX*)0x021AF2B4 = { 322, 67 };
	*(NJS_TEX*)0x021AF2B8 = { 30, 224 };
	*(NJS_TEX*)0x021AF2BC = { 30, 67 };
	*(NJS_TEX*)0x021AF2C0 = { 187, 224 };
	*(NJS_TEX*)0x021AF2C4 = { 187, 67 };
	*(NJS_TEX*)0x021AF2C8 = { 30, -224 };
	*(NJS_TEX*)0x021AF2CC = { 187, -224 };
	*(NJS_TEX*)0x021AF2D0 = { 30, -67 };
	*(NJS_TEX*)0x021AF2D4 = { 187, -67 };
	*(NJS_TEX*)0x021AF2D8 = { 479, -224 };
	*(NJS_TEX*)0x021AF2DC = { 479, -67 };
	*(NJS_TEX*)0x021AF2E0 = { 322, -224 };
	*(NJS_TEX*)0x021AF2E4 = { 322, -67 };
	*(NJS_TEX*)0x021AF2E8 = { 510, -255 };
	*(NJS_TEX*)0x021AF2EC = { 30, -224 };
	*(NJS_TEX*)0x021AF2F0 = { 0, -255 };
	*(NJS_TEX*)0x021AF2F4 = { 30, 224 };
	*(NJS_TEX*)0x021AF2F8 = { 0, 254 };
	*(NJS_TEX*)0x021AF2FC = { 479, 224 };
	*(NJS_TEX*)0x021AF300 = { 510, 254 };
	*(NJS_TEX*)0x021AF304 = { 479, -224 };
	*(NJS_TEX*)0x021AF308 = { 510, -255 };
	*(NJS_TEX*)0x021AF30C = { 30, -224 };
	*(NJS_TEX*)0x021AF310 = { 187, 168 };
	*(NJS_TEX*)0x021AF314 = { 322, 168 };
	*(NJS_TEX*)0x021AF318 = { 187, 224 };
	*(NJS_TEX*)0x021AF31C = { 322, 224 };
	*(NJS_TEX*)0x021AF320 = { 423, 67 };
	*(NJS_TEX*)0x021AF324 = { 423, -67 };
	*(NJS_TEX*)0x021AF328 = { 479, 67 };
	*(NJS_TEX*)0x021AF32C = { 479, -67 };

	// dxpc\stg06_skydeck\common\models\sky_ud_houdai.nja.sa1mdl
		//Model too different

	// dxpc\stg06_skydeck\common\models\sky_yajirusi.nja.sa1mdl
	*(NJS_TEX*)0x021FDDBC = { 251, -247 };
	*(NJS_TEX*)0x021FDDC0 = { 251, 247 };
	*(NJS_TEX*)0x021FDDC4 = { 3, -247 };
	*(NJS_TEX*)0x021FDDC8 = { 3, 247 };

	// dxpc\stg06_skydeck\common\models\sky_yaji_a.nja.sa1mdl
	*(NJS_TEX*)0x021FE940 = { 255, -255 };
	*(NJS_TEX*)0x021FE948 = { 0, -255 };

	// dxpc\stg06_skydeck\common\models\sky_yaji_b.nja.sa1mdl
	*(NJS_TEX*)0x021FEA4C = { 255, -255 };
	*(NJS_TEX*)0x021FEA54 = { 0, -255 };

	// dxpc\stg06_skydeck\common\models\sky_yaji_c.nja.sa1mdl
	*(NJS_TEX*)0x021FEB58 = { 255, -255 };
	*(NJS_TEX*)0x021FEB60 = { 0, -255 };
}