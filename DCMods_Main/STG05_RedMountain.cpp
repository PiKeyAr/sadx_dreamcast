#include "stdafx.h"
#include "STG05_RedMountain.h"

static bool ModelsLoaded_STG05;

// TODO: Hammer shadow overlapping switch maybe?

static const float KUSA_DISTANCE = 25000.0f; // Distance check for MtKusa animation

NJS_OBJECT* object_kumo_fr_kumo2a_fr_kumo2a_dc = nullptr; // Cloned Red Mountain cloud mesh for a different UV animation

// Renders the top cloud with different depth depending on whether the camera is above or below the clouds
void RenderRMClouds_Top(task *a1, NJS_OBJECT *a2)
{
	float BottomY = GetPlayerNumber() == Characters_Knuckles ? RMCloudKnuckles_BottomY : RMCloudSonic_BottomY;
	late_z_ofs___ = (camera_twp->pos.y <= BottomY) ? -8000.0f : -6000.0f;
	Object_Mountain_CL_sub(a1, object_kumo_fr_kumo2a_fr_kumo2a_dc);
	late_z_ofs___ = 0.0f;
}

// Renders the bottom cloud with different depth depending on whether the camera is above or below the clouds
void RenderRMClouds_Bottom(task *a1, NJS_OBJECT *a2)
{
	float BottomY = GetPlayerNumber() == Characters_Knuckles ? RMCloudKnuckles_BottomY : RMCloudSonic_BottomY;
	late_z_ofs___ = (camera_twp->pos.y <= BottomY) ? -6000.0f : -8000.0f;
	Object_Mountain_CL_sub(a1, &object_kumo_fr_kumo2a_fr_kumo2a);
	late_z_ofs___ = 0.0f;
}

// Forces cloud color to white like on DC without affecting transparency
void SetCloudColor(NJS_ARGB *a1)
{
	SetMaterial(a1->a, 1.0f, 1.0f, 1.0f);
}

// Draws OLight with custom models
void __cdecl Draw_22_r(task* a1)
{
	taskwk* twp; // esi
	Angle z; // eax
	Angle x; // eax
	Angle y; // eax
	Angle rot; // st7
	NJS_OBJECT* object_kaiten_kaiten_sphere1 = object_kaiten_kaiten_orgboxb.child;
	NJS_OBJECT* object_kaiten_kaiten_sphere3 = object_kaiten_kaiten_orgboxb.child->child;
	NJS_OBJECT* object_kaiten_kaiten_kaiten = object_kaiten_kaiten_orgboxb.child->sibling;
	NJS_OBJECT* object_kaiten_kaiten_kaiten_dc = object_kaiten_kaiten_orgboxb.child->sibling->sibling; // Extra model with the glass

	twp = a1->twp;
	if (dsCheckViewV(&twp->pos, 50.0))
	{
		njSetTexture(&texlist_obj_mountain);
		njPushMatrix(0);
		njTranslate(0, twp->pos.x, twp->pos.y, twp->pos.z);
		z = twp->ang.z;
		if (z)
			njRotateZ(0, z);
		x = twp->ang.x;
		if (x)
			njRotateX(0, x);
		y = twp->ang.y;
		if (y)
			njRotateY(0, y);
		// Draw box
		njTranslateV(0, (NJS_VECTOR*)object_kaiten_kaiten_kaiten->pos);
		dsDrawModel(object_kaiten_kaiten_kaiten->basicdxmodel);
		// Draw round frame
		njTranslateV(0, (NJS_VECTOR*)object_kaiten_kaiten_orgboxb.pos);
		dsDrawModel(object_kaiten_kaiten_orgboxb.basicdxmodel);
		// Draw rotating parts
		njPushMatrix(0);
		njTranslate(
			0,
			object_kaiten_kaiten_sphere1->pos[0],
			object_kaiten_kaiten_sphere1->pos[1],
			object_kaiten_kaiten_sphere1->pos[2]);
		if (object_kaiten_kaiten_sphere1->ang[2])
			njRotateZ(0, object_kaiten_kaiten_sphere1->ang[2]);
		if (object_kaiten_kaiten_sphere1->ang[0])
			njRotateX(0, object_kaiten_kaiten_sphere1->ang[0]);
		if (object_kaiten_kaiten_sphere1->ang[1])
			njRotateY(0, object_kaiten_kaiten_sphere1->ang[1]);
		rot = (Angle)(twp->wtimer * 65536.0 * 0.002777777777777778);
		if (rot)
			njRotateY(0, rot);
		ds_DrawModelClip(object_kaiten_kaiten_orgboxb.child->basicdxmodel, 1.0);
		njRotateY(0, 0x8000);
		ds_DrawModelClip(object_kaiten_kaiten_orgboxb.child->child->basicdxmodel, 1.0);
		njPopMatrix(1u);
		njPushMatrix(0);
		njTranslate(
			0,
			object_kaiten_kaiten_orgboxb.pos[0],
			object_kaiten_kaiten_orgboxb.pos[1],
			object_kaiten_kaiten_orgboxb.pos[2]);
		if (object_kaiten_kaiten_orgboxb.ang[2])
			njRotateZ(0, object_kaiten_kaiten_orgboxb.ang[2]);
		if (object_kaiten_kaiten_orgboxb.ang[0])
			njRotateX(0, object_kaiten_kaiten_orgboxb.ang[0]);
		if (object_kaiten_kaiten_orgboxb.ang[1])
			njRotateY(0, object_kaiten_kaiten_orgboxb.ang[1]);
		njPopMatrix(1u);
		// Draw glass
		late_DrawModelClip(object_kaiten_kaiten_kaiten_dc->basicdxmodel, LATE_WZ, 1.0f);
		njPopMatrix(1u);
	}
}

// Hook to make Red Mountain clouds load after the camera by loading the level object in slot 1 instead of 0
void LoadLevelObjectHook(unsigned __int16 im, int level, void(__cdecl* exec)(task*))
{
	if (ModConfig::EnabledLevels[LevelIDs_RedMountain] && ssStageNumber == LevelIDs_RedMountain)
		CreateElementalTask(im, 1, exec);
	else
		CreateElementalTask(im, level, exec);
}

void RedMountain_Init()
{
	ReplaceCAM("CAM0500S");
	ReplaceCAM("CAM0501E");
	ReplaceCAM("CAM0501S");
	ReplaceCAM("CAM0502K");
	ReplaceSET("SET0500S");
	ReplaceSET("SET0501E");
	ReplaceSET("SET0501S");
	ReplaceSET("SET0502K");
	ReplacePVM("MOUNTAIN01");
	ReplacePVM("MOUNTAIN02");
	ReplacePVM("MOUNTAIN03");
	ReplacePVM("MOUNTAIN_CARRIER");
	ReplacePVM("MOUNTAIN_E104");
	ReplacePVM("MOUNTAIN_MEXPLOSION");
	ReplacePVM("MOUNTAIN_STEAM");
	ReplacePVM("OBJ_MOUNTAIN");
	ReplacePVM("YOUGAN_ANIM");
	WriteData<1>((char*)0x600700, 0xC3u); // Disable SetClip_RedMountain
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg05[0][i].Col = 0xFFFFFFFF;
		pFogTable_Stg05[0][i].f32StartZ = 2000.0f;
		pFogTable_Stg05[0][i].f32EndZ = 16000.0f;
		pFogTable_Stg05[0][i].u8Enable = 1;

		pFogTable_Stg05[1][i].Col = 0xFF000000;
		pFogTable_Stg05[1][i].f32StartZ = 1650.0f;		
		pFogTable_Stg05[1][i].f32EndZ = 4000.0f;
		pFogTable_Stg05[1][i].u8Enable = 1;

		pFogTable_Stg05[2][i].Col = 0xFFFFFFFF;
		pFogTable_Stg05[2][i].f32StartZ = 1000.0f;
		pFogTable_Stg05[2][i].f32EndZ = 12000.0f;
		pFogTable_Stg05[2][i].u8Enable = 1;

		pClipMap_Stg05[0][i].f32Far = -16000.0f;
		pClipMap_Stg05[2][i].f32Far = -16000.0f;
		
		pScale_Stg05[1][i].x = pScale_Stg05[1][i].y = pScale_Stg05[1][i].z = 1.0f;
	}
}

void RedMountain_Load()
{
	LevelLoader(LevelAndActIDs_RedMountain1, "SYSTEM\\data\\stg05_mountain\\landtable0500.c.sa1lvl", &texlist_mountain01);
	LevelLoader(LevelAndActIDs_RedMountain2, "SYSTEM\\data\\stg05_mountain\\landtable0501.c.sa1lvl", &texlist_mountain02);
	LevelLoader(LevelAndActIDs_RedMountain3, "SYSTEM\\data\\stg05_mountain\\landtable0502.c.sa1lvl", &texlist_mountain03);
	if (!ModelsLoaded_STG05)
	{
		// Cloud fixes
		object_kumo_fr_kumo2a_fr_kumo2a_dc = CloneObject(&object_kumo_fr_kumo2a_fr_kumo2a);
		object_kumo_fr_kumo2a_fr_kumo2a_dc->basicdxmodel->mats = object_kumo_fr_kumo2a_fr_kumo2a.basicdxmodel->mats;
		object_kumo_fr_kumo2a_fr_kumo2a_dc->basicdxmodel->meshsets[0].vertuv = ((NJS_TEX*)0x3C80F00); // Red Mountain cloud UVs
		WriteCall((void*)0x00600CA3, SetCloudColor); // Set alpha, but keep colors at 1.0
		WriteCall((void*)0x006011D8, RenderRMClouds_Top);
		WriteCall((void*)0x0060121C, RenderRMClouds_Bottom);
		WriteCall((void*)0x004143F9, LoadLevelObjectHook); // Load level object in slot 1 instead of 0 to fix clouds
		// Objects
		AddWhiteDiffuseMaterial(&object_sotolight_light_light.basicdxmodel->mats[1]); // OLightB
		AddWhiteDiffuseMaterial(&object_sotolight_light_light.basicdxmodel->mats[2]); // OLightB
		AddWhiteDiffuseMaterial(&object_sotolight_light_light.basicdxmodel->mats[3]); // OLightB
		CorrectSpecular(&object_sotolight_light_light);
		AddWhiteDiffuseMaterial(&object_lamp1a_lump1a_lump1a.basicdxmodel->mats[1]); // Lamp1
		AddWhiteDiffuseMaterial(&object_lamp2a_lump2a_lump2a.basicdxmodel->mats[1]); // Lamp2
		CorrectSpecular(&object_lamp1a_lump1a_lump1a);
		CorrectSpecular(&object_lamp2a_lump2a_lump2a);
		CorrectSpecular(&object_saku_saku_saku);
		AddAlphaRejectMaterial(&object_saku_saku_saku.child->basicdxmodel->mats[0]); // Saku2
		AddAlphaRejectMaterial(&object_saku_saku_saku.child->basicdxmodel->mats[1]); // Saku2
		object_stones_stone3e_stone3e.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA; // OErupt small piece (SL OBJECT)
		object_ki01_ki01_ki01 = *LoadModel("system\\data\\stg05_mountain\\common\\models\\ki01_ki01.nja.sa1mdl"); // OTree1
		object_setdai_asiba_asiba = *LoadModel("system\\data\\stg05_mountain\\common\\models\\setdai_asiba.nja.sa1mdl"); // OAshiba1
		object_untei_6pon_6pon_6pon = *LoadModel("system\\data\\stg05_mountain\\common\\models\\untei_6pon_6pon.nja.sa1mdl"); // Monkey bars
		object_hasi_piler1a_piler1a = *LoadModel("system\\data\\stg05_mountain\\common\\models\\hasi_piler1a.nja.sa1mdl"); // O Bpole
		object_hasi_piler2a_piler2a = *LoadModel("system\\data\\stg05_mountain\\common\\models\\hasi_piler2a.nja.sa1mdl"); // O Bpole 2
		LoadModel_ReplaceMeshes(&object_tate_hago_hago, "system\\data\\stg05_mountain\\common\\models\\tate_hago.nja.sa1mdl"); // OHago (sorted model)
		model_ukisima_ukisima1a_ukisima1a = *LoadModel("system\\data\\stg05_mountain\\common\\models\\ukisima_ukisima1a.nja.sa1mdl")->basicdxmodel; // OMAshiba
		model_hasi_hasiita_hasiita = *LoadModel("system\\data\\stg05_mountain\\common\\models\\hasi_hasiita.nja.sa1mdl")->basicdxmodel; // Bridge piece 2
		object_kaiten_kaiten_orgboxb = *LoadModel("system\\data\\stg05_mountain\\common\\models\\kaiten_kaiten.nja.sa1mdl"); // OLight (edited hierarchy)
		WriteJump(Draw_22, Draw_22_r); // OLight
		// From SADXFE
		WriteData<2>((char*)0x0060839D, 0x90u); // Disable MtKusa check to make it work in Act 1 too
		WriteData(reinterpret_cast<const float**>(0x00608331), &KUSA_DISTANCE); // Make the animated MtKusa model show up at larger distances
		ModelsLoaded_STG05 = true;
	}
}

void RedMountain_OnFrame()
{
	if (ssStageNumber != LevelIDs_RedMountain)
		return;
	if (!ChkPause())
	{
		if (ssActNumber == 1 && camera_twp != nullptr)
		{
			if (camera_twp->pos.y > 900)
				gClipMap.f32Far = -9000.0f;
		}
	}
}