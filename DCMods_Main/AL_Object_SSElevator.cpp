#include "stdafx.h"
#include "AL_Object_SSElevator.h"

// Elevator in SS Garden

NJS_OBJECT* SSGardenElevatorObject = nullptr;

void SSGardenElevator(task* tp)
{
    taskwk* twp = tp->twp;
    twp->ang.x = 0;
    twp->ang.z = 0;
    twp->scl.x = 0;
    twp->mode = 0;
    twp->btimer = LevelIDs_StationSquare;
    twp->smode = 4;
    twp->counter.ptr = SSGardenElevatorObject;
    CCL_Init(tp, door_c_info, 5, 4u);
    CreateChildTask(6, BoxInit, tp);
    tp->exec = Exec_27;
    tp->disp = Disp_20;
    tp->dest = ObjectSkydeck_wings_End;
}

void SetLevelEntranceForReturningtoSS(LevelIDs level, unsigned __int8 act)
{
    if (IsChaoGarden)
    {
        SetEntranceNumber(4);
        AL_ExitToStage(LevelIDs_StationSquare, 4);
    }
    else
        ChangeStageWithFadeOut(level, act);
}

Sint32 SetElevatorTexlist()
{
    if (IsChaoGarden)
    {
        njSetTexture(&texlist_garden00_object);
        return 1;
    }
    else
        return SetObjectTexture();
}

void PlayElevatorSFX(int tone, int id, int pri, int volume)
{
    dsPlay_oneshot((IsChaoGarden && ModConfig::DLLLoaded_SoundOverhaul) ? SE_CH_HANABI_SYUU : tone, id, pri, volume);
}

void SSGardenElevator_Load()
{
    WriteCall((void*)0x006390B2, PlayElevatorSFX);
    WriteCall((void*)0x00639038, PlayElevatorSFX);
    WriteCall((void*)0x00638FAA, PlayElevatorSFX);
    WriteCall((void*)0x00638DD7, SetElevatorTexlist);
    WriteCall((void*)0x00639198, SetLevelEntranceForReturningtoSS);
    SSGardenElevatorObject = LoadModel("system\\data\\chao\\stg_garden00_ss\\obj\\sssikake_hotelele.nja.sa1mdl");
}