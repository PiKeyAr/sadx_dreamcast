#include "stdafx.h"
#include "ALR_Nameplate.h"

// Dialog frame with the Chao's name that appears in Chao Race
// TODO: Rewrite this trash...

int ChaoRaceNameCounter = 0;
int ChaoRaceTimer = 0;
bool ChaoRaceEnded = false;

static NJS_TEXANIM ChaoNameplateTexanim = { 64, 16, 32, 8, 0, 0, 0x0FF, 0x0FF, 1, 0 };
static NJS_SPRITE ChaoNameplateSprite = { { 0, 7.5f, 0.0f }, 0.12f, 0.12f, 0, &texlist_chao_hyouji, &ChaoNameplateTexanim };

static float RedArrowX = 0;
static float RedArrowY = 0;
static float RedArrowZ = 0;

void ChaoRaceRedArrowHook(NJS_SPRITE* _sp, Int n, NJD_SPRITE attr)
{
	RedArrowX = _sp->p.x;
	RedArrowY = _sp->p.y;
	RedArrowZ = _sp->p.z;
	late_z_ofs___ = -1000.0f;
	late_DrawSprite3D(_sp, n, attr, LATE_LIG);
	late_z_ofs___ = 0.0f;
}

void ChaoNameplate_Display(ObjectMaster* tp)
{
	//float LetterSpriteDepth = 32000.0f;
	int ChaoNameCurrentCharacter = 0;
	int ChaoNameNumSpaces = 0;
	ChaoData1* data1 = (ChaoData1*)tp->Data1;
	// Subtract the internal timer
	tp->Data1->CharID = max(0, tp->Data1->CharID--);
	// Display the bubble
	if (((data1->entity.CharIndex == 1 && data1->entity.CharID > 0) || data1->entity.CharIndex == 2) && data1->ChaoDataBase_ptr->Name[0] != 0x00)
	{
		NJS_VECTOR ChatBubblePosition = { tp->Data1->Position.x, tp->Data1->Position.y, tp->Data1->Position.z };
		NJS_SPRITE ChaoNameLetterSprite;
		ChaoNameLetterSprite.tlist = &texlist_al_tex_common;
		ChaoNameLetterSprite.p.x = 0;
		ChaoNameLetterSprite.p.y = ChaoNameplateSprite.p.y - 0.35f;
		ChaoNameLetterSprite.p.z = ChaoNameplateSprite.p.z + 0.05f;
		ChaoNameLetterSprite.sx = 0.04f;
		ChaoNameLetterSprite.sy = 0.04f;
		ChaoNameLetterSprite.tanim = ChaoNameLettersTexanim;
		// Calculate the number of spaces for centering the name
		for (int i = 0; i < 7; i++)
		{
			ChaoNameCurrentCharacter = data1->ChaoDataBase_ptr->Name[i];
			if (ChaoNameCurrentCharacter == 0x5F || ChaoNameCurrentCharacter == 0i8) ChaoNameNumSpaces++;
		}
		// Draw the letters
		for (int i = 0; i < 7; i++)
		{
			njPushMatrix(0);
			njTranslateV(0, &ChatBubblePosition);
			ChaoNameLetterSprite.p.x = -3.4f + i * 1.0f + ChaoNameNumSpaces * 0.5f;
			njRotateXYZ(0, camera_twp->ang.x, camera_twp->ang.y, 0);
			ChaoNameCurrentCharacter = data1->ChaoDataBase_ptr->Name[i];
			// Set letter color (blue for CPU, black for player's Chao)
			if (data1->entity.Index == 7) SetMaterial(1.0f, 0.2f, 0.2f, 0.2f); else SetMaterial(1.0f, 0.0f, 0.0f, 0.9f);
			// Draw space
			if (ChaoNameCurrentCharacter == 0x5F || ChaoNameCurrentCharacter == 0x00) late_DrawSprite3D(&ChaoNameLetterSprite, 254, NJD_SPRITE_ALPHA | NJD_SPRITE_VFLIP, LATE_WZ);
			// Draw all letters before space (because SADX is retarded)
			else if (ChaoNameCurrentCharacter < 0)
				late_DrawSprite3D(&ChaoNameLetterSprite, 254 + ChaoNameCurrentCharacter, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_VFLIP, LATE_WZ);
			else
				late_DrawSprite3D(&ChaoNameLetterSprite, ChaoNameCurrentCharacter - 1, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_VFLIP, LATE_WZ);
			Sprite3DDepth_Current = 0.0f;
			njPopMatrix(1u);
			ResetMaterial();
		}
		// Draw the chat bubble
		SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		njPushMatrix(0);
		njTranslateV(0, &ChatBubblePosition);
		njRotateXYZ(0, camera_twp->ang.x, camera_twp->ang.y, 0);
		late_DrawSprite3D(&ChaoNameplateSprite, 0, NJD_SPRITE_ALPHA | NJD_SPRITE_VFLIP, LATE_WZ);
		njPopMatrix(1u);
		ResetMaterial();
	}
}

unsigned __int8 ConvertToUppercase(unsigned __int8 character)
{
	// SADX Chao font uses an encoding which swaps upper- and lowercase characters.
	// That makes SA1 rival names, which are uppercase-only, appear lowercase in DX. This function corrects them.
	return (character < 'A' || character > 'Z') ? character : character - 32;
}

void GenerateRaceChaoName(ObjectMaster* a1)
{
	ChaoData1* data1 = (ChaoData1*)a1->Data1;
	// Assign a name if the Chao doesn't have one
	unsigned __int8 character = 0i8;
	if (data1->ChaoDataBase_ptr->Name[0] == 0)
	{
		for (int i = 0; i < 8; i++)
		{
			if (data1->ChaoDataBase_ptr->Type == ChaoType_Child)
			{
				character = Language ? nameChildE[ChaoRaceNameCounter][i] : nameChild[ChaoRaceNameCounter][i];
			}
			else
			{
				switch (data1->ChaoDataBase_ptr->Texture)
				{
				case SkinColor_White:
					character = Language ? nameGodE[ChaoRaceNameCounter][i] : nameGod[ChaoRaceNameCounter][i];
					break;
				case SkinColor_Yellow:
					character = Language ? nameHGodE[ChaoRaceNameCounter][i] : nameHGod[ChaoRaceNameCounter][i];
					break;
				default:
					character = Language ? nameAdultE[ChaoRaceNameCounter][i] : nameAdult[ChaoRaceNameCounter][i];
					break;
				}
			}
			if (Language)
				character = ConvertToUppercase(character);
			data1->ChaoDataBase_ptr->Name[i] = character;
		}
		ChaoRaceNameCounter++;
	}
}

static Trampoline* Chao_Display_t = nullptr;
static void __cdecl Chao_Display_r(ObjectMaster* a1)
{
	const auto original = TARGET_DYNAMIC(Chao_Display);
	ChaoData1* data1 = (ChaoData1*)a1->Data1;
	int ChaoAnimationWhatever = data1->MotionTable[4];
	// All this stuff should only happen during Chao Race
	if (ChaoStageNumber == CHAO_STG_RACE)
	{
		// Generate Chao name if it's not on the first track (which would be the player's Chao)
		if (ChaoRaceTimer < 50)
		{
			if (a1->Data1->Index != 7) GenerateRaceChaoName(a1);
		}
		float ChaoX = data1->entity.Position.x;
		float ChaoY = data1->entity.Position.y;
		float ChaoZ = data1->entity.Position.z;
		float CameraX = camera_twp->pos.x;
		float CameraY = camera_twp->pos.y;
		float CameraZ = camera_twp->pos.z;
		float x_c = CameraX - ChaoX;
		float y_c = CameraY - ChaoY;
		float z_c = CameraZ - ChaoZ;
		float x_a = RedArrowX - ChaoX;
		float y_a = RedArrowY - ChaoY;
		float z_a = RedArrowZ - ChaoZ;
		float dist_cam = sqrt(x_c * x_c + y_c * y_c + z_c * z_c);
		float dist_arrow = sqrt(x_a * x_a + y_a * y_a + z_a * z_a);
		//PrintDebug("Index: %d, Distance cam: %f, Distance arrow: %f, CharIndex: %d, CharID: %d\n", data1->entity.Index, dist_cam, dist_arrow, data1->entity.CharIndex, data1->entity.CharID);
		// Assign the winner a permanent name badge
		if (!ChaoRaceEnded && ChaoAnimationWhatever == -17)
		{
			ChaoRaceEnded = true;
			ChaoRaceTimer = 0;
			data1->entity.CharID = 80;
			data1->entity.CharIndex = 2;
		}
		// Disable the nameplate if too far from the arrow/camera
		if ((dist_arrow >= 8.5f || dist_cam >= 45.0f) && ChaoAnimationWhatever != -17)
		{
			data1->entity.CharIndex = 0;
			data1->entity.CharID = 0;
		}
		// Disable the nameplate if playing the win animation but another Chao reached the end first
		if (data1->entity.CharIndex != 2 && ChaoRaceEnded && ChaoAnimationWhatever == -17)
		{
			data1->entity.CharIndex = 0;
			data1->entity.CharID = 0;
		}
		// Trigger the nameplate when the red arrow's coordinates roughly match the Chao's
		if (data1->entity.CharIndex != 2 && ChaoAnimationWhatever != -17 && dist_cam < 45.0f)
		{
			if (dist_arrow < 8.5f)
			{
				if (!data1->entity.CharIndex)
				{
					data1->entity.CharIndex = 1;
					data1->entity.CharID = 100;
				}
			}
			else
			{
				data1->entity.CharID = 0;
				data1->entity.CharIndex = 0;
			}
		}
		ChaoNameplate_Display(a1);
	}
	original(a1);
}

void ALR_Nameplate_Init()
{
	// Convert the first letter in Chaclon's name to uppercase
	for (int i = 0;i < 8;i++)
	{
		name[1][i] = ConvertToUppercase(name[1][i]);
		name[2][i] = ConvertToUppercase(name[2][i]);
		name[3][i] = ConvertToUppercase(name[3][i]);
		name[4][i] = ConvertToUppercase(name[4][i]);
	}
	WriteCall((void*)0x00751746, ChaoRaceRedArrowHook);
	Chao_Display_t = new Trampoline(0x007204B0, 0x007204B5, Chao_Display_r);
}

void ALR_Nameplate_OnFrame()
{
	if (ChaoStageNumber != CHAO_STG_RACE)
	{
		ChaoRaceTimer = 0;
		ChaoRaceEnded = false;
		return;
	}
	if (!ChaoRaceEnded && (IsLevelLoaded(39, 4) && !ChkPause()))
		ChaoRaceTimer += g_CurrentFrame;
}

/*
void BuildChaoFontUVMap()
{
	int numrows = 16;
	int numcolumns = 16;
	int xrow = 0;
	do
	{
		for (int xcolumn = 0; xcolumn < numcolumns; xcolumn++)
		{
			//ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].u1 = 11 * xcolumn;
			//ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].u2 = 11 * xcolumn + 11; // This is half the real number because the texture is rectangular
			//ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].v1 = 22 * xrow;
			//ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].v2 = 22 * xrow + 22;
			PrintDebug("{ 64, 64, 0, 0, %d, %d, %d, %d, 61, 0 },\n", -1 + 16 * xcolumn, -1 + 16 * xrow, -1 + 16 * xcolumn + 16, -1 + 16 * xrow + 16);
		}
		xrow++;
	} while (xrow < numrows);
	Old stuff
	int numrows = 12;
	int numcolumns = 23;
	int xrow = 0;
	do
	{
		for (int xcolumn = 0; xcolumn < numcolumns; xcolumn++)
		{
			ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].u1 = 11 * xcolumn;
			ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].u2 = 11 * xcolumn + 11; // This is half the real number because the texture is rectangular
			ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].v1 = 22 * xrow;
			ChaoNameLettersTexanim[xrow*numcolumns + xcolumn].v2 = 22 * xrow + 22;
			PrintDebug("{ 22, 22, 0, 0, %d, %d, %d, %d, 1, 0 },\n", 11 * xcolumn, 22 * xrow, 11 * xcolumn + 11, 22 * xrow + 22);
		}
		xrow++;
	} while (xrow < numrows);
}
*/