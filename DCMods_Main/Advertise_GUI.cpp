#include "stdafx.h"
#include "ScaleInfo.h"
#include "Advertise_GUI.h"

NJS_POINT2 PauseSelectionBoxPosScale[2]; // Stores position and scale (X+Y) for the pause selection rectangle
static bool EnableUIScale;

// Constants
static float LivesOffset1 = 80.0f; // Y offset for the lives icon
static float LivesOffset2 = 0.0f; // Y offset for the lives count

// Trampoline to remove items in the options menu
static Trampoline* GetPauseMenuActive_t = nullptr;
static Uint8 __cdecl GetPauseMenuActive_r(Uint8* options_orig)
{
	Uint8 options;
	const auto original = TARGET_DYNAMIC(GetPauseMenuActive);
	// Pause Hide by SF94
	if ((per[0]->on & (Buttons_X | Buttons_Y)) == (Buttons_X | Buttons_Y))
	{
		*options_orig = 0;
		return 0;
	}
	Uint8 result = original(options_orig);
	options = *options_orig;
	if (ModConfig::RemoveMap && options & PauseOptions_Map)
	{
		options &= ~PauseOptions_Map;
		result--;
	}
	if (ModConfig::RemoveCamera && options & PauseOptions_Camera)
	{
		options &= ~PauseOptions_Camera;
		result--;
	}
	if (ModConfig::RemoveSetUpPad && options & PauseOptions_Controls)
	{
		options &= ~PauseOptions_Controls;
		result--;
	}
	*options_orig = options;
	return result;
}

// Custom function to draw the pause menu frame with corners
void DrawPauseBox_DC(int n, NJS_POINT2* pos, NJS_POINT2* scale)
{
	NJS_POINT2 temppos = { 0 };
	NJS_POINT2 tempscale = { 0 };
	NJS_SPRITE _sp = { { pos->x, pos->y, -1.0001f }, scale->x, scale->y, 0, &texlist_pause, &anim_pause }; // [esp+4h] [ebp-20h]
	NJS_COLOR Pause_Colors[] = { 0xE00075C7, 0xE0040052, 0xE00075C7, 0xE0040052 };
	NJS_POINT2 Pause_Points[] = { { pos->x - 16, pos->y + 12 }, { pos->x - 16, pos->y + scale->y * 16 - 12 }, { pos->x + 16 + scale->x * 16, pos->y + 12 }, { pos->x + 16 + scale->x * 16, pos->y + scale->y * 16 - 12 } };
	NJS_POINT2COL Pause_Point2Col = { Pause_Points, Pause_Colors, NULL, 4 };

	// Main box with gradient
	late_DrawPolygon2D(&Pause_Point2Col, 4, 22047.0f, NJD_TRANSPARENT | NJD_FILL, LATE_LIG);
	// Frame
	for (int hz = 0; hz < 2; hz++)
	{
		bool left = hz == 0;
		// Horizontal bars
		temppos.x = pos->x;
		temppos.y = pos->y + 12.0f - 16.0f * left + (scale->y - 1.5f) * 16.0f * !left; // "left" here is top
		tempscale.x = scale->x;
		tempscale.y = 1.0f;
		_sp.p = { temppos.x, temppos.y, -1.0001f };
		_sp.sx = tempscale.x;
		_sp.sy = tempscale.y;
		SetMaterial(0.9f, left ? 0.0f : 0.016f, left ? 0.458f : 0.0f, left ? 0.78f : 0.322f);
		late_DrawSprite2D(&_sp, n, 22047.0f, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR, LATE_LIG);
		// Corners
		for (int vt = 0; vt < 2; vt++)
		{
			NJD_SPRITE flags = 0;
			bool top = vt == 0;
			temppos.x = pos->x - 16.0f * left + scale->x * 16.0f * !left;
			temppos.y = pos->y + 12.0f - 16.0f * top + (scale->y - 1.5f) * 16.0f * !top;
			tempscale.x = 1.0f;
			tempscale.y = 1.0f;
			if (!left)
				flags |= NJD_SPRITE_HFLIP;
			if (!top)
				flags |= NJD_SPRITE_VFLIP;
			SetMaterial(0.9f, top ? 0.0f : 0.016f, top ? 0.458f : 0.0f, top ? 0.78f : 0.322f);
			_sp.p = { temppos.x, temppos.y, -1.0001f };
			_sp.sx = tempscale.x;
			_sp.sy = tempscale.y;
			late_DrawSprite2D(&_sp, 7, 22047.0f, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | flags, LATE_LIG);
		}
	}
}

// Drawing the pause selection box (callback because it's hard to queue ghDrawPvrTextureS)
void DrawPauseSelectionBox_Callback(NJS_POINT2* a)
{
	
	using namespace uiscale;
	if (EnableUIScale)
		helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	njSetTexture(&texlist_pause);
	ghDrawPvrTextureS(2, a[0].x, a[0].y, 1.0f, a[1].x, a[1].y);
	if (EnableUIScale)
		helperFunctionsGlobal->PopScaleUI();
}

// Hook to draw the pause selection box
void DrawPauseSelectionBox_DC(int n, NJS_POINT2* pos, NJS_POINT2* scale)
{
	ghSetPvrTexVertexColor(0xFFED9700, 0xFFFFEA00, 0xFFED9700, 0xFFFFEA00);
	PauseSelectionBoxPosScale[0] = { pos->x, pos->y };	
	PauseSelectionBoxPosScale[1] = { scale->x, scale->y };
	late_SetFunc((void(__cdecl*)(void*))DrawPauseSelectionBox_Callback, (void*)PauseSelectionBoxPosScale, 48059.0f, LATE_LIG);
}

// Hack for the character mini boss hint count
void __cdecl EvBossDispHitPoint_r(__HPPOS* hppos)
{
	float x; // ST0C_4
	float y; // ST10_4
	ghInitPvrTexture();
	njSetTexture(&texlist_score);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	x = ScreenRaitoX * hppos->pos.x;
	y = ScreenRaitoY * (hppos->pos.y - 20.0f);
	float scale = ScreenRaitoY; // 1x for original PVM
	if (ModConfig::DLLLoaded_HDGUI)
		scale = scale / 4.0f;
	ghDrawPvrTextureS(hppos->hp, x, y, hppos->pos.z, scale, scale);
}

// Hook to disable alpha rejection for shadows (sprite version)
void DrawSprite_Hook(NJS_SPRITE* sp, Int n, Float pri, NJD_SPRITE attr, LATE queue_flags)
{
	njTextureShadingMode(1);
	late_DrawSprite2D(sp, n, pri, attr, queue_flags);
	njTextureShadingMode(2);
}

// Hook to disable alpha rejection for shadows (ghDrawPvrTexture version)
void DrawTexture_Hook(int texnum, float x, float y, float z)
{
	njTextureShadingMode(1);
	ghDrawPvrTexture(texnum, x, y, z);
	njTextureShadingMode(2);
}

// Hook to disable alpha rejection for shadows (ghDrawPvrTextureS version)
void DrawShadow_Hook(int texnum, float x, float y, float z, float scaleX, float scaleY)
{
	njTextureShadingMode(1);
	ghDrawPvrTextureS(texnum, x, y, z, scaleX, scaleY);
	njTextureShadingMode(2);
}

// Hook to disable alpha rejection for the green-white gradient (tutorial header etc.)
void DrawTitleBack_r(float x, float y, float z, float width, float height)
{
	njTextureShadingMode(1);
	GreenMenuRect_Draw(x, y, z, width, height);
	njTextureShadingMode(2);
}

// Hook for the main menu shadow (scale + alpha rejection)
void DrawMainMenuShadow_Hook(int texnum, float x, float y, float z, float scaleX, float scaleY)
{
	if (!ModConfig::DLLLoaded_HDGUI)
	{
		// This is for the original shadow texture which is 4 times smaller
		scaleX = scaleX * 4.0f;
		scaleY = scaleY * 4.0f;
	}
	njTextureShadingMode(1);
	ghDrawPvrTextureS(texnum, x, y, z, scaleX, scaleY);
	njTextureShadingMode(2);
}

// Reset Z Write before drawing stuff with B_CHNAM textures
void DrawChnamBFix(Uint8 index)
{
	njSetZCompare(index);
	njSetZUpdateMode(1);
}

void AdvertiseGUI_Init()
{
	EnableUIScale = helperFunctionsGlobal->LoaderSettings->ScaleHud;
	AdvertiseGUITextures_Init();
	// Disable "Now loading"
	WriteData<5>((char*)0x0040BE0D, 0x90);
	WriteData<5>((char*)0x00503438, 0x90);
	WriteData<5>((char*)0x0050346D, 0x90);
	// Pause box stuff
	WriteCall((void*)0x004585DA, DrawPauseBox_DC); // Main pause
	WriteCall((void*)0x00459085, DrawPauseBox_DC); // Camera
	WriteCall((void*)0x00458DBB, DrawPauseBox_DC); // Key remap
	WriteCall((void*)0x00458232, DrawPauseSelectionBox_DC);
	WriteData((float*)0x0045812F, 0.0f); // Button R
	WriteData((float*)0x0045812A, 0.7f); // Button G
	WriteData((float*)0x00458125, 1.0f); // Button B
	GetPauseMenuActive_t = new Trampoline(0x004582E0, 0x004582E8, GetPauseMenuActive_r);
	WriteJump(EvBossDispHitPoint, EvBossDispHitPoint_r); // HUD hack for Knuckles/Gamma boss fight
	WriteData((float*)0x004B63AD, 10000.0f); // Depth for boss HUD
	// HUD position
	if (ModConfig::HUDTweak)
	{
		WriteData((float**)0x00425E51, &LivesOffset1);
		WriteData((float**)0x00425EA4, &LivesOffset2);
		sprRing.p.y = 49.0f; // Adventure Field ring count
	}
	// Various fixes and textures already included in HD GUI	
	if (!ModConfig::DLLLoaded_HDGUI)
	{
		WriteCall((void*)0x005092A1, DrawTexture_Hook); // File icon
		WriteCall((void*)0x00511E47, DrawChnamBFix); // Fix disappearing character name after loading a different save
		WriteData<5>((void*)0x00511C18, 0x90); // Disable ZFunc stuff to prevent character model overlap issues
		// Shadow blending fixes
		WriteCall((void*)0x00457F2F, DrawSprite_Hook); // Pause menu
		WriteCall((void*)0x00431D37, DrawShadow_Hook); // AVA_SQUARE
		WriteCall((void*)0x00506EFF, DrawShadow_Hook); // Trial level select
		WriteCall((void*)0x0050D8B3, DrawShadow_Hook); // Emblem results screen
		WriteCall((void*)0x0050B584, DrawMainMenuShadow_Hook); // Main menu shadow
		WriteCall((void*)0x0050B61A, DrawMainMenuShadow_Hook); // Main menu (trial) shadow
		WriteCall((void*)0x00508FFD, DrawTexture_Hook); // Sound test icon
		WriteCall((void*)0x00509130, DrawTexture_Hook); // Sonic icon background
		WriteCall((void*)0x00509191, DrawTexture_Hook); // Sonic icon
		WriteCall((void*)0x00509439, DrawTexture_Hook); // Languages icon
		WriteCall((void*)0x0050952F, DrawTexture_Hook); // Rumble icon
		WriteCall((void*)0x0050782A, DrawTexture_Hook); // AVA_SAN triangle shadow
		WriteCall((void*)0x0064393E, DrawTitleBack_r); // Fix alpha rejection on green rectangle in tutorials
		WriteCall((void*)0x0050959A, DrawTitleBack_r); // Fix alpha rejection on green rectangle in the options screen
	}
}