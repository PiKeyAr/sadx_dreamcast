#include "stdafx.h"

void ProcessLandtable(_OBJ_LANDTABLE* landtable, unsigned __int16 levelact, bool unregister, bool lantern);
void PatchModels(LevelIDs level);
void RemoveMaterialColors(_OBJ_LANDTABLE* landtable);

// A struct that stores level data for a single act.
struct LevelLoadInfo
{
	LevelIDs Level;
	unsigned __int8 Act;
	LandTableInfo* Data;
	_OBJ_LANDTABLE* LandTableData;
};

// Array of loaded levels. Up to 8 LandTables can be loaded at the same time.
LevelLoadInfo LoadedLevels[8];

// Clear levels that aren't used currently
void ClearLoadedLevels(int levelact)
{
	for (int i = 0; i < 8; i++)
	{
		if (LoadedLevels[i].Data == nullptr)
			continue;
		// If a different level is loaded, unload it
		if (LoadedLevels[i].Level != (LevelIDs)(levelact >> 8))
		{
			// Don't unload Chao levels when going between them
			if (ssStageNumber >= LevelIDs_SSGarden && LoadedLevels[i].Level >= LevelIDs_SSGarden)
				continue;
			ProcessLandtable(LoadedLevels[i].LandTableData, ((LoadedLevels[i].Level << 8) | LoadedLevels[i].Act), true, true);
			if (ModConfig::DebugVerbose != DebugVerboseConfig::Off)
				PrintDebug("Unloading level %d act %d at %X: ", LoadedLevels[i].Level, LoadedLevels[i].Act, LoadedLevels[i].Data);
			delete LoadedLevels[i].Data;
			LoadedLevels[i].Data = nullptr;
			LoadedLevels[i].LandTableData = nullptr;
			if (ModConfig::DebugVerbose != DebugVerboseConfig::Off)
				PrintDebug("OK\n");
		}
		// If the level is already loaded, add back animations but don't add Lantern materials that are already there
		// Why is it like this? Sometimes the game calls LoadLevelFiles when a level is already loaded (e.g. after playing an FMV)
		// and I need to reload animations that were cleared.
		else if ((levelact & 0xFF) == LoadedLevels[i].Act)
		{
			//PrintDebug("Level already loaded: %X\n", levelact);
			ProcessLandtable(LoadedLevels[i].LandTableData, ((LoadedLevels[i].Level << 8) | LoadedLevels[i].Act), false, false);
			return;
		}
	}
}

// Main function that loads a new level per act.
void LevelLoader(unsigned __int16 levelact, const char* filename, NJS_TEXLIST* landTableTexlist, _OBJ_LANDTABLE* landtablePointer)
{
	// Load the level in an empty slot
	for (int i = 0; i < 8; i++)
	{
		if (LoadedLevels[i].Data == nullptr)
		{
			bool nopvm = false;
			LevelLoadInfo* info = &LoadedLevels[i];
			info->Level = (LevelIDs)(levelact >> 8);
			info->Act = levelact & 0xFF;
			if (info->Act == 0)
				PatchModels(info->Level);
			if (ModConfig::DebugVerbose != DebugVerboseConfig::Off)
				PrintDebug("Loading level %d act %d from file: %s: ", info->Level, info->Act, filename);
			info->Data = new LandTableInfo(helperFunctionsGlobal->GetReplaceablePath(filename));
			if (ModConfig::DebugVerbose != DebugVerboseConfig::Off)
				PrintDebug("OK\n");
			info->LandTableData = info->Data->getobjlandtable();
			info->LandTableData->pTexList = landTableTexlist;
			RemoveMaterialColors(info->LandTableData);
			ProcessLandtable(info->LandTableData, levelact, false, true);
			switch (info->Level)
			{
			case LevelIDs_EmeraldCoast:
			case LevelIDs_WindyValley:
			case LevelIDs_TwinklePark:
			case LevelIDs_SpeedHighway:
			case LevelIDs_RedMountain:
			case LevelIDs_SkyDeck:
			case LevelIDs_LostWorld:
			case LevelIDs_IceCap:
			case LevelIDs_Casinopolis:
			case LevelIDs_HotShelter:
			case LevelIDs_Chaos0:
			case LevelIDs_Chaos2:
			case LevelIDs_EggCarrierOutside:
			case LevelIDs_EggCarrierInside:
			case LevelIDs_MysticRuins:
			case LevelIDs_Past:
				nopvm = true;
				goto setlevel;
			case LevelIDs_StationSquare:
				nopvm = true;
				objLandTable[info->Level + 1][info->Act] = info->LandTableData;
				objLandTable[info->Level + 2][info->Act] = info->LandTableData;
				goto setlevel;
				break;
			case LevelIDs_SSGarden:
			case LevelIDs_ECGarden:
			case LevelIDs_MRGarden:
			case LevelIDs_ChaoRace:
				break;
			default:
				goto setlevel;
			setlevel:
				objLandTable[info->Level][info->Act] = info->LandTableData;
				break;
			}
			if (nopvm)
				info->LandTableData->pPvmFileName = NULL;
			if (landtablePointer != NULL)
				*landtablePointer = *info->LandTableData;
			return;
		}
	}
}

// Gets the DC landtable pointer from level+act (int16 version).
_OBJ_LANDTABLE* GetLevelLandtable(unsigned __int16 levelact)
{
	for (int i = 0; i < 8; i++)
	{
		if (LoadedLevels[i].Data == nullptr)
			continue;
		if (LoadedLevels[i].Level == (LevelIDs)(levelact >> 8) && LoadedLevels[i].Act == (levelact & 0xFF))
			return LoadedLevels[i].LandTableData;
	}
	return nullptr;
}

// Gets the DC landtable pointer from level+act (separated version).
_OBJ_LANDTABLE* GetLevelLandtable(unsigned __int8 level, unsigned __int8 act)
{
	return GetLevelLandtable((level << 8) | act);
}

// Checks if a level+act is currently loaded (int16 version).
bool IsLevelLoaded(unsigned __int16 levelact)
{
	for (int i = 0; i < 8; i++)
	{
		if (LoadedLevels[i].Data == nullptr)
			continue;
		if (LoadedLevels[i].Level == (levelact >> 8) && LoadedLevels[i].Act == (levelact & 0xFF) && LoadedLevels[i].Data != nullptr)
			return true;
	}
	return false;
}

// Checks if a level+act is currently loaded (separated version).
bool IsLevelLoaded(unsigned __int8 level, unsigned __int8 act)
{
	return IsLevelLoaded((level << 8) | act);
}