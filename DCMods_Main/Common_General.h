#pragma once

#include <SADXModLoader.h>

void ScaleEnvironmentMap(float value1 = -1, float value2 = -1, float value3 = -1, float value4 = -1);

DataPointer(NJS_COLOR, coloroffset, 0x03B29D5C); // Screen fade color

ThiscallFunctionPointer(void, gjTrans2D, (int attr), 0x00791940);
FunctionPointer(HRESULT, stResetTexture, (), 0x0078BBA0);
VoidFunc(gjRender2DSet, 0x0078B780);
VoidFunc(gjRender2DReset, 0x0078B800);
ThiscallFunctionPointer(void, stDrawTriangleFan2D, (NJS_POINT2COL* _this, float depth), 0x0078EA10);