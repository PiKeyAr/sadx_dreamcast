#pragma once

#include <SADXModLoader.h>

extern bool UpdatePaletteAtlas;
FunctionPointer(void, addColorRGB, (float r, float g, float b), 0x0057E590); // Function that edits Egg Viper stage lights for background color effect
TaskFunc(Egm3Init, 0x0057F120);
TaskFunc(Egm3Energy, 0x00582BC0);
DataPointer(task*, egm3_taskptr, 0x03C6CF60); // Egg Viper task pointer
DataArray(NJS_TEX, kemuri_uv_bias, 0x03C6E0BC, 2); // Egg Viper dust effect UV offset
DataArray(NJS_TEX, vuvS_0_kemuri_boss_boss, 0x01673820, 12); // Egg Viper dust effect UVs
DataArray(NJS_TEX, egm3DamageNow, 0x03C6E0E4, 12); // Egg Viper dust effect UVs copy
DataPointer(NJS_ACTION, action_s_s0009_sonic, 0x03C84898); // Sonic wait animation in the Egg Viper cutscene