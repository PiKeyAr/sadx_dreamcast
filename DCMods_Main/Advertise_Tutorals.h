#pragma once

#include <SADXModLoader.h>

// Tutorial screen items (not actual structs)

DataArray(GH_PVR_TEX, SonicSpr0_0_items, 0x02BC3ACE, 6);
DataArray(GH_PVR_TEX, SonicSpr0_J_items, 0x02BC3A8E, 5);
DataArray(GH_PVR_TEX, TailsCmnSpr0_E_items, 0x02BC3E90, 4); // Tails and Knuckles
DataArray(GH_PVR_TEX, TailsCmnSpr0_J_items, 0x02BC3E74, 3); // Tails and Knuckles
DataPointer(GH_PVR_TEX, TailsSpr0_E_items, 0x02BC3EB2);
DataPointer(GH_PVR_TEX, TailsSpr0_J_items, 0x02BC3EAA);
DataPointer(GH_PVR_TEX, KnucklesSpr0_E_items, 0x02BC425E);
DataPointer(GH_PVR_TEX, KnucklesSpr0_J_items, 0x02BC4256);
DataArray(GH_PVR_TEX, AmyCmnSpr0_E_items, 0x02BC46E6, 4); // Amy and Gamma
DataArray(GH_PVR_TEX, AmyCmnSpr0_J_items, 0x02BC46D2, 3); // Amy and Gamma
DataArray(GH_PVR_TEX, AmySpr0_E_items, 0x02BC4712, 2);
DataArray(GH_PVR_TEX, AmySpr0_J_items, 0x02BC4702, 2);
DataArray(GH_PVR_TEX, BigCmnSpr0_E_items, 0x02BC4E8A, 4);
DataArray(GH_PVR_TEX, BigSpr0_E_items, 0x02BC4EB6, 2);
DataArray(GH_PVR_TEX, BigSpr0_J_items, 0x02BC4EA6, 2);
DataArray(GH_PVR_TEX, BigSpr4_0_items, 0x02BC4F46, 6);
DataArray(GH_PVR_TEX, E102Spr0_E_items, 0x02BC4A7E, 2);
DataArray(GH_PVR_TEX, E102Spr0_J_items, 0x02BC4A6E, 2);