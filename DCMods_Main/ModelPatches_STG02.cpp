#include "stdafx.h"

void PatchModels_STG02()
{
	// dxpc\stg02_windy\common\models\windobj_cl_hasi2.nja.sa1mdl
		//Not replaced

	// dxpc\stg02_windy\bg\models\act3nbg_nbg1.nja.sa1mdl
	//((NJS_MATERIAL*)0x00C09CAC)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg02_windy\bg\models\act3nbg_nbg2.nja.sa1mdl
	//((NJS_MATERIAL*)0x00C0A640)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg02_windy\bg\models\act3nbg_nbg3.nja.sa1mdl
	//((NJS_MATERIAL*)0x00C0AB78)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg02_windy\bg\models\act3nbg_nbg4.nja.sa1mdl
	//((NJS_MATERIAL*)0x00C0B0B0)->diffuse.color = 0xFFB2B2B2;
	* (NJS_TEX*)0x00C0B0D0 = { 510, -255 };
	*(NJS_TEX*)0x00C0B0D4 = { 510, 255 };
	*(NJS_TEX*)0x00C0B0D8 = { 0, -255 };

	// dxpc\stg02_windy\common\models\kazeroop_kazegate.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\windobj_bigfloot.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bigflootb.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_a1.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_a2.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_a3.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_a4.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab01.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab02.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab03.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab04.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab05.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab06.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab07.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab08.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab09.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab10.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab11.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab12.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab13.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab14.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab15.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab16.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab17.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab18.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab19.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab20.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab21.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab22.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab23.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab24.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab25.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab26.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab27.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab28.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab29.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab30.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab31.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab32.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_bridge_ab33.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_cl_hasi1.nja.sa1mdl
		//Whatever

	// dxpc\stg02_windy\common\models\windobj_haneobj_a.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_haneobj_b.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_haneobj_c.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb1.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb2.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb3.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb4.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb5.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb6.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb7.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb8.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hane_cb9.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hasib.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hasib_break.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_hasib_end.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_isibasi.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\windobj_kazami.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_saku_a.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_saku_b.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\windobj_saku_c.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_tatehimo.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_tramp.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\windobj_wasi.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\windobj_yokohimo.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_brock_a.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_brock_b.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_brock_c.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_brock_d.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_brock_e.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_brock_moto.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_busshu.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_cube_l.nja.sa1mdl
		// Full replacement

	// dxpc\stg02_windy\common\models\wvobj_cube_m.nja.sa1mdl
		// Full replacement

	// dxpc\stg02_windy\common\models\wvobj_cube_s.nja.sa1mdl
		// Full replacement

	// dxpc\stg02_windy\common\models\wvobj_flower_a.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_flower_b.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_ha_green.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_ha_red.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_ha_yellow.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_iwaita.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_popo.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_sekichu_a.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_sekichu_b.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_tampopo.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_tateiwa_l.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_tateiwa_s.nja.sa1mdl
		//Full replacement

	// dxpc\stg02_windy\common\models\wvobj_tree.nja.sa1mdl
		//Full replacement
	// dxpc\stg02_windy\common\models\wvobj_ukisima.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_wara.nja.sa1mdl
		//Model too different

	// dxpc\stg02_windy\common\models\wvobj_watage.nja.sa1mdl
		//Full replacement
}