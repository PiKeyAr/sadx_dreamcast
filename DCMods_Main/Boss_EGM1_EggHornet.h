#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_OBJECT, object_egm01_egm01_m1_bw01, 0x0155AA54);
DataPointer(NJS_OBJECT, object_egm01_egm01_m1_m02, 0x0155EA6C);
DataPointer(NJS_OBJECT, object_egm01_egm01_m1_m01, 0x0155CE84);
DataPointer(NJS_OBJECT, object_egm01_egm01_hari2, 0x0155D5D0);
DataPointer(NJS_OBJECT, object_egm01_egm01_hari1, 0x0155B9E8);

VoidFunc(njBeginModel, 0x00781CC0);
VoidFunc(njEndModel, 0x00781DF0);
VoidFunc(setBoss_egm1, 0x00572230);
TaskFunc(TaskBossEnd, 0x0571FE0);

void ScaleEnvironmentMap(float value1 = -1, float value2 = -1, float value3 = -1, float value4 = -1);
void RestoreEnvironmentMap();