#include "stdafx.h"
#include "Advertise_Features.h"

// Quit prompt timer to make sure the quit menu sound plays even when the quit prompt is disabled
static int QuitSoundTimer = -1;

// Now Saving
static bool DebugFontItalic = false;
static int NowSavingProgress = 12;

// Sets the color of the recap text
void ColorizeRecapText(NJS_TEXTURE_VTX* a1, int a2, float a3, float a4, float a5, float a6, float a7, float a8)
{
	CreateMsgTexVtx(a1, 0xFFF8F8F8, a3, a4, a5, a6, a7, a8);
}

// Sets 30 FPS mode for stages
void FPSLockHook(int frameratemode)
{
	// 1 is 60 FPS, 2 is 30 FPS
	if (frameratemode == 1 && ssGameMode != MD_GAME_STAGENAME_INIT && ssStageNumber != LevelIDs_TwinkleCircuit)
		frameratemode = 2;
	stHWSetVSync(frameratemode);
}

// Draws a squished quad(for debug font)
void njDrawQuadTexture_Italic(NJS_QUAD_TEXTURE* points, float scale)
{
	// TODO: Rewrite this
	float widthmaybe; // st7
	Float base_x; // ecx
	Float v4; // edx
	float v5; // st7
	Float v6; // ecx
	float v7; // st7
	float v8; // st7
	NJS_QUAD_TEXTURE_EX _points; // [esp+0h] [ebp-40h]

	widthmaybe = points->x2 - points->x1;
	base_x = points->x1;
	_points.y = points->y1;
	v4 = points->u1;
	_points.vx1 = widthmaybe + DebugFontItalic * 4.0f; // Width
	_points.x = base_x + DebugFontItalic * 4.0f; // Offset for accuracy
	v5 = points->y2 - points->y1;
	_points.u = v4;
	_points.z = scale;
	v6 = points->v1;
	_points.vy2 = v5;
	v7 = points->u2 - points->u1;
	_points.vy1 = 0.0f;
	_points.vx2 = 0.0f;
	_points.v = v6;
	_points.vu1 = v7;
	_points.vv1 = 0.0f;
	v8 = points->v2;
	_points.vu2 = 0.0f;
	_points.vv2 = v8 - points->v1;
	_points.vx2 -= DebugFontItalic * 10.0f;
	Direct3D_DrawQuad(&_points);
}

// Hook to let the mod know to draw using italicized debug font
void SetHudColorAndTextureNum_Italic(int n, NJS_COLOR color)
{
	NJS_COLOR result;
	NJS_BGRA c = { color.argb.b, color.argb.g, color.argb.r, 255 };
	result.argb = c;
	if (color.color == 0x015A97E2 || color.color ==	0x01E2E2E2)
	{
		DebugFontItalic = true;
		njSetQuadTexture(n, result.color);
	}
	else
	{
		DebugFontItalic = false;
		njSetQuadTexture(n, color.color);
	}
}

// Trampoline for removing Mini Games Collection
static Trampoline* UnlockMiniGamesCollection_t = nullptr;
static int __cdecl UnlockMiniGamesCollection_r()
{
	return 0;
}

// Hook of njDrawSprite2D with disabled Z Write (for InetDemo)
void Draw2DSpriteHook(NJS_SPRITE* sp, Int n, Float pri, NJD_SPRITE attr, LATE queue_flags)
{
	// Dunno why it crashes when the sprite is queued but it doesn't if it's not queued
	njSetZUpdateMode(0);
	njDrawSprite2D_DrawNow(sp, n, pri, attr);
	njSetZUpdateMode(1);
}

// Hook to remove the Black Market ring count display
void __cdecl DisplayTotalRing_cdecl(task* a1)
{
	if (a1->twp->mode == 10 && a1->twp->counter.l > 0)
		a1->twp->counter.l = 0;
	return;
}

// Hook to remove the Black Market ring count display (asm)
static void __declspec(naked) DisplayTotalRing_asm()
{
	__asm
	{
		push esi // a1

		// Call your __cdecl function here:
		call DisplayTotalRing_cdecl

		pop esi // a1
		retn
	}
}

// Hook to remove the "Are you sure" prompt in the pause menu
void __cdecl MenuConfirmationPrompt_DontDisplay(void* arg_0)
{
	if (QuitSoundTimer == -1)
	{
		if (ulGlobalMode == MD_TRIAL || ulGlobalMode == MD_ADVENTURE || ulGlobalMode == MD_SUMMARY)
			QuitSoundTimer = 30;
		else
			QuitSoundTimer = 0;
	}
}

// Hook to let the mod know when to draw "Now Saving"
void NowSaving()
{
	if (!TEXTURE_FLAG)
	{
		TEXTURE_FLAG = 1;
		NowSavingProgress = -3;
	}
}

// Hook that replaces Mission Mode with Internet Demo
void InetDemoHook(int a1)
{
	CmnAdvaModeProcedure(ADVA_MODE_INETDEMO);
}

// Sets the Mission Mode item to be unlocked always
bool UnlockMissionMode()
{
	return true;
}

// Draws "Now Saving"
void NowSaving_Display()
{
	if (ulGlobalMode == MD_TRIAL || AdvertiseWork.MainMenuSelectedMode == 1)
		return;
	BackupDebugFontSettings();
	unsigned __int16 FontSize = unsigned __int16((16 * ((float)VerticalResolution / 480.0f)));
	float totalcount = (float)HorizontalResolution / FontSize;
	SetDebugFontSize(FontSize);
	for (int i = 0; i < 10; i++)
	{
		if (NowSavingProgress >= i)
			SetDebugFontColor(0x015A97E2);
		else
			SetDebugFontColor(0x01E2E2E2); // Set alpha to 1 to italicize
		DisplayDebugString(NJM_LOCATION((int)totalcount - 13 + i, 3), &NowSavingString[i]);
	}
	RestoreDebugFontSettings();
}

void AdvertiseFeatures_Init()
{
	if (ModConfig::RemoveGameGearGames)
		UnlockMiniGamesCollection_t = new Trampoline(0x00506460, 0x00506465, UnlockMiniGamesCollection_r);
	if (ModConfig::RemoveCream)
		WriteData<1>((void*)0x635610, 0xC3u);
	if (ModConfig::DrawNowSaving)
	{
		WriteData((char*)NowSavingString, "NOW SAVING");
		WriteJump((void*)0x0040BE40, NowSaving);
		if (!ModConfig::DLLLoaded_DLCs)
		{
			WriteCall((void*)0x00793D06, SetHudColorAndTextureNum_Italic);
			WriteCall((void*)0x00793BCC, njDrawQuadTexture_Italic);
		}
	}
	if (ModConfig::RemoveUnlockMessage)
		WriteData<1>((char*)0x004B5800, 0xC3u);
	if (ModConfig::RemoveMarketRingCount)
		WriteJump((void*)0x004297E0, DisplayTotalRing_asm); // Don't draw Black Market ring count after clearing a stage
	if (ModConfig::RemoveQuitPrompt)
	{
		WriteData<5>((char*)0x00414F28, 0x90u); // Don't show subtitle
		WriteCall((void*)0x00414EE3, MenuConfirmationPrompt_DontDisplay); // Set a delay to play the sound if needed, otherwise skip the menu
	}
	if (ModConfig::RemoveMissionMode)
	{
		WriteJump((void*)0x00506410, UnlockMissionMode); // Mission Mode menu item always enabled
		WriteData<1>((char*)0x007EE050, 0x0A); // Replace Mission texture ID with Internet
		WriteData<1>((char*)0x007EE098, 0x0A); // Replace Mission texture ID with Internet
		WriteCall((void*)0x0050C740, Draw2DSpriteHook);
		WriteCall((void*)0x0050C590, Draw2DSpriteHook);
		WriteCall((void*)0x0050B7A8, InetDemoHook); // Load InetDemo instead of Mission Mode
	}
	if (ModConfig::FPSLock)
		WriteCall((void*)0x00411E79, FPSLockHook);
	if (ModConfig::RestoreYButton)
		WriteData<5>((char*)0x0040FDEA, 0x90u);
	// Disable font smoothing
	if (ModConfig::DisableFontSmoothing)
	{
		// Probably better than making the whole texture ARGB1555
		WriteData<1>((char*)0x0040DA0B, 0x00);
		WriteData<1>((char*)0x0040DA0C, 0x00);
		WriteData<1>((char*)0x0040DA12, 0x00);
	}
	if (ModConfig::ColorizeFont)
	{
		// Subtitles (ARGB from 0 to F: CEEF)
		WriteData<1>((char*)0x0040E28D, 0xEF);
		WriteData<1>((char*)0x0040E28E, 0xCE);
		// Pause menu text (ARGB from 00 to FF: BFEFEFFF)
		WriteData<1>((char*)0x0040E541, 0xFF);
		WriteData<1>((char*)0x0040E542, 0xEF);
		WriteData<1>((char*)0x0040E543, 0xEF);
		WriteData<1>((char*)0x0040E544, 0xBF);
		// Recap screen (just FF F8 F8 F8)
		WriteCall((void*)0x006428AD, ColorizeRecapText);
	}
	if (ModConfig::DisableFontFiltering && !ModConfig::DLLLoaded_HDGUI)
		WriteCall((void*)0x0040D7DA, late_DrawSprite2D_Point);
}

void AdvertiseFeatures_OnFrame()
{
	if (ModConfig::DrawNowSaving && NowSavingProgress < 12)
	{
		if (ulGlobalTimer % 6 * g_CurrentFrame == 0)
			NowSavingProgress++;
		if (ulGlobalMode != MD_TITLE2 && ssGameMode != MD_GAME_MAIN && current_event == -1)
			return;
		NowSaving_Display();
	}
	// Skip pause menu confirmation prompt
	if (ModConfig::RemoveQuitPrompt && mode == 2)
	{
		if (QuitSoundTimer == 0)
		{
			QuitSoundTimer = -1;
			mode = 3;
		}
		else
			QuitSoundTimer--;
	}
	// Use Y button as intended in stages
	if (ModConfig::RestoreYButton && !IsLevelChaoGarden())
		GrabButtons = (Buttons)(Buttons_B | Buttons_X | Buttons_Y);
}