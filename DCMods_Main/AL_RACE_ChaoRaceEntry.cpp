#include "stdafx.h"
#include "Trampoline.h"
#include "AL_RACE_ChaoRaceEntry.h"

VoidFunc(_alg_entrance_epilog, 0x007199E0);
VoidFunc(PauseEnable, 0x00413980);

NJS_TEXNAME textures_al_race01_dc[23];
NJS_TEXLIST texlist_al_race01_dc = { arrayptrandlength(textures_al_race01_dc) };

task* ObjectManageTask_p_entry = nullptr; // Task manager for Chao Race Entry (needed for "Chao Race" letters and the cheer neon thing)

static AL_ST_POS StartPosChaoRaceEntryDC = { { 2052, 0, 0 }, 0x8000 }; // Start position for the DC lobby

// Trampoline for DC entry button
static Trampoline* execObject_11_t = nullptr;
static void __cdecl execObject_11_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(execObject_11);
	original(tp);
	// If the button is pressed, go to the DX Chao Race Entry
	if (tp->mwp->work.b[0] == 2)
	{
		SkipSA1Entry = true;
		AL_ChangeStageLater(CHAO_STG_ENTRANCE);
	}
}

// Chao Race Entry load function
void __cdecl AL_EntranceMaster_r(task* tp)
{
	ObjectManageTask_p_entry = tp;
	tp->exec = AL_EntranceMasterExec;
	tp->disp = (void(__cdecl*)(task*))nullsub;
	tp->dest = AL_EntranceMasterDest;
	___njSetBackColor(0xFF000000, 0xFF000000, 0xFF000000);
	AL_LoadTex("OBJ_AL_RACE_E", &texlist_obj_al_race, 1u);
	AL_LoadTex("AL_RACE01", &texlist_al_race01_dc, 1u);
	AL_LoadTex("AL_TEX_COMMON", &texlist_al_tex_common, 1u);
	AL_JoyInputStart();
	gLoadingAnotherGarden = AL_LoadAnotherGardenInfoStart();
	gTask = CreateElementalTask(0xA, 2, ChaoRaceNumbers_Load);
	ALCAM_CreateCameraManager();
	AL_CreateNormalCameraTask();
	if (ObjectManageTask_p_entry)
		MirenSetChildTask(ObjectManageTask_p_entry, &NeonLogoTaskInfo, &RaceEntryParam); // "Chao Race" letters 
	LoadObjects_RaceEntry(); // SET objects
	ADXTaskInit();
	BGM_Play(MusicIDs_ChaoRaceEntrance_OLD);
	gClipMap.f32Near = -1.0f;
	gClipMap.f32Far = -12000.0f;
}

// Exit Chao Race to the SA1 lobby (2) instead of SS garden (4)
void ExitRaceEntry(eCHAO_STAGE_NUMBER a1)
{
	SkipSA1Entry = false;
	AL_ChangeStageLater(CHAO_STG_ENTRANCE);
}

// Chao dance animation
void ALBHV_Oshaburiburi_r(task* tp)
{
	__int16* p_flag; // esi

	p_flag = &tp->twp[7].flag;
	if (!tp->twp[7].wtimer)
	{
		AL_SetMotionLink(tp, 207);
		++p_flag[1];
	}
}

// Makes Chao dance when the number doors go up
void ChaoStartDance(task* tp, void* a2)
{
	AL_SetBehavior(tp, ALBHV_Oshaburiburi_r);
}

// Hack for the Chao Race Entry prolog load function
void _alg_entrance_prolog_r()
{
	if (SkipSA1Entry)
	{
		WriteData((AL_ST_POS**)0x007152A9, &AL_StartPosSS_SS);
		PrintDebug("DX ChaoStgEntrance _prolog begin.\n");
		CreateElementalTask(LoadObj_Data1, 5, AL_EntranceMaster);
		LandChangeLandTable(&objLandTableChaoEntrance);
		PrintDebug("DX ChaoStgEntrance _prolog end.\n");
		SkipSA1Entry = false;
	}
	else
	{
		LevelLoader(LevelAndActIDs_ChaoRace, "system\\data\\chao\\stg_entrance1\\landtablechaoentrance.c.sa1lvl", &texlist_al_race01_dc);
		WriteData((AL_ST_POS**)0x007152A9, &StartPosChaoRaceEntryDC);
		PrintDebug("DCChaoStgEntrance _prolog begin.\n");
		CreateElementalTask(LoadObj_Data1, 5, AL_EntranceMaster_r);
		LandChangeLandTable(GetLevelLandtable(42, 0));
		PrintDebug("DCChaoStgEntrance _prolog end.\n");
	}
}

// Exiting DC Chao Race entry restores the original SS spawn point
void _alg_entrance_epilog_r()
{
	PauseEnable();
	PrintDebug("DCChaoStgEntrance _epilog begin.\n");
	WriteData((AL_ST_POS**)0x007152A9, &AL_StartPosSS_SS);
	PrintDebug("DCChaoStgEntrance _epilog end.\n");
}

void AL_ChaoRaceEntry_Init()
{
	WriteCall((void*)0x00746FFF, ChaoStartDance);
	WriteJump(_alg_entrance_prolog, _alg_entrance_prolog_r);
	WriteJump(_alg_entrance_epilog, _alg_entrance_epilog_r);
	WriteCall((void*)0x0072C618, ExitRaceEntry);
	execObject_11_t = new Trampoline(0x0071D0E0, 0x0071D0E6, execObject_11_r);
	ReplaceCAM("CAMAL_RACE00S");
	ReplacePVM("AL_RACE01");
}