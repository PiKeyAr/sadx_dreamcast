#pragma once

#include <SADXModLoader.h>

FunctionPointer(void, SetChunkParametersModel, (NJS_CNK_MODEL* a1, int a2), 0x0078AC80); // Inlined in X360
FunctionPointer(void, chRareEggDrawObject, (NJS_CNK_OBJECT* a1, int color), 0x0078AF40);
FunctionPointer(void, chRareEggDrawMotion, (NJS_CNK_OBJECT* object, int color, NJS_MOTION* motion, float frame), 0x0078AF80);
FunctionPointer(void, chRareEggDrawModel, (NJS_CNK_MODEL* a1, int color), 0x0078AF10);
FunctionPointer(void, chCnkDrawMotion, (NJS_CNK_OBJECT* object, NJS_MOTION* motion, float frame), 0x0078ABB0);

DataPointer(NJS_CNK_OBJECT, object_alo_dummyegg_dummyegg, 0x03601B94); // Chao Egg full
DataPointer(NJS_CNK_OBJECT, object_alm_egg_egg, 0x036014D0); // Chao Egg broken
DataPointer(NJS_CNK_OBJECT, object_alm_egg_eggbottom, 0x03600F0C); // Chao Egg bottom
DataPointer(NJS_CNK_OBJECT, object_alm_egg_eggtop, 0x0360149C); // Chao Egg top