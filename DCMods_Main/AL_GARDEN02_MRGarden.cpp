#include "stdafx.h"

NJS_TEXNAME textures_garden_mr_sky_dc[5];
NJS_TEXLIST texlist_garden_mr_sky_dc = { arrayptrandlength(textures_garden_mr_sky_dc) };

NJS_TEXNAME textures_garden_mr_dc[52];
NJS_TEXLIST texlist_garden_mr_dc = { arrayptrandlength(textures_garden_mr_dc) };

NJS_OBJECT* ChaoGardenSky_MR_Day = nullptr;
NJS_OBJECT* ChaoGardenSky_MR_Night = nullptr; // MR garden skybox for evening and night

void LoadObjects_MR();

bool ModelsLoaded_MRGarden = false;

void MRGardenSkybox_Display(task* tp)
{
	___njFogDisable();
	njSetTexture(&texlist_garden_mr_sky_dc);
	njPushMatrix(0);
	njTranslate(0, camera_twp->pos.x, 0, camera_twp->pos.z);
	njScale(0, 1.0f, 1.0f, 1.0f);
	late_z_ofs___ = -47000.0f;
	late_DrawObjectClip(SeqGetTime() == SEQ_DAYLIGHT ? ChaoGardenSky_MR_Day : ChaoGardenSky_MR_Night, LATE_WZ, 1.0f);
	njPopMatrix(1u);
	late_z_ofs___ = 0;
	___njFogEnable();
}

void MRGardenSkybox_Main(task* tp)
{
	MRGardenSkybox_Display(tp);
}

void MRGardenSkybox_Load(task* tp)
{
	tp->exec = MRGardenSkybox_Main;
	tp->disp = MRGardenSkybox_Display;
	tp->dest = FreeTask;
}

void _alg_garden02_mr_daytime_prolog_r()
{
	HMODULE handle = GetModuleHandle(L"CHAOSTGGARDEN02MR_DAYTIME");
	PrintDebug("DCChaoStgGarden02MR_Daytime _prolog\n");
	stg_garden02_mr_objects = (NJS_OBJECT**)GetProcAddress(handle, "stg_garden02_mr_objects");
	texlist_garden02mr_daytime = (NJS_TEXLIST*)&texlist_garden_mr_dc;
	objLandTableGarden02_Daytime = GetLevelLandtable(LevelAndActIDs_MRGarden);
}

void _alg_garden02_mr_evening_prolog_r()
{
	HMODULE handle = GetModuleHandle(L"CHAOSTGGARDEN02MR_EVENING");
	PrintDebug("DCChaoStgGarden02MR_Evening _prolog\n");
	stg_garden02_mr_objects = (NJS_OBJECT**)GetProcAddress(handle, "stg_garden02_mr_objects");
	texlist_garden02mr_evening = (NJS_TEXLIST*)&texlist_garden_mr_dc;
	objLandTableGarden02_Evening = GetLevelLandtable(LevelAndActIDs_MRGarden);
}

void _alg_garden02_mr_night_prolog_r()
{
	HMODULE handle = GetModuleHandle(L"CHAOSTGGARDEN02MR_NIGHT");
	PrintDebug("DCChaoStgGarden02MR_Night _prolog\n");
	stg_garden02_mr_objects = (NJS_OBJECT**)GetProcAddress(handle, "stg_garden02_mr_objects");
	texlist_garden02mr_night = (NJS_TEXLIST*)&texlist_garden_mr_dc;
	objLandTableGarden02_Night = GetLevelLandtable(LevelAndActIDs_MRGarden);
}

void Garden02_Load()
{
	LevelLoader(LevelAndActIDs_MRGarden, "system\\data\\chao\\stg_garden02_mr\\landtablegarden02.c.sa1lvl", &texlist_garden_mr_dc);
	if (!ModelsLoaded_MRGarden)
	{
		ChaoGardenSky_MR_Day = LoadModel("system\\data\\chao\\stg_garden02_mr\\terr_daytime\\map\\mrch_sky.nja.sa1mdl");
		ChaoGardenSky_MR_Night = LoadModel("system\\data\\chao\\stg_garden02_mr\\terr_night\\map\\mrch_sky.nja.sa1mdl");
		ModelsLoaded_MRGarden = true;
	}
}

void _alg_garden02_mr_prolog_r()
{
	PrintDebug("DCChaoStgGarden02MR Prolog\n");
	Garden02_Load();
	CreateElementalTask(LoadObj_Data1, 2, AL_Garden02Master);
	LoadObjects_MR();
	switch (SeqGetTime())
	{
	case SEQ_DAYLIGHT:
		texLoadTexturePvmFile("GARDEN_MR_SKY_HIRU", &texlist_garden_mr_sky_dc);
		LoadGameDLL("ChaoStgGarden02MR_Daytime", 2);
		ChaoGardenMR_SetLandTable_Day();
		ModuleDestructors[1] = Print_ChaoStgGarden02MR_Daytime_epilog;
		break;
	case SEQ_EVENING:
		texLoadTexturePvmFile("GARDEN_MR_SKY_YUU", &texlist_garden_mr_sky_dc);
		LoadGameDLL("ChaoStgGarden02MR_Evening", 2);
		ChaoGardenMR_SetLandTable_Evening();
		ModuleDestructors[1] = Print_ChaoStgGarden02MR_Evening_epilog;
		break;
	case SEQ_NIGHT:
		texLoadTexturePvmFile("GARDEN_MR_SKY_YORU", &texlist_garden_mr_sky_dc);
		LoadGameDLL("ChaoStgGarden02MR_Night", 2);
		ChaoGardenMR_SetLandTable_Night();
		ModuleDestructors[1] = Print_ChaoStgGarden02MR_Night_epilog;
		break;
	}
	SetChaoLandTable(LandTable_ChaoGardenMR);
}

void Garden02_Init()
{
	ReplacePVM("GARDEN_MR_SKY_HIRU");
	ReplacePVM("GARDEN_MR_SKY_YORU");
	ReplacePVM("GARDEN_MR_SKY_YUU");
	ReplacePVM("GARDEN02");
	ReplacePVM("GARDEN02_OBJECT");
	WriteJump(_alg_garden02_mr_prolog, _alg_garden02_mr_prolog_r);
	WriteJump(_alg_garden02_mr_daytime_prolog, _alg_garden02_mr_daytime_prolog_r);
	WriteJump(_alg_garden02_mr_evening_prolog, _alg_garden02_mr_evening_prolog_r);
	WriteJump(_alg_garden02_mr_night_prolog, _alg_garden02_mr_night_prolog_r);
	WriteData<5>((void*)0x00718E43, 0x90); // Kill SADX water
	WriteData((float*)0x00718E7C, -12000.0f); // Draw distance
	ChaoSetPositionMR[0] = { 264, 15.4f, -65.375f };
	ChaoSetPositionMR[1] = { 138.125f, 3.4f, 26.75f };
	ChaoSetPositionMR[2] = { 235, 15.525f, -24 };
	ChaoSetPositionMR[3] = { 131.875f, 5.05f, 90.3f };
	ChaoSetPositionMR[4] = { 80, 9.2f, -35 };
	ChaoSetPositionMR[5] = { -12, 2.25f, 33.75f };
	ChaoSetPositionMR[6] = { 18.5f, 19.2f, -11.875f };
	ChaoSetPositionMR[7] = { -53.75f, 46.25f, -74 };
	ChaoSetPositionMR[8] = { -139, 25.125f, 3.875f };
	ChaoSetPositionMR[9] = { -200.625f, 10.875f, 18 };
	ChaoSetPositionMR[10] = { -199.375f, 7.65f, 56.5f };
	ChaoSetPositionMR[11] = { -207, 2, 154.375f };
	ChaoSetPositionMR[12] = { -85.25f, 1.575f, 141.75f };
	ChaoSetPositionMR[13] = { -164.75f, 1.8f, 152.5f };
	ChaoSetPositionMR[14] = { -36, 4.6f, 148.875f };
	ChaoSetPositionMR[15] = { 6.7f, 2, 68.5f };
	TreeSetPos[2][0].x = 126.8847f;
	TreeSetPos[2][0].y = 3.2116146f;
	TreeSetPos[2][0].z = 129.4048f;
	TreeSetPos[2][1].x = -45.25877f;
	TreeSetPos[2][1].y = 3.0f;
	TreeSetPos[2][1].z = 108.8823f;
	TreeSetPos[2][2].x = -157.6473f;
	TreeSetPos[2][2].y = 2.8f;
	TreeSetPos[2][2].z = 122.3606f;
	TreeSetPos[2][3].x = -55.71612f;
	TreeSetPos[2][3].y = 47.5f;
	TreeSetPos[2][3].z = 20.53316f;
	TreeSetPos[2][4].x = 83.6948f;
	TreeSetPos[2][4].y = 7.5f;
	TreeSetPos[2][4].z = -47.53315f;
}