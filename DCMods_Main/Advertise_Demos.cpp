#include "stdafx.h"
#include "Advertise_Demos.h"

static bool DemosDone = false;

int dsReturnCheck_r()
{
	// Disabled vanilla DX code
	/*
	if (DemoPause == 2 && !DemoStage)
	{
		MsMovie(0); // Plays the intro movie? I don't get this
	}
	*/
	// If a demo was cancelled with the Start button, go back to the title screen.
	if (!DemoPause)
	{
		if (GB_demoevent != -1) // If it was an event demo, skip the gameplay segment and go to the next event demo.
			DemoStage++;
		return 0;
	}
	// If a cutscene demo finished, proceed to the gameplay demo.
	if (DemoPause != 0 && GB_demoevent != -1)
	{
		dsGoDemoLoop(); // Run the demo loop
		return MD_ACTION;  // Don't go back to the title screen.
	}
	return 0; // Goes back to the title screen
}

void CheckAndRestoreDemos()
{
	// Check if some other mod has messed with demos
	if (demotbl[0][0] != LevelIDs_EggCarrierOutside || demotbl[0][3] != 27)
	{
		DemosDone = true;
		return;
	}
	WriteData((__int16**)0x0042C82A, &SA1DemoArray[0][1]); // Act
	WriteData((__int16**)0x0042C833, &SA1DemoArray[0][2]); // Character
	WriteData((__int16**)0x0042C83C, &SA1DemoArray[0][0]); // Level
	WriteData((__int16**)0x0042C844, &SA1DemoArray[0][3]); // Cutscene
	WriteData<1>((char*)0x0042C8A3, 0x0Cu); // 12 demos instead of 6
	WriteData<1>((char*)0x0042CD98, 0x0Cu); // 12 demos instead of 6
	DemosDone = true;
}

void AdvertiseDemos_Init()
{
	WriteJump(dsReturnCheck, dsReturnCheck_r);
	switch (ModConfig::RestoreDemos)
	{
	case DemosConfig::DreamcastUS:
		ReplaceGeneric("K_AMY.BIN", "K_AMY_DC.BIN");
		ReplaceGeneric("K_BIG.BIN", "K_BIG_DC.BIN");
		ReplaceGeneric("K_BIG_PAL.BIN", "K_BIG_PAL_DC.BIN");
		ReplaceGeneric("K_E102.BIN", "K_E102_DC.BIN");
		ReplaceGeneric("K_KNUCK.BIN", "K_KNUCK_DC.BIN");
		ReplaceGeneric("K_MILES.BIN", "K_MILES_DC.BIN");
		ReplaceGeneric("K_SONIC.BIN", "K_SONIC_DC.BIN");
		ReplaceGeneric("K_SONIC_PAL.BIN", "K_SONIC_PAL_DC.BIN");
		break;
	case DemosConfig::DreamcastJP:
		ReplaceGeneric("K_AMY.BIN", "K_AMY_JP.BIN");
		ReplaceGeneric("K_BIG.BIN", "K_BIG_JP.BIN");
		ReplaceGeneric("K_E102.BIN", "K_E102_JP.BIN");
		ReplaceGeneric("K_KNUCK.BIN", "K_KNUCK_JP.BIN");
		ReplaceGeneric("K_MILES.BIN", "K_MILES_JP.BIN");
		ReplaceGeneric("K_SONIC.BIN", "K_SONIC_JP.BIN");
		break;
	case DemosConfig::Gamecube:
		ReplaceGeneric("K_AMY.BIN", "K_AMY_GC.BIN");
		ReplaceGeneric("K_BIG.BIN", "K_BIG_GC.BIN");
		ReplaceGeneric("K_BIG_PAL.BIN", "K_BIG_PAL_GC.BIN");
		ReplaceGeneric("K_E102.BIN", "K_E102_GC.BIN");
		ReplaceGeneric("K_KNUCK.BIN", "K_KNUCK_GC.BIN");
		ReplaceGeneric("K_MILES.BIN", "K_MILES_GC.BIN");
		ReplaceGeneric("K_SONIC.BIN", "K_SONIC_GC.BIN");
		ReplaceGeneric("K_SONIC_PAL.BIN", "K_SONIC_PAL_GC.BIN");
		break;
	default:
		break;
	}
}

void AdvertiseDemos_OnFrame()
{
	if (DemosDone)
		return;
	if (ulGlobalMode != MD_START)
		CheckAndRestoreDemos();
}

void AdvertiseDemos_OnInput()
{
	// Demo player - SADX PC advances the demo frame counter under the wrong condition so it never increases.
	if (ssGameMode == MD_GAME_MAIN && DemoType == 1 && GB_demoevent == -1)
		DemoDataPtrNum++;
}