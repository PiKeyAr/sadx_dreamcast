#include "stdafx.h"

static bool ModelsLoaded_SBOARD;

void SandHill_Init()
{
	ReplaceCAM("CAMSBOARD00S");
	ReplaceCAM("CAMSBOARD01S");
	ReplaceSET("SETSBOARD00M");
	ReplaceSET("SETSBOARD00S");
	ReplaceSET("SETSBOARD01S");
	ReplacePVM("BG_SANDBOARD");
	ReplacePVM("EFF_SANDBOARD");
	ReplacePVM("OBJ_SANDBOARD");
	ReplacePVM("SANDBOARD");
	// Fog color
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Sand[0][i].Col= 0xFFE0E0B0;
		pFogTable_Sand[0][i].f32StartZ = 800.0f;
		pFogTable_Sand[0][i].f32EndZ = 10000.0f;
	}
}

void SandHill_Load()
{
	// This stuff is done every time the function is called
	LevelLoader(LevelAndActIDs_SandHill, "SYSTEM\\data\\sandboard\\landtablesboard00.c.sa1lvl", &texlist_sandboard);
	objLandTable[38][1] = objLandTable[38][0]; // Clone landtable pointer
	// This stuff is done only once
	if (!ModelsLoaded_SBOARD)
	{
		// Models
		object_sunabo_sb_jpdai_sb_jpdai = *LoadModel("system\\data\\sandboard\\common\\models\\sunabo_sb_jpdai.nja.sa1mdl"); // Ramp
		ModelsLoaded_SBOARD = true;
	}
}