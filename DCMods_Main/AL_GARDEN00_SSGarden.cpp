#include "stdafx.h"
#include "FunctionHook.h"

VoidFunc(SeqInitStage, 0x00413870);

DataPointer(NJS_MOTION, motion_stch_racegate_waku, 0x0340D978);
DataArray(TEX_PVMTABLE, PvmListChaoSS, 0x0090E868, 25);

void LoadObjects_SS();
void SSGardenElevator_Load();

NJS_TEXNAME texnames_garden00_object_dc[16]; // The original texlist doesn't have texnames in SADX

FunctionHook<void> SeqInitStage_h(SeqInitStage);

bool ModelsLoaded_SSGarden = false;

// In SADX, the function SeqInitStage resets the first 64 flags in seqVars, which are needed for DC garden hints to work.
// This function backs up the flags needed for garden hints and restores them after running the original function.
static void __cdecl SeqInitStage_r()
{
	char backup2 = seqVars[2];
	char backup3 = seqVars[3];
	char backup8 = seqVars[8];
	SeqInitStage_h.Original();
	seqVars[2] = backup2;
	seqVars[3] = backup3;
	seqVars[8] = backup8;
}

void Garden00_Load()
{
	LevelLoader(LevelAndActIDs_SSGarden, "SYSTEM\\data\\chao\\stg_garden00_ss\\landtablegarden00.c.sa1lvl", &texlist_garden00ss);
	// Objects
	if (!ModelsLoaded_SSGarden)
	{
		object_stch_racegate_waku_waku = *LoadModel("system\\data\\chao\\stg_garden00_ss\\obj\\stch_racegate_waku.nad.sa1mdl"); // Chao Race door
		motion_stch_racegate_waku = *LoadAnimation("system\\data\\chao\\stg_garden00_ss\\obj\\stch_racegate_waku.nam.saanim"); // Chao Race door animation
		texLoadTexturePvmFile("GARDEN00_OBJECT", &texlist_garden00_object); // Other gardens call these functions in the Load function, but SS garden uses the main function
		WriteData((NJS_TEXLIST**)0x0072A963, &texlist_garden00_object); // Chao Race door texlist
		SSGardenElevator_Load();
		ModelsLoaded_SSGarden = true;
	}
}

void Garden00_OnFrame()
{
	if (ChaoStageNumber != CHAO_STG_SS)
		return;
	if (IsLevelLoaded(LevelAndActIDs_SSGarden) && !ChkPause() && ModConfig::EnabledLevels[LevelIDs_SSGarden])
	{
		auto entity = playertwp[0];
		if (entity != nullptr)
		{
			if (entity->pos.z > 150.0f && entity->pos.x > 30.0f)
			{
				AL_ChangeStageLater(CHAO_STG_BLACKMARKET);
			}
		}
	}
}

void _alg_garden00_ss_prolog_r()
{
	PrintDebug("DCChaoStgGarden00SS Prolog begin\n");
	Garden00_Load();
	CreateElementalTask(LoadObj_Data1, 2, AL_Garden00Master);
	LoadObjects_SS();
	LandChangeLandTable(GetLevelLandtable(LevelAndActIDs_SSGarden));
	PrintDebug("DCChaoStgGarden00SS Prolog end\n");
}

void Garden00_Init()
{	
	// TODO: Figure out how to set hint state properly
	//SeqInitStage_h.Hook(SeqInitStage_r); // Hook to not reset Chao hints
	WriteData<6>((char*)0x0063E1A2, 0x90u); // The elevator function also resets seqVars[2] for some reason
	ReplacePVM("GARDEN00");
	ReplaceCAM("SETMI3900M");
	ReplacePVM("GARDEN00_OBJECT");
	texlist_garden00_object.textures = texnames_garden00_object_dc; // Original texlist has no texnames
	texlist_garden00_object.nbTexture = 16;
	WriteData<5>((void*)0x0071957E, 0x90); // Disable the Sonic Team homepage prompt
	WriteData<5>((void*)0x007195A3, 0x90); // SADX SS Garden Exit
	WriteData<5>((void*)0x007195A9, 0x90); // SADX Chao Race door
	WriteData<1>((void*)0x00719265, 0x7D); // Exit 1
	WriteData<1>((void*)0x00719264, 0xF2); // Exit 2
	WriteData<1>((void*)0x00719263, 0xF0); // Exit 2
	WriteJump(_alg_garden00_ss_prolog, _alg_garden00_ss_prolog_r);
	ChaoSetPositionSS[0] = { 190.375f, 2.875f, 58.4f };
	ChaoSetPositionSS[1] = { 116.25f, 4, 38 };
	ChaoSetPositionSS[2] = { 76, 2.875f, 36 };
	ChaoSetPositionSS[3] = { 103, 3.375f, 56 };
	ChaoSetPositionSS[4] = { 141, 1.075f, 80 };
	ChaoSetPositionSS[5] = { 55, 0, -104 };
	ChaoSetPositionSS[6] = { 47,  0, -58 };
	ChaoSetPositionSS[7] = { -4, 0, 4.36f };
	ChaoSetPositionSS[8] = { -34, 0, 52 };
	ChaoSetPositionSS[9] = { -100, 0, 100 };
	ChaoSetPositionSS[10] = { -47, 0, -109 };
	ChaoSetPositionSS[11] = { -100, 0, -88 };
	ChaoSetPositionSS[12] = { -100, 0, -67 };
	ChaoSetPositionSS[13] = { -165, -0.5f, -6.3f };
	ChaoSetPositionSS[14] = { -179.625f, -1.125f, 76 };
	ChaoSetPositionSS[15] = { -172.625f, 0.125f, 100 };
	TreeSetPos[0][0].x = 129.32f; // Palm tree 1
	TreeSetPos[0][0].y = 4.0f;  // Palm tree 1
	TreeSetPos[0][0].z = 124.78f;  // Palm tree 1
	TreeSetPos[0][1].x = 177.62f; // Palm tree 1
	TreeSetPos[0][1].y = 4.0f; // Palm tree 1
	TreeSetPos[0][1].z = 62.64f;  // Palm tree 1
	TreeSetPos[0][2].x = 109.29f; // Palm tree 1
	TreeSetPos[0][2].y = 4.0f;  // Palm tree 1
	TreeSetPos[0][2].z = 2.2f;  // Palm tree 1
	TreeSetPos[0][3].x = 80.24f; // Palm tree 1
	TreeSetPos[0][3].y = 4.0f;  // Palm tree 1
	TreeSetPos[0][3].z = 52.08f;  // Palm tree 1
	TreeSetPos[0][4].x = 107.19f; // Palm tree 1
	TreeSetPos[0][4].y = 4.0f;  // Palm tree 1
	TreeSetPos[0][4].z = 28.25f;  // Palm tree 1
	WriteData((float*)0x00719461, 184.88f); // EC Transporter X
	WriteData((float*)0x0071945C, 3.0f); // EC Transporter Y
	WriteData((float*)0x00719457, 107.09f); // EC Transporter Z
	WriteData((float*)0x00719442, 161.91f); // MR Transporter X
	WriteData((float*)0x0071943D, 3.0f); // MR Transporter Y
	WriteData((float*)0x00719438, 127.91f); // MR Transporter Z
	WriteData((float*)0x0072AFF0, 59.5f); // Black market door X (collision)
	WriteData((float*)0x0072AE8E, 59.5f); // Black market door X
	WriteData((float*)0x0072AE87, 145.0f); // Black market door Z
	WriteData((float*)0x0072AFFA, 145.0f); // Black market door Z (collision)
	WriteData<5>((void*)0x007195D1, 0x90); // Kill SADX water
	WriteData<5>((void*)0x0071946E, 0x90); // Kill SADX fountain
	WriteData((float*)0x0071949E, -1000.0f); // Kill hintbox
	WriteData((float*)0x00719496, -1000.0f); // Kill hintbox
}