#include "stdafx.h"
#include "STG06_SkyDeck.h"

static bool ModelsLoaded_STG06;

void DisableMetalStruts(_OBJ_LANDTABLE* landtable)
{
	for (int j = 0; j < landtable->ssCount; j++)
	{
		for (int k = 0; k < landtable->pLandEntry[j].pObject->basicdxmodel->nbMat; ++k)
		{
			if (landtable->pLandEntry[j].pObject->basicdxmodel->mats[k].attr_texId == 62 && landtable->pLandEntry[j].pObject->basicdxmodel->mats[k].attrflags & NJD_FLAG_USE_ALPHA)
				landtable->pLandEntry[j].pObject->basicdxmodel->mats[k].attrflags &= ~NJD_FLAG_USE_ALPHA;
		}
	}
}

void Bg_Skydeck_kumo_DrawObject_r(NJS_OBJECT* obj, NJS_TEX* uv, int uv_count, float depth)
{
	NJS_OBJECT *v3; // ebx
	NJS_MODEL_SADX *v4; // ebp
	NJS_MESHSET_SADX *v5; // ebx
	NJS_TEX *v6; // eax

	v3 = (NJS_OBJECT *)late_alloca(0x34);
	if (v3)
	{
		memcpy(v3, obj, sizeof(NJS_OBJECT));
		v4 = (NJS_MODEL_SADX *)late_alloca(44);
		if (v4)
		{
			memcpy(v4, obj->model, sizeof(NJS_MODEL_SADX));
			v3->model = v4;
			v5 = (NJS_MESHSET_SADX *)late_alloca(0x1C * v4->nbMeshset);
			if (v5)
			{
				memcpy(v5, obj->basicdxmodel->meshsets, sizeof(NJS_MESHSET_SADX));
				v4->meshsets = v5;
				v6 = (NJS_TEX *)late_alloca(4 * uv_count);
				if (v6)
				{
					memcpy(v6, uv, 4 * uv_count);
					v5->vertuv = v6;
					late_z_ofs___ = depth;
					late_DrawObject(v3, LATE_MAT);
					late_z_ofs___ = 0.0f;
				}
			}
		}
	}
}

void SkyDeckSky1(NJS_OBJECT* obj, NJS_TEX* uv, int uv_count)
{
	float depth = obj == &object_ecsc_skykumo_scroal ? 42000.0f : -36000.0f;
	Bg_Skydeck_kumo_DrawObject_r(obj, uv, uv_count, depth);
}

void SkyDeckSky2(NJS_OBJECT* obj, NJS_TEX* uv, int uv_count)
{
	Bg_Skydeck_kumo_DrawObject_r(obj, uv, uv_count, -38000.0f);
}

void SkyDeckSky3(NJS_OBJECT* obj, NJS_TEX* uv, int uv_count)
{
	Bg_Skydeck_kumo_DrawObject_r(obj, uv, uv_count, -36000.0f);
}

void RenderSmallCloud(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = EC_posy > 300.0f ? -46000.0f : -6000.0f;
	late_DrawObjectClip(a1, a2, a3);
	late_z_ofs___ = 0.0f;
}

void FixHangA(NJS_OBJECT *obj, float scale)
{
	late_DrawObjectClip(obj, LATE_WZ, scale);
}

void RenderCrane(NJS_OBJECT* obj, float scale)
{
	DrawObjectClipMesh(obj, LATE_WZ, scale);
}

void DrawDecalHook(NJS_OBJECT *a1)
{
	late_z_ofs___ = -47952.0f;
	late_DrawObjectClip(a1, LATE_NO, 1.0f);
	late_z_ofs___ = 0.0f;
}

void ObjectSkydeck_com_Display_r(task* tp)
{
	taskwk* twp; // r31
	NJS_OBJECT* obj; // r29
	float frame; // fp13

	twp = tp->twp;
	obj = com_objtbl[4 * twp->smode];
	if (!obj)
		return;
	if (!loop_count)
	{
		njSetTexture(&texlist_obj_skydeck);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		if (twp->ang.z)
			njRotateZ(0, twp->ang.z);
		if (twp->ang.x)
			njRotateX(0, twp->ang.x);
		if (twp->ang.y)
			njRotateY(0, twp->ang.y);
		if (twp->scl.x != 1.0f)
		{
			njScale(0, twp->scl.x, twp->scl.x, twp->scl.x);
			dsScaleLight(twp->scl.x);
		}
		if (twp->btimer)
		{
			frame = twp->scl.z + 0.5f * g_CurrentFrame;
			if (frame >= com_anmcnttbl[twp->btimer] - 1.0f)
				frame = 0.0f;
			twp->scl.z = frame;
			late_DrawMotionClipEx(obj, com_anmtbl[twp->btimer], twp->scl.z, LATE_WZ, twp->scl.x);
		}
		else
		{
			late_DrawObjectClipEx(obj, LATE_WZ, twp->scl.x);
		}
		if (twp->scl.x != 1.0f)
			dsReScaleLight();
		njPopMatrix(1u);
	}
	ObjectSkydeck_Shadow_Display(tp, com_objtbl[4 * twp->smode + 2]);
}

void SkyDeckObjectShadow(NJS_OBJECT *a1, LATE flg, float scl)
{
	late_z_ofs___ = -38000.0f;
	late_DrawObjectClip(a1, flg, scl);
	late_z_ofs___ = 0;
}

// Set material flags necessary for sky additive blending to work
void SetSkyMaterialFlags(NJS_OBJECT* obj)
{
	if (obj->basicdxmodel)
		for (int i = 0; i < obj->basicdxmodel->nbMat; i++)
		{
			obj->basicdxmodel->mats[i].attrflags &= ~(NJD_SA_MASK | NJD_DA_MASK);
			obj->basicdxmodel->mats[i].attrflags |= NJD_DA_ONE | NJD_SA_ONE | NJD_FLAG_USE_ALPHA;
		}
	if (obj->child)
		SetSkyMaterialFlags(obj->child);
	if (obj->sibling)
		SetSkyMaterialFlags(obj->sibling);
}

static Trampoline* TheyForgotToClampAgain_t = nullptr;
static void __cdecl TheyForgotToClampAgain_r(task* a1)
{
	const auto original = TARGET_DYNAMIC(TheyForgotToClampAgain);
	original(a1);
	if (ModConfig::EnabledLevels[LevelIDs_SkyDeck])
		ResetMaterial();
}

void SkyDeck_Init()
{
	ReplaceSET("SET0600M");
	ReplaceSET("SET0600S");
	ReplaceSET("SET0601M");
	ReplaceSET("SET0601S");
	ReplaceSET("SET0602K");
	ReplaceSET("SET0602M");
	ReplaceSET("SET0602S");
	ReplaceCAM("CAM0600M");
	ReplaceCAM("CAM0600S");
	ReplaceCAM("CAM0601S");
	ReplaceCAM("CAM0602K");
	ReplaceCAM("CAM0602S");
	ReplacePVM("E_AIRCRAFT");
	ReplacePVM("OBJ_SKYDECK");
	ReplacePVM("SKYDECK01");
	ReplacePVM("SKYDECK02");
	ReplacePVM("SKYDECK03");
	ReplacePVM("AIR_SIGNAL");
	ReplacePVR("SKY_H_BAL01");
	// Fog/skybox data
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg06[0][i].f32StartZ = 4000.0f;
		pFogTable_Stg06[0][i].f32EndZ = 12000.0f;
		pFogTable_Stg06[0][i].Col = 0xFF000000;

		pFogTable_Stg06[1][i].f32StartZ = 4000.0f;
		pFogTable_Stg06[1][i].f32EndZ = 12000.0f;
		pFogTable_Stg06[1][i].Col = 0xFF000000;

		pFogTable_Stg06[2][i].f32StartZ = 4000.0f;
		pFogTable_Stg06[2][i].f32EndZ = 12000.0f;

		pClipMap_Stg06[0][i].f32Far = -20000.0f;
		pClipMap_Stg06[1][i].f32Far = -20000.0f;
		pClipMap_Stg06[2][i].f32Far = -16000.0f;

		pScale_Stg06[2][i].x = 1.0f;
		pScale_Stg06[2][i].y = 1.0f;
		pScale_Stg06[2][i].z = 1.0f;
	}
}

void SkyDeck_Load()
{
	LevelLoader(LevelAndActIDs_SkyDeck1, "SYSTEM\\data\\stg06_skydeck\\landtable0600.c.sa1lvl", &texlist_skydeck01);
	LevelLoader(LevelAndActIDs_SkyDeck2, "SYSTEM\\data\\stg06_skydeck\\landtable0601.c.sa1lvl", &texlist_skydeck02, &objLandTable0601); // Pointer set because the landtable is accessed directly when the wing falls off
	LevelLoader(LevelAndActIDs_SkyDeck3, "SYSTEM\\data\\stg06_skydeck\\landtable0602.c.sa1lvl", &texlist_skydeck03);
	DisableMetalStruts(GetLevelLandtable(LevelAndActIDs_SkyDeck1));
	DisableMetalStruts(GetLevelLandtable(LevelAndActIDs_SkyDeck2));
	DisableMetalStruts(GetLevelLandtable(LevelAndActIDs_SkyDeck3));
	if (!ModelsLoaded_STG06)
	{
		// Sky materials
		SetSkyMaterialFlags(&object_sky_bg_sora_sora);
		SetSkyMaterialFlags(&object_ecsc_sky_sky);
		// Sky depth fixes
		WriteCall((void*)0x005ED3BD, SkyDeckSky1); // Top 1
		WriteCall((void*)0x005ED485, SkyDeckSky2); // Top 2
		WriteCall((void*)0x005ED500, SkyDeckSky2); // Bottom 1
		WriteCall((void*)0x005ED53F, SkyDeckSky3); // Bottom 2
		WriteJump(Bg_Skydeck_kumo_DrawObject, Bg_Skydeck_kumo_DrawObject_r);
		WriteCall((void*)0x005ED91C, SkyDeckObjectShadow);
		// Object fixes
		TheyForgotToClampAgain_t = new Trampoline(0x004AA540, 0x004AA547, TheyForgotToClampAgain_r); // Fix global material color getting stuck after rendering some models
		WriteJump(ObjectSkydeck_com_Display, ObjectSkydeck_com_Display_r); // Fix jerky animation for some objects (mostly unused)
		WriteCall((void*)0x005ED72F, RenderSmallCloud); // Depth for small cloud when the sky goes up
		// Some material fixes
		object_saru_air_wing_l.basicdxmodel->mats[7].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Aircraft
		object_saru_air_wing_r.basicdxmodel->mats[7].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Aircraft
		object_air_missle.basicdxmodel->mats[4].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // Some rocket or whatever
		// Lol wtf is this? Disable robot underwear?
		WriteData((float*)0x005F4D20, 1.0f);
		WriteData((float*)0x005F4D28, 1.0f);
		WriteData((float*)0x005F4D30, 1.0f);
		WriteData((float*)0x005F4D38, 1.0f);
		// Alpha rejected explosions/shockwaves
		AddAlphaRejectMaterial(&object_tama04_hat_ta01a_hat_ta01a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_tama04_hat_ta02a_hat_ta02a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_tama04_hat_ta03a_hat_ta03a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_tama04_hat_ta04a_hat_ta04a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_tama_hat_o01a_hat_o01a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_h_bakuha01a_h_bakuha01a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_h_bakuha02a_h_bakuha02a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_h_bakuha03a_h_bakuha03a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_h_bakuha04a_h_bakuha04a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_h_bakuha05a_h_bakuha05a.basicdxmodel->mats[0]);
		// Objects
		AddWhiteDiffuseMaterial(&object_sky_t_ueki_t_ueki.basicdxmodel->mats[1]); // OUeKi
		AddWhiteDiffuseMaterial(&object_sky_t_ueki_t_ueki.basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_sky_t_ueki_t_ueki.basicdxmodel->mats[3]);
		object_sky_obj_erea_obj_erea = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_erea.nja.sa1mdl"); // OSkyEv 1
		object_sky_obj_erec_obj_erec = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_erec.nja.sa1mdl"); // OSkyEv 2
		object_sky_obj_ered_obj_ered = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_ered.nja.sa1mdl"); // OSkyEv 3
		object_sky_ud_houdai_ud_houdai = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_ud_houdai.nja.sa1mdl"); // CannonB
		object_koware04_obj_kotu01q_obj_kotu01q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu01q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu02q_obj_kotu02q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu02q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu03q_obj_kotu03q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu03q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu04q_obj_kotu04q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu04q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu05q_obj_kotu05q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu05q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu06q_obj_kotu06q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu06q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu07q_obj_kotu07q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu07q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu08q_obj_kotu08q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu08q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu09q_obj_kotu09q = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu09q.nja.sa1mdl"); // OConnect0 debris
		object_koware04_obj_kotu02y_obj_kotu02y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu02y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu03y_obj_kotu03y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu03y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu04y_obj_kotu04y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu04y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu05y_obj_kotu05y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu05y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu06y_obj_kotu06y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu06y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu07y_obj_kotu07y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu07y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu08y_obj_kotu08y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu08y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu09y_obj_kotu09y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu09y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu10y_obj_kotu10y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu10y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu11y_obj_kotu11y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu11y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu12y_obj_kotu12y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu12y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu13y_obj_kotu13y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu13y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu14y_obj_kotu14y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu14y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu15y_obj_kotu15y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu15y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu16y_obj_kotu16y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu16y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu17y_obj_kotu17y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu17y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu18y_obj_kotu18y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu18y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu19y_obj_kotu19y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu19y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu20y_obj_kotu20y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu20y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu21y_obj_kotu21y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu21y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu22y_obj_kotu22y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu22y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu23y_obj_kotu23y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu23y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu24y_obj_kotu24y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu24y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu25y_obj_kotu25y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu25y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu26y_obj_kotu26y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu26y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu27y_obj_kotu27y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu27y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu28y_obj_kotu28y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu28y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu29y_obj_kotu29y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu29y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu30y_obj_kotu30y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu30y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu31y_obj_kotu31y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu31y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu32y_obj_kotu32y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu32y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu33y_obj_kotu33y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu33y.nja.sa1mdl"); // OTalap0 debris
		object_koware04_obj_kotu34y_obj_kotu34y = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\koware04_obj_kotu34y.nja.sa1mdl"); // OTalap0 debris
		object_sky_stop_stop = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_stop.nja.sa1mdl"); // Stop
		// ODecal
		AddAlphaRejectMaterial(&object_sky_yajirusi_yajirusi.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_yaji_a_yaji_a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_yaji_b_yaji_b.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_yaji_c_yaji_c.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_mark_a_mark_a.basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_sky_mark_b_mark_b.basicdxmodel->mats[0]);
		WriteCall((void*)0x005F2399, DrawDecalHook);
		object_sky_ud_houdai_ud_houdai = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_ud_houdai.nja.sa1mdl"); // Cannon in Act 1
		object_hdai5_l_houdai_l_houdai = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\hdai5_l_houdai.nja.sa1mdl"); // Cannon in Act 2
		object_sky_obj_6jiasi_obj_6jiasi = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_6jiasi.nja.sa1mdl"); // Rail platform 1
		object_sky_obj_45asi_obj_45asi = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_45asi.nja.sa1mdl"); // Rail platform 2
		object_sky_obj_9jiasi_obj_9jiasi = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_9jiasi.nja.sa1mdl"); // Rail platform 3
		object_sky_obj_maruyoko_obj_maruyoko = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_maruyoko.nja.sa1mdl"); // Rail platform 4
		object_sky_obj_tlp_obj_tlp = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_tlp.nja.sa1mdl"); // Rail platform 5
		object_sky_obj_tunagi_obj_tunagi = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_tunagi.nja.sa1mdl"); // OConnect_0
		object_sky_obj_turokado_obj_turokado = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_turokado.nja.sa1mdl"); // Curved platform
		object_sky_obj_robo_a_obj_robo_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_robo_a.nja.sa1mdl"); // Top0
		object_sky_obj_turo_b_obj_turo_b = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_turo_b.nja.sa1mdl"); // Stairs 1
		object_sky_obj_turo_a_obj_turo_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_turo_a.nja.sa1mdl"); // Stairs 2
		object_skymain_kr_skymain_kr = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skymain_kr.nja.sa1mdl"); // Crane
		WriteCall((void*)0x005F2DD6, RenderCrane);
		object_skymain_kr_skymain_kr.child->basicdxmodel->mats[1].attrflags |= NJD_FLAG_USE_TEXTURE;
		object_skymain_kr_skymain_kr.child->basicdxmodel->mats[1].attr_texId = 6;
		object_skymain_kr_skymain_kr.child->sibling->basicdxmodel->mats[1].attrflags |= NJD_FLAG_USE_TEXTURE;
		object_skymain_kr_skymain_kr.child->sibling->basicdxmodel->mats[1].attr_texId = 6;
		object_sky_obj_kontena_obj_kontena = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_kontena.nja.sa1mdl"); // Trolley thing or whatever that is
		object_sky_obj_kazarikon_obj_kazarikon = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_kazarikon.nja.sa1mdl"); // Platform2
		object_eg_under_a_under_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\eg_under_a.nja.sa1mdl"); // Another big object
		object_skytop_buhin_ana_buhin_ana = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_ana.nja.sa1mdl"); // OAnaA
		object_skytop_buhin_a_buhin_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_a.nja.sa1mdl"); // ORaneA
		object_sky_obj_hasira_l_obj_hasira_l = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_hasira_l.nja.sa1mdl"); // Pole_L
		object_sky_obj_hasira_s_obj_hasira_s = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_hasira_s.nja.sa1mdl"); // Pole_S
		object_sky_obj_etc_obj_etc = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_etc.nja.sa1mdl"); // Cyl_S (different hierarchy in DX because of collision maybe?)
		object_sky_obj_marumoto_obj_marumoto = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_marumoto.nja.sa1mdl"); // Cyl_0
		object_sky_obj_maruasi_a_obj_maruasi_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_maruasi_a.nja.sa1mdl"); // Cyl_1
		object_sky_obj_maru2asi_obj_maru2asi = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_maru2asi.nja.sa1mdl"); // Cyl_2
		object_sky_obj_maru_c_obj_maru_c = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_obj_maru_c.nja.sa1mdl"); // BaseL
		object_sky_duct_duct = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_duct.nja.sa1mdl"); // Duct_0
		object_sky_m_houdai_m_houdai = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\sky_m_houdai.nja.sa1mdl"); // Cannon_M
		// Lever white material color fix
		object_sky_s_levar_s_levar.basicdxmodel->mats[8].attrflags |= NJD_FLAG_USE_TEXTURE;
		object_sky_s_levar_s_levar.basicdxmodel->mats[8].attr_texId = 6;
		object_skytop_buhin_c_buhin_c = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_c.nja.sa1mdl"); // ORaneC
		object_skytop_buhin_d_buhin_d = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_d.nja.sa1mdl"); // ORaneD
		object_skytop_buhin_e_buhin_e = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_e.nja.sa1mdl"); // ORaneE
		object_skytop_buhin_f_buhin_f = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\skytop_buhin_f.nja.sa1mdl"); // ORaneF
		object_eg_top_a_top_a = *LoadModel("system\\data\\stg06_skydeck\\common\\models\\eg_top_a.nja.sa1mdl"); // Huge Egg Carrier decoration (need to sort alpha?)
		WriteCall((void*)0x005EE8C0, FixHangA);
		WriteCall((void*)0x005EE8EC, FixHangA);
		WriteCall((void*)0x005EE919, FixHangA);
		// Clip distance improvements		
		objItemTable06.pObjItemEntry[24].ssAttribute = 1;
		objItemTable06.pObjItemEntry[25].ssAttribute = 1;
		objItemTable06.pObjItemEntry[29].ssAttribute = 1;
		objItemTable06.pObjItemEntry[30].ssAttribute = 1;
		objItemTable06.pObjItemEntry[31].ssAttribute = 1;
		objItemTable06.pObjItemEntry[32].ssAttribute = 1;
		objItemTable06.pObjItemEntry[33].ssAttribute = 1;
		objItemTable06.pObjItemEntry[34].ssAttribute = 1;
		objItemTable06.pObjItemEntry[35].ssAttribute = 1;
		objItemTable06.pObjItemEntry[36].ssAttribute = 1;
		objItemTable06.pObjItemEntry[37].ssAttribute = 1;
		objItemTable06.pObjItemEntry[38].ssAttribute = 1;
		objItemTable06.pObjItemEntry[24].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[25].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[29].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[30].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[31].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[32].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[33].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[34].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[35].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[36].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[37].fRange = 8000000.0f;
		objItemTable06.pObjItemEntry[38].fRange = 3000000.0f;
		ModelsLoaded_STG06 = true;
	}
}

void SkyDeck_OnFrame()
{
	if (ssStageNumber != LevelIDs_SkyDeck)
		return;
	// Set fog
	if (ssActNumber != 2)
	{
		gFog.f32StartZ = 4000.0f - bgcolor_ofs__ * 3000;
		gFog.f32EndZ = 12000.0f - bgcolor_ofs__ * 9000;
		gFog.Col = stg06_fogcolor;
	}
}