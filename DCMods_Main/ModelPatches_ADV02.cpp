#include "stdafx.h"

void PatchModels_ADV02()
{
	HMODULE ADV02MODELS = GetModuleHandle(L"ADV02MODELS");

	// dxpc\adv02_mysticruin\sky\mra_s_sora_hare.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00009030)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00009034)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00009038)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mra_s_sora_yoru.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000B274)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000B278)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000B27C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mra_s_sora_yuu.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000A154)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000A158)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000A15C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrb_s_sora_hare.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000C394)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000C398)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000C39C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrb_s_sora_yoru.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000E5D4)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000E5D8)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000E5DC)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrb_s_sora_yuu.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000D4B4)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000D4B8)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000D4BC)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrc_bf_s_skyhiru.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000F6F4)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x0000F6F8)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrc_bf_s_skyyoru.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00011434)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00011438)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\sky\mrc_bf_s_skyyuu.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00010594)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00010598)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv02_mysticruin\common\models\mra_f_itemdai.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016018) = { 0, -178 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001601C) = { 0, 178 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016020) = { 76, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016024) = { 76, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016028) = { 433, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001602C) = { 433, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016030) = { 509, -178 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00016034) = { 509, 178 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015638) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001563C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015640) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015644) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015648) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001564C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015650) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015654) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015658) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001565C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015660) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015664) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015668) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001566C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015670) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015674) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015678) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001567C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015680) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015684) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015688) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001568C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015690) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015694) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015698) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001569C) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156A0) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156A4) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156A8) = { 494, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156AC) = { 494, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156B0) = { 15, -239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156B4) = { 15, 239 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156B8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156BC) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156C4) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156C8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156CC) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156D4) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156D8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156DC) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156E4) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156E8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156EC) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156F4) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156F8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000156FC) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015704) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015708) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001570C) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015714) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015718) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001571C) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015724) = { 19, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015728) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001572C) = { 494, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00015734) = { 19, 254 };

	// dxpc\adv02_mysticruin\common\models\mra_o_door_2.nja.sa1mdl
		//Model too different

	// dxpc\adv02_mysticruin\common\models\mra_o_torokko.nja.sa1mdl
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x000236E8))->attrflags = 0x9424A400;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x000236FC))->attrflags = 0x94262400;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00023774))->attrflags = 0x9424A400;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00023788))->attrflags = 0x9424A400;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x000237B0))->attrflags = 0x94262400;
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BC8) = { 416, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BCC) = { 97, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BD0) = { 65, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BD4) = { 65, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BD8) = { 33, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BDC) = { 65, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BE0) = { 33, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BE4) = { 350, 229 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BE8) = { 160, 229 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BEC) = { 384, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BF0) = { 129, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BF4) = { 384, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BF8) = { 129, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023BFC) = { 350, 229 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C00) = { 160, 229 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C04) = { 384, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C08) = { 129, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C0C) = { 384, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C10) = { 129, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C14) = { 453, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C18) = { 56, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C1C) = { 480, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C20) = { 33, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C24) = { 480, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C28) = { 33, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C2C) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C34) = { 509, 128 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C38) = { 3, 128 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C3C) = { 477, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C40) = { 31, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C44) = { 448, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C48) = { 480, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C4C) = { 448, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C50) = { 480, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C54) = { 416, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C58) = { 448, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C5C) = { 65, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C60) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C64) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C68) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C6C) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C70) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C74) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C78) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023C7C) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023E98) = { 40, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EA0) = { 469, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EA4) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EA8) = { 469, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EAC) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EB0) = { 40, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EC0) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EC4) = { 510, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023EC8) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023ECC) = { 510, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023F08) = { 67, 187 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023F0C) = { 67, -195 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023F10) = { 450, 187 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023F14) = { 450, -195 };
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00023498))->attrflags = 0x9424A400;
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234E0) = { 477, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234E4) = { 508, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234E8) = { 31, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234EC) = { 1, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234F0) = { 480, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234F4) = { 448, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234F8) = { 448, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000234FC) = { 448, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023500) = { 400, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023504) = { 448, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023508) = { 97, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002350C) = { 65, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023510) = { 33, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023514) = { 81, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023518) = { 33, 129 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002351C) = { 432, 230 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023520) = { 479, 128 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023524) = { 65, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023528) = { 97, 206 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002352C) = { 33, 150 };
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00023290))->attrflags = 0x9424A400;
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000232F0) = { 33, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000232F4) = { 65, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000232F8) = { 33, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000232FC) = { 65, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023300) = { 31, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023304) = { 477, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023308) = { 33, 129 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002330C) = { 479, 128 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023310) = { 448, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023314) = { 480, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023318) = { 448, 126 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002331C) = { 480, 150 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023320) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00023328) = { 508, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002332C) = { 1, 127 };

	// dxpc\adv02_mysticruin\common\models\mra_o_zenrin.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253D0) = { 67, 123 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253D4) = { 131, 59 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253D8) = { 131, 187 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253DC) = { 195, 123 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253E0) = { 67, 123 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253E4) = { 131, 187 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253E8) = { 131, 59 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253EC) = { 195, 123 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253F0) = { 127, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253F4) = { 254, -765 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253F8) = { 254, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000253FC) = { 127, -765 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00025400) = { 127, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00025404) = { 0, -765 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002540C) = { 127, -765 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00025410) = { 127, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00025414) = { 254, -765 };

	// dxpc\adv02_mysticruin\common\models\mrb_cl_f_hasigeta.nja.sa1mdl
		// Collision
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x00059FD8) = { 77.82511f, 56.917183f, 77.79544f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x00059FFC) = { 102.40358f, 134.6164f, 38.700764f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A014) = { 102.231995f, 134.63588f, -42.702915f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A02C) = { 77.814606f, 56.908455f, -27.008553f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A050) = { 65.375824f, 34.560303f, 51.110043f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A05C) = { 65.36586f, 34.56024f, -43.1318f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A068) = { 89.02496f, 91.668755f, -40.0517f };
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0005A074) = { 89.11603f, 91.6634f, 51.747784f };

	// dxpc\adv02_mysticruin\common\models\mrb_f_doorice.nja.sa1mdl
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00028CD0))->diffuse.color = 0x7FFFFFFF;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x0002877C))->diffuse.color = 0x7FFFFFFF;
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00028018))->diffuse.color = 0x7FFFFFFF;
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00028254) = { 0, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00028258) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002825C) = { 510, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00028260) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00028264) = { 510, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002826C) = { 0, -254 };

	// dxpc\adv02_mysticruin\common\models\mrb_f_kagidaiice.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002D988) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002D98C) = { 509, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002D990) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002D994) = { 0, 254 };

	// dxpc\adv02_mysticruin\common\models\mrb_f_keyice.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00029C20) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00029C24) = { 0, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00029C28) = { 509, -255 };

	// dxpc\adv02_mysticruin\common\models\mrb_f_mihari.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E07C) = { 412, 3 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E080) = { 510, 104 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E084) = { 97, 4 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E088) = { 510, 104 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E08C) = { 399, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E090) = { 97, 4 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E094) = { 110, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E09C) = { 399, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0A0) = { 510, 59 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0A4) = { 110, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0A8) = { 412, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0AC) = { 0, 59 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0B0) = { 97, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0BC) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0C0) = { 23, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0C4) = { 486, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0CC) = { 509, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0D0) = { 15, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0D4) = { 485, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0DC) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0E0) = { 23, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0E4) = { 486, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0EC) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0F0) = { 23, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0F4) = { 486, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E0FC) = { 509, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E100) = { 15, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001E104) = { 485, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DD9C) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDA0) = { 459, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDA4) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDA8) = { 50, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDAC) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDB0) = { 459, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDB4) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDB8) = { 50, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDBC) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDC0) = { 459, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDC4) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDC8) = { 50, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDCC) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001DDD0) = { 459, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D790) = { 446, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D794) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D798) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D79C) = { 254, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7A0) = { 446, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7A4) = { 446, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7A8) = { 63, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7AC) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7B0) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7B4) = { 254, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D7B8) = { 63, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D80C) = { 382, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D810) = { 127, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D814) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D818) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D81C) = { 382, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D820) = { 127, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D824) = { 382, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D828) = { 127, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D82C) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D830) = { 0, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D834) = { 382, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D838) = { 127, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D38C) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D390) = { 509, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D394) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D398) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D0D8) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D0DC) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D0E0) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001D0E4) = { 509, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001C7E8) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001C7EC) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001C7F0) = { 509, 7 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001C7F4) = { 509, 254 };

	// dxpc\adv02_mysticruin\common\models\mrc_cl_s_feturoa.nja.sa1mdl
		//Collision

	// dxpc\adv02_mysticruin\common\models\mrc_s_feturoa.nja.sa1mdl
		//Edited manually
	/**(NJS_TEX*)((size_t)ADV02MODELS + 0x000852B8) = { 510, -2295 };
	/*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852BC) = { 0, -2295 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852C0) = { 509, -1924 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852C4) = { 0, -1958 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852C8) = { 509, -1384 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852CC) = { 0, -1360 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852D0) = { 510, -651 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852D4) = { 0, -676 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852D8) = { 509, -113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852DC) = { 0, -79 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852E0) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852E4) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852E8) = { 3, -1263 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852EC) = { 3, -139 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852F0) = { 506, -474 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852F4) = { 3, 75 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852F8) = { 3, -1263 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000852FC) = { 506, -474 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085300) = { 3, -139 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085304) = { 3, 75 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085308) = { 20400, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085310) = { 19081, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085318) = { 16984, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085324) = { 16984, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008532C) = { 14405, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085334) = { 11853, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008533C) = { 9477, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085344) = { 7114, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008534C) = { 4986, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085354) = { 3130, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008535C) = { 1501, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085364) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008536C) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085374) = { 128, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0008537C) = { 1518, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085384) = { 3730, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085388) = { 6448, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085390) = { 21674, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00085398) = { 20088, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853A0) = { 18365, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853A8) = { 16397, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853B0) = { 14143, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853B8) = { 11647, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853C0) = { 9138, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000853C8) = { 6448, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855A0) = { 0, -71 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855A4) = { 765, -71 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855A8) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855AC) = { 765, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855B8) = { 0, 71 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855BC) = { 764, 71 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855C0) = { 0, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000855C4) = { 765, -255 };*/

	// dxpc\adv02_mysticruin\common\models\mrc_s_fe_dodai.nja.sa1mdl
		//Edited manually
	/**(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDA0) = { 475, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDA4) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDA8) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDAC) = { 475, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDB0) = { 382, 220 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDB4) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDB8) = { 255, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDBC) = { 127, 220 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDC0) = { 127, 220 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDC4) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EDC8) = { 34, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEA0) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEA4) = { 382, 34 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEA8) = { 254, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEAC) = { 475, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEB0) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEB8) = { 34, 127 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEBC) = { 254, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEC0) = { 127, 34 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007EEC4) = { 254, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DBC8) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DBD0) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DBD4) = { 509, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DC00) = { 382, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DC04) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007DC08) = { 127, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9C0) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9C4) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9CC) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9D0) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9D4) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D9D8) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D83C) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D840) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D844) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D848) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D84C) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D850) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D854) = { 509, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D858) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D520) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D528) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D52C) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D55C) = { 128, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D560) = { 510, 256 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D564) = { 383, 1 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D318) = { 3, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D31C) = { 506, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D320) = { 3, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D324) = { 506, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D328) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D32C) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D334) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D194) = { 3, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D198) = { 506, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D19C) = { 3, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D1A0) = { 506, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D1A4) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D1A8) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007D1B0) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CE78) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CE80) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CE84) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CEB4) = { 127, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CEB8) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CEBC) = { 382, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC70) = { 482, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC74) = { 27, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC78) = { 482, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC7C) = { 27, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC80) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC84) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CC88) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CAEC) = { 482, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CAF0) = { 27, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CAF4) = { 482, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CAF8) = { 27, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CAFC) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CB00) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007CB04) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C7D0) = { 127, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C7D8) = { 382, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C7DC) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C808) = { 382, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C80C) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C810) = { 127, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5C8) = { 508, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5CC) = { 1, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5D0) = { 508, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5D4) = { 1, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5D8) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5DC) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5E0) = { 0, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C5E4) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C440) = { 509, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C444) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C448) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C450) = { 18, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C454) = { 491, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C458) = { 18, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0007C45C) = { 491, 255 };*/

	// dxpc\adv02_mysticruin\common\models\mrc_s_treea.nja.sa1mdl
		//Model too different

	// dxpc\adv02_mysticruin\common\models\mrc_s_yasi.nja.sa1mdl
		//Model too different

	// dxpc\adv02_mysticruin\common\models\mri_k_cuhiza.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C270) = { 509, -254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C274) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C278) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C27C) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C280) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C284) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C288) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C28C) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C294) = { 509, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C29C) = { 510, 0 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C05C) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C060) = { 0, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C064) = { 509, -1020 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0003C068) = { 0, -1020 };

	// dxpc\adv02_mysticruin\common\models\mri_k_uraguti.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrj_n_ayasijimen.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017380) = { 126, -113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017384) = { -6, 248 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017388) = { 126, 113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001738C) = { 516, 248 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017390) = { 371, 113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017394) = { 516, -233 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00017398) = { 371, -113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0001739C) = { -6, -233 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000173A0) = { 126, -113 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000173A4) = { -6, 248 };

	// dxpc\adv02_mysticruin\common\models\mrj_n_bigkusanul.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrj_n_eggtobira.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021170) = { 211, 99 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021174) = { 211, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021178) = { 199, 87 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002117C) = { 190, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021180) = { 107, 87 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021184) = { 115, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021188) = { 95, 99 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002118C) = { 95, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021190) = { 1, 5 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021194) = { 1, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021198) = { 162, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002119C) = { 109, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211A0) = { 109, 145 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211A4) = { 138, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211A8) = { 138, 99 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211AC) = { 167, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211B0) = { 167, 99 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211B4) = { 197, 191 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211B8) = { 197, 145 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211BC) = { 162, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211C0) = { 1, 253 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211C4) = { 1, 5 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211F0) = { 510, 3 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211F4) = { 339, 3 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211F8) = { 339, 254 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x000211FC) = { 170, 3 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00021200) = { 170, 254 };

	// dxpc\adv02_mysticruin\common\models\mrj_n_hideswich.nja.sa1mdl
		//Same maybe?
	
	// dxpc\adv02_mysticruin\common\models\mrj_n_jibakudai_b.nja.sa1mdl
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B038) = { -2.2603579f, 5.002536f, 1.3038449f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B044) = { 0, 5.002536f, 2.607688f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B050) = { 2.2603588f, 5.002536f, 1.303844f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B05C) = { 2.2603588f, 5.002536f, -1.3038449f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B068) = { -0.00001f, 5.002536f, -2.607689f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B074) = { -2.2603588f, 5.002536f, -1.303844f };
	*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0001B080) = { 0, 5.002536f, 0 };

	// dxpc\adv02_mysticruin\common\models\mrj_n_kakusicolib.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrj_n_stobirawaku.nja.sa1mdl
		//Same maybe?
	
	// dxpc\adv02_mysticruin\common\models\mrj_n_sunadaiza_a.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002264C) = { 51, -203 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022650) = { 51, 203 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022654) = { 458, -203 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022658) = { 458, 203 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x0002265C) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022660) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022664) = { 0, -255 };

	// dxpc\adv02_mysticruin\common\models\mrj_n_sunadaiza_b.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F7C) = { 433, 188 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F80) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F84) = { 433, -193 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F88) = { 510, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F8C) = { 76, -193 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F90) = { 0, -255 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F94) = { 76, 188 };
	*(NJS_TEX*)((size_t)ADV02MODELS + 0x00022F9C) = { 433, 188 };

	// dxpc\adv02_mysticruin\common\models\mrobj_cl_entiwa.nja.sa1mdl
		//Collision
	//((NJS_MATERIAL*)(size_t)ADV02MODELS + 0x00012434)->attrflags = 0x95382400;

	// dxpc\adv02_mysticruin\common\models\mrobj_mast_b.nja.sa1mdl
	// Weird broken shit
	//*(NJS_VECTOR*)((size_t)ADV02MODELS + 0x0007C384) = { -10, 10, 0 };

	// dxpc\adv02_mysticruin\common\models\mrobj_nc_iwaent_a.nja.sa1mdl
		//Same?
	
	// dxpc\adv02_mysticruin\common\models\mrobj_nc_iwaent_b.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrobj_s_bliwb.nja.sa1mdl
		//Model too different

	// dxpc\adv02_mysticruin\common\models\mrobj_s_egggdum.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrobj_s_kasoyasi.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrobj_s_kassort.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrobj_train.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\mrobj_train_cl.nja.sa1mdl
		//Collision

	// dxpc\adv02_mysticruin\common\models\suna1_suna.nja.sa1mdl
	((NJS_MATERIAL*)((size_t)ADV02MODELS + 0x00087510))->attrflags = 0x94282400;

	// dxpc\adv02_mysticruin\common\models\tanken_a.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\tanken_c.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\common\models\tanken_d.nja.sa1mdl
		//Full replacement

	// dxpc\adv02_mysticruin\killcolli\act1\otionly_mra51.nja.sa1mdl
	*(NJS_VECTOR*)0x011039F8 = { 70.631516f, -79.064384f, 730.3276f };
	*(NJS_VECTOR*)0x01103A28 = { 89.519135f, -79.064384f, 2665.614f };
	*(NJS_VECTOR*)0x01103A34 = { 886.2314f, -70.42911f, 962.112f };
	*(NJS_VECTOR*)0x01103A40 = { 126.64168f, -79.064384f, 1169.5472f };
	*(NJS_VECTOR*)0x01103A4C = { 781.1413f, -70.42911f, 2691.3298f };

	// dxpc\adv02_mysticruin\killcolli\act1\otionly_mra52.nja.sa1mdl
	*(NJS_VECTOR*)0x01103B6C = { 665.56635f, 90.799866f, 849.89655f };
	*(NJS_VECTOR*)0x01103B78 = { 456.05353f, 90.799866f, 2664.1423f };
	*(NJS_VECTOR*)0x01103B84 = { 3058.563f, 90.79779f, 612.7759f };
	*(NJS_VECTOR*)0x01103B90 = { 3097.1575f, 90.79779f, 2818.5461f };

	// dxpc\adv02_mysticruin\killcolli\act1\otionly_mra53.nja.sa1mdl
	*(NJS_VECTOR*)0x01103C64 = { 968.97076f, -49.523006f, 326.02533f };
	*(NJS_VECTOR*)0x01103C70 = { 3091.8062f, -49.523006f, 1373.7677f };
	*(NJS_VECTOR*)0x01103C7C = { 3097.3193f, -49.523006f, -225.41803f };

	// dxpc\adv02_mysticruin\killcolli\act2\otijini_mrb54.nja.sa1mdl
	*(NJS_VECTOR*)0x01103D7C = { 1486.9718f, -687.98206f, 2415.095f };
	*(NJS_VECTOR*)0x01103D88 = { -27.576626f, -687.98206f, 3793.5679f };
	*(NJS_VECTOR*)0x01103D94 = { -2094.935f, -792.00543f, 2839.2405f };
}