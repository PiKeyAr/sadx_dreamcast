#include "stdafx.h"

void PatchModels_STG05()
{
	// dxpc\stg05_mountain\common\models\hasi_hasiita.nja.sa1mdl
		//Model too different

	// dxpc\stg05_mountain\common\models\hasi_kori1a.nja.sa1mdl
		//Collision

	// dxpc\stg05_mountain\common\models\hasi_kori2a.nja.sa1mdl
	((NJS_MATERIAL*)0x02467700)->attr_texId = 0;
	((NJS_MATERIAL*)0x02467700)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x02467700)->exponent = 6;
	//((NJS_MATERIAL*)0x02467700)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x02467700)->specular.color = 0xFFFFFFFF;

	// dxpc\stg05_mountain\common\models\hasi_kori3a.nja.sa1mdl
	((NJS_MATERIAL*)0x02468660)->attr_texId = 0;
	((NJS_MATERIAL*)0x02468660)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x02468660)->exponent = 6;
	//((NJS_MATERIAL*)0x02468660)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x02468660)->specular.color = 0xFFFFFFFF;

	// dxpc\stg05_mountain\common\models\hasi_kori4a.nja.sa1mdl
	((NJS_MATERIAL*)0x02469480)->attr_texId = 0;
	((NJS_MATERIAL*)0x02469480)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x02469480)->exponent = 6;
	//((NJS_MATERIAL*)0x02469480)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x02469480)->specular.color = 0xFFFFFFFF;

	// dxpc\stg05_mountain\common\models\hasi_piler1a.nja.sa1mdl
	*(NJS_TEX*)0x0246A2C0 = { 1004, -247 };
	*(NJS_TEX*)0x0246A2C4 = { 15, -247 };
	*(NJS_TEX*)0x0246A2C8 = { 1020, -152 };
	*(NJS_TEX*)0x0246A2CC = { 0, -152 };
	*(NJS_TEX*)0x0246A2D0 = { 1020, -50 };
	*(NJS_TEX*)0x0246A2D4 = { 0, -50 };
	*(NJS_TEX*)0x0246A2D8 = { 1020, 51 };
	*(NJS_TEX*)0x0246A2DC = { 0, 51 };
	*(NJS_TEX*)0x0246A2E0 = { 1020, 153 };
	*(NJS_TEX*)0x0246A2E4 = { 0, 153 };
	*(NJS_TEX*)0x0246A2E8 = { 1004, 247 };
	*(NJS_TEX*)0x0246A2EC = { 15, 247 };

	// dxpc\stg05_mountain\common\models\hasi_piler2a.nja.sa1mdl
	*(NJS_TEX*)0x0246A7C0 = { 1004, -247 };
	*(NJS_TEX*)0x0246A7C4 = { 15, -247 };
	*(NJS_TEX*)0x0246A7C8 = { 1020, -152 };
	*(NJS_TEX*)0x0246A7CC = { 0, -152 };
	*(NJS_TEX*)0x0246A7D0 = { 1020, -50 };
	*(NJS_TEX*)0x0246A7D4 = { 0, -50 };
	*(NJS_TEX*)0x0246A7D8 = { 1020, 51 };
	*(NJS_TEX*)0x0246A7DC = { 0, 51 };
	*(NJS_TEX*)0x0246A7E0 = { 1020, 153 };
	*(NJS_TEX*)0x0246A7E4 = { 0, 153 };
	*(NJS_TEX*)0x0246A7E8 = { 1004, 247 };
	*(NJS_TEX*)0x0246A7EC = { 15, 247 };

	// dxpc\stg05_mountain\common\models\kaiten_kaiten.nja.sa1mdl
		//Full replacement

	// dxpc\stg05_mountain\common\models\ki01_ki01.nja.sa1mdl
		//Full replacement

	// dxpc\stg05_mountain\common\models\setdai_asiba.nja.sa1mdl
		//Full replacement

	// dxpc\stg05_mountain\common\models\tate_hago.nja.sa1mdl
		//Full replacement

	// dxpc\stg05_mountain\common\models\ukisima_ukisima1a.nja.sa1mdl
		//Model too different

	// dxpc\stg05_mountain\common\models\untei_6pon_6pon.nja.sa1mdl
		//Model too different
}