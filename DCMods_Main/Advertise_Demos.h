#pragma once

#include <SADXModLoader.h>

__int16 SA1DemoArray[12][4] = {
	{ LevelIDs_EggCarrierOutside, 2, Characters_Sonic, 27 }, // Sonic defeats Gamma
	{ LevelIDs_SpeedHighway, 0, Characters_Sonic, -1 }, // Sonic's Speed Highway gameplay
	{ LevelIDs_MysticRuins, 0, Characters_Sonic, 13 }, // Sonic hops onto the Tornado 1
	{ LevelIDs_IceCap, 2, Characters_Tails, -1 }, // Tails' Ice Cap gameplay
	{ LevelIDs_MysticRuins, 0, Characters_Sonic, 40 }, // Sonic vs Knuckles
	{ LevelIDs_RedMountain, 2, Characters_Knuckles, -1 }, // Knuckles' Red Mountain gameplay
	{ LevelIDs_StationSquare, 1, Characters_Sonic, 18 }, // Sonic meets Amy
	{ LevelIDs_HotShelter, 0, Characters_Amy, -1 }, // Amy's Hot Shelter gameplay
	{ LevelIDs_EggCarrierInside, 1, Characters_Gamma, 183 }, // The hunt for Froggy
	{ LevelIDs_WindyValley, 0, Characters_Gamma, -1 }, // Gamma's Windy Valley gameplay
	{ LevelIDs_EmeraldCoast, 2, Characters_Big, 212 }, // Big loses Froggy to Gamma
	{ LevelIDs_EmeraldCoast, 2, Characters_Big, -1 }, // Big's Emerald Coast gameplay
};