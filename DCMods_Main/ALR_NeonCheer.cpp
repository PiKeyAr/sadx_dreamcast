#include "stdafx.h"

// Cheer Chao hologram in Chao Stadium

void ChaoCheerHologram_Display(task* tp)
{
	taskwk* twp = tp->twp;
	njSetTexture(&texlist_obj_al_race);
	njPushMatrix(0);
	object_entranse_nc_chao_nc_chao.basicdxmodel->mats[0].attr_texId = tp->twp->btimer ? 34 : 35;
	late_DrawObjectClip(&object_entranse_nc_chao_nc_chao, LATE_LIG, 1.0f);
	njPopMatrix(1u);
}

void ChaoCheerHologram_Main(task* tp)
{
	if (ulGlobalTimer % (35 / g_CurrentFrame) == 0)
		tp->twp->btimer = !tp->twp->btimer;
	ChaoCheerHologram_Display(tp);
}

void ChaoCheerHologram_Load(ObjectMaster* om)
{
	task* tp = (task*)om;
	tp->exec = ChaoCheerHologram_Main;
	tp->disp = ChaoCheerHologram_Main;
	tp->dest = FreeTask;
}