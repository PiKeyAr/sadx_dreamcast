#include "stdafx.h"

static bool ModelsLoaded_Animals;

void QueueAnimals(NJS_ACTION* action, Float frame)
{
	late_z_ofs___ = -47952.0f;
	late_ActionEx(action, frame, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

void QueueChaoAnimals1(NJS_ACTION_LINK* a1, float a2)
{
	late_z_ofs___ = -20000.0f;
	late_ActionLinkEx(a1, a2, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

void QueueChaoAnimals2(NJS_OBJECT* a1, NJS_MOTION* a2, float a3)
{
	late_z_ofs___ = -20000.0f;
	late_DrawMotionClipEx(a1, a2, a3, LATE_WZ, 1.0f);
	late_z_ofs___ = 0.0f;
}

void AnimalBubbleHook(NJS_OBJECT* a1, LATE a2)
{
	late_z_ofs___ = 4000.0f;
	late_DrawObjectClip(a1, LATE_MAT, 1.0f);
	late_z_ofs___ = 0.0f;
}

void Animals_Init()
{
	ReplacePVR("AL_BARRIA");
	ReplacePVM("BANB");
	ReplacePVM("GOMA");
	ReplacePVM("GORI");
	ReplacePVM("KOAR");
	ReplacePVM("KUJA");
	ReplacePVM("OUM");
	ReplacePVM("LION");
	ReplacePVM("MOGU");
	ReplacePVM("PEN");
	ReplacePVM("RAKO");
	ReplacePVM("SUKA");
	ReplacePVM("TUBA");
	ReplacePVM("USA");
	ReplacePVM("WARA");
	ReplacePVM("ZOU");
	WriteCall((void*)0x004D7718, AnimalBubbleHook); // Animal bubble blending mode + depth
	WriteCall((void*)0x004D769B, QueueAnimals); // Queue animal models because they have transparency
	WriteCall((void*)0x0073F726, QueueChaoAnimals1); // Also queue Chao animals
	WriteCall((void*)0x0073F742, QueueChaoAnimals2); // Also queue Chao animals
}

void Animals_Load()
{
	if (ModelsLoaded_Animals)
		return;
	ArchiveX* AnimalsArchive = new ArchiveX(helperFunctionsGlobal->GetReplaceablePath("system\\data\\a_life\\minimal\\minimal.arcx"));

	// "Wait" and "walk" animations are custom made by retargeting SADX animations to SA1 model hierarchy.
	// "Wait" animations for PEN, USA, WARA and KOAR had one node that didn't work with SA1 hierarchy, so it was removed.
	object_goma_goma_goma = *LoadModel(AnimalsArchive, "goma_goma.nja.sa1mdl");
	motion_goma_move_goma = *LoadAnimation(AnimalsArchive, "goma_move_goma.nam.saanim");
	motion_goma_hand_goma = *LoadAnimation(AnimalsArchive, "goma_hand_goma.nam.saanim");
	motion_goma_meet_goma = *LoadAnimation(AnimalsArchive, "goma_meet_goma.nam.saanim");
	motion_goma_wait_goma = *LoadAnimation(AnimalsArchive, "goma_wait_goma.nam.saanim");

	object_pen_pen_pen = *LoadModel(AnimalsArchive, "pen_pen.nja.sa1mdl");
	motion_pen_move_pen = *LoadAnimation(AnimalsArchive, "pen_move_pen.nam.saanim");
	motion_pen_hand_pen = *LoadAnimation(AnimalsArchive, "pen_hand_pen.nam.saanim");
	motion_pen_meet_pen = *LoadAnimation(AnimalsArchive, "pen_meet_pen.nam.saanim");
	motion_pen_wait_pen = *LoadAnimation(AnimalsArchive, "pen_wait_pen.nam.saanim");

	object_tuba_tuba_tuba = *LoadModel(AnimalsArchive, "tuba_tuba.nja.sa1mdl");
	motion_tuba_move_tuba = *LoadAnimation(AnimalsArchive, "tuba_move_tuba.nam.saanim");
	motion_tuba_hand_tuba = *LoadAnimation(AnimalsArchive, "tuba_hand_tuba.nam.saanim");
	motion_tuba_meet_tuba = *LoadAnimation(AnimalsArchive, "tuba_meet_tuba.nam.saanim");
	motion_tuba_wait_tuba = *LoadAnimation(AnimalsArchive, "tuba_wait_tuba.nam.saanim");
	motion_tuba_walk_tuba = *LoadAnimation(AnimalsArchive, "tuba_walk_tuba.nam.saanim");

	object_kuja_kuja_kuja = *LoadModel(AnimalsArchive, "kuja_kuja.nja.sa1mdl");
	motion_kuja_move_kuja = *LoadAnimation(AnimalsArchive, "kuja_move_kuja.nam.saanim");
	motion_kuja_hand_kuja = *LoadAnimation(AnimalsArchive, "kuja_hand_kuja.nam.saanim");
	motion_kuja_meet_kuja = *LoadAnimation(AnimalsArchive, "kuja_meet_kuja.nam.saanim");
	motion_kuja_wait_kuja = *LoadAnimation(AnimalsArchive, "kuja_wait_kuja.nam.saanim");
	motion_kuja_walk_kuja = *LoadAnimation(AnimalsArchive, "kuja_walk_kuja.nam.saanim");

	object_oum_oum_oum = *LoadModel(AnimalsArchive, "oum_oum.nja.sa1mdl");
	motion_oum_move_oum = *LoadAnimation(AnimalsArchive, "oum_move_oum.nam.saanim");
	motion_oum_hand_oum = *LoadAnimation(AnimalsArchive, "oum_hand_oum.nam.saanim");
	motion_oum_meet_oum = *LoadAnimation(AnimalsArchive, "oum_meet_oum.nam.saanim");
	motion_oum_wait_oum = *LoadAnimation(AnimalsArchive, "oum_wait_oum.nam.saanim");
	motion_oum_walk_oum = *LoadAnimation(AnimalsArchive, "oum_walk_oum.nam.saanim");

	object_koar_koar_koar = *LoadModel(AnimalsArchive, "koar_koar.nja.sa1mdl");
	motion_koar_move_koar = *LoadAnimation(AnimalsArchive, "koar_move_koar.nam.saanim");
	motion_koar_hand_koar = *LoadAnimation(AnimalsArchive, "koar_hand_koar.nam.saanim");
	motion_koar_meet_koar = *LoadAnimation(AnimalsArchive, "koar_meet_koar.nam.saanim");
	motion_koar_wait_koar = *LoadAnimation(AnimalsArchive, "koar_wait_koar.nam.saanim");

	object_wara_wara_wara = *LoadModel(AnimalsArchive, "wara_wara.nja.sa1mdl");
	motion_wara_move_wara = *LoadAnimation(AnimalsArchive, "wara_move_wara.nam.saanim");
	motion_wara_hand_wara = *LoadAnimation(AnimalsArchive, "wara_hand_wara.nam.saanim");
	motion_wara_meet_wara = *LoadAnimation(AnimalsArchive, "wara_meet_wara.nam.saanim");
	motion_wara_wait_wara = *LoadAnimation(AnimalsArchive, "wara_wait_wara.nam.saanim");

	object_banb_banb_banb = *LoadModel(AnimalsArchive, "banb_banb.nja.sa1mdl");
	motion_banb_move_banb = *LoadAnimation(AnimalsArchive, "banb_move_banb.nam.saanim");
	motion_banb_hand_banb = *LoadAnimation(AnimalsArchive, "banb_hand_banb.nam.saanim");
	motion_banb_meet_banb = *LoadAnimation(AnimalsArchive, "banb_meet_banb.nam.saanim");
	motion_banb_wait_banb = *LoadAnimation(AnimalsArchive, "banb_wait_banb.nam.saanim");

	object_usa_usa_usa = *LoadModel(AnimalsArchive, "usa_usa.nja.sa1mdl");
	motion_usa_move_usa = *LoadAnimation(AnimalsArchive, "usa_move_usa.nam.saanim");
	motion_usa_hand_usa = *LoadAnimation(AnimalsArchive, "usa_hand_usa.nam.saanim");
	motion_usa_meet_usa = *LoadAnimation(AnimalsArchive, "usa_meet_usa.nam.saanim");
	motion_usa_wait_usa = *LoadAnimation(AnimalsArchive, "usa_wait_usa.nam.saanim");

	object_mogu_mogu_mogu = *LoadModel(AnimalsArchive, "mogu_mogu.nja.sa1mdl");
	motion_mogu_move_mogu = *LoadAnimation(AnimalsArchive, "mogu_move_mogu.nam.saanim");
	motion_mogu_hand_mogu = *LoadAnimation(AnimalsArchive, "mogu_hand_mogu.nam.saanim");
	motion_mogu_meet_mogu = *LoadAnimation(AnimalsArchive, "mogu_meet_mogu.nam.saanim");
	motion_mogu_wait_mogu = *LoadAnimation(AnimalsArchive, "mogu_wait_mogu.nam.saanim");

	object_zou_zou_zou = *LoadModel(AnimalsArchive, "zou_zou.nja.sa1mdl");
	motion_zou_move_zou = *LoadAnimation(AnimalsArchive, "zou_move_zou.nam.saanim");
	motion_zou_hand_zou = *LoadAnimation(AnimalsArchive, "zou_hand_zou.nam.saanim");
	motion_zou_meet_zou = *LoadAnimation(AnimalsArchive, "zou_meet_zou.nam.saanim");
	motion_zou_wait_zou = *LoadAnimation(AnimalsArchive, "zou_wait_zou.nam.saanim");

	object_lion_lion_lion = *LoadModel(AnimalsArchive, "lion_lion.nja.sa1mdl");
	motion_lion_move_lion = *LoadAnimation(AnimalsArchive, "lion_move_lion.nam.saanim");
	motion_lion_hand_lion = *LoadAnimation(AnimalsArchive, "lion_hand_lion.nam.saanim");
	motion_lion_meet_lion = *LoadAnimation(AnimalsArchive, "lion_meet_lion.nam.saanim");
	motion_lion_wait_lion = *LoadAnimation(AnimalsArchive, "lion_wait_lion.nam.saanim");

	object_gori_gori_gori = *LoadModel(AnimalsArchive, "gori_gori.nja.sa1mdl");
	motion_gori_move_gori = *LoadAnimation(AnimalsArchive, "gori_move_gori.nam.saanim");
	motion_gori_hand_gori = *LoadAnimation(AnimalsArchive, "gori_hand_gori.nam.saanim");
	motion_gori_meet_gori = *LoadAnimation(AnimalsArchive, "gori_meet_gori.nam.saanim");
	motion_gori_wait_gori = *LoadAnimation(AnimalsArchive, "gori_wait_gori.nam.saanim");

	object_suka_suka_suka = *LoadModel(AnimalsArchive, "suka_suka.nja.sa1mdl");
	motion_suka_move_suka = *LoadAnimation(AnimalsArchive, "suka_move_suka.nam.saanim");
	motion_suka_hand_suka = *LoadAnimation(AnimalsArchive, "suka_hand_suka.nam.saanim");
	motion_suka_meet_suka = *LoadAnimation(AnimalsArchive, "suka_meet_suka.nam.saanim");
	motion_suka_wait_suka = *LoadAnimation(AnimalsArchive, "suka_wait_suka.nam.saanim");

	object_rako_rako_rako = *LoadModel(AnimalsArchive, "rako_rako.nja.sa1mdl");
	motion_rako_move_rako = *LoadAnimation(AnimalsArchive, "rako_move_rako.nam.saanim");
	motion_rako_hand_rako = *LoadAnimation(AnimalsArchive, "rako_hand_rako.nam.saanim");
	motion_rako_meet_rako = *LoadAnimation(AnimalsArchive, "rako_meet_rako.nam.saanim");
	motion_rako_wait_rako = *LoadAnimation(AnimalsArchive, "rako_wait_rako.nam.saanim");

	delete AnimalsArchive;
	ModelsLoaded_Animals = true;
}