#include "stdafx.h"

void PatchModels_STG07()
{
	// dxpc\stg07_ruin\bg\models\lost2_nbg2_1.nja.sa1mdl
	//((NJS_MATERIAL*)0x020310F4)->diffuse.color = 0xFFB2B2B2;

	// dxpc\stg07_ruin\bg\models\lost2_nbg_1.nja.sa1mdl
	//((NJS_MATERIAL*)0x02030728)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x0203073C)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x02030750)->diffuse.color = 0xFF7F7F7F;

	// dxpc\stg07_ruin\common\models\big_bigmirror.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebigate_hebikuchi.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebi_asiba01_hebi_asiba01.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebi_asiba02_hebi_asiba02.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebi_asiba03_hebi_asiba03.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebi_asibakabe01_hebi_asibakabe01.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\hebi_asibakabe02_hebi_asibakabe02.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\l02_hasira.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\l02_lightway_h.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lostobj_allgate.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lost_col_a.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\los_nc_busshu_a.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\los_nc_hako_a.nja.sa1mdl
	*(NJS_TEX*)0x0201ADEC = { 125, -255 };
	*(NJS_TEX*)0x0201ADF0 = { 125, 255 };
	*(NJS_TEX*)0x0201ADF4 = { 0, -255 };
	*(NJS_TEX*)0x0201ADFC = { 251, -255 };
	*(NJS_TEX*)0x0201AE00 = { 251, 255 };
	*(NJS_TEX*)0x0201AE08 = { 125, 255 };
	*(NJS_TEX*)0x0201AE0C = { 0, 3 };
	*(NJS_TEX*)0x0201AE10 = { 125, 3 };
	*(NJS_TEX*)0x0201AE14 = { 0, 3 };
	*(NJS_TEX*)0x0201AE18 = { 125, 3 };
	*(NJS_TEX*)0x0201AE20 = { 125, 255 };

	// dxpc\stg07_ruin\common\models\los_nc_hako_b.nja.sa1mdl
	*(NJS_TEX*)0x0201B034 = { 251, 255 };
	*(NJS_TEX*)0x0201B038 = { 0, 3 };
	*(NJS_TEX*)0x0201B03C = { 251, 3 };
	*(NJS_TEX*)0x0201B040 = { 0, 3 };
	*(NJS_TEX*)0x0201B044 = { 251, 3 };
	*(NJS_TEX*)0x0201B04C = { 251, 255 };
	*(NJS_TEX*)0x0201B068 = { 251, -255 };
	*(NJS_TEX*)0x0201B06C = { 251, 255 };
	*(NJS_TEX*)0x0201B070 = { 0, -255 };
	*(NJS_TEX*)0x0201B078 = { 251, -255 };
	*(NJS_TEX*)0x0201B07C = { 251, 255 };

	// dxpc\stg07_ruin\common\models\los_nc_hako_c.nja.sa1mdl
	*(NJS_TEX*)0x0201B294 = { 251, 255 };
	*(NJS_TEX*)0x0201B298 = { 62, 255 };
	*(NJS_TEX*)0x0201B29C = { 251, -255 };
	*(NJS_TEX*)0x0201B2A0 = { 62, -255 };
	*(NJS_TEX*)0x0201B2A4 = { 0, 3 };
	*(NJS_TEX*)0x0201B2A8 = { 0, 192 };
	*(NJS_TEX*)0x0201B2AC = { 251, 3 };
	*(NJS_TEX*)0x0201B2B0 = { 251, 192 };
	*(NJS_TEX*)0x0201B2B4 = { 251, 3 };
	*(NJS_TEX*)0x0201B2B8 = { 251, 192 };
	*(NJS_TEX*)0x0201B2BC = { 0, 3 };
	*(NJS_TEX*)0x0201B2C0 = { 0, 192 };

	// dxpc\stg07_ruin\common\models\los_nc_hako_d.nja.sa1mdl
	*(NJS_TEX*)0x0201B520 = { 251, 254 };
	*(NJS_TEX*)0x0201B524 = { 251, -127 };
	*(NJS_TEX*)0x0201B528 = { 0, 254 };
	*(NJS_TEX*)0x0201B52C = { 0, -127 };
	*(NJS_TEX*)0x0201B530 = { 510, 254 };
	*(NJS_TEX*)0x0201B534 = { 510, -127 };
	*(NJS_TEX*)0x0201B538 = { 127, 254 };
	*(NJS_TEX*)0x0201B53C = { 127, -127 };
	*(NJS_TEX*)0x0201B544 = { 382, 255 };
	*(NJS_TEX*)0x0201B548 = { 0, 3 };
	*(NJS_TEX*)0x0201B54C = { 382, 3 };

	// dxpc\stg07_ruin\common\models\los_nc_hako_e.nja.sa1mdl
	*(NJS_TEX*)0x0201B79C = { 62, 255 };
	*(NJS_TEX*)0x0201B7A0 = { 62, -255 };
	*(NJS_TEX*)0x0201B7A8 = { 0, -255 };
	*(NJS_TEX*)0x0201B7AC = { 251, 255 };
	*(NJS_TEX*)0x0201B7B0 = { 251, -255 };
	*(NJS_TEX*)0x0201B7B8 = { 0, 192 };
	*(NJS_TEX*)0x0201B7BC = { 251, 255 };
	*(NJS_TEX*)0x0201B7C0 = { 251, 192 };
	*(NJS_TEX*)0x0201B7C4 = { 251, 255 };
	*(NJS_TEX*)0x0201B7C8 = { 251, 192 };
	*(NJS_TEX*)0x0201B7D0 = { 0, 192 };

	// dxpc\stg07_ruin\common\models\los_nc_hako_f.nja.sa1mdl
	*(NJS_TEX*)0x0201BA00 = { 0, 129 };
	*(NJS_TEX*)0x0201BA08 = { 382, 129 };
	*(NJS_TEX*)0x0201BA0C = { 382, 129 };
	*(NJS_TEX*)0x0201BA10 = { 382, 255 };
	*(NJS_TEX*)0x0201BA18 = { 382, 3 };
	*(NJS_TEX*)0x0201BA1C = { 0, 3 };
	*(NJS_TEX*)0x0201BA24 = { 0, 129 };
	*(NJS_TEX*)0x0201BA28 = { 251, 255 };
	*(NJS_TEX*)0x0201BA2C = { 251, 129 };

	// dxpc\stg07_ruin\common\models\los_nc_shoku_a.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\los_nc_shoku_b.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\los_nc_shoku_c.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_arrow.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_bow.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_cl_button.nja.sa1mdl
		//Model too different (collision, so maybe it's fine?)

	// dxpc\stg07_ruin\common\models\lwobj_jumpdai.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_lwswitch.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_paneru.nja.sa1mdl
	*(NJS_TEX*)0x0201CA10 = { 510, -255 };
	*(NJS_TEX*)0x0201CA14 = { 510, 255 };
	*(NJS_TEX*)0x0201CA18 = { 0, -255 };

	// dxpc\stg07_ruin\common\models\lwobj_prop_a01.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_prop_a02.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake01_body.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake01_hcl.nja.sa1mdl
		//Model too different (collision)

	// dxpc\stg07_ruin\common\models\lwobj_snake01_head.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake01_tail.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake01_tail2.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake01_tail3.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_snake02.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_switch_body.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\lwobj_s_water.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\mirror_mirrorbox_1.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\obj_komorebi.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\sekichyuu_sekichyuu.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\togedai_togedai.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\turncube_turncube.nja.sa1mdl
		//Model too different

	// dxpc\stg07_ruin\common\models\no_unite\lwobj_lwdoor.nja.sa1mdl
	*(NJS_TEX*)0x0201EE88 = { 68, -1769 };
	*(NJS_TEX*)0x0201EE8C = { 1, -1769 };
	*(NJS_TEX*)0x0201EE90 = { 68, 239 };
	*(NJS_TEX*)0x0201EE94 = { 1, 239 };
	*(NJS_TEX*)0x0201EE98 = { 1, -1769 };
	*(NJS_TEX*)0x0201EE9C = { 68, -1769 };
	*(NJS_TEX*)0x0201EEA0 = { 1, 239 };
	*(NJS_TEX*)0x0201EEA4 = { 68, 239 };
	*(NJS_TEX*)0x0201EEA8 = { 68, -1769 };
	*(NJS_TEX*)0x0201EEAC = { 1, -1769 };
	*(NJS_TEX*)0x0201EEB0 = { 68, 239 };
	*(NJS_TEX*)0x0201EEB4 = { 1, 239 };
	*(NJS_TEX*)0x0201EEB8 = { 1, -1769 };
	*(NJS_TEX*)0x0201EEBC = { 68, -1769 };
	*(NJS_TEX*)0x0201EEC0 = { 1, 239 };
	*(NJS_TEX*)0x0201EEC4 = { 68, 239 };
	*(NJS_TEX*)0x0201EEC8 = { 2024, 251 };
	*(NJS_TEX*)0x0201EECC = { 15, 251 };
	*(NJS_TEX*)0x0201EED0 = { 2024, -251 };
	*(NJS_TEX*)0x0201EED4 = { 15, -251 };
	*(NJS_TEX*)0x0201EED8 = { 2024, 251 };
	*(NJS_TEX*)0x0201EEDC = { 15, 251 };
	*(NJS_TEX*)0x0201EEE0 = { 2024, -251 };
	*(NJS_TEX*)0x0201EEE4 = { 15, -251 };
	*(NJS_TEX*)0x0201EEE8 = { 15, -251 };
	*(NJS_TEX*)0x0201EEEC = { 2024, -251 };
	*(NJS_TEX*)0x0201EEF0 = { 15, 251 };
	*(NJS_TEX*)0x0201EEF4 = { 2024, 251 };
	*(NJS_TEX*)0x0201EEF8 = { 15, -251 };
	*(NJS_TEX*)0x0201EEFC = { 2024, -251 };
	*(NJS_TEX*)0x0201EF00 = { 15, 251 };
	*(NJS_TEX*)0x0201EF04 = { 2024, 251 };
	*(NJS_TEX*)0x0201EF08 = { 68, 5 };
	*(NJS_TEX*)0x0201EF0C = { 1, 5 };
	*(NJS_TEX*)0x0201EF10 = { 68, 253 };
	*(NJS_TEX*)0x0201EF14 = { 1, 253 };
	*(NJS_TEX*)0x0201EF18 = { 68, 5 };
	*(NJS_TEX*)0x0201EF1C = { 1, 5 };
	*(NJS_TEX*)0x0201EF20 = { 68, 253 };
	*(NJS_TEX*)0x0201EF24 = { 1, 253 };
	*(NJS_TEX*)0x0201F098 = { 3, 5 };
	*(NJS_TEX*)0x0201F09C = { 3, 253 };
	*(NJS_TEX*)0x0201F0A0 = { 506, 5 };
	*(NJS_TEX*)0x0201F0A4 = { 506, 253 };
	*(NJS_TEX*)0x0201F0A8 = { 3, 5 };
	*(NJS_TEX*)0x0201F0AC = { 3, 253 };
	*(NJS_TEX*)0x0201F0B0 = { 506, 5 };
	*(NJS_TEX*)0x0201F0B4 = { 506, 253 };
	*(NJS_TEX*)0x0201F0B8 = { 3, 5 };
	*(NJS_TEX*)0x0201F0BC = { 3, 253 };
	*(NJS_TEX*)0x0201F0C0 = { 506, 5 };
	*(NJS_TEX*)0x0201F0C4 = { 506, 253 };
	*(NJS_TEX*)0x0201F0C8 = { 3, 5 };
	*(NJS_TEX*)0x0201F0CC = { 3, 253 };
	*(NJS_TEX*)0x0201F0D0 = { 506, 5 };
	*(NJS_TEX*)0x0201F0D4 = { 506, 253 };
	*(NJS_TEX*)0x0201F0D8 = { 506, 253 };
	*(NJS_TEX*)0x0201F0DC = { 506, 5 };
	*(NJS_TEX*)0x0201F0E0 = { 3, 253 };
	*(NJS_TEX*)0x0201F0E4 = { 3, 5 };
	*(NJS_TEX*)0x0201F0E8 = { 506, 253 };
	*(NJS_TEX*)0x0201F0EC = { 506, 5 };
	*(NJS_TEX*)0x0201F0F0 = { 3, 253 };
	*(NJS_TEX*)0x0201F0F4 = { 3, 5 };
	*(NJS_TEX*)0x0201F0F8 = { 506, 253 };
	*(NJS_TEX*)0x0201F0FC = { 506, 5 };
	*(NJS_TEX*)0x0201F100 = { 3, 253 };
	*(NJS_TEX*)0x0201F104 = { 3, 5 };
	*(NJS_TEX*)0x0201F108 = { 506, 253 };
	*(NJS_TEX*)0x0201F10C = { 506, 5 };
	*(NJS_TEX*)0x0201F110 = { 3, 253 };
	*(NJS_TEX*)0x0201F114 = { 3, 5 };
	*(NJS_TEX*)0x0201F118 = { 3, 253 };
	*(NJS_TEX*)0x0201F11C = { 506, 253 };
	*(NJS_TEX*)0x0201F120 = { 3, 5 };
	*(NJS_TEX*)0x0201F124 = { 506, 5 };
	*(NJS_TEX*)0x0201F128 = { 506, 5 };
	*(NJS_TEX*)0x0201F12C = { 3, 5 };
	*(NJS_TEX*)0x0201F130 = { 506, 253 };
	*(NJS_TEX*)0x0201F134 = { 3, 253 };
}