#include "stdafx.h"

// TODO: Water ripples in Knuckles' cutscene "My father is coming here soon" aren't positioned exactly like on DC
// TODO: Look into "invalid" reflection textures

static bool ModelsLoaded_ADV03;

FunctionPointer(void, MRPDrawShadow, (float radius), 0x634EB0); // Draws an NPC shadow with the specified radius

// Model pointers: Chao with different eye textures
NJS_OBJECT *PastChaoModel_2 = nullptr; // Past Chao: regular eyes
NJS_OBJECT *PastChaoModel_7 = nullptr; // Past Chao: angry eyes
NJS_OBJECT *PastChaoModel_8 = nullptr; // Past Chao: happy eyes
NJS_OBJECT *PastChaoModel_9 = nullptr; // Past Chao: singing eyes
NJS_OBJECT *PastChaoModel_10 = nullptr; // Past Chao: worried eyes

static float PastStairsDistanceFix = 2000.0f; // Distance check for stairs to pop in

// Past Chao display hook to rotate the emotion ball
static Trampoline* EV_Alife_draw_t = nullptr;
static void __cdecl EV_Alife_draw_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(EV_Alife_draw);
	ADV03_OBJECTS[1]->child->ang[0] = evCameraW.ang.x;
	ADV03_OBJECTS[1]->child->ang[1] = evCameraW.ang.y;
	if (PastChaoModel_2)
	{
		PastChaoModel_2->child->ang[0] = evCameraW.ang.x;
		PastChaoModel_7->child->ang[0] = evCameraW.ang.x;
		PastChaoModel_8->child->ang[0] = evCameraW.ang.x;
		PastChaoModel_9->child->ang[0] = evCameraW.ang.x;
		PastChaoModel_10->child->ang[0] = evCameraW.ang.x;
		PastChaoModel_2->child->ang[1] = evCameraW.ang.y;
		PastChaoModel_7->child->ang[1] = evCameraW.ang.y;
		PastChaoModel_8->child->ang[1] = evCameraW.ang.y;
		PastChaoModel_9->child->ang[1] = evCameraW.ang.y;
		PastChaoModel_10->child->ang[1] = evCameraW.ang.y;
	}
	original(tp);
}

// Hook to draw the palm reflection separately in Act 2
void RenderPalmAct2(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawObjectClipEx(a1, a2, a3);
	late_z_ofs___ = -49000.0f;
	late_DrawModelClipMesh(a1->child->basicdxmodel, LATE_MAT, 1.0f);
	late_z_ofs___ = 0.0f;
}

// Hook to draw the palm reflection separately in Act 3
void RenderPalmAct3(NJS_ACTION *a1, float a2, LATE a3, float a4)
{
	late_ActionClipEx(a1, a2, a3, a4);
	late_z_ofs___ = -49000.0f;
	late_DrawModelClipMesh(a1->object->child->sibling->basicdxmodel, LATE_MAT, 1.0f);
	late_z_ofs___ = 0.0f;
}

// Hook to draw the tree shadow separately
void FixTreeShadowFlickering(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawModelClipEx(a1->basicdxmodel, a2, a3);
	late_z_ofs___ = -27000.0f;
	late_DrawModel(a1->child->basicdxmodel, LATE_LIG);
	late_z_ofs___ = 0.0f;
}

// Readds contant material after drawing Past NPC shadow
void PastNPCShadowFix(float size)
{
	MRPDrawShadow(size);
	njControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
}

void AllocateEventChao_2(task* a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{
	NJS_ACTION newaction = { PastChaoModel_2, a2->motion };
	EV_SetAction(a1, &newaction, a3, a4, a5, a6);
}

void AllocateEventChao_7(task* a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{
	NJS_ACTION newaction = { PastChaoModel_7, a2->motion };
	EV_SetAction(a1, &newaction, a3, a4, a5, a6);
}

void AllocateEventChao_8(task* a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{
	NJS_ACTION newaction = { PastChaoModel_8, a2->motion };
	EV_SetAction(a1, &newaction, a3, a4, a5, a6);
}

void AllocateEventChao_9(task* a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{
	NJS_ACTION newaction = { PastChaoModel_9, a2->motion };
	EV_SetAction(a1, &newaction, a3, a4, a5, a6);
}

void AllocateEventChao_10(task *a1, NJS_ACTION *a2, NJS_TEXLIST *a3, float a4, char a5, char a6)
{
	NJS_ACTION newaction = { PastChaoModel_10, a2->motion };
	EV_SetAction(a1, &newaction, a3, a4, a5, a6);
}

void EventChaoFix()
{
	// Load models
	PastChaoModel_2 = CloneObject(ADV03_OBJECTS[1]);
	PastChaoModel_7 = CloneObject(ADV03_OBJECTS[1]);
	PastChaoModel_8 = CloneObject(ADV03_OBJECTS[1]);
	PastChaoModel_9 = CloneObject(ADV03_OBJECTS[1]);
	PastChaoModel_10 = CloneObject(ADV03_OBJECTS[1]);
	PastChaoModel_7->getnode(11)->basicdxmodel->mats[0].attr_texId = 7;
	PastChaoModel_7->getnode(12)->basicdxmodel->mats[0].attr_texId = 7;
	PastChaoModel_8->getnode(11)->basicdxmodel->mats[0].attr_texId = 8;
	PastChaoModel_8->getnode(12)->basicdxmodel->mats[0].attr_texId = 8;
	PastChaoModel_9->getnode(11)->basicdxmodel->mats[0].attr_texId = 9;
	PastChaoModel_9->getnode(12)->basicdxmodel->mats[0].attr_texId = 9;
	PastChaoModel_10->getnode(11)->basicdxmodel->mats[0].attr_texId = 10;
	PastChaoModel_10->getnode(12)->basicdxmodel->mats[0].attr_texId = 10;
	// Echidnas face Chaos
	WriteCall((void*)0x00653C40, AllocateEventChao_9);
	WriteCall((void*)0x00653C67, AllocateEventChao_9);
	WriteCall((void*)0x00653C91, AllocateEventChao_9);
	// Tikal pleads with her father
	WriteCall((void*)0x0066181E, AllocateEventChao_9);
	WriteCall((void*)0x00661840, AllocateEventChao_9);
	WriteCall((void*)0x00661862, AllocateEventChao_9);
	WriteCall((void*)0x00661887, AllocateEventChao_9);
	// Gamma meets Tikal
	WriteCall((void*)0x0067D993, AllocateEventChao_8);
	WriteCall((void*)0x0067D9B4, AllocateEventChao_2);
	WriteCall((void*)0x0067D9D8, AllocateEventChao_10);
	WriteCall((void*)0x0067D9F9, AllocateEventChao_8);
	WriteCall((void*)0x0067DA1A, AllocateEventChao_2);
	WriteCall((void*)0x0067DA3E, AllocateEventChao_10);
	WriteCall((void*)0x0067DA5F, AllocateEventChao_2);
	WriteCall((void*)0x0067DA80, AllocateEventChao_7);
	WriteCall((void*)0x0067DAA4, AllocateEventChao_2);
	WriteCall((void*)0x0067DAC5, AllocateEventChao_8);
	WriteCall((void*)0x0067DAE6, AllocateEventChao_8);
	WriteCall((void*)0x0067DB0A, AllocateEventChao_2);
	WriteCall((void*)0x0067DB2B, AllocateEventChao_10);
	WriteCall((void*)0x0067DB4C, AllocateEventChao_8);
	WriteCall((void*)0x0067DB70, AllocateEventChao_10);
	WriteCall((void*)0x0067DB91, AllocateEventChao_2);
	// Knuckles meets Tikal
	WriteCall((void*)0x0068BF49, AllocateEventChao_2);
	WriteCall((void*)0x0068BF6D, AllocateEventChao_2);
	WriteCall((void*)0x0068BF8E, AllocateEventChao_2);
	WriteCall((void*)0x0068BFAF, AllocateEventChao_2);
	WriteCall((void*)0x0068BFD3, AllocateEventChao_2);
	// Amy meets Tikal
	WriteCall((void*)0x006A1D94, AllocateEventChao_2);
	WriteCall((void*)0x006A1DB5, AllocateEventChao_2);
	WriteCall((void*)0x006A1DD9, AllocateEventChao_2);
	WriteCall((void*)0x006A1DFA, AllocateEventChao_2);
	WriteCall((void*)0x006A2A09, AllocateEventChao_2);
}

void ADV03_Init()
{
	ReplaceCAM("CAMPAST00S");
	ReplaceCAM("CAMPAST01S");
	ReplaceCAM("CAMPAST02S");
	ReplaceSET("SETPAST00S");
	ReplaceSET("SETPAST01S");
	ReplaceSET("SETPAST02S");
	ReplacePVM("EFF_PAST");
	ReplacePVM("EV_ALIFE");
	ReplacePVM("K_PATYA");
	ReplacePVM("OBJ_PAST");
	ReplacePVM("PAST00");
	ReplacePVM("PAST01");
	ReplacePVM("PAST02");
	ReplacePVM("PAST_KN_FAM");
	ReplacePVM("KNUCKLES_NORMAL");
	ReplacePVM("KNUCKLES_DEBU");
	ReplacePVM("KNUCKLES_LONG");
	// Fog and draw distance data
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Adv03[0][i].f32StartZ = -12000.0f;
		pFogTable_Adv03[0][i].f32EndZ = -12000.0f;
		pFogTable_Adv03[1][i].f32StartZ = -12000.0f;
		pFogTable_Adv03[1][i].f32EndZ = -12000.0f;
		pFogTable_Adv03[2][i].f32StartZ = -12000.0f;
		pFogTable_Adv03[2][i].f32EndZ = -12000.0f;
		pClipMap_Adv03[0][i].f32Far = -12000.0f;
		pClipMap_Adv03[1][i].f32Far = -16000.0f;
		pClipMap_Adv03[2][i].f32Far = -16000.0f;
	}
}

void ADV03_Load()
{
	// This is done every time the function is called
	LevelLoader(LevelAndActIDs_Past1, "SYSTEM\\data\\adv03_past\\landtablepast00.c.sa1lvl", ADV03_TEXLISTS[4], ___LANDTABLEPAST[0]);
	LevelLoader(LevelAndActIDs_Past2, "SYSTEM\\data\\adv03_past\\landtablepast01.c.sa1lvl", ADV03_TEXLISTS[5], ___LANDTABLEPAST[1]);
	LevelLoader(LevelAndActIDs_Past3, "SYSTEM\\data\\adv03_past\\landtablepast02.c.sa1lvl", ADV03_TEXLISTS[6], ___LANDTABLEPAST[2]);
	// This is done only once
	if (!ModelsLoaded_ADV03)
	{		
		// Disable SADX water
		if (!CheckSADXWater(LevelIDs_Past))
			WriteData<5>((void*)0x00541F14, 0x90u);
		// Make stairs objects in Past Act 1 appear sooner
		WriteData((float**)0x00545349, &PastStairsDistanceFix);
		WriteData((float**)0x00545409, &PastStairsDistanceFix);
		// Echidnas shadow fix
		WriteData<5>((char*)0x00543364, 0x90u); // Disable constant material before drawing NPC shadow
		WriteCall((void*)0x005433A3, PastNPCShadowFix); // Enable constant material after drawing NPC shadow
		// Event Chao eye fixes
		EventChaoFix();
		EV_Alife_draw_t = new Trampoline(0x006EF080, 0x006EF085, EV_Alife_draw_r);
		// Palm fixes		
		*ADV03_ACTIONS[10]->object = *LoadModel("system\\data\\adv03_past\\common\\models\\mra_n_yasikia.nja.sa1mdl"); // Palm in Act 3 (edited hirarchy)
		*ADV03_OBJECTS[9] = *LoadModel("system\\data\\adv03_past\\common\\models\\mra_n_sindenyasib.nja.sa1mdl"); // Palm in Act 2 (edited hierarchy)
		WriteCall((void*)0x00545C1A, RenderPalmAct2);
		WriteCall((void*)0x00545BFD, RenderPalmAct3);
		AddWhiteDiffuseMaterial(&ADV03_OBJECTS[12]->basicdxmodel->mats[1]); // OTree 0 second model
		// Tikal cutscene water ripple thing
		WriteData((float*)0x0068BA27, -40.7f); // Ripple 1 X
		WriteData((float*)0x0068BA22, 86.0f); // Ripple 1 Y
		WriteData((float*)0x0068BA1D, 59.43f); // Ripple 1 Z
		WriteData((float*)0x0068BA62, -40.7f); // Ripple 2 X
		WriteData((float*)0x0068BA5D, 86.0f); // Ripple 2 Y
		WriteData((float*)0x0068BA58, 59.43f); // Ripple 2 Z
		WriteData((float*)0x0068BA94, -52.01f); // Ripple 3 X
		WriteData((float*)0x0068BA8F, 86.0f); // Ripple 3 Y
		WriteData((float*)0x0068BA8A, 52.42f); // Ripple 3 Z
		// Other objects
		*ADV03_ACTIONS[9]->object = *LoadModel("system\\data\\adv03_past\\common\\models\\mra_n_treeaa.nja.sa1mdl"); // OTree 0
		*ADV03_ACTIONS[12]->object = *LoadModel("system\\data\\adv03_past\\common\\models\\mrj_n_sotetubody.nja.sa1mdl"); // Animated palm tree (edited meshsets)
		*ADV03_OBJECTS[16] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrk_k_objtreeb.nja.sa1mdl"); // Tree 16 (edited hierarchy)
		*ADV03_OBJECTS[17] = *CloneObject(ADV03_OBJECTS[16]); // Tree 17 (low LOD model)
		WriteCall((void*)0x005455A9, FixTreeShadowFlickering);
		WriteCall((void*)0x0054557C, FixTreeShadowFlickering);
		*ADV03_OBJECTS[13] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrj_n_ido.nja.sa1mdl"); // OWell
		*ADV03_OBJECTS[21] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrk_nc_k_gmkaidan.nja.sa1mdl"); // OPyStairs
		*ADV03_OBJECTS[20] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrk_nc_k_gmdaikaidan.nja.sa1mdl"); // OBigStairs
		*ADV03_OBJECTS[19] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrk_nc_k_daikaidanlow.nja.sa1mdl"); // OBigStairs low LOD
		*ADV03_OBJECTS[22] = *LoadModel("system\\data\\adv03_past\\common\\models\\mrk_nc_k_kaidanlow.nja.sa1mdl"); // OPyStairs low LOD
		ModelsLoaded_ADV03 = true;
	}
}