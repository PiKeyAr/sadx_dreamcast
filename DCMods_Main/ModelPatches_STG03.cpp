#include "stdafx.h"

void PatchModels_STG03()
{
	// dxpc\stg03_twinkle\common\models\boom_boomtaru.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\boom_hahen1.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\courseter_courster.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\fence02_sakub.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\fence_parksaku.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\flyasiba_asiba.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\hata01_hata.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\hata02_hata02.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\lighthata_lump.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\mado_outmado.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\newship_oya.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\park01_monitor.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\park02_bowl.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\park02_bowldoor.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\park02_go_center.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\park02_k_door.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\park02_ring_yuka.nja.sa1mdl
	*(NJS_TEX*)0x027AF900 = { 254, 7 };
	*(NJS_TEX*)0x027AF904 = { 509, 255 };
	*(NJS_TEX*)0x027AF90C = { 254, 7 };
	*(NJS_TEX*)0x027AF910 = { 509, 255 };
	*(NJS_TEX*)0x027AF918 = { 254, 7 };
	*(NJS_TEX*)0x027AF91C = { 509, 255 };
	*(NJS_TEX*)0x027AF924 = { 254, 7 };
	*(NJS_TEX*)0x027AF928 = { 509, 255 };
	*(NJS_TEX*)0x027AF930 = { 254, 7 };
	*(NJS_TEX*)0x027AF934 = { 509, 255 };
	*(NJS_TEX*)0x027AF93C = { 509, 255 };
	*(NJS_TEX*)0x027AF944 = { 254, 7 };

	// dxpc\stg03_twinkle\common\models\park03_futa_a.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\park03_lr_door.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\pinall_pin.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\tpobj_demado.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tpobj_kadann00.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\tpobj_kadann01.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\tpobj_kadann02.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tpobj_kadann03.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tpobj_kadann04.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tpobj_kadann05.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tpobj_shutter.nja.sa1mdl
		//Full replacement

	// dxpc\stg03_twinkle\common\models\tpobj_w_asiba.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\tp_obj_hunger.nja.sa1mdl
	((NJS_MATERIAL*)0x027B3EF8)->attrflags = 0x26B18300;
	((NJS_MATERIAL*)0x027B3EF8)->specular.color = 0xFFFFFFFF;
	*(NJS_TEX*)0x027B3F18 = { 247, 255 };
	*(NJS_TEX*)0x027B3F1C = { 247, 7 };
	*(NJS_TEX*)0x027B3F24 = { 0, 7 };
	((NJS_MATERIAL*)0x027B3DEC)->attrflags = 0x26B18300;
	((NJS_MATERIAL*)0x027B3DEC)->specular.color = 0xFFFFFFFF;
	*(NJS_TEX*)0x027B3E10 = { 0, 7 };
	*(NJS_TEX*)0x027B3E14 = { 247, 255 };
	*(NJS_TEX*)0x027B3E18 = { 247, 7 };

	// dxpc\stg03_twinkle\common\models\uma_uma.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\common\models\yane_turnyane.nja.sa1mdl
		//Model too different

	// dxpc\stg03_twinkle\killcolli\act1\tp_a1oti04.nja.sa1mdl
	*(NJS_VECTOR*)0x026B3960 = { -1529.9208f, -1544.1671f, 6593.769f };
	*(NJS_VECTOR*)0x026B396C = { -1529.9208f, -1544.1671f, 8385.503f };
	*(NJS_VECTOR*)0x026B3978 = { 785.76776f, -1883.8839f, 8548.387f };
	*(NJS_VECTOR*)0x026B3984 = { 1201.868f, -1883.8839f, 6394.6875f };
}