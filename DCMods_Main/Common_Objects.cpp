#include "stdafx.h"
#include "Common_Objects.h"

static float EmeraldScale = 1.005f; // Scale for emerald piece overlay

static Trampoline* ObjectKaosEme_t = nullptr;
static Trampoline* ObjectKaosEme_IC_t = nullptr;
static Trampoline* ObjectKaosEme_CA_t = nullptr;
static Trampoline* DispATask_t = nullptr;

// Trampoline to draw the unpressed switch button with the darkening effect
static void __cdecl DispATask_r(task* tp)
{
	// One of the values in taskwk is used as material color for materials 3 (star) and 4 (glass)
	unsigned char alpha = (unsigned char)tp->twp->counter.b[1];
	for (int i = 3; i < 5; i++)
	{
		model_switch_body_button2.mats[i].diffuse.argb.r =
			model_switch_body_button2.mats[i].diffuse.argb.g =
			model_switch_body_button2.mats[i].diffuse.argb.b = alpha;
	}

	TARGET_DYNAMIC(DispATask)(tp);
}

// Draws the emerald glow effect sprite
void DrawEmeraldGlow(taskwk* twp)
{
	NJS_SPRITE* sprite;
	switch ((int)twp->scl.y)
	{
	case 0: // Windy Valley 
		sprite = &sprite_kaos_eme_eff_1;
		break;
	case 1: // Casinopolis
		sprite = &sprite_kaos_eme_eff_0;
		break;
	case 2: // Ice Cap
	default:
		sprite = &sprite_kaos_eme_eff;
		break;
	}
	njPushMatrix(0);
	njTranslate(0, twp->pos.x, twp->pos.y + 15, twp->pos.z);
	njRotateXYZ(0, camera_twp->ang.x, camera_twp->ang.y, 0);
	float alpha = min(255, abs(twp->value.l)) / 255.0f;
	SetMaterial(1.0f, alpha, alpha, alpha);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_ONE);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	// Draw it twice lol
	late_DrawSprite3D(sprite, (int)twp->scl.y, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_ANGLE, LATE_LIG);
	late_DrawSprite3D(sprite, (int)twp->scl.y, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_ANGLE, LATE_LIG);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	njPopMatrix(1u);
	ResetMaterial();
	// Increase and limit glow timer
	twp->value.l += 2;
	if ((twp->value.l > 0 && (unsigned int)twp->value.l > twp->counter.l + 128) || (twp->value.l < 0 && twp->value.l > -128))
		twp->value.l = twp->value.l * -1;
	//PrintDebug("counter %d, value: %d, F: %f\n", twp->counter.l, twp->value.l, alpha);
}

// Goal emerald (Windy Valley) trampoline to draw the glow effect
static void __cdecl ObjectKaosEme_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectKaosEme);
	original(tp);
	taskwk* twp = tp->twp;
	if (twp->mode == 2)
		DrawEmeraldGlow(twp);
}

// Goal emerald (Ice Cap) trampoline to draw the glow effect
static void __cdecl ObjectKaosEme_IC_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectKaosEme_IC);
	original(tp);
	taskwk* twp = tp->twp;
	if (twp->mode == 2)
		DrawEmeraldGlow(twp);
}

// Goal emerald (Casino) trampoline to draw the glow effect
static void __cdecl ObjectKaosEme_CA_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectKaosEme_CA);
	original(tp);
	taskwk* twp = tp->twp;
	if (twp->mode == 2)
		DrawEmeraldGlow(twp);
}

// Draws the lightning effect for the magnetic barrier
void MagneticBarrierLightning(NJS_POINT3COL* a1, int a2, NJD_DRAW attr, LATE a4)
{
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	late_z_ofs___ = (ssStageNumber == 3 && ssActNumber == 2) ? 10 : 22048.0f; // ???
	late_DrawTriangle3D(a1, a2, attr, LATE_MAT);
	late_z_ofs___ = 0;
	ResetMaterial();
}

// Hook to set magnetic barrier alpha
void SetMagneticBarrierColor(float a, float r, float g, float b)
{
	SetMaterial(r, r, g, b);
}

// General function for rendering barrier models (invincibility, normal and magnetic)
int ef_barrier_DrawModel_r(NJS_MODEL_SADX* a1)
{
	if (ssStageNumber == 3 && ssActNumber == 2)
		late_z_ofs___ = 0;
	else
		late_z_ofs___ = 20048.0f;
	if (ModConfig::EnabledLevels[LevelIDs_SpeedHighway] && ssStageNumber == LevelIDs_SpeedHighway && ssActNumber == 2)
		late_z_ofs___ = 500.0f;
	late_DrawModel(a1, LATE_MAT);
	late_z_ofs___ = 0;
	return 0;
}

// Hook to stop draw spam in normal barrier
task* CreateChildTask_Barrier(int flags, void(__cdecl* address)(task*), task* parent)
{
	return ((double)rand() * 0.000030517578f > 0.84f) ? CreateChildTask(flags, address, parent) : 0;
}

// Invincibility lines depth/queue flags fix
void RenderInvincibilityLines(NJS_MODEL_SADX* a1)
{
	late_z_ofs___ = (ssStageNumber == 3 && ssActNumber == 2) ? 0 : 20048.0f; // No idea why
	late_DrawModel(a1, LATE_MAT);
	late_z_ofs___ = 0.0f;
}

// Water splash depth fix
void FixWaterSplash(taskwk* a1)
{
	sp_zoffset = 2000.0f;
	PSetSplashEffect(a1);
	sp_zoffset = 0;
}

// Character shadow depth hack
void late_DrawShadowObject_r(NJS_OBJECT* a1, float a2)
{
	float depthbk = late_z_ofs___;
	// Set depth
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
		late_z_ofs___ = 2600.0f;
	else if (ModConfig::EnabledLevels[LevelIDs_Chaos4] && ssStageNumber == LevelIDs_Chaos4)
	{
		if (playertwp[0]->pos.y >= 18)
			late_z_ofs___ = 4000.0f;
		else
			late_z_ofs___ = -32952.0f;
	}
	else if (ModConfig::EnabledLevels[LevelIDs_RedMountain] && ssStageNumber == LevelIDs_RedMountain && ssActNumber != 1)
	{
		// Disable for digging (not sure if this is still needed)
		if (GetPlayerNumber() == Characters_Knuckles && playerpwp[0]->mj.action >= 41 && playerpwp[0]->mj.action <= 44)
			late_z_ofs___ = -21000.0f;
	}
	else
		late_z_ofs___ = -27952.0f;
	// From original function
	if (loop_count || isTextureNG(njds_texList))
		late_z_ofs___ = depthbk;
	else
		late_DrawObjectClip(a1, LATE(LATE_LIG | LATE_EASY), a2);
	late_z_ofs___ = depthbk;
}

// Ring shadow depth hack
void late_DrawShadowModel_r(NJS_MODEL_SADX* a1, float a2)
{
	float v2 = late_z_ofs___;
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley] && ssStageNumber == LevelIDs_WindyValley && ssActNumber == 2)
	{
		late_z_ofs___ = 2600.0f;
	}
	else if (ModConfig::EnabledLevels[LevelIDs_FinalEgg] && ssStageNumber == LevelIDs_FinalEgg && ssActNumber == 2)
	{
		late_z_ofs___ = 3000.0f;
	}
	else
		late_z_ofs___ = -27952.0f;
	late_DrawModelClip(a1, LATE(LATE_LIG | LATE_EASY), a2);
	late_z_ofs___ = v2;
}

// Renders the Master Emerald piece
void RenderEmeraldShard_MainModel(NJS_OBJECT* a1, LATE a2, float a3)
{
	SaveControl3D();
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	OnControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
	SetMaterial(1.0f, 0.68f, 0.68f, 0.68f);
	DrawObjectClip(a1, a3);
	LoadControl3D();
}

// Renders the Master Emerald piece glow (inner layer)
void RenderEmeraldShard_Overlay1(NJS_OBJECT* a1, LATE a2)
{
	a1->basicdxmodel->mats[0].diffuse.color = 0x00000000;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	late_z_ofs___ = 1500.0f;
	DrawObjectClipMesh(a1, LATE_MAT, 1.0f);
	late_z_ofs___ = 0.0f;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

// Renders the Master Emerald piece glow (outer layer)
void RenderEmeraldShard_Overlay2(NJS_OBJECT* a1)
{
	a1->basicdxmodel->mats[0].diffuse.color = 0x00000000;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	late_z_ofs___ = 1500.0f;
	DrawObjectClipMesh(a1, LATE_MAT, 1.0f);
	late_z_ofs___ = 0.0f;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

// Sets the alpha for Master Emerald shards' constant material
void SetEmeraldShardColor(float a, float r, float g, float b)
{
	SetMaterial(0.08f, 1.0f, 1.0f, 1.0f);
}

// Sets transparency for the hint monitor's constant material to enable glass transparency
void SetHintMonitorTransparency(NJS_ARGB* a1)
{
	SetMaterial(min(0.69f, a1->a), min(0.69f, a1->r), min(0.69f, a1->g), min(0.69f, a1->b));
}

// Generates ripples from character movement in water
void PSetRippleEffect_r(unsigned __int8 pno, NJS_VECTOR* pp)
{
	playerwk* pwk; // esi
	float random; // st7
	bool con1; // c0
	bool con2; // c3
	NJS_VECTOR resultvec = { 0, 0, 1.5f }; // [esp+Ch] [ebp-Ch]

	pwk = playerpwp[pno];
	if (playertwp[pno]->pos.y + pwk->p.height >= pp->y)
	{
		// SADX generates ripples even if the character is not moving, but SA1 doesn't
		if (njScalor(&pwk->spd) == 0.0f)
			return;
		random = rand() * 0.000030517578f;
		con1 = random < 0.85f; // 0.8 in vanilla DX
		con2 = random == 0.85f; // 0.8 in vanilla DX
		if (con1 || con2)
			return;
		resultvec.x = fabs(pwk->spd.y) * 0.005f + 0.03f;
		CreateWaterripple(pp, &resultvec, pwk->p.rad * 0.1f);
	}
}

// Hook to draw the bubble version of the ripple with depth
void RippleHack_Bubble(NJS_OBJECT* a1)
{
	late_z_ofs___ = -17952.0f;
	late_DrawObjectClip(a1, LATE_MAT, 2.0f);
	late_z_ofs___ = 0.0f;
}

// Hook to make the normal version of the ripple look more like the DC version
void RippleHack_Normal(float a, float r, float g, float b)
{
	float alpha = -1.0f * abs(r); // Hack for offset material
	njScale(0, 0.74f, 1.0f, 0.74f); // Haven't figured out why it's smaller on DC. The values used in scale calculation appear to be the same.
	SetMaterial(0, alpha, alpha, alpha);
}

// Hack to draw upgrade models with depth
void RenderMainUpgradeModel(NJS_OBJECT* model, LATE flags, float scale)
{
	late_z_ofs___ = 1000.0f;
	late_DrawObjectClip(model, flags, scale);
	late_z_ofs___ = 0;
}

void Objects_Init()
{
	ReplacePVM("HINT");
	ReplacePVM("TOGEBALL_TOGEBALL");
	ReplacePVM("OBJ_REGULAR");
	ReplacePVM("EFF_REGULAR");
	PatchModels_Objects();
	ItemBox_Init();
	WriteData<1>(ModelTurnWhite, 0xC3u); // Disable the "light up the object when you approach it" thing
	// Emblem field model
	object_o_emblm_emblm.basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
	object_o_emblm_emblm.basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
	object_o_emblm_emblm.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
	object_o_emblm_emblm.basicdxmodel->mats[4].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
	AddWhiteDiffuseMaterial(&object_o_emblm_emblm.basicdxmodel->mats[1]);
	AddWhiteDiffuseMaterial(&object_o_emblm_emblm.basicdxmodel->mats[2]);
	AddWhiteDiffuseMaterial(&object_o_emblm_emblm.basicdxmodel->mats[3]);
	AddWhiteDiffuseMaterial(&object_o_emblm_emblm.basicdxmodel->mats[4]);
	// Character upgrades
	WriteCall((void*)0x004BEA22, RenderMainUpgradeModel);
	WriteCall((void*)0x004BEA33, RenderMainUpgradeModel);
	// Emeralds glow
	ObjectKaosEme_t = new Trampoline(0x004DF3B0, 0x004DF3B6, ObjectKaosEme_r);
	ObjectKaosEme_IC_t = new Trampoline(0x004ECFA0, 0x004ECFA6, ObjectKaosEme_IC_r);
	ObjectKaosEme_CA_t = new Trampoline(0x005DD0A0, 0x005DD0A6, ObjectKaosEme_CA_r);
	// Water splash particle
	WriteCall((void*)0x0049F1C0, FixWaterSplash);
	// Stupid hacks for Windy Valley 3 and other stages
	WriteJump(late_DrawShadowModel, late_DrawShadowModel_r);
	WriteJump(late_DrawShadowObject, late_DrawShadowObject_r);
	// Barrier fixes
	WriteData<5>((char*)0x004B9E3A, 0x90u); // Disable ResetMaterial (it's called later in my replacement function)
	WriteCall((void*)0x004B9F0F, MagneticBarrierLightning);
	WriteCall((void*)0x004B9DDA, SetMagneticBarrierColor); // Don't set alpha to 0 to make it look more like on DC
	WriteJump(ef_barrier_DrawModel, ef_barrier_DrawModel_r);
	WriteCall((void*)0x004BA3BE, CreateChildTask_Barrier); // Barrier regular
	WriteCall((void*)0x004BA0E4, RenderInvincibilityLines);
	// Some emerald shard "fixes": disable transparency on the main model and render overlays with slightly different flags
	WriteCall((void*)0x004A2CEF, RenderEmeraldShard_MainModel);
	WriteCall((void*)0x004A2DBD, RenderEmeraldShard_Overlay1);
	WriteCall((void*)0x004A2E02, RenderEmeraldShard_Overlay2);
	WriteCall((void*)0x004A2D0D, SetEmeraldShardColor);
	WriteData((float**)0x004A2D39, &EmeraldScale); // Prevent minor Z Fighting with the main model
	// Ripple
	if (ModConfig::EnableDCRipple)
	{
		object_ripple_ripple = *LoadModel("system\\data\\object\\ripple.nja.sa1mdl");
		object_ripple_ripple.basicdxmodel->mats[0].attr_texId = 99; // Texture ID to match the new OBJ_REGULAR where the DC ripple texture is last
		AddAlphaRejectMaterial(&object_ripple_ripple.basicdxmodel->mats[0]);
		WriteJump(PSetRippleEffect, PSetRippleEffect_r); // Creates ripples from character movement
		WriteCall((void*)0x004B92FA, RippleHack_Normal);
		WriteCall((void*)0x007A822B, RippleHack_Bubble);
		WriteData((float*)0x004B9334, 2000.0f); // Ripple depth (normal)
	}
	// Hint monitor
	WriteCall((void*)0x007A957F, SetHintMonitorTransparency);
	// Other
	object_shadow.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT; // Shadow blob (SL object)
	AddAlphaRejectMaterial(&object_shadow.basicdxmodel->mats[0]);
	AddAlphaRejectMaterial(&object_muteki_rod_rod.basicdxmodel->mats[0]); // Invincibility lines
	AddAlphaRejectMaterial(&object_muteki_ball_ball.basicdxmodel->mats[0]); // Invincibility ball
	AddAlphaRejectMaterial(&object_barria_t_barria_barria.basicdxmodel->mats[0]); // Magnetic barrier
	AddAlphaRejectMaterial((NJS_MATERIAL*)((size_t)GetModuleHandle(L"ADV02MODELS") + 0x0007C334)); // Emerald shards (cutscene)
	// Gamma's dynamite
	object_sikake_dynamite_dynamite.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA; // I think it's supposed to be transparent but it looks like that on DC so...
	object_sikake_dynamite_dynamite.basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_USE_ALPHA; // But this one is totally unnecessary either way
	ForceObjectSpecular_Object(&object_sikake_dynamite_dynamite, false);
	// Switch
	DispATask_t = new Trampoline(0x004CB590, 0x004CB595, DispATask_r);
	for (int i = 0; i < model_switch_body_button2.nbMat; i++)
		memcpy(&model_switch_body_button2.mats[i], &model_switch_body_button.mats[i], sizeof(NJS_MATERIAL)); // Fix the weird looking button materials
}

void Objects_OnFrame()
{
	// Disable depth override for Tikal hints in Chao gardens
	if (IsChaoGarden)
		tikal_sw = 0;
}