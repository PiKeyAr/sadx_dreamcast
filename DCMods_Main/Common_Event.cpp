#include "stdafx.h"
#include "Common_Event.h"

// TODO: Chaos puddle in Knuckles' "Chaos 4 emerges" cutscene shouldn't ignore lighting (use specular 3)
// TODO: Chaos puddle in the cutscene after Chaos 2 is defeated should use object light type

static float EggWalkerCarOffset = -0.25f; // Moves Egg Walker cars up if they sink too low into the ground

// Environment maps for Chaos puddle in Chaos 1 cutscene (model draw callback)
void ChaosTimeToEat_DrawModel(NJS_OBJECT* a1)
{
	ScaleEnvironmentMap(2.0f, 1.0f, 0.5f, 1.0f);
	njSetZUpdateMode(0);
	dsDrawModel(a1->basicdxmodel);
	njSetZUpdateMode(1);
	RestoreEnvironmentMap();
}

// Environment maps for Chaos puddle in some cutscenes
void ComeOnChaosTimeToEat(NJS_OBJECT* a1)
{
	late_SetFunc((void(__cdecl*)(void*))ChaosTimeToEat_DrawModel, (void*)a1, 2000.0f, 0);
}

// This hook forces the game to skip cutscenes when D-Pad Up+Down are pressed (instead of Start/A)
void InputHookForCutscenes()
{
	TH_Wait();
	if (CutsceneFadeoutMode == CutsceneFade::FadeoutWait)
		per[0]->press |= (Buttons_Down | Buttons_Up);
}

// This hook removes camera transition after some cutscenes
void FixCutsceneTransition()
{
	switch (current_event)
	{
	case 134: // Knuckles back in Station Square after meeting Pacman
	case 380: // Gamma after Windy Valley
		ResetCameraTimer();
		break;
	default:
		break;
	}
}

// Trampoline to reset values related to cutscene skipping
static Trampoline* DisplayTitleCard_t = nullptr;
static Sint32 __cdecl DisplayTitleCard_r()
{
	const auto original = TARGET_DYNAMIC(DisplayTitleCard);
	CutsceneFadeoutMode = CutsceneFade::FadeoutEnabled;
	CutsceneFadeAlpha = 0;
	SkipPressed_Cutscene = false;
	return original();
}

// Hook to override depth in Amy's cutscene
void DrawCutsceneZeroShadow(NJS_OBJECT* object, float scale)
{
	late_z_ofs___ = 2000.0f;
	late_DrawObjectClip(object, LATE_LIG, scale);
	late_z_ofs___ = 0.0f;
}

// Hook to replace the drawing function for Chaos puddle
void RenderChaosPuddle_Last(NJS_OBJECT* a1)
{
	DrawObjectClipMesh(a1, LATE_MAT, 1.0f);
}

// Trampoline to force mode in EV_SetAction - this adds a flag to change the draw function for some objects
static Trampoline* EV_SetAction_t = nullptr;
static void __cdecl EV_SetAction_r(taskwk* twp, NJS_ACTION* action, NJS_TEXLIST* texlist, float speed, char mode, char linkframe)
{
	const auto original = TARGET_DYNAMIC(EV_SetAction);
	bool useActionMesh = false;

	// Chaos emeralds or Tornado 2
	if (texlist == &texlist_m_em_blue ||
		texlist == &texlist_m_em_green ||
		texlist == &texlist_m_em_white ||
		texlist == &texlist_m_em_purple ||
		texlist == &texlist_m_em_sky ||
		texlist == &texlist_m_em_yellow ||
		texlist == &texlist_m_em_red ||
		texlist == &texlist_m_em_black ||
		texlist == &texlist_tr2a_s_t2b_body)
		mode |= 0x8;

	original(twp, action, texlist, speed, mode, linkframe);
}

// Hook to override depth for Eggman's capture beam
void CaptureBeamFix(NJS_OBJECT* a1, LATE a2, float a3)
{
	switch (current_event)
	{
	case 9: // Sonic and Tails gassed at Casinopolis (Sonic)
	case 11: // Sonic and Tails gassed at Casinopolis (Tails)
	case 53: // Chaos 4 emerges (Sonic)
	case 57: // Chaos 4 emerges (Tails)
	case 139: // Chaos 4 emerges (Knuckles)
		late_z_ofs___ = 8000.0f;
		late_DrawObjectClipMesh(a1, a2, a3);
		late_z_ofs___ = 0.0f;
		break;
	default:
		late_DrawObjectClip(a1, a2, a3);
		break;
	}
}

// Hook to fade out the subtitle box when cutscenes fade out
void CutsceneFadeHookForSubtitleBox(float left, float top, float right, float bottom, float depth, NJS_COLOR color)
{
	color.argb.a = (Uint8)(color.argb.a * (1.0f - CutsceneFadeAlpha / 255.0f));
	DrawRect_DrawNowMaybe(left, top, right, bottom, depth, color.color);
}

// Hook to fade out the subtitle text when cutscenes fade out
void CutsceneFadeHookForSubtitleText(NJS_ARGB* a1)
{
	float alpha = a1->a * (1.0f - CutsceneFadeAlpha / 255.0f);
	if (ModConfig::CutsceneSkipFix == CutsceneSkipConfig::Fadeout)
		SetMaterial(alpha, a1->r, a1->g, a1->b);
	else
		SetMaterialAndSpriteColor(a1);
}

// Hook to fix Gamma's hoverin animation in some cutscenes
void ECGammaCutsceneFix(int wait)
{
	EV_Wait(25);
	task* v112 = EV_GetPlayer(4u);
	EV_SetAction(v112, E102_ACTIONS[49], &texlist_e102, 1.0f, 1, 16);
	moveObject(v112, 0.0f, 1560.0f, 3426.0f, 0.0f, 1535.0f, 3426.0f, 85);
}

// Draw Gamma's damage sprite above his body (callback)
void DrawGammaDamageSprite(int n)
{
	njSetZUpdateMode(0);
	njSetZCompare(7u);
	njDrawSprite3D((NJS_SPRITE*)0x03033144, n, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE | NJD_SPRITE_COLOR | NJD_SPRITE_ANGLE);
	njSetZUpdateMode(1u);
	njSetZCompare(1u);
}

// Hook to draw Gamma's damage sprite above his body
void GammaDamageSpriteFix(NJS_SPRITE* a1, Int n, NJD_SPRITE attr)
{
	late_SetFunc((void(__cdecl*)(void*))DrawGammaDamageSprite, (void*)n, 20000.0f, 0);
}

// Draw the fadeout box for cutscenes
void DisplayCutsceneFadeout(int alpha)
{
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	njSetZCompare(7u);
	njSetZUpdateMode(0);
	late_DrawBoxFill2D(0.0f, 0.0f, (float)HorizontalResolution, (float)VerticalResolution, 22048.0f, alpha << 24, LATE_WZ);
	njSetZUpdateMode(1);
	npSetZCompare();
}

// Fix the walking guy in Amy's intro
void WalkingGuyFix(task* tp, NJS_OBJECT* op, NJS_MOTION* mp, NJS_TEXLIST* lp, float speed, int mode, int linkframe)
{
	EV_SetMotion(tp, op, MODEL_SS_PEOPLE_MOTIONS[0], lp, speed, mode, linkframe);
}

void Event_Init()
{
	PatchModels_Event();
	ReplacePVM("ICM0060");
	ReplacePVM("ICM006D");
	ReplacePVM("ICM00C3");
	ReplacePVM("ICM00C5");
	ReplacePVM("ICM00C7");
	ReplacePVM("ICM0142");
	ReplacePVM("LOCKET");
	ReplacePVM("CHAOS1");
	ReplacePVM("F_EGG_ZANGAI");
	ReplacePVM("FPACK");
	ReplacePVM("MP_10000_POLICE");
	ReplacePVM("FROG");
	ReplacePVM("FROG1");
	ReplacePVM("FROG2");
	ReplacePVM("FROG3");
	ReplacePVM("SHAPE_FROG");
	ReplacePVM("SHAPE_FROG_2");
	ReplacePVM("F0000_FROG");
	ReplacePVM("KAOS_EME");
	ReplacePVM("TR2CRASH");
	ReplacePVM("WAVE7_WA");
	ReplacePVR("CAPTUREBEAM");
	ReplacePVM("CAPTUREBEAM");
	ReplacePVM("BULLET");
	ReplacePVM("EV_CHAOS1");
	ReplacePVM("EV_E101KAI");
	ReplacePVM("EV_E101_BODY");
	ReplacePVM("EV_E101_FUN");
	ReplacePVM("EV_E103_BODY");
	ReplacePVM("EV_E103_FUN");
	ReplacePVM("EV_E104_BODY");
	ReplacePVM("EV_E104_FUN");
	ReplacePVM("EV_E105_BODY");
	ReplacePVM("EV_E105_FUN");
	ReplacePVM("ICM0001_3");
	ReplacePVM("ICM0001_5");
	ReplacePVM("EV_EGGMAN_BODY");
	ReplacePVM("EV_EGGMOBILE1");
	ReplacePVM("EV_EGGMOBLE0DM");
	//ReplacePVM("EV_EGGMOBLE0"); // DC Characters
	ReplacePVM("EV_EGGMOBLE1");
	ReplacePVM("EV_EGGMOBLE2");
	ReplacePVM("EV_HELI");
	ReplacePVM("EV_K_PATYA");
	ReplacePVM("EV_SPOTLIGHT");
	ReplacePVM("EV_S_MSBODY");
	ReplacePVM("EV_S_T2C_BODY");
	ReplacePVM("EV_TR1");
	ReplacePVM("EV_TR2_BIG");
	ReplacePVM("BO1_MODEL");
	ReplacePVM("E103OLD");
	ReplacePVM("E104OLD");
	ReplacePVM("E105");
	ReplacePVM("E105OLD");
	ReplacePVM("FAT_B");
	ReplacePVM("GR1_MODEL");
	ReplacePVM("GR_10000_GIRL1");
	ReplacePVM("OL_10000");
	ReplacePVM("OY_10000");
	ReplacePVM("M_EM_BLACK");
	ReplacePVM("M_EM_BLUE");
	ReplacePVM("M_EM_GREEN");
	ReplacePVM("M_EM_PURPLE");
	ReplacePVM("M_EM_RED");
	ReplacePVM("M_EM_SKY");
	ReplacePVM("M_EM_WHITE");
	ReplacePVM("M_EM_YELLOW");
	ReplacePVM("M_TR_S");
	ReplacePVM("MGHAND");
	ReplacePVM("MIZUBASIRA");
	ReplacePVM("L_SIBUKI");
	ReplacePVM("ER_9000_EGGMANROBO");
	ReplacePVM("EGG_MISSILE");
	ReplacePVM("EME_KIRAN");
	AddWhiteDiffuseMaterial(&object_sspatcar_body_body.basicdxmodel->mats[3]); // Police car in Sonic's intro
	AddWhiteDiffuseMaterial(&object_sspatcar_body_body.basicdxmodel->mats[4]); // Police car in Sonic's intro
	AddWhiteDiffuseMaterial(&object_sspatcar_body_body.basicdxmodel->mats[5]); // Police car in Sonic's intro
	WriteCall((void*)0x006AD0A1, WalkingGuyFix); // Walking guy in Amy's intro
	WriteData((short*)0x006AD16C, (short)460); // Walking guy speed
	WriteData((float**)0x006F7F52, &EggWalkerCarOffset); // Fixed cars not showing up in the cutscene before Egg Walker
	*(NJS_OBJECT*)0x2F67B78 = *LoadModel("system\\data\\event\\model\\tr2crash\\tr2crash.nja.sa1mdl"); // Tornado 2 crashed
	((NJS_OBJECT*)0x2DA770C)->basicdxmodel->mats[2].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA770C)->basicdxmodel->mats[5].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA3064)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA6364)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA2A2C)->basicdxmodel->mats[3].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA1014)->basicdxmodel->mats[3].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA3064)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA1444)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA3CEC)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	((NJS_OBJECT*)0x2DA39EC)->basicdxmodel->mats[1].attrflags |= NJD_FLAG_IGNORE_LIGHT; // E101R model in cutscenes
	// Cancel cutscenes with D-Pad Up+Down
	if (ModConfig::CutsceneSkipFix != CutsceneSkipConfig::DefaultDX)
	{
		if (ModConfig::CutsceneSkipFix != CutsceneSkipConfig::NoSkip)
			WriteCall((void*)0x004314F9, InputHookForCutscenes);
		DisplayTitleCard_t = new Trampoline(0x0047E170, 0x0047E175, DisplayTitleCard_r); // Reset values
		WriteData<1>((char*)0x00431520, 0x30); // Use D-Pad Up+Down instead of A or Start
		if (ModConfig::CutsceneSkipFix == CutsceneSkipConfig::Fadeout)
		{
			// Force subtitle constant material alpha to fade out. There's no other way because subtitles are drawn after late_exec().
			WriteCall((void*)0x0040D69C, CutsceneFadeHookForSubtitleBox);
			WriteCall((void*)0x0040D78A, CutsceneFadeHookForSubtitleText);
		}
	}
	// Fix for cutscene transitions
	if (ModConfig::CutsceneTransitionFix)
	{
		WriteCall((void*)0x004311E3, FixCutsceneTransition); // Main thread
		WriteData<5>((void*)0x0043131D, 0x90u); // Skipping cutscenes
	}
	// Various cutscene hooks
	WriteCall((void*)0x006D9DA7, ECGammaCutsceneFix); // Fix Gamma hover scene in Sonic's story
	WriteCall((void*)0x006B8D5E, ECGammaCutsceneFix); // Fix Gamma hover scene in Tails' story
	WriteCall((void*)0x006F2D96, GammaDamageSpriteFix); // Make the electric sparks render above Gamma's body in post-fight cutscenes
	WriteCall((void*)0x006A65CB, DrawCutsceneZeroShadow); // Fix Zero's shadow overlapping Amy's in a cutscene
	WriteCall((void*)0x006F37DC, CaptureBeamFix); // The beam that collects the emeralds
	WriteCall((void*)0x006EE43F, ComeOnChaosTimeToEat); // Environment mapping effect on Chaos' puddle before Chaos 0 emerges
	WriteCall((void*)0x007AF877, RenderChaosPuddle_Last); // Chaos puddle in Final Story cutscenes
	EV_SetAction_t = new Trampoline(0x0042FE00, 0x0042FE06, EV_SetAction_r); // Force drawing function for some event models
	// Event helicopter
	_action_c0_heli_body.object = LoadModel("system\\data\\event\\model\\heli\\heli_body.nja.sa1mdl"); // Edited hierarchy
	_action_c0_heli_body.object->child->child->child->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // UV-less stuff fix
	AddWhiteDiffuseMaterial(&_action_c0_heli_body.object->child->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[0]);
	AddWhiteDiffuseMaterial(&_action_c0_heli_body.object->child->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
	// Other stuff
	ForceObjectSpecular_Object(&object_missile_body_body, false); // Egg Missile cutscene model - maybe unnecessary?
	evpvm_125[0].pname = "ICM0001_5"; // Higher quality background in Sonic story
	// Metal Sonic in Amy's story
	AddWhiteDiffuseMaterial(&object_ev_s_msbody_s_msbody.basicdxmodel->mats[1]);
	AddWhiteDiffuseMaterial(&object_ev_s_msbody_s_msbody.basicdxmodel->mats[2]);
	AddWhiteDiffuseMaterial(&object_ev_s_msbody_s_msbody.basicdxmodel->mats[3]);
	AddWhiteDiffuseMaterial(&object_ev_s_msbody_s_msbody.basicdxmodel->mats[4]);
	// Egg Carrier 2 crash in Perfect Chaos cutscene: camera angle fix
	WriteData((float*)0x0065D8D1, 837.418f); // X1
	WriteData((float*)0x0065D8CC, 412.38f); // Y1
	WriteData((float*)0x0065D8C7, -406.796f); // Z1
	WriteData((int*)0x0065D8E3, 65238); // XA1
	WriteData((int*)0x0065D8DE, 29421); // YA1
	WriteData((float*)0x0065D8FC, 1148.37f); // X2
	WriteData((float*)0x0065D8F7, 423.5f); // Y2 
	WriteData((float*)0x0065D8F2, -325.65f); // Z2
	WriteData((int*)0x0065D912, 64083); // XA2
	WriteData((int*)0x0065D90D, 28705); // YA2
}

void Event_OnInput()
{
	// Input hook for cutscenes
	if (ModConfig::CutsceneSkipFix < CutsceneSkipConfig::NoSkip && !SkipPressed_Cutscene && !DemoPlaying)
		if (EV_MainThread_ptr != 0 && per[0]->press & Buttons_Start)
		{
			SkipPressed_Cutscene = true;
			if (ModConfig::DebugVerbose >= DebugVerboseConfig::Levels)
				PrintDebug("Cutscene skip pressed!\n");
		}
}

void Event_OnFrame()
{
	// Cutscene skip
	if (ModConfig::CutsceneSkipFix <= CutsceneSkipConfig::Immediate && SkipPressed_Cutscene)
	{
		if (ModConfig::CutsceneSkipFix == CutsceneSkipConfig::Fadeout)
		{
			switch (CutsceneFadeoutMode)
			{
			case CutsceneFade::FadeoutEnabled:
				CutsceneFadeAlpha += 8;
				if (CutsceneFadeAlpha >= 255)
				{
					CutsceneFadeAlpha = 255;
					CutsceneFadeoutMode = CutsceneFade::FadeoutWait;
				}
				break;
			case CutsceneFade::FadeoutWait:
				if (EV_MainThread_ptr != nullptr)
				{
					if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
						PrintDebug("Trying to skip the cutscene...\n");
				}
				else
				{
					CutsceneFadeoutMode = CutsceneFade::FadeoutEnd;
					if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
						PrintDebug("Cutscene skipped!\n");
				}
				break;
			case CutsceneFade::FadeoutEnd:
				CutsceneFadeAlpha -= 8;
				if (CutsceneFadeAlpha <= 0)
				{
					CutsceneFadeAlpha = 0;
					CutsceneFadeoutMode = CutsceneFade::FadeoutEnabled;
					SkipPressed_Cutscene = false;
				}
				break;
			}
			DisplayCutsceneFadeout(CutsceneFadeAlpha);
		}
		else if (ModConfig::CutsceneSkipFix == CutsceneSkipConfig::Immediate)
		{
			if (EV_MainThread_ptr != nullptr)
			{
				CutsceneFadeoutMode = CutsceneFade::FadeoutWait;
				if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
					PrintDebug("Trying to skip the cutscene...\n");
			}
			else
			{
				CutsceneFadeoutMode = CutsceneFade::FadeoutEnabled;
				CutsceneFadeAlpha = 0;
				SkipPressed_Cutscene = false;
				if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
					PrintDebug("Cutscene skipped!\n");
			}
		}
	}
	// Chaos 1 puddle
	if (current_event != -1)
	{
		if (ssStageNumber == LevelIDs_MysticRuins && current_event != 57)
			object_ev_chaos0_manju_ev_chaos0_manju.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT;
		else
			object_ev_chaos0_manju_ev_chaos0_manju.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
	}
}