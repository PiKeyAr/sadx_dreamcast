#pragma once

#include <SADXModLoader.h>

// Lighting functions
bool ForceWhiteDiffuse(NJS_MATERIAL* material, uint32_t flags);
bool ForceWhiteDiffuse_Night(NJS_MATERIAL* material, uint32_t flags);
bool DisableAlphaRejection(NJS_MATERIAL* material, uint32_t flags);