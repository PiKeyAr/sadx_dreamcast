#include "stdafx.h"

void PatchModels_ShareObj()
{
	// Shareobj
	// dxpc\shareobj\bg\models\tp_nbg2.nja.sa1mdl
	//((NJS_MATERIAL*)0x038A6460)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x038A6474)->diffuse.color = 0xFF010000;

	// dxpc\shareobj\common\models\tpobj7_dpaneru.nja.sa1mdl
	*(NJS_TEX*)0x038BE3E0 = { 0, -510 };
	*(NJS_TEX*)0x038BE3E4 = { 510, -510 };
	*(NJS_TEX*)0x038BE3E8 = { 0, 255 };
	*(NJS_TEX*)0x038BE3EC = { 510, 255 };

	// dxpc\shareobj\common\models\tpobj_arch02a.nja.sa1mdl
	*(NJS_TEX*)0x038BF2FC = { 255, -255 };
	*(NJS_TEX*)0x038BF304 = { 127, -255 };
	*(NJS_TEX*)0x038BF30C = { 127, -255 };
	*(NJS_TEX*)0x038BF314 = { 255, -255 };

	// dxpc\shareobj\common\models\tpobj_jumpdai.nja.sa1mdl
	*(NJS_TEX*)0x038C0488 = { 0, -510 };
	*(NJS_TEX*)0x038C048C = { 1020, -510 };
	*(NJS_TEX*)0x038C0490 = { 0, 255 };
	*(NJS_TEX*)0x038C0494 = { 1020, 255 };

	// dxpc\shareobj\common\models\tpobj_slight1.nja.sa1mdl
		//Model too different

	// dxpc\shareobj\common\models\tpobj_slight2.nja.sa1mdl
		//Model too different

	// dxpc\shareobj\common\models\tpobj_stop.nja.sa1mdl
	*(NJS_TEX*)0x038C5028 = { 0, 234 };
	*(NJS_TEX*)0x038C502C = { 130, 254 };
	*(NJS_TEX*)0x038C5030 = { 0, 255 };
	*(NJS_TEX*)0x038C5034 = { 130, 254 };
	*(NJS_TEX*)0x038C5038 = { 0, 234 };
	*(NJS_TEX*)0x038C503C = { 0, 255 };
	*(NJS_TEX*)0x038C5040 = { 130, 254 };
	*(NJS_TEX*)0x038C5044 = { 0, 234 };
	*(NJS_TEX*)0x038C5048 = { 0, 255 };
	*(NJS_TEX*)0x038C504C = { 0, 255 };
	*(NJS_TEX*)0x038C5050 = { 0, 234 };
	*(NJS_TEX*)0x038C5054 = { 130, 254 };
	*(NJS_TEX*)0x038C5138 = { 126, 221 };
	*(NJS_TEX*)0x038C513C = { 127, -227 };
	*(NJS_TEX*)0x038C5140 = { 0, 253 };
	*(NJS_TEX*)0x038C5144 = { 0, -251 };
	*(NJS_TEX*)0x038C5148 = { 0, -251 };
	*(NJS_TEX*)0x038C514C = { 0, 253 };
	*(NJS_TEX*)0x038C5150 = { 127, -227 };
	*(NJS_TEX*)0x038C5154 = { 126, 221 };

	// dxpc\shareobj\common\models\tp_obj_planet03.nja.sa1mdl
	*(NJS_TEX*)0x038BDAC8 = { 506, -251 };
	*(NJS_TEX*)0x038BDACC = { 506, 251 };
	*(NJS_TEX*)0x038BDAD0 = { 3, -251 };
	*(NJS_TEX*)0x038BDAD4 = { 3, 251 };
}