#include "stdafx.h"

void PatchModels_STG12()
{
	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_big.nja.sa1mdl
		// Not replaced - causes bugs

	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_bigcircle.nja.sa1mdl
		// Full replacement

	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_hiyoko.nja.sa1mdl
	((NJS_MATERIAL*)0x0180F608)->attrflags = 0x94B02400;

	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_j1h.nja.sa1mdl
	((NJS_MATERIAL*)0x0180F824)->attrflags = 0x94B02400;

	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_j2h.nja.sa1mdl
	((NJS_MATERIAL*)0x0180FA60)->attrflags = 0x94B02400;

	// dxpc\stg12_hotshelter\act01\suimen\suimen_wt_small1h.nja.sa1mdl
	((NJS_MATERIAL*)0x0180FBE8)->attrflags = 0x94B02400;

	// dxpc\stg12_hotshelter\act02\models\bluebox__.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act02\models\orangebox__.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act02\models\purplebox__.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act02\models\yellowbox__.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_cl_crg.nja.sa1mdl
	((NJS_MATERIAL*)0x017F3948)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x017F3948)->exponent = 6;
	((NJS_MATERIAL*)0x017F3948)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x017F3948)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\act04\models\cargo_cl_crgfront.nja.sa1mdl
	((NJS_MATERIAL*)0x017F36D8)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x017F36D8)->exponent = 6;
	((NJS_MATERIAL*)0x017F36D8)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x017F36D8)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\act04\models\cargo_contenaa.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_crg.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_crgfront.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_midcontena.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_renketubo.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_tnnlami.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\cargo_tnnlblue.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\act04\models\enecrg_cl_enecrg.nja.sa1mdl
	((NJS_MATERIAL*)0x017F4530)->attrflags = 0x94002400;
	((NJS_MATERIAL*)0x017F4530)->exponent = 6;
	((NJS_MATERIAL*)0x017F4530)->diffuse.color = 0xFFBFBFBF;
	((NJS_MATERIAL*)0x017F4530)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\act04\models\enecrg_cl_enehuta.nja.sa1mdl
	((NJS_MATERIAL*)0x017F4880)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\act04\models\enecrg_enemycont.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\biribiria_br.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\blhs_hotblbase.nja.sa1mdl
		//Model too different
	
	// dxpc\stg12_hotshelter\common\models\breakkabe_after.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\breakkabe_before.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\breakkabe_br_part.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\clain_clnull.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\computer.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\daruma_ue.nja.sa1mdl
	*(NJS_TEX*)0x01853288 = { 510, 255 };
	*(NJS_TEX*)0x0185328C = { 510, 0 };
	*(NJS_TEX*)0x01853298 = { 510, 255 };
	*(NJS_TEX*)0x0185329C = { 510, 0 };
	*(NJS_TEX*)0x018532A8 = { 510, 255 };
	*(NJS_TEX*)0x018532AC = { 510, 0 };
	*(NJS_TEX*)0x018532B8 = { 510, 255 };
	*(NJS_TEX*)0x018532BC = { 510, 0 };

	// dxpc\stg12_hotshelter\common\models\drmcan_drmcan.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\eel1_doa_a.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\eel1_doa_b.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\eel1_elvater.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\fens_nc_fen.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\gt_bgatecente.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\gt_gateyoko.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\g_sasae1a_sasae1a.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\g_sasae1b_sasae1b.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\g_sasae1c_sasae1c.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\g_sasae1d_sasae1d.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\hasi_coritori.nja.sa1mdl
		//Collision whatever

	// dxpc\stg12_hotshelter\common\models\hasi_enhasi.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\hasi_enhasi_e.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\hasi_enhasi_e_kori.nja.sa1mdl
		//Collision

	// dxpc\stg12_hotshelter\common\models\hasi_hasirakori.nja.sa1mdl
		//Collision

	// dxpc\stg12_hotshelter\common\models\hikari01a_light.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\kaidan_hontai.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\kaitendai_colicoli.nja.sa1mdl
		//Collision

	// dxpc\stg12_hotshelter\common\models\kaitendai_daidai.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\kaitenhyouji_hikari_left.nja.sa1mdl
	((NJS_MATERIAL*)0x0182EE2C)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\common\models\kaitenhyouji_hikari_right.nja.sa1mdl
	((NJS_MATERIAL*)0x0182ED20)->specular.color = 0xFFFFFFFF;

	// dxpc\stg12_hotshelter\common\models\kaitenhyouji_meter.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\kaitenkey_dai.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\kaitenkey_daionly.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\lightdw_daiv.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\ligyel_ligyy.nja.sa1mdl
	((NJS_MATERIAL*)0x01826F38)->attrflags = 0x94A9A400;
	((NJS_MATERIAL*)0x01826F4C)->attrflags = 0x94A9A400;
	((NJS_MATERIAL*)0x01826F60)->attrflags = 0x9429A400;
	((NJS_MATERIAL*)0x01826F74)->attrflags = 0x9429A400;
	((NJS_MATERIAL*)0x01826C80)->attrflags = 0x8639A440;
	((NJS_MATERIAL*)0x01826C94)->attrflags = 0x9629A400;

	// dxpc\stg12_hotshelter\common\models\redkem_ent_a.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\sen_br_parts.nja.sa1mdl
	*(NJS_TEX*)0x0181CBD0 = { 331, -137 };
	*(NJS_TEX*)0x0181CBD4 = { 467, -46 };
	*(NJS_TEX*)0x0181CBD8 = { 221, 177 };
	*(NJS_TEX*)0x0181CBDC = { 408, 255 };
	*(NJS_TEX*)0x0181CAB4 = { 221, 177 };
	*(NJS_TEX*)0x0181CAB8 = { 77, 33 };
	*(NJS_TEX*)0x0181CABC = { 331, -137 };
	*(NJS_TEX*)0x0181C990 = { 77, -543 };
	*(NJS_TEX*)0x0181C994 = { 0, -356 };
	*(NJS_TEX*)0x0181C998 = { 298, -296 };
	*(NJS_TEX*)0x0181C99C = { 0, -153 };
	*(NJS_TEX*)0x0181C9A0 = { 331, -137 };
	*(NJS_TEX*)0x0181C710 = { 331, -137 };
	*(NJS_TEX*)0x0181C714 = { 77, 33 };
	*(NJS_TEX*)0x0181C718 = { 0, -153 };
	*(NJS_TEX*)0x0181C544 = { 77, -543 };
	*(NJS_TEX*)0x0181C548 = { 390, -431 };
	*(NJS_TEX*)0x0181C54C = { 298, -296 };
	*(NJS_TEX*)0x0181C2F0 = { 551, -463 };
	*(NJS_TEX*)0x0181C2F4 = { 390, -431 };
	*(NJS_TEX*)0x0181C2F8 = { 408, -765 };
	*(NJS_TEX*)0x0181C2FC = { 221, -687 };
	*(NJS_TEX*)0x0181C174 = { 798, -687 };
	*(NJS_TEX*)0x0181C178 = { 551, -463 };
	*(NJS_TEX*)0x0181C17C = { 611, -765 };
	*(NJS_TEX*)0x0181C180 = { 408, -765 };
	*(NJS_TEX*)0x0181BFA0 = { 798, -687 };
	*(NJS_TEX*)0x0181BFA4 = { 688, -372 };
	*(NJS_TEX*)0x0181BFA8 = { 551, -463 };
	*(NJS_TEX*)0x0181BE44 = { 798, -687 };
	*(NJS_TEX*)0x0181BE48 = { 942, -543 };
	*(NJS_TEX*)0x0181BE4C = { 688, -372 };
	*(NJS_TEX*)0x0181BC00 = { 942, -543 };
	*(NJS_TEX*)0x0181BC04 = { 1020, -356 };
	*(NJS_TEX*)0x0181BC08 = { 688, -372 };
	*(NJS_TEX*)0x0181BC0C = { 721, -213 };
	*(NJS_TEX*)0x0181BA28 = { 1020, -356 };
	*(NJS_TEX*)0x0181BA2C = { 1020, -153 };
	*(NJS_TEX*)0x0181BA30 = { 721, -213 };
	*(NJS_TEX*)0x0181BA34 = { 942, 33 };
	*(NJS_TEX*)0x0181BA38 = { 629, -78 };
	*(NJS_TEX*)0x0181BA3C = { 798, 177 };
	*(NJS_TEX*)0x0181B7F4 = { 798, 177 };
	*(NJS_TEX*)0x0181B7F8 = { 629, -78 };
	*(NJS_TEX*)0x0181B7FC = { 611, 255 };
	*(NJS_TEX*)0x0181B800 = { 467, -46 };
	*(NJS_TEX*)0x0181B804 = { 408, 255 };
	*(NJS_TEX*)0x0181B6D8 = { 77, -543 };
	*(NJS_TEX*)0x0181B6DC = { 221, -687 };
	*(NJS_TEX*)0x0181B6E0 = { 390, -431 };

	// dxpc\stg12_hotshelter\common\models\suisoukazari1a_hontai.nja.sa1mdl
		//Model too different
	
	// dxpc\stg12_hotshelter\common\models\suisoukazari2a_hontai.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\syoumei_syoumei1h.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\tobira_hot_door1h.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\ukidoor_ukidoor.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\ukijima_ukiasiba.nja.sa1mdl
		//Model too different

	// dxpc\stg12_hotshelter\common\models\wc_doa_doa.nja.sa1mdl
	*(NJS_TEX*)0x018131E0 = { 509, -255 };
	*(NJS_TEX*)0x018131E4 = { 0, -255 };
	*(NJS_TEX*)0x018131E8 = { 509, 254 };
	*(NJS_TEX*)0x018131EC = { 0, 254 };
	*(NJS_TEX*)0x018131F0 = { 0, -254 };
	*(NJS_TEX*)0x018131F4 = { 510, -254 };
	*(NJS_TEX*)0x018131F8 = { 0, 254 };
	*(NJS_TEX*)0x018131FC = { 510, 254 };
}