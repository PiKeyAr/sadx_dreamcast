#include "stdafx.h"

void PatchModels_ADV0100()
{
	HMODULE ADV01MODELS = GetModuleHandle(L"ADV01MODELS");
	// dxpc\adv01_eggcarrierab\sea\ecsc_s_sora_hare.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0025A770)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0025A774)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0025A778)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0025A77C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv01_eggcarrierab\sky\ecsc_s_sitakumo.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0024BD34)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv01_eggcarrierab\sky\ecsc_s_sora.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0024AD94)->diffuse.color = 0xFF006AFF;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0024AD98)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x0024AD9C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv01_eggcarrierab\transform\ecf_bf_s_bodya.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\eca_cl_s_msto.nja.sa1mdl
		//Collision

	// dxpc\adv01_eggcarrierab\common\models\eca_s_msto.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\eda_o_lift.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edb_o_daiza.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edb_o_isu.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edb_o_kasa.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edb_o_lbed.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edb_o_mizu.nja.sa1mdl
		//Water model not replaced

	// dxpc\adv01_eggcarrierab\common\models\edb_wt_o_mizu1a.nja.sa1mdl
		//Water model not replaced

	// dxpc\adv01_eggcarrierab\common\models\edv_cg_s_stdoor.nja.sa1mdl
		//Collision

	// dxpc\adv01_eggcarrierab\common\models\edv_f_lbody.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F20) = { 74, 77 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F24) = { 255, 3 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F28) = { 255, 254 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F2C) = { 435, 77 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F30) = { 510, 254 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F34) = { 74, 77 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F3C) = { 255, 254 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F40) = { 435, 181 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F44) = { 510, 3 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F48) = { 255, 3 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F4C) = { 435, 181 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F50) = { 255, 254 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F54) = { 255, 3 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209F58) = { 74, 181 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209B9C) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209BA0) = { 510, 255 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209BAC) = { 510, 7 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x00209BB0) = { 510, 255 };

	// dxpc\adv01_eggcarrierab\common\models\edv_k_hlicl.nja.sa1mdl
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x00202090)->attr_texId = 0;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x00202090)->attrflags = 0x94000400;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x00202090)->exponent = 6;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x00202090)->diffuse.color = 0xFFBFBFBF;
	//((NJS_MATERIAL*)(size_t)ADV01MODELS + 0x00202090)->specular.color = 0xFFFFFFFF;

	// dxpc\adv01_eggcarrierab\common\models\edv_k_hlift.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edv_k_monorb.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC3C0) = { 3, 5 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC3C4) = { 506, 5 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC3C8) = { 3, 253 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC3CC) = { 506, 253 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC0B8) = { 3, -504 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC0BC) = { 3, 249 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC0C0) = { 506, -504 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001FC0C4) = { 506, 249 };

	// dxpc\adv01_eggcarrierab\common\models\edv_k_monorf.nja.sa1mdl
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F83A4) = { 506, 5 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F83AC) = { 506, 253 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F8098) = { 3, -504 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F809C) = { 3, 249 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F80A0) = { 506, -504 };
	*(NJS_TEX*)((size_t)ADV01MODELS + 0x001F80A4) = { 506, 249 };

	// dxpc\adv01_eggcarrierab\common\models\edv_s_dorup.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edv_s_ermdoor.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\edv_s_stdoor.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\era_cl_s_egseat.nja.sa1mdl
		//Collision

	// dxpc\adv01_eggcarrierab\common\models\era_s_seatrt.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\futa_s_rmfuta.nja.sa1mdl
		//Model too different

	// dxpc\adv01_eggcarrierab\common\models\gun_s_guna.nja.sa1mdl
		//Model too different
}