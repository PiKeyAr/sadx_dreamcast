#include "stdafx.h"

void PatchModels_STG04()
{
	// dxpc\stg04_highway\common\models\crane_rail.nja.sa1mdl
	((NJS_MATERIAL*)0x02671BAC)->attrflags = 0x94312400;
	((NJS_MATERIAL*)0x02671BC0)->attrflags = 0x94342400;
	*(NJS_TEX*)0x02671E18 = { 0, -4845 };
	*(NJS_TEX*)0x02671E1C = { 510, -4845 };
	*(NJS_TEX*)0x02671E20 = { 0, 255 };
	*(NJS_TEX*)0x02671E24 = { 510, 255 };
	*(NJS_TEX*)0x02671E28 = { 510, -4845 };
	*(NJS_TEX*)0x02671E2C = { 0, -4845 };
	*(NJS_TEX*)0x02671E30 = { 510, 255 };
	*(NJS_TEX*)0x02671E34 = { 0, 255 };
	*(NJS_TEX*)0x02671E38 = { 0, -4845 };
	*(NJS_TEX*)0x02671E3C = { 510, -4845 };
	*(NJS_TEX*)0x02671E40 = { 0, 255 };
	*(NJS_TEX*)0x02671E44 = { 510, 255 };
	*(NJS_TEX*)0x02671E48 = { 510, -4845 };
	*(NJS_TEX*)0x02671E4C = { 0, -4845 };
	*(NJS_TEX*)0x02671E50 = { 510, 255 };
	*(NJS_TEX*)0x02671E54 = { 0, 255 };
	*(NJS_TEX*)0x02671E58 = { 510, -4845 };
	*(NJS_TEX*)0x02671E5C = { 0, -4845 };
	*(NJS_TEX*)0x02671E60 = { 510, 255 };
	*(NJS_TEX*)0x02671E64 = { 0, 255 };
	*(NJS_TEX*)0x02671E68 = { 0, 255 };
	*(NJS_TEX*)0x02671E6C = { 510, 255 };
	*(NJS_TEX*)0x02671E70 = { 0, -4845 };
	*(NJS_TEX*)0x02671E74 = { 510, -4845 };
	*(NJS_TEX*)0x02671E78 = { 0, -4845 };
	*(NJS_TEX*)0x02671E7C = { 510, -4845 };
	*(NJS_TEX*)0x02671E80 = { 0, 255 };
	*(NJS_TEX*)0x02671E84 = { 510, 255 };
	*(NJS_TEX*)0x02671E88 = { 510, 255 };
	*(NJS_TEX*)0x02671E8C = { 0, 255 };
	*(NJS_TEX*)0x02671E90 = { 510, -4845 };
	*(NJS_TEX*)0x02671E94 = { 0, -4845 };

	// dxpc\stg04_highway\common\models\esc2_escalator2_2.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\esc_escalator1.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\flydai3_flydai_2.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_cube_1.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_ga.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gb.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gc.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gd.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_ge.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gf.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gg.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gh.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gj.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gk.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gl.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gm.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\glass_gn.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\heli_body.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\soni_dai_soni_dai.nja.sa1mdl
	*(NJS_TEX*)0x02674764 = { 0, -255 };
	*(NJS_TEX*)0x02674768 = { 510, 255 };
	*(NJS_TEX*)0x0267476C = { 510, -255 };

	// dxpc\stg04_highway\common\models\stobj_tokei.nja.sa1mdl
		// Full replacement

	// dxpc\stg04_highway\common\models\stobj_tokei.nja.sa1mdl
	*(NJS_TEX*)0x026956D8 = { 510, 255 };
	*(NJS_TEX*)0x026956E0 = { 510, 190 };
	*(NJS_TEX*)0x026956E4 = { 0, 190 };
	*(NJS_TEX*)0x026956E8 = { 510, 255 };
	*(NJS_TEX*)0x026956F0 = { 510, 190 };
	*(NJS_TEX*)0x026956F4 = { 0, 190 };
	*(NJS_TEX*)0x026956F8 = { 510, 255 };
	*(NJS_TEX*)0x02695700 = { 510, 190 };
	*(NJS_TEX*)0x02695704 = { 0, 190 };
	*(NJS_TEX*)0x02695708 = { 510, 255 };
	*(NJS_TEX*)0x02695710 = { 510, 190 };
	*(NJS_TEX*)0x02695714 = { 0, 190 };
	*(NJS_TEX*)0x0269571C = { 0, -255 };
	*(NJS_TEX*)0x02695720 = { 510, 255 };
	*(NJS_TEX*)0x02695724 = { 510, -255 };

	// dxpc\stg04_highway\common\models\st_clamp.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\st_tank_b_body.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\st_tank_c_body.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\st_tank_b_body_d01.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\st_tank_b_body_d03.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\tai_dai_tai_dai.nja.sa1mdl
	*(NJS_TEX*)0x02679C14 = { 0, -255 };
	*(NJS_TEX*)0x02679C18 = { 510, 255 };
	*(NJS_TEX*)0x02679C1C = { 510, -255 };

	// dxpc\stg04_highway\common\models\turnasi_oya.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\no_unite\fountain_w01.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\no_unite\fountain_w02.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\no_unite\fountain_water.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\no_unite\kuruma4_mini.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\no_unite\r_kuruma3_wagon.nja.sa1mdl
		//Full replacement

	// dxpc\stg04_highway\common\models\no_unite\sskuruma2_d_body_d.nja.sa1mdl
		//Model too different

	// dxpc\stg04_highway\common\models\no_unite\ssrotyucar_body.nja.sa1mdl
		//Model too different
}