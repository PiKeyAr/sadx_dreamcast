#include "stdafx.h"
#include "config.h"

DebugVerboseConfig ModConfig::DebugVerbose; // Output debug messages for file replacement etc.
bool ModConfig::CutsceneTransitionFix; // Fix missing camera transitions when cutscenes end
CutsceneSkipConfig ModConfig::CutsceneSkipFix; // Fade out cutscenes
ImpressFontConfig ModConfig::FontReplacementMode; // Replace subtitle font
SetFileConfig ModConfig::SETReplacement; // Select which SET files should be used for replacement

bool ModConfig::ColorizeFont; // Imitate SA1DC subtitle font color
bool ModConfig::DisableFontSmoothing; // Make subtitle font sharper (1)
bool ModConfig::DisableFontFiltering; // Make subtitle font sharper (2)
bool ModConfig::EnableLSDFix; // Patch Light Speed Dash distance checks to work more like SA1 DC
bool ModConfig::FPSLock; // Lock levels to 30 FPS except Twinkle Circuit
bool ModConfig::EnableDCRipple;  // Water ripple from the Dreamcast version
bool ModConfig::EnableWhiteDiffuse; // Imitate the look of some models in SA1 which get a fullbright diffuse palette
bool ModConfig::RestoreHumpAnimations; // Restore frozen item hold animations
bool ModConfig::JapaneseLostWorldSpikes; // Change Lost World spikes behavior to be more like SA1 JP

bool ModConfig::BigBeltFix; // Transparency fix for Big's belt
bool ModConfig::FixHeldObjects; // Improve positioning of held/throwable items like Ice Key etc.
bool ModConfig::RestoreYButton; // Restore the functionality of the Y button like on DC

bool ModConfig::RemoveSetUpPad; // Remove "Set up pad" from the Pause menu
bool ModConfig::RemoveMap; // Remove "Map" from the Pause menu
bool ModConfig::RemoveCamera; // Remove "Camera" from the Pause menu

bool ModConfig::EnableWindowTitle; // Change window title to "Sonic Adventure"
bool ModConfig::EnableDCBranding; // Enable Dreamcast controller graphics, menus etc.

bool ModConfig::IamStupidAndIWantFuckedUpOcean; // Revert Emerald Coast skybox fixes
bool ModConfig::EnableCowgirl;
bool ModConfig::DisableAllVideoStuff; // Disable all video enhancements

bool ModConfig::EnabledLevels[43];

bool ModConfig::EnableSkyChaseFixes;

bool ModConfig::EnableDXOceanEffects;
bool ModConfig::DisableChao;
bool ModConfig::ReplaceEggs;
bool ModConfig::ReplaceTrees;
bool ModConfig::ReplaceNameMachine;
FruitConfig ModConfig::ReplaceFruits;
bool ModConfig::EnableLobby;
bool ModConfig::DisableChaoButtonPrompts;

bool ModConfig::EnableSpeedFixes;
bool ModConfig::SuppressWarnings;

bool ModConfig::DLLLoaded_SA1Chars;
bool ModConfig::DLLLoaded_Lantern;
bool ModConfig::DLLLoaded_HDGUI;
bool ModConfig::DLLLoaded_DLCs;
bool ModConfig::DLLLoaded_SoundOverhaul;
bool ModConfig::DLLLoaded_SADXFE;
bool ModConfig::DLLLoaded_Korean;
SonicTeamLogoConfig ModConfig::SonicTeamLogo;
bool ModConfig::RemoveMissionMode;
bool ModConfig::RemoveQuitPrompt;
bool ModConfig::RemoveUnlockMessage;
bool ModConfig::RemoveMarketRingCount;
bool ModConfig::RemoveGameGearGames;
bool ModConfig::ColorizeVideos;
bool ModConfig::SA1Intro;
bool ModConfig::RemoveCream;
bool ModConfig::HUDTweak;
bool ModConfig::DrawNowSaving;

TitleScreenConfig ModConfig::TitleScreenLogoMode;
DemosConfig ModConfig::RestoreDemos;

void LoadModConfig(const char* path)
{
	const std::string s_path(path);	
	// Mod Config stuff
	const std::string s_config_ini(s_path + "\\config.ini");
	const IniFile* const config = new IniFile(s_config_ini);
	// Read config stuff
#ifdef _DEBUG
	ModConfig::DebugVerbose = DebugVerboseConfig::Models;
#endif
	ModConfig::SuppressWarnings = config->getBool("General", "SuppressWarnings", false);
	ModConfig::FPSLock = config->getBool("General", "FPSLock", false);
	ModConfig::RestoreHumpAnimations = config->getBool("General", "RestoreHumpAnimations", true);
	ModConfig::EnableDCRipple = config->getBool("General", "EnableDreamcastWaterRipple", true);
	ModConfig::CutsceneTransitionFix = config->getBool("General", "EnableCutsceneFix", true);
	ModConfig::FontReplacementMode = (ImpressFontConfig)config->getInt("General", "ImpressFont", (int)ImpressFontConfig::Impress);
	ModConfig::CutsceneSkipFix = (CutsceneSkipConfig)config->getInt("General", "CutsceneSkipConfig", (int)CutsceneSkipConfig::Fadeout);
	ModConfig::ColorizeFont = config->getBool("General", "ColorizeFont", true);
	ModConfig::DisableFontSmoothing = config->getBool("General", "DisableFontSmoothing", true);
	ModConfig::DisableFontFiltering = config->getBool("General", "DisableFontFiltering", true);
	ModConfig::EnableLSDFix = config->getBool("Miscellaneous", "EnableLSDFix", false);
	ModConfig::BigBeltFix = config->getBool("Miscellaneous", "BigBeltFix", true);
	ModConfig::RemoveSetUpPad = config->getBool("Branding", "RemoveSetUpPad", false);
	ModConfig::RemoveMap = config->getBool("Branding", "RemoveMap", false);
	ModConfig::RemoveCamera = config->getBool("Branding", "RemoveCamera", false);
	ModConfig::EnableDCBranding = config->getBool("General", "EnableDreamcastBranding", true);
	ModConfig::EnableSpeedFixes = config->getBool("General", "EnableSpeedFixes", true);
	ModConfig::EnableWindowTitle = config->getBool("General", "EnableWindowTitle", true);
	ModConfig::EnabledLevels[LevelIDs_EmeraldCoast] = config->getBool("Levels", "EnableEmeraldCoast", true);
	ModConfig::EnabledLevels[LevelIDs_WindyValley] = config->getBool("Levels", "EnableWindyValley", true);
	ModConfig::EnabledLevels[LevelIDs_TwinklePark] = config->getBool("Levels", "EnableTwinklePark", true);
	ModConfig::EnabledLevels[LevelIDs_SpeedHighway] = config->getBool("Levels", "EnableSpeedHighway", true);
	ModConfig::EnabledLevels[LevelIDs_RedMountain] = config->getBool("Levels", "EnableRedMountain", true);
	ModConfig::EnabledLevels[LevelIDs_SkyDeck] = config->getBool("Levels", "EnableSkyDeck", true);
	ModConfig::EnabledLevels[LevelIDs_LostWorld] = config->getBool("Levels", "EnableLostWorld", true);
	ModConfig::EnabledLevels[LevelIDs_IceCap] = config->getBool("Levels", "EnableIceCap", true);
	ModConfig::EnabledLevels[LevelIDs_Casinopolis] = config->getBool("Levels", "EnableCasinopolis", true);
	ModConfig::EnabledLevels[LevelIDs_FinalEgg] = config->getBool("Levels", "EnableFinalEgg", true);
	ModConfig::EnabledLevels[LevelIDs_HotShelter] = config->getBool("Levels", "EnableHotShelter", true);
	ModConfig::EnabledLevels[LevelIDs_StationSquare] = config->getBool("Levels", "EnableStationSquare", true);
	ModConfig::EnabledLevels[LevelIDs_MysticRuins] = config->getBool("Levels", "EnableMysticRuins", true);
	ModConfig::EnabledLevels[LevelIDs_EggCarrierOutside] = config->getBool("Levels", "EnableEggCarrier", true);
	ModConfig::EnabledLevels[LevelIDs_EggCarrierInside] = config->getBool("Levels", "EnableEggCarrier", true);
	ModConfig::EnabledLevels[LevelIDs_Past] = config->getBool("Levels", "EnablePast", true);
	ModConfig::DisableAllVideoStuff = config->getBool("Videos", "DisableAllVideoStuff", false);
	ModConfig::EnabledLevels[LevelIDs_Chaos0] = config->getBool("Bosses", "EnableChaos0", true);
	ModConfig::EnabledLevels[LevelIDs_Chaos2] = config->getBool("Bosses", "EnableChaos2", true);
	ModConfig::EnabledLevels[LevelIDs_Chaos4] = config->getBool("Bosses", "EnableChaos4", true);
	ModConfig::EnabledLevels[LevelIDs_Chaos6] = config->getBool("Bosses", "EnableChaos6", true);
	ModConfig::EnabledLevels[LevelIDs_PerfectChaos] = config->getBool("Bosses", "EnablePerfectChaos", true);
	ModConfig::EnabledLevels[LevelIDs_EggHornet] = config->getBool("Bosses", "EnableEggHornet", true);
	ModConfig::EnabledLevels[LevelIDs_EggWalker] = config->getBool("Bosses", "EnableEggWalker", true);
	ModConfig::EnabledLevels[LevelIDs_EggViper] = config->getBool("Bosses", "EnableEggViper", true);
	ModConfig::EnabledLevels[LevelIDs_E101] = config->getBool("Bosses", "EnableE101", true);
	ModConfig::EnabledLevels[LevelIDs_Zero] = config->getBool("Bosses", "EnableZeroE101R", true);
	ModConfig::EnabledLevels[LevelIDs_E101R] = config->getBool("Bosses", "EnableZeroE101R", true);
	ModConfig::EnabledLevels[LevelIDs_HedgehogHammer] = config->getBool("Miscellaneous", "EnableHedgehogHammer", true);
	ModConfig::EnabledLevels[LevelIDs_TwinkleCircuit] = config->getBool("Miscellaneous", "EnableTwinkleCircuit", true);
	ModConfig::EnabledLevels[LevelIDs_SandHill] = config->getBool("Miscellaneous", "EnableSandHill", true);
	ModConfig::EnableCowgirl = config->getBool("Miscellaneous", "EnableCasinopolisCowgirl", true);
	ModConfig::JapaneseLostWorldSpikes = config->getBool("Miscellaneous", "JapaneseLostWorldSpikes", false);
	ModConfig::SETReplacement = (SetFileConfig)config->getInt("Miscellaneous", "Use1999SetFiles", (int)SetFileConfig::Optimized);
	ModConfig::FixHeldObjects = config->getBool("Miscellaneous", "FixHeldObjects", true);
	ModConfig::RestoreYButton = config->getBool("Miscellaneous", "RestoreYButton", true);
	ModConfig::IamStupidAndIWantFuckedUpOcean = config->getBool("Miscellaneous", "RevertEmeraldCoastDrawDistance", false);
	ModConfig::EnabledLevels[LevelIDs_SkyChase1] = config->getBool("Miscellaneous", "EnableSkyChaseEnemyModels", true);
	ModConfig::EnabledLevels[LevelIDs_SkyChase2] = config->getBool("Miscellaneous", "EnableSkyChaseEnemyModels", true);
	ModConfig::EnableDXOceanEffects = config->getBool("Miscellaneous", "EnableDXOceanEffects", true);
	ModConfig::EnableWhiteDiffuse = config->getBool("Miscellaneous", "EnableWhiteDiffuse", true);
	ModConfig::EnabledLevels[LevelIDs_SSGarden] = config->getBool("Chao Gardens", "EnableStationSquareGarden", true);
	ModConfig::EnabledLevels[LevelIDs_MRGarden] = config->getBool("Chao Gardens", "EnableMysticRuinsGarden", true);
	ModConfig::EnabledLevels[LevelIDs_ECGarden] = config->getBool("Chao Gardens", "EnableEggCarrierGarden", true);
	ModConfig::ReplaceFruits = (FruitConfig)config->getInt("Chao Gardens", "ReplaceFruits", (int)FruitConfig::CommonFruits);
	ModConfig::DisableChao = config->getBool("Chao Gardens", "DisableChao", false);
	ModConfig::ReplaceEggs = config->getBool("Chao Gardens", "ReplaceEggs", true);
	ModConfig::ReplaceTrees = config->getBool("Chao Gardens", "ReplaceTrees", true);
	ModConfig::ReplaceNameMachine = config->getBool("Chao Gardens", "ReplaceNameMachine", true);
	ModConfig::EnableLobby = config->getBool("Chao Gardens", "EnableChaoRaceLobby", true);
	ModConfig::DisableChaoButtonPrompts = config->getBool("Chao Gardens", "DisableChaoButtonPrompts", false);
	// Videos
	ModConfig::ColorizeVideos = config->getBool("Videos", "ColorizeVideos", true);
	ModConfig::SA1Intro = config->getBool("Videos", "EnableSA1Intro", true);
	ModConfig::SonicTeamLogo = (SonicTeamLogoConfig)config->getInt("Videos", "SonicTeamLogoConfig", (int)SonicTeamLogoConfig::Animated);
	// Branding
	ModConfig::RemoveMarketRingCount = config->getBool("Branding", "RemoveMarketRingCount", false);
	ModConfig::RemoveGameGearGames = config->getBool("Branding", "RemoveGameGearGames", false);
	ModConfig::RemoveUnlockMessage = config->getBool("Branding", "RemoveUnlockMessage", false);
	ModConfig::RemoveQuitPrompt = config->getBool("Branding", "RemoveQuitPrompt", false);
	ModConfig::RestoreDemos = (DemosConfig)config->getInt("Branding", "RestoreDemos", (int)DemosConfig::DreamcastUS);
	ModConfig::RemoveMissionMode = config->getBool("Branding", "RemoveMissionMode", false);
	ModConfig::RemoveCream = config->getBool("Branding", "RemoveCream", true);
	ModConfig::HUDTweak = config->getBool("Branding", "HUDTweak", true);
	ModConfig::DrawNowSaving = config->getBool("Branding", "DrawNowSaving", true);
	ModConfig::TitleScreenLogoMode = (TitleScreenConfig)config->getInt("Branding", "SA1LogoMode", (int)TitleScreenConfig::US_EU);
	// Set window title (can't be called in OnFrame etc.)
	if (ModConfig::EnableWindowTitle)
	{
		helperFunctionsGlobal->SetWindowTitle("Sonic Adventure");
	}
	// Enable Impress/Comic Sans subtitle font (can only be called within the mod's init)
	switch (ModConfig::FontReplacementMode)
	{
	case ImpressFontConfig::Impress:
		ReplaceGeneric("FONTDATA1.BIN", "FONTDATA1_I.BIN");
		break;
	case ImpressFontConfig::ComicSans:
		ReplaceGeneric("FONTDATA1.BIN", "FONTDATA1_C.BIN");
		break;
	default:
		break;
	}
	delete config;
}