#include "stdafx.h"
#include "STG04_SpeedHighway.h"

static bool ModelsLoaded_STG04;

void FountainPart1(NJS_MODEL_SADX *a1, int a2, float a3)
{
	late_DrawModelClipMesh(object_fountain_water_water.basicdxmodel, LATE_MAT, a3);
}

void FountainPart2(NJS_MODEL_SADX *a1, int a2, float a3)
{
	late_DrawModelClipMesh(object_fountain_water_water.child->sibling->child->sibling->basicdxmodel, LATE_MAT, a3);
	late_DrawModelClipMesh(object_fountain_water_water.child->sibling->child->basicdxmodel, LATE_MAT, a3);
}

void FountainPart3(NJS_MODEL_SADX *a1, int a2, float a3)
{
	late_DrawModelClipMesh(object_fountain_water_water.child->basicdxmodel, LATE_MAT, a3);
}

void RocketSprite(float a, float r, float g, float b)
{
	SetMaterial(max(0, a), r, g, b);
}

void AntennaModel(NJS_OBJECT *obj)
{
	dsDrawModel(obj->basicdxmodel);
	late_DrawModel(obj->child->basicdxmodel, LATE_WZ);
	late_DrawModel(obj->child->sibling->basicdxmodel, LATE_LIG);
}

void AntennaSprite(NJS_ARGB *a1)
{
	float val = max(0.0f, min(1.0f, a1->r));
	SetMaterial(1.0f, val, val, val);
}

void SetCopSpeederEffectAlpha(float a, float r, float g, float b)
{
	SetMaterial(min(a, 1.0f), r, g, b);
}

void OCraneFix(NJS_MODEL_SADX *a1, int a2, float a3)
{
	dsDrawModel(a1);
}

void ConeModel(NJS_OBJECT *a1, LATE a2)
{
	DrawModel(a1->basicdxmodel);
	late_DrawModel(a1->child->basicdxmodel, LATE_MAT);
}

void HelicopterLight(NJS_OBJECT *a1, LATE a2)
{
	late_DrawModel(a1->basicdxmodel, LATE_WZ);
	late_DrawModel(a1->child->basicdxmodel, LATE_WZ);
	late_DrawModel(a1->child->child->basicdxmodel, LATE_LIG);
	late_DrawObject(a1->child->child->child, LATE_LIG);
}

void FlyStFix(NJS_ACTION *action, Float frame)
{
	njAction(action, frame);
	late_DrawObjectClip(action->object->child->sibling->sibling->sibling->sibling, LATE_WZ, 1.0f);
}

void OKanbanAFix(NJS_MODEL_SADX* a1)
{
	late_DrawModelClip(a1, LATE_WZ, 1.0f);
}

void SpeedHighway_Init()
{
	ReplaceCAM("CAM0400M");
	ReplaceCAM("CAM0400S");
	ReplaceCAM("CAM0401S");
	ReplaceCAM("CAM0402K");
	ReplaceCAM("CAM0402S");
	ReplaceSET("SET0400M");
	ReplaceSET("SET0400S");
	ReplaceSET("SET0401S");
	ReplaceSET("SET0402K");
	ReplaceSET("SET0402S");
	ReplacePVM("BG_HIGHWAY");
	ReplacePVM("BG_HIGHWAY01");
	ReplacePVM("BG_HIGHWAY02");
	ReplacePVM("BG_HIGHWAY03");
	ReplacePVM("HIGHWAY01");
	ReplacePVM("HIGHWAY02");
	ReplacePVM("HIGHWAY03");
	ReplacePVM("HIGHWAY_CAR");
	ReplacePVM("OBJ_HIGHWAY");
	ReplacePVM("OBJ_HIGHWAY2");
	ReplacePVM("MRACE_EGGMOBLE");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg04[0][i].f32StartZ = 2000.0f;
		pFogTable_Stg04[0][i].f32EndZ = 5200.0f;

		pFogTable_Stg04[1][i].f32StartZ = 1600.0f;
		pFogTable_Stg04[1][i].f32EndZ = 4800.0f;
		pFogTable_Stg04[1][i].Col = 0xFF300020;

		pFogTable_Stg04[2][i].Col = 0xFF7FB2E5;
		pFogTable_Stg04[2][i].f32StartZ = 1200.0f;
		pFogTable_Stg04[2][i].f32EndZ = 2900.0f;
		pFogTable_Stg04[2][i].u8Enable = 1;
	}
}

void CranePlatformLightFix(NJS_ACTION* a1, float a2, LATE a3, float a4)
{
	// Draw the lights animation
	late_ActionClipMesh(a1, a2, a3, a4);

	// Draw the platform
	njTranslateV(0, (NJS_VECTOR*)object_crane_cage_cage.pos);
	dsDrawModel(object_crane_cage_cage.basicdxmodel);

	// Draw the glass frame
	NJS_OBJECT* glassframe = object_crane_cage_cage.child;
	njTranslateV(0, (NJS_VECTOR*)glassframe->pos);
	njRotateXYZ(0, glassframe->ang[0], glassframe->ang[1], glassframe->ang[2]);
	dsDrawModel(glassframe->basicdxmodel);

	// Draw the glass
	late_DrawModelClipMesh(glassframe->sibling->basicdxmodel, LATE_MAT, a4);
}

void SpeedHighway_Load()
{
	LevelLoader(LevelAndActIDs_SpeedHighway1, "SYSTEM\\data\\stg04_highway\\landtable0400.c.sa1lvl", &texlist_highway01);
	LevelLoader(LevelAndActIDs_SpeedHighway2, "SYSTEM\\data\\stg04_highway\\landtable0401.c.sa1lvl", &texlist_highway02);
	LevelLoader(LevelAndActIDs_SpeedHighway3, "SYSTEM\\data\\stg04_highway\\landtable0402.c.sa1lvl", &texlist_highway03);
	if (!ModelsLoaded_STG04)
	{
		// Fountain fixes
		object_fountain_water_water = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\fountain_water.nja.sa1mdl");
		object_fountain_w01_w01 = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\fountain_w01.nja.sa1mdl");
		object_fountain_w02_w02 = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\fountain_w02.nja.sa1mdl");
		WriteCall((void*)0x0061BAA0, FountainPart1);
		WriteCall((void*)0x0061BAF1, FountainPart2);
		WriteCall((void*)0x0061BB31, FountainPart3);
		WriteCall((void*)0x0061722D, OKanbanAFix);
		CorrectSpecular(&object_obj_nbox1_nbox1);
		CorrectSpecular(&object_obj_nbox3_nbox3);
		CorrectSpecular(&object_esc2_escalator2_2_escalator2_2);
		object_jama02_jama02_jama02 = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\jama02_jama02.nja.sa1mdl"); // OJamer
		AddWhiteDiffuseMaterial(&object_jama02_jama02_jama02.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_jama02_jama02_jama02.basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_jama02_jama02_jama02.basicdxmodel->mats[4]);
		WriteData((NJS_TEXNAME**)0x026B2968, (NJS_TEXNAME*)0x02670590); // OJamer texture list 1
		WriteData((NJS_TEXNAME**)0x026B2960, (NJS_TEXNAME*)0x02670554); // OJamer texture list 2
		object_hwobj_hpante_hpante = *LoadModel("system\\data\\stg04_highway\\common\\models\\hwobj_hpante.nja.sa1mdl"); // Antenna model
		AddWhiteDiffuseMaterial(&object_hwobj_hpante_hpante.basicdxmodel->mats[3]);
		AddWhiteDiffuseMaterial(&object_hwobj_hpante_hpante.child->basicdxmodel->mats[0]);
		WriteCall((void*)0x00615D60, AntennaModel);
		WriteData<1>((char*)0x004B19E2, NJD_COLOR_BLENDING_SRCALPHA); // Cop speeder effect blending
		WriteCall((void*)0x004B1C6F, SetCopSpeederEffectAlpha);
		// Fix light sprites in various objects
		WriteData((float**)0x00615DA0, (float*)0x7DCB10); // Antenna sprite maximum brightness 1.0 instead of 0.5
		WriteCall((void*)0x00615DB5, AntennaSprite); // Clamp antenna sprite RGB to 0-1
		WriteCall((void*)0x00616649, AntennaSprite); // Clamp GCLight sprite RGB to 0-1
		WriteData((float**)0x00616625, (float*)0x7DCC98); // GCLight sprite maximum brightness 0.5 instead of 0.8
		WriteData((float**)0x0061662B, (float*)0x7DCB5C); // Prevent inversion of the GCLight sprite alpha sign
		WriteCall((void*)0x00614122, RocketSprite); // Clamp rocket sprite
		AddAlphaRejectMaterial(&matlist_hw_obj_lmpa_lmpalght[0]);
		AddAlphaRejectMaterial(&matlist_st_lamp_lamppost_1_lamp02[0]);
		AddAlphaRejectMaterial(&matlist_st_lamp_lamppost_1_lamp01[0]);
		AddWhiteDiffuseMaterial(&matlist_nisepat_nisepat_nisepat[12]);
		AddWhiteDiffuseMaterial(&matlist_nisepat_nisepat_nisepat[13]);
		AddWhiteDiffuseMaterial(&matlist_nisepat_nisepat_nisepat[16]);
		WriteCall((void*)0x0061AF66, OCraneFix); // Don't queue a model that doesn't have transparency
		object_crane_cage_cage = *LoadModel("system\\data\\stg04_highway\\common\\models\\crane_cage.nja.sa1mdl"); // OCrane (edited hierarchy)
		object_crane_cage_cage.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_USE_ALPHA; // Remove unnecessary alpha in OCrane
		AddWhiteDiffuseMaterial(&object_crane_cage_cage.child->sibling->basicdxmodel->mats[4]);
		WriteCall((void*)0x0061B036, CranePlatformLightFix);
		// Helicopter light
		object_heli_light_light = *LoadModel("system\\data\\stg04_highway\\common\\models\\heli_light.nja.sa1mdl"); // Helicopter light (edited hierarchy)
		AddWhiteDiffuseMaterial(&object_heli_light_light.basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&object_heli_light_light.basicdxmodel->mats[1]);
		AddAlphaRejectMaterial(&object_heli_light_light.child->child->basicdxmodel->mats[0]);
		WriteCall((void*)0x0061378E, HelicopterLight);
		object_st_clamp_clamp = *LoadModel("system\\data\\stg04_highway\\common\\models\\st_clamp.nja.sa1mdl"); // Cone (edited hierarchy)
		AddWhiteDiffuseMaterial(&object_st_clamp_clamp.basicdxmodel->mats[0]);
		WriteCall((void*)0x006165E5, ConeModel);
		object_soni_dai_soni_dai_soni_dai = *LoadModel("system\\data\\stg04_highway\\common\\models\\soni_dai_soni_dai.nja.sa1mdl"); // Platform
		object_st_green_basin_b_basin_b= *LoadModel("system\\data\\stg04_highway\\common\\models\\st_green_basin_b.nja.sa1mdl"); // Small plant in Act 3
		object_flydai3_flydai_2_flydai_2 = *LoadModel("system\\data\\stg04_highway\\common\\models\\flydai3_flydai_2.nja.sa1mdl"); // FlyST
		AddAlphaRejectMaterial(&object_flydai3_flydai_2_flydai_2.child->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_flydai3_flydai_2_flydai_2.child->sibling->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_flydai3_flydai_2_flydai_2.child->sibling->sibling->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_flydai3_flydai_2_flydai_2.child->sibling->sibling->sibling->basicdxmodel->mats[0]);
		WriteCall((void*)0x00617FCA, FlyStFix);
		object_tai_dai_tai_dai_tai_dai = *LoadModel("system\\data\\stg04_highway\\common\\models\\tai_dai_tai_dai.nja.sa1mdl"); // Platform (Tails)
		for (int i = 93; i < 105; i++)
			object_missile_body_body.basicdxmodel->points[i].x -= 0.5f; // Offset missile vertices to reduce Z fighting
		object_esc2_escalator2_2_escalator2_2 = *LoadModel("system\\data\\stg04_highway\\common\\models\\esc2_escalator2_2.nja.sa1mdl"); // Escalator2
		object_esc_escalator1_escalator1 = *LoadModel("system\\data\\stg04_highway\\common\\models\\esc_escalator1.nja.sa1mdl"); // Escalator1
		object_st_tank_b_body_d03_b_body_d03 = *LoadModel("system\\data\\stg04_highway\\common\\models\\st_tank_b_body_d03.nja.sa1mdl"); // Container B top broken 1
		object_st_tank_b_body_d01_b_body_d01 = *LoadModel("system\\data\\stg04_highway\\common\\models\\st_tank_b_body_d01.nja.sa1mdl"); // Container B top broken 2
		object_st_tank_b_body_b_body = *LoadModel("system\\data\\stg04_highway\\common\\models\\st_tank_b_body.nja.sa1mdl"); // Container B
		object_st_tank_c_body_c_body = *LoadModel("system\\data\\stg04_highway\\common\\models\\st_tank_c_body.nja.sa1mdl"); // Container C
		WriteData<1>((void*)0x00619545, LATE_WZ); // For posters
		WriteData<1>((void*)0x0061A8EA, LATE_MAT); // For glass
		WriteData<1>((void*)0x0061A951, LATE_MAT); // For glass fragments
		texlist_board[0].textures = (NJS_TEXNAME*)0x026705CC; // Texlists for posters
		texlist_board[1].textures = (NJS_TEXNAME*)0x026705F0; // Texlists for posters
		texlist_board[2].textures = (NJS_TEXNAME*)0x02670614; // Texlists for posters
		texlist_board[3].textures = (NJS_TEXNAME*)0x02670638; // Texlists for posters
		texlist_board[4].textures = (NJS_TEXNAME*)0x0267065C; // Texlists for posters
		object_turnasi_oya_oya = *LoadModel("system\\data\\stg04_highway\\common\\models\\turnasi_oya.nja.sa1mdl"); // Turnasi part 1 (edited materials)
		object_turnasi_oya_koa = *object_turnasi_oya_oya.child; // Turnasi part 2
		object_turnasi_oya_kob = *object_turnasi_oya_oya.child->child; // Turnasi part 3
		object_heli_body_body = *LoadModel("system\\data\\stg04_highway\\common\\models\\heli_body.nja.sa1mdl"); // Helicopter
		object_hw_kuruma3_wagon_wagon = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\r_kuruma3_wagon.nja.sa1mdl"); // Bus
		object_hwkuruma2_d_body_d_body_d = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\sskuruma2_d_body_d.nja.sa1mdl"); // Red Car
		object_hwrotyucar_body_body = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\ssrotyucar_body.nja.sa1mdl"); // Blue Car
		object_hw_kuruma4_mini_mini = *LoadModel("system\\data\\stg04_highway\\common\\models\\no_unite\\kuruma4_mini.nja.sa1mdl"); // Yellow Car
		model_glass_cube_1_cube_1 = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_cube_1.nja.sa1mdl")->basicdxmodel; // SH Glass
		// Glass fragments
		object_glass_ga_ga = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_ga.nja.sa1mdl");
		object_glass_gb_gb = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gb.nja.sa1mdl");
		object_glass_gc_gc = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gc.nja.sa1mdl");
		object_glass_gd_gd = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gd.nja.sa1mdl");
		object_glass_ge_ge = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_ge.nja.sa1mdl");
		object_glass_gf_gf = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gf.nja.sa1mdl");
		object_glass_gg_gg = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gg.nja.sa1mdl");
		object_glass_gh_gh = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gh.nja.sa1mdl");
		object_glass_gj_gj = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gj.nja.sa1mdl");
		object_glass_gk_gk = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gk.nja.sa1mdl");
		object_glass_gl_gl = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gl.nja.sa1mdl");
		object_glass_gm_gm = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gm.nja.sa1mdl");
		object_glass_gn_gn = *LoadModel("system\\data\\stg04_highway\\common\\models\\glass_gn.nja.sa1mdl");
		ModelsLoaded_STG04 = true;
	}
}