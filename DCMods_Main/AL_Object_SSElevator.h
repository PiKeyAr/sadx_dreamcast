#pragma once

#include <SADXModLoader.h>

DataArray(CCL_INFO, door_c_info, 0x02BBFA68, 5); // Elevator collision

TaskFunc(BoxInit, 0x00639250); // Box collision for elevators
TaskFunc(Exec_27, 0x00638F50); // Station Square elevator main function
TaskFunc(Disp_20, 0x00638DC0); // Station Square elevator display function