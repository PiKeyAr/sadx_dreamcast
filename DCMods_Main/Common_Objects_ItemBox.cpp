#include "stdafx.h"
#include "Common_Objects_ItemBox.h"

NJS_OBJECT* ItemBoxAirModel_Resize = nullptr;

// Custom drawing function for item box (air) with depth hacks
void DrawCustomObject_wz_r(NJS_OBJECT* top_object, CUSTOM_OBJ* custom)
{
	NJS_OBJECT* obj; // ecx
	int index; // eax
	Angle v5; // eax
	Angle v6; // eax
	Angle v7; // eax
	float a3; // ST08_4
	void(__cdecl * exad)(NJS_OBJECT*); // eax
	Angle v10; // eax
	Angle v11; // eax
	Angle v12; // eax

	do
	{
		njPushMatrix(0);
		if (top_object->model)
		{
			obj = custom->obj;
			index = 0;
			if (custom->obj)
			{
				do
				{
					if (obj == top_object)
					{
						break;
					}
					obj = custom[index++ + 1].obj;
				} while (obj);
			}
			if (custom[index].obj)
			{
				exad = custom[index].exad;
				if (exad)
				{
					exad(top_object);
				}
			}
			else if (!loop_count)
			{
				njTranslateV(0, (const NJS_VECTOR*)top_object->pos);
				v5 = top_object->ang[2];
				if (v5)
				{
					njRotateZ(0, (unsigned __int16)v5);
				}
				v6 = top_object->ang[0];
				if (v6)
				{
					njRotateX(0, (unsigned __int16)v6);
				}
				v7 = top_object->ang[1];
				if (v7)
				{
					njRotateY(0, (unsigned __int16)v7);
				}
				njScaleV(0, (const NJS_VECTOR*)top_object->scl);
				a3 = VectorMaxAbs((NJS_VECTOR*)top_object->scl);
				if (IsCameraUnderwater) late_z_ofs___ = -13000.0f;
				else if (ssStageNumber == LevelIDs_HotShelter && ssActNumber == 0 && playertwp[0]->pos.x < 1050)
					late_z_ofs___ = -1000.0f;
				else
					late_z_ofs___ = 3000.0f;
				late_DrawModelClipEx(top_object->basicdxmodel, LATE_MAT, a3);
				late_z_ofs___ = 0.0f;
			}
		}
		else
		{
			njTranslateV(0, (const NJS_VECTOR*)top_object->pos);
			v10 = top_object->ang[2];
			if (v10)
			{
				njRotateZ(0, (unsigned __int16)v10);
			}
			v11 = top_object->ang[0];
			if (v11)
			{
				njRotateX(0, (unsigned __int16)v11);
			}
			v12 = top_object->ang[1];
			if (v12)
			{
				njRotateY(0, (unsigned __int16)v12);
			}
			njScaleV(0, (const NJS_VECTOR*)top_object->scl);
		}
		if (top_object->child)
		{
			njPushMatrix(0);
			DrawCustomObject_wz_r(top_object->child, custom);
			njPopMatrix(1u);
		}
		njPopMatrix(1u);
		top_object = top_object->sibling;
	} while (top_object);
}

// Resized version of item box (air)
void DrawCustomObject_wz_r_resize(NJS_OBJECT* a1, CUSTOM_OBJ* a2)
{
	DrawCustomObject_wz_r(ItemBoxAirModel_Resize, a2);
}

// Draws the item box icon with depth
void RenderItemBoxIcon(NJS_MODEL_SADX* a1)
{
	if (IsCameraUnderwater)
		late_z_ofs___ = -15000.0f;
	else if (ssStageNumber == LevelIDs_HotShelter && ssActNumber == 0 && playertwp[0]->pos.x < 1050)
		late_z_ofs___ = -2000.0f;
	late_DrawModel(a1, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

// Custom draw function for destroyed item box with rotation
void ItemBox_Display_Destroyed_Rotate(task* _this)
{
	auto v1 = _this->twp;
	SetTextureToCommon();
	njPushMatrix(nullptr);
	njTranslateV(nullptr, &v1->pos);

	// Rotate
	if (ModConfig::SETReplacement == SetFileConfig::Optimized)
	{
		njRotateEx((Angle*)&v1->ang, 0);
	}
	dsDrawModel(&model_itembox_boxbody_boxbody);
	njPopMatrix(1u);
}

// Custom draw function for underwater item box with rotation and depth hacks
void ItemBox_Display_Water_Rotate(task* _this)
{
	auto v1 = _this->twp;
	if (!loop_count)
	{
		item_info[6].texture_id = gu8flgPlayingMetalSonic ? 98 : levelup_texture[GetPlayerNumber()];
		if (dsCheckViewV(&v1->pos, 20.0f))
		{
			SetTextureToCommon();
			njPushMatrixEx();

			auto model = (NJS_MODEL_SADX*)late_alloca(44);
			auto material = (NJS_MATERIAL*)late_alloca(20);

			if (model && material)
			{
				njTranslateEx(&v1->pos);

				// Rotate
				if (ModConfig::SETReplacement == SetFileConfig::Optimized)
				{
					njRotateEx((Angle*)&v1->ang, 0);
				}
				if (v1->mode != 2)
				{
					auto scale = v1->scl.z * 0.2f;
					njScale(nullptr, scale, scale, scale);
				}
				njPushMatrixEx();
				njTranslate(nullptr, 0.0f, 7.5f, 0.0f);
				auto v6 = (Uint16)(v1->scl.y * 65536.0f * 0.002777777777777778f);
				if (v6)
				{
					njRotateY(nullptr, v6);
				}
				memcpy(model, &model_itembox_boxbody_item_panel, 0x2Cu);
				memcpy(material, model_itembox_boxbody_item_panel.mats, 0x14u);
				model->mats = material;
				auto texId = item_info[(int)_this->twp->scl.x].texture_id;
				item_kind = (int)_this->twp->scl.x;
				material->attr_texId = texId;
				dsDrawModel(model);
				njPopMatrixEx();
				dsDrawModel(&model_itembox_boxbody_boxbody);
				// Depth hacks
				if (IsCameraUnderwater)
					late_z_ofs___ = -10000.0f;
				else if (ssStageNumber == LevelIDs_HotShelter && ssActNumber == 0 && playertwp[0]->pos.x < 1050)
					late_z_ofs___ = -1000.0f;
				else
					late_z_ofs___ = 8000.0f;
				late_DrawModel(&model_itembox_boxbody_cyl2, LATE_MAT);
				late_z_ofs___ = 0.0f;
				dsDrawModel(&model_itembox_boxbody_cyl3);
			}
			njPopMatrixEx();
		}
	}
}

// Custom draw function for regular item box with rotation and depth hacks
void ItemBox_Display_Rotate(task* _this)
{
	auto v1 = _this->twp;
	if (!loop_count)
	{
		item_info[6].texture_id = gu8flgPlayingMetalSonic ? 98 : levelup_texture[GetPlayerNumber()];
		if (dsCheckViewV(&v1->pos, 20.0f))
		{
			SetTextureToCommon();
			njPushMatrix(nullptr);
			njTranslateV(nullptr, &v1->pos);

			// Rotate
			if (ModConfig::SETReplacement == SetFileConfig::Optimized)
			{
				njRotateEx((Angle*)&v1->ang, 0);
			}
			if (v1->mode != 2)
			{
				auto scale = v1->scl.z * 0.2f;
				njScale(nullptr, scale, scale, scale);
			}
			auto model = (NJS_MODEL_SADX*)late_alloca(44);
			auto material = (NJS_MATERIAL*)late_alloca(20);

			if (model && material)
			{
				njPushMatrixEx();
				njTranslate(nullptr, 0.0f, 7.5f, 0.0f);
				auto v6 = (Uint16)(v1->scl.y * 65536.0f * 0.002777777777777778f);
				if (v6)
				{
					njRotateY(nullptr, v6);
				}
				memcpy(model, &model_itembox_boxbody_item_panel, 0x2Cu);
				memcpy(material, model_itembox_boxbody_item_panel.mats, 0x14u);
				model->mats = material;
				auto v7 = item_info[(int)_this->twp->scl.x].texture_id;
				ItemBox_CurrentItem = (int)_this->twp->scl.x;
				material->attr_texId = v7;
				dsDrawModel(model);
				njPopMatrixEx();
				dsDrawModel(&model_itembox_boxbody_boxbody);
				// Depth hacks
				if (IsCameraUnderwater) late_z_ofs___ = -10000.0f;
				else if (ssStageNumber == LevelIDs_HotShelter && ssActNumber == 0 && playertwp[0]->pos.x < 1050) 
					late_z_ofs___ = -1000.0f;
				else 
					late_z_ofs___ = 8000.0f;
				late_DrawModel(&model_itembox_boxbody_cyl2, LATE_MAT);
				late_z_ofs___ = 0.0f;
				dsDrawModel(&model_itembox_boxbody_cyl3);
			}
			njPopMatrixEx();
		}
	}
}

void ItemBox_Init()
{
	AddWhiteDiffuseMaterial(&object_itembox_boxbody_boxbody.basicdxmodel->mats[3]); // Item capsule
	WriteCall((void*)0x004C0066, RenderItemBoxIcon);
	WriteCall((void*)0x004BFEBF, DrawCustomObject_wz_r_resize);
	WriteCall((void*)0x004BFEFE, DrawCustomObject_wz_r);
	WriteJump(Draw_Break, ItemBox_Display_Destroyed_Rotate);
	WriteJump(DrawInWater, ItemBox_Display_Water_Rotate);
	WriteJump(Draw, ItemBox_Display_Rotate);
	ItemBoxAirModel_Resize = CloneObject(&object_itemboxair_boxbody_boxbody);
	ItemBoxAirModel_Resize->basicdxmodel->mats[0].attrflags |= NJD_FLAG_USE_ALPHA;
	ItemBoxAirModel_Resize->basicdxmodel->mats[1].attrflags |= NJD_FLAG_USE_ALPHA;
	ItemBoxAirModel_Resize->basicdxmodel->mats[2].attrflags |= NJD_FLAG_USE_ALPHA;
	ItemBoxAirModel_Resize->child->basicdxmodel->mats[0].attrflags |= NJD_FLAG_USE_ALPHA;
	ItemBoxAirModel_Resize->child->basicdxmodel->mats[1].attrflags |= NJD_FLAG_USE_ALPHA;
	ItemBoxAirModel_Resize->child->sibling = object_itemboxair_boxbody_boxbody.child->sibling;
	ItemBoxAirModel_Resize->child->sibling->sibling = object_itemboxair_boxbody_boxbody.child->sibling->sibling;
	// Enable rotation for collision
	itembox_colli_info0[0].attr |= 0x200;
	itembox_colli_info0[0].form = 2;
	itembox_colli_info0[1].attr |= 0x200;
	itembox_colli_info1[0].attr |= 0x200;
	itembox_colli_info1[0].form = 2;
	itembox_colli_info1[1].attr |= 0x200;
	itembox_colli_info1[1].form = 2;
}