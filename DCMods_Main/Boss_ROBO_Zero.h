#pragma once

#include <SADXModLoader.h>

struct WIDELINE_TEXDATA
{
	int index;
	NJS_COLOR pColor[2];
	float uv[4][2];
};

FunctionPointer(void, dispPole, (void* a1), 0x0058F470); // Displays barrier poles
FunctionPointer(void, disp_ChainElec, (void* a1), 0x0058C500); // Displays chain attack

DataArray(WIDELINE_TEXDATA, wline_thunder, 0x016E8EF8, 2); // Pole electric effect data