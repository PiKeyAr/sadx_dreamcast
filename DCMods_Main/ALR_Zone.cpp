#include "stdafx.h"

// Fixes for the object called ZONE that makes the Chao pick up toys (code by Exant)

int tree_counter = 0;
int jointva = 0;

void AL_MyCode(task* tp, al_object* al, NJS_VECTOR* out)
{
	al_object* v2 = al;
	do
	{
		njPushMatrixEx();
		AL_CalcMotionMartix(v2);
		if (tree_counter == jointva) //store vec if node found
		{
			//njGetTranslation(0, out);
			njCalcPoint(0, &ZeroVector, out);
		}
		al_object* v29 = v2->pChild;
		++tree_counter;
		if (v29)
		{
			AL_MyCode(tp, v29, out);
		}
		njPopMatrixEx();
		v2 = v2->pSibling;
	} while (v2);
}

int ALR_GetBodyPosition_r(NJS_VECTOR* vec, taskwk* twp, alifewk* awp, int joint)
{
	task* tsk = awp->pAdv2ChaoTask;
	tree_counter = 0;
	jointva = joint;
	if (jointva == 7)
		jointva = 11;
	njPushMatrixEx();
	njUnitMatrix(0);
	njRotateY(0, tsk->twp->ang.y);
	AL_InitCalcMotionMatrix(tsk);
	AL_MyCode(tsk, ((chaowk*)tsk->twp)->Shape.pObject, vec);
	njPopMatrixEx();
	return 1;
}

void ZoneFix_Init()
{
	WriteJump((void*)0x00754D00, ALR_GetBodyPosition_r);
}