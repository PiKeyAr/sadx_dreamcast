#include "stdafx.h"
#include "ScaleInfo.h"
using namespace uiscale;

// Displays Start, Goal and Cool sprites (version with fixed alpha and scaling)
void __cdecl dialog_dsp_cdecl(taskwk* twp)
{
	char smode; // al
	NJS_TEXLIST* texlist; // eax
	float depth; // [esp+0h] [ebp-28h]
	float pri; // [esp+4h] [ebp-24h]
	NJS_SPRITE _sp; // [esp+8h] [ebp-20h] BYREF

	helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	if (!loop_count)
	{
		smode = twp->smode;
		switch (smode)
		{
		case 2:
			depth = -45.0f;
			break;
		case 4:
		case 5:
			depth = -1.9f;
			break;
		default:
			depth = -2.0f;
			break;
		}
		_sp.p.x = twp->pos.x;
		_sp.p.y = twp->pos.y;
		_sp.p.z = 0.0f;
		_sp.ang = 0;
		texlist = &texlist_chao_hyouji;
		if (twp->btimer < 0x1Au)
			texlist = &texlist_obj_al_race;
		_sp.tlist = texlist;
		_sp.tanim = &anim_dialog;
		_sp.sx = twp->scl.x;
		_sp.sy = twp->scl.y;
		njSetTexture(texlist);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		// Fixed alpha
		switch (smode)
		{
		case 1:
			SetMaterial(max(0, twp->scl.z), 1.0f, 0.6f, 0.6f);
			break;
		case 3:
		case 5:
			SetMaterial(max(0, twp->scl.z), 1.0f, 1.0f, 0.5f);
			break;
		default:
			SetMaterial(max(0, twp->scl.z), 1.0f, 1.0f, 1.0f);
			break;
		}
		pri = depth;
		if (depth >= -3.0f && depth < 12048.0f)
			pri = depth + 22048.0f;
		late_DrawSprite2D(&_sp, twp->btimer, pri, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR, LATE_LIG);
		ResetMaterial();
	}
	helperFunctionsGlobal->PopScaleUI();
}

static void __declspec(naked) dialog_dsp_asm()
{
	__asm
	{
		push esi // twp

		// Call your __cdecl function here:
		call dialog_dsp_cdecl

		pop esi // twp
		retn
	}
}

void ALR_GoalSprite_Init()
{
	WriteJump((void*)0x007519A0, dialog_dsp_asm);
}