#include "stdafx.h"

// Chao Race skybox

NJS_OBJECT* object_cr4_fr_haikei_fr_haikei = nullptr;
NJS_OBJECT* object_cr4_nc_crjimen_nc_crjimen = nullptr;

NJS_TEXNAME textures_bg_al_race02_dc[1];
NJS_TEXLIST texlist_bg_al_race02_dc = { arrayptrandlength(textures_bg_al_race02_dc) };

void ChaoRaceSkybox_Display(task* a1)
{
	taskwk* v1 = a1->twp;
	njSetTexture(&texlist_bg_al_race02_dc);
	njPushMatrix(0);
	njTranslateV(0, &v1->pos);
	njRotateY(0, v1->ang.y);
	njScale(0, 1.0f, 1.0f, 1.0f);
	late_z_ofs___ = -47000;
	late_DrawObjectClip(object_cr4_fr_haikei_fr_haikei, LATE_WZ, 1.0f); // Bottom thing
	late_DrawObjectClip(object_cr4_nc_crjimen_nc_crjimen, LATE_WZ, 1.0f); // Sky
	njPopMatrix(1u);
	late_z_ofs___ = 0;
}

void ChaoRaceSkybox_Main(task* a1)
{
	ChaoRaceSkybox_Display(a1);
}

void ChaoRaceSkybox_Load(task* a1)
{
	a1->exec = ChaoRaceSkybox_Main;
	a1->disp = ChaoRaceSkybox_Display;
	a1->dest = FreeTask;
}

void ALR_Skybox_Load()
{
	object_cr4_nc_crjimen_nc_crjimen = LoadModel("system\\data\\chao\\stg_race1\\map\\cr4_nc_crjimen.nja.sa1mdl"); // Sky
	object_cr4_fr_haikei_fr_haikei = LoadModel("system\\data\\chao\\stg_race1\\map\\cr4_fr_haikei.nja.sa1mdl"); // Bottom
	object_cr4_fr_haikei_fr_haikei->basicdxmodel->mats[0].attr_texId = 0;
}

void ALR_Skybox_Init()
{
	PvmListAL_Race[1].pname = "BG_AL_RACE02";
	PvmListAL_Race[1].ptexlist = &texlist_bg_al_race02_dc;
}