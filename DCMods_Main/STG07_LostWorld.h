#pragma once

#include <SADXModLoader.h>

TaskFunc(Normal_37, 0x005EA7A0); // Lost World spikes main function
TaskFunc(Disp_118, 0x5EA720); // Lost World spikes display function

DataPointer(NJS_MODEL_SADX, model_lwobj_prop_a02_prop_a02, 0x02027290);
DataPointer(NJS_MODEL_SADX, model_lostobj_allgate_allgate, 0x02012380);
DataPointer(NJS_MODEL_SADX, model_lost_col_a_col_a, 0x01FFCD68);
DataPointer(NJS_MODEL_SADX, model_lost_col_a_col_b, 0x01FFAB20);
DataPointer(NJS_MODEL_SADX, model_lwobj_snake02_snake02, 0x02002920);
DataPointer(NJS_MODEL_SADX, model_sekichyuu_sekichyuu_sekichyuu, 0x020068D0);
DataPointer(NJS_MODEL_SADX, model_lostobj_terasu_terasu, 0x02006D68);
DataPointer(NJS_MODEL_SADX, model_l02_hasira_hasira, 0x02004E80);
DataPointer(NJS_MODEL_SADX, model_l02_lightway_h_lightway_h, 0x020062E0);
DataPointer(NJS_MODEL_SADX, model_lwobj_prop_a01_prop_a01, 0x02026E38);
DataPointer(NJS_MODEL_SADX, model_lwobj_snake00_snake00, 0x0202A9D8);
DataPointer(NJS_MODEL_SADX, model_lwobj_s_water_s_water, 0x0202AE00);
DataPointer(NJS_MODEL_SADX, model_mirror_hikari_hikari_hikari, 0x02028C98);
DataPointer(NJS_OBJECT, object_lwobj_lwswitch_button, 0x0201C120);