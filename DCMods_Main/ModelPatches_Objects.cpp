#include "stdafx.h"

void PatchModels_Objects()
{
	// dxpc\object\baloon_esc_baloon.nja.sa1mdl
	//((NJS_MATERIAL*)0x008BD93C)->diffuse.color = 0xCCB2B2B2; // Nope
	//((NJS_MATERIAL*)0x008BD950)->diffuse.color = 0xCCB2B2B2;
	*(NJS_TEX*)0x008BDC44 = { 292, 234 };
	*(NJS_TEX*)0x008BDC4C = { 292, 127 };
	*(NJS_TEX*)0x008BDC54 = { 292, 127 };
	*(NJS_TEX*)0x008BDC5C = { 292, 20 };
	*(NJS_TEX*)0x008BDC60 = { 292, 234 };
	*(NJS_TEX*)0x008BDC64 = { 585, 234 };
	*(NJS_TEX*)0x008BDC68 = { 292, 127 };
	*(NJS_TEX*)0x008BDC6C = { 585, 127 };
	*(NJS_TEX*)0x008BDC70 = { 292, 127 };
	*(NJS_TEX*)0x008BDC74 = { 585, 127 };
	*(NJS_TEX*)0x008BDC78 = { 292, 20 };
	*(NJS_TEX*)0x008BDC7C = { 585, 20 };
	*(NJS_TEX*)0x008BDC80 = { 585, 234 };
	*(NJS_TEX*)0x008BDC84 = { 877, 234 };
	*(NJS_TEX*)0x008BDC88 = { 585, 127 };
	*(NJS_TEX*)0x008BDC8C = { 877, 127 };
	*(NJS_TEX*)0x008BDC90 = { 585, 127 };
	*(NJS_TEX*)0x008BDC94 = { 877, 127 };
	*(NJS_TEX*)0x008BDC98 = { 585, 20 };
	*(NJS_TEX*)0x008BDC9C = { 877, 20 };
	*(NJS_TEX*)0x008BDCA0 = { 877, 234 };
	*(NJS_TEX*)0x008BDCA4 = { 1170, 234 };
	*(NJS_TEX*)0x008BDCA8 = { 877, 127 };
	*(NJS_TEX*)0x008BDCAC = { 1170, 127 };
	*(NJS_TEX*)0x008BDCB0 = { 877, 127 };
	*(NJS_TEX*)0x008BDCB4 = { 1170, 127 };
	*(NJS_TEX*)0x008BDCB8 = { 877, 20 };
	*(NJS_TEX*)0x008BDCBC = { 1170, 20 };
	*(NJS_TEX*)0x008BDCC0 = { 1170, 234 };
	*(NJS_TEX*)0x008BDCC4 = { 1462, 234 };
	*(NJS_TEX*)0x008BDCC8 = { 1170, 127 };
	*(NJS_TEX*)0x008BDCCC = { 1462, 127 };
	*(NJS_TEX*)0x008BDCD0 = { 1170, 127 };
	*(NJS_TEX*)0x008BDCD4 = { 1462, 127 };
	*(NJS_TEX*)0x008BDCD8 = { 1170, 20 };
	*(NJS_TEX*)0x008BDCDC = { 1462, 20 };
	*(NJS_TEX*)0x008BDCE0 = { 1462, 234 };
	*(NJS_TEX*)0x008BDCE4 = { 1755, 234 };
	*(NJS_TEX*)0x008BDCE8 = { 1462, 127 };
	*(NJS_TEX*)0x008BDCEC = { 1755, 127 };
	*(NJS_TEX*)0x008BDCF0 = { 1462, 127 };
	*(NJS_TEX*)0x008BDCF4 = { 1755, 127 };
	*(NJS_TEX*)0x008BDCF8 = { 1462, 20 };
	*(NJS_TEX*)0x008BDCFC = { 1755, 20 };
	*(NJS_TEX*)0x008BDD00 = { 1755, 234 };
	*(NJS_TEX*)0x008BDD04 = { 2038, 234 };
	*(NJS_TEX*)0x008BDD08 = { 1755, 127 };
	*(NJS_TEX*)0x008BDD0C = { 2038, 127 };
	*(NJS_TEX*)0x008BDD10 = { 1755, 127 };
	*(NJS_TEX*)0x008BDD14 = { 2038, 127 };
	*(NJS_TEX*)0x008BDD18 = { 1755, 20 };
	*(NJS_TEX*)0x008BDD1C = { 2038, 20 };

	// dxpc\object\bane_banebottom.nja.sa1mdl
	((NJS_MATERIAL*)0x008B5C70)->attrflags = 0x9424A400;
	*(NJS_TEX*)0x008B5D34 = { 74, 37 };
	*(NJS_TEX*)0x008B5D38 = { 254, 127 };
	*(NJS_TEX*)0x008B5D3C = { 254, 0 };
	*(NJS_TEX*)0x008B5D40 = { 509, 127 };
	*(NJS_TEX*)0x008B5D44 = { 435, 37 };
	*(NJS_TEX*)0x008B5D4C = { 74, 217 };
	*(NJS_TEX*)0x008B5D50 = { 254, 127 };
	*(NJS_TEX*)0x008B5D54 = { 254, 254 };
	*(NJS_TEX*)0x008B5D58 = { 509, 127 };
	*(NJS_TEX*)0x008B5D5C = { 435, 217 };

	// dxpc\object\bane_type_b_type_b.nja.sa1mdl
	((NJS_MATERIAL*)0x008B50F8)->attrflags = 0x9424A400;
	*(NJS_TEX*)0x008B51BC = { 74, 37 };
	*(NJS_TEX*)0x008B51C0 = { 254, 127 };
	*(NJS_TEX*)0x008B51C4 = { 254, 0 };
	*(NJS_TEX*)0x008B51C8 = { 509, 127 };
	*(NJS_TEX*)0x008B51CC = { 435, 37 };
	*(NJS_TEX*)0x008B51D4 = { 74, 217 };
	*(NJS_TEX*)0x008B51D8 = { 254, 127 };
	*(NJS_TEX*)0x008B51DC = { 254, 254 };
	*(NJS_TEX*)0x008B51E0 = { 509, 127 };
	*(NJS_TEX*)0x008B51E4 = { 435, 217 };

	// dxpc\object\bane_yoko_body.nja.sa1mdl
	*(NJS_TEX*)0x008C6088 = { 73, 40 };
	*(NJS_TEX*)0x008C608C = { 248, 129 };
	*(NJS_TEX*)0x008C6094 = { 73, 218 };
	*(NJS_TEX*)0x008C6098 = { 73, 218 };
	*(NJS_TEX*)0x008C609C = { 248, 255 };
	*(NJS_TEX*)0x008C60A0 = { 248, 129 };
	*(NJS_TEX*)0x008C60A4 = { 73, 218 };
	*(NJS_TEX*)0x008C60AC = { 73, 40 };
	*(NJS_TEX*)0x008C60B0 = { 248, 5 };
	*(NJS_TEX*)0x008C60B4 = { 248, 129 };
	*(NJS_TEX*)0x008C60B8 = { 73, 40 };
	*(NJS_TEX*)0x008C60C0 = { 73, 218 };
	*(NJS_TEX*)0x008C60C4 = { 248, 255 };
	*(NJS_TEX*)0x008C60C8 = { 248, 129 };
	*(NJS_TEX*)0x008C60CC = { 73, 218 };
	*(NJS_TEX*)0x008C60D0 = { 73, 218 };
	*(NJS_TEX*)0x008C60D8 = { 248, 129 };
	*(NJS_TEX*)0x008C60DC = { 73, 40 };
	*(NJS_TEX*)0x008C60E0 = { 248, 5 };
	*(NJS_TEX*)0x008C60E4 = { 248, 5 };
	*(NJS_TEX*)0x008C60E8 = { 73, 40 };
	*(NJS_TEX*)0x008C60EC = { 248, 129 };
	*(NJS_TEX*)0x008C60F4 = { 73, 218 };
	*(NJS_TEX*)0x008C60F8 = { 73, 40 };
	*(NJS_TEX*)0x008C60FC = { 248, 5 };
	*(NJS_TEX*)0x008C6100 = { 248, 129 };
	*(NJS_TEX*)0x008C6104 = { 73, 40 };
	*(NJS_TEX*)0x008C6108 = { 248, 255 };
	*(NJS_TEX*)0x008C610C = { 73, 218 };
	*(NJS_TEX*)0x008C6110 = { 248, 129 };
	*(NJS_TEX*)0x008C6118 = { 73, 40 };
	*(NJS_TEX*)0x008C611C = { 248, 255 };
	*(NJS_TEX*)0x008C6120 = { 73, 218 };
	*(NJS_TEX*)0x008C6124 = { 248, 129 };
	*(NJS_TEX*)0x008C612C = { 73, 40 };

	// dxpc\object\hintbox_body.nja.sa1mdl
		// Sorted and split model in DX, no actual differences beyond disabling alpha on non-transparent meshes.

	// dxpc\object\kasoku_panel.nja.sa1mdl
	*(NJS_TEX*)0x008B81B0 = { 509, 20 };
	*(NJS_TEX*)0x008B81B4 = { 509, 0 };
	*(NJS_TEX*)0x008B81B8 = { 509, 20 };
	*(NJS_TEX*)0x008B81BC = { 509, 234 };
	*(NJS_TEX*)0x008B81C0 = { 509, 234 };
	*(NJS_TEX*)0x008B81C4 = { 509, 255 };
	*(NJS_TEX*)0x008B81C8 = { 5, 2 };
	*(NJS_TEX*)0x008B81CC = { 5, 251 };
	*(NJS_TEX*)0x008B81D0 = { 505, 2 };
	*(NJS_TEX*)0x008B81D4 = { 504, 250 };
	*(NJS_TEX*)0x008B81D8 = { 5, 2 };
	*(NJS_TEX*)0x008B81DC = { 5, 34 };
	*(NJS_TEX*)0x008B81E0 = { 505, 2 };
	*(NJS_TEX*)0x008B81E4 = { 505, 34 };

	// dxpc\object\lastpanel_base.nja.sa1mdl
	((NJS_MATERIAL*)0x008C5898)->attrflags = 0x9424A400;
	*(NJS_TEX*)0x008C59E8 = { 435, 40 };
	*(NJS_TEX*)0x008C59EC = { 509, 129 };
	*(NJS_TEX*)0x008C59F0 = { 254, 129 };
	*(NJS_TEX*)0x008C59F4 = { 74, 218 };
	*(NJS_TEX*)0x008C59FC = { 254, 129 };
	*(NJS_TEX*)0x008C5A00 = { 509, 129 };
	*(NJS_TEX*)0x008C5A04 = { 435, 218 };
	*(NJS_TEX*)0x008C5A08 = { 254, 129 };
	*(NJS_TEX*)0x008C5A0C = { 254, 254 };
	*(NJS_TEX*)0x008C5A10 = { 74, 218 };
	*(NJS_TEX*)0x008C5A14 = { 435, 40 };
	*(NJS_TEX*)0x008C5A18 = { 254, 3 };
	*(NJS_TEX*)0x008C5A1C = { 254, 129 };
	*(NJS_TEX*)0x008C5A20 = { 74, 40 };

	// dxpc\object\ripple.nja.sa1mdl
		// Full replacement

	// dxpc\object\rocket_launcher_body.nja.sa1mdl
	*(NJS_TEX*)0x008BF888 = { 419, -179 };
	*(NJS_TEX*)0x008BF88C = { 419, 155 };
	*(NJS_TEX*)0x008BF890 = { 348, -179 };
	*(NJS_TEX*)0x008BF894 = { 348, 155 };
	*(NJS_TEX*)0x008BF898 = { 161, -179 };
	*(NJS_TEX*)0x008BF89C = { 161, 155 };
	*(NJS_TEX*)0x008BF8A0 = { 90, -179 };
	*(NJS_TEX*)0x008BF8A4 = { 90, 155 };
	*(NJS_TEX*)0x008BF8A8 = { 90, 255 };
	*(NJS_TEX*)0x008BF8AC = { 90, 155 };
	*(NJS_TEX*)0x008BF8B0 = { 419, 255 };
	*(NJS_TEX*)0x008BF8B4 = { 419, 155 };
	*(NJS_TEX*)0x008BF8B8 = { 90, -179 };
	*(NJS_TEX*)0x008BF8BC = { 255, -254 };
	*(NJS_TEX*)0x008BF8C0 = { 419, -179 };
	*(NJS_TEX*)0x008BF8C4 = { 510, 49 };
	*(NJS_TEX*)0x008BF8C8 = { 419, 255 };
	*(NJS_TEX*)0x008BF8CC = { 419, -179 };
	*(NJS_TEX*)0x008BF8D0 = { 90, -179 };
	*(NJS_TEX*)0x008BF8D4 = { 90, 255 };
	*(NJS_TEX*)0x008BF8D8 = { 0, 49 };
	*(NJS_TEX*)0x008BF964 = { 254, 224 };
	*(NJS_TEX*)0x008BF96C = { 254, 255 };
	*(NJS_TEX*)0x008BF970 = { 254, 255 };
	*(NJS_TEX*)0x008BF978 = { 254, 224 };
	*(NJS_TEX*)0x008BF97C = { 255, -1275 };
	*(NJS_TEX*)0x008BF980 = { 0, -1275 };
	*(NJS_TEX*)0x008BF984 = { 255, -1015 };
	*(NJS_TEX*)0x008BF988 = { 0, -1007 };
	*(NJS_TEX*)0x008BF98C = { 255, -734 };
	*(NJS_TEX*)0x008BF990 = { 0, -736 };
	*(NJS_TEX*)0x008BF994 = { 255, -510 };
	*(NJS_TEX*)0x008BF998 = { 0, -510 };
	*(NJS_TEX*)0x008BF99C = { 255, -285 };
	*(NJS_TEX*)0x008BF9A0 = { 0, -283 };
	*(NJS_TEX*)0x008BF9A4 = { 255, -4 };
	*(NJS_TEX*)0x008BF9A8 = { 0, -12 };
	*(NJS_TEX*)0x008BF9AC = { 255, 255 };
	*(NJS_TEX*)0x008BF9B0 = { 0, 255 };

	// dxpc\object\savepoint_bmerge9.nja.sa1mdl
	*(NJS_TEX*)0x008C8690 = { 255, 0 };
	*(NJS_TEX*)0x008C8698 = { 54, 0 };
	*(NJS_TEX*)0x008C869C = { 255, 254 };
	*(NJS_TEX*)0x008C86A0 = { 255, 0 };
	*(NJS_TEX*)0x008C86A8 = { 54, 0 };
	*(NJS_TEX*)0x008C86AC = { 255, 254 };
	*(NJS_TEX*)0x008C86B0 = { 255, 0 };
	*(NJS_TEX*)0x008C86B8 = { 54, 0 };
	*(NJS_TEX*)0x008C86BC = { 255, 254 };
	*(NJS_TEX*)0x008C86C0 = { 255, 0 };
	*(NJS_TEX*)0x008C86CC = { 255, 0 };
	*(NJS_TEX*)0x008C86D0 = { 255, 254 };
	*(NJS_TEX*)0x008C86D4 = { 54, 0 };
	*(NJS_TEX*)0x008C86DC = { 255, 0 };
	*(NJS_TEX*)0x008C86E0 = { 255, 254 };
	*(NJS_TEX*)0x008C86E4 = { 54, 0 };
	*(NJS_TEX*)0x008C86EC = { 255, 0 };
	*(NJS_TEX*)0x008C86F0 = { 255, 254 };
	*(NJS_TEX*)0x008C86F4 = { 54, 0 };
	*(NJS_TEX*)0x008C86FC = { 255, 0 };

	// dxpc\object\switch_body.nja.sa1mdl
	*(NJS_TEX*)0x008BB8C0 = { 474, 127 };
	*(NJS_TEX*)0x008BB8C4 = { 410, 205 };
	*(NJS_TEX*)0x008BB8C8 = { 410, 49 };
	*(NJS_TEX*)0x008BB8CC = { 254, 237 };
	*(NJS_TEX*)0x008BB8D0 = { 255, 17 };
	*(NJS_TEX*)0x008BB8D4 = { 99, 205 };
	*(NJS_TEX*)0x008BB8D8 = { 99, 49 };
	*(NJS_TEX*)0x008BB8DC = { 35, 127 };
}