#include "stdafx.h"
#include "AL_RACE_ChaoRace.h"
#include "FunctionHook.h"

bool ObjectsLoaded_AL_RACE = false;

bool SkipSA1Entry = false; // Go straight to the SADX Chao Race Entry if true

FunctionHook<void, NJS_POINT3*, Angle3*, float, float, float> AL_MaruKageDraw_h(AL_MaruKageDraw);

// Chao Race double shadow fix
static void __cdecl AL_MaruKageDraw_r(NJS_POINT3* pPos, Angle3* pAng, float scl, float scl_ratio, float alpha)
{
	if (ChaoStageNumber != CHAO_STG_RACE)
		AL_MaruKageDraw_h.Original(pPos, pAng, scl, scl_ratio, alpha);
}

// Renders the "Chao Race" letters with depth
void RenderChaoRaceLetters_Fix(NJS_OBJECT* a1)
{
	late_DrawObjectClip(a1, LATE_LIG, 1.0f);
}

// Adds an English version of OBJ_AL_RACE to load during Chao Race initialization
void LoadObjChaoRaceTexlist(const char* PVMName, NJS_TEXLIST* TexListPtr, unsigned __int16 level)
{
	AL_LoadTex(Language ? "OBJ_AL_RACE_E" : "OBJ_AL_RACE", TexListPtr, level);
}

// Renders the Basic Model version of the race ball
void RenderChaoBall()
{
	njSetTexture(&texlist_chao);
	njScale(0, 1.5f, 1.5f, 1.5f);
	ds_DrawObjectClip(&object_item_lball_lball, 1.5f);
}

void AL_ChaoRace_Load()
{
	LevelLoader(LevelAndActIDs_ChaoRace + 1, "system\\data\\chao\\stg_race1\\landtablechaorace.c.sa1lvl", &texlist_chao_race);
	if (!ObjectsLoaded_AL_RACE)
	{
		object_item_lball_lball = *LoadModel("system\\data\\chao\\data\\object\\item_lball.nja.sa1mdl");
		WriteCall((void*)0x0075A550, RenderChaoBall);
		ALR_Skybox_Load();
		ALR_StartMark_Load();
		ALR_Waterfall_Load();
		ObjectsLoaded_AL_RACE = true;
	}
}

void AL_ChaoRace_OnFrame()
{
	if (IsLevelLoaded(39, 4) && ChaoStageNumber == CHAO_STG_RACE && !ChkPause())
		ALR_Nameplate_OnFrame();
}

// Hook for the prolog load function
void __cdecl _alg_race_prolog_r()
{
	PrintDebug("DC ChaoStgRace _prolog begin.\n");
	AL_ChaoRace_Load();
	dialog = (task**)CAlloc(0x34, 5); // Create space for dialog tasks for Start/Goal
	ChaoRaceNameCounter = 0;
	SkipSA1Entry = false;
	AL_LoadTex("BG_AL_RACE02", &texlist_bg_al_race02_dc, 1u);
	AL_LoadTex("AL_TEX_COMMON", &texlist_al_tex_common, 1u); // Name font
	CreateElementalTask(LoadObj_Data1, 2, AL_RaceMaster); // Load DX level object
	LoadObjects_Race(); // SET objects
	LandChangeLandTable(GetLevelLandtable(42, 1));
	PrintDebug("DC ChaoStgRace _prolog end.\n");
}

void AL_ChaoRace_Init()
{
	ReplacePVM("AL_RACE02");
	ReplacePVM("BG_AL_RACE02");
	ReplacePVM("OBJ_AL_RACE");
	ReplacePVM("OBJ_AL_RACE_E");
	ReplaceSET("SETAL_RACE00S");
	ReplaceSET("SETAL_RACE01S");
	ReplaceCAM("CAMAL_RACE01S");
	ALR_Nameplate_Init();
	ALR_GoalSprite_Init();
	ALR_Skybox_Init();
	ALR_Cracker_Init();
	ZoneFix_Init();
	WriteCall((void*)0x0076DA03, RenderChaoRaceLetters_Fix);
	WriteData((eCHAO_STAGE_NUMBER**)0x00751B11, &ChaoStageNumber); // Restore Chao Race jewel by replacing the invalid pointer with something that always returns something
	WriteCall((void*)0x00719D8C, LoadObjChaoRaceTexlist);
	WriteJump(_alg_race_prolog, _alg_race_prolog_r);
	WriteData((float*)0x00719D74, -16000.0f); // Draw distance
	// Trampolines and hooks
	AL_MaruKageDraw_h.Hook(AL_MaruKageDraw_r);
	if (ModConfig::EnableLobby)
		AL_ChaoRaceEntry_Init();
}