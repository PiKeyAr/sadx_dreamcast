#include "stdafx.h"
#include "Advertise_TitleScreen.h"
#include "ScaleInfo.h"

// TODO: Figure out a better solution for the File Select transition

DataPointer(char*, savepath, 0x421E4E); // Path to the save folder (replaceable)

using namespace uiscale;

#define SQUARE(x) (x)*(x) 
#define PYTHAGORAS(x,y) sqrt(SQUARE(x) + SQUARE(y))

NJS_TEXNAME textures_ava_gtitle0_640[42]; // Original DC GTITLE textures for 640x480 (AVA_TITLE_CMN_SMALL texlist)
NJS_TEXLIST texlist_ava_gtitle00_640 = { arrayptrandlength(textures_ava_gtitle0_640) };

NJS_TEXNAME textures_title_back_640[24]; // Original DC TITLE textures for 640x480 (AVA_TITLE_BACK texlist)
NJS_TEXLIST texlist_title_back_640 = { arrayptrandlength(textures_title_back_640) };

NJS_TEXNAME textures_ava_gtitle0_e_dc_hd[19]; // Custom HD textures (AVA_GTITLE0_E texlist)
NJS_TEXLIST texlist_ava_gtitle00_e_dc_hd = { arrayptrandlength(textures_ava_gtitle0_e_dc_hd) };

NJS_OBJECT* object_hamon_null1_null1; // Title screen model

// PVM IDs for menus
static int TitleScreenIndices[] = { 17, 29, 0, 1, 10, 13, 7, 30 }; // 0, 1, 10, 13, 7 from the file select screen, 29 is SMALL, removed 18 AVA_TITLE_CMN
static int MainMenuIndices[] = { 19, 20, 1, 10, 17, 29, 30 }; // 17 is needed to draw the logo (GTITLE0), 29 is SMALL, 20 is TITLE_BACK

// Variables
static bool RipplesOn; // Title screen is 3D if on
static bool DrawOverlay; // Glow on the Adventure logo if on
static bool EnableTransition; // Zoom logo on pressing Start if on
static bool UpdateTransform = true; // Reset title screen scaling if this is on

// Colors
static NJS_COLOR FileSelectVtxColor = { 0xFFFFFFFF }; // Vertex color for the file select screen
static NJS_COLOR TitleBackOverlayColor; // Main menu background
static NJS_COLOR TitleBGTransparency; // Title screen background
static NJS_COLOR SonicTeamTransparency; // Sonic Team logo color
static NJS_COLOR BlackFadeout;

// Other variables
static int SonicTeamAlpha; // Sonic Team logo transparency (can be lower than 0)
static TitleScreenTransitionMode TitleTransition = TitleScreenTransitionMode::Normal;
static float LogoScaleX = 1.0f; // Current SA logo horizontal scale
static float LogoScaleY = 1.0f; // Current SA logo vertical scale
static AdvaModeEnum PreviousMenuIndex = ADVA_MODE_NONE; // Last menu
static AdvaModeEnum CurrentMenuIndex = ADVA_MODE_NONE; // Current menu
static bool WhiteOverlayDrawn = false;
static bool MainMenuAccessed = false;
static bool EnableFileSelectScreenFade = false;
static bool TitleScreenFadedIntoBlack = false;

bool IsScreenResolution640x480()
{
	return (HorizontalResolution == 640 && VerticalResolution == 480);
}

// Hook to draw file select buttons with transparency
void DrawButtonHook(NJS_COLOR n, float x, float y, float z, float sx, float sy)
{
	NJS_COLOR color;
	color.argb.a = FileSelectVtxColor.argb.a;
	color.argb.r = n.argb.r;
	color.argb.g = n.argb.g;
	color.argb.b = n.argb.b;
	DrawBG_ava_square_b(color.color, x, y, z, sx, sy);
}

// Hook to draw "Create New File" etc. with transparency
void DrawStringHook(const char* text, float x, float y, float scale)
{
	SetABCTextThingColor(FileSelectVtxColor.color, FileSelectVtxColor.color, FileSelectVtxColor.color, FileSelectVtxColor.color);
	ghFontPuts(text, x, y, scale);
}

// Draws the 2D version of the title screen background
void DrawTitleStatic()
{
	float z = 1.2f;
	char texture_id = 0;
	helperFunctionsGlobal->PushScaleUI(Align::Align_Center, true, 1.0f, 1.0f);
	if (IsScreenResolution640x480())
	{
		njSetTexture(&texlist_ava_title_cmn_small);
		switch (ModConfig::TitleScreenLogoMode)
		{
		case TitleScreenConfig::Static:
			texture_id = Language ? 22 : 15;
			break;
		case TitleScreenConfig::Limited:
			texture_id = 29;
			break;
		default:
			texture_id = 0;
			break;
		}
	}
	else
	{
		njSetTexture(&texlist_ava_gtitle0);
		texture_id = 0;
	}
	ghSetPvrTexMaterial((!MainMenuAccessed || TitleScreenFadedIntoBlack) ? TitleBGTransparency.color : 0xFFFFFFFF);
	ghDrawPvrTextureS(texture_id, 0, 0, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 1, 0, 256, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 2, 256, 0, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 3, 256, 256, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 4, 512, 0, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 5, 512, 128, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(texture_id + 6, 512, 256, z, 1.0f, 1.0f);
	ghDrawPvrTextureS(7, 512, 384, z, 1.0f, 1.0f);
	helperFunctionsGlobal->PopScaleUI();
}

// Creates ripples by distorting vertices in the model
void DistortVertices(NJS_MODEL_SADX* model, unsigned int r30, int index)
{
	float kj_fWaveTitleWaveRad = 4.0f;
	float kj_fWaveTitleWaveSpd = 0.033f;
	float kj_fWaveTitleWaveHeight = 4.3f;
	int kj_xWaveTitleFlactRot = 50;
	int kj_xWaveTitleWaveRot = 1024;
	float f1 = PYTHAGORAS(model->points[index].x, model->points[index].z);
	Angle angle = (Angle)(r30 * kj_xWaveTitleFlactRot + 7.0f);
	Angle angle_f3 = (Angle)(r30 - f1 / kj_fWaveTitleWaveSpd) * kj_xWaveTitleWaveRot;
	float f30 = (njSin(angle) + 1.0f) * 0.15f + 0.7f;
	float f3 = njSin(angle_f3) / ((1.0f / kj_fWaveTitleWaveRad) * f1 + 1.0f);
	float f2 = 1.0f - (model->points[index].z + 21.0f) * 0.023809524f;
	model->points[index].y = kj_fWaveTitleWaveHeight * f3 * f30;
	model->normals[index].y = (f2 * f3 * f30 + 1.0f) * 0.5f;
}

// Creates the palettes used for the title screen
void GenerateGradientForPalette()
{
	for (int c = 0; c < 256; c++)
	{
		LSPAL_0[0][c][0].argb.r = 255;
		LSPAL_0[0][c][0].argb.r = 255 - c;
		LSPAL_0[0][c][0].argb.g = 255 - c;
		LSPAL_0[0][c][0].argb.b = 255 - c;
		LSPAL_0[0][c][1].argb.r = 0;
		LSPAL_0[0][c][1].argb.r = 0;
		LSPAL_0[0][c][1].argb.g = 0;
		LSPAL_0[0][c][1].argb.b = 0;
	}
	if (ModConfig::DLLLoaded_Lantern)
		generate_atlas();
}

// Draw the 3D version of the title screen background
void DrawTitle_New(TitleNewWk* tnk)
{
	if (!RipplesOn)
	{
		DrawTitleStatic();
		return;
	}
	// Set scaling
	float scale = 1.0f;
	float ResolutionScaleX = (float)HorizontalResolution / 640.0f;
	float ResolutionScaleY = (float)VerticalResolution / 480.0f;
	if (ResolutionScaleX > ResolutionScaleY)
		scale= ResolutionScaleX / ResolutionScaleY;
	else if (ResolutionScaleX < ResolutionScaleY)
		scale= ResolutionScaleY / ResolutionScaleX;
	else
		scale= 1.0f;
	float kj_fWaveTitleOfsBaseX = 0.0f;
	float kj_fWaveTitleOfsBaseY = -1.25f;
	float kj_fWaveTitleOfsBaseZ = -40.0f;
	int kj_dWaveTitleTime = 3; // 2
	unsigned int r30 = tnk->wavetimer++ * kj_dWaveTitleTime;
	float kj_fWaveTitleSclBaseX = 1.15f * scale;
	float kj_fWaveTitleSclBaseY = 1.13f * scale;
	if (tnk->Stat == 1)
		tnk->logotimer = 0;
	else if (tnk->logotimer < 70)
		tnk->logotimer++;
	njSetTexture(IsScreenResolution640x480() ? &texlist_ava_title_cmn_small : &texlist_ava_gtitle0);
	njPushMatrix(NULL);
	njTranslate(NULL, kj_fWaveTitleOfsBaseX, kj_fWaveTitleOfsBaseY, kj_fWaveTitleOfsBaseZ);
	njScale(NULL, kj_fWaveTitleSclBaseX, kj_fWaveTitleSclBaseY, 1.0f);
	njRotateX(NULL, 0x4000);
	dsScaleLight(0.80f);
	for (int i = 0; i < 1071; i++)
	{
		DistortVertices(object_hamon_null1_null1->child->sibling->basicdxmodel, r30, i);
		DistortVertices(object_hamon_null1_null1->child->basicdxmodel, r30, i);
	}
	SaveControl3D();
	njControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL | NJD_CONTROL_3D_CONSTANT_ATTR);
	SaveConstantAttr();
	if (RipplesOn)
		___dsSetPalette(0);
	else
		OnConstantAttr(0, NJD_FLAG_IGNORE_LIGHT);
	float alpha = (!MainMenuAccessed || TitleScreenFadedIntoBlack) ? (float)TitleBGTransparency.argb.a / 255.0f : 1.0f;
	SetMaterial(alpha, alpha, alpha, alpha);
	dsDrawObject(object_hamon_null1_null1);
	ResetMaterial();
	njPopMatrix(1u);
	LoadConstantAttr();
	LoadControl3D();
	dsReScaleLight();
}

// Update the variable that the title screen has faded into black
void DelayTransitionHook(AdvaModeEnum a1)
{
	TitleScreenFadedIntoBlack = true;
	TitleTransition = TitleScreenTransitionMode::FadeOut;
	CmnAdvaModeProcedure(a1);
}

// Plays the Start sound and sets variables for the crossfade
int PlayStartSound_EnableTransition()
{
	TitleScreenFadedIntoBlack = false;
	EnableFileSelectScreenFade = false;
	TitleTransition = TitleScreenTransitionMode::FadeOut;
	dsPlay_oneshot(SE_DECIDE, 0, 0, 0);
	return 0;
}

// Plays the Return sound and sets variables for the crossfade back
int PlayReturnSound_EnableTransition()
{
	dsPlay_oneshot(SE_PAUSE, 0, 0, 0);
	TitleTransition = TitleScreenTransitionMode::FadeIn;
	EnableFileSelectScreenFade = false;
	SonicTeamAlpha = 0;
	return 0;
}

// Draws the main menu background
void DisplayTitleNew_3_r(float depth)
{
	float z = depth - 4.0f;
	char texture_id = 0;
	MainMenuAccessed = true;
	EnableFileSelectScreenFade = true;
	njTextureShadingMode(1);
	ghSetPvrTexMaterial(0xFFFFFFFF);
	// Set texture
	if (!IsScreenResolution640x480())
		njSetTexture(&texlist_ava_gtitle0);
	else
	{
		switch (ModConfig::TitleScreenLogoMode)
		{
		case TitleScreenConfig::US_EU:
		case TitleScreenConfig::International:
			njSetTexture(&texlist_ava_title_cmn_small);
			break;
		case TitleScreenConfig::Static:
			njSetTexture(&texlist_ava_title_back);
			texture_id = Language ? 0 : 8;
			break;
		case TitleScreenConfig::Limited:
			njSetTexture(&texlist_ava_title_back);
			texture_id = 16;
			break;
		}
	}
	if (!loop_count)
	{
		// Draw background
		helperFunctionsGlobal->PushScaleUI(Align::Align_Center, true, 1.0f, 1.0f);
		helperFunctionsGlobal->SetScaleFillMode(FillMode_Fill);
		ghDrawPvrTextureS(texture_id, 0.0f, 0.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 1, 0.0f, 256.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 2, 256.0f, 0.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 3, 256.0f, 256.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 4, 512.0f, 0.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 5, 512.0f, 128.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(texture_id + 6, 512.0f, 256.0f, z, 1.0f, 1.0f);
		ghDrawPvrTextureS(7, 512.0f, 384.0f, z, 1.0f, 1.0f);
		helperFunctionsGlobal->PopScaleUI();
		if (!(IsScreenResolution640x480() && ModConfig::TitleScreenLogoMode >= TitleScreenConfig::Static)) // Don't draw in static modes at 640x480
		{
			helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
			// Draw logo
			// Japanese, static or Limited are 30 pixels higher
			float yoff = (!Language || ModConfig::TitleScreenLogoMode < TitleScreenConfig::Static || ModConfig::TitleScreenLogoMode == TitleScreenConfig::Limited) ? 30.0f : 0.0f;
			// Draw logo shadow
			njTextureShadingMode(1);
			if (!IsScreenResolution640x480())
				ghDrawPvrTextureS(13, 64.0f, 140.0f - yoff, z, 1.0f, 1.0f);
			// Draw the actual logo
			if (!IsScreenResolution640x480() && DrawOverlay)
				ghDrawPvrTextureS(15, 64.0f, 140.0f - yoff, z, 1.0f, 1.0f); // Left part with highlight
			else 
				ghDrawPvrTextureS(9, 64.0f, 140.0f - yoff, z, 1.0f, 1.0f); // Left part without highlight
			ghDrawPvrTextureS(10, 64.0f + 256.0f, 140.0f - yoff, z, 1.0f, 1.0f); // Right part
			helperFunctionsGlobal->PopScaleUI();
		}
		// Draw white overlay
		if (!WhiteOverlayDrawn)
		{
			if (!IsScreenResolution640x480() || ModConfig::TitleScreenLogoMode < TitleScreenConfig::Static)
			{
				helperFunctionsGlobal->PushScaleUI(Align::Align_Center, true, 1.0f, 1.0f);
				ghSetPvrTexMaterial(0x99FFFFFF);
				ghDrawPvrTextureS(7, 0.0f, 0.0f, z, 5.0f, 5.0f);
				ghSetPvrTexMaterial(0xFFFFFFFF);
				helperFunctionsGlobal->PopScaleUI();
			}
			TitleTransition = TitleScreenTransitionMode::FadeIn;
		}
	}
	njTextureShadingMode(2);
	njSetTexture(&texlist_ava_title);
}

// Draws an empty File Select screen without calling the actual File Select...
void DrawFileSelectMockup(float depth_orig, bool use_scaling, int VertexColor)
{
	helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	njTextureShadingMode(1);
	ghSetPvrTexBaseColor(VertexColor);
	DrawSkyBg(depth_orig - 2.0f); // AVA_BACK
	DrawTitleBack_r(0.0f, 38.0f, depth_orig - 10.0f, 564.0f, 41.0f); // Green rectangle
	// "Select a file" texture
	njSetTexture(&texlist_ava_vmssel);
	ghDrawPvrTextureS(8, 42.0f, 46.0f, depth_orig - 14.0f, 1.0f, 1.0f);
	// File select window
	ghSetPvrTexMaterial(0x78004FFFu);
	DrawShadowWindow(47.0f, 84.0f, depth_orig - 6.0f, 200.0f, 340.0f);
	/*
	// File buttons
	DrawBG_ava_square_b(0xFF12B4FF, 147.0f, 165.0f, depth_orig - 6.0f, 0.7f, 0.7f);
	// Update the save file count for the options/file select transition
	if (GCMemoca_State.u8FileNum != 0)
		NumberOfSaves = GCMemoca_State.u8FileNum;
	if (NumberOfSaves >= 1)
		DrawBG_ava_square_b(0xFF12B4FF, 147.0f, 225.0f, depth_orig - 6.0f, 0.7f, 0.7f);
	if (NumberOfSaves >= 2)
		DrawBG_ava_square_b(0xFF12B4FF, 147.0f, 285.0f, depth_orig - 6.0f, 0.7f, 0.7f);
	if (NumberOfSaves >= 3)
		DrawBG_ava_square_b(0xFF12B4FF, 147.0f, 345.0f, depth_orig - 6.0f, 0.7f, 0.7f);
	*/
	// Right side
	ghSetPvrTexVertexColor(0x7812B4FFu, 0x7812B4FFu, 0x7812B4FFu, 0x7812B4FFu);
	DrawShadowWindow(268.0f, 78.0f, depth_orig - 4.0f, 320.0f, 360.0f);
	njSetTexture(&texlist_ava_filesel);
	// Yellow thing
	float depth = 0.0f;
	for (int i = 2; i >= 0; i--)
	{
		depth = depth_orig + WakuPrm[i].OfsZ - 1.0f;
		int color = BetweenCol(WakuPrm[i].ColU, WakuPrm[i].ColD, 0.069444448f);
		ghSetPvrTexVertexColor(WakuPrm[i].ColU, color, WakuPrm[i].ColU, color);
		ghDrawPvrTextureS(WakuPrm[i].KadoPvrIdx, 268.0f, 78.0f, depth, 0.78125f, 0.78125f); // Top left corner (yellow)
		ghDrawPvrTextureS(WakuPrm[i].KadoPvrIdx | 0x82000000, 588.0f, 78.0f, depth, 0.78125f, 0.78125f); // Top right corner (yellow)
		ghDrawPvrTextureWH(WakuPrm[i].YokoPvrIdx, 293.0f, 78.0f, depth, 270.0f, 25.0f); // Top horizonal bar
		int color2 = BetweenCol(WakuPrm[i].ColU, WakuPrm[i].ColD, 0.93055558f);
		ghSetPvrTexVertexColor(color, color2, color, color2);
		ghDrawPvrTextureWH(WakuPrm[i].TatePvrIdx, 268.0f, 103.0f, depth, 25.0f, 310.0f); // Left vertical bar
		ghDrawPvrTextureWH(WakuPrm[i].TatePvrIdx | 0x82000000, 588.0f, 103.0f, depth, 25.0f, 310.0f); // Right vertical bar
		ghSetPvrTexVertexColor(color2, WakuPrm[i].ColD, color2, WakuPrm[i].ColD);
		ghDrawPvrTextureS(WakuPrm[i].KadoPvrIdx | 0x46000000, 268.0f, 438.0f, depth, 0.78125f, 0.78125f); // Bottom left corner (yellow)
		ghDrawPvrTextureS(WakuPrm[i].KadoPvrIdx | 0xC8000000, 588.0f, 438.0f, depth, 0.78125f, 0.78125f); // Bottom right corner
		ghDrawPvrTextureWH(WakuPrm[i].YokoPvrIdx | 0x40000000, 293.0f, 413.0f, depth, 270.0f, 25.0f); // Bottom horizontal bar
	}
	// Right pane
	njSetTexture(&texlist_ava_filesel);
	ghSetPvrTexMaterial(0xFFFFFFFF);
	for (int counter = 0; counter < 4; counter++)
	{
		float temp_offset = counter * 57.0f + 200.0f;
		float yoffset = temp_offset - 240.0f;
		njSetTexture(&texlist_ava_filesel);
		ghSetPvrTexMaterial(0xFFFFFFFF);
		ghDrawPvrTexture(0x4000000, 20.0f + 248.0f + 160.0f, 240 + yoffset, depth_orig - 8.0f);
		ghDrawPvrTexture(fs_m_PvrIdx[counter] | 0x4000000, 20.0f + 248.0f + 160.0f, 240.0f + yoffset, depth_orig - 10.0f);
		DrawBG_ava_square_b(0xFF12B4FF, 20.0f + 248.0f + 160.0f, temp_offset + 28.0f, depth_orig - 6.0f, 0.96899998f, 0.5f);
	}
	// Oval thing
	njSetTexture(&texlist_ava_filesel);
	ghSetPvrTexMaterial(0xFFFFFFFF);
	depth = depth_orig - 5.0f;
	ghDrawPvrTextureS(0x4000008, 20.0f + 248.0 + 160.0, 240.0 - 118.0, depth, 1.883f, 1.0f);
	helperFunctionsGlobal->PopScaleUI();
	njTextureShadingMode(2);
}

// Title screen display function
void DrawTitleScreen(TitleNewWk* tnk)
{
	// Variables for logo/background
	float xpos, ypos;
	char texture_id = 11;
	// Fade out from black
	if (TitleTransition == TitleScreenTransitionMode::Normal)
	{
		TitleBGTransparency.argb.a = 255;
		BlackFadeout.argb.a = max(0, BlackFadeout.argb.a - 32);
	}
	// Draw file select screen mockup
	if (!TitleScreenFadedIntoBlack && !MainMenuAccessed && TitleTransition != TitleScreenTransitionMode::None)
	{
		DrawFileSelectMockup(5800.0f, true, 0xFFFFFFFF);
	}
	// Draw title BG
	DrawTitle_New(tnk);
	njSetTexture(IsScreenResolution640x480() ? &texlist_ava_title_cmn_small : &texlist_ava_gtitle0);
	if (!loop_count)
	{
		helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
		if (!(IsScreenResolution640x480() && ModConfig::TitleScreenLogoMode >= TitleScreenConfig::Static)) // Don't draw in static modes at 640x480
		{
			// Set up logo
			ghSetPvrTexMaterial((!MainMenuAccessed || TitleScreenFadedIntoBlack) ? TitleBGTransparency.color : 0xFFFFFFFF);
			xpos = 64.0f;
			// Offset for J or International/Limited
			ypos = (!Language || ModConfig::TitleScreenLogoMode < TitleScreenConfig::Static || ModConfig::TitleScreenLogoMode == TitleScreenConfig::Limited) ? 110.0f : 140.0f;
			njTextureShadingMode(1);
			// Draw logo shadow (HD only)
			if(!IsScreenResolution640x480())
				ghDrawPvrTextureS(13, xpos, ypos, 1.2f, 1.0f, 1.0f);
			// Draw logo
			ghDrawPvrTextureS((!IsScreenResolution640x480() && DrawOverlay) ? 15 : 9, xpos, ypos, 1.2f, 1.0f, 1.0f); // Left part with or without highlight
			ghDrawPvrTextureS(10, xpos + 256.0f, ypos, 1.2f, 1.0f, 1.0f); // Right part
			// Sonic Team logo fade-in
			if (TitleTransition == TitleScreenTransitionMode::Normal)
			{
				if (SonicTeamAlpha <= 247) 
					SonicTeamAlpha += 4;
				else 
					SonicTeamAlpha = TitleBGTransparency.argb.a;
			}
			if (TitleTransition == TitleScreenTransitionMode::FadeOut)
			{
				SonicTeamAlpha = max(0, SonicTeamAlpha - 32);
			}
			if (!EnableTransition)
			{
				SonicTeamAlpha = TitleBGTransparency.argb.a;
			}
			SonicTeamTransparency.argb.a = max(0, SonicTeamAlpha);
			if (SonicTeamTransparency.argb.a > 0)
			{
				ghSetPvrTexMaterial(SonicTeamTransparency.color);
				// Draw Sonic Team logo
				ghDrawPvrTextureS(14, 288.0f, 33.0f, 1.2f, 1.0f, 1.0f);
				// Copyright text for 640x480
				if (IsScreenResolution640x480())
				{
					switch (ModConfig::TitleScreenLogoMode)
					{
					case TitleScreenConfig::US_EU:
						texture_id = 11; // US/EU animated
						break;
					case TitleScreenConfig::International:
						texture_id = Language ? 36: 39; // JP/INT
						break;
					}
					ghDrawPvrTextureS(texture_id, 128.0f, 300.0f, 1.2f, 1.0f, 1.0f);
					ghDrawPvrTextureS(texture_id + 1, 256.0f, 300.0f, 1.2f, 1.0f, 1.0f);
					ghDrawPvrTextureS(texture_id + 2, 384.0f, 300.0f, 1.2f, 1.0f, 1.0f);
				}
				// Copyright text for bigger resolutions
				else
				{
					switch (ModConfig::TitleScreenLogoMode)
					{
					case TitleScreenConfig::US_EU:
					case TitleScreenConfig::Static:
						ghDrawPvrTextureS(Language ? 11 : 16, 64.0f, 300.0f, 1.2f, 1.0f, 1.0f);
						break;
					case TitleScreenConfig::International:
						ghDrawPvrTextureS(Language ? 12 : 17, 64.0f, 300.0f, 1.2f, 1.0f, 1.0f);
						break;
					case TitleScreenConfig::Limited:
						ghDrawPvrTextureS(18, 64.0f, 300.0f, 1.2f, 1.0f, 1.0f);
						break;
					}
				}
			}
		}
		if (TitleTransition != TitleScreenTransitionMode::Normal)
		{
			// Fade out
			if (TitleTransition == TitleScreenTransitionMode::FadeOut)
			{
				{
					// Fade out in progress
					if (TitleBGTransparency.argb.a >= 32)
					{
						TitleBGTransparency.argb.a -= 32;
						if (LogoScaleX < 2.0f)
							LogoScaleX = LogoScaleX * 1.05f;
						if (LogoScaleY < 2.0f)
							LogoScaleY = LogoScaleY * 1.05f;
						if (TitleBGTransparency.argb.a > 64)
							BlackFadeout.argb.a = min(128, BlackFadeout.argb.a + 32);
						else
							BlackFadeout.argb.a = max(32, BlackFadeout.argb.a - 32);
					}
					// Fade out complete
					else
					{
						TitleTransition = TitleScreenTransitionMode::FadeEnd;
						TitleBGTransparency.argb.a = 0;
						BlackFadeout.argb.a = 0;
						LogoScaleX = 2.0f;
						LogoScaleY = 2.0f;
					}
				}
			}
			// Fade back in
			if (TitleTransition == TitleScreenTransitionMode::FadeIn)
			{
				if (TitleBGTransparency.argb.a <= 223)
				{
					BlackFadeout.argb.a = min(128, BlackFadeout.argb.a + 32);
					TitleBGTransparency.argb.a += 32;
					if (LogoScaleX > 1.0f)
						LogoScaleX = LogoScaleX * 0.92f;
					if (LogoScaleY > 1.0f)
						LogoScaleY = LogoScaleY * 0.92f;
				}
				else
				{
					BlackFadeout.argb.a = max(32, BlackFadeout.argb.a - 32);
					TitleTransition = TitleScreenTransitionMode::Normal;
					TitleBGTransparency.argb.a = 255;
					LogoScaleX = 1.0f;
					LogoScaleY = 1.0f;
				}
			}
			if (TitleTransition == TitleScreenTransitionMode::None)
			{
				if (TitleBGTransparency.argb.a <= 223)
				{
					BlackFadeout.argb.a = max(4, BlackFadeout.argb.a - 4);
					TitleBGTransparency.argb.a += 32;
					if (LogoScaleX > 1.0f)
						LogoScaleX = LogoScaleX * 0.92f;
					if (LogoScaleY > 1.0f)
						LogoScaleY = LogoScaleY * 0.92f;
				}
				else
				{
					TitleTransition = TitleScreenTransitionMode::Normal;
					TitleBGTransparency.argb.a = 255;
					LogoScaleX = 1.0f;
					LogoScaleY = 1.0f;
				}
			}
			// Draw logo transition
			xpos = (640.0f - LogoScaleX * 502.0f) / 2.0f;
			ypos = (480.0f - LogoScaleY * 256.0f) / 2.0f;
			ghSetPvrTexMaterial(TitleBGTransparency.color);
			if (EnableTransition && LogoScaleX > 1.02f)
			{
				ghDrawPvrTextureS(9, xpos, ypos, 1.2f, LogoScaleX, LogoScaleY);
				ghDrawPvrTextureS(10, xpos + 256.0f * LogoScaleX, ypos, 1.2f, LogoScaleX, LogoScaleY);
			}
		}
		njTextureShadingMode(2);
		helperFunctionsGlobal->PopScaleUI();
		// Draw black/white box if transitioning
		if ((TitleTransition >= TitleScreenTransitionMode::FadeOut && TitleTransition < TitleScreenTransitionMode::FadeIn) || TitleTransition == TitleScreenTransitionMode::None)
		{
			TitleBackOverlayColor.argb.a = min(153, TitleBackOverlayColor.argb.a + 8);
		}
		if (TitleTransition == TitleScreenTransitionMode::FadeIn || TitleTransition < TitleScreenTransitionMode::FadeOut)
		{
			TitleBackOverlayColor.argb.a = max(8, TitleBackOverlayColor.argb.a - 8);
		}
		if (MainMenuAccessed && !TitleScreenFadedIntoBlack)
		{
			helperFunctionsGlobal->PushScaleUI(Align::Align_Center, true, 1.0f, 1.0f);
			ghSetPvrTexMaterial(TitleBackOverlayColor.color);
			ghDrawPvrTextureS(7, 0.0f, 0.0f, 1.2f, 5.0f, 5.0f);
			ghSetPvrTexMaterial(0xFFFFFFFF);
			WhiteOverlayDrawn = true;
			helperFunctionsGlobal->PopScaleUI();
		}
		else
			late_DrawBoxFill2D(0.0f, 0.0f, (float)HorizontalResolution, (float)VerticalResolution, 1.2f, BlackFadeout.color, LATE_LIG);
	}
}

// Draws the PressStartButton texture
void DrawPressStart()
{
	if (loop_count || TitleTransition >= TitleScreenTransitionMode::FadeOut)
		return;
	helperFunctionsGlobal->PushScaleUI(Align::Align_Center, false, 1.0f, 1.0f);
	njTextureShadingMode(1);
	njSetTexture(IsScreenResolution640x480() ? &texlist_ava_title_cmn_small : &texlist_ava_gtitle0);
	ghDrawPvrTextureS(8, 193.0f, 364.0f, 1.1f, 1.0f, 1.0f);
	njTextureShadingMode(2);
	helperFunctionsGlobal->PopScaleUI();
}

// Sets the vertex color for the File Select screen
void FileSelectVtxColorHook(Uint32 a1)
{
	FileSelectVtxColor.color = a1;
	ghSetPvrTexBaseColor(EnableFileSelectScreenFade ? a1 : 0xFFFFFFFF);
}

// Hook to force vertex color for the sky background in the File Select screen
void FileSelect_AVA_BACK_Hook(float depth)
{
	ghSetPvrTexBaseColor(0xFFFFFFFF);
	DrawSkyBg(depth);
}

// Hook to force vertex color for the File Select screen
void FileSelect_VtxColorB_Hook(Uint32 a1)
{
	ghSetPvrTexMaterial(CurrentMenuIndex == ADVA_MODE_FILE_SEL ? FileSelectVtxColor.color : a1);
}

// Hook to force vertex color for character names in the File Select screen
void CharacterName_VtxColorB_Hook(Uint32 a1)
{
	if (CurrentMenuIndex == ADVA_MODE_FILE_SEL)
		ghSetPvrTexMaterial(FileSelectVtxColor.color);
	else
	{
		if (!ModConfig::DLLLoaded_HDGUI && a1 == 0xFFFFFFFF)
			a1 = 0xF0FFFFFF;
		ghSetPvrTexMaterial(a1);
	}
}

// Avoid drawing the green rectangle in the transition between file select and options because it draws on top of everything for whatever reason
void FileSelectGreenRectHook(float x, float y, float z, float width, float height)
{
	if (FileSelectVtxColor.color == 0xFFFFFFFF || (CurrentMenuIndex != ADVA_MODE_OPTION_SEL && CurrentMenuIndex != ADVA_MODE_FILE_SEL) || (CurrentMenuIndex == ADVA_MODE_FILE_SEL && PreviousMenuIndex != ADVA_MODE_OPTION_SEL))
		DrawTitleBack(x, y, z, width, height);
}

// Hook to set vertex color for the main menu
void MainMenuVtxColorHook(Uint32 a1)
{
	ghSetPvrTexBaseColor(CurrentMenuIndex == 5 ? 0xFFFFFFFF : a1);
}

// Reset 3D rendering stuff when resolution changes
void UpdateTransform_Apply()
{
	njUnitMatrix(0);
	NJS_VECTOR orig = { 0, 0, -1.0f };
	NJS_VECTOR result;
	njPushMatrix(nj_unit_matrix_);
	njCalcVector(0, &orig, &result);
	njPopMatrix(1u);
	View.vx = result.x;
	View.vy = result.y;
	View.vz = result.z;
	View.px = 0;
	View.py = 0;
	View.pz = 0;
	View.roll = 0;
	njSetCamera(&View);
	UpdateTransform = false;
}

// Trampoline to set the next menu and update variables
static Trampoline* CmnAdvaModeProcedure_t = nullptr;
static void __cdecl CmnAdvaModeProcedure_r(AdvaModeEnum mode)
{
	const auto original = TARGET_DYNAMIC(CmnAdvaModeProcedure);
	if (ModConfig::TitleScreenLogoMode != TitleScreenConfig::SADX && ModConfig::EnableDCBranding)
	{
		PreviousMenuIndex = CurrentMenuIndex;
		if (mode == ADVA_MODE_TITLE_NEW)
			GenerateGradientForPalette();
		CurrentMenuIndex = mode;
	}
	original(mode);
}

// Trampoline for the Options screen to draw the File Select mockup and force vertex color
static Trampoline* option_sel_disp_t = nullptr;
static void __cdecl option_sel_disp_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(option_sel_disp);
	if (ModConfig::TitleScreenLogoMode != TitleScreenConfig::SADX && ModConfig::EnableDCBranding)
	{
		// Don't draw the options screen if the current menu has nothing to do with it
		if (CurrentMenuIndex != ADVA_MODE_OPTION_SEL && CurrentMenuIndex != ADVA_MODE_FILE_SEL && CurrentMenuIndex != ADVA_MODE_TITLE_MENU && CurrentMenuIndex != ADVA_MODE_SND_TEST)
			return;
		// Dont' draw the options screen if the file select overlay obscures it completely
		if (FileSelectVtxColor.color == 0xFFFFFFFF)
			return;
		// Draw file select overlay when going from options to file select
		if (CurrentMenuIndex == ADVA_MODE_OPTION_SEL && PreviousMenuIndex == ADVA_MODE_FILE_SEL)
			DrawFileSelectMockup(5800.0f, true, 0xFFFFFFFF);
		original(tp);
		// Draw file select overlay when going from file select to options
		if (CurrentMenuIndex == ADVA_MODE_FILE_SEL && PreviousMenuIndex == ADVA_MODE_OPTION_SEL)
			DrawFileSelectMockup(1000.0f, true, FileSelectVtxColor.color);
	}
	else
		original(tp);
}

// Trampoline for the title screen load function to update variables
static Trampoline* SetAdvaTitleNewTask_t = nullptr;
static void __cdecl SetAdvaTitleNewTask_r(ModeSelPrmType* prmp)
{
	const auto original = TARGET_DYNAMIC(SetAdvaTitleNewTask);
	if (RipplesOn)
		UpdateTransform = true;
	GenerateGradientForPalette();
	original(prmp);
}

// Trampoline for the title screen display function
static Trampoline* title_new_disp_t = nullptr;
static void __cdecl title_new_disp_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(title_new_disp);
	// Draw the original DX title screen
	if (ModConfig::TitleScreenLogoMode == TitleScreenConfig::SADX)
	{
		original(tp);
		return;
	}
	NJS_COLOR StartColor = { 0x00FFFFFF }; // PressStartButton initial color
	if (UpdateTransform)
		UpdateTransform_Apply();
	TitleNewWk* twk = (TitleNewWk*)tp->awp; // Title screen worker
	if (twk->BaseCol)
	{
		if (twk->SubMode >= 0)
		{
			SetDefaultAlphaBlend();
			if (TitleTransition != TitleScreenTransitionMode::None && TitleTransition != TitleScreenTransitionMode::FadeIn)
				ghSetPvrTexBaseColor(0xFFFFFFFF);
			else if (!TitleScreenFadedIntoBlack && PreviousMenuIndex == ADVA_MODE_TITLE_MENU)
				ghSetPvrTexBaseColor(0xFFFFFFFF);
			else
				ghSetPvrTexBaseColor(twk->BaseCol);
			DrawTitleScreen(twk);
			// Draw Press Start
			unsigned char PressStartTransparency = twk->titleblinktimer;
			if ((twk->titleblinktimer & 0x1FFu) >= 0x100)
				PressStartTransparency = -1 - twk->titleblinktimer;
			StartColor.argb.a = PressStartTransparency;
			ghSetPvrTexMaterial(StartColor.color);
			DrawPressStart();
		}
	}
}

void AdvertiseTitleScreen_OnReset()
{
	UpdateTransform = true;
}

void AdvertiseTitleScreen_Init()
{
	AdvertiseTitleScreen_OnReset();
	UpdateTransform = true;
	CmnAdvaModeProcedure_t = new Trampoline(0x00505B40, 0x00505B45, CmnAdvaModeProcedure_r);
	option_sel_disp_t = new Trampoline(0x00509810, 0x00509815, option_sel_disp_r);
	SetAdvaTitleNewTask_t = new Trampoline(0x00510390, 0x00510396, SetAdvaTitleNewTask_r);
	title_new_disp_t = new Trampoline(0x00510350, 0x00510355, title_new_disp_r);
	object_hamon_null1_null1 = LoadModel("system\\data\\advertise\\hamon_null1.nja.sa1mdl");
	TitleBackOverlayColor.color = 0x99FFFFFF;
	// Title screen textures for HD
	texlist_ava_gtitle0 = texlist_ava_gtitle00_e_dc_hd;
	AvaTexLdPrmJ[17].FileName = "AVA_GTITLE0_DC_HD";
	AvaTexLdPrmE[17].FileName = "AVA_GTITLE0_DC_HD";
	AvaTexLdPrmF[17].FileName = "AVA_GTITLE0_DC_HD";
	AvaTexLdPrmG[17].FileName = "AVA_GTITLE0_DC_HD";
	AvaTexLdPrmS[17].FileName = "AVA_GTITLE0_DC_HD";
	// Title screen textures for 640x480
	texlist_ava_title_cmn_small = texlist_ava_gtitle00_640;
	AvaTexLdPrmJ[29].FileName = "AVA_GTITLE0_DC";
	AvaTexLdPrmE[29].FileName = "AVA_GTITLE0_DC";
	AvaTexLdPrmF[29].FileName = "AVA_GTITLE0_DC";
	AvaTexLdPrmG[29].FileName = "AVA_GTITLE0_DC";
	AvaTexLdPrmS[29].FileName = "AVA_GTITLE0_DC";
	// Main menu textures for 640x480
	texlist_ava_title_back = texlist_title_back_640;
	AvaTexLdPrmF[20].FileName = "AVA_TITLE_BACK_DC";
	AvaTexLdPrmE[20].FileName = "AVA_TITLE_BACK_DC";
	AvaTexLdPrmF[20].FileName = "AVA_TITLE_BACK_DC";
	AvaTexLdPrmG[20].FileName = "AVA_TITLE_BACK_DC";
	AvaTexLdPrmS[20].FileName = "AVA_TITLE_BACK_DC";
	// Title screen modes
	DrawOverlay = RipplesOn = EnableTransition = (ModConfig::TitleScreenLogoMode == TitleScreenConfig::International || ModConfig::TitleScreenLogoMode == TitleScreenConfig::US_EU);
	WriteJump(DisplayTitleNew_3, DisplayTitleNew_3_r); // Main menu background
	// Various transition stuff
	SonicTeamAlpha = -256;	
	BlackFadeout.color = 0x00000000;
	TitleBGTransparency.color = 0x00FFFFFF;
	SonicTeamTransparency.color = 0x00FFFFFF;
	TitleTransition = TitleScreenTransitionMode::None;
	LogoScaleX = 2.0f;
	LogoScaleY = 2.0f;
	WriteCall((void*)0x00503DD8, PlayReturnSound_EnableTransition);
	WriteCall((void*)0x0050E386, PlayStartSound_EnableTransition);
	WriteCall((void*)0x0050E3E2, DelayTransitionHook);
	// Transitions and other fixes
	WriteCall((void*)0x00504F9C, FileSelectGreenRectHook);
	WriteCall((void*)0x00504C38, DrawStringHook);
	WriteCall((void*)0x00504D2C, DrawStringHook);
	WriteCall((void*)0x00504B5F, DrawStringHook);
	WriteCall((void*)0x00504ABF, DrawButtonHook);
	WriteCall((void*)0x005077D1, FileSelect_VtxColorB_Hook); // AVA_SAN
	WriteCall((void*)0x00504830, FileSelect_VtxColorB_Hook); // Character spheres
	WriteCall((void*)0x00504143, FileSelect_VtxColorB_Hook); // Emblem count 1
	WriteCall((void*)0x00507454, FileSelect_VtxColorB_Hook); // Emblem count 2
	WriteCall((void*)0x005076DC, FileSelect_VtxColorB_Hook); // Emblem count 3
	WriteCall((void*)0x00507261, CharacterName_VtxColorB_Hook); // Character name 1
	WriteCall((void*)0x005071F2, FileSelect_VtxColorB_Hook); // Character name 2
	WriteCall((void*)0x00503E69, FileSelect_VtxColorB_Hook); // Last Adventure Field
	WriteCall((void*)0x00503F09, FileSelect_VtxColorB_Hook); // Stage Completed
	WriteCall((void*)0x0050400B, FileSelect_VtxColorB_Hook); // Play Time 1
	WriteCall((void*)0x00507454, FileSelect_VtxColorB_Hook); // Play Time 2
	WriteCall((void*)0x0050557D, FileSelect_AVA_BACK_Hook);
	WriteCall((void*)0x0050558D, FileSelectVtxColorHook); // FileSelect_Display
	WriteCall((void*)0x0050BCC8, MainMenuVtxColorHook); // MainMenu_Display
	// PVM indices to load in different menus
	AvaTexLdLists[5] = (int*)&TitleScreenIndices;
	AvaTexLdLists[6] = (int*)&MainMenuIndices;
}

void AdvertiseTitleScreen_OnFrame()
{
	WhiteOverlayDrawn = false;
	// Reset title screen transition mode
	if (ulGlobalMode != MD_TITLE2 && TitleTransition == TitleScreenTransitionMode::FadeEnd)
		TitleTransition = TitleScreenTransitionMode::FadeIn;
}