#include "stdafx.h"

// TODO: Fix letters for real

static bool ModelsLoaded_STG00;

void DrawHedgehogHammerScoreboardHook(NJS_POINT3COL* a1, int texnum, NJD_DRAW n, LATE a4)
{
	late_DrawPolygon3D(a1, texnum, n, (current_event != -1) ? a4 : LATE_NO);
}

void DrawHedgehogHammerTextHook(NJS_SPRITE* sp, Int n, NJD_SPRITE attr)
{
	if (current_event != -1)
		dsDrawSprite3D(sp, n, attr);
	else
		late_DrawSprite3D(sp, n, attr, LATE_NO);
}

// Queue the Hedgehog Hammer platform
void DrawHedgehogHammerPlatform(NJS_OBJECT* obj, float scale)
{
	late_z_ofs___ = -47000.0f;
	late_DrawObjectClipEx(obj, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
}

void HedgehogHammer_Init()
{
	ReplaceSET("SET0000A");
	ReplaceSET("SET0000S");
	ReplaceSET("SET0001S");
	ReplacePVM("MOGURATATAKI");
	// Hedgehog Hammer fixes
	WriteCall((void*)0x00528926, DrawHedgehogHammerPlatform);
	WriteCall((void*)0x005279E2, DrawHedgehogHammerTextHook);
	WriteCall((void*)0x00527C53, DrawHedgehogHammerScoreboardHook); // Adventure Field version
	WriteCall((void*)0x00625713, DrawHedgehogHammerScoreboardHook); // Minigame version
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg00[0][i].f32EndZ = 16000.0f;
		pFogTable_Stg00[0][i].f32StartZ = 5000.0f;
	}
}

void HedgehogHammer_Load()
{
	LevelLoader(LevelAndActIDs_HedgehogHammer, "SYSTEM\\data\\stg00_practice\\landtable0000.c.sa1lvl", &texlist_adv_EC32);
	ModelsLoaded_STG00 = true;
}