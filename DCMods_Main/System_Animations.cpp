#include "stdafx.h"
#include "System_Animations.h"

std::vector<TextureAnimation*> TextureAnimationData; // Texture animations that work only in a specified level/act.
std::vector<TextureAnimation*> TextureAnimationData_Permanent; // Texture animations that work regardless of level ID.
std::vector<UVAnimation*> UVAnimationData; // UV animations that work only in a specified level/act.
std::vector<UVAnimation*> UVAnimationData_Permanent; // UV animations that work regardless of level ID.

void AnimateTexture(TextureAnimation* texanim)
{
	//PrintDebug("Animation: level %d, act %d, original %d, final %d\n", texanim->level, texanim->act, texanim->Frames[0], texanim->Frames[1]);
	int framenumber;
	int actualspeed = 1;
	// Calculate animation speed if in 30 or 15 FPS mode
	if (g_CurrentFrame > 1 && texanim->Speed > 1)
		actualspeed = texanim->Speed / 2;
	else
		actualspeed = texanim->Speed;
	if (texanim->material && ulGlobalTimer % actualspeed == 0)
	{
		// Deal with non-sequential animations manually
		if (texanim->NonSequential)
		{
			for (int i = 0; i < 16; i++)
			{
				if (texanim->material->attr_texId != texanim->Frames[i])
					continue;
				if (i < 15 && texanim->Frames[i + 1] != -1)
					texanim->material->attr_texId = texanim->Frames[i + 1];
				else
					texanim->material->attr_texId = texanim->Frames[0];
				goto nonseq_done;
			}
		nonseq_done:
			return;
		}
		// Animate automatically if sequential
		else
		{
			framenumber = texanim->material->attr_texId;
			framenumber++;
			// Reset if reached end of animation or incorrect initial frame
			if (framenumber > texanim->Frames[1] || framenumber < texanim->Frames[0])
				framenumber = texanim->Frames[0];
			texanim->material->attr_texId = framenumber;
			//PrintDebug("Framenumber for material %X: %d\n", texanim->material, framenumber);
		}
	}
}

void AnimateUVs(UVAnimation* animation)
{
	if (ssActNumber == animation->act || animation->act == -1)
	{
		int actualtimer = 1;
		// Calculate animation speed if in 30 or 15 FPS mode
		if (g_CurrentFrame > 1 && animation->timer > 1)
			actualtimer = animation->timer / 2;
		else
			actualtimer = animation->timer;
		if (actualtimer == 0)
			actualtimer = 1;
		if (animation->uv_pointer && animation->uv_count && ulGlobalTimer % actualtimer == 0)
		{
			animation->v_shift += animation->v_speed;
			animation->u_shift += animation->u_speed;
			//PrintDebug("U speed: %d, V speed: %d, U shift: %d, V shift: %d\n", animation->u_speed, animation->v_speed, animation->u_shift, animation->v_shift);
			// Limit V +
			if (animation->v_shift > 510)
			{
				animation->v_shift -= 510;
				for (int i = 0; i < animation->uv_count; i++)
				{
					animation->uv_pointer[i].v -= 510;
				}
			}
			// Limit V -
			if (animation->v_shift < -510)
			{
				animation->v_shift += 510;
				for (int i = 0; i < animation->uv_count; i++)
				{
					animation->uv_pointer[i].v += 510;
				}
			}
			// Limit U +
			if (animation->u_shift > 510)
			{
				animation->u_shift -= 510;
				for (int i = 0; i < animation->uv_count; i++)
				{
					animation->uv_pointer[i].u -= 510;
				}
			}
			// Limit U -
			if (animation->u_shift < -510)
			{
				animation->u_shift += 510;
				for (int i = 0; i < animation->uv_count; i++)
				{
					animation->uv_pointer[i].u += 510;
				}
			}
			// Add U and V
			for (int i = 0; i < animation->uv_count; i++)
			{
				animation->uv_pointer[i].v += animation->v_speed;
				animation->uv_pointer[i].u += animation->u_speed;
			}
			//PrintDebug("UV Animation count %d, timer %d, add %d, current %d \n", animation->uv_count, animation->timer, animation->v_shift, animation->uv_pointer[0].v);
		}
	}
	else return;
}

void RunCustomAnimations()
{
	int levelid = -1;
	int actid = 0;
	switch (ChaoStageNumber)
	{
	case CHAO_STG_RACE:
		levelid = LevelIDs_ChaoRace;
		actid = 1;
		break;
	case CHAO_STG_SS:
		levelid = LevelIDs_SSGarden;
		break;
	case CHAO_STG_EC:
		levelid = LevelIDs_ECGarden;
		break;
	case CHAO_STG_MR:
		levelid = LevelIDs_MRGarden;
		break;
	case -1:
	default:
		levelid = ssStageNumber;
		actid = ssActNumber;
		break;
	}

	// Texture animations
	for (TextureAnimation* anim : TextureAnimationData)
	{
		if (anim->level == levelid && (anim->act == actid || anim->act == -1))
			AnimateTexture(anim);
	}

	// Texture animations permanent
	for (TextureAnimation* anim : TextureAnimationData_Permanent)
	{
		if (anim->level == levelid && (anim->act == actid || anim->act == -1))
			AnimateTexture(anim);
	}

	// UV animations
	for (UVAnimation* uvanim : UVAnimationData)
	{
		if (uvanim->level == levelid && (uvanim->act == actid || uvanim->act == -1))
			AnimateUVs(uvanim);
	}

	// UV animations permanent
	for (UVAnimation* uvanim : UVAnimationData_Permanent)
	{
		if (uvanim->level == levelid && (uvanim->act == actid || uvanim->act == -1))
			AnimateUVs(uvanim);
	}
}

void ClearTextureAnimationData()
{
	for (TextureAnimation* texanim : TextureAnimationData)
	{
		if (texanim->level != ssStageNumber)
		{
			//PrintDebug("Deleting texture animation for %d/%d\n", texanim->level, texanim->act);
			delete texanim;
			texanim = nullptr;
		}
	}
	TextureAnimationData.clear();
	for (UVAnimation* uvanim : UVAnimationData)
	{
		if (uvanim->level != ssStageNumber)
		{
			delete uvanim;
			uvanim = nullptr;
		}
	}
	UVAnimationData.clear();
}

// Adds a level texture animation. This will be cleared when the character leaves the current level/act.
void AddTextureAnimation(int level, int act, NJS_MATERIAL* material, bool nonsequential, int speed, int frame1, int frame2, int frame3, int frame4, int frame5, int frame6, int frame7, int frame8, int frame9, int frame10, int frame11, int frame12, int frame13, int frame14, int frame15, int frame16)
{
	// Check for duplicate materials
	for (TextureAnimation* i : TextureAnimationData)
	{
		if (i->material == material)
		{
			//PrintDebug("Already added %d/%d\n", level, act);
			return;
		}
	}
	TextureAnimation* anim = new TextureAnimation{ level, act, material, speed, nonsequential, { frame1,frame2,frame3,frame4,frame5,frame6,frame7,frame8,frame9,frame10,frame11,frame12,frame13,frame14,frame15,frame16} };
	TextureAnimationData.push_back(anim);
	//PrintDebug("Added texture animation: level %X, act %d, frame1: %d, frame2: %d, total: %d\n", level, act, frame1, frame2, TextureAnimationData.size());
}

// Adds a texture animation. This will stay even when the character leaves the current level/act.
void AddTextureAnimation_Permanent(int level, int act, NJS_MATERIAL* material, bool nonsequential, int speed, int frame1, int frame2, int frame3, int frame4, int frame5, int frame6, int frame7, int frame8, int frame9, int frame10, int frame11, int frame12, int frame13, int frame14, int frame15, int frame16)
{
	// Check for duplicate materials
	for (TextureAnimation* i : TextureAnimationData_Permanent)
	{
		if (i->material == material)
			return;
	}
	TextureAnimation* anim = new TextureAnimation{ level, act, material, speed, nonsequential, frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8, frame9, frame10, frame11, frame12, frame13, frame14, frame15, frame16 };
	TextureAnimationData_Permanent.push_back(anim);
	//PrintDebug("Added permanent texture animation: level %X, act %d, frame1: %d, frame2: %d, total: %d\n", level, act, frame1, frame2, TextureAnimationData.size());
}

int GetUVCount(NJS_MESHSET_SADX* meshset)
{
	int meshtype = meshset->type_matId >> 0xE;
	int indices = 0;
	switch (meshtype)
	{
	case 0: // Tri
		return meshset->nbMesh * 3;
	case 1: // Quad
		return meshset->nbMesh * 4;
	case 2: // NPoly
	case 3: // Strip
		int currentindex = 0;
		int currentmesh = 0;
		do
		{
			//PrintDebug("Mesh %d of %d\n", currentmesh, meshset->nbMesh);
			int numvrt = meshset->meshes[currentindex] & 0x7FFF;
			//PrintDebug("Num: %d\n", numvrt);
			indices += numvrt;
			currentindex += numvrt + 1;
			currentmesh++;
		} while (currentmesh < meshset->nbMesh);
		break;
	}
	//PrintDebug("Mesh type: %d\n", meshtype);
	return indices;
}

// Adds a level UV animation. This will be cleared when the character leaves the current level/act.
void AddUVAnimation(int level, int act, NJS_MESHSET_SADX* meshset, int timer, int u_speed, int v_speed)
{
	// Check for duplicate UVs
	for (UVAnimation* i : UVAnimationData)
	{
		if (i->uv_pointer == meshset->vertuv)
			return;
	}
	int uv_count = GetUVCount(meshset);
	UVAnimation* uvanim = new UVAnimation{ level, act, meshset->vertuv, uv_count, timer, u_speed, v_speed, 0, 0 };
	UVAnimationData.push_back(uvanim);
}

// Adds a UV animation. This will stay even when the character leaves the current level/act.
void AddUVAnimation_Permanent(int level, int act, NJS_MESHSET_SADX* meshset, int timer, int u_speed, int v_speed)
{
	// Check for duplicate UVs
	for (UVAnimation* i : UVAnimationData_Permanent)
	{
		if (i->uv_pointer == meshset->vertuv)
			return;
	}
	int uv_count = GetUVCount(meshset);
	UVAnimation* uvanim = new UVAnimation{ level, act, meshset->vertuv, uv_count, timer, u_speed, v_speed, 0, 0 };
	UVAnimationData_Permanent.push_back(uvanim);
}