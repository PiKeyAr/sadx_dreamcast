#pragma once

#include <SADXModLoader.h>

DataPointer(NJS_MODEL_SADX, model_hasi_enhasi_obj4, 0x0183C594);
DataPointer(NJS_MODEL_SADX, model_hasi_enhasi_bmerge1, 0x0183CC10);
DataPointer(NJS_MODEL_SADX, model_hasi_enhasi_enhasi, 0x0183D2A0);
DataPointer(NJS_MODEL_SADX, model_kaitenkey_dai_dai, 0x0182DFF8);
DataPointer(NJS_MODEL_SADX, model_kaitenkey_dai_totte, 0x0182D6F4);
DataPointer(NJS_OBJECT, object_computer_bmerge6, 0x0185A974);
DataPointer(NJS_OBJECT, object_gt_bgatecente_bgate01, 0x0184C22C);
DataPointer(NJS_ACTION, action_kowaresuisou_hikari_sin, 0x0186DEF8);
DataPointer(NJS_MATERIAL, matlist_suimen_wt_big_wt_big, 0x0180EED8);