#include "stdafx.h"
#include "STG10_FinalEgg.h"

// This has some WIP stuff for OStandLight, most of it is unused.
// TODO: Fix this for real

struct WIDELINE_TEXDATA
{
	int index; // GBIX
	NJS_COLOR pColor[2];
	float uv[4][2];
};

FunctionPointer(void, DrawWideLineTex2, (NJS_VECTOR* pos1, NJS_VECTOR* pos2, const float* widthtable, WIDELINE_TEXDATA* wdata), 0x007A68F0);
DataPointer(CCL_INFO, standlight_colli_info, 0x01AC4664);

float standlight_linewidth[] = { 0.15f, 11.0f };
WIDELINE_TEXDATA standlight_wideline = { 0x19A3A, { 0xFFFFFFFF, 0xFFFFFFFF },  { { 1.0f, 1.0f }, { 1.0f, 0.0f }, { 0.0f, 1.0f }, { 0.0f, 0.0f } } };

NJS_OBJECT* OStandLight_Light = nullptr;

void OStandLight_Display_F(task* a1)
{
	// Info below is outdated
	// There are two light beams in the model.
	// At the moment I don't know how the game selects the one to use (if it uses both).
	// So I just disable the vertices for the other beam.
	// Entity Rotation X is beam X rotation 
	// Entity Rotation Y is object Y rotation
	// Entity Rotation Z is ???
	// Entity Scale X is light+beam model Y rotation (in degrees)
	// Entity Scale Y is beam length 
	// Entity Scale Z is beam width
	int v2; // eax@2
	standlight_taskwk* v1 = (standlight_taskwk*)a1->twp; // esi@1
	// Stretch beam vertices
	OStandLight_Light->child->basicdxmodel->points[19].z = v1->length; // Beam length
	OStandLight_Light->child->basicdxmodel->points[20].z = v1->length; // Beam length
	OStandLight_Light->child->basicdxmodel->points[19].x = (float)(-1.0f * v1->width); // Beam width
	OStandLight_Light->child->basicdxmodel->points[20].x = (float)v1->width; // Beam width
	// Disable the other beam
	OStandLight_Light->child->basicdxmodel->points[23].x = 0;
	OStandLight_Light->child->basicdxmodel->points[23].y = 0;
	OStandLight_Light->child->basicdxmodel->points[23].z = 0;
	OStandLight_Light->child->basicdxmodel->points[24].x = 0;
	OStandLight_Light->child->basicdxmodel->points[24].y = 0;
	OStandLight_Light->child->basicdxmodel->points[24].z = 0;
	if (!loop_count)
	{
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		// Rotate the main object
		v2 = v1->ang.y;
		if (v2)
		{
			njRotateY(0, (unsigned __int16)v2);
		}
		// Render the main object model
		ds_DrawModelClip(((NJS_OBJECT*)0x1C28C78)->basicdxmodel, 1.0f);
		// Render the light part without the beam
		njTranslate(0, ((NJS_OBJECT*)0x1C28C78)->child->pos[0], ((NJS_OBJECT*)0x1C28C78)->child->pos[1], ((NJS_OBJECT*)0x1C28C78)->child->pos[2]);
		njRotateXYZ(0, ((NJS_OBJECT*)0x1C28C78)->child->ang[0] + *(Sint32*)&v1->rot_x_angle, ((NJS_OBJECT*)0x1C28C78)->child->ang[1], ((NJS_OBJECT*)0x1C28C78)->child->ang[2]);
		late_DrawModelClipEx(((NJS_OBJECT*)0x1C28C78)->child->basicdxmodel, LATE_WZ, 1.0f);
		// Rotate and render the beam
		njRotateX(0, v1->ang.x);
		auto BaseRotation = atan2(camera_twp->pos.x - v1->pos.x, camera_twp->pos.y - v1->pos.y);
		// This is hardcoded for now until I figure out how to rotate it properly
		if (ssActNumber == 0 && usPlayer != Characters_Amy)
			njRotateZ(0, NJM_RAD_ANG(BaseRotation));
		if (ssActNumber == 1)
			njRotateZ(0, NJM_RAD_ANG(-BaseRotation));
		if (ssActNumber == 2 || usPlayer == Characters_Amy)
			njRotateZ(0, 16384);
		/* WIP stuff
		float deltax = camera_twp->pos.x - v1->Position.x;
		float deltaz = camera_twp->pos.z - v1->Position.z;
		float cosine = cos((NJM_ANG_DEG(v1->Rotation.y)*3.14159265 / 180.0f));
		auto BaseRotation = atan2(deltax, deltaz);
		PrintDebug("Cosine: %f\n", v1->Position.x*cosine);
		PrintDebug("DeltaX: %f\n", deltax);
		PrintDebug("DeltaZ: %f\n", deltaz);
		if (ssActNumber == 0) njRotateZ(0, NJM_RAD_ANG(BaseRotation));
		else njRotateZ(0, NJM_RAD_ANG(-BaseRotation));*/
		late_DrawModelClipEx(OStandLight_Light->child->basicdxmodel, LATE_LIG, 1.0f);
		njPopMatrix(1u);
	}
}

void OStandLight_Main_F(task* obj)
{
	taskwk* v1; // esi

	v1 = obj->twp;
	EntryColliList(v1);
	if (!CheckRangeOut(obj))
	{
		OStandLight_Display_F(obj);
	}
}

void OStandLight_F(task* a1)
{
	standlight_taskwk* v1 = (standlight_taskwk*)a1->twp; // esi@1
	v1->rot_x_angle = (int)(v1->rot_x * 65536.0f * 0.002777777777777778f);
	CCL_Init(a1, &standlight_colli_info, 1, 4u);
	a1->exec = OStandLight_Main_F;
	a1->disp = OStandLight_Display_F;
	a1->dest = FreeTask;
}

void OStandLight_Init()
{
	OStandLight_Light = CloneObject(&object_standlig_daig_daig);
	OStandLight_Light->evalflags |= NJD_EVAL_HIDE;
	HideMesh_Object(OStandLight_Light->child, 0, 1, 2, 3);
	OStandLight_Light->child->basicdxmodel->mats[4].attrflags &= ~NJD_DA_SRC; // No idea why it has that in the original model
	OStandLight_Light->child->basicdxmodel->mats[4].attrflags |= NJD_DA_ONE;
	HideMesh_Object(object_standlig_daig_daig.child, 4); // Hide beam
	WriteJump(OStandLight, OStandLight_F);
	WriteJump(OStandLight_Main, OStandLight_Main_F);
	WriteJump(OStandLight_Display, OStandLight_Display_F); // O Stand Light function
}