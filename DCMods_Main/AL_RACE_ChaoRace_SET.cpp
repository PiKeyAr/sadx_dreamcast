#include "stdafx.h"

void ChaoRaceStartMark_Load(task* a1); // Extra Start Mark in Pearl course that isn't in the object list
void ChaoRaceWaterfall_Load(task* a1);
void ChaoRaceSkybox_Load(task* a1);
void ChaoRaceCracker_Load(task* a1);

OBJ_CONDITION setdata_race = { };

TaskFunc(OF0, 0x450370); // RING   
TaskFunc(OF1, 0x71D230); // ENTRY BUTTON
TaskFunc(OF2, 0x71CE60); // ZONE
TaskFunc(OF3, 0x71CD50); // PALM
TaskFunc(OF4, 0x71CBC0); // COCONUT
TaskFunc(OF5, 0x71CA70); // FLAG
TaskFunc(OF6, 0x71C9D0); // WHITE FLOWER
TaskFunc(OF7, 0x71C9F0); // PURPLE FLOWER
TaskFunc(OF8, 0x71C2E0); // START MARK
TaskFunc(OF9, 0x71C7D0); // WATERING CAN
TaskFunc(OF10, 0x71C5F0); // BUGLE
TaskFunc(OF11, 0x71C3D0); // SCOOP
TaskFunc(OF12, 0x71C300); // SMALL SCREEN
TaskFunc(OF13, 0x71C320); // ISLET
TaskFunc(OF14, 0x7A9C60); // HINT BOX
TaskFunc(OF15, 0x71C180); // CHEER CHAO
TaskFunc(OF16, 0x71BFF0); // BOW CHAO
TaskFunc(OF17, 0x71BEA0); // CRACKER
TaskFunc(OF18, 0x71BBB0); // BUTTERFLY

void LoadObjects_Race()
{
	task* tp;
	taskwk* twp;
	setdata_race.unionStatus.fRangeOut = 612800.0f;
	// Non-SET objects
	// Start Mark 2
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceStartMark_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 0;
		twp->pos.y = 0;
		twp->pos.z = 0;
		twp->ang.x = 0;
		twp->ang.y = 0;
		twp->ang.z = 0;
	}
	// Skybox
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceSkybox_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 0;
		twp->pos.y = 0;
		twp->pos.z = 0;
		twp->ang.y = 0;
	}
	// Waterfall 1
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceWaterfall_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		taskwk* twp = (taskwk*)tp->twp;
		twp->pos.x = 382.1735f;
		twp->pos.y = 0;
		twp->pos.z = 199.4976f;
		twp->ang.x = 0x68;
		twp->ang.y = 0x88E;
		twp->ang.z = 0;
		twp->counter.ptr = &object_cr4_nc_taki_b_nc_taki_b;
	}
	// Waterfall 2
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceWaterfall_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		taskwk* twp = (taskwk*)tp->twp;
		twp->pos.x = 323.048f;
		twp->pos.y = 0;
		twp->pos.z = 245.5593f;
		twp->ang.x = 0x68;
		twp->ang.y = 0x271;
		twp->ang.z = 0x6;
		twp->counter.ptr = &object_cr4_nc_taki_a_nc_taki_a;
	}
	// SET objects
	// Crackers
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceCracker_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -103.82f;
		twp->pos.y = 10;
		twp->pos.z = 370.34f;
		twp->ang.y = 0x1000;
		twp->ang.z = 0x3000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceCracker_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -86.57f;
		twp->pos.y = 10;
		twp->pos.z = 392.97f;
		twp->ang.y = 0x4000;
		twp->ang.z = 0x3000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceCracker_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -52.01f;
		twp->pos.y = 10;
		twp->pos.z = 384.09f;
		twp->ang.y = 0x7000;
		twp->ang.z = 0x3000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceCracker_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -67.99f;
		twp->pos.y = 10;
		twp->pos.z = 337.77f;
		twp->ang.y = 0xD000;
		twp->ang.z = 0x3000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, ChaoRaceCracker_Load);
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -97.47f;
		twp->pos.y = 10;
		twp->pos.z = 343.87f;
		twp->ang.y = 0xE000;
		twp->ang.z = 0x3000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF8); // START MARK
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 293.08f;
		twp->pos.y = 10.06f;
		twp->pos.z = 230.36f;
		twp->ang.x = 0;
		twp->ang.y = 0x900;
		twp->ang.z = 0;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF10); // BUGLE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -560.62f;
		twp->pos.y = 60.06f;
		twp->pos.z = 96.41f;
		twp->ang.y = 0x1F1E;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF10); // BUGLE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -553.07f;
		twp->pos.y = 62.06f;
		twp->pos.z = 88.41f;
		twp->ang.y = 0x4F1E;
		twp->ang.z = 0xC000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF10); // BUGLE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -563.8f;
		twp->pos.y = 61.68f;
		twp->pos.z = 117.21f;
		twp->ang.x = 0x4000;
		twp->ang.y = 0x600B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF10); // BUGLE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -571.19f;
		twp->pos.y = 62.06f;
		twp->pos.z = 99.27f;
		twp->ang.x = 0x4000;
		twp->ang.y = 0x900B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 357.1f;
		twp->pos.y = 84.24f;
		twp->pos.z = 313.43f;
		twp->ang.y = 0xC3B;
		twp->scl.x = 3;
		twp->scl.y = 1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 359.58f;
		twp->pos.y = 84.24f;
		twp->pos.z = 310.29f;
		twp->ang.y = 0xC3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -575.26f;
		twp->pos.y = 64.24f;
		twp->pos.z = 131.18f;
		twp->ang.y = 0xC3B;
		twp->scl.x = 2;
		twp->scl.y = 1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 145.95f;
		twp->pos.y = 74.52f;
		twp->pos.z = 16.97f;
		twp->ang.y = 0x159B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 270.15f;
		twp->pos.y = 16.09f;
		twp->pos.z = 192.81f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 272.95f;
		twp->pos.y = 12.09f;
		twp->pos.z = 198.11f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 292.24f;
		twp->pos.y = 16.09f;
		twp->pos.z = 258.31f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 303.46f;
		twp->pos.y = 16.09f;
		twp->pos.z = 268.26f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -333.44f;
		twp->pos.y = 87.09f;
		twp->pos.z = 120.53f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -334.02f;
		twp->pos.y = 87.09f;
		twp->pos.z = 159.52f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -339.12f;
		twp->pos.y = 87.09f;
		twp->pos.z = 161.11f;
		twp->ang.y = 0x7F77;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 119.18f;
		twp->pos.y = 74.06f;
		twp->pos.z = 13.68f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 120.53f;
		twp->pos.y = 74.06f;
		twp->pos.z = -85.6f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 134.33f;
		twp->pos.y = 69.06f;
		twp->pos.z = -86.54f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 134.33f;
		twp->pos.y = 74.06f;
		twp->pos.z = -86.54f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 150.22f;
		twp->pos.y = 69.06f;
		twp->pos.z = 8.92f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 153.12f;
		twp->pos.y = 72.06f;
		twp->pos.z = 9.68f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -426.22f;
		twp->pos.y = 66.06f;
		twp->pos.z = -0.59f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -438.55f;
		twp->pos.y = 66.06f;
		twp->pos.z = -27.28f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -443.88f;
		twp->pos.y = 66.06f;
		twp->pos.z = 5.3f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -505.1f;
		twp->pos.y = 64.06f;
		twp->pos.z = 96.66f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -564.52f;
		twp->pos.y = 66.06f;
		twp->pos.z = 78.22f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -567.8f;
		twp->pos.y = 66.06f;
		twp->pos.z = 82;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -577.52f;
		twp->pos.y = 70.06f;
		twp->pos.z = 145.06f;
		twp->ang.y = 0x8BA1;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 169.84f;
		twp->pos.y = 72.34f;
		twp->pos.z = -82.51f;
		twp->ang.y = 0xE66C;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 137.06f;
		twp->pos.y = 91.32f;
		twp->pos.z = 2.56f;
		twp->ang.y = 0xF066;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 166.62f;
		twp->pos.y = 81.32f;
		twp->pos.z = 12.07f;
		twp->ang.y = 0xF066;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 327;
		twp->pos.y = 93.32f;
		twp->pos.z = 279.54f;
		twp->ang.y = 0xF066;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 110.78f;
		twp->pos.y = 73.24f;
		twp->pos.z = 4.63f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 133.01f;
		twp->pos.y = 81.24f;
		twp->pos.z = -86.08f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 334.55f;
		twp->pos.y = 85.24f;
		twp->pos.z = 266.41f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 335.19f;
		twp->pos.y = 85.24f;
		twp->pos.z = 273.38f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 350.48f;
		twp->pos.y = 84.24f;
		twp->pos.z = 324.19f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 2;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -447.05f;
		twp->pos.y = 67.24f;
		twp->pos.z = 4.23f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -476.47f;
		twp->pos.y = 63.24f;
		twp->pos.z = -17.07f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 2;
		twp->scl.y = 2;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -512.26f;
		twp->pos.y = 67.24f;
		twp->pos.z = 101.4f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 5;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF18); // BUTTERFLY
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -560.2f;
		twp->pos.y = 67.24f;
		twp->pos.z = 94.82f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 4;
		twp->scl.y = 2;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 241.64f;
		twp->pos.y = 77.96f;
		twp->pos.z = 324;
		twp->ang.y = 0x5D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -574.82f;
		twp->pos.y = 60.06f;
		twp->pos.z = 95.91f;
		twp->ang.y = 0x30D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -574.67f;
		twp->pos.y = 60.06f;
		twp->pos.z = 117.43f;
		twp->ang.y = 0x40D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -570.56f;
		twp->pos.y = 60.06f;
		twp->pos.z = 140.93f;
		twp->ang.y = 0x50D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 250.71f;
		twp->pos.y = 77.96f;
		twp->pos.z = 358.07f;
		twp->ang.y = 0x85D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 293.58f;
		twp->pos.y = 80.26f;
		twp->pos.z = 354.14f;
		twp->ang.y = 0x8AD7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 316.52f;
		twp->pos.y = 80.26f;
		twp->pos.z = 345.95f;
		twp->ang.y = 0x8AD7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 345.86f;
		twp->pos.y = 80.26f;
		twp->pos.z = 331.87f;
		twp->ang.y = 0xA0D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -514.71f;
		twp->pos.y = 60.06f;
		twp->pos.z = 123.62f;
		twp->ang.y = 0xA0D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 364.51f;
		twp->pos.y = 80.26f;
		twp->pos.z = 304.91f;
		twp->ang.y = 0xABD7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 382.32f;
		twp->pos.y = 80.26f;
		twp->pos.z = 280.68f;
		twp->ang.y = 0xABD7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -212.91f;
		twp->pos.y = 19.36f;
		twp->pos.z = 233.91f;
		twp->ang.y = 0xB0D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -215.88f;
		twp->pos.y = 20.06f;
		twp->pos.z = 245.54f;
		twp->ang.y = 0xB0D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF15); // CHEER CHAO
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -504.34f;
		twp->pos.y = 60.06f;
		twp->pos.z = 107.87f;
		twp->ang.y = 0xB0D7;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 99.18f;
		twp->pos.y = 9.43f;
		twp->pos.z = 434.54f;
		twp->ang.y = 0x7A15;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 136.94f;
		twp->pos.y = 9.43f;
		twp->pos.z = 430.7f;
		twp->ang.y = 0x7A15;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -6.47f;
		twp->pos.y = 9.32f;
		twp->pos.z = 452.66f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 30.09f;
		twp->pos.y = 9.32f;
		twp->pos.z = 446.52f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -45.91f;
		twp->pos.y = 9.32f;
		twp->pos.z = 456.07f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -76.93f;
		twp->pos.y = 9.32f;
		twp->pos.z = 448.95f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -90.31f;
		twp->pos.y = 10.32f;
		twp->pos.z = 197.29f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 167.08f;
		twp->pos.y = 66.32f;
		twp->pos.z = 18.05f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 169.69f;
		twp->pos.y = 66.32f;
		twp->pos.z = -95.93f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 281.43f;
		twp->pos.y = 9.32f;
		twp->pos.z = 205.1f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -115.33f;
		twp->pos.y = 10.32f;
		twp->pos.z = 219.45f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -117.82f;
		twp->pos.y = 9.32f;
		twp->pos.z = 421.35f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -141.02f;
		twp->pos.y = 9.32f;
		twp->pos.z = 395.44f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -141.8f;
		twp->pos.y = 10.32f;
		twp->pos.z = 244.79f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -151.93f;
		twp->pos.y = 9.32f;
		twp->pos.z = 353.82f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -155.19f;
		twp->pos.y = 9.32f;
		twp->pos.z = 304.82f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -156.55f;
		twp->pos.y = 9.32f;
		twp->pos.z = 270.97f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -355.08f;
		twp->pos.y = 81.32f;
		twp->pos.z = 162.38f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -357.57f;
		twp->pos.y = 81.32f;
		twp->pos.z = 119.57f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 56.58f;
		twp->pos.y = 6.06f;
		twp->pos.z = 519.36f;
		twp->ang.y = 0x4066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 250.07f;
		twp->pos.y = 6.06f;
		twp->pos.z = 491.5f;
		twp->ang.y = 0x4066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 78.39f;
		twp->pos.y = 10.06f;
		twp->pos.z = 86;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -32.34f;
		twp->pos.y = 10.06f;
		twp->pos.z = 53.35f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 251.87f;
		twp->pos.y = 22.06f;
		twp->pos.z = 369.26f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF13); // ISLET
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -210.18f;
		twp->pos.y = 5.06f;
		twp->pos.z = 241.57f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -433.52f;
		twp->pos.y = 60.06f;
		twp->pos.z = -25.01f;
		twp->ang.y = 0xC3B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -502.93f;
		twp->pos.y = 60.06f;
		twp->pos.z = 100.83f;
		twp->ang.y = 0x4000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -506.86f;
		twp->pos.y = 60.06f;
		twp->pos.z = 98.34f;
		twp->ang.y = 0x4000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -427.08f;
		twp->pos.y = 60.06f;
		twp->pos.z = -3.42f;
		twp->ang.y = 0x4BA1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -437.51f;
		twp->pos.y = 60.06f;
		twp->pos.z = -28.54f;
		twp->ang.y = 0x4BA1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -439.89f;
		twp->pos.y = 60.06f;
		twp->pos.z = -24.73f;
		twp->ang.y = 0x6BA1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -422.41f;
		twp->pos.y = 60.06f;
		twp->pos.z = -0.21f;
		twp->ang.y = 0x8BA1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -323.11f;
		twp->pos.y = 82.63f;
		twp->pos.z = 127.87f;
		twp->ang.y = 0xB4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -330.92f;
		twp->pos.y = 82.63f;
		twp->pos.z = 155.05f;
		twp->ang.y = 0xB4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -561.97f;
		twp->pos.y = 60.06f;
		twp->pos.z = 93.02f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -562.37f;
		twp->pos.y = 60.06f;
		twp->pos.z = 100.22f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -568.97f;
		twp->pos.y = 60.06f;
		twp->pos.z = 94.02f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -365.66f; // -265.66f in the original, probably a mistake
		twp->pos.y = 82.63f;
		twp->pos.z = 123.59f; // 223.59f in the original, probably a mistake
		twp->ang.y = 0xC4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -321.1f;
		twp->pos.y = 82.63f;
		twp->pos.z = 125.65f;
		twp->ang.y = 0xC4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -329.94f;
		twp->pos.y = 83.03f;
		twp->pos.z = 157.57f;
		twp->ang.y = 0xE4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -501.99f;
		twp->pos.y = 60.06f;
		twp->pos.z = 104.69f;
		twp->ang.y = 0xF04C;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -503.51f;
		twp->pos.y = 60.06f;
		twp->pos.z = 94.5f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -507.48f;
		twp->pos.y = 60.06f;
		twp->pos.z = 97.55f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -332.9f;
		twp->pos.y = 83.03f;
		twp->pos.z = 155.52f;
		twp->ang.y = 0xF4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 337.91f;
		twp->pos.y = 80.44f;
		twp->pos.z = 265.07f;
		twp->ang.x = 0x300;
		twp->ang.y = 0x9C3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 337.07f;
		twp->pos.y = 80.44f;
		twp->pos.z = 267.95f;
		twp->ang.x = 0x300;
		twp->ang.y = 0xAC3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF7); // PURPLE FLOWER

	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 334.64f;
		twp->pos.y = 80.44f;
		twp->pos.z = 267.68f;
		twp->ang.x = 0x300;
		twp->ang.y = 0xEC3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF11); // SCOOP
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -555.17f;
		twp->pos.y = 60.06f;
		twp->pos.z = 79.23f;
		twp->ang.y = 0x4F1E;
		twp->ang.z = 0xC000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF11); // SCOOP
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -557.63f;
		twp->pos.y = 60.06f;
		twp->pos.z = 104.18f;
		twp->ang.y = 0x4F1E;
		twp->ang.z = 0xC000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF11); // SCOOP
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -561.71f;
		twp->pos.y = 60.68f;
		twp->pos.z = 126.16f;
		twp->ang.x = 0x4000;
		twp->ang.y = 0x600B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF9); // WATERING CAN
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -554.03f;
		twp->pos.y = 60.06f;
		twp->pos.z = 97.63f;
		twp->ang.y = 0x1F1E;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF9); // WATERING CAN
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -565.7f;
		twp->pos.y = 60.06f;
		twp->pos.z = 109.13f;
		twp->ang.y = 0x8E0B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 325.75f;
		twp->pos.y = 80.44f;
		twp->pos.z = 284.61f;
		twp->ang.y = 0xC3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 357.7f;
		twp->pos.y = 80.24f;
		twp->pos.z = 312.08f;
		twp->ang.y = 0xC3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -453.93f;
		twp->pos.y = 60.06f;
		twp->pos.z = 0.75f;
		twp->ang.y = 0xC3B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -461.8f;
		twp->pos.y = 60.06f;
		twp->pos.z = -15.1f;
		twp->ang.y = 0xC3B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -474.18f;
		twp->pos.y = 60.06f;
		twp->pos.z = -17.84f;
		twp->ang.y = 0xC3B;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -324.56f;
		twp->pos.y = 82.98f;
		twp->pos.z = 124.2f;
		twp->ang.y = 0xDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -336.82f;
		twp->pos.y = 81.98f;
		twp->pos.z = 126.13f;
		twp->ang.y = 0xDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -345.83f;
		twp->pos.y = 81.98f;
		twp->pos.z = 152.45f;
		twp->ang.y = 0xDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -348.81f;
		twp->pos.y = 81.98f;
		twp->pos.z = 156.59f;
		twp->ang.y = 0xDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 119.41f;
		twp->pos.y = 66.52f;
		twp->pos.z = -87.69f;
		twp->ang.y = 0x159B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 301.31f;
		twp->pos.y = 80.24f;
		twp->pos.z = 346.04f;
		twp->ang.y = 0x1C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 352.41f;
		twp->pos.y = 80.24f;
		twp->pos.z = 322.5f;
		twp->ang.y = 0x1C3B;
		twp->ang.z = 0x1000;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -540.12f;
		twp->pos.y = 60.06f;
		twp->pos.z = 61.46f;
		twp->ang.y = 0x1F1E;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -344.42f;
		twp->pos.y = 82.03f;
		twp->pos.z = 154.74f;
		twp->ang.y = 0x34B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 349.32f;
		twp->pos.y = 80.24f;
		twp->pos.z = 320.12f;
		twp->ang.y = 0x4C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 356.18f;
		twp->pos.y = 80.24f;
		twp->pos.z = 314.67f;
		twp->ang.y = 0x4C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 304.46f;
		twp->pos.y = 80.24f;
		twp->pos.z = 347.8f;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -443.49f;
		twp->pos.y = 60.06f;
		twp->pos.z = 0.98f;
		twp->ang.y = 0x6BA1;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 323.16f;
		twp->pos.y = 80.24f;
		twp->pos.z = 343.05f;
		twp->ang.y = 0x6C3B;
		twp->ang.z = 0x1000;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 347.63f;
		twp->pos.y = 80.24f;
		twp->pos.z = 323.49f;
		twp->ang.y = 0x6C3B;
		twp->ang.z = 0x1000;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 152.16f;
		twp->pos.y = 65.33f;
		twp->pos.z = 7.23f;
		twp->ang.y = 0x86A8;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 293.98f;
		twp->pos.y = 80.44f;
		twp->pos.z = 306.97f;
		twp->ang.y = 0x8E3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -452.46f;
		twp->pos.y = 60.06f;
		twp->pos.z = 1.3f;
		twp->ang.y = 0x9BA1;
		twp->ang.z = 0xF000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -469.66f;
		twp->pos.y = 60.06f;
		twp->pos.z = -14.86f;
		twp->ang.y = 0x9BA1;
		twp->ang.z = 0xF000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -342.54f;
		twp->pos.y = 81.98f;
		twp->pos.z = 124.19f;
		twp->ang.y = 0x9DFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 149.52f;
		twp->pos.y = 65.33f;
		twp->pos.z = 8.97f;
		twp->ang.y = 0xABA8;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -509.37f;
		twp->pos.y = 60.06f;
		twp->pos.z = 108.93f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -512.17f;
		twp->pos.y = 60.06f;
		twp->pos.z = 99.33f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -516.63f;
		twp->pos.y = 60.06f;
		twp->pos.z = 133.05f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -561.23f;
		twp->pos.y = 60.06f;
		twp->pos.z = 140.84f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -563.03f;
		twp->pos.y = 60.06f;
		twp->pos.z = 133.25f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -566.23f;
		twp->pos.y = 60.06f;
		twp->pos.z = 150.84f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -569.42f;
		twp->pos.y = 60.06f;
		twp->pos.z = 133.44f;
		twp->ang.y = 0xC066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 119.37f;
		twp->pos.y = 64.33f;
		twp->pos.z = 7.11f;
		twp->ang.y = 0xC6A8;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 154.89f;
		twp->pos.y = 65.33f;
		twp->pos.z = 8.88f;
		twp->ang.y = 0xC6A8;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -332.41f;
		twp->pos.y = 82.98f;
		twp->pos.z = 125.73f;
		twp->ang.y = 0xCDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -333.61f;
		twp->pos.y = 81.98f;
		twp->pos.z = 123.12f;
		twp->ang.y = 0xCDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 294.49f;
		twp->pos.y = 80.44f;
		twp->pos.z = 304.36f;
		twp->ang.y = 0xCE3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 318.76f;
		twp->pos.y = 80.44f;
		twp->pos.z = 289.63f;
		twp->ang.y = 0xCE3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -343.15f;
		twp->pos.y = 82.03f;
		twp->pos.z = 152.94f;
		twp->ang.y = 0xD4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -322.44f;
		twp->pos.y = 83.03f;
		twp->pos.z = 121.98f;
		twp->ang.y = 0xE4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -331.28f;
		twp->pos.y = 82.03f;
		twp->pos.z = 123.9f;
		twp->ang.y = 0xE4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -337.96f;
		twp->pos.y = 82.03f;
		twp->pos.z = 153.24f;
		twp->ang.y = 0xE4B5;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -338.81f;
		twp->pos.y = 81.98f;
		twp->pos.z = 121.73f;
		twp->ang.y = 0xEDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -342.9f;
		twp->pos.y = 81.98f;
		twp->pos.z = 121.21f;
		twp->ang.y = 0xEDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -353.78f;
		twp->pos.y = 81.98f;
		twp->pos.z = 155.43f;
		twp->ang.y = 0xEDFB;
		twp->ang.z = 0x15F;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -505.46f;
		twp->pos.y = 60.06f;
		twp->pos.z = 116.96f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -508.43f;
		twp->pos.y = 60.06f;
		twp->pos.z = 152.34f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -508.94f;
		twp->pos.y = 60.06f;
		twp->pos.z = 91.74f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -510.92f;
		twp->pos.y = 60.06f;
		twp->pos.z = 129.13f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -512.15f;
		twp->pos.y = 60.06f;
		twp->pos.z = 94.33f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -526.96f;
		twp->pos.y = 60.06f;
		twp->pos.z = 164.17f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -542.02f;
		twp->pos.y = 60.06f;
		twp->pos.z = 159.34f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -552.06f;
		twp->pos.y = 60.06f;
		twp->pos.z = 74.13f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -556.91f;
		twp->pos.y = 60.06f;
		twp->pos.z = 85.5f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -563.99f;
		twp->pos.y = 60.06f;
		twp->pos.z = 72.83f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -569.41f;
		twp->pos.y = 60.06f;
		twp->pos.z = 123.64f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -570.93f;
		twp->pos.y = 60.06f;
		twp->pos.z = 129.86f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 324.94f;
		twp->pos.y = 80.44f;
		twp->pos.z = 281.09f;
		twp->ang.x = 0x300;
		twp->ang.y = 0xAC3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 328.46f;
		twp->pos.y = 80.44f;
		twp->pos.z = 280.29f;
		twp->ang.x = 0x300;
		twp->ang.y = 0xCC3B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 121.59f;
		twp->pos.y = 66.52f;
		twp->pos.z = -88.68f;
		twp->ang.x = 0x1000;
		twp->ang.y = 0x359B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 132.53f;
		twp->pos.y = 64.33f;
		twp->pos.z = -84.62f;
		twp->ang.x = 0x1000;
		twp->ang.y = 0x44A8;
		twp->ang.z = 0x1000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 133.47f;
		twp->pos.y = 67.33f;
		twp->pos.z = -89.7f;
		twp->ang.x = 0x1000;
		twp->ang.y = 0x54A8;
		twp->ang.z = 0x1000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 129.67f;
		twp->pos.y = 64.33f;
		twp->pos.z = -83.74f;
		twp->ang.x = 0x1000;
		twp->ang.y = 0x64A8;
		twp->ang.z = 0x1000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 138.2f;
		twp->pos.y = 64.33f;
		twp->pos.z = -84.16f;
		twp->ang.x = 0x1000;
		twp->ang.y = 0x64A8;
		twp->ang.z = 0x1000;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 137.65f;
		twp->pos.y = 64.52f;
		twp->pos.z = 7.7f;
		twp->ang.x = 0xF000;
		twp->ang.y = 0x159B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 141.72f;
		twp->pos.y = 67.52f;
		twp->pos.z = 12.16f;
		twp->ang.x = 0xF000;
		twp->ang.y = 0x159B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 123.09f;
		twp->pos.y = 64.52f;
		twp->pos.z = 8.18f;
		twp->ang.x = 0xF000;
		twp->ang.y = 0x459B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 146.45f;
		twp->pos.y = 64.52f;
		twp->pos.z = 7.6f;
		twp->ang.x = 0xF000;
		twp->ang.y = 0x459B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 114.38f;
		twp->pos.y = 66.52f;
		twp->pos.z = 11.91f;
		twp->ang.x = 0xF000;
		twp->ang.y = 0x659B;
		twp->scl.x = 4;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 313.68f;
		twp->pos.y = 80.24f;
		twp->pos.z = 345.49f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 353.74f;
		twp->pos.y = 80.24f;
		twp->pos.z = 317.12f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF6); // WHITE FLOWER
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 366.88f;
		twp->pos.y = 80.24f;
		twp->pos.z = 300.5f;
		twp->ang.x = 0xF300;
		twp->ang.y = 0x5C3B;
		twp->scl.x = 3;
		twp->scl.y = 3;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF2); // ZONE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 312.64f;
		twp->pos.y = 80.37f;
		twp->pos.z = 151.66f;
		twp->scl.x = 30;
		twp->scl.z = 200;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF2); // ZONE
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -542.76f;
		twp->pos.y = 60;
		twp->pos.z = 97.08f;
		twp->scl.x = 50;
		twp->scl.z = 0;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -4.34f;
		twp->pos.y = 65.34f;
		twp->pos.z = 20.65f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -5.28f;
		twp->pos.y = 65.34f;
		twp->pos.z = -97.9f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 29.11f;
		twp->pos.y = 65.34f;
		twp->pos.z = -98.31f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 35.14f;
		twp->pos.y = 65.34f;
		twp->pos.z = 20.52f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 64.83f;
		twp->pos.y = 65.34f;
		twp->pos.z = -97.6f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 68.51f;
		twp->pos.y = 65.34f;
		twp->pos.z = 20.87f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -40.98f;
		twp->pos.y = 65.34f;
		twp->pos.z = 20.23f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = -41.89f;
		twp->pos.y = 65.34f;
		twp->pos.z = -97.77f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 104.12f;
		twp->pos.y = 65.34f;
		twp->pos.z = -99.01f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 104.14f;
		twp->pos.y = 65.34f;
		twp->pos.z = 19.99f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 135.93f;
		twp->pos.y = 65.34f;
		twp->pos.z = 19.46f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 137.47f;
		twp->pos.y = 65.34f;
		twp->pos.z = -97.52f;
		twp->ang.y = 0xC331;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 64.22f;
		twp->pos.y = 9.32f;
		twp->pos.z = 442.37f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 172.18f;
		twp->pos.y = 9.32f;
		twp->pos.z = 422.01f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 213.56f;
		twp->pos.y = 9.32f;
		twp->pos.z = 414.33f;
		twp->ang.y = 0xF066;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF5); // FLAG
	tp->ocp = &setdata_race;
if (tp)
{
	twp = tp->twp;
	twp->pos.x = 296.23f;
	twp->pos.y = 9.32f;
	twp->pos.z = 257.22f;
	twp->ang.y = 0xF066;
}
	tp = CreateElementalTask((LoadObj)2, 3, OF3); // PALM
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 240.82f;
		twp->pos.y = -0.58f;
		twp->pos.z = 195.57f;
		twp->ang.y = 0xF581;
	}
	tp = CreateElementalTask((LoadObj)2, 3, OF3); // PALM
	tp->ocp = &setdata_race;
	if (tp)
	{
		twp = tp->twp;
		twp->pos.x = 263.54f;
		twp->pos.y = -0.58f;
		twp->pos.z = 283.33f;
		twp->ang.y = 0xF581;
	}
}