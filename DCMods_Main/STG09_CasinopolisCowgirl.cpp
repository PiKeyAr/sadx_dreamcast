#include "stdafx.h"

NJS_OBJECT* CowgirlModel = nullptr;
NJS_MODEL_SADX* CowgirlGlass = nullptr;
static NJS_VECTOR CowGirlDynColPos;
static int CowgirlDelay = 0;
NJS_VECTOR Cowgirl1 = { 457.6972f, 45.06788f, 390 };
NJS_VECTOR Cowgirl2 = { 340.3949f, 51.20071f, 480 };
DataPointer(NJS_ACTION, action_cas_obj_cow_girl, 0x01E5C414);
DataPointer(NJS_OBJECT, object_cas_obj_cow_girl_obj19, 0x01E5340C); // Cowgirl glass
DataArray(CCL_INFO, object_c_info_46, 0x01E763B8, 3); // NeonK collision

void Cowgirl_AddDynCol(task* tsk, NJS_OBJECT* object)
{
	taskwk* original = tsk->twp;
	NJS_OBJECT* col = GetMobileLandObject();

	col->scl[0] = 1;
	col->scl[1] = 1;
	col->scl[2] = 1;

	col->basicdxmodel = object->basicdxmodel;

	tsk->twp->counter.ptr = col;

	RegisterCollisionEntry((SurfaceFlags_DynamicCollision | SurfaceFlags_Solid), tsk, col);
}

void Cowgirl_MoveDynCol(NJS_OBJECT* dyncol, Float frame)
{
	dyncol->pos[0] = CowGirlDynColPos.x;
	dyncol->pos[1] = CowGirlDynColPos.y;
	dyncol->pos[2] = CowGirlDynColPos.z;

	// Approximating the rotation

	if (frame > 15.0f) {
		frame = fabs(frame - 30.0f);
	}

	dyncol->ang[2] = static_cast<Angle>(55.0f * frame);
	dyncol->ang[1] = 0xC00;
}

void __cdecl CowGirl_DisplayPart(NJS_MODEL_SADX* model, LATE a2, int a3)
{
	DrawModelMS(model, a2);
	// If the model is the glass, retrieve its position
	if (model == CowgirlGlass)
	{
		ProjectToWorldSpace();
		njGetTranslation((NJS_MATRIX_PTR)&WorldMatrix, &CowGirlDynColPos);
	}
}

void __cdecl Cowgirl_Display(task* tp)
{
	taskwk* twp; // esi@1
	twp = tp->twp;
	if (dsCheckViewV(&twp->pos, 280.0f))
	{
		njSetTexture(&texlist_obj_casino9);
		njPushMatrixEx();
		njTranslateEx(&twp->pos);
		njRotateEx((Angle*)&twp->ang, 0);
		late_z_ofs___ = 4000.0f;
		DrawAction(&action_cas_obj_cow_girl, twp->scl.x, LATE_MAT, 0.0f, CowGirl_DisplayPart);
		late_z_ofs___ = 0;
		njPopMatrixEx();
	}
}

void Cowgirl_Main(task* tsk)
{
	if (!CheckRangeWithR(tsk, 640010.0f))
	{
		taskwk* data = tsk->twp;
		taskwk* entity = nullptr;

		data->scl.x += 0.1f;

		if (data->scl.x > 29.0f)
			data->scl.x = 0;

		// CowGirl sound
		if (data->mode == 0)
		{
			if (entity = CCL_IsHitPlayer(data), entity&& entity->flag& Status_Attack)
			{
				dsPlay_oneshot(SE_CA_H, 0, 0, 0);
				data->mode = 1;
			}
		}
		else
		{
			if (++data->btimer >= 100)
			{
				data->btimer = 0;
				data->mode = 0;
			}
		}

		// Detect if the player is on the object's dynamic collision
		NJS_OBJECT* pointer = (NJS_OBJECT*)data->counter.ptr;
		for (int i = 0; i < 4; ++i)
		{
			playerwk* co2 = playerpwp[i];

			if (co2 && co2->htp == tsk)
			{
				// Move the player by the difference between the old position and the new position.

				NJS_VECTOR pos = CowGirlDynColPos;
				pos.x -= pointer->pos[0];
				pos.y -= pointer->pos[1];
				pos.z -= pointer->pos[2];

				njAddVector(&playertwp[i]->pos, &pos);
			}
		}

		Cowgirl_MoveDynCol(pointer, data->scl.x);
		tsk->disp(tsk);
		EntryColliList(data);
	}
}

void Cowgirl_Delete(task* tsk)
{
	WithdrawCollisionEntry(tsk, (NJS_OBJECT*)tsk->twp->counter.ptr);
}

void Cowgirl_Load(task* tsk)
{
	// Hardcoded position and rotation to match SA1 JP
	tsk->twp->pos = { 311.62f, 0.0f, 338.93f };
	tsk->twp->ang = { 0, 0x1E00, 0 };
	CCL_Init(tsk, object_c_info_46, 3, 4u);
	tsk->disp = (void(__cdecl*)(task*))Cowgirl_Display;
	tsk->exec = (void(__cdecl*)(task*))Cowgirl_Main;
	tsk->dest = (void(__cdecl*)(task*))Cowgirl_Delete;
	Cowgirl_AddDynCol(tsk, &object_cas_obj_cow_girl_obj19);
}

void AddCowgirlUVAnimations(NJS_OBJECT* obj)
{
	NJS_MODEL_SADX* mdl = obj->basicdxmodel;
	if (mdl)
	{
		for (int u = 0; u < mdl->nbMeshset; u++)
		{
			int matid = mdl->meshsets[u].type_matId & 0x3FFF;
			int texid = mdl->mats[matid].attr_texId;
			switch (texid)
			{
			case 191:
			case 192:
			case 193:
			case 194:
			case 195:
				AddUVAnimation_Permanent(9, 0, &mdl->meshsets[u], 16, 100, 0);
				break;
			case 133:
				AddUVAnimation_Permanent(9, 0, &mdl->meshsets[u], 16, 65, 0);
				break;
			default:
				break;
			}
		}
	}
	if (obj->child)
		AddCowgirlUVAnimations(obj->child);
	if (obj->sibling)
		AddCowgirlUVAnimations(obj->sibling);
}

void Cowgirl_Init()
{
	object_cas_obj_cow_girl_cow_girl = *LoadModel("system\\data\\stg09_casino\\common\\models\\ogino_model\\cas_obj_cow_girl.nja.sa1mdl");
	CowgirlGlass = object_cas_obj_cow_girl_cow_girl.child->sibling->sibling->sibling->sibling->child->basicdxmodel;
	AddCowgirlUVAnimations(&object_cas_obj_cow_girl_cow_girl);
	for (int i = 0; i < 3; i++)
	{
		object_c_info_46[i].b = object_c_info_46[i].b * 6;
		object_c_info_46[i].center.z -= 14;
	}
	WriteJump(ONeonk, Cowgirl_Load);
}