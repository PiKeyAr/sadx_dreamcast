#include "stdafx.h"

static bool ModelsLoaded_B_CHAOS6;

NJS_OBJECT *Chaos6Main_OpaqueOnly = nullptr;

// Depth hack for skybox (main)
void DrawChaos6SkyboxMain(NJS_OBJECT* a1)
{
	late_z_ofs___ = -30000.0f;
	late_DrawObjectClip(a1, LATE_LIG, 3.0f);
	late_z_ofs___ = 0;
}

// Depth hack for skybox (bottom)
void DrawChaos6SkyboxBottom(NJS_OBJECT* a1)
{
	late_z_ofs___ = -32000.0f;
	late_DrawObjectClip(a1, LATE_LIG, 3.0f);
	late_z_ofs___ = 0;
}

// Draws the main Chaos 6 model with depth hacks
void Chaos6Action(NJS_ACTION *a1, float frameNumber)
{
	if (a1->object == &object_c6_kihon_caos6_caos6)
	{
		if (Chaos6Main_OpaqueOnly)
			dsDrawMotion(Chaos6Main_OpaqueOnly, a1->motion, frameNumber);
	}
	CHAOS_Action(a1, frameNumber);
}

// Fixes stuck global material after drawing the freezer effect
void late_DrawSprite3D_TheyForgotToClamp(NJS_SPRITE *sp, Int n, NJD_SPRITE attr, LATE zfunc_type)
{
	late_DrawSprite3D(sp, n, attr, zfunc_type);
	ResetMaterial();
}

// Function to hide all model hierarchy with transparency in the first material
void HideTransparentParts(NJS_OBJECT *obj)
{
	if (obj->basicdxmodel)
		if (obj->basicdxmodel->mats[0].attrflags & NJD_FLAG_USE_ALPHA)
			obj->evalflags |= NJD_EVAL_HIDE;
	if (obj->child)
		HideTransparentParts(obj->child);
	if (obj->sibling)
		HideTransparentParts(obj->sibling);
}

// Function to hide all model hierarchy without transparency in the first material
void HideOpaqueParts(NJS_OBJECT* obj)
{
	if (obj->basicdxmodel)
		if (!(obj->basicdxmodel->mats[0].attrflags & NJD_FLAG_USE_ALPHA))
			obj->evalflags |= NJD_EVAL_HIDE;
	if (obj->child)
		HideOpaqueParts(obj->child);
	if (obj->sibling)
		HideOpaqueParts(obj->sibling);
}

void B_CHAOS6_Init()
{
	WriteData<1>((char*)0x00556E40, 0xC3u); // Disable SetClip_Chaos6S
	WriteData<1>((char*)0x00556D60, 0xC3u); // Disable SetClip_Chaos6K
	ReplaceSET("SET1800B");
	ReplaceSET("SET1800S");
	ReplaceSET("SET1801K");
	ReplacePVM("LM_CHAOS6");
	ReplacePVM("LM_CHAOS6_2");
	ReplacePVM("CHAOS6");
	ReplacePVM("CHAOS6_BG");
	ReplacePVM("CHAOS6_EFFECT");
	ReplacePVM("CHAOS6_EGGMAN");
	ReplacePVM("CHAOS6_EISEI");
	ReplacePVM("CHAOS6_OBJECT");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Chaos06[0][i].f32EndZ = 12000.0f;
		pFogTable_Chaos06[1][i].f32EndZ = 12000.0f;
		pScale_Chaos06[0][i].x = pScale_Chaos06[0][i].y = pScale_Chaos06[0][i].z = 1.0f;
		pScale_Chaos06[1][i].x = pScale_Chaos06[1][i].y = pScale_Chaos06[1][i].z = 1.0f;
		pClipMap_Chaos06[0][i].f32Far = -18000.0f;
		pClipMap_Chaos06[1][i].f32Far = -18000.0f;
	}
}

void B_CHAOS6_Load()
{
	LevelLoader(LevelAndActIDs_Chaos6Sonic, "SYSTEM\\data\\boss_chaos6\\landtable1800.c.sa1lvl", &texlist_lm_chaos6);
	LevelLoader(LevelAndActIDs_Chaos6Knuckles, "SYSTEM\\data\\boss_chaos6\\landtable1801.c.sa1lvl", &texlist_lm_chaos6_2);
	if (!ModelsLoaded_B_CHAOS6)
	{
		// Main model stuff
		Chaos6Main_OpaqueOnly = CloneObject(&object_c6_kihon_caos6_caos6);
		HideOpaqueParts(&object_c6_kihon_caos6_caos6);
		HideTransparentParts(Chaos6Main_OpaqueOnly);
		WriteCall((void*)0x00558FFC, Chaos6Action); // Chaos 6 main model rendering
		WriteCall((void*)0x0055BCF4, late_DrawSprite3D_TheyForgotToClamp); // Freezer sprite fix
		// Skybox fixes
		WriteCall((void*)0x00556F97, DrawChaos6SkyboxMain);
		WriteCall((void*)0x0055708B, DrawChaos6SkyboxBottom);
		WriteData((float*)0x00557064, 0.0f); // Fix skybox bottom position
		ModelsLoaded_B_CHAOS6 = true;
	}
}