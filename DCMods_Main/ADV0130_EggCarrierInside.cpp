#include "stdafx.h"
#include "ADV0130_EggCarrierInside.h"

static bool ModelsLoaded_ADV0130;

// NPC display hook
void RenderEggCarrier3NPC(NJS_ACTION* action, Float frame)
{
	if (action->object == E102_OBJECTS[0])
	{
		OffControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
		OnControl3D(NJD_CONTROL_3D_ENABLE_ALPHA);
		late_ActionEx(action, frame, LATE_WZ);
	}
	else 
		ds_ActionClipNoScale(action, frame);
}

// Green door barrier display code
void BlockDisplayer_r(task *tp)
{
	taskwk *v1; // esi
	Angle v2; // eax

	v1 = tp->twp;
	if (v1->flag & 0x200)
	{
		if (!loop_count)
		{
			SetObjectTexture();
			njPushMatrix(0);
			njTranslateV(0, &v1->pos);
			v2 = v1->ang.y;
			if (v2)
			{
				njRotateY(0, (unsigned __int16)v2);
			}
			late_DrawObjectClip((NJS_OBJECT*)ADV01C_OBJECTS[21], LATE_LIG, 1.0f);
			njPopMatrix(1u);
		}
	}
}

// Green door barrier display code (separate sub)
void DisplayBlock_r(taskwk* twp)
{
	Angle v2; // eax

	if (!loop_count)
	{
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		v2 = twp->ang.y;
		if (v2)
		{
			njRotateY(0, (unsigned __int16)v2);
		}
		late_DrawObjectClip((NJS_OBJECT*)ADV01C_OBJECTS[21], LATE_LIG, 1.0f);
		njPopMatrix(1u);
	}
}

// Assembly to hook the usercall for green barrier display
static void __declspec(naked) DisplayBlock_asm()
{
	__asm
	{
		push esi // a2

		// Call your __cdecl function here:
		call DisplayBlock_r

		pop esi // a2
		retn
	}
}

// Callback to draw EGGMAN buttons
void RenderTaraiButtonLetter(int a1)
{
	njPushMatrix(0);
	njTranslate(0, 0.0, 2.0282631f, 0.0f);
	ADV01C_OBJECTS[7]->child->basicdxmodel->mats->attr_texId = a1;
	dsDrawModel(ADV01C_MODELS[27]);
	njPopMatrix(1u);
}

// EGGMAN buttons display code
void ButtonDisp_r(task* tp)
{
	taskwk* twp; // ebp
	Angle v2; // eax
	NJS_MODEL_SADX* v3; // ebx
	NJS_MATERIAL* v4; // eax

	twp = tp->twp;
	if (!loop_count)
	{
		njSetTexture(&texlist_ec_tarai);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		v2 = twp->ang.y;
		if (v2)
		{
			njRotateY(0, v2);
		}
		// Draw the letter
		late_SetFunc((void(__cdecl*)(void*))RenderTaraiButtonLetter, (void*)character_tex[button_name[twp->btimer]], -27000.0f, LATE_LIG);
		// Render the opaque part of the button
		dsDrawModel(ADV01C_OBJECTS[7]->basicdxmodel);
		// Create and draw the button (transparent)
		v3 = (NJS_MODEL_SADX*)late_alloca(44);
		v4 = (NJS_MATERIAL*)late_alloca(60);
		if (v3)
		{
			if (v4)
			{
				memcpy(v3, ADV01C_MODELS[28], 0x2Cu);
				memcpy(v4, ADV01C_MODELS[28]->mats, 0x3Cu);
				v3->mats = v4;
				v4->attr_texId = (twp->mode == 13 || twp->mode == 12) ? 13 : 2;
				late_DrawModelEx(v3, LATE_LIG);
			}
		}
		njPopMatrix(1u);
	}
}

// Hook to draw the Chao transporter effect with depth
void RenderChaoTransporterEffect_Fix(NJS_MODEL_SADX *a1, float scale)
{
	late_z_ofs___ = 2000.0f;
	DrawModelMesh(a1, LATE_LIG);
	late_z_ofs___ = 0;
}

// Hook to draw the water in the reservoir room with depth
void OReservoirFix(NJS_OBJECT *obj, float scale)
{
	late_z_ofs___ = 20000.0f;
	late_DrawObjectClip(obj, LATE_MAT, scale);
	late_z_ofs___ = 0.0f;
}

void ADV01C_Init()
{
	ReplaceSET("SETEC30S");
	ReplaceSET("SETEC31S");
	ReplaceSET("SETEC31B");
	ReplaceSET("SETEC32S");
	ReplaceSET("SETEC33S");
	ReplaceSET("SETEC34S");
	ReplaceSET("SETEC35S");
	ReplaceCAM("CAMEC30S");
	ReplaceCAM("CAMEC31S");
	ReplaceCAM("CAMEC32S");
	ReplaceCAM("CAMEC33S");
	ReplaceCAM("CAMEC34S");
	ReplaceCAM("CAMEC35S");
	ReplacePVM("ADV_EC30");
	ReplacePVM("ADV_EC31");
	ReplacePVM("ADV_EC32");
	ReplacePVM("ADV_EC33");
	ReplacePVM("ADV_EC34");
	ReplacePVM("ADV_EC35");
	ReplacePVM("ADV_EC36");
	ReplacePVM("OBJ_EC30");
	ReplacePVM("EC_ACTDOOR");
	//ReplacePVM("EC_ALIFE"); // Replaced in AL_Main because of the egg
	ReplacePVM("EC_EGGLIFT");
	ReplacePVM("EC_TARAI");
	ReplacePVM("PVME101FACTORY");
	ReplacePVM("EDV_K_HLIFT");
	ReplacePVM("EDV_S_STDOOR"); // Unused?
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Adv01C[0][i].f32EndZ = -12000;
		pFogTable_Adv01C[0][i].f32StartZ = -12000;
		pFogTable_Adv01C[1][i].u8Enable = 1;
		pFogTable_Adv01C[1][i].f32EndZ = 4000.0f;
		pFogTable_Adv01C[1][i].f32StartZ = 800.0f;
		pFogTable_Adv01C[1][i].Col = 0xFFA0A0A0;
		pFogTable_Adv01C[2][i].f32EndZ = -12000;
		pFogTable_Adv01C[2][i].f32StartZ = -12000;
		pFogTable_Adv01C[3][i].u8Enable = 1;
		pFogTable_Adv01C[3][i].f32EndZ = 1216.0f;
		pFogTable_Adv01C[3][i].f32StartZ = 139.0f;
		pFogTable_Adv01C[3][i].Col = 0xFF7F7F40;
		pFogTable_Adv01C[4][i].f32EndZ = -12000;
		pFogTable_Adv01C[4][i].f32StartZ = -12000;
		pFogTable_Adv01C[5][i].f32EndZ = -12000;
		pFogTable_Adv01C[5][i].f32StartZ = -12000;
	}
}

void ADV01C_Load()
{
	LevelLoader(LevelAndActIDs_EggCarrierInside1, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec30.c.sa1lvl", ADV01C_TEXLISTS[15], ___LANDTABLEEC3[0]);
	LevelLoader(LevelAndActIDs_EggCarrierInside2, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec31.c.sa1lvl", ADV01C_TEXLISTS[16], ___LANDTABLEEC3[1]);
	LevelLoader(LevelAndActIDs_EggCarrierInside3, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec32.c.sa1lvl", ADV01C_TEXLISTS[17], ___LANDTABLEEC3[2]);
	LevelLoader(LevelAndActIDs_EggCarrierInside4, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec33.c.sa1lvl", ADV01C_TEXLISTS[18], ___LANDTABLEEC3[3]);
	LevelLoader(LevelAndActIDs_EggCarrierInside5, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec34.c.sa1lvl", ADV01C_TEXLISTS[19], ___LANDTABLEEC3[4]);
	LevelLoader(LevelAndActIDs_EggCarrierInside6, "SYSTEM\\data\\adv01_eggcarrierc\\landtableec35.c.sa1lvl", ADV01C_TEXLISTS[20], ___LANDTABLEEC3[5]);
	if (!ModelsLoaded_ADV0130)
	{
		// Gamma's chest fix
		WriteCall((void*)0x00525405, RenderEggCarrier3NPC);
		// Chao transporter fix
		WriteCall((void*)0x00526369, RenderChaoTransporterEffect_Fix); // Transporter effect fix
		WriteCall((void*)0x00525D39, OReservoirFix); // Fix water in Gamma's upgrade room
		// Door barrier fixes (Gamma's story)
		WriteJump((void*)0x0052B2E0, BlockDisplayer_r);
		WriteJump((void*)0x0052B250, DisplayBlock_asm);
		// Fix camera in Amy-Gamma prison cutscene
		WriteData((float*)0x006A4EBE, -134.0f); // X1
		WriteData((float*)0x006A4EB9, 15.0f); // Y1
		WriteData((float*)0x006A4EB4, 54.0f); // Z1
		WriteData((float*)0x006A4F41, -143.85f); // X2
		WriteData((float*)0x006A4F3C, 15.93f); // Y2
		WriteData((float*)0x006A4F37, 80.25f); // Z2
		// Fix camera in Gamma-Amy prison cutscene
		WriteData((float*)0x00678C48, -134.0f); // X1
		WriteData((float*)0x00678C43, 15.0f); // Y1
		WriteData((float*)0x00678C3E, 54.0f); // Z1
		WriteData((float*)0x00678CCB, -143.85f); // X2
		WriteData((float*)0x00678CC6, 15.93f); // Y2
		WriteData((float*)0x00678CC1, 80.25f); // Z2
		// Material fixes
		AddAlphaRejectMaterial(&ADV01C_OBJECTS[32]->basicdxmodel->mats[0]); // Monorail sign (inside)
		AddWhiteDiffuseMaterial(&ADV01C_MODELS[30]->mats[3]); // O HammerSW
		*ADV01C_OBJECTS[3] = *LoadModel("system\\data\\adv01_eggcarrierc\\mogobj\\tataki1_dai1mt.nja.sa1mdl"); // Platform for Hedgehog Hammer
		// Tarai fix
		*ADV01C_OBJECTS[7] = *LoadModel("system\\data\\adv01_eggcarrierc\\common\\models\\edv_f_botan.nja.sa1mdl"); // EGGMAN button (edited hierarchy)
		AddWhiteDiffuseMaterial(&ADV01C_OBJECTS[7]->basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&ADV01C_OBJECTS[7]->basicdxmodel->mats[1]);
		*ADV01C_MODELS[28] = *ADV01C_OBJECTS[7]->child->sibling->basicdxmodel; // Transparent frame around the button
		*ADV01C_OBJECTS[7]->child = *ADV01C_OBJECTS[7]->child;
		*ADV01C_MODELS[27] = *ADV01C_OBJECTS[7]->child->basicdxmodel;
		WriteJump((void*)0x0052BA70, ButtonDisp_r);
		// Other objects
		*ADV01C_ACTIONS[6]->object = *LoadModel("system\\data\\adv01_eggcarrierc\\common\\models\\n_eggtobira_n_hontai.nja.sa1mdl"); // Door
		*ADV01C_OBJECTS[23] = *LoadModel("system\\data\\adv01_eggcarrierc\\common\\models\\edv_k_hlift.nja.sa1mdl"); // OEggLift
		*ADV01C_ACTIONS[7]->object = *ADV01C_OBJECTS[23];
		ModelsLoaded_ADV0130 = true;
	}
}