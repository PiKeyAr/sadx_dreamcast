#include "stdafx.h"

// Another Chao Race Start Mark that isn't in the object list (appears at the start of courses other than Ruby)

NJS_OBJECT* object_cr4_gate_gate_b = nullptr;

void ChaoRaceStartMark_Display(task* tp)
{
	taskwk* twp = tp->twp;
	njSetTexture(&texlist_obj_al_race);
	njPushMatrix(0);
	DrawObjectClip(object_cr4_gate_gate_b, 1.0f);
	njPopMatrix(1u);
}

void ChaoRaceStartMark_Main(task* tp)
{
	ChaoRaceStartMark_Display(tp);
}

void ChaoRaceStartMark_Load(task* tp)
{
	tp->exec = ChaoRaceStartMark_Main;
	tp->disp = ChaoRaceStartMark_Display;
	tp->dest = FreeTask;
}

void ALR_StartMark_Load()
{
	object_cr4_gate_gate_b = LoadModel("system\\data\\chao\\stg_race1\\al_race\\common\\models\\cr4_gate_b.nja.sa1mdl");
}