#pragma once

#include <SADXModLoader.h>

TaskFunc(MannequinDisplayer, 0x00630690); // Burger shop statue display function
TaskFunc(Disp_0, 0x00593410); // Mission statue display function
DataArray(task*, MRLockInfo, 0x03C63284, 5); // HandyKey locks array

// Collision data pointers
DataArray(CCL_INFO, cinfo_mannequin_0, 0x02BBE5B8, 12); // Burger shop statue collision
DataArray(CCL_INFO, obj_c_info_0, 0x02BBF020, 2); // ChaoCard collision
DataArray(CCL_INFO, obj_c_info_1, 0x02BBF088, 3); // Fake egg collision
DataArray(CCL_INFO, obj_c_info_2, 0x02BBF3F0, 2); // OKartPass collision
DataArray(CCL_INFO, obj_c_info_3, 0x01101F10, 9); // Black egg collision
DataArray(CCL_INFO, ColliInfo_6, 0x0111C610, 2); // OHandyKey collision
DataArray(CCL_INFO, ColliInfo_9, 0x0111CA78, 2); // OKillSwitch collision
DataArray(CCL_INFO, ColliInfo_51, 0x017D0A88, 2); // OBoxSwitch collision
DataArray(CCL_INFO, ccinfo_8, 0x0111D420, 2); // OEggStand collision
DataArray(CCL_INFO, ccinfo_25, 0x01386AC8, 3); // Chaos 6 freezer collision
DataArray(CCL_INFO, base_c_info, 0x02BBF148, 1); // OKeyBlock (unsolved) collision
DataArray(CCL_INFO, key_c_info, 0x02BBF178, 2); // OKeyBlock collision
DataArray(CCL_INFO, put_c_info, 0x02BBF118, 1); // OKeyBlock (solved) collision
DataArray(CCL_INFO, alm_colli_info, 0x00885198, 3); // Animal collision in the gardens
DataArray(CCL_INFO, fruit_colli_info, 0x00885268, 6); // Chao fruit collision
DataArray(CCL_INFO, bomb1_info, 0x0096CA18, 4); // Kiki bomb collision
DataArray(CCL_INFO, cci_sscard, 0x02BBF4C8, 2); // Ice Key collision (Station Square)
DataArray(CCL_INFO, cci_sscard_0, 0x02BC0748, 2); // OCard collision

// List of njTranslate calls to hook
int njTranslateVCalls[] = {
	0x006361D8, // Eggs
	0x00532273, // OHandKey
	0x00526020, // OKurotama
	0x00535406, // OKillSwitch
	0x00536902, // OEggStand
	0x0059A8E3, // OBoxSwitch
	0x00635F82, // ChaoCard
	0x00636702, // OKeyblock
	0x006377E1, // KartPass
	0x0063BF24, // OCard
	0x00637E0A, // Ice Key
};