#include "stdafx.h"

static bool ModelsLoaded_B_CHAOS2;

DataArray(NJS_OBJECT*, column_tbl, 0x01121550, 16); // Chaos 2 columns pointer list

// Fixes awkward long walking in Knuckles' post-Chaos 2 cutscene
void Chaos2KnucklesWalkFix(task* tp, float x, float y, float z, float s, float a)
{
	EV_MovePoint2(tp, x, y, z, s * 4, a * 4);
}

// Draws Chaos 2 columns with depth
void FixChaos2Columns(NJS_OBJECT *a1, LATE a2, float a3)
{
	late_z_ofs___ = 8000.0f;
	late_DrawObjectClip(a1, LATE_MAT, a3);
	late_z_ofs___ = 0;
}

// Draws restaurant tableware with depth
void Chaos2TableTopFix(NJS_MODEL_SADX* model, float scale)
{
	late_z_ofs___ = -20000.0f;
	late_DrawModel(model, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

// Draws chandelier with depth
void ChandelierFix(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawObjectMesh(a1, LATE_WZ);
}

// Draws chandelier light with depth
void ChandLightFix(NJS_OBJECT* a1, LATE a2)
{
	SetMaterial(0.65f, 0.6f, 0.6f, 0.6f);
	late_z_ofs___ = -18000.0f;
	late_DrawObjectClip(a1, LATE_LIG, 1.0f);
	late_z_ofs___ = 0.0f;
}

void Chaos2Ball(NJS_OBJECT *object)
{
	late_DrawObjectMesh(object, LATE_MAT);
}

void Chaos2BallMS(NJS_OBJECT* object)
{
	late_DrawObjectClipMS(object, LATE_MAT, 1.0f);
}

void Chaos2_Transform(NJS_OBJECT *object)
{
	late_z_ofs___ = -17000.0f;
	late_DrawObjectMesh(object, LATE_MAT);
	late_z_ofs___ = 0;
}

void Chaos2Action(NJS_ACTION *a1, float frameNumber)
{
	late_ActionMesh(a1, frameNumber, LATE_MAT);
}

void B_CHAOS2_Init()
{
	ReplaceSET("SET1600S");
	ReplacePVM("CHAOS2");
	ReplacePVM("LM_CHAOS2");
	ReplacePVM("CHAOS2_BARRIER");
	ReplacePVM("CHAOS2_EFFECT");
	ReplacePVM("CHAOS2_OBJECT");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Chaos02[0][i].Col = 0xFF000000;
		pFogTable_Chaos02[0][i].f32StartZ = 700.0f;
		pFogTable_Chaos02[0][i].f32EndZ = 1700.0f;
		pFogTable_Chaos02[0][i].u8Enable = 0;
	}
	// Cutscene fix
	WriteCall((void*)0x00691E2E, Chaos2KnucklesWalkFix);
	WriteCall((void*)0x00691DDC, Chaos2KnucklesWalkFix);
}

void B_CHAOS2_Load()
{
	LevelLoader(LevelAndActIDs_Chaos2, "SYSTEM\\data\\boss_chaos2\\landtable1600.c.sa1lvl", &texlist_lm_chaos2);
	objLandTable1600 = *objLandTable[16][0];
	// Columns
	int cl = 0;
	for (int c = 0; c < objLandTable[16][0]->ssCount; c++)
	{
		if (objLandTable[16][0]->pLandEntry[c].slAttribute == 0)
			column_tbl[cl++] = objLandTable[16][0]->pLandEntry[c].pObject;
	}
	if (!ModelsLoaded_B_CHAOS2)
	{
		object_caos2obj_tenboutable_tenboutable = *LoadModel("system\\data\\boss_chaos2\\common\\objmodels\\caos2obj_tenboutable.nja.sa1mdl"); // Table
		object_caos2obj_tenboutable_tenboutable.basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_USE_ALPHA; // This doesn't use alpha anyway
		WriteCall((void*)0x0054E3FE, Chaos2TableTopFix);
		WriteCall((void*)0x0054DFCC, ChandelierFix);
		AddAlphaRejectMaterial(&object_chaos2obj_yuka_ref_yuka_ref.basicdxmodel->mats[0]); // Floor light
		object_chaos2obj_yuka_ref_yuka_ref.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT;
		WriteCall((void*)0x0054E13C, ChandLightFix);
		WriteCall((void*)0x0054AC30, FixChaos2Columns);
		WriteCall((void*)0x0054D991, Chaos2Ball); // Ball bouncing
		WriteCall((void*)0x0054C5E7, Chaos2_Transform); // Ball transforming
		WriteCall((void*)0x0054F496, Chaos2Ball); // Chaos transformation model
		WriteCall((void*)0x0054CFC9, Chaos2Ball); // Hand attack
		WriteCall((void*)0x0054DA8A, Chaos2Action); // Main model
		WriteCall((void*)0x0054D928, Chaos2Ball); // Puddle
		WriteCall((void*)0x0054F5AC, Chaos2BallMS); // Small puddles - disabled because it runs out of memory for late_alloca
		WriteCall((void*)0x0054F5DF, Chaos2Ball); // Small ball attack
		ModelsLoaded_B_CHAOS2 = true;
	}
}