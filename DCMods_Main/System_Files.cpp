#include "stdafx.h"

void ProcessMaterials_Object(NJS_OBJECT* object);

NJS_OBJECT* LoadModel(const char* ModelName)
{
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("Loading model: %s: ", helperFunctionsGlobal->GetReplaceablePath(ModelName));
	ModelInfo* info = new ModelInfo(helperFunctionsGlobal->GetReplaceablePath(ModelName));
	NJS_OBJECT* object = info->getmodel();
	ProcessMaterials_Object(object);
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("OK\n");
	return object;
}

LandTableInfo* LoadLandTable(ArchiveX* arc, const char* LandName)
{
	return arc->GetLandTable(LandName);
}

NJS_OBJECT* LoadModel(ArchiveX* arc, const char* ModelName)
{
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("Loading model(x) %s: ", ModelName);
	ModelInfo* info = arc->GetModel(ModelName);
	NJS_OBJECT* object = info->getmodel();
	if (object == nullptr)
		PrintDebug("ERROR");
	else
	{
		ProcessMaterials_Object(object);
		if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
			PrintDebug("OK\n");
	}
	return object;
}

NJS_MOTION* LoadAnimation(ArchiveX* arc, const char* AnimationName)
{
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("Loading animation(x): %s: ", helperFunctionsGlobal->GetReplaceablePath(AnimationName));
	AnimationFile* animfile = arc->GetAnimation(AnimationName);
	NJS_MOTION* motion = animfile->getmotion();
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("OK\n");
	return motion;
}

NJS_MOTION* LoadAnimation(const char* AnimationName)
{
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("Loading animation: %s: ", helperFunctionsGlobal->GetReplaceablePath(AnimationName));
	AnimationFile* animfile = new AnimationFile(helperFunctionsGlobal->GetReplaceablePath(AnimationName));
	NJS_MOTION* motion = animfile->getmotion();
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("OK\n");
	return motion;
}

// Replaces the specified filename in the system folder to load a different file.
void ReplaceGeneric(std::string src, std::string dest)
{
	std::string fullsrc = "system\\" + src;
	std::string fulldest = "system\\" + dest;
	helperFunctionsGlobal->ReplaceFile(fullsrc.c_str(), fulldest.c_str());
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
		PrintDebug("Replace file %s with file %s\n", fullsrc.c_str(), fulldest.c_str());
}

// Replaces SET files (argument example: 'SET0100S').
void ReplaceSET(std::string src)
{
	std::string fulldest;
	std::string fullsrc;
	switch (ModConfig::SETReplacement)
	{
	case SetFileConfig::International:
		fulldest = "system\\bin_1999\\" + src + ".BIN";
		break;
	case SetFileConfig::Japan:
		fulldest = "system\\bin_1998\\" + src + ".BIN";
		break;
	case SetFileConfig::Optimized:
	default:
		fulldest = "system\\" + src + "_DC.BIN";
		break;
	}
	fullsrc = "system\\" + src + ".BIN";
	helperFunctionsGlobal->ReplaceFile(fullsrc.c_str(), fulldest.c_str());
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
		PrintDebug("Replace SET file %s with file %s\n", fullsrc.c_str(), fulldest.c_str());
}

// Replaces CAM files (argument example: 'CAM0100S').
void ReplaceCAM(std::string src)
{
	std::string fulldest;
	std::string fullsrc;
	switch (ModConfig::SETReplacement)
	{
	case SetFileConfig::Japan:
		fulldest = "system\\bin_1998\\" + src + ".BIN";
		break;
	case SetFileConfig::Optimized:
	case SetFileConfig::International:
	default:
		fulldest = "system\\" + src + "_DC.BIN";
		break;
	}
	fullsrc = "system\\" + src + ".BIN";
	helperFunctionsGlobal->ReplaceFile(fullsrc.c_str(), fulldest.c_str());
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Files)
		PrintDebug("Replace CAM file %s with file %s\n", fullsrc.c_str(), fulldest.c_str());
}