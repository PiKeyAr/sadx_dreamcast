#pragma once

#include <SADXModLoader.h>

enum class VideoFadeoutModes
{
	FadeIn = 0,
	Play = 1,
	FadeOut = 2,
	FadeOutFinish = 3
};

// This shit is missing in the symbols

DataPointer(int, CurrentVideoID, 0x03C5FFEC);
DataPointer(int, PreviousVideoFrameCopy, 0x03C60018);
FunctionPointer(int, CheckReset, (), 0x0040EFE0);