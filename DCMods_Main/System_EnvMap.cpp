#include "stdafx.h"

DataArray(float, EnvMapMatrix, 0x38A5DD0, 16);

static float BackupEnv1;
static float BackupEnv2;
static float BackupEnv3;
static float BackupEnv4;

void ScaleEnvironmentMap(float value1, float value2, float value3, float value4)
{
	BackupEnv1 = EnvMapMatrix[0];
	BackupEnv2 = EnvMapMatrix[5];
	BackupEnv3 = EnvMapMatrix[12];
	BackupEnv4 = EnvMapMatrix[13];
	if (value1 != -1)
		EnvMapMatrix[0] = value1;
	if (value2 != -1)
		EnvMapMatrix[5] = value2;
	if (value3 != -1)
		EnvMapMatrix[12] = value3;
	if (value4 != -1)
		EnvMapMatrix[13] = value4;
}

void RestoreEnvironmentMap()
{
	EnvMapMatrix[0] = BackupEnv1;
	EnvMapMatrix[5] = BackupEnv2;
	EnvMapMatrix[12] = BackupEnv3;
	EnvMapMatrix[13] = BackupEnv4;
}