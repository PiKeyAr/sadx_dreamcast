#include "stdafx.h"

static OBJ_CONDITION setdata_LateDrawLand = {};
static std::vector<int> LateDrawLandList;
static _OBJ_LANDTABLE* LateDrawLand;

void DrawLandtableCallback_NoZWrite(NJS_MODEL_SADX* model)
{
	if (loop_count)
		return;
	njSetZUpdateMode(0);
	dsDrawModel(model);
	njSetZUpdateMode(1u);
}

void DrawLandtableCallback_ZWrite(NJS_MODEL_SADX* model)
{
	if (loop_count)
		return;
	dsDrawModel(model);
}

// void __usercall(NJS_OBJECT *a1@<edi>, COL *a2)
static const void* const land_DrawObjectPtr = (void*)0x43A570;
static inline void land_DrawObjectOriginal(NJS_OBJECT* a1, _OBJ_LANDENTRY* a2)
{
	__asm
	{
		push[a2]
		mov edi, [a1]
		call land_DrawObjectPtr
		add esp, 4
	}
}

// Landtable draw hook
// This uses the normally unused "Y width" and "Z width" COL item fields to manipulate depth.
// The Z width value is used in the following ways:
//  0: Ignore
// -1: Put the COL items on a list to render after the skybox
//  Value other than -1 or 0: used as depth for the callback function
void land_DrawObject_New(NJS_OBJECT* a1, _OBJ_LANDENTRY* a2)
{
	NJS_MODEL_SADX* v2; // esi
	Uint32 flags; // ecx

	// Call the original function if the level is disabled
	int levelid = -1;
	switch (ChaoStageNumber)
	{
	case CHAO_STG_RACE:
		levelid = LevelIDs_ChaoRace;
		break;
	case CHAO_STG_SS:
		levelid = LevelIDs_SSGarden;
		break;
	case CHAO_STG_EC:
		levelid = LevelIDs_ECGarden;
		break;
	case CHAO_STG_MR:
		levelid = LevelIDs_MRGarden;
		break;
	case -1:
	default:
		levelid = ssStageNumber;
		break;
	}

	if (!ModConfig::EnabledLevels[levelid])
	{
		land_DrawObjectOriginal(a1, a2);
		return;
	}

	// Call the original function if no additional depth data is found
	if (a2->yWidth == 0 && a2->zWidth == 0)
	{
		land_DrawObjectOriginal(a1, a2);
		return;
	}

	v2 = a1->basicdxmodel;
	if (!dsCheckViewV(&v2->center, v2->r))
		return;

	flags = a2->slAttribute;
	LATE queueFlags = LATE_WZ;

	// Draw with callback
	if (a2->zWidth != 0 && a2->zWidth != -1)
	{
		if (flags & SurfaceFlags_Waterfall) // Disable Z Write for the callback function
			late_SetFunc((void(__cdecl*)(void*))DrawLandtableCallback_NoZWrite, (void*)v2, a2->zWidth, queueFlags);
		else
			late_SetFunc((void(__cdecl*)(void*))DrawLandtableCallback_ZWrite, (void*)v2, a2->zWidth, queueFlags);
		return;
	}

	// Disable Z Write
	if (flags & SurfaceFlags_NoZWrite)
	{
		if (flags & SurfaceFlags_Waterfall)
			queueFlags = LATE_NO;
		else
			queueFlags = LATE_MAT; // Regular flag
	}

	// Alternative queue flags
	else if (flags & SurfaceFlags_Waterfall)
		queueFlags = LATE_LIG;

	// Set depth
	late_z_ofs___ = a2->yWidth;

	// Of course there are cutscenes using camera angles that break everything
	switch (current_event)
	{
	case 240: // Final story Tornado 2 flashback
		late_z_ofs___ = 0;
		break;
	default:
		break;
	}

	// Draw by Mesh
	if (flags & SurfaceFlags_DrawByMesh)
	{
		late_DrawModelClipMesh(v2, queueFlags, 1.0f);
		late_z_ofs___ = 0.0f;
		return;
	}

	// Draw regular transparent (early)
	else if (flags & SurfaceFlags_LowDepth)
	{
		ds_DrawModelClip(v2, 1.0f);
		late_z_ofs___ = 0.0f;
		return;
	}

	// Draw regular transparent (late)
	else
	{
		late_DrawModelClip(v2, queueFlags, 1.0f);
		late_z_ofs___ = 0.0f;
		return;
	}
}

// Stuff that draws after the skybox. This can be used in levels that make the skybox draw after level items (i.e. most levels).
void __cdecl LateDrawLand_Display(task* a1)
{
	NJS_MATRIX matrix;
	if (!LateDrawLand || LateDrawLand != pObjLandTable)
		return;
	if (!loop_count && !isTextureNG(LateDrawLand->pTexList))
	{
		for (int i : LateDrawLandList)
		{
			_OBJ_LANDENTRY colitem = LateDrawLand->pLandEntry[i];
			NJS_OBJECT* colmodel = colitem.pObject;
			njGetMatrix(matrix);
			njSetTexture(LateDrawLand->pTexList);
			njTranslateEx((NJS_VECTOR*)&colmodel->pos);
			njRotateXYZ(0, colmodel->ang[0], colmodel->ang[1], colmodel->ang[2]);
			land_DrawObject_New(colmodel, (_OBJ_LANDENTRY*)&colitem);
			njSetMatrix(0, matrix);
		}
	}
}

void LateDrawLand_Load(task* a1)
{
	a1->exec = (void(__cdecl*)(task*))LateDrawLand_Display;
	a1->disp = (void(__cdecl*)(task*))LateDrawLand_Display;
	a1->dest = (void(__cdecl*)(task*))CheckThingButThenDeleteObject;
}

void LoadLateDrawLand()
{
	task* obj;
	taskwk* ent;
	setdata_LateDrawLand.unionStatus.fRangeOut = 612800.0f;
	obj = CreateElementalTask((LoadObj)2, 3, LateDrawLand_Load);
	obj->ocp = &setdata_LateDrawLand;
	if (obj)
	{
		ent = obj->twp;
	}
}

void AddLateDrawLandtable(_OBJ_LANDTABLE* landtable)
{
	LateDrawLand = landtable;
	for (int j = 0; j < landtable->ssCount; j++)
	{
		if (landtable->pLandEntry[j].zWidth == -1)
		{
			if (landtable->pLandEntry[j].slAttribute & SurfaceFlags_Visible)
				landtable->pLandEntry[j].slAttribute &= ~SurfaceFlags_Visible;
			LateDrawLandList.push_back(j);
		}
	}
}

void RemoveLateDrawLandtable()
{
	LateDrawLandList.clear();
	LateDrawLand = NULL;
}