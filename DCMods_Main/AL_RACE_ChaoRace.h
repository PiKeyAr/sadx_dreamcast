#pragma once

#include <SADXModLoader.h>

extern int ChaoRaceNameCounter;
extern NJS_TEXLIST texlist_bg_al_race02_dc;

extern task* ChaoRaceMenuTask;
DataPointer(NJS_OBJECT, object_item_lball_lball, 0x036ADE9C);
DataPointer(task**, dialog, 0x03CE0574); // Pointer for DC Chao Race menus array, used for Start/Goal signs

FunctionPointer(void, AL_MaruKageDraw, (NJS_POINT3* pPos, Angle3* pAng, float scl, float scl_ratio, float alpha), 0x0073EF60); // Draws Chao shadow

void LoadObjects_Race();

void ALR_Cracker_Init();
void ALR_Skybox_Load();
void ALR_StartMark_Load();
void ALR_Waterfall_Load();

void ALR_Nameplate_Init();
void ALR_GoalSprite_Init();
void ALR_Skybox_Init();

void ALR_Nameplate_OnFrame();

void AL_ChaoRaceEntry_Init();

void ZoneFix_Init();