#include "stdafx.h"

void ChaoRaceWaterfall_Display(task* tp)
{
	taskwk* twp = tp->twp;
	njSetTexture(&texlist_obj_al_race);
	njPushMatrix(0);
	late_z_ofs___ = 2000.0f;
	late_DrawObjectClip((NJS_OBJECT*)twp->counter.ptr, LATE_MAT, 1.0f);
	njPopMatrix(1u);
	late_z_ofs___ = 0;
}

void ChaoRaceWaterfall_Main(task* tp)
{
	taskwk* twp = tp->twp;
	ChaoRaceWaterfall_Display(tp);
	dsPlay_timer_v(SE_CH_SPRING, (int)twp, 0, 1, 10, twp->pos.x, twp->pos.y, twp->pos.z);
}

void ChaoRaceWaterfall_Load(task* tp)
{	
	tp->exec = ChaoRaceWaterfall_Main;
	tp->disp = ChaoRaceWaterfall_Display;
	tp->dest = FreeTask;
}

void ALR_Waterfall_Load()
{
	object_cr4_nc_taki_b_nc_taki_b.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_CLAMP_V;
	object_cr4_nc_taki_a_nc_taki_a.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_CLAMP_V;
	AddUVAnimation_Permanent(LevelIDs_ChaoRace, 0, &object_cr4_nc_taki_a_nc_taki_a.basicdxmodel->meshsets[0], 0, 0, -6);
	AddUVAnimation_Permanent(LevelIDs_ChaoRace, 0, &object_cr4_nc_taki_b_nc_taki_b.basicdxmodel->meshsets[0], 0, 0, -6);
}