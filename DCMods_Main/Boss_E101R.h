#pragma once

#include <SADXModLoader.h>

DataPointer(int, nj_motion_callback_, 0x03D08844); // Old motion callback that is only used by the function njAction_Old

FunctionPointer(void, E101R_AfterImageParts, (int a1, int a2), 0x00570900); // Draws afterimage effect
FunctionPointer(void, DisplayFineSky_0, (taskwk* a1), 0x00568EC0);// E101R skybox display
FunctionPointer(void, njDrawTexture3DEx, (NJS_TEXTURE_VTX* a1, signed int count, int a3), 0x0077E940); // FVF stuff drawing function
FunctionPointer(void, nozzleListDisp, (task* a1), 0x0056F750); // Particle effect
FunctionPointer(void, control_disp, (task* a1), 0x004CC040); // Draws small balls