#pragma once

enum class DebugVerboseConfig
{
	Off = 0,
	Levels = 1,
	Models = 2,
	Files = 3
};

enum class DemosConfig
{
	Off = 0,
	DreamcastUS = 1,
	DreamcastJP = 2,
	Gamecube = 3,
	PC = 4
};

enum class TitleScreenConfig
{
	US_EU = 0,
	International = 1,
	Static = 2,
	Limited = 3,
	SADX = 4
};

enum class SonicTeamLogoConfig
{
	Animated = 0,
	Static = 1,
	Off = 2,
};

enum class ImpressFontConfig
{
	DefaultDX = 0,
	Impress = 1,
	ComicSans = 2,
};

enum class SetFileConfig
{
	Optimized = 0,
	International = 1,
	Japan = 2
};

enum class FruitConfig
{
	CommonFruits = 0,
	NormalFruitOnly = 1,
	DefaultDX = 2
};

enum class CutsceneSkipConfig
{
	Fadeout = 0,
	Immediate = 1,
	NoSkip = 2,
	DefaultDX = 3
};

class ModConfig
{
public:
	static DebugVerboseConfig DebugVerbose; // Output debug messages for file replacement etc.
	static bool CutsceneTransitionFix; // Fix missing camera transitions when cutscenes end
	static CutsceneSkipConfig CutsceneSkipFix; // Fade out cutscenes
	static ImpressFontConfig FontReplacementMode; // Replace subtitle font
	static SetFileConfig SETReplacement; // Replace SET files

	static bool ColorizeFont; // Change subtitle font color to resemble SA1 DC
	static bool DisableFontSmoothing; // Disable filtering
	static bool DisableFontFiltering;
	static bool EnableLSDFix;
	static bool FPSLock;
	static bool EnableDCRipple;  // Water ripple from the Dreamcast version
	static bool EnableWhiteDiffuse; // 
	static bool RestoreHumpAnimations; // Restore frozen item hold animations
	static bool JapaneseLostWorldSpikes; // Change Lost World spikes behavior to be more like SA1 JP
	
	static bool BigBeltFix; // Transparency fix for Big's belt
	static bool FixHeldObjects; // Improve positioning of held/throwable items like Ice Key etc.
	static bool RestoreYButton; // Restore the functionality of the Y button like on DC
	
	static bool RemoveSetUpPad; // Remove "Set up pad" from the Pause menu
	static bool RemoveMap; // Remove "Map" from the Pause menu
	static bool RemoveCamera; // Remove "Camera" from the Pause menu

	static bool EnableWindowTitle; // Change window title to "Sonic Adventure"
	static bool EnableDCBranding; // Enable Dreamcast controller graphics, menus etc.
	
	static bool IamStupidAndIWantFuckedUpOcean; // Revert Emerald Coast skybox fixes
	static bool EnableCowgirl;
	static bool DisableAllVideoStuff; // Disable all video enhancements

	static bool EnabledLevels[43];

	static bool EnableSkyChaseFixes;

	static bool EnableDXOceanEffects;
	static bool DisableChao;
	static bool ReplaceEggs;
	static bool ReplaceTrees;
	static bool ReplaceNameMachine;
	static FruitConfig ReplaceFruits;
	static bool EnableLobby;
	static bool DisableChaoButtonPrompts;

	static bool EnableSpeedFixes;
	static bool SuppressWarnings;

	static bool DLLLoaded_SA1Chars;
	static bool DLLLoaded_Lantern;
	static bool DLLLoaded_HDGUI;
	static bool DLLLoaded_DLCs;
	static bool DLLLoaded_SADXFE;
	static bool DLLLoaded_SoundOverhaul;
	static bool DLLLoaded_Korean;

	// Branding
	static SonicTeamLogoConfig SonicTeamLogo;
	static bool RemoveMissionMode;
	static bool RemoveQuitPrompt;
	static bool RemoveUnlockMessage;
	static bool RemoveMarketRingCount;
	static bool RemoveGameGearGames;
	static bool EnableTransition;
	static bool DrawOverlay;
	static bool RemoveCream;
	static bool HUDTweak;
	static bool DrawNowSaving;
	static TitleScreenConfig TitleScreenLogoMode;
	static DemosConfig RestoreDemos;

	// Videos
	static bool SA1Intro;
	static bool ColorizeVideos;
};