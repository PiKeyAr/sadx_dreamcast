#pragma once

#include <SADXModLoader.h>

TaskFunc(Draw_22, 0x0060C900); // OLight display function

DataPointer(float, RMCloudSonic_BottomY, 0x006011F6); // Position of Red Mountain bottom cloud for Sonic (hardcoded)
DataPointer(float, RMCloudKnuckles_BottomY, 0x006011ED); // Position of Red Mountain bottom cloud for Knuckles (hardcoded)

DataPointer(NJS_MODEL_SADX, model_ukisima_ukisima1a_ukisima1a, 0x0243762C);
DataPointer(NJS_MODEL_SADX, model_hasi_hasiita_hasiita, 0x02466568);