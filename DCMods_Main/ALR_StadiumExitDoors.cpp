#include "stdafx.h"

// Sliding doors with Chao and hears in Chao Race Entry

DataPointer(NJS_ACTION, action_holl_open_hg_center, 0x033B6EC8);
DataPointer(bool, OpenMainGate, 0x3CE03E0); // Chao Stadium Omochao bow synchronously when this is true

void ChaoSlidingDoors_Display(task* tp)
{
	njSetTexture(&texlist_obj_al_race);
	njPushMatrix(0);
	ds_ActionClip(&action_holl_open_hg_center, tp->twp->counter.f, 1.0f);
	njPopMatrix(1u);
}

void ChaoSlidingDoors_Main(task* tp)
{
	taskwk* twp = tp->twp;
	OpenMainGate = false; // Reset bowing Omochao status
	if (playertwp[0] == nullptr)
		return;
	// Open door
	if (playertwp[0]->pos.x > 2090 && playertwp[0]->pos.x < 2115)
	{
		if (!twp->mode)
		{
			OpenMainGate = true; // Make Omochao bow
			dsPlay_oneshot(SE_CH_DORE_OPEN, 0, 0, 0);
			twp->mode = 1;
		}
		twp->counter.f = min(1, twp->counter.f + 0.04f);
	}
	// Exit
	else if (playertwp[0]->pos.x > 2125)
		AL_ChangeStageLater(CHAO_STG_SS);
	// Close door
	else if (playertwp[0]->pos.x < 2090 || playertwp[0]->pos.x > 2115)
	{
		if (twp->mode)
		{
			dsPlay_oneshot(SE_CH_DORE_OPEN, 0, 0, 0);
			twp->mode = false;
		}
		twp->counter.f = max(0, twp->counter.f - 0.04f);
	}
	ChaoSlidingDoors_Display(tp);
}

void ChaoSlidingDoors_Load(ObjectMaster* om)
{
	task* tp = (task*)om;
	tp->twp->counter.f = 0; // Reset animation frame for action
	tp->twp->mode = 0; // Reset animation status for action
	tp->exec = ChaoSlidingDoors_Main;
	tp->disp = ChaoSlidingDoors_Display;
	tp->dest = FreeTask;
}