#include "stdafx.h"
#include "STG07_LostWorld.h"

static bool ModelsLoaded_STG07;

// Lost World spikes behavior like in SA1 JP
void __cdecl Normal_37_r(task* a1)
{
	// Original code by supercoolsonic
	// Normally Lost World spikes use two global variables (kouka_flag at 0x03C7EE0C and last_counter at 0x03C7EE08) to synchronize movement.
	// This function is adjusted to store them in each object's taskwk (smode for kouka_flag and id for last_counter) so they can move independently.
	taskwk* v1; // esi

	NJS_VECTOR change = { 0, 0, 0 }; // Difference between old and new position
	NJS_VECTOR smoke_velocity; // Used by smoke generation function
	NJS_VECTOR smoke_pos; // Used by smoke generation function
	Angle smoke_rot; // Angle used to generate the smoke effect

	v1 = a1->twp;
	if (!CheckRangeOut(a1))
	{
		float diff_x = playertwp[0]->pos.x - v1->pos.x;
		float diff_y = playertwp[0]->pos.y - v1->pos.y;
		float diff_z = playertwp[0]->pos.z - v1->pos.z;
		Angle ang_x = v1->ang.x;
		Angle ang_y = v1->ang.y;
		Angle ang_z = v1->ang.z;
		switch (v1->mode)
		{
		case 0: // Go down if player is near
			if (sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z) < 60.0f)
			{
				dsPlay_oneshot(SE_LW_NEEDLE, 0, 0, 0);
				v1->smode = 1;
				v1->mode = 20;
				goto updatepos;
			}
			if (!v1->smode == 0)
			{
				goto checkandrender;
			}
			goto updatepos;
		case 1: // Generate smoke
			change.y = v1->scl.y;
			v1->smode = 0;
			v1->scl.z = change.y + v1->scl.z;
			if (v1->scl.z >= v1->scl.x)
			{
				goto checkandrender;
			}
			smoke_rot = (Angle)(atan2(camera_twp->pos.x - v1->pos.x, camera_twp->pos.z - v1->pos.z) * 65536.0f * 0.1591549762031479f);
			smoke_velocity.y = -12.0f;
			smoke_velocity.x = njSin(smoke_rot) * 20.0f;
			smoke_velocity.z = njCos(smoke_rot) * 20.0f;
			njPushMatrix(_nj_unit_matrix_);
			if (ang_z)
				njRotateZ(0, (unsigned __int16)ang_z);
			if (ang_x)
				njRotateX(0, (unsigned __int16)ang_x);
			if (ang_y)
				njRotateY(0, (unsigned __int16)ang_y);
			njCalcVector(0, &smoke_velocity, &smoke_velocity);
			njPopMatrix(1u);
			smoke_pos.x = smoke_velocity.x + v1->pos.x;
			smoke_pos.y = smoke_velocity.y + v1->pos.y;
			smoke_pos.z = smoke_velocity.z + v1->pos.z;
			smoke_velocity.y = 0.0f;
			smoke_velocity.x = 0.0f;
			smoke_velocity.z = 0.0f;
			CreateSmoke(&smoke_pos, &smoke_velocity, 3.0f);
			v1->btimer = 30;
			goto nextmode;
		case 2: // Check if reached the bottom
			if (v1->flag & 0x100)
			{
				v1->mode = 5;
				goto render;
			}
			if (v1->btimer-- == 1)
			{
			nextmode:
				v1->mode += 1;
			}
			goto checkandrender;
		case 3: // Going up
			change.y = v1->scl.y * -0.1f;
			v1->scl.z = change.y + v1->scl.z;
			if (v1->scl.z <= 0.0f)
			{
				goto checkandrender;
			}
			change.y = 0.0f;
			v1->pos.y = v1->timer.f;
			v1->pos.z = v1->value.f;
			v1->btimer = 10;
			goto nextmode;
		case 4: // Reset
			if (v1->btimer-- != 1)
			{
				goto checkandrender;
			}
			v1->mode = 0;
			goto updatepos;
		case 20: // Going down
			if (GameTimer == v1->id)
			{
				if (v1->smode == 2)
				{
					v1->mode = 1;
				}
				else
				{
				checkandrender:
					if (v1->mode == 5)
					{
						goto render;
					}
				}
			}
			else
			{
				v1->smode = 2;
				v1->mode = 1;
			}
		updatepos:
			njPushMatrix(_nj_unit_matrix_);
			if (ang_z)
				njRotateZ(0, (unsigned __int16)ang_z);
			if (ang_x)
				njRotateX(0, (unsigned __int16)ang_x);
			if (ang_y)
				njRotateY(0, (unsigned __int16)ang_y);
			njCalcVector(0, &change, &change);
			njPopMatrix(1u);
			v1->pos.x = change.x + v1->pos.x;
			v1->pos.y = change.y + v1->pos.y;
			v1->pos.z = change.z + v1->pos.z;
		render:
			Disp_118(a1);
			EntryColliList(v1);
			ObjectSetupInput(v1, nullptr);
			v1->id = GameTimer;
			break;
		default:
			goto checkandrender;
		}
	}
}

void __cdecl AokiSwitch_Display(task *a1)
{
	taskwk *v1; // esi

	v1 = a1->twp;
	if (IsPlayerInsideSphere(&v1->pos, 410.0f))
	{
		if (!loop_count)
		{
			SaveControl3D();
			OffControl3D(NJD_CONTROL_3D_CONSTANT_TEXTURE_MATERIAL);
			SetObjectTexture();
			njPushMatrix(0);
			njTranslateV(0, &v1->pos);
			Angle rot_y = v1->ang.y;
			if (rot_y)
				njRotateY(0, rot_y);
			ds_DrawModelClip(object_lwobj_switch_body_switch_body.basicdxmodel, 1.0f);
			if (SwitchFlg_1[((signed __int16 *)&a1->twp->timer)[1]] & 1)
				njTranslate(0, 0.0f, 0.0f, 0.0f);
			else
				njTranslate(0, 0.0f, 0.6f, 0.0f);
			ds_DrawModelClip(object_lwobj_switch_body_switch_body.child->basicdxmodel, 1.0f);
			Uint8 alpha = ((Uint8 *)&v1->counter)[1];
			float alpha_f = (float)alpha / 255.0f;
			njControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
			SetMaterial(alpha_f, alpha_f, alpha_f, alpha_f);
			ds_DrawModelClip(object_lwobj_switch_body_switch_body.child->child->basicdxmodel, 1.0f);
			late_DrawModel(object_lwobj_switch_body_switch_body.child->child->sibling->basicdxmodel, LATE_WZ);
			ResetMaterial();
			OffControl3D(NJD_CONTROL_3D_CONSTANT_MATERIAL);
			njPopMatrix(1u);
			LoadControl3D();
		}
	}
}

void RenderTPanel(NJS_MODEL_SADX *model, LATE blend, float scale)
{
	dsDrawModel(model);
}

void RenderTPanelTriangle(NJS_MODEL_SADX *model, LATE blend, float scale)
{
	late_z_ofs___ = -40000.0f;
	late_DrawModelClip(model, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
}

void RenderTPanelLight(NJS_MODEL_SADX *model, LATE blend, float scale)
{	
	late_z_ofs___ = -20000.0f;
	late_DrawModelClip(model, LATE_LIG, scale);
	late_z_ofs___ = 0.0f;
}

void RenderTPanelDust(NJS_MODEL_SADX *model, LATE blend, float scale) // 5E8AAC
{
	njSetZUpdateMode(0);
	njSetZCompare(7u);
	dsDrawModel(object_obj_komorebi_komorebi.basicdxmodel);
	njSetZCompare(1u);
	njSetZUpdateMode(1);
}

void RLight_Display(task *a1)
{
	taskwk *twp; // esi
	twp = a1->twp;

	if (!CheckRangeOut(a1) && !loop_count)
	{
		njSetZCompare(3u);
		if (!SetObjectTexture())
			njSetTexture((NJS_TEXLIST *)&twp->value.l);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		Angle angx = twp->ang.x;
		Angle angy = twp->ang.y;
		Angle angz = twp->ang.z;
		if (angz)
			njRotateZ(0, (unsigned __int16)angz);
		if (angx)
			njRotateX(0, (unsigned __int16)angx);
		if (angy)
			njRotateY(0, (unsigned __int16)angy);
		float sz = twp->scl.z + 1.0f;
		float sy = twp->scl.y + 1.0f;
		float sx = twp->scl.x + 1.0f;
		float clipscale = GetHighestFloatAbs(sx, sy, sz);
		njScale(0, sx, sy, sz);
		late_DrawObjectClipMesh(&object_lwobj_komorebi2_komorebi2, LATE_MAT, clipscale);
		njPopMatrix(1u);
		npSetZCompare();
	}
}

signed int __cdecl RLight_Load(task *a1)
{
	taskwk *v1; // edi

	v1 = a1->twp;
	if (CheckRangeOut(a1))
		return 1;
	if (CheckObjectTexture())
		v1->value.l = 0;
	else
		NeonuLoadTexture((NJS_TEXLIST *)&v1->value.l);
	if (CheckObjectTexture())
	{
		a1->exec = RLight_Display;
		a1->disp = RLight_Display;
		a1->dest = ObjectNormalExit;
	}
	ObjectScaleDisplay(a1);
	return 0;
}

void __cdecl HitMirror(task* a1)
{
	taskwk* v1; // esi
	float v2; // [esp+4h] [ebp-18h]
	NJS_POINT2 mirror_screen; // [esp+8h] [ebp-14h] BYREF
	NJS_VECTOR mirror_pos; // [esp+10h] [ebp-Ch] BYREF

	v1 = a1->twp;
	mirror_pos = v1->pos;
	mirror_pos.y = mirror_pos.y + 12.0f;
	njPushMatrix(0);
	njProjectScreen(0, &mirror_pos, &mirror_screen);
	njPopMatrix(1u);
	float screencenter_x = (float)HorizontalResolution / 2.0f;
	float screencenter_y = (float)VerticalResolution / 2.0f;
	float screenscale_x = (float)HorizontalResolution / 640.0f;
	float screenscale_y = (float)VerticalResolution / 480.0f;
	float checkpos_x = screencenter_x + 20.0f * screenscale_x;
	v2 = (checkpos_x - mirror_screen.x) * (checkpos_x - mirror_screen.x)
		+ (screencenter_y - mirror_screen.y) * (screencenter_y - mirror_screen.y);
	if (squareroot(v2) < 40.0f * screenscale_y)
	{
		other_flag = (unsigned char)v1->scl.y;
		fog_switch = 0;
		v1->smode = 1;
		discovery = 1;
	}
}

static void __declspec(naked) HitMirror_asm()
{
	__asm
	{
		push eax // a1

		// Call your __cdecl function here:
		call HitMirror

		pop eax // a1
		retn
	}
}

void LostWorld_Init()
{
	ReplaceCAM("CAM0700S");
	ReplaceCAM("CAM0701K");
	ReplaceCAM("CAM0701S");
	ReplaceCAM("CAM0702S");
	ReplaceSET("SET0700S");
	ReplaceSET("SET0701K");
	ReplaceSET("SET0701S");
	ReplaceSET("SET0702S");
	ReplacePVM("BG_RUIN");
	ReplacePVM("RUIN01");
	ReplacePVM("RUIN02");
	ReplacePVM("RUIN03");
	ReplacePVM("OBJ_RUIN");
	ReplacePVM("OBJ_RUIN2");
	// Fog and other stuff
	WriteData<1>((char*)0x005E315D, 0i8); // Prevent the mirror room from disabling character lighting
	WriteData((float*)0x814CB4, -25.0f); // LW2 fog stuff
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg07[0][i].Col = 0xFFFFFFFF;
		pFogTable_Stg07[0][i].f32StartZ = 1.0f;
		pFogTable_Stg07[0][i].f32EndZ = 3400.0f;
		pFogTable_Stg07[0][i].u8Enable = 1;

		pFogTable_Stg07[1][i].Col = 0xFFFFFFFF;
		pFogTable_Stg07[1][i].f32StartZ = 150.0f;
		pFogTable_Stg07[1][i].f32EndZ = 3200.0f;

		pFogTable_Stg07[2][i].u8Enable = 1;
		pFogTable_Stg07[2][i].f32StartZ = 1.0f;
		pFogTable_Stg07[2][i].f32EndZ = 3400.0f;
		pFogTable_Stg07[2][i].Col = 0xFFFFFFFF;

		pClipMap_Stg07[1][i].f32Far = -2700.0f;
	}
}

void LostWorld_Load()
{
	LevelLoader(LevelAndActIDs_LostWorld1, "SYSTEM\\data\\stg07_ruin\\landtable0700.c.sa1lvl", &texlist_ruin01);
	LevelLoader(LevelAndActIDs_LostWorld2, "SYSTEM\\data\\stg07_ruin\\landtable0701.c.sa1lvl", &texlist_ruin02);
	LevelLoader(LevelAndActIDs_LostWorld3, "SYSTEM\\data\\stg07_ruin\\landtable0702.c.sa1lvl", &texlist_ruin03);
	if (!ModelsLoaded_STG07)
	{
		if (ModConfig::SETReplacement == SetFileConfig::Japan || ModConfig::JapaneseLostWorldSpikes)
			WriteJump(Normal_37, Normal_37_r);
		// Improve clip distance for some platforms in the snake room
		objItemTable07.pObjItemEntry[31].ssAttribute = 1;
		objItemTable07.pObjItemEntry[32].ssAttribute = 1;
		objItemTable07.pObjItemEntry[33].ssAttribute = 1;
		objItemTable07.pObjItemEntry[34].ssAttribute = 1;
		objItemTable07.pObjItemEntry[35].ssAttribute = 1;
		objItemTable07.pObjItemEntry[36].ssAttribute = 1;
		objItemTable07.pObjItemEntry[31].fRange = 800000.0f;
		objItemTable07.pObjItemEntry[32].fRange = 800000.0f;
		objItemTable07.pObjItemEntry[33].fRange = 800000.0f;
		objItemTable07.pObjItemEntry[34].fRange = 800000.0f;
		objItemTable07.pObjItemEntry[35].fRange = 800000.0f;
		objItemTable07.pObjItemEntry[36].fRange = 800000.0f;
		// Various effects
		WriteData((float*)0x02039774, 0.005f); // SA1 scale for fire particle
		// AokiSwitch
		WriteJump((void*)0x005E66D0, AokiSwitch_Display);
		object_lwobj_switch_body_switch_body = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_switch_body.nja.sa1mdl"); // AokiSwitch (edited hierarchy)
		// Fix mirror distance detection
		WriteJump((void*)0x005E22C0, HitMirror_asm);
		// Models
		model_lwobj_prop_a02_prop_a02 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_prop_a02.nja.sa1mdl")->basicdxmodel; // RndBox
		NJS_OBJECT* ColA = LoadModel("system\\data\\stg07_ruin\\common\\models\\lost_col_a.nja.sa1mdl");
		model_lost_col_a_col_a = *ColA->basicdxmodel; // ORRaf 1
		model_lost_col_a_col_b = *ColA->child->basicdxmodel; // ORRaf 2
		model_lwobj_snake02_snake02 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake02.nja.sa1mdl")->basicdxmodel; // HebiZou / MRSna
		model_sekichyuu_sekichyuu_sekichyuu = *LoadModel("system\\data\\stg07_ruin\\common\\models\\sekichyuu_sekichyuu.nja.sa1mdl")->basicdxmodel; // Sekicyuu 1
		model_lostobj_terasu_terasu = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lostobj_terasu.nja.sa1mdl")->basicdxmodel; // Sekicyuu 2
		model_l02_hasira_hasira = *LoadModel("system\\data\\stg07_ruin\\common\\models\\l02_hasira.nja.sa1mdl")->basicdxmodel; // Hasira01 and Sekicyuu 3
		model_l02_lightway_h_lightway_h = *LoadModel("system\\data\\stg07_ruin\\common\\models\\l02_lightway_h.nja.sa1mdl")->basicdxmodel; // Hasira02 and Sekicyuu 4
		model_lostobj_allgate_allgate = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lostobj_allgate.nja.sa1mdl")->basicdxmodel;
		model_lwobj_prop_a01_prop_a01 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_prop_a01.nja.sa1mdl")->basicdxmodel; // Fire obstacle
		model_lwobj_snake00_snake00 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake00.nja.sa1mdl")->basicdxmodel; // OTap (snake head)
		model_lwobj_s_water_s_water = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_s_water.nja.sa1mdl")->basicdxmodel; // OTap (water)
		WriteData((NJS_MESHSET_SADX***)0x005E8818, &object_lwobj_s_water_s_water.basicdxmodel->meshsets); // UV animation for OTap
		WriteData((NJS_MESHSET_SADX***)0x005E884C, &object_lwobj_s_water_s_water.basicdxmodel->meshsets); // UV animation for OTap
		object_lwobj_jumpdai_jumpdai.basicdxmodel = LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_jumpdai.nja.sa1mdl")->basicdxmodel; // Lostjumpdai
		object_lwobj_bow_bow.basicdxmodel = LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_bow.nja.sa1mdl")->basicdxmodel; // BurningBow
		object_lwobj_arrow_arrow = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_arrow.nja.sa1mdl"); // BurningBow Arrow
		NJS_OBJECT* Switch = LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_lwswitch.nja.sa1mdl");
		object_lwobj_lwswitch_lwswitch.basicdxmodel = Switch->basicdxmodel; // OSuimenSwitch
		object_lwobj_lwswitch_button.basicdxmodel = Switch->child->basicdxmodel; // OSuimenSwitch button part
		object_big_bigmirror_bigmirror = *LoadModel("system\\data\\stg07_ruin\\common\\models\\big_bigmirror.nja.sa1mdl"); // OBigMr
		object_hebi_asibakabe01_hebi_asibakabe01_hebi_asibakabe01 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebi_asibakabe01_hebi_asibakabe01.nja.sa1mdl"); // Ashikabe01
		object_hebi_asibakabe02_hebi_asibakabe02_hebi_asibakabe02 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebi_asibakabe02_hebi_asibakabe02.nja.sa1mdl"); // Ashikabe02
		object_mirror_mirrorbox_1_mirrorbox_1 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\mirror_mirrorbox_1.nja.sa1mdl"); // RMirror
		model_mirror_hikari_hikari_hikari = *LoadModel("system\\data\\stg07_ruin\\common\\models\\mirror_hikari_hikari.nja.sa1mdl")->basicdxmodel; // RMirror light
		object_lostobj_togetoge_togetoge = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lostobj_togetoge.nja.sa1mdl"); // Toge
		object_togedai_togedai_togedai = *LoadModel("system\\data\\stg07_ruin\\common\\models\\togedai_togedai.nja.sa1mdl"); // TogedaiUD
		object_turncube_turncube_turncube = *LoadModel("system\\data\\stg07_ruin\\common\\models\\turncube_turncube.nja.sa1mdl"); // TurnCube
		object_los_nc_shoku_a_nc_shoku_a = *LoadModel("system\\data\\stg07_ruin\\common\\models\\los_nc_shoku_a.nja.sa1mdl"); // Kusa02 type 1
		object_los_nc_shoku_b_nc_shoku_b = *LoadModel("system\\data\\stg07_ruin\\common\\models\\los_nc_shoku_b.nja.sa1mdl"); // Kusa02 type 2
		object_los_nc_shoku_c_nc_shoku_c = *LoadModel("system\\data\\stg07_ruin\\common\\models\\los_nc_shoku_c.nja.sa1mdl"); // Kusa02 type 3 (Shitakusa)
		object_los_nc_busshu_a_nc_busshu_a = *LoadModel("system\\data\\stg07_ruin\\common\\models\\los_nc_busshu_a.nja.sa1mdl"); // Kusa02 type 4 (Shitakusa)
		object_hebigate_hebikuchi_hebikuchi = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebigate_hebikuchi.nja.sa1mdl"); // Snake gate
		object_hebi_asiba01_hebi_asiba01_hebi_asiba01 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebi_asiba01_hebi_asiba01.nja.sa1mdl"); // Ashiba01
		object_hebi_asiba02_hebi_asiba02_hebi_asiba02 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebi_asiba02_hebi_asiba02.nja.sa1mdl"); // Ashiba02
		object_hebi_asiba03_hebi_asiba03_hebi_asiba03 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\hebi_asiba03_hebi_asiba03.nja.sa1mdl"); // Ashiba03
		object_lwobj_snake01_head_snake01_head = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake01_head.nja.sa1mdl"); // Snake head
		object_lwobj_snake01_body_snake01_body = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake01_body.nja.sa1mdl"); // Snake joints
		object_lwobj_snake01_tail2_snake01_tail2 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake01_tail2.nja.sa1mdl"); // Snake joint near the tail
		object_lwobj_snake01_tail3_snake01_tail3 = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake01_tail3.nja.sa1mdl"); // Snake tail
		object_lwobj_snake01_tail_snake01_tail = *LoadModel("system\\data\\stg07_ruin\\common\\models\\lwobj_snake01_tail.nja.sa1mdl"); // Snake tail tip
		object_l01_wt_hebimizu_wt_hebimizu = *LoadModel("system\\data\\stg07_ruin\\common\\models\\l01_wt_hebimizu.nja.sa1mdl"); // OSuimen
		AddTextureAnimation_Permanent(7, 0, &object_l01_wt_hebimizu_wt_hebimizu.basicdxmodel->mats[0], false, 1, 44, 57);
		object_l03_wt_mizu_wt_mizu = *LoadModel("system\\data\\stg07_ruin\\common\\models\\no_unite\\l03_wt_mizu.nja.sa1mdl"); // Some other water (loaded with the skybox)
		// Rlight stuff
		WriteCall((void*)0x005E8976, RLight_Load);
		object_lwobj_komorebi2_komorebi2.basicdxmodel->mats[0].attrflags &= ~NJD_SA_ONE;
		object_lwobj_komorebi2_komorebi2.basicdxmodel->mats[0].attrflags &= ~NJD_DA_ONE;
		object_lwobj_komorebi2_komorebi2.basicdxmodel->mats[0].attrflags |= NJD_SA_SRC;
		object_lwobj_komorebi2_komorebi2.basicdxmodel->mats[0].attrflags |= NJD_DA_DST;
		// TPanel fixes
		WriteCall((void*)0x005E9216, RenderTPanelTriangle);
		WriteCall((void*)0x005E927F, RenderTPanelLight);
		WriteCall((void*)0x005E91CA, RenderTPanel);
		WriteCall((void*)0x005E8AAC, RenderTPanelDust);
		object_obj_komorebi_komorebi = *LoadModel("system\\data\\stg07_ruin\\common\\models\\obj_komorebi.nja.sa1mdl"); // TPanel effect
		// Water fixes
		WriteData<1>((void*)0x005E2090, 0xC3u); // Kill water animation in Act 1
		ModelsLoaded_STG07 = true;
	}
}

void LostWorld_OnFrame()
{
	if (ssStageNumber != LevelIDs_LostWorld)
		return;
	if (!ChkPause())
	{
		// Draw distance hack for Act 2 boulder corridor
		if (ssActNumber == 1)
		{
			auto entity = playertwp[0];
			if (entity != nullptr && entity->pos.x < 7000.0f && entity->pos.x > 1800.0f)
				gClipMap.f32Far = -6000.0f;
			else
				gClipMap.f32Far = -2700.0f;
		}
	}
}