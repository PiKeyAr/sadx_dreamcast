#pragma once

#include <SADXModLoader.h>

enum class CutsceneFade
{
	FadeoutEnabled = 0, // Normal
	FadeoutWait = 1, // Set when full black is reached
	FadeoutEnd = 2, // Set when cutscene is successfully skipped
};

static int CutsceneFadeAlpha = 0; // 255 is black, 0 is transparent
static CutsceneFade CutsceneFadeoutMode = CutsceneFade::FadeoutEnabled;
static bool SkipPressed_Cutscene = false;

void ScaleEnvironmentMap(float value1 = -1, float value2 = -1, float value3 = -1, float value4 = -1);
void RestoreEnvironmentMap();
void PatchModels_Event();

DataPointer(NJS_ACTION*, _action_tr2a_s_t2b_body, 0x006B9527);
DataPointer(NJS_ACTION, _action_c0_heli_body, 0x02DBD864);
DataArray(TEX_PVMTABLE, evpvm_125, 0x02C12BEC, 4); // Sonic's intro PVM list