#include "stdafx.h"

NJS_TEXNAME textures_garden01_sky[5];
NJS_TEXLIST texlist_garden01_sky = { arrayptrandlength(textures_garden01_sky) };

NJS_OBJECT* ChaoGardenSky_EC_Sky = nullptr;
NJS_OBJECT* ChaoGardenSky_EC_Water = nullptr;

bool ModelsLoaded_ECGarden = false;

void LoadObjects_EC();

void ECGardenWater_Display(task* tp)
{
	if (!loop_count)
	{
		// Skybox
		___njFogDisable();
		___njSetBackColor(0x000029u, 0x000029u, 0x000029u);
		njSetTexture(&texlist_garden01_sky);
		njPushMatrix(0);
		njTranslate(0, 0, 0, 0);
		njScale(0, 1.0f, 1.0f, 1.0f);
		ds_DrawObjectClip(ChaoGardenSky_EC_Sky, 1.0f);
		njPopMatrix(1u);
		// Water
		njSetTexture(&texlist_garden01ec);
		njPushMatrix(0);
		njTranslate(0, 0, -415.8f, 0);
		late_DrawObjectClip(ChaoGardenSky_EC_Water, LATE_NO, 1.0f); // Water
		njPopMatrix(1u);
		___njFogEnable();
	}
}

void ECGardenWater_Main(task* tp)
{
	taskwk* twp = tp->twp;
	if (g_CurrentFrame < 2 && ulGlobalTimer % 4 == 0 || g_CurrentFrame == 2 && ulGlobalTimer % 2 == 0 || g_CurrentFrame > 2)
		twp->btimer++;
	if (twp->btimer > 63)
		twp->btimer = 54;
	ChaoGardenSky_EC_Water->basicdxmodel->mats[0].attr_texId = twp->btimer;
	ECGardenWater_Display(tp);
}

void ECGardenWater_Load(task* tp)
{
	taskwk* twp = tp->twp;
	twp->btimer = 54;
	tp->exec = ECGardenWater_Main;
	tp->disp = ECGardenWater_Display;
	tp->dest = (void(__cdecl*)(task*))nullsub;
}

void Garden01_Load()
{
	LevelLoader(LevelAndActIDs_ECGarden, "SYSTEM\\data\\chao\\stg_garden01_ec\\landtablegarden01.c.sa1lvl", &texlist_garden01ec);
	if (!ModelsLoaded_ECGarden)
	{
		AL_StartPosEC_Warp.pos.y = 71.0f; // Prevent endless jumping in EC garden with the DC model for the Name Machine (DC garden only)
		AL_StartPosEC_Odekake.pos.y = 3.2f;
		AL_StartPosEC_Warp.pos.y = 71.0f;
		ChaoGardenSky_EC_Sky = LoadModel("system\\data\\chao\\stg_garden01_ec\\map\\ecch_nc_sky.nja.sa1mdl"); // Modified model with different UVs
		ChaoGardenSky_EC_Water = LoadModel("system\\data\\chao\\stg_garden01_ec\\map\\ecch_wt_sea.nja.sa1mdl");
		ChaoGardenSky_EC_Water->basicdxmodel->mats[0].attrflags &= ~NJD_DA_ONE;
		ChaoGardenSky_EC_Water->basicdxmodel->mats[0].attrflags &= ~NJD_SA_ONE;
		ChaoGardenSky_EC_Water->basicdxmodel->mats[0].attrflags |= NJD_SA_SRC;
		ChaoGardenSky_EC_Water->basicdxmodel->mats[0].attrflags |= NJD_DA_INV_SRC;
		ChaoGardenSky_EC_Water->basicdxmodel->mats[0].diffuse.argb.a = 127;
		WriteData<1>((void*)0x00718FE8, 0xC4); // Disable fall Y check
		WriteData((float*)0x007191BF, -12000.0f); // Draw distance
		WriteData((float*)0x007190FE, 131.67f); // Name machine
		WriteData((float*)0x00719106, 2.6f); // Name machine
		WriteData((float*)0x0071910E, -204.28f); // Name machine
		WriteData((float*)0x00719147, 92.5f); // SS transporter
		WriteData((float*)0x00719142, 70.86f);  // SS transporter
		WriteData((float*)0x0071913D, -10.77f);  // SS transporter
		WriteData((float*)0x0071912B, 80.47f); // MR transporter
		WriteData((float*)0x00719126, 70.86f);  // MR transporter
		WriteData((float*)0x00719121, -41.76f);  // MR transporter
		ChaoSetPositionEC[0] = { 102.0f, 78.375f, 29.5f };
		ChaoSetPositionEC[1] = { 129.625f, 10.125f, 113.0f };
		ChaoSetPositionEC[2] = { 203.625f, 11.625f, -61.975f };
		ChaoSetPositionEC[3] = { 132.0f, 12.375f, 107.0f };
		ChaoSetPositionEC[4] = { -37.0f, 42.95f, 135.0f };
		ChaoSetPositionEC[5] = { -145.0f, 13.475f, 129.0f };
		ChaoSetPositionEC[6] = { -202.875f, 13.5f, 83.0f };
		ChaoSetPositionEC[7] = { -120.0f, 78.375f, -102.0f };
		ChaoSetPositionEC[8] = { -83.0f, 15.55f, -232.5f };
		ChaoSetPositionEC[9] = { 71.0f, 11.25f, -254.0f };
		ChaoSetPositionEC[10] = { 103.0f, 41.075f, -137.0f };
		ChaoSetPositionEC[11] = { 2.875f, 41.625f, -147.625f };
		ChaoSetPositionEC[12] = { 202.625f, 11.25f, -76.0f };
		ChaoSetPositionEC[13] = { 31.0f, 11.25f, 164.5f };
		ChaoSetPositionEC[14] = { -272.0f, 12.375f, -61.0f };
		ChaoSetPositionEC[15] = { -195.0f, 10.125f, -207.0f };
		TreeSetPos[1][0].x = -181; // Palm tree 1
		TreeSetPos[1][0].y = 4.0f;  // Palm tree 1
		TreeSetPos[1][0].z = -151.19f;  // Palm tree 1
		TreeSetPos[1][1].x = -102.76f; // Palm tree 2
		TreeSetPos[1][1].y = 4.0f;  // Palm tree 2
		TreeSetPos[1][1].z = -193.18f;  // Palm tree 2
		TreeSetPos[1][2].x = -8.89f; // Palm tree 3
		TreeSetPos[1][2].y = 4.0f;  // Palm tree 3
		TreeSetPos[1][2].z = -219.26f;  // Palm tree 3
		TreeSetPos[1][3].x = 46.81f; // Palm tree 4
		TreeSetPos[1][3].y = 4.0f;  // Palm tree 4
		TreeSetPos[1][3].z = -220.62f;  // Palm tree 4
		TreeSetPos[1][4].x = 100.89f; // Palm tree 5
		TreeSetPos[1][4].y = 72.0f;  // Palm tree 5
		TreeSetPos[1][4].z = -65.27f;  // Palm tree 5
		ModelsLoaded_ECGarden = true;
	}
}

void _alg_garden01_ec_prolog_r()
{
	PrintDebug("DCChaoStgGarden01EC Prolog begin\n");
	texLoadTexturePvmFile("GARDEN01_SKY", &texlist_garden01_sky);
	Garden01_Load();
	CreateElementalTask(LoadObj_Data1, 2, AL_Garden01Master);
	CreateElementalTask(LoadObj_Data1, 2, ECGardenWater_Load);
	LoadObjects_EC();
	LandChangeLandTable(GetLevelLandtable(LevelAndActIDs_ECGarden));
	PrintDebug("DCChaoStgGarden01EC Prolog end\n");
}

void Garden01_Init()
{
	ReplacePVM("GARDEN01");
	ReplacePVM("GARDEN01_SKY");
	WriteJump(_alg_garden01_ec_prolog, _alg_garden01_ec_prolog_r);
}