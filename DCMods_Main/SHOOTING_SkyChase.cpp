#include "stdafx.h"
#include "SHOOTING_SkyChase.h"

static bool ModelsLoaded_SHOOTING;

// Skybox part 1 in Act 1
void FixSky1(NJS_OBJECT* a1, float scale)
{
	if (ssStageNumber == LevelIDs_SkyChase2)
	{
		late_z_ofs___ = -40000.0f;
		DrawObjectClipEx(a1, LATE_MAT, scale);
		late_z_ofs___ = 0;
	}
	else
		ds_DrawObjectClip(a1, scale);
}

// Skybox part 2 in Act 1
void FixSky2(NJS_OBJECT* a1, float scale)
{
	if (ssStageNumber == LevelIDs_SkyChase2)
	{
	late_z_ofs___ = -25000.0f;
	DrawObjectClipEx(a1, LATE_MAT, scale);
	late_z_ofs___ = 0;
	}
	else
		ds_DrawObjectClip(a1, scale);
}

// Skybox draw function general
void DrawSkyChaseSkybox(NJS_OBJECT* a1, float scale)
{
	if (ssStageNumber == LevelIDs_SkyChase2)
	{
		late_z_ofs___ = -40000.0f;
		DrawObjectClipEx(a1, LATE_WZ, scale);
		late_z_ofs___ = 0;
	}
	else
		ds_DrawObjectClip(a1, scale);
}

// Depth hack for rockets
void RenderSkyChaseRocket(NJS_POINT3COL* a1, int texnum, NJD_DRAW n, LATE a4)
{
	late_z_ofs___ = 20000.0f;
	late_DrawPolygon3D(a1, texnum, n, LATE_LIG);
	late_z_ofs___ = 0;
}

// Override constant attributes for rockets
void SetSkyChaseRocketColor(float a, float r, float g, float b)
{
	OnConstantAttr(0, NJD_FLAG_IGNORE_LIGHT);
	SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
}

// Adds white diffuse materials from a range of material IDs in a model
void AddObjectWhiteDiffuseMaterials_Range(NJS_OBJECT* object, int first, int last)
{
	if (object->basicdxmodel)
	{
		for (int j = first; j < last + 1; j++)
		{
			AddWhiteDiffuseMaterial(&object->basicdxmodel->mats[j]);
		}
	}
}

// Act 1 Egg Carrier beam draw callback
void BeamCallback(float frame)
{
	njSetZCompare(7u);
	njSetZUpdateMode(0);
	njAction(&action_beam2_beamev_s_beam, frame);
	njSetZUpdateMode(1);
	npSetZCompare();
}

// Act 1 Egg Carrier beam display function
void __cdecl BeamDisp_r(task* a1)
{
	taskwk* v1; // esi
	v1 = a1->twp;
	njPushMatrix(0);
	njTranslate(0, 0.0f, 0.0f, 0.0f); // Wtf?
	njSetTexture(&texlist_shooting0);
	njAction(&action_beam2_shot_bf_s_bodya, v1->counter.f);
	late_ActionEx(&action_shot_s_ecfire, v1->counter.f, LATE_LIG);
	njPopMatrix(1u);
	njPushMatrix(0);
	njTranslate(0, 0.0f, 0.0f, 0.0f);
	njSetTexture(&texlist_shooting1);
	// Draw normally after the camera angle changes, but before that just draw on top of everything
	if (v1->counter.f >= 440.0f)
		late_ActionClipEx(&action_beam2_beamev_s_beam, v1->counter.f, LATE_LIG, 1.0);
	else
		late_SetFunc((void(__cdecl*)(void*))BeamCallback, (void*)v1->counter.l, 30000.0f, LATE_LIG);
	njPopMatrix(1u);
	njPushMatrix(0);
	njTranslate(0, 0.0f, -648.09998f, -3698.0f);
	njSetTexture(&texlist_shooting0);
	late_ActionClipEx(&action_beam2_stef_s_tamari, v1->counter.f, LATE_LIG, 1.0);
	njPopMatrix(1u);
}

void SkyChase_Init()
{
	// Common for both acts
	PatchModels_Shooting0();
	ReplacePVM("SHOOTING0");
	// Cursor textures
	if (!ModConfig::DLLLoaded_HDGUI)
	{
		ReplacePVR("ST_064S_LOCKA");
		ReplacePVR("ST_064S_LOCKB");
		ReplacePVR("ST_064S_LOCKC");
		ReplacePVR("STG_S_LOCKMK");
	}
	WriteData<1>((char*)0x0062751B, 0i8); // Force Tornado light type
	WriteData<1>((char*)0x0062AC1F, 0i8); // Force Tornado light type (transformation cutscene)
	// Act 1 only
	if (ModConfig::EnabledLevels[LevelIDs_SkyChase1])
	{
		PatchModels_Shooting1();
		ReplaceSET("SETSHT1S");
		ReplaceCAM("CAMSHT1S");
		if (!ModConfig::DLLLoaded_SA1Chars)
			ReplacePVM("SHOOTING1");
	}
	// Act 2 only
	if (ModConfig::EnabledLevels[LevelIDs_SkyChase2])
	{
		PatchModels_Shooting2();
		ReplaceSET("SETSHT2S");
		ReplaceCAM("CAMSHT2S");
		if (!ModConfig::DLLLoaded_SA1Chars)
			ReplacePVM("SHOOTING2");
	}
}

void SkyChase_Load()
{
	// This stuff is done only once
	if (!ModelsLoaded_SHOOTING)
	{
		// Common stuff for both acts
		if (ModConfig::EnabledLevels[LevelIDs_SkyChase1] || ModConfig::EnabledLevels[LevelIDs_SkyChase2])
		{
			WriteCall((void*)0x0062C161, DrawSkyChaseSkybox); // Skybox rendering function for both acts
			object_sht_s_sitakumo2_s_sitakumo2.basicdxmodel->mats[0].attrflags |= NJD_FLAG_USE_ALPHA;
			sht2_object_ecsc_s_sitakumo_s_sitakumo.basicdxmodel->mats[0].attrflags |= NJD_FLAG_USE_ALPHA;
			// Egg Carrier model
			object_shot_bf_s_bodya_bf_s_bodya = *LoadModel("system\\data\\shooting\\common\\models\\shot_bf_s_bodya.nja.sa1mdl");
			// Oh gee... This is gonna be long
			AddObjectWhiteDiffuseMaterials_Range(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling, 75, 102);
			AddObjectWhiteDiffuseMaterials_Range(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling->sibling, 19, 96);
			AddObjectWhiteDiffuseMaterials_Range(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling->sibling->sibling, 37, 105);
			AddObjectWhiteDiffuseMaterials_Range(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling->sibling->sibling->sibling, 33, 102);
			ForceLevelSpecular_Object(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling, false);
			ForceLevelSpecular_Object(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling->sibling->sibling->sibling->sibling, false);
			ForceLevelSpecular_Object(object_shot_bf_s_bodya_bf_s_bodya.child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling, false);
			// Other objects
			WriteCall((void*)0x0062C764, SetSkyChaseRocketColor);
			WriteCall((void*)0x0062C704, RenderSkyChaseRocket);
		}
		// Act 1 only
		if (ModConfig::EnabledLevels[LevelIDs_SkyChase1])
		{
			// Skybox scale
			for (int i = 0; i < 3; i++)
			{
				pScale_Sht[0][i].x = 4.0f;
				pScale_Sht[0][i].y = 4.0f;
				pScale_Sht[0][i].z = 4.0f;
				if (ModConfig::EnableSkyChaseFixes)
					pClipMap_Sht[0][i].f32Far = -60000.0f;
			}
			WriteCall((void*)0x0062BF35, FixSky1); // Sky piece 1 (act 1)
			WriteCall((void*)0x0062C01D, FixSky2); // Sky piece 2 (act 1)
			// Tornado hit specular
			object_beam_beam_tr1_s_l_f4_3.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
			object_beam_beam_tr1_s_l_f5_3.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
			object_beam_beam_tr1_s_r_f4_3.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
			object_beam_beam_tr1_s_r_f5_3.basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
			WriteJump(BeamDisp, BeamDisp_r); // Make the beam in Act 1 render above the clouds
		}
		ModelsLoaded_SHOOTING = true;
	}
}