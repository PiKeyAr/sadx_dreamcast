#include "stdafx.h"
#include "ADV00_StationSquare.h"

// TODO: OGaitou light...

static bool ModelsLoaded_ADV00;

static Sint8 DelayedTimeOfDay = -1;

// Remove the check to "turn on the lights" in the hotel
int __cdecl checkInSsHodelLol()
{
	return 0;
}

// Car display function hook
static Trampoline* SSCar_Display_t = nullptr;
static void __cdecl SSCar_Display_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(SSCar_Display);
	motionwk* mwk = tp->mwp;
	// Fix animation frame going out of range
	if (mwk->height > 7)
		mwk->height = 0;
	original(tp);
}

// Car drawing hook
void FixPoliceCar(NJS_ACTION* action, float frame, int flags)
{
	// Draw the transparent parts separately if this is the police car
	if (action->object == &object_sspatcar_body_body)
	{
		late_ActionClip(action, frame, LATE_WZ, 1.0f);
		njPushMatrix(0);
		njTranslate(0, action->object->pos[0], action->object->pos[1], action->object->pos[2]);
		late_z_ofs___ = 1000.0f;
		late_DrawModelClip(action->object->child->sibling->sibling->sibling->sibling->basicdxmodel, LATE_MAT, 1.0f);
		late_z_ofs___ = 0.0f;
		njPopMatrix(1u);
	}
	// Draw the car animation without transparent parts
	else
		ds_ActionClip(action, frame, 1.0f);
}

// Depth hack: Police car barricade
void RenderPoliceCarBarricade(NJS_OBJECT* obj, float scale)
{
	late_DrawObjectClipEx(obj, LATE_WZ, scale);
	njPushMatrix(0);
	njTranslate(0, obj->pos[0], obj->pos[1], obj->pos[2]);
	late_z_ofs___ = 1000.0f;
	late_DrawModelClip(obj->child->sibling->sibling->sibling->sibling->basicdxmodel, LATE_MAT, scale);
	late_z_ofs___ = 0.0f;
	njPopMatrix(1u);
}

// Depth hack: City Hall doors
void RenderOfficeDoor(NJS_MODEL_SADX *a1, float scale)
{
	late_z_ofs___ = -2000.0f;
	late_DrawModel(a1, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

// Depth hack: City Hall doors (2)
void RenderOfficeDoor_Child(NJS_MODEL_SADX *a1, float scale)
{
	late_z_ofs___ = -3000.0f;
	late_DrawModel(a1, LATE_WZ);
	late_z_ofs___ = 0.0f;
}

// Pool parasol display hook
void RenderParasol(NJS_MODEL_SADX* model, LATE flags , float scale)
{
	ds_DrawModelClip(object_poolparasol_poolparasol_poolparasol.basicdxmodel, scale);
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->basicdxmodel, LATE_WZ, 1.0f); // Chair bottom
	//late_z_ofs___ = 1000.0f;
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->basicdxmodel, LATE_WZ, 1.0f); // Chair side
	//late_z_ofs___ = 1100.0f;
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->sibling->basicdxmodel, LATE_WZ, 1.0f); // Glass bottom
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->sibling->sibling->sibling->basicdxmodel, LATE_WZ, 1.0f); // Lemon
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->sibling->sibling->sibling->sibling->basicdxmodel, LATE_WZ, 1.0f); // Heart
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->sibling->sibling->basicdxmodel, LATE_WZ, 1.0f); // Glass body
	//late_z_ofs___ = 1200.0f;
	late_DrawModelClip(object_poolparasol_poolparasol_poolparasol.child->sibling->sibling->sibling->sibling->sibling->sibling->basicdxmodel, LATE_WZ, 1.0f); // Parasol
	//late_z_ofs___ = 0.0f;
}

// Delay updating the time of day to prevent sudden lighting change when exiting the sewers
void DelaySettingTimeOfDay(Sint8 time)
{
	DelayedTimeOfDay = time;
}

// Depth hack: OMSaku (Gamma's target on beach)
void OMSakuFix(NJS_OBJECT *a1, float scale)
{
	late_DrawObjectClip(a1, LATE_WZ, scale);
}

// Depth hack: City Hall barricade
void OOfficeBarricade_Fix(NJS_MODEL_SADX* a1, float scale)
{
	late_DrawModel(a1, LATE_WZ);
}

// Depth hack: Big's puzzle
void BigPuzzleFix(NJS_OBJECT* obj, float scale)
{
	late_DrawObjectClip(obj, LATE_LIG, 1.0f);
}

// Depth hack: Planet decoration
void OWakuseiFix(NJS_MODEL_SADX *model, LATE blend, float scale)
{
	if (EV_MainThread_ptr) late_z_ofs___ = 9000.0f;
	late_DrawModelClip(model, blend, scale);
	if (EV_MainThread_ptr) late_z_ofs___ = 0.0f;
}

// Depth hack: Souvenir shop door
void SouvenirShopDoor_Depth(NJS_ACTION* a1, float a2, LATE a3, float a4)
{
	late_z_ofs___ = 3000.0f;
	late_ActionClipEx(a1, a2, a3, a4);
	late_z_ofs___ = 0.0f;
}

// Queue drawing casino door
void DrawCasinoDoor1(NJS_MODEL_SADX* model, float scale)
{
	late_DrawModelClip(model, LATE_WZ, 1.0f);
}

// Queue drawing casino door (2)
void DrawCasinoDoor2(NJS_MODEL_SADX* model, float scale)
{
	late_z_ofs___ = -1000.0f;
	late_DrawModelClip(model, LATE_WZ, 1.0f);
}

// Queue drawing casino door (3)
void DrawCasinoDoor3(NJS_MODEL_SADX* model, float scale)
{
	late_DrawModelClip(model, LATE_WZ, 1.0f);
	late_z_ofs___ = 0.0f;
}

void HotelDoorCallback(void* frame)
{
	ds_ActionClip(&action_sscasinosikake_hnakagen, *(float*)&frame, 1.0f);
}

void RenderHotelDoor(NJS_ACTION* a1, float a2, int a3, float a4)
{
	late_SetFunc(HotelDoorCallback, (void*)(*(int*)&a2), 2000.0f, LATE_MAT);
}

// Ice key display hook
void IceKeySSFix(NJS_ACTION* action, float frame, int flags, float scale)
{
	late_z_ofs___ = -28000.0f;
	late_ActionClipMesh(action, frame, LATE_WZ, scale);
	late_z_ofs___ = -27000.0f;;
	late_DrawModelClipMesh(object_ssobj_keyice_f_keyice.basicdxmodel, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
}

void SetDayTexture(NJS_MATERIAL* material, DaytimeTextureInfo* info)
{
	if (info == nullptr)
		return;
	Sint8 tday = SeqGetTime();
	int texid = material->attr_texId;
	int t = 0;
	do
	{
		if (texid == info[t].texids[0] || texid == info[t].texids[1] || texid == info[t].texids[2])
			material->attr_texId = info[t].texids[tday];
		t++;
	} while (info[t].texids[0] != -1);
}

void AddOrRemoveIgnoreLightFlag_TimeOfDay(NJS_MATERIAL* material)
{
	Uint32 materialflags = material->attrflags;
	Sint8 TimeOfDay = SeqGetTime();
	if (materialflags & NJD_CUSTOMFLAG_NIGHT && !(materialflags & NJD_CUSTOMFLAG_WHITE))
	{
		// Add the Ignore Light flag at night
		if (TimeOfDay == 2)
		{
			if (!(materialflags & NJD_FLAG_IGNORE_LIGHT)) 
				material->attrflags |= NJD_FLAG_IGNORE_LIGHT;
		}
		// Remove the Ignore Light flag at other times of day
		else
		{
			if (materialflags & NJD_FLAG_IGNORE_LIGHT) 
				material->attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		}
	}
}

// Hook to change textures at different times of day
void SSChangeTimetex_r()
{
	if (!IsLevelLoaded(LevelIDs_StationSquare, ssActNumber))
		return;

	Sint8 TimeOfDay = SeqGetTime();
	_OBJ_LANDTABLE* landtable = ___LANDTABLESS[ssActNumber];

	for (int j = 0; j < landtable->ssCount; j++)
	{
		for (int k = 0; k < landtable->pLandEntry[j].pObject->basicdxmodel->nbMat; ++k)
		{
			NJS_MATERIAL* material = &landtable->pLandEntry[j].pObject->basicdxmodel->mats[k];
			// Add or remove "ignore light"
			AddOrRemoveIgnoreLightFlag_TimeOfDay(material);
			// Go through all materials and set day/evening/night texture IDs
			SetDayTexture(material, SSDayTexInfo[ssActNumber]);
			// This is specialcased because bullshit
			if (ssActNumber == 3 && landtable->pLandEntry[j].pObject->pos[1] == 45)
				SetDayTexture(material, &SS03DayTexInfo_Special);
		}
	}
}

// Street lights display (daytime)
void OGaitou_Hack(NJS_MODEL_SADX* mdl, LATE flgs, float clipScl)
{
	ds_DrawModelClip(mdl, clipScl);
}

// Street lights display (night)
void __cdecl OGaitou_Display_Night(task* tp)
{
	taskwk* twp; // edi
	Angle y; // eax
	NJS_OBJECT* sibling; // esi
	Angle v4; // eax
	float ZDist; // [esp+0h] [ebp-20h]
	if (!loop_count)
	{
		twp = tp->twp;
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		njTranslateV(0, (NJS_VECTOR*)object_slights_gaitou_gaitou.pos);
		// Pole
		njPushMatrix(0);
		y = twp->ang.y;
		if (y)
		{
			njRotateY(0, y);
		}
		ds_DrawModelClip(object_slights_gaitou_gaitou.basicdxmodel, 1.0f);
		njPopMatrix(1u);
		// Pole top
		sibling = object_slights_gaitou_gaitou.child->sibling;
		njTranslate(0, sibling->pos[0], sibling->pos[1], sibling->pos[2]);
		v4 = twp->ang.y;
		if (v4)
		{
			njRotateY(0, v4);
		}
		njScale(0, sibling->scl[0], sibling->scl[1], sibling->scl[2]);
		ZDist = GetHighestFloatAbs(sibling->scl[0], sibling->scl[1], sibling->scl[2]);
		ds_DrawModelClip(sibling->basicdxmodel, ZDist);
		njPopMatrix(1u);
		// Light
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		NJS_VECTOR posVec = { twp->pos.x, twp->pos.y + 39.0f, twp->pos.z };
		NJS_VECTOR zoomVec = { njSin(camera_twp->ang.y) * 3.5f, njSin(camera_twp->ang.z) * 3.5f, njCos(camera_twp->ang.y) * 3.5f };
		njAddVector(&posVec, &zoomVec);
		sprite_lamp.p.x = posVec.x;
		sprite_lamp.p.y = twp->pos.y + 39.0f;
		sprite_lamp.p.z = posVec.z;
		Set3DSpriteDepth(-3.0f);
		SetMaterial(0.7f, 0.8f, 0.8f, 0.8f);
		saDrawSprite3D(&sprite_lamp, 0, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE | NJD_SPRITE_COLOR);
		ResetMaterial();
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		late_DrawModelClip(object_slights_gaitou_gaitou.child->basicdxmodel, LATE_LIG, 1.0f);
		njPopMatrix(1u);
	}
}

// Correct the texture list for Casino entrance shutter in station area
void SetCasinoShutterTexture()
{
	njSetTexture(&texlist_ss_casino);
}

void ADV00_Init()
{
	ReplaceSET("SETSS00A");
	ReplaceSET("SETSS00B");
	ReplaceSET("SETSS00E");
	ReplaceSET("SETSS00K");
	ReplaceSET("SETSS00L");
	ReplaceSET("SETSS00M");
	ReplaceSET("SETSS00S");
	ReplaceSET("SETSS01A");
	ReplaceSET("SETSS01B");
	ReplaceSET("SETSS01E");
	ReplaceSET("SETSS01K");
	ReplaceSET("SETSS01L");
	ReplaceSET("SETSS01M");
	ReplaceSET("SETSS01S");
	ReplaceSET("SETSS02S");
	ReplaceSET("SETSS02B");
	ReplaceSET("SETSS03A");
	ReplaceSET("SETSS03B");
	ReplaceSET("SETSS03E");
	ReplaceSET("SETSS03K");
	ReplaceSET("SETSS03L");
	ReplaceSET("SETSS03M");
	ReplaceSET("SETSS03S");
	ReplaceSET("SETSS04A");
	ReplaceSET("SETSS04B");
	ReplaceSET("SETSS04E");
	ReplaceSET("SETSS04K");
	ReplaceSET("SETSS04L");
	ReplaceSET("SETSS04M");
	ReplaceSET("SETSS04S");
	ReplaceSET("SETSS05S");
	ReplaceCAM("CAMSS00S");
	ReplaceCAM("CAMSS01S");
	ReplaceCAM("CAMSS02S");
	ReplaceCAM("CAMSS03S");
	ReplaceCAM("CAMSS04S");
	ReplaceCAM("CAMSS05S");
	ReplacePVM("ADVSS00");
	ReplacePVM("ADVSS01");
	ReplacePVM("ADVSS02");
	ReplacePVM("ADVSS03");
	ReplacePVM("ADVSS04");
	ReplacePVM("ADVSS05");
	ReplacePVM("OBJ_SS");
	ReplacePVM("SS_BG");
	ReplacePVM("SS_BOAT");
	ReplacePVM("SS_BURGER");
	ReplacePVM("SS_CASINO");
	ReplacePVM("SS_DENTOU");
	ReplacePVM("SS_EKIIN");
	ReplacePVM("SS_KANBAN");
	ReplacePVM("SS_MIZUGI");
	ReplacePVM("SS_PEOPLE");
	ReplacePVM("SS_TRAIN");
	ReplacePVM("SS_TWINS");
	ReplacePVM("SSCAR");
	ReplacePVM("SSPATCAR_BODY");
	ReplacePVR("SS_FINESKY");
	ReplacePVR("SS_NIGHTSKY");
	ReplacePVR("SS_NIGHTSKYB");
	ReplacePVR("SS_YUSKAY_MINI");
	ReplacePVR("BELT2"); // Train or boat related (unused?)
	// Fog data
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Adv00[0][i].u8Enable = 1;
		pFogTable_Adv00[0][i].f32StartZ = -12000.0f;
		pFogTable_Adv00[0][i].f32EndZ = -12000.0f;

		pFogTable_Adv00[1][i].u8Enable = 1;
		pFogTable_Adv00[1][i].f32EndZ = -12000.0f;
		pFogTable_Adv00[1][i].f32StartZ = -12000.0f;

		pFogTable_Adv00[2][i].u8Enable = 1;
		pFogTable_Adv00[2][i].f32EndZ = -12000.0f;
		pFogTable_Adv00[2][i].f32StartZ = -12000.0f;

		pFogTable_Adv00[3][i].u8Enable = 1;
		pFogTable_Adv00[3][i].f32StartZ = -12000.0f;
		pFogTable_Adv00[3][i].f32EndZ = -12000.0f;

		pFogTable_Adv00[4][i].u8Enable = 1;
		pFogTable_Adv00[4][i].f32StartZ = -12000.0f;
		pFogTable_Adv00[4][i].f32EndZ = -12000.0f;

		pFogTable_Adv00[5][i].u8Enable = 1;
		pFogTable_Adv00[5][i].f32StartZ = -12000.0f;	
		pFogTable_Adv00[5][i].f32EndZ = -12000.0f;
				
		pClipMap_Adv00[5][i].f32Far = -600.0f;
	}
}

void ADV00_Load()
{
	// This is done every time the function is called
	LevelLoader(LevelAndActIDs_StationSquare1, "SYSTEM\\data\\adv00_stationsquare\\landtabless00.c.sa1lvl", ADV00_TEXLISTS[0], ___LANDTABLESS[0]);
	LevelLoader(LevelAndActIDs_StationSquare2, "SYSTEM\\data\\adv00_stationsquare\\landtabless01.c.sa1lvl", ADV00_TEXLISTS[1], ___LANDTABLESS[1]);
	LevelLoader(LevelAndActIDs_StationSquare3, "SYSTEM\\data\\adv00_stationsquare\\landtabless02.c.sa1lvl", ADV00_TEXLISTS[2], ___LANDTABLESS[2]);
	LevelLoader(LevelAndActIDs_StationSquare4, "SYSTEM\\data\\adv00_stationsquare\\landtabless03.c.sa1lvl", ADV00_TEXLISTS[3], ___LANDTABLESS[3]);
	LevelLoader(LevelAndActIDs_StationSquare5, "SYSTEM\\data\\adv00_stationsquare\\landtabless04.c.sa1lvl", ADV00_TEXLISTS[4], ___LANDTABLESS[4]);
	LevelLoader(LevelAndActIDs_StationSquare6, "SYSTEM\\data\\adv00_stationsquare\\landtabless05.c.sa1lvl", ADV00_TEXLISTS[5], ___LANDTABLESS[5]);
	// This is done only once
	if (!ModelsLoaded_ADV00)
	{
		WriteCall((void*)0x0063A908, OGaitou_Hack); // Street light hack (day)
		WriteJump(LampDisp, OGaitou_Display_Night); // Street light hack (night)
		WriteJump(SSChangeTimetex, SSChangeTimetex_r); // Time of day textures
		SSCar_Display_t = new Trampoline(0x006325D0, 0x006325D5, SSCar_Display_r); // Fix animation frame overflow in cars
		WriteCall((void*)0x00632773, FixPoliceCar); // Police car hack
		WriteCall((void*)0x00638B2E, RenderPoliceCarBarricade);
		WriteCall((void*)0x00638B50, RenderPoliceCarBarricade);
		tex_color_list[5] = 7; // Set last car texture (not an actual car surface texture but it's like that in SA1 too)
		if (!CheckSADXWater(LevelIDs_StationSquare))
		{
			// Disable DX ocean callbacks
			WriteData<5>((char*)0x0062EC3C, 0x90u);
			WriteData<5>((char*)0x0062EC52, 0x90u);
		}
		// Depth hacks
		WriteCall((void*)0x00630AC7, RenderHotelDoor);
		WriteCall((void*)0x00638601, DrawCasinoDoor1);
		WriteCall((void*)0x00638640, DrawCasinoDoor2);
		WriteCall((void*)0x00638683, DrawCasinoDoor3);
		WriteCall((void*)0x0063EECF, SouvenirShopDoor_Depth);
		WriteCall((void*)0x00636DE9, RenderOfficeDoor);
		WriteCall((void*)0x00636E99, RenderOfficeDoor);
		WriteCall((void*)0x00636F0B, RenderOfficeDoor);
		WriteCall((void*)0x00636E1A, RenderOfficeDoor_Child);
		WriteCall((void*)0x00636E52, RenderOfficeDoor_Child);
		WriteCall((void*)0x00636EC0, RenderOfficeDoor_Child);
		WriteCall((void*)0x00636F32, RenderOfficeDoor_Child);
		WriteData((float*)0x00634EB9, 0.601f); // Prevent Z fighting with SS NPC shadow when overlapping transparent stuff
		// Fix camera in Light Speed Shoes cutscene
		WriteData((float*)0x00652F74, 800.0f); // Player X
		WriteData((float*)0x00652F79, -92.6f); // Player Y
		WriteData((float*)0x006532BB, 509.9f); // Start Camera X
		WriteData((float*)0x006532B6, -89.4f); // Start Camera Y
		WriteData((float*)0x006532B1, 812.3f); // Start Camera Z
		WriteData((float*)0x00652EA1, 487.0f); // End camera X
		WriteData((float*)0x00652E9C, -77.0f); // End camera Y
		WriteData((float*)0x00652E97, 837.0f); // End camera Z
		WriteCall((void*)0x006304B6, DelaySettingTimeOfDay); // Prevent sudden lighting change in Sonic's story
		WriteData<5>((void*)0x00630ADA, 0x90); // Hotel door fix 1 (don't SaveConstantAttr)
		WriteData<5>((void*)0x00630AE6, 0x90); // Hotel door fix 2 (don't add ignore light)
		WriteData<5>((void*)0x00630B03, 0x90); // Hotel door fix 3 (don't LoadConstantAddr)
		WriteJump(checkInSsHodel, checkInSsHodelLol); // Disable hotel lighting check
		WriteCall((void*)0x00639B90, OMSakuFix); // OMSaku fence fadeout fix
		WriteCall((void*)0x00639CD2, OMSakuFix); // OMSaku target fadeout fix
		WriteCall((void*)0x006372D4, OOfficeBarricade_Fix); // Fix office barricade rendering (same problem as OMSaku)
		WriteCall((void*)0x006372F8, OOfficeBarricade_Fix); // Fix office barricade rendering (same problem as OMSaku)
		WriteCall((void*)0x0063731C, OOfficeBarricade_Fix); // Fix office barricade rendering (same problem as OMSaku)
		// Material stuff
		object_sssikake_gesuielebox_gesuielebox.child->child->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_USE_ALPHA; // Speed Highway elevator door
		object_sssikake_gesuielebox_gesuielebox.child->sibling->child->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_USE_ALPHA; // Speed Highway elevator door
		ForceObjectSpecular_Object(&object_sssikake_gesuielebox_gesuielebox, false); // OHighEle (elevator in sewers)
		// Not part of the Mod Loader's material fixes
		RemoveMaterialColors(&object_cajinoobj_casinostar_casinostar);
		RemoveMaterialColors(&object_casinoobj_builg_builg);
		RemoveMaterialColors(&object_casinoobj_builh_builh);
		RemoveMaterialColors(&object_casinoobj_casinoji_casinoji);
		RemoveMaterialColors(&object_casinoobj_casinobuil_casinobuil);
		RemoveMaterialColors(&object_sssikake_cajinobutton_cajinobutton);
		RemoveMaterialColors(&object_sssikake_stationshutter_stationshutter);
		WriteCall((void*)0x006388E8, SetCasinoShutterTexture); // Correct the texture list for object_sssikake_stationshutter_stationshutter
		// Objects
		model_ssobj_poolchair_poolchair = *LoadModel("system\\data\\adv00_stationsquare\\object\\ssobj_poolchair.nja.sa1mdl")->basicdxmodel; // OPoolChair
		model_ssobj_syoukaki_syoukaki = *LoadModel("system\\data\\adv00_stationsquare\\object\\ssobj_syoukaki.nja.sa1mdl")->basicdxmodel; // Fire hydrant
		AddAlphaRejectMaterial(&object_sssikake_offbarri_offbarri.child->basicdxmodel->mats[0]); // OOfficeBarricade shadow
		object_slights_gaitou_gaitou = *LoadModel("system\\data\\adv00_stationsquare\\object\\slights_gaitou.nja.sa1mdl"); // OGaitou (street light)
		object_ssobj_shop1_dentou_d_shop1_dentou_d.basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // OS1Dnto doesn't ignore lighting on DC even though the model does
		object_sssikake_twadoor_twadoor = *LoadModel("system\\data\\adv00_stationsquare\\object\\sssikake_twadoor.nja.sa1mdl"); // OTwaDoor
		motion_sssikake_twadoor = *LoadAnimation("system\\data\\adv00_stationsquare\\object\\sssikake_twadoor.nam.saanim"); // OTwaDoor motion
		// Scale for Casino buildings (otherwise DC models clip)
		WriteData((float*)0x0063197B, 10.0f); // Night
		WriteData((float*)0x006319A5, 10.0f); // Night
		WriteData((float*)0x00577059, 10.0f); // OKazari
		WriteData((float*)0x00577077, 10.0f); // OKazari
		WriteData((float*)0x00577119, 10.0f); // OKazari child
		WriteData((float*)0x00577199, 10.0f); // OKazari child
		WriteData((float*)0x00631A25, 10.0f); // OKazari child night
		WriteData((float*)0x00631AA9, 10.0f); // OKazari child night
		WriteData((float*)0x00631B16, 10.0f); // Day
		WriteData((float*)0x00631B34, 10.0f); // Day
		WriteData((float*)0x00631B43, 10.0f); // Day
		object_stdoor_stationdoor_stationdoor = *LoadModel("system\\data\\adv00_stationsquare\\object\\stdoor_stationdoor.nja.sa1mdl"); // OStDoor
		object_sssikake_shop1door_shop1door = *LoadModel("system\\data\\adv00_stationsquare\\object\\sssikake_shop1door.nja.sa1mdl"); // Burger shop door
		object_hotelsikake_hidndoor_hidndoor = *LoadModel("system\\data\\adv00_stationsquare\\object\\hotelsikake_hidndoor.nja.sa1mdl"); // Hidden door 1
		object_sscasinosikake_hnakagen_hnakagen = *LoadModel("system\\data\\adv00_stationsquare\\object\\sscasinosikake_hnakagen.nja.sa1mdl"); // OHotelFront 1
		object_sscasinosikake_hnakagen_2_hnakagen = *CloneObject(&object_sscasinosikake_hnakagen_hnakagen); // OHotelFront 2 (duplicate?)
		object_sikake_containerbig_containerbig = *LoadModel("system\\data\\adv00_stationsquare\\object\\sikake_containerbig.nja.sa1mdl"); // Box in the sewers
		object_sskuruma2_body_body = *LoadModel("system\\data\\adv00_stationsquare\\object\\no_unite\\kuruma\\sskuruma2_body.nja.sa1mdl"); // Red Car
		object_ssrotyucar_body_body = *LoadModel("system\\data\\adv00_stationsquare\\object\\no_unite\\kuruma\\ssrotyucar_body.nja.sa1mdl"); // Blue Car
		object_sstaxi_body_body = *LoadModel("system\\data\\adv00_stationsquare\\object\\no_unite\\kuruma\\sstaxi_body.nja.sa1mdl"); // Taxi
		object_sspatcar_body_body = *LoadModel("system\\data\\adv00_stationsquare\\object\\no_unite\\kuruma\\sspatcar_body.nja.sa1mdl"); // Police
		object_sssikake_wakusei_wakusei = *LoadModel("system\\data\\adv00_stationsquare\\object\\sssikake_wakusei.nja.sa1mdl"); // OWakusei
		WriteCall((void*)0x006366C1, BigPuzzleFix);
		WriteCall((void*)0x0063951A, OWakuseiFix);
		WriteCall((void*)0x00639584, OWakuseiFix);		
		// Ice Key fix
		object_ssobj_keyice_f_keyice.evalflags |= NJD_EVAL_HIDE;
		WriteCall((void*)0x00637E34, IceKeySSFix);
		object_train_base_base = *LoadModel("system\\data\\adv00_stationsquare\\object\\no_unite\\train_base.nja.sa1mdl"); // Train
		AddWhiteDiffuseMaterial(&object_train_base_base.child->sibling->sibling->sibling->basicdxmodel->mats[9]);
		// Parasol stuff
		object_poolparasol_poolparasol_poolparasol = *LoadModel("system\\data\\adv00_stationsquare\\object\\poolparasol_poolparasol.nja.sa1mdl"); // Edited model with custom hierarchy
		WriteCall((void*)0x0063A6A4, RenderParasol);
		ModelsLoaded_ADV00 = true;
	}
}

void ADV00_OnFrame()
{
	if (ssStageNumber != LevelIDs_StationSquare)
		return;
	// Delayed time of day for Sonic's cutscene
	{
		if (ssActNumber != 2 && DelayedTimeOfDay != -1)
		{
			SetTimeOfDay(DelayedTimeOfDay);
			DelayedTimeOfDay = -1;
		}
	}
}