#pragma once

#include <SADXModLoader.h>

void RemoveMaterialColors(NJS_OBJECT* obj);

DataPointer(NJS_ACTION, action_sscasinosikake_hnakagen, 0x02AAB204);
DataPointer(NJS_MODEL_SADX, model_ssobj_poolchair_poolchair, 0x02ACBB80);
DataPointer(NJS_MODEL_SADX, model_ssobj_syoukaki_syoukaki, 0x02AC95BC);
DataPointer(NJS_MOTION, motion_sssikake_twadoor, 0x02AB5960);
DataPointer(NJS_TEXLIST, texlist_ss_casino, 0x02AFC820);

struct DaytimeTextureInfo
{
	int texids[3];
};

DaytimeTextureInfo SS00DayTexInfo[] =
{
	{ 124, 206, 207 },
	{ 145, 208, 209 },
	{ 69, 210, 211 },
	{ 184, 212, 213 },
	{ 36, 214, 215 },
	{ 39, 216, 217 },
	{ 185, 218, 219 },
	{ -1, -1, -1 },
};

DaytimeTextureInfo SS01DayTexInfo[] =
{
	{ 240, 264, 265 },
	{ -1, -1, -1 },
};

DaytimeTextureInfo SS03DayTexInfo[] =
{
	{ 60, 258, 259 },
	{ 89, 262, 263 },
	{ 94, 260, 261 },
	{ 165, 256, 257 },
	{ -1, -1, -1 },
};

DaytimeTextureInfo SS03DayTexInfo_Special = { 52, 258, 259 };

DaytimeTextureInfo SS04DayTexInfo[] =
{
	{ 69, 116, 118 },
	{ 70, 115, 117 },
	{ -1, -1, -1 },
};

DaytimeTextureInfo* SSDayTexInfo[] =
{
	SS00DayTexInfo,
	SS01DayTexInfo,
	NULL,
	SS03DayTexInfo,
	SS04DayTexInfo,
	NULL,
};