#include "stdafx.h"
#include "config.h"

// Exported variables for other mods

extern "C"
{
	__declspec(dllexport) bool EnabledLevels[43];
	__declspec(dllexport) bool EnableDCTitleScreen;
}

void UpdateExportVariables()
{
	for (int i = 0; i < 43; i++)
		EnabledLevels[i] = ModConfig::EnabledLevels[i];
	EnableDCTitleScreen = (ModConfig::EnableDCBranding && ModConfig::TitleScreenLogoMode != TitleScreenConfig::SADX);
}