#pragma once

#include <SADXModLoader.h>

DataPointer(char, DolphinsActivated, 0x03C5E444);
DataPointer(float, EC1OceanYShift, 0x010C85A8);
DataPointer(int, EffectActive, 0x03C5E4B0); // The ocean wave effect is active when this is above 0
DataArray(NJS_TEX, vuvS_0_sea_wt_2525umi_wt_2525umi, 0x10BB000, 1300);

FunctionPointer(void, Disp_132, (task* a1), 0x00501130); // Emerald Coast Act 1 water rendering function
FunctionPointer(void, Disp2_5, (task* a1), 0x004F76C0); // Emerald Coast Act 2 water rendering function
FunctionPointer(void, Disp3, (task* a1), 0x004F7760); // Emerald Coast Act 3 water rendering function