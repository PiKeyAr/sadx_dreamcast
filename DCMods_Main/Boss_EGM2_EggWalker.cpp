#include "stdafx.h"

static bool ModelsLoaded_B_EGM2;

DataArray(NJS_TEXANIM, anim_kaen, 0x1655790, 16); // Sprite animation for Egg Walker's fire attack 

void SetEggWalkerFireColor(NJS_ARGB *a1a)
{
	SetMaterial(max(0, a1a->a), max(0, a1a->r), max(0, a1a->g), max(0, a1a->b));
}

void B_EGM2_Init()
{
	ReplaceSET("SETEGM2S");
	ReplacePVM("EGM2");
	ReplacePVM("EGM2_BAKU");
	ReplacePVM("EGM2_CAR");
	ReplacePVM("EGM2_COMMON");
	ReplacePVM("EGM2_EFFECT");
	ReplacePVM("EGM2_FIRE");
	ReplacePVM("EGM2_HAMON");
	ReplacePVM("EGM2_MINE");
	ReplacePVM("EGM2_MISSILE");
	ReplacePVM("EGM2_SKY");
	ReplacePVM("EGM2_TIKEI");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Egm02[0][i].f32StartZ = -10000.0f;
		pFogTable_Egm02[0][i].f32EndZ = -10000.0f;
	}
}

void B_EGM2_Load()
{
	LevelLoader(LevelAndActIDs_EggWalker, "SYSTEM\\data\\bossegm2\\landtableegm2.c.sa1lvl", &texlist_egm2_tikei);
	if (!ModelsLoaded_B_EGM2)
	{
		object_egm02_top_top.basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR; // Seat specular
		object_egm02_top_top.child->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR; // Seat specular
		// Object fixes
		object_m2mis_m2mis_m2mis.basicdxmodel->mats[3].attrflags &= ~NJD_FLAG_USE_ALPHA; // Remove unneeded alpha in Egg Walker missile model
		WriteData<1>((char*)0x0057AB35, 0x01u); // Queue flag for Egg walker missile
		WriteCall((void*)0x0057A43A, SetEggWalkerFireColor); // Clamp Egg Walker fire sprite alpha
		// Fix fire sprite texanim to match texture order in the PVM
		anim_kaen[0].texid = 7;
		anim_kaen[7].texid = 0;
		ModelsLoaded_B_EGM2 = true;
	}
}