#include "stdafx.h"
#include "STG01_EmeraldCoast.h"

static bool ModelsLoaded_STG01;

NJS_OBJECT* HighPolyOcean_Static = nullptr;
NJS_TEX backup_vuvS_0_sea_wt_2525umi_wt_2525umi[1300];

static float WallCollisionNerf = 0.0f;
static bool SkyboxHidden = false;
static bool InsideSecretArea = false;
static bool SmallOceanActivated = false;
static NJS_VECTOR UVShiftOldPosition = { 0, 0, 0 };

// Act 1 water
void __cdecl Disp_132_r(task* a1)
{
	auto entity = playertwp[0];
	taskwk* work = a1->twp;
	int unitsize_u_small = 10;
	int unitsize_v_small = 10;
	___njFogDisable();
	if (work->counter.l != 0)
	{
		float distort = njSin(gu32LocalCnt << 11);
		EC1OceanYShift = (distort * 3.0f) + 2.5f;
	}
	else if (EC1OceanYShift > -1.5f)
		EC1OceanYShift -= 0.1f;
	float f = njSin(gu32LocalCnt << 6) * 40;
	if (!ModConfig::EnableDXOceanEffects)
	{
		f = 0;
		EC1OceanYShift = -1.5f;
	}
	if (entity != nullptr)
	{
		if (entity->pos.x > 1800)
			SmallOceanActivated = true;
		if (entity->pos.x < 1400)
			SmallOceanActivated = false;
	}
	if (!SmallOceanActivated)
	{
		if (!loop_count)
		{
			njSetTexture(&texlist_seates);
			late_z_ofs___ = -19952.0f;
			for (int x = -1; x < 2; x++)
			{
				for (int z = -1; z < 2; z++)
				{
					njPushMatrix(0);
					njTranslate(0, f + x * 3951.0f, EC1OceanYShift, f + z * 3951.0f);
					late_DrawObjectClip(&object_sea_wt_60x04nbg_wt_60x04nbg, LATE_LIG, 1.0f);
					njPopMatrix(1u);
				}
			}
			late_z_ofs___ = 0;
		}
	}
	else
	{
		if (UVShiftOldPosition.x != work->pos.x)
		{
			int u2_add = int(255.0f * (work->pos.x - UVShiftOldPosition.x) / (float)unitsize_u_small) % 255;
			for (int u_step = 0; u_step < 1300; u_step++)
			{
				vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u = vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u - u2_add;
				int u2_delta = vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u - backup_vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u;
				vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u = backup_vuvS_0_sea_wt_2525umi_wt_2525umi[u_step].u + (u2_delta % 255);
			}
		}
		if (UVShiftOldPosition.z != work->pos.z)
		{
			int v2_add = int(255.0f * (work->pos.z - UVShiftOldPosition.z) / (float)unitsize_v_small) % 255;
			for (int v_step = 0; v_step < 1300; v_step++)
			{
				vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v = vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v - v2_add;
				int v2_delta = vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v - backup_vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v;
				vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v = backup_vuvS_0_sea_wt_2525umi_wt_2525umi[v_step].v + (v2_delta % 255);
			}
		}
		UVShiftOldPosition.x = work->pos.x;
		UVShiftOldPosition.z = work->pos.z;
		if (!loop_count)
		{
			njSetTexture(&texlist_seates); // BEACH_SEA
			njPushMatrix(0);
			njTranslate(0, work->pos.x + f, EC1OceanYShift, work->pos.z + f);
			late_z_ofs___ = -19952.0f;
			LATE flag = (EffectActive != 0 || EC1OceanYShift != -1.5f) ? LATE_WZ : LATE_LIG;
			late_DrawObjectMesh(&object_sea_wt_2525umi_wt_2525umi, flag);
			njPopMatrix(1u);

			for (int x = -3; x < 3; x++)
			{
				for (int z = -2; z < 3; z++)
				{
					if (x == 0 && z == 0)
						continue;
					njPushMatrix(0);
					njSetTexture(&texlist_seates); // BEACH_SEA
					njTranslate(0, work->pos.x + 1000 * x + f, EC1OceanYShift, work->pos.z + 1000.0f * z + f);
					late_DrawObjectClip(HighPolyOcean_Static, LATE_NO, 1.0f);
					njPopMatrix(1u);
				}
			}

			late_z_ofs___ = 0;
		}
	}
}

// Act 2/3 water
void __cdecl Disp2_5_r(task* a1)
{
	taskwk* work = a1->twp;
	float f = ModConfig::EnableDXOceanEffects ? njSin(gu32LocalCnt << 6) * 40 : 0;
	// Draw SA1 ocean Acts 2 and 3
	njSetTexture(&texlist_seates);
	late_z_ofs___ = ssActNumber == 1 ? -25952.0f : -7000.0f;
	for (int x = -2; x < 3; x++)
	{
		for (int z = -2; z < 3; z++)
		{
			njPushMatrix(0);
			njTranslate(0, f + x * 3951.0f, EC1OceanYShift, f + z * 3951.0f);
			late_DrawObjectClip(&object_sea_wt_60x04nbg_wt_60x04nbg, LATE_LIG, 1.0f);
			njPopMatrix(1u);
		}
	}
	late_z_ofs___ = 0;
}

void WhaleSplash(NJS_OBJECT *a1)
{
	late_z_ofs___ = 8000.0f;
	late_DrawObjectClip(a1, LATE_MAT, 1.0f);
}

void PalmFix1(NJS_OBJECT *a1, LATE a2, float a3)
{
	if (IsCameraUnderwater)	
		late_z_ofs___ = -18952.0f;
	if (a1 == &object_seaobj_kusa6_kusa6)
		late_DrawObjectClipMesh(a1, a2, a3); // OGrasC
	else 
		late_DrawObjectClipEx(a1, a2, a3);
	if (IsCameraUnderwater) 
		late_z_ofs___ = 0.0f;
}

void PalmFix2(NJS_MODEL_SADX *model, LATE blend, float scale)
{
	if (IsCameraUnderwater)	
		late_z_ofs___ = -18952.0f;
	late_DrawModelClip(model, blend, scale);
	if (IsCameraUnderwater)	
		late_z_ofs___ = 0.0f;
}

void EmeraldCoast_Init()
{
	// Store default UVs for the dynamic ocean
	for (int i = 0; i < 1300; i++)
	{
		backup_vuvS_0_sea_wt_2525umi_wt_2525umi[i].u = vuvS_0_sea_wt_2525umi_wt_2525umi[i].u;
		backup_vuvS_0_sea_wt_2525umi_wt_2525umi[i].v = vuvS_0_sea_wt_2525umi_wt_2525umi[i].v;
	}
	if (!ModConfig::IamStupidAndIWantFuckedUpOcean)
	{
		ReplaceCAM("CAM0100E");
		ReplaceCAM("CAM0100S");
		ReplaceCAM("CAM0101S");
		ReplaceCAM("CAM0102B");
		ReplaceCAM("CAM0102S");
	}
	else
	{
		ReplaceGeneric("CAM0100E.BIN", "CAM0100E_R.BIN");
		ReplaceGeneric("CAM0100S.BIN", "CAM0100S_R.BIN");
		ReplaceGeneric("CAM0101S.BIN", "CAM0101S_R.BIN");
		ReplaceGeneric("CAM0102S.BIN", "CAM0102S_R.BIN");
		ReplaceGeneric("CAM0102B.BIN", "CAM0102B_R.BIN");
	}
	ReplaceSET("SET0100E");
	ReplaceSET("SET0100S");
	ReplaceSET("SET0101M");
	ReplaceSET("SET0101S");
	ReplaceSET("SET0102B");
	ReplaceSET("SET0102S");	
	ReplacePVM("BEACH01");
	ReplacePVM("BEACH02");
	ReplacePVM("BEACH03");
	ReplacePVM("BG_BEACH");
	ReplacePVM("OBJ_BEACH");
	ReplacePVM("BEACH_SEA");
	ReplacePVR("AM_SEA124_8"); // Dolphins related
	ReplacePVR("WATERCOLUMN01"); // Dolphins related
	WriteData<1>((char*)0x004F68E0, 0xC3u); // Disable SetClip_ECoast1
	if (!ModConfig::IamStupidAndIWantFuckedUpOcean)
	{
		for (int i = 0; i < 3; i++)
		{
			pScale_Stg01[0][i].x = 1.0f;
			pScale_Stg01[0][i].y = 1.0f;
			pScale_Stg01[0][i].z = 1.0f;
			pScale_Stg01[1][i].x = 1.0f;
			pScale_Stg01[1][i].y = 1.0f;
			pScale_Stg01[1][i].z = 1.0f;
			pScale_Stg01[2][i].x = 1.0f;
			pScale_Stg01[2][i].y = 1.0f;
			pScale_Stg01[2][i].z = 1.0f;
			pClipMap_Stg01[0][i].f32Far = -6000.0f;
			pClipMap_Stg01[1][i].f32Far = -3900.0f;
			pFogTable_Stg01[0][i].f32EndZ = -12000.0f;
			pFogTable_Stg01[0][i].f32StartZ = -12000.0f;
			pFogTable_Stg01[1][i].f32EndZ = -12000.0f;
			pFogTable_Stg01[1][i].f32StartZ = -12000.0f;
		}
	}
}

void EmeraldCoast_Load()
{
	// Landtables
	LevelLoader(LevelAndActIDs_EmeraldCoast1, "SYSTEM\\data\\stg01_beach\\landtable0100.c.sa1lvl", &texlist_beach01);
	LevelLoader(LevelAndActIDs_EmeraldCoast2, "SYSTEM\\data\\stg01_beach\\landtable0101.c.sa1lvl", &texlist_beach02);
	LevelLoader(LevelAndActIDs_EmeraldCoast3, "SYSTEM\\data\\stg01_beach\\landtable0102.c.sa1lvl", &texlist_beach03);
	if (!ModelsLoaded_STG01)
	{
		if (ModConfig::SETReplacement > SetFileConfig::Optimized) // Japan/International
		{
			WriteData((float**)0x004D46C8, &WallCollisionNerf);
			WriteData((float**)0x004D46D7, &WallCollisionNerf);
			WriteData((float**)0x004D46E3, &WallCollisionNerf);
		}
		// Ocean models
		if (!CheckSADXWater(LevelIDs_EmeraldCoast))
		{
			object_sea_wt_2525umi_wt_2525umi.basicdxmodel->mats[0].diffuse.color = 0x99FFFFFF;
			object_sea_wt_60x04nbg_wt_60x04nbg.basicdxmodel->mats[0].diffuse.color = 0x99FFFFFF;
			HighPolyOcean_Static = CloneObject(&object_sea_wt_2525umi_wt_2525umi);
			HighPolyOcean_Static->basicdxmodel->mats = object_sea_wt_2525umi_wt_2525umi.basicdxmodel->mats;
			AddTextureAnimation_Permanent(1, 0, &object_sea_wt_60x04nbg_wt_60x04nbg.basicdxmodel->mats[0], false, 4, 0, 9); // Static ocean in Act 1
			AddTextureAnimation_Permanent(1, 0, &object_sea_wt_2525umi_wt_2525umi.basicdxmodel->mats[0], false, 4, 0, 9); // Dynamic ocean
			// Write water rendering functions
			WriteJump(Disp_132, Disp_132_r); // Act 1
			WriteJump(Disp2_5, Disp2_5_r); // Act 2
			WriteJump(Disp3, Disp2_5_r); // Act 3
		}
		// Models
		object_seaobj_dolphin_dolphin = *LoadModel("system\\data\\stg01_beach\\common\\models\\seaobj_dolphin.nja.sa1mdl"); // Dolphin
		object_seaobj_oruka_oruka = *LoadModel("system\\data\\stg01_beach\\common\\models\\seaobj_oruka.nja.sa1mdl"); // Whale
		object_seaobj_yashi04_yashi04 = *LoadModel("system\\data\\stg01_beach\\common\\models\\seaobj_yashi04.nja.sa1mdl"); // Palm tree (sorted)
		// Palm trees and other objects underwater transparency fixes
		WriteCall((void*)0x00500D66, PalmFix1);
		WriteCall((void*)0x004FD233, PalmFix1); // Works for OBKusa too
		WriteCall((void*)0x0049CD32, PalmFix1); // OGrasC and many other objects probably
		WriteCall((void*)0x00500CB2, PalmFix1); // Yasi3
		WriteCall((void*)0x00500DCA, PalmFix2); // Yasi0_Display2
		WriteCall((void*)0x00500E47, PalmFix2); // Yasi0_Display2
		// Whale splash transparency fixes
		WriteCall((void*)0x00502F8F, WhaleSplash);
		WriteCall((void*)0x00502F9A, WhaleSplash);
		for (int i = 0; i < 3; i++)
		{
			pClipMap_Stg01[2][i].f32Far = -4000.0f;
			pFogTable_Stg01[2][i].u8Enable = 0;
			pFogTable_Stg01[2][i].f32StartZ = -1200.0f;
			pFogTable_Stg01[2][i].f32EndZ = -3000.0f;
			pFogTable_Stg01[2][i].Col = 0xFFFFFFFF;
		}
		// Alpha rejection
		if (ModConfig::DLLLoaded_Lantern)
		{
			AddAlphaRejectMaterial(&object_seaobj_kage_kage.basicdxmodel->mats[0]); // Spike gate shadow
		}
		ModelsLoaded_STG01 = true;
	}
}

void EmeraldCoast_OnFrame()
{
	if (ssStageNumber != LevelIDs_EmeraldCoast)
		return;
	// Restore ocean UVs and reset fog on level exit/restart
	switch (ssGameMode)
	{
	case MD_GAME_REINIT2:
	case MD_GAME_FADEIN:
	case MD_GAME_FADEOUT_MISS_RESTART:
	case MD_GAME_STAGENAME_INIT:
		DolphinsActivated = 0;
		InsideSecretArea = false;
		break;
	}
	// Reset dolphin status
	if (playertwp[0] != nullptr && ssActNumber == 1 && playertwp[0]->pos.x < 4000)
		DolphinsActivated = 0;
	// Hide skybox bottom in Act 3
	if (IsLevelLoaded(LevelAndActIDs_EmeraldCoast3) && !ModConfig::IamStupidAndIWantFuckedUpOcean)
	{
		if (ssActNumber == 2 && camera_twp != nullptr)
		{
			if (!SkyboxHidden && camera_twp->pos.y < 50)
			{
				object_sea_nbg3_nbg3.evalflags |= NJD_EVAL_HIDE;
				SkyboxHidden = true;
			}
			if (SkyboxHidden && camera_twp->pos.y >= 50)
			{
				object_sea_nbg3_nbg3.evalflags &= ~NJD_EVAL_HIDE;
				SkyboxHidden = false;
			}
		}
	}
	// Fog stuff in Act 3
	if (ssActNumber == 2 && !ChkPause())
	{
		if (camera_twp != nullptr && camera_twp->pos.z > 2000)
		{
			InsideSecretArea = true;
			gFog.u8Enable = 1;
			if (gFog.f32StartZ < -21) 
				gFog.f32StartZ += 20;
		}
		else
		{
			if (gFog.f32StartZ > -1200) 
				gFog.f32StartZ -= 20;
			if (gFog.f32StartZ <= -1200 && gFog.u8Enable)
			{
				InsideSecretArea = false;
				gFog.u8Enable = 0;
			}
		}
	}
}