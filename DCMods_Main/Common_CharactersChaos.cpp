#include "stdafx.h"

// TODO: Rewrite with symbol stuff

DataPointer(BUBBLE_LIST*, ChaosBubbleEffect_BubbleList, 0x3D0DC00);
DataPointer(float, min_fluctuation, 0x3D0DB90);
DataPointer(NJS_SPRITE, ChaosBubbleEffect_Sprite, 0x3D0DB9C);
DataPointer(NJS_ARGB, ChaosBubbleEffect_Color, 0x38E200C);
DataPointer(NJS_MATRIX_PTR, bubble_matrix, 0x3D0DB88);
DataPointer(task, ChaosBubbleEffect_ObjectMaster, 0x3D0DB94);
DataPointer(NJS_VECTOR, Chaos4Position, 0x3C5A358);
DataArray(float, ChaosBubbleEffect_ScaleOffsets, 0x38E2020, 16);
DataArray(char, ChaosBubbleEffect_StateArray, 0x3D0DBC0, 64);

void DrawChaosBubbles(NJS_SPRITE* sp, Int n, NJD_SPRITE attr, LATE zfunc_type)
{
	if (ssStageNumber == LevelIDs_Chaos4)
	{
		// Camera below water - Chaos above water
		if (camera_twp->pos.y <= 0) late_z_ofs___ = 7500.0f;
		if (camera_twp->pos.y > 0)
		{
			// Camera above water and Chaos above water - Chaos above water
			if (Chaos4Position.y >= 15) late_z_ofs___ = 7500.0f;
			// Camera above water and Chaos below water - Chaos below water
			else late_z_ofs___ = -20500.0f;
		}
		late_DrawSprite3D(sp, n, attr, zfunc_type);
	}
	else
	{
		late_z_ofs___ = -1.5f;
		late_DrawSprite3D(sp, n, attr, zfunc_type);
	}
}

void actBubble(task* a1)
{
	BUBBLE* v1; // esi
	short bubble_id; // edi
	int spline_id;
	float ampl; // ST1C_4
	float spiral_sine; // ST1C_4
	float spiral_cosine; // st7
	float scale_change; // st6
	float scale_swing; // [esp+1Ch] [ebp+4h]
	float f3;
	float f8;
	float f7;
	float f4;
	NJS_VECTOR v; // [esp+Ch] [ebp-Ch]
	int tmax; // [esp+1Ch] [ebp+4h]
	v1 = (BUBBLE*)a1->awp;
	bubble_id = ChaosBubbleEffect_BubbleList[v1->id].id;
	switch (v1->mode)
	{
	case 0:
		v1->t = 0.15f * 0.000030517578f * (float)rand() - 0.5f;
		ampl = fabs(v1->spline[ChaosBubbleEffect_BubbleList[v1->id].num - 1].y - v1->spline[0].y + v1->spline[ChaosBubbleEffect_BubbleList[v1->id].num - 1].x - v1->spline[0].x);
		v1->tadd = ((0.18f * 0.000030517578f * (float)rand() + 0.08f) / ampl) * min_fluctuation;
		tmax = ChaosBubbleEffect_BubbleList[v1->id].num - 3;
		v1->timer = 15;
		v1->tmax = (float)tmax + 1.25f;
		v1->spiral_add = (int)(182.0f * 30.0f * (0.8f * 0.000030517578f * (float)rand() + 0.2f));
		v1->swing = (float)rand() * 0.000030517578f * 0.1f + 0.05f;
		v1->size = ChaosBubbleEffect_ScaleOffsets[int(rand() * 0.000030517578f * 16.0f)];
		v1->scale_x = 0.1f;
		v1->mode = 1;
		goto swing;
	case 1:
	swing:
		v1->timer--;
		v1->t += v1->tadd;
		v1->scale_x = v1->scale_x + 0.06f;
		if (!v1->timer)
		{
			v1->mode = 2;
		}
		goto render;
	case 2:
		v1->t += v1->tadd;
		if (v1->t >= v1->tmax)
		{
			v1->timer = 8;
			v1->mode = 3;
		}
		v1->spiral += v1->spiral_add;
		v1->scale_x = 0.85f;
		goto render;
	case 3:
		v1->timer--;
		v1->scale_x -= 0.1125f;
		if (v1->timer)
		{
			goto render;
		}
		FreeTask(a1);
		break;
	default:
	render:
		if (!loop_count && !((_DWORD)ChaosBubbleEffect_ObjectMaster.next[1].exec & 4) && !ChaosBubbleEffect_StateArray[bubble_id])
		{
			spline_id = (int)(v1->t + 0.5f);
			f8 = v1->t - (float)spline_id;
			f7 = 1.0f - f8;
			f3 = f8 * f8;
			f4 = f7 * f7;
			f7 = f7 * f8 + 0.5f;
			f8 = 0.5f * f3;
			f3 = 0.5f * f4;
			v.x = v1->spline[spline_id].x * f8 + v1->spline[spline_id].x * f3 + v1->spline[spline_id + 1].x * f7;
			v.y = v1->spline[spline_id].y * f8 + v1->spline[spline_id].y * f3 + v1->spline[spline_id + 1].y * f7;
			v.z = v1->spline[spline_id].z * f8 + v1->spline[spline_id].z * f3 + v1->spline[spline_id + 1].z * f7;
			scale_swing = v1->swing * v1->scale_x;
			spiral_sine = njSin(v1->spiral);
			spiral_cosine = njCos(v1->spiral);
			v.x = spiral_sine * scale_swing + v.x;
			v.z = scale_swing * spiral_cosine + v.z;
			scale_change = min_fluctuation * v1->size;
			ChaosBubbleEffect_Sprite.sx = (spiral_sine * 0.3f + v1->scale_x) * scale_change;
			ChaosBubbleEffect_Sprite.sy = (spiral_cosine * 0.3f + v1->scale_x) * scale_change;
			njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
			njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
			SetMaterialAndSpriteColor(&ChaosBubbleEffect_Color);
			njPushMatrix((NJS_MATRIX_PTR)bubble_matrix + 16 * ChaosBubbleEffect_BubbleList[v1->id].id);
			njTranslateV(0, &v);
			DrawChaosBubbles(&ChaosBubbleEffect_Sprite, 0, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE | NJD_SPRITE_COLOR, LATE_LIG);
			njPopMatrix(1u);
			ResetMaterial();
			njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
			njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		}
		break;
	}
}

void ChaosFixes_Init()
{
	ReplacePVM("CHAOS_BRAINFRAME");
	ReplacePVM("CHAOS_EFFECT");
	ReplacePVM("CHAOS_SURFACE");
	ReplacePVM("CHAOS_EMERALD");
	ReplacePVM("EFF_C7_BUBBLE");
	// Chaos bubbles fix
	WriteJump((void*)0x007ADD30, actBubble);
	((NJS_MATERIAL*)0x02D64FD8)->exponent = 11; // Chaos 1/4 puddle
	((NJS_MATERIAL*)0x038D936C)->attrflags &= ~NJD_FLAG_USE_ENV; // Chaos 0/2/6 puddle
	((NJS_MATERIAL*)0x038D936C)->exponent = 11; // Chaos 0/2/6 puddle
}