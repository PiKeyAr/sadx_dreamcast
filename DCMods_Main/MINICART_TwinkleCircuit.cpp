#include "stdafx.h"
#include "MINICART_TwinkleCircuit.h"

void TwinkleCircuit_Init()
{
	ReplaceSET("SETMCART00S");
	ReplaceSET("SETMCART01S");
	ReplaceSET("SETMCART02S");
	ReplaceSET("SETMCART03S");
	ReplaceSET("SETMCART04S");
	ReplaceSET("SETMCART05S");
	ReplaceCAM("CAMMCART00S");
	ReplaceCAM("CAMMCART01S");
	ReplaceCAM("CAMMCART02S");
	ReplaceCAM("CAMMCART03S");
	ReplaceCAM("CAMMCART04S");
	ReplaceCAM("CAMMCART05S");
	ReplacePVM("MINI_CART01");
	ReplacePVM("MINI_CART02");
	ReplacePVM("MINI_CART03");
	ReplacePVM("MINI_CART04");
	ReplacePVM("MINI_CART05");
	ReplacePVM("MINI_CART06");
	ReplacePVM("OBJ_MINI_CART");
	if (ModConfig::DLLLoaded_Lantern)
	{
		for (int i = 0; i < 8; i++)
			AddWhiteDiffuseMaterial(&object_motion_min_robo_min_robo.child->basicdxmodel->mats[i]);
	}
}

void TwinkleCircuit_Load()
{
	ShareObj_Load();
	LevelLoader(LevelAndActIDs_TwinkleCircuit1, "SYSTEM\\data\\mini_cart\\landtablemcart00.c.sa1lvl", &texlist_mini_cart01); // Landtables are 0 based but texlists are 1 based. Not confusing at all.
	LevelLoader(LevelAndActIDs_TwinkleCircuit2, "SYSTEM\\data\\mini_cart\\landtablemcart01.c.sa1lvl", &texlist_mini_cart02);
	LevelLoader(LevelAndActIDs_TwinkleCircuit3, "SYSTEM\\data\\mini_cart\\landtablemcart02.c.sa1lvl", &texlist_mini_cart03);
	LevelLoader(LevelAndActIDs_TwinkleCircuit4, "SYSTEM\\data\\mini_cart\\landtablemcart03.c.sa1lvl", &texlist_mini_cart04);
	LevelLoader(LevelAndActIDs_TwinkleCircuit5, "SYSTEM\\data\\mini_cart\\landtablemcart04.c.sa1lvl", &texlist_mini_cart05);
	LevelLoader(LevelAndActIDs_TwinkleCircuit6, "SYSTEM\\data\\mini_cart\\landtablemcart05.c.sa1lvl", &texlist_mini_cart06);
}