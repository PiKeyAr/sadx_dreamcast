#pragma once

#include <SADXModLoader.h>

void BackupDebugFontSettings();
void RestoreDebugFontSettings();
void late_DrawSprite2D_Point(NJS_SPRITE* sp, Int n, Float pri, NJD_SPRITE attr, LATE queue_flags);

DataArray(char, NowSavingString, 0x007DCBF4, 13);