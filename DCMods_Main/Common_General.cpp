#include "stdafx.h"
#include "Common_General.h"

// TODO: Figure out the black flash before the Chaos 6 cutscene in Sonic's story / SeqChangeStage call at 0052E832

bool DisableAlphaRejection = false;
bool IsCameraUnderwater = false; // True if the underwater overlay is drawn
static NJS_COLOR GlobalColorsBK[4]; // Global background color backup
bool GlobalColor_Wait = false; // If 0, global color will be restored from GlobalColorsBK on next frame; -1 is default

static Trampoline* CameraFilter_t = nullptr;
static void __cdecl CameraFilter_r()
{
	const auto original = TARGET_DYNAMIC(CameraFilter);
	IsCameraUnderwater = false;
	original();
}

// Original version of njDrawPolygon2D. The Mod Loader trampolines it with a scaled version so I can't call it directly.
void njDrawPolygon2D_o(NJS_POINT2COL* p, Int n, Float pri, NJD_DRAW attr)
{
	NJS_POINT2COL points; // [esp+0h] [ebp-10h] BYREF
	float depth; // [esp+1Ch] [ebp+Ch]

	points.p = p->p;
	points.col = p->col;
	points.tex = attr & NJD_USE_TEXTURE ? p->tex : NULL;
	points.num = n;
	if (n > 2)
	{
		depth = -1.0f / pri;
		if (depth > 1.0f)
		{
			depth = 1.0f;
		}
		gjTrans2D(attr & NJD_TRANSPARENT);
		stResetTexture();
		gjRender2DSet();
		stDrawTriangleFan2D(&points, depth);
		gjRender2DReset();
	}
}

void RestoreGlobalColor()
{
	___njSetBackColor(GlobalColorsBK[0].color, GlobalColorsBK[1].color, GlobalColorsBK[2].color);
	GlobalColor_Wait = false;
}

void __cdecl DrawUnderwaterOverlay(NJS_MATRIX_PTR m)
{
	njPushMatrix(0);
	IsCameraUnderwater = true;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	late_z_ofs___ = 22041.496f;
	late_DrawBoxFill2D(0.0f, 0.0f, (float)HorizontalResolution, (float)VerticalResolution, 0.01f, 0x40000040, LATE_LIG);
	late_DrawBoxFill2D(0.0f, 0.0f, (float)HorizontalResolution, (float)VerticalResolution, 0.01f, 0x40000040, LATE_LIG);
	late_z_ofs___ = 0;
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

static void __declspec(naked) CameraFilterHook_asm()
{
	__asm
	{
		push ecx
		call DrawUnderwaterOverlay
		pop ecx
		retn
	}
}

void DrawMotionGroundCallHook(NJS_ACTION* action, float frame, LATE flags, float scale)
{
	if (ModConfig::EnabledLevels[LevelIDs_MysticRuins] && ssStageNumber == LevelIDs_MysticRuins && ssActNumber == 2)
	{
		late_z_ofs___ = -20000.0f;
		if (action == GetLevelLandtable(LevelIDs_MysticRuins, 2)->pMotLandEntry[0].pMotion)
			late_ActionClipMS(action, frame, LATE_WZ, 1.0f);
		else
		{
			late_z_ofs___ = -19000.0f;
			late_ActionClipMesh(action, frame, LATE_MAT, 1.0f);
		}
		late_z_ofs___ = 0.0f;
	}
	else if (ModConfig::EnabledLevels[LevelIDs_FinalEgg] && ssStageNumber == LevelIDs_FinalEgg && ssActNumber == 2)
	{
		late_z_ofs___ = -47952.0f;
		late_ActionClipEx(action, frame, LATE_MAT, 1.0);
		late_z_ofs___ = 0.0f;
	}
	else
		late_ActionClipEx(action, frame, LATE_MAT, 1.0);
}

// Fix for global color flashing before/after cutscenes
static Trampoline* njSetBackColor_t = nullptr;
static void __cdecl njSetBackColor_r(Uint32 color1, Uint32 color2, Uint32 color3)
{
	const auto original = TARGET_DYNAMIC(njSetBackColor);
	if (movie_num != 0 || ssGameMode == MD_SUMMARY_INIT)
		original(0, 0, 0);
	else if (usFadeLevel < 200 || coloroffset.color == 0xFFFFFFFF)
	{
		original(color1, color2, color3);
	}
	else
	{
		original(0, 0, 0);
		GlobalColorsBK[0].color = color1 | 0xFF000000;
		GlobalColorsBK[1].color = color2 | 0xFF000000;
		GlobalColorsBK[2].color = color3 | 0xFF000000;
		GlobalColorsBK[3].color = color3 | 0xFF000000;
		GlobalColor_Wait = true;
	}
}

// Manage alpha rejection (true to disable)
void SetDisableAlphaRejection(bool disable)
{
	WriteData<1>((char*)0x007919CD, disable ? 0i8 : 0x16u);
	DisableAlphaRejection = disable;
}

void General_Init()
{		
	// Landtable animation hook for depth hacks
	WriteCall((void*)0x0043A85F, DrawMotionGroundCallHook);
	// Underwater overlay
	WriteCall((void*)0x0043708D, CameraFilterHook_asm);
	// Reset camera underwater status
	CameraFilter_t = new Trampoline(0x00436CD0, 0x00436CD6, CameraFilter_r);
	// Fix global colors flashing at the start of some cutscenes
	njSetBackColor_t = new Trampoline(0x00402F10, 0x00402F18, njSetBackColor_r);
	// Make environment maps "flipped" like on DC
	ScaleEnvironmentMap(0.5f, 0.5f, 0.5f, 0.5f);
}

void General_OnFrame()
{
	// Global color/screen fade fix
	if (GlobalColor_Wait && usFadeLevel < 200)
		RestoreGlobalColor();
	if (ssGameMode == MD_SUMMARY_INIT) // Before playing an FMV
		___njSetBackColor(0, 0, 0);
	// Alpha rejection
	if (ModConfig::DLLLoaded_Lantern)
	{
		if (!DisableAlphaRejection)
		{
			if (ulGlobalMode != MD_TITLE2_INIT && ulGlobalMode != MD_TITLE2 && ChaoStageNumber != CHAO_STG_ENTRANCE)
				SetDisableAlphaRejection(true);
		}
		else if (ulGlobalMode == MD_TITLE2_INIT || ulGlobalMode == MD_TITLE2 || ChaoStageNumber == CHAO_STG_ENTRANCE)
		{
			SetDisableAlphaRejection(false);
		}
	}
}