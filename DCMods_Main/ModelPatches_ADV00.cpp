#include "stdafx.h"

void PatchModels_ADV00()
{
	// dxpc\adv00_stationsquare\object\hotelsikake_hidndoor.nja.sa1mdl
	*(NJS_TEX*)0x02AD4D50 = { 255, -255 };
	*(NJS_TEX*)0x02AD4D54 = { 0, -255 };
	*(NJS_TEX*)0x02AD4D58 = { 255, 255 };
	*(NJS_TEX*)0x02AD4D5C = { 0, 255 };
	*(NJS_TEX*)0x02AD4D60 = { 0, -255 };
	*(NJS_TEX*)0x02AD4D64 = { 255, -255 };
	*(NJS_TEX*)0x02AD4D68 = { 0, 255 };
	*(NJS_TEX*)0x02AD4D6C = { 255, 255 };
	*(NJS_TEX*)0x02AD4B8C = { 255, -255 };
	*(NJS_TEX*)0x02AD4B90 = { 0, -255 };
	*(NJS_TEX*)0x02AD4B94 = { 255, 255 };
	*(NJS_TEX*)0x02AD4B98 = { 0, 255 };
	*(NJS_TEX*)0x02AD4B9C = { 0, -255 };
	*(NJS_TEX*)0x02AD4BA0 = { 255, -255 };
	*(NJS_TEX*)0x02AD4BA4 = { 0, 255 };
	*(NJS_TEX*)0x02AD4BA8 = { 255, 255 };

	// dxpc\adv00_stationsquare\object\poolparasol_poolparasol.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\sikake_containerbig.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\slights_gaitou.nja.sa1mdl
		//Custom edits

	// dxpc\adv00_stationsquare\object\sscasinosikake_hnakagen.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\ssobj_keyice.nja.sa1mdl
	*(NJS_TEX*)0x02AD3B24 = { 510, 254 };
	*(NJS_TEX*)0x02AD3B28 = { 0, -254 };
	*(NJS_TEX*)0x02AD3B2C = { 509, -255 };

	// dxpc\adv00_stationsquare\object\ssobj_poolchair.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\ssobj_syoukaki.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\sssikake_gesuielebox.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\sssikake_hotelele.nja.sa1mdl
	*(NJS_TEX*)0x02AD60D8 = { 510, -765 };
	*(NJS_TEX*)0x02AD60DC = { 510, 255 };
	*(NJS_TEX*)0x02AD60E0 = { 274, -765 };
	*(NJS_TEX*)0x02AD60E4 = { 274, 255 };
	*(NJS_TEX*)0x02AD60E8 = { 244, -765 };
	*(NJS_TEX*)0x02AD60EC = { 244, 255 };
	*(NJS_TEX*)0x02AD60F0 = { 0, -765 };
	*(NJS_TEX*)0x02AD60F4 = { 0, 255 };
	*(NJS_TEX*)0x02AD5AF8 = { 0, -692 };
	*(NJS_TEX*)0x02AD5AFC = { 0, -332 };
	*(NJS_TEX*)0x02AD5B00 = { 49, -692 };
	*(NJS_TEX*)0x02AD5B04 = { 49, -332 };
	*(NJS_TEX*)0x02AD5B08 = { 0, -332 };
	*(NJS_TEX*)0x02AD5B0C = { 0, -692 };
	*(NJS_TEX*)0x02AD5B10 = { 49, -332 };
	*(NJS_TEX*)0x02AD5B14 = { 49, -692 };
	*(NJS_TEX*)0x02AD5B18 = { 234, -332 };
	*(NJS_TEX*)0x02AD5B1C = { 234, -692 };
	*(NJS_TEX*)0x02AD5B20 = { 185, -332 };
	*(NJS_TEX*)0x02AD5B24 = { 185, -692 };
	*(NJS_TEX*)0x02AD5B28 = { 255, -692 };
	*(NJS_TEX*)0x02AD5B2C = { 255, -332 };
	*(NJS_TEX*)0x02AD5B30 = { 185, -692 };
	*(NJS_TEX*)0x02AD5B34 = { 185, -332 };
	*(NJS_TEX*)0x02AD5B38 = { 234, -332 };
	*(NJS_TEX*)0x02AD5B3C = { 0, -332 };
	*(NJS_TEX*)0x02AD5B40 = { 234, 253 };
	*(NJS_TEX*)0x02AD5B44 = { 0, 253 };
	*(NJS_TEX*)0x02AD5B48 = { 0, -332 };
	*(NJS_TEX*)0x02AD5B4C = { 255, -332 };
	*(NJS_TEX*)0x02AD5B50 = { 0, 260 };
	*(NJS_TEX*)0x02AD5B54 = { 255, 260 };
	*(NJS_TEX*)0x02AD5B58 = { 234, -692 };
	*(NJS_TEX*)0x02AD5B5C = { 234, -761 };
	*(NJS_TEX*)0x02AD5B60 = { 0, -692 };
	*(NJS_TEX*)0x02AD5B64 = { 0, -761 };
	*(NJS_TEX*)0x02AD5B68 = { 255, 253 };
	*(NJS_TEX*)0x02AD5B6C = { 255, -763 };
	*(NJS_TEX*)0x02AD5B70 = { 234, 253 };
	*(NJS_TEX*)0x02AD5B74 = { 234, -763 };
	*(NJS_TEX*)0x02AD5B78 = { 0, -692 };
	*(NJS_TEX*)0x02AD5B7C = { 0, -770 };
	*(NJS_TEX*)0x02AD5B80 = { 255, -692 };
	*(NJS_TEX*)0x02AD5B84 = { 255, -770 };
	*(NJS_TEX*)0x02AD57C8 = { 0, 255 };
	*(NJS_TEX*)0x02AD57CC = { 0, -765 };
	*(NJS_TEX*)0x02AD57D0 = { 235, 255 };
	*(NJS_TEX*)0x02AD57D4 = { 235, -765 };
	*(NJS_TEX*)0x02AD57D8 = { 265, 255 };
	*(NJS_TEX*)0x02AD57DC = { 265, -765 };
	*(NJS_TEX*)0x02AD57E0 = { 510, 255 };
	*(NJS_TEX*)0x02AD57E4 = { 510, -765 };
	*(NJS_TEX*)0x02AD51E8 = { 49, -692 };
	*(NJS_TEX*)0x02AD51EC = { 49, -332 };
	*(NJS_TEX*)0x02AD51F0 = { 0, -692 };
	*(NJS_TEX*)0x02AD51F4 = { 0, -332 };
	*(NJS_TEX*)0x02AD51F8 = { 49, -332 };
	*(NJS_TEX*)0x02AD51FC = { 49, -692 };
	*(NJS_TEX*)0x02AD5200 = { 0, -332 };
	*(NJS_TEX*)0x02AD5204 = { 0, -692 };
	*(NJS_TEX*)0x02AD5208 = { 185, -332 };
	*(NJS_TEX*)0x02AD520C = { 185, -692 };
	*(NJS_TEX*)0x02AD5210 = { 234, -332 };
	*(NJS_TEX*)0x02AD5214 = { 234, -692 };
	*(NJS_TEX*)0x02AD5218 = { 185, -692 };
	*(NJS_TEX*)0x02AD521C = { 185, -332 };
	*(NJS_TEX*)0x02AD5220 = { 255, -692 };
	*(NJS_TEX*)0x02AD5224 = { 255, -332 };
	*(NJS_TEX*)0x02AD5228 = { 234, 253 };
	*(NJS_TEX*)0x02AD522C = { 0, 253 };
	*(NJS_TEX*)0x02AD5230 = { 234, -332 };
	*(NJS_TEX*)0x02AD5234 = { 0, -332 };
	*(NJS_TEX*)0x02AD5238 = { 0, 260 };
	*(NJS_TEX*)0x02AD523C = { 255, 260 };
	*(NJS_TEX*)0x02AD5240 = { 0, -332 };
	*(NJS_TEX*)0x02AD5244 = { 255, -332 };
	*(NJS_TEX*)0x02AD5248 = { 0, -692 };
	*(NJS_TEX*)0x02AD524C = { 0, -761 };
	*(NJS_TEX*)0x02AD5250 = { 234, -692 };
	*(NJS_TEX*)0x02AD5254 = { 234, -761 };
	*(NJS_TEX*)0x02AD5258 = { 234, 253 };
	*(NJS_TEX*)0x02AD525C = { 234, -763 };
	*(NJS_TEX*)0x02AD5260 = { 255, 253 };
	*(NJS_TEX*)0x02AD5264 = { 255, -763 };
	*(NJS_TEX*)0x02AD5268 = { 255, -692 };
	*(NJS_TEX*)0x02AD526C = { 255, -770 };
	*(NJS_TEX*)0x02AD5270 = { 0, -692 };
	*(NJS_TEX*)0x02AD5274 = { 0, -770 };

	// dxpc\adv00_stationsquare\object\sssikake_kaiganmato.nja.sa1mdl
	*(NJS_TEX*)0x02AD1108 = { 74, 180 };
	*(NJS_TEX*)0x02AD110C = { 0, 0 };
	*(NJS_TEX*)0x02AD1110 = { 255, 0 };
	*(NJS_TEX*)0x02AD1114 = { 74, -180 };
	*(NJS_TEX*)0x02AD1118 = { 255, -254 };
	*(NJS_TEX*)0x02AD111C = { 255, -254 };
	*(NJS_TEX*)0x02AD1120 = { 435, -180 };
	*(NJS_TEX*)0x02AD1124 = { 255, 0 };
	*(NJS_TEX*)0x02AD1128 = { 255, 255 };
	*(NJS_TEX*)0x02AD112C = { 74, 180 };
	*(NJS_TEX*)0x02AD1130 = { 255, 0 };
	*(NJS_TEX*)0x02AD1134 = { 435, -180 };
	*(NJS_TEX*)0x02AD1138 = { 510, 0 };
	*(NJS_TEX*)0x02AD113C = { 255, 0 };
	*(NJS_TEX*)0x02AD1140 = { 435, 180 };
	*(NJS_TEX*)0x02AD1144 = { 255, 255 };

	// dxpc\adv00_stationsquare\object\sssikake_shop1door.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\sssikake_twadoor.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\sssikake_wakusei.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\stdoor_stationdoor.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\no_unite\sssikake_cl_trainbase.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\no_unite\train_base.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\no_unite\bg\ss_haikei_sky_d.nja.sa1mdl
	//((NJS_MATERIAL*)0x02AA4C38)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x02AA4C4C)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv00_stationsquare\object\no_unite\bg\ss_haikei_sky_e.nja.sa1mdl
	//((NJS_MATERIAL*)0x02AA5E34)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x02AA5E48)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv00_stationsquare\object\no_unite\bg\ss_haikei_sky_n.nja.sa1mdl
	//((NJS_MATERIAL*)0x02AA7034)->diffuse.color = 0xFFB2B2B2;
	//((NJS_MATERIAL*)0x02AA7048)->diffuse.color = 0xFFB2B2B2;

	// dxpc\adv00_stationsquare\object\no_unite\casino\cajinoobj_casinostar.nja.sa1mdl
	((NJS_MATERIAL*)0x02AFCD64)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFCD78)->attrflags = 0x9621A400;
	((NJS_MATERIAL*)0x02AFCD8C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFC828)->attrflags = 0x9621A400;

	// dxpc\adv00_stationsquare\object\no_unite\casino\casinoobj_builg.nja.sa1mdl
	((NJS_MATERIAL*)0x02B00C48)->attrflags = 0x96282400;
	((NJS_MATERIAL*)0x02B009B8)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02B009CC)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B009E0)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B00728)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02B0073C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B00750)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B00498)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02B004AC)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B004C0)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B00208)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02B0021C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B00230)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFF78)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFFF8C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFFA0)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFCE8)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFFCFC)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFD10)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFA58)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFFA6C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFFA80)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF7C8)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFF7DC)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF7F0)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF538)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFF54C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF560)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF2A8)->attrflags = 0x96292400;
	((NJS_MATERIAL*)0x02AFF2BC)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02AFF2D0)->attrflags = 0x96212400;

	// dxpc\adv00_stationsquare\object\no_unite\casino\casinoobj_casinobuil.nja.sa1mdl
	((NJS_MATERIAL*)0x02B03A70)->attrflags = 0x9620A400;
	((NJS_MATERIAL*)0x02B03A84)->attrflags = 0x94282400;
	((NJS_MATERIAL*)0x02B03A98)->attrflags = 0x9428A400;
	((NJS_MATERIAL*)0x02B03AAC)->attrflags = 0x94282400;
	((NJS_MATERIAL*)0x02B03AC0)->attrflags = 0x9420A400;
	((NJS_MATERIAL*)0x02B03358)->attrflags = 0x94282400;
	((NJS_MATERIAL*)0x02B0336C)->attrflags = 0x96202400;
	((NJS_MATERIAL*)0x02B03380)->attrflags = 0x96202400;
	((NJS_MATERIAL*)0x02B03394)->attrflags = 0x9620A400;
	((NJS_MATERIAL*)0x02B033A8)->attrflags = 0x96202400;
	((NJS_MATERIAL*)0x02B02B18)->attrflags = 0x962CA400;
	((NJS_MATERIAL*)0x02B02B2C)->attrflags = 0x96212400;
	((NJS_MATERIAL*)0x02B02B40)->attrflags = 0x96202400;
	((NJS_MATERIAL*)0x02B0280C)->attrflags = 0x9620A400;
	((NJS_MATERIAL*)0x02B02820)->attrflags = 0x96202400;

	// dxpc\adv00_stationsquare\object\no_unite\casino\casinoobj_casinoji.nja.sa1mdl
	((NJS_MATERIAL*)0x02B040E8)->attrflags = 0x9624A400;

	// dxpc\adv00_stationsquare\object\no_unite\kuruma\sskuruma2_body.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\no_unite\kuruma\sspatcar_body.nja.sa1mdl
		// Full replacement

	// dxpc\adv00_stationsquare\object\no_unite\kuruma\ssrotyucar_body.nja.sa1mdl
		//Model too different

	// dxpc\adv00_stationsquare\object\no_unite\kuruma\sstaxi_body.nja.sa1mdl
		// Full replacement
}