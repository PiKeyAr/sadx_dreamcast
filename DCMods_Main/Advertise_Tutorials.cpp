#include "stdafx.h"
#include "Advertise_Tutorals.h"

float PadManuXOffset_F = 175.0f; // Controller graphic X offset (French)
float PadManuXOffset_J = 200.0f; // Controller graphic X offset (Japanese)
float PadManuXOffset_General = 220.0f; // Controller graphic X offset (Languages other than Japanese and French)
float PadManuYOffset = 136.0f; // Controller graphic Y offset
float PadManuYOffset2 = 105.0f; // Controller graphic Y offset
float PadManuYMultiplier = 1.0f; // Scale for the controller graphic

void AdvertiseTutorials_Init()
{
	// PVMs
	ReplacePVM("TUTOBG_AMY");
	ReplacePVM("TUTOBG_BIG");
	ReplacePVM("TUTOBG_E102");
	ReplacePVM("TUTOBG_KNUCKLES");
	ReplacePVM("TUTOBG_SONIC");
	ReplacePVM("TUTOBG_TAILS");
	if (!ModConfig::DLLLoaded_HDGUI)
	{
		ReplacePVM("TUTO_CMN");
		ReplacePVM("TUTO_CMN_E");
		ReplacePVM("TUTOMSG_AMY_E");
		ReplacePVM("TUTOMSG_BIG_E");
		ReplacePVM("TUTOMSG_E102_E");
		ReplacePVM("TUTOMSG_KNUCKLES_E");
		ReplacePVM("TUTOMSG_SONIC_E");
		ReplacePVM("TUTOMSG_TAILS_E");
		ReplacePVM("TUTOMSG_AMY");
		ReplacePVM("TUTOMSG_AMY_F");
		ReplacePVM("TUTOMSG_AMY_G");
		ReplacePVM("TUTOMSG_AMY_S");
		ReplacePVM("TUTOMSG_BIG");
		ReplacePVM("TUTOMSG_BIG_F");
		ReplacePVM("TUTOMSG_BIG_G");
		ReplacePVM("TUTOMSG_BIG_S");
		ReplacePVM("TUTOMSG_E102");
		ReplacePVM("TUTOMSG_E102_F");
		ReplacePVM("TUTOMSG_E102_G");
		ReplacePVM("TUTOMSG_E102_S");
		ReplacePVM("TUTOMSG_KNUCKLES");
		ReplacePVM("TUTOMSG_KNUCKLES_F");
		ReplacePVM("TUTOMSG_KNUCKLES_G");
		ReplacePVM("TUTOMSG_KNUCKLES_S");
		ReplacePVM("TUTOMSG_SONIC");
		ReplacePVM("TUTOMSG_SONIC_F");
		ReplacePVM("TUTOMSG_SONIC_G");
		ReplacePVM("TUTOMSG_SONIC_S");
		ReplacePVM("TUTOMSG_TAILS");
		ReplacePVM("TUTOMSG_TAILS_F");
		ReplacePVM("TUTOMSG_TAILS_G");
		ReplacePVM("TUTOMSG_TAILS_S");
	}
	WriteData((float**)0x64328D, &PadManuXOffset_F);
	WriteData((float**)0x643295, &PadManuXOffset_General);
	WriteData((float**)0x643280, &PadManuXOffset_J);
	WriteData((float**)0x6432C6, &PadManuYOffset);
	WriteData((float**)0x6432E4, &PadManuYOffset2);
	WriteData((float**)0x6432D4, &PadManuYMultiplier);
	// Sonic
	// English
	SonicPageTbl_E[0].w = 390;
	SonicPageTbl_E[0].h = 144;
	SonicPageTbl_E[0].x = 210;
	SonicPageTbl_E[4].h = 160;
	SonicSpr0_0_items[1].x = 136; // Rotate camera
	SonicSpr0_0_items[1].y = 0; // Rotate camera
	SonicSpr0_0_items[2].x = 136; // Maneuver character
	SonicSpr0_0_items[2].y = 24; // Maneuver character
	SonicSpr0_0_items[4].x = 136; // A button
	SonicSpr0_0_items[4].y = 64; // A button
	SonicSpr0_0_items[4].index = 3;
	SonicSpr0_0_items[5].index = 4;
	SonicSpr0_0_items[5].x = 136;
	SonicSpr0_0_items[5].y = 96;
	WriteData((__int16*)0x2BC3AE2, (__int16)2000); // Hide an extra item in controls page (all langs)
	// Japanese
	SonicPageTbl_J[0].w = 400;
	SonicPageTbl_J[0].h = 144;
	SonicPageTbl_J[0].x = 200;
	SonicPageTbl_J[4].h = 160;
	SonicSpr0_J_items[1].x = 136; // Rotate camera
	SonicSpr0_J_items[1].y = 0; // Rotate camera
	SonicSpr0_J_items[2].x = 136; // Maneuver character
	SonicSpr0_J_items[2].y = 24; // Maneuver character
	SonicSpr0_J_items[3].x = 136; // A button
	SonicSpr0_J_items[3].y = 64; // A button
	SonicSpr0_J_items[3].index = 3;
	SonicSpr0_J_items[4].index = 4;
	SonicSpr0_J_items[4].x = 136;
	SonicSpr0_J_items[4].y = 96;
	// German
	SonicPageTbl_G[0].w = 380;
	SonicPageTbl_G[0].h = 144;
	SonicPageTbl_G[0].x = 220;
	SonicPageTbl_G[3].h = 160;
	SonicPageTbl_G[4].h = 160;
	// French
	SonicPageTbl_F[4].h = 192;
	SonicPageTbl_F[0].w = 425;
	SonicPageTbl_F[0].h = 144;
	SonicPageTbl_F[0].x = 175;
	// Spanish
	SonicPageTbl_S[0].w = 370;
	SonicPageTbl_S[0].h = 144;
	SonicPageTbl_S[0].x = 230;
	SonicPageTbl_S[4].h = 192;
	// Tails
	// English
	TailsPageTbl_E[0].w = 390;
	TailsPageTbl_E[0].h = 144;
	TailsPageTbl_E[0].x = 210;
	TailsPageTbl_E[4].h = 160;
	TailsCmnSpr0_E_items[0].x = 136; // Rotate camera
	TailsCmnSpr0_E_items[0].y = 0; // Rotate camera
	TailsCmnSpr0_E_items[1].x = 136; // Maneuver character
	TailsCmnSpr0_E_items[1].y = 24; // Maneuver character
	TailsCmnSpr0_E_items[3].x = 136; // A button
	TailsCmnSpr0_E_items[3].y = 96; // A button
	TailsSpr0_E_items.x = 136; // Tail attack
	TailsSpr0_E_items.y = 64; // Tail attack
	// Japanese
	TailsPageTbl_J[0].w = 400;
	TailsPageTbl_J[0].h = 144;
	TailsPageTbl_J[0].x = 200;
	TailsPageTbl_J[4].h = 160;
	TailsCmnSpr0_J_items[0].x = 136; // Rotate camera
	TailsCmnSpr0_J_items[0].y = 0; // Rotate camera
	TailsCmnSpr0_J_items[1].x = 136; // Maneuver character
	TailsCmnSpr0_J_items[1].y = 24; // Maneuver character
	TailsCmnSpr0_J_items[2].x = 136; // A button
	TailsCmnSpr0_J_items[2].y = 96; // A button
	TailsSpr0_J_items.x = 136; // Tail attack
	TailsSpr0_J_items.y = 64; // Tail attack
	// French
	TailsPageTbl_F[4].h = 192;
	TailsPageTbl_F[0].w = 425;
	TailsPageTbl_F[0].h = 144;
	TailsPageTbl_F[0].x = 175;
	// German
	TailsPageTbl_G[0].w = 390;
	TailsPageTbl_G[0].h = 144;
	TailsPageTbl_G[0].x = 210;
	TailsPageTbl_G[3].h = 128;
	TailsPageTbl_G[4].h = 160;
	TailsPageTbl_G[0].w = 380;
	TailsPageTbl_G[0].h = 144;
	TailsPageTbl_G[0].x = 220;
	WriteData((__int16*)0x2BC3E9E, (__int16)2000); // Hide an extra item in controls page
	// Spanish
	TailsPageTbl_S[0].w = 370;
	TailsPageTbl_S[0].h = 144;
	TailsPageTbl_S[0].x = 230;
	TailsPageTbl_S[4].h = 192;
	// Knuckles
	// English
	KnucklesPageTbl_E[0].w = 390;
	KnucklesPageTbl_E[0].h = 144;
	KnucklesPageTbl_E[0].x = 210;
	KnucklesPageTbl_E[4].x = 180;
	KnucklesPageTbl_E[4].h = 128;
	KnucklesPageTbl_E[4].w = 425;
	KnucklesPageTbl_E[5].h = 160;
	KnucklesSpr0_E_items.x = 136; // Punch attack
	KnucklesSpr0_E_items.y = 64; // Punch attack
	if (!ModConfig::DLLLoaded_Korean)
	{
		WriteData<1>((char*)0x2BC4308, 0x03); // Number of items in Maximum Heat screen, remove unnecessary line
		KnucklesPageTbl_J[4].h = 128;
	}
	// Japanese
	KnucklesPageTbl_J[0].w = 400;
	KnucklesPageTbl_J[0].h = 144;
	KnucklesPageTbl_J[0].x = 200;
	KnucklesPageTbl_J[4].x = 180;

	KnucklesPageTbl_J[4].w = 425;
	KnucklesPageTbl_J[5].h = 160;
	KnucklesSpr0_J_items.x = 136; // Punch attack
	KnucklesSpr0_J_items.y = 64; // Punch attack
	// German
	KnucklesPageTbl_G[0].w = 380;
	KnucklesPageTbl_G[0].h = 144;
	KnucklesPageTbl_G[0].x = 220;
	KnucklesPageTbl_G[2].h = 128;
	KnucklesPageTbl_G[4].x = 180;
	KnucklesPageTbl_G[4].w = 420;
	KnucklesPageTbl_G[4].h = 128;
	KnucklesPageTbl_G[5].h = 160;
	WriteData((__int16*)0x2BC42E0, (__int16)2000); // Hide an extra item in Climbing page
	// French
	KnucklesPageTbl_F[0].w = 430;
	KnucklesPageTbl_F[0].h = 144;
	KnucklesPageTbl_F[0].x = 170;
	KnucklesPageTbl_F[1].w = 455;
	KnucklesPageTbl_F[1].x = 145;
	KnucklesPageTbl_F[4].w = 475;
	KnucklesPageTbl_F[4].x = 125;
	KnucklesPageTbl_F[4].h = 128;
	KnucklesPageTbl_F[5].h = 192;
	WriteData((__int16*)0x2BC3E9E, (__int16)2000); // Hide an extra item in Controls page
	WriteData((__int16*)0x2BC433A, (__int16)2000); // Hide an extra item in Maximum Heat page
	WriteData((__int16*)0x2BC4340, (__int16)2000); // Hide an extra item in Maximum Heat page
	// Spanish
	KnucklesPageTbl_S[0].w = 370;
	KnucklesPageTbl_S[0].h = 144;
	KnucklesPageTbl_S[0].x = 230;
	KnucklesPageTbl_S[4].w = 415;
	KnucklesPageTbl_S[4].x = 190;
	KnucklesPageTbl_S[4].h = 128;
	KnucklesPageTbl_S[5].h = 192;
	// Amy
	// English
	AmyPageTbl_E[0].w = 390;
	AmyPageTbl_E[0].h = 144;
	AmyPageTbl_E[0].x = 210;
	AmyPageTbl_E[4].h = 160;
	AmyCmnSpr0_E_items[1].x = 136; // Rotate camera
	AmyCmnSpr0_E_items[1].y = 0; // Rotate camera
	AmyCmnSpr0_E_items[2].x = 136; // Maneuver character
	AmyCmnSpr0_E_items[2].y = 24; // Maneuver character
	AmySpr0_E_items[0].x = 136; // A
	AmySpr0_E_items[0].y = 96; // A
	AmySpr0_E_items[1].x = 136; // B
	AmySpr0_E_items[1].y = 64; // B
	// Japanese
	AmyPageTbl_J[0].w = 400;
	AmyPageTbl_J[0].h = 144;
	AmyPageTbl_J[0].x = 200;
	AmyPageTbl_J[4].h = 160;
	AmyCmnSpr0_J_items[1].x = 136; // Rotate camera
	AmyCmnSpr0_J_items[1].y = 0; // Rotate camera
	AmyCmnSpr0_J_items[2].x = 136; // Maneuver character
	AmyCmnSpr0_J_items[2].y = 24; // Maneuver character
	AmySpr0_J_items[0].x = 136; // A
	AmySpr0_J_items[0].y = 96; // A
	AmySpr0_J_items[1].x = 136; // B
	AmySpr0_J_items[1].y = 64; // B
	// German
	AmyPageTbl_G[0].w = 380;
	AmyPageTbl_G[0].h = 144;
	AmyPageTbl_G[0].x = 220;
	AmyPageTbl_G[4].h = 160;
	WriteData((__int16*)0x2BC46FA, (__int16)2000); // Hide an extra item in controls page
	// French
	AmyPageTbl_F[0].w = 425;
	AmyPageTbl_F[0].h = 144;
	AmyPageTbl_F[0].x = 175;
	AmyPageTbl_F[4].h = 192;
	// Spanish
	AmyPageTbl_S[0].w = 370;
	AmyPageTbl_S[0].h = 144;
	AmyPageTbl_S[0].x = 230;
	AmyPageTbl_S[4].h = 192;
	// Big
	// English
	BigPageTbl_E[0].w = 390;
	BigPageTbl_E[0].h = 144;
	BigPageTbl_E[0].x = 210;
	BigPageTbl_E[4].h = 128;
	BigPageTbl_E[7].w = 420;
	BigPageTbl_E[7].x = 180;
	BigCmnSpr0_E_items[1].x = 136; // Rotate camera
	BigCmnSpr0_E_items[1].y = 0; // Rotate camera
	BigCmnSpr0_E_items[2].x = 136; // Maneuver character
	BigCmnSpr0_E_items[2].y = 24; // Maneuver character
	BigSpr0_E_items[0].x = 136; // A
	BigSpr0_E_items[0].y = 96; // A
	BigSpr0_E_items[1].x = 136; // B
	BigSpr0_E_items[1].y = 64; // B
	BigSpr4_0_items[2].index = 32; // Hide "Tugging the rod"
	BigSpr4_0_items[3].y = 64; // Move A up
	BigSpr4_0_items[4].y = 96; // Move B up
	// Japanese
	BigPageTbl_J[0].w = 400;
	BigPageTbl_J[0].h = 144;
	BigPageTbl_J[0].x = 200;
	BigPageTbl_J[4].h = 160;
	BigPageTbl_J[7].w = 400;
	BigPageTbl_J[7].x = 200;
	BigSpr0_J_items[0].x = 136; // A
	BigSpr0_J_items[0].y = 96; // A
	BigSpr0_J_items[1].x = 136; // B
	BigSpr0_J_items[1].y = 64; // B
	// German
	BigPageTbl_G[0].w = 395;
	BigPageTbl_G[0].h = 144;
	BigPageTbl_G[0].x = 205;
	BigPageTbl_G[4].h = 128;
	WriteData((__int16*)0x2BC4E9E, (__int16)2000); // Hide an extra item in controls page
	// French
	BigPageTbl_F[0].w = 425;
	BigPageTbl_F[0].h = 144;
	BigPageTbl_F[0].x = 175;
	BigPageTbl_F[4].h = 128;
	BigPageTbl_F[7].w = 445;
	BigPageTbl_F[7].x = 155;
	// Spanish
	BigPageTbl_S[0].w = 370;
	BigPageTbl_S[0].h = 144;
	BigPageTbl_S[0].x = 230;
	BigPageTbl_S[4].w = 275;
	BigPageTbl_S[4].x = 325;
	BigPageTbl_S[4].h = 128;
	BigPageTbl_S[7].w = 420;
	BigPageTbl_S[7].x = 180;
	BigPageTbl_S[7].w = 475;
	BigPageTbl_S[7].x = 125;
	// Gamma
	// English
	E102PageTbl_E[0].w = 390;
	E102PageTbl_E[0].h = 144;
	E102PageTbl_E[0].x = 210;
	E102PageTbl_E[4].h = 160;
	E102Spr0_E_items[0].x = 136; // A
	E102Spr0_E_items[0].y = 96; // A
	E102Spr0_E_items[1].x = 136; // B
	E102Spr0_E_items[1].y = 64; // B
	// Japanese
	E102PageTbl_J[0].w = 400;
	E102PageTbl_J[0].h = 144;
	E102PageTbl_J[0].x = 200;
	E102PageTbl_J[4].h = 160;
	E102Spr0_J_items[0].x = 136; // A
	E102Spr0_J_items[0].y = 96; // A
	E102Spr0_J_items[1].x = 136; // B
	E102Spr0_J_items[1].y = 64; // B
	// German
	E102PageTbl_G[4].h = 160;
	E102PageTbl_G[0].w = 380;
	E102PageTbl_G[0].h = 144;
	E102PageTbl_G[0].x = 220;
	// French
	E102PageTbl_F[0].w = 425;
	E102PageTbl_F[0].h = 144;
	E102PageTbl_F[0].x = 175;
	E102PageTbl_F[1].h = 192;
	E102PageTbl_F[4].h = 192;
	WriteData((__int16*)0x2BC4AE4, (__int16)2000); // Hide an extra item in the second page
	// Spanish
	E102PageTbl_S[0].w = 370;
	E102PageTbl_S[0].h = 144;
	E102PageTbl_S[0].x = 230;
	E102PageTbl_S[4].h = 192;
}

void AdvertiseTutorials_OnFrame()
{
	if (ModConfig::DLLLoaded_HDGUI)
		return;
	// Offset the controller graphic depending on current language and character
		if (Language == SPANISH)
			PadManuXOffset_General = 230.0f;
		if (Language == GERMAN && GetAdvertisePlayerNumber() != AVA_BIG)
			PadManuXOffset_General = 220.0f;
		if (Language == GERMAN && GetAdvertisePlayerNumber() == AVA_BIG)
			PadManuXOffset_General = 205.0f;
		if (Language != SPANISH && Language != GERMAN) 
			PadManuXOffset_General = 205.0f;
		if (GetAdvertisePlayerNumber() == ADV_PLNO_KNUCKLES) // Knucklles
			PadManuXOffset_F = 170.0f;
		else
			PadManuXOffset_F = 175.0f;
}