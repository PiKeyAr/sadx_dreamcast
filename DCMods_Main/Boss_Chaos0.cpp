#include "stdafx.h"

static bool ModelsLoaded_B_CHAOS0;
DataArray(NJS_TEXANIM, anim_lensflare, 0x009BF310, 8);

// Draw missing lights under police cars
void DrawChaos0Spotlight(task *tp)
{
	NJS_SPRITE Chaos0SpotlightSprite1 = { -54.0f, 10.0f, 0.01f, 0.5f, 0.84f, 17500, BOSSCHAOS0_TEXLISTS[1], anim_lensflare };
	NJS_SPRITE Chaos0SpotlightSprite2 = { -54.0f, -10.0f, 0.01f, 0.5f, 0.84f, 15000, BOSSCHAOS0_TEXLISTS[1], anim_lensflare };
	taskwk *twp = tp->twp;
	if (!loop_count)
	{
		SetMaterial(0.5f, 1.0f, 1.0f, 1.0f);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njSetTexture(BOSSCHAOS0_TEXLISTS[1]);
		late_z_ofs___ = -17000.0f;
		// Left
		njPushMatrix(0);
		njTranslate(0, twp->pos.x, twp->pos.y + 0.1f, twp->pos.z);
		njRotateXYZ(0, 0xC000, twp->ang.y, 0);
		late_DrawSprite3D(&Chaos0SpotlightSprite1, 1, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_ANGLE, LATE_LIG);
		njPopMatrix(1u);
		// Right
		njPushMatrix(0);
		njTranslate(0, twp->pos.x, twp->pos.y + 0.1f, twp->pos.z);
		njRotateXYZ(0, 0xC000, twp->ang.y, 0);
		late_DrawSprite3D(&Chaos0SpotlightSprite2, 1, NJD_SPRITE_ALPHA | NJD_SPRITE_COLOR | NJD_SPRITE_ANGLE, LATE_LIG);
		njPopMatrix(1u);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		ResetMaterial();
		late_z_ofs___ = 0.0f;
	}
}

// Trampoline to fix police car animation and draw bottom lights
static Trampoline* OPato0_Display_t = nullptr;
static void __cdecl OPato0_Display_r(task *tp)
{
	float nbFrames = BOSSCHAOS0_ACTIONS[18]->motion->nbFrame - 1.0f;
	const auto original = TARGET_DYNAMIC(OPato0_Display);
	if (tp->twp->scl.x >= nbFrames)
		tp->twp->scl.x -= nbFrames; // Animation stutter fix
	original(tp);
	if (ModConfig::EnabledLevels[LevelIDs_Chaos0])
	{
		if (LANDTABLEBOSSCHAOS0[0]->Col[4].Flags & SurfaceFlags_Visible)
		{
			DrawChaos0Spotlight(tp);
		}
	}
}

// Depth/draw order hooks

void FixChaos0Car(NJS_ACTION *a1, float frame, float scale)
{
	DrawAction(a1, frame, LATE_MAT, scale, (void(__cdecl *)(NJS_MODEL_SADX *, int, int))DrawModelThing);
	late_DrawModel(a1->object->child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling->basicdxmodel, LATE_MAT); // Top lights
	late_DrawModel(a1->object->child->sibling->sibling->sibling->sibling->sibling->sibling->basicdxmodel, LATE_WZ); // Front lights
}

void Chaos0PuddleFix(NJS_OBJECT* a1)
{
	DrawObjectClipMesh(a1, LATE_MAT, 1.0f);
}

void DrawChaos0HelicopterWithLight(NJS_ACTION* a1, float a2, LATE a3, float a4)
{
	late_ActionClipMesh(a1, a2, a3, a4);
}

void Chaos0Action(NJS_ACTION* a1, float frameNumber)
{
	late_ActionMesh(a1, frameNumber, LATE_MAT);
}

void B_CHAOS0_Init()
{
	ReplaceSET("SET1500S");
	ReplacePVM("LM_CHAOS0");
	ReplacePVM("CHAOS0");
	ReplacePVM("CHAOS0_EFFECT");
	ReplacePVM("CHAOS0_OBJECT");
	ReplacePVM("EV_CHAOS0_MANJU");
}

void B_CHAOS0_Load()
{
	LevelLoader(LevelAndActIDs_Chaos0, "SYSTEM\\data\\boss00_chaos0\\landtable0b00.c.sa1lvl", BOSSCHAOS0_TEXLISTS[2],___LANDTABLEBOSSCHAOS0[0]);
	if (!ModelsLoaded_B_CHAOS0)
	{
		OPato0_Display_t = new Trampoline(0x00549600, 0x00549605, OPato0_Display_r);
		WriteCall((void*)0x0054858A, Chaos0Action); // Main model
		WriteData<1>((char*)0x0054932B, 0x08); // Police car lights blending mode
		WriteData<1>((char*)0x007AD16D, 0x08); // Chaos 0 puddle mark blending mode
		WriteData<1>((char*)0x00548470, 0i8); // Chaos 0 puddle queued flags I guess
		WriteCall((void*)0x0054847B, Chaos0PuddleFix);
		WriteCall((void*)0x006E9B4A, Chaos0PuddleFix);
		// Helicopter
		BOSSCHAOS0_ACTIONS[17]->object = LoadModel("system\\data\\boss00_chaos0\\common\\objmodels\\heli_body.nja.sa1mdl"); // Edited hierarchy
		BOSSCHAOS0_ACTIONS[17]->object->child->child->child->basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_LIGHT; // UV-less stuff fix
		AddWhiteDiffuseMaterial(&BOSSCHAOS0_ACTIONS[17]->object->child->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&BOSSCHAOS0_ACTIONS[17]->object->child->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[1]);
		WriteCall((void*)0x005498F6, DrawChaos0HelicopterWithLight);
		// Police car
		*BOSSCHAOS0_ACTIONS[18]->object = *LoadModel("system\\data\\boss00_chaos0\\common\\objmodels\\patcar_body.nja.sa1mdl");
		AddWhiteDiffuseMaterial(&BOSSCHAOS0_ACTIONS[18]->object->child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling->basicdxmodel->mats[1]);
		BOSSCHAOS0_ACTIONS[18]->object->child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE;
		BOSSCHAOS0_ACTIONS[18]->object->child->sibling->sibling->sibling->sibling->sibling->sibling->evalflags |= NJD_EVAL_HIDE;
		// Make spinning things inside flashers (UV-less) dark like in SA1
		BOSSCHAOS0_ACTIONS[18]->object->child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling->child->sibling->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		BOSSCHAOS0_ACTIONS[18]->object->child->sibling->sibling->sibling->sibling->sibling->sibling->sibling->sibling->child->basicdxmodel->mats[0].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		WriteCall((void*)0x0054968E, FixChaos0Car);
		WriteData((float*)0x00549797, 0.12f); // Camera-based car light sprite scale
		WriteData((float*)0x005497A1, 0.12f); // Camera-based car light sprite scale
		ModelsLoaded_B_CHAOS0 = true;
	}
}

/*
WriteJump((void*)0x006E9B00, ComeOneYaBigDrip); // Alterating transparency in the cutscene after Chaos 0
DataPointer(task*, p_Chaos0Task, 0x03C84628);
DataPointer(task*, p_Chaos0Task_2, 0x03C84628);
struct Chaos0Work_EV0002
{
	float cng_int;
	NJS_POINT3 velocity;
	int frame;
	float manju_scale;
	float manju_dscale;
	Uint8 manju_alpha;
	Uint8 manju_dalpha;
};

// Makes puddle transparency pulsate
void __cdecl ComeOneYaBigDrip()
{
	taskwk* twp = p_Chaos0Task_2->twp;
	Chaos0Work_EV0002* cwk = (Chaos0Work_EV0002*)twp->value.ptr;
	Uint8 alpha1 = min(255, p_Chaos0Task_2->twp->scl.x * 255);
	Uint8 alpha2 = cwk->manju_alpha;
	object_c0_manju_c0_manju.basicdxmodel->mats->diffuse.argb.a = alpha1;
	njSetTexture(&texlist_chaos_surface);
	njPushMatrix(0);
	njTranslateV(0, &twp->pos);
	njScaleV(0, &twp->scl);
	DrawObjectClipMesh(&object_c0_manju_c0_manju, LATE_MAT, 1.0f);
	njPopMatrix(1u);
}
*/