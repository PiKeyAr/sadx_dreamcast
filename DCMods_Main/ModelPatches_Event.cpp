#include "stdafx.h"

// This isn't like on DC but it fixes wrong textures on frogs in Gamma's cutscene
void PatchFrogsLol()
{
	// original\f_frog1.nja.sa1mdl
	((NJS_MATERIAL*)0x030CA7E8)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CA7FC)->attr_texId = 6;
	((NJS_MATERIAL*)0x030CA810)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CA810)->attrflags = 0x9439A400;
	((NJS_MATERIAL*)0x030CA824)->attr_texId = 3;
	((NJS_MATERIAL*)0x030CA838)->attr_texId = 1;
	((NJS_MATERIAL*)0x030CA6E0)->attr_texId = 0;
	((NJS_MATERIAL*)0x030CA3B0)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CA3C4)->attr_texId = 2;
	((NJS_MATERIAL*)0x030CA1F8)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CA040)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C9D10)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C9D24)->attr_texId = 2;
	((NJS_MATERIAL*)0x030C9B58)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C99A0)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C9634)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C94C0)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C9154)->attr_texId = 4;
	((NJS_MATERIAL*)0x030C8FDC)->attr_texId = 4;

	// original\f_frog2.nja.sa1mdl
	((NJS_MATERIAL*)0x030CCE68)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CCE7C)->attr_texId = 3;
	((NJS_MATERIAL*)0x030CC9F4)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CC880)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CC550)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CC564)->attr_texId = 2;
	((NJS_MATERIAL*)0x030CC398)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CC1E0)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CBEB0)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CBEC4)->attr_texId = 2;
	((NJS_MATERIAL*)0x030CBCF8)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CBB40)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CB7D4)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CB65C)->attr_texId = 4;

	// original\f_frog3.nja.sa1mdl
	((NJS_MATERIAL*)0x030CF3D0)->attr_texId = 3;
	((NJS_MATERIAL*)0x030CF40C)->attr_texId = 4;
	((NJS_MATERIAL*)0x030CF0A0)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CF0B4)->attr_texId = 2;
	((NJS_MATERIAL*)0x030CEEE8)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CED30)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CEA00)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CEA14)->attr_texId = 2;
	((NJS_MATERIAL*)0x030CE848)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CE690)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CE690)->attrflags = 0x9469A400;
	((NJS_MATERIAL*)0x030CE324)->attr_texId = 5;
	((NJS_MATERIAL*)0x030CDE44)->attr_texId = 5;
}

void PatchModels_Event()
{
	PatchFrogsLol();
}