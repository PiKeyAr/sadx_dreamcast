#pragma once

void UpdateExportVariables();
void LoadLateDrawLand();
void RunCustomAnimations();
void BackupDebugFontSettings();
void RestoreDebugFontSettings();
void ClearTextureAnimationData();
void ClearLoadedLevels(int levelact);
void LoadModConfig(const char* path);
void land_DrawObject_New(NJS_OBJECT* a1, _OBJ_LANDENTRY* a2);

// Mod Init, Load, OnFrame and OnInput functions
void ADV00_Load();
void ADV01_Load();
void ADV01C_Load();
void ADV02_Load();
void ADV03_Load();
void B_CHAOS0_Load();
void B_CHAOS2_Load();
void B_CHAOS4_Load();
void B_CHAOS6_Load();
void B_CHAOS7_Load();
void B_EGM1_Load();
void B_EGM2_Load();
void B_EGM3_Load();
void B_ROBO_Load();
void B_E101_Load();
void B_E101_R_Load();
void EmeraldCoast_Load();
void WindyValley_Load();
void TwinklePark_Load();
void SpeedHighway_Load();
void RedMountain_Load();
void SkyDeck_Load();
void LostWorld_Load();
void IceCap_Load();
void Casinopolis_Load();
void FinalEgg_Load();
void HotShelter_Load();
void SkyChase_Load();
void TwinkleCircuit_Load();
void ShareObj_Load();
void SandHill_Load();
void HedgehogHammer_Load();
void AL_Main_Load();
void AL_ChaoRace_Load();
void Objects_OnFrame();
void Animals_Load();
void Characters_Load();

void ADV00_Init();
void ADV01_Init();
void ADV01C_Init();
void ADV02_Init();
void ADV03_Init();
void B_CHAOS0_Init();
void B_CHAOS2_Init();
void B_CHAOS4_Init();
void B_CHAOS6_Init();
void B_CHAOS7_Init();
void B_EGM1_Init();
void B_EGM2_Init();
void B_EGM3_Init();
void B_ROBO_Init();
void B_E101_Init();
void B_E101_R_Init();
void EmeraldCoast_Init();
void WindyValley_Init();
void TwinklePark_Init();
void SpeedHighway_Init();
void RedMountain_Init();
void SkyDeck_Init();
void LostWorld_Init();
void IceCap_Init();
void Casinopolis_Init();
void FinalEgg_Init();
void HotShelter_Init();
void SkyChase_Init();
void TwinkleCircuit_Init();
void HedgehogHammer_Init();
void AL_Main_Init();
void AL_ChaoRace_Init();
void General_Init();
void SpeedFixes_Init();
void SandHill_Init();
void AdvertiseTutorials_Init();
void AdvertiseFeatures_Init();
void AdvertiseTitleScreen_Init();
void AdvertiseGUI_Init();
void AdvertiseCredits_Init();
void AdvertiseDemos_Init();
void Videos_Init();
void Fish_Init();
void Animals_Init();
void Characters_Init();
void Enemies_Init();
void Event_Init();
void HeldObjects_Init();
void Objects_Init();

void EmeraldCoast_OnFrame();
void WindyValley_OnFrame();
void RedMountain_OnFrame();
void SkyDeck_OnFrame();
void LostWorld_OnFrame();
void IceCap_OnFrame();
void Casinopolis_OnFrame();
void FinalEgg_OnFrame();
void HotShelter_OnFrame();
void AL_Main_OnFrame();
void AL_ChaoRace_OnFrame();
void Videos_OnFrame();
void SpeedFixes_OnFrame();
void ADV00_OnFrame();
void ADV01_OnFrame();
void ADV02_OnFrame();
void AdvertiseTutorials_OnFrame();
void AdvertiseDemos_OnFrame();
void AdvertiseFeatures_OnFrame();
void AdvertiseTitleScreen_OnFrame();
void Event_OnFrame();
void General_OnFrame();
void Characters_OnFrame();

void Event_OnInput();
void AdvertiseDemos_OnInput();

void AdvertiseTitleScreen_OnReset();

// Nullsub calls for other mods to hook
DataPointer(Uint8, nullsub_ADV00, 0x4231E6);
DataPointer(Uint8, nullsub_ADV01, 0x4232C9);
DataPointer(Uint8, nullsub_ADV01C, 0x4233BB);
DataPointer(Uint8, nullsub_ADV02, 0x4234AD);
DataPointer(Uint8, nullsub_ADV03, 0x423554);
DataPointer(Uint8, nullsub_STG00, 0x422B2A);
DataPointer(Uint8, nullsub_STG01, 0x422B68);
DataPointer(Uint8, nullsub_STG02, 0x422BD3);
DataPointer(Uint8, nullsub_STG03, 0x422C3E);
DataPointer(Uint8, nullsub_STG04, 0x422CA9);
DataPointer(Uint8, nullsub_STG05, 0x422D14);
DataPointer(Uint8, nullsub_STG06, 0x422D84);
DataPointer(Uint8, nullsub_STG07, 0x422DEF);
DataPointer(Uint8, nullsub_STG08, 0x422E5A);
DataPointer(Uint8, nullsub_STG09, 0x422EE8);
DataPointer(Uint8, nullsub_STG10, 0x422F71);
DataPointer(Uint8, nullsub_STG12, 0x422FFF);
DataPointer(Uint8, nullsub_B_CHAOS0, 0x423088);
DataPointer(Uint8, nullsub_B_CHAOS2, 0x4230B7);
DataPointer(Uint8, nullsub_B_CHAOS4, 0x4230CD);
DataPointer(Uint8, nullsub_B_CHAOS6, 0x4230E3);
DataPointer(Uint8, nullsub_B_CHAOS7, 0x423108);
DataPointer(Uint8, nullsub_B_EGM1, 0x423146);
DataPointer(Uint8, nullsub_B_EGM2, 0x42315F);
DataPointer(Uint8, nullsub_B_EGM3, 0x423178);
DataPointer(Uint8, nullsub_B_ROBO, 0x423196);
DataPointer(Uint8, nullsub_B_E101, 0x4231AF);
DataPointer(Uint8, nullsub_B_E101R, 0x4231CD);
DataPointer(Uint8, nullsub_SHOOTING1, 0x4236B1);
DataPointer(Uint8, nullsub_SHOOTING2, 0x4236E0);
DataPointer(Uint8, nullsub_SBOARD, 0x42370F);
DataPointer(Uint8, nullsub_MINICART, 0x4235EC);
DataPointer(Uint8, nullsub_Chao, 0x423795);

// Old mod DLL names

static const wchar_t* const OldModDLLs[] = {
	L"DC_Bosses",
	L"DC_Branding",
	L"DC_ChaoGardens",
	L"DC_Casinopolis",
	L"DC_ADV00MODELS",
	L"DC_ADV01MODELS",
	L"DC_ADV02MODELS",
	L"DC_ADV03MODELS",
	L"DC_EmeraldCoast",
	L"DC_EnvMaps",
	L"DC_FinalEgg",
	L"DC_General",
	L"DC_HotShelter",
	L"DC_IceCap",
	L"DC_LostWorld",
	L"DC_RedMountain",
	L"DC_SkyDeck",
	L"DC_SpeedHighway",
	L"DC_TwinklePark",
	L"DC_WindyValley",
	L"DC_SubGames",
	L"DC_TornadoModels",
	L"DisableSA1TitleScreen",
	L"KillCream",
	L"RevertECDrawDistance",
	L"MRFinalEggFix",
	L"WaterEffect"
};