#include "stdafx.h"
#include "STG03_TwinklePark.h"

static bool ModelsLoaded_STG03;

void dispTPPirates_r(task* tsk)
{
	taskwk* twp; // esi
	twp = tsk->twp;
	int texid = 28;
	NJS_OBJECT* object_newship_oya_bou1 = object_newship_oya_oya.child;
	NJS_OBJECT* object_newship_oya_ship = object_newship_oya_oya.child->child;
	NJS_OBJECT* object_newship_oya_bou2 = object_newship_oya_oya.child->sibling;
	if (!loop_count)
	{
		// Texture animation
		if (twp->btimer < 30u)
			texid = 28;
		else
			texid = 95;
		object_newship_oya_oya.basicdxmodel->mats[4].attr_texId =
			object_newship_oya_bou1->basicdxmodel->mats[0].attr_texId =
			object_newship_oya_ship->basicdxmodel->mats[2].attr_texId =
			object_newship_oya_ship->basicdxmodel->mats[3].attr_texId =
			object_newship_oya_ship->basicdxmodel->mats[8].attr_texId =
			object_newship_oya_ship->basicdxmodel->mats[12].attr_texId =
			object_newship_oya_bou2->basicdxmodel->mats[0].attr_texId
			= texid;
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		Angle rot_y = twp->ang.y;
		if (rot_y)
			njRotateY(0, rot_y);
		// Draw base model
		if (IsCameraUnderwater)
			late_z_ofs___ = -28000.0f;
		else
			late_z_ofs___ = 0.0f;
		late_DrawModel(object_newship_oya_oya.basicdxmodel, LATE_WZ);
		late_z_ofs___ = 0.0f;
		njPushMatrix(0);
		// Draw rotating part 1
		njTranslateV(0, (NJS_VECTOR*)object_newship_oya_bou1->pos);
		Angle rot_z = twp->ang.z;
		if (rot_z)
			njRotateZ(0, rot_z);
		dsDrawModel(object_newship_oya_bou1->basicdxmodel);
		njPushMatrix(0);
		// Draw ships
		njTranslateV(0, (NJS_VECTOR*)object_newship_oya_ship->pos);
		if (rot_z)
			njRotateZ(0, -rot_z);
		dsDrawModel(object_newship_oya_ship->basicdxmodel);
		njPopMatrix(1u);
		njPopMatrix(1u);
		njPushMatrix(0);
		// Draw rotating part 2
		njTranslateV(0, (NJS_VECTOR*)object_newship_oya_bou2->pos);
		if (rot_z)
			njRotateZ(0, rot_z);
		dsDrawModel(object_newship_oya_bou2->basicdxmodel);
		njPopMatrix(1u);
		njPopMatrix(1u);
	}
}

void RenderCatapult(NJS_ACTION *a1, float frame, float scale)
{
	late_ActionClipEx(a1, frame, LATE_WZ, scale);
}

void __cdecl dispTPRoof_r(NJS_OBJECT* obj, float scale)
{
	bool isFloor = obj == &object_park02_yuka_yuka;
	if (isFloor)
		njTranslate(0, 0, -0.02f, 0);
	ds_DrawObjectClip(obj, scale);
}

void __cdecl FlowerBedFix(NJS_OBJECT* a1, LATE a2, float a3)
{
	// The game does this shit too exactly like this, don't blame me
	if (a1 == &object_tpobj_kadann00_kadann00 ||
		a1 == &object_tpobj_kadann02_kadann02 ||
		a1 == &object_tpobj_kadann04_kadann04 ||
		a1 == &object_tpobj_kadann01_kadann01 ||
		a1 == &object_tpobj_kadann03_kadann03 ||
		a1 == &object_tpobj_kadann05_kadann05)
	{
		DrawModelMesh(a1->basicdxmodel, LATE_WZ);
	}
	else
		late_DrawObjectClipEx(a1, a2, a3);
}

void TwinklePark_Init()
{
	ReplaceCAM("CAM0300S");
	ReplaceCAM("CAM0301A");
	ReplaceCAM("CAM0301B");
	ReplaceCAM("CAM0301S");
	ReplaceCAM("CAM0302A");
	ReplaceCAM("CAM0302S");
	ReplaceSET("SET0300S");
	ReplaceSET("SET0301A");
	ReplaceSET("SET0301B");
	ReplaceSET("SET0301S");
	ReplaceSET("SET0302A");
	ReplaceSET("SET0302S");
	ReplacePVM("OBJ_TWINKLE");
	ReplacePVM("EFF_TWINKLE");
	ReplacePVM("EFF_CANDLE");
	ReplacePVM("TWINKLE01");
	ReplacePVM("TWINKLE02");
	ReplacePVM("TWINKLE03");
	// Fog and draw distance
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg03[0][i].f32StartZ = -1500.0f;

		pFogTable_Stg03[1][i].f32StartZ = -1400.0f;
		pFogTable_Stg03[1][i].f32EndZ = -3200.0f;
		pFogTable_Stg03[1][i].Col = 0xFF100030;

		pFogTable_Stg03[2][i].f32StartZ = -800.0f;
		pFogTable_Stg03[2][i].f32EndZ = -2200.0f;
		pFogTable_Stg03[2][i].Col = 0xFF100030;

		pFogTable_Stg03[3][i].f32StartZ = 1.0f;
		pFogTable_Stg03[3][i].f32EndZ = 1200.0f;
		pFogTable_Stg03[3][i].Col = 0xFF000000;
		pFogTable_Stg03[3][i].u8Enable = 1;
	}
}

void TwinklePark_Load()
{
	LevelLoader(LevelAndActIDs_TwinklePark1, "SYSTEM\\data\\stg03_twinkle\\landtable0300.c.sa1lvl", &texlist_twinkle01);
	LevelLoader(LevelAndActIDs_TwinklePark2, "SYSTEM\\data\\stg03_twinkle\\landtable0301.c.sa1lvl", &texlist_twinkle02);
	LevelLoader(LevelAndActIDs_TwinklePark3, "SYSTEM\\data\\stg03_twinkle\\landtable0302.c.sa1lvl", &texlist_twinkle03);
	for (int i = 0; i < objLandTable[3][1]->ssCount; i++)
		objLandTable[3][1]->pLandEntry[i].blockbit = 0; // Remove block bit flags that cause pop-in
	ShareObj_Load();
	if (!ModelsLoaded_STG03)
	{
		// Code fixes
		WriteCall((void*)0x0061F684, FlowerBedFix); // Make flower beds look less awful by using a different rendering function
		WriteCall((void*)0x005F73B1, dispTPRoof_r); // Fix merry-go-round floor shadow flickering
		// Models
		object_newship_oya_oya = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\newship_oya.nja.sa1mdl"); // Pirate ship
		WriteJump(dispTPPirates, dispTPPirates_r); // Pirate ship display function
		object_fence_parksaku_parksaku = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\fence_parksaku.nja.sa1mdl"); // Fence (merged meshes 0+1 and made it trimesh instead of MESHSET_N)
		AddWhiteDiffuseMaterial(&object_fence_parksaku_parksaku.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_fence_parksaku_parksaku.basicdxmodel->mats[2]);
		object_park02_k_door_k_door = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park02_k_door.nja.sa1mdl"); // Double door
		object_tpobj_shutter_shutter = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_shutter.nja.sa1mdl"); // OShutter
		WriteData<1>((char*)0x0062429F, 0x68u); // Change meshset ID for UV animation in Shutter (5 to 3)
		WriteData<1>((char*)0x0062434A, 0x68u); // Change meshset ID for UV animation in Shutter (5 to 3)
		object_flyasiba_asiba_asiba = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\flyasiba_asiba.nja.sa1mdl"); // OFlyer
		object_park02_go_center_go_center = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park02_go_center.nja.sa1mdl"); // Merry-go-round (swapped materials/meshsets 0 and 2 for compatibility with DX animation code)
		object_uma_uma_uma = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\uma_uma.nja.sa1mdl"); // Horsies
		object_park02_bowl_bowl = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park02_bowl.nja.sa1mdl"); // Bowling ball
		object_park02_bowldoor_bowldoor = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park02_bowldoor.nja.sa1mdl"); // Bowling door
		object_mado_outmado_outmado = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\mado_outmado.nja.sa1mdl"); // OBowWindow
		object_park03_futa_a_futa_a = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park03_futa_a.nja.sa1mdl"); // Trap door
		object_park03_lr_door_lr_door = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park03_lr_door.nja.sa1mdl"); // ODoor
		object_tpobj_demado_demado = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_demado.nja.sa1mdl"); // O Foothold
		object_tpobj_kadann00_kadann00 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann00.nja.sa1mdl"); // Yellow flower pot (wall)
		object_tpobj_kadann01_kadann01 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann01.nja.sa1mdl"); // Pink flower pot (wall)
		object_tpobj_kadann02_kadann02 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann02.nja.sa1mdl"); // Yellow flower pot
		object_tpobj_kadann03_kadann03 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann03.nja.sa1mdl"); // Pink flower pot
		object_tpobj_kadann04_kadann04 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann04.nja.sa1mdl"); // Yellow flower bed	
		object_tpobj_kadann05_kadann05 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_kadann05.nja.sa1mdl"); // Pink flower bed
		object_yane_turnyane_turnyane = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\yane_turnyane.nja.sa1mdl"); // Spinning roof
		object_tpobj_w_asiba_w_asiba = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\tpobj_w_asiba.nja.sa1mdl"); // Lilypad
		object_park01_monitor_monitor = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\park01_monitor.nja.sa1mdl"); // Monitor in Act 1 (meshes 4 and 5 swapped for UV code compatibility)
		object_pinall_pin_pin = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\pinall_pin.nja.sa1mdl"); // Bowling pin
		object_courseter_courster_courster = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\courseter_courster.nja.sa1mdl"); // Rollercoaster
		action_hata01_hata.object = LoadModel("system\\data\\stg03_twinkle\\common\\models\\hata01_hata.nja.sa1mdl"); // Flag 1
		action_hata02_hata02.object = LoadModel("system\\data\\stg03_twinkle\\common\\models\\hata02_hata02.nja.sa1mdl"); // Flag 2
		action_lighthata_lump.object = LoadModel("system\\data\\stg03_twinkle\\common\\models\\lighthata_lump.nja.sa1mdl"); // OFlagWLamp
		// OFlagWLamp light
		AddAlphaRejectMaterial(&object_cllight_cllight.basicdxmodel->mats[0]);
		// OLamp
		AddAlphaRejectMaterial(&object_tp_obj_hunger_hunger.child->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_tp_obj_hunger_hunger.child->sibling->basicdxmodel->mats[0]);
		// Bowling
		WriteCall((void*)0x00621FE5, RenderCatapult); // Bowling catapult fix
		// Barrel
		object_boom_boomtaru_boomtaru = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\boom_boomtaru.nja.sa1mdl"); // Barrel
		object_boom_hahen1_hahen1 = *LoadModel("system\\data\\stg03_twinkle\\common\\models\\boom_hahen1.nja.sa1mdl"); // Barrel (destroyed) part 1
		AddWhiteDiffuseMaterial(&object_boom_boomtaru_boomtaru.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_boom_boomtaru_boomtaru.basicdxmodel->mats[2]);
		ModelsLoaded_STG03 = true;
	}
}