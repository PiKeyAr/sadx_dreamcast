#pragma once

#include <SADXModLoader.h>

TaskFunc(dispTPPirates, 0x00620BC0); // Twinkle Park ships draw function

DataPointer(NJS_ACTION, action_hata02_hata02, 0x027C295C);
DataPointer(NJS_ACTION, action_hata01_hata, 0x027C26B4);
DataPointer(NJS_ACTION, action_lighthata_lump, 0x027C2EA4);

void ShareObj_Load();