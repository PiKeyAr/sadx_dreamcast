#include "stdafx.h"

static NJS_COLOR DebugFontColorBK;
static float DebugFontSizeBK;

// Saves debug font color and size.
void BackupDebugFontSettings()
{
	DebugFontColorBK = DebugFontColor;
	DebugFontSizeBK = DebugFontSize;
}

// Restores debug font color and size.
void RestoreDebugFontSettings()
{
	DebugFontColor = DebugFontColorBK;
	DebugFontSize = DebugFontSizeBK;
}