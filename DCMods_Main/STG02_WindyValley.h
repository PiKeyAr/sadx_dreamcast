#pragma once

#include <SADXModLoader.h>

enum class TornadoFogMode // Fog gets thicker as Sonic gets closer to the tornado, then returns to original state once Sonic is sucked into the tornado.
{
	NoAction = 0, // Original state.
	ExpandLow = 1, // Make fog thicker.
	ExpandMid = 2, // Make fog even thicker.
	Maintain = 3, // Maintain fog thickness.
	Shrink = 4 // Restore fog thickness to original state.
};

DataPointer(NJS_MODEL_SADX, model_windobj_hasib_hasib, 0x00C1DDF8);
DataPointer(NJS_MODEL_SADX, model_windobj_tatehimo_tatehimo, 0x00C1E168);
DataPointer(NJS_MODEL_SADX, model_wvobj_popo_popo, 0x00C1D068);
DataPointer(NJS_MODEL_SADX, model_wvobj_tampopo_tampopo, 0x00C1DAB4);
DataPointer(NJS_MODEL_SADX, model_wvobj_iwaita_iwaita, 0x00C185E0);
DataPointer(NJS_MODEL_SADX, model_windobj_yokohimo_yokohimo, 0x00C1673C);
DataPointer(NJS_MODEL_SADX, model_windobj_haneobj_c_haneobj_c, 0x00C2AF1C);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb1_hane_cb1, 0x00C2B060);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb2_hane_cb2, 0x00C2B40C);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb3_hane_cb3, 0x00C2B550);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb4_hane_cb4, 0x00C2B834);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb5_hane_cb5, 0x00C2C134);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb6_hane_cb6, 0x00C2C2E8);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb7_hane_cb7, 0x00C2C75C);
DataPointer(NJS_MODEL_SADX, model_windobj_hane_cb8_hane_cb8, 0x00C2CAF8);
DataPointer(NJS_MODEL_SADX, model_windobj_bridge_ab07_bridge_ab07, 0x00C0EC2C);
DataPointer(NJS_MODEL_SADX, model_wvobj_brock_moto_brock_moto, 0x00C1673C);
DataPointer(NJS_MODEL_SADX, model_wvobj_ha_green_ha_green, 0x00C15B00);
DataPointer(NJS_MODEL_SADX, model_wvobj_ha_red_ha_red, 0x00C158B4);
DataPointer(NJS_MODEL_SADX, model_wvobj_ha_yellow_ha_yellow, 0x00C159D0);
DataPointer(NJS_OBJECT, object_wvobj_popo_hana1, 0x00C1C848);
DataPointer(NJS_OBJECT, object_wvobj_popo_hana2, 0x00C1C648);
DataPointer(NJS_OBJECT, object_wvobj_tampopo_wata, 0x00C1D1B0);
DataPointer(NJS_OBJECT, object_kazeroop_kazegate_gatehane_a, 0x00C30C44);
DataPointer(NJS_OBJECT, object_kazeroop_kazegate_gatehane_b, 0x00C305A4);
DataPointer(NJS_OBJECT, object_kazeroop_kazegate_gatehane_c, 0x00C2FF04);
DataPointer(NJS_OBJECT, object_windobj_hasib_end_hasi_hane1, 0x00C278F0);
DataPointer(NJS_OBJECT, object_windobj_hasib_end_hasi_hane2, 0x00C27200);
DataPointer(NJS_OBJECT, object_windobj_tramp_ami_a, 0x00C21E88);
DataPointer(NJS_OBJECT, object_windobj_tramp_ami_b, 0x00C21B10);
DataPointer(NJS_OBJECT, object_windobj_kazami_kubi, 0x00C365AC);
DataPointer(NJS_OBJECT, object_windobj_kazami_hane, 0x00C35F44);