#include "stdafx.h"
#include "FunctionHook.h"
#include "Common_FramerateFixes.h"

// Main variables
static int g_CurrentFrame_old = 0; // Backup of FramerateSetting
static int ulGlobalTimer_half = 0; // Half FrameCounter
static int loop_count_half = 1; // MissedFrames at 30 FPS

// All levels
float BigHudFix_float = 2.0f; // Big HUD flashing speed, for both 30 and 60 FPS because it's broken in SADX
short RingCountFlashSpeed = 512;
float InvincibilitySpeed = 0.84f;
short SpinnerYAnimationSpeedOverride = 384;
short SpinnerXAnimationSpeedOverride = 288;
short SpinnerZAnimationSpeedOverride = 416;
short SpinnerBladesAnimationSpeedOverride = 6144;
short EmeraldPieceAnimationSpeedOverride = 128;
float TailsWiggleSpeed_Run = 0.005f;
float TailsWiggleSpeed_RunX2 = 0.01f;
float TailsWiggleSpeed_Rotation = 2048.0f;
float TailsWiggleSpeed_RotationX2 = 4096.0f;
short Tails_819 = 819;
short Tails_20480 = 20480;
short Tails_2048 = 2048;
short Tails_1792 = 1792;
short Tails_1280 = 1280;
short Tails_1536 = 1536;
short HintMonitorAnimationSpeedOverride = 364;
char LeonTimer1 = 20;
char LeonTimer2 = 60;
int FrameIncrement_Tails = 1; // Tails' speed on the character select screen

// Emerald Coast
char TakiSpeed = 8; // Waterfall speed

// Windy Valley
double OSetiffNegativeSpeedOverride = -0.0013888885f; // Debris
short TornadoSpeed = 172;
float TornadoCenterLayer = 1.5f;
double OHaneaSpeedOverride = 0.0013888885f;
float LeafPathRotxyOverride = 512.0f;
float LeafPathPositionOverride = 0.444999995f; 
float WCWindSpeedOverride = 0.05f;

// Twinkle Park
float Flag1AnimationSpeedOverride = 0.2f;

// Red Mountain
float OGLSpeedOverride = 0.25f;

// Sky Deck
float OMekaSpeedOverride = 0.5f;

// Casinopolis
double OKaizAnimationSpeedOverride = 0.001388885f;
int OCrystalAnimationSpeedOverride = 168;
float NightsOverlayTransparencySubtract = 0.05f;

// Ice Cap
float AvalancheMultiplier = 12.5f;

// Lost World
float OTPanel1SpeedOverride = 0.0084745765f;
char OTPanelTimer = 120;
char LostWorldDoorFix = 34;
char LostWorldDoorFix1 = 6;

// Final Egg
float OFunFrame = 0.0f; // Floating Fan Animation Speed Tweak

// Station Square
float IceKeyRotationSpeed = 0.5f;

// Animals
float BubbleMovementSpeed = 0.0049999999f; // 0.0099999998 at 60
float BubbleMovementSpeed2 = 0.0249999985f; // 0.049999997 at 60
float BubbleMovementSpeed3 = 0.039999999f; // 0.079999998 at 60
float BubbleRotationSpeed = 91.022225f; // 182.04445 at 60
float AnimalMultiplier2 = 0.44999999f; // 0.89999998 at 60
float AnimalGravity = 0.140475005f; // 0.28095001 at 60
float AnimalPositionMultiplier = 0.125f; // 0.25 at 60
float DistanceMultiplier_3 = 1.5f;
float DistanceMultiplier_1 = 0.5f;
float DistanceMultiplier_4 = 2.0f;
float DistanceMultiplier_8 = 4.0f;

// Functions

// Gets a 30 FPS-like frame counter
int GetulGlobalTimer_half()
{
	return g_CurrentFrame == 1 ? ulGlobalTimer_half : ulGlobalTimer;
}

// Trampolines and hacks

// Bomb fuse effect display funciton
void EffectSpark_hack(task* tp)
{
	taskwk* twp; // edi
	unsigned __int16 wtimer; // ax
	float sqr; // st7
	float addx; // st7
	float addy; // st6
	float addz; // [esp+18h] [ebp+4h]
	float dist; // [esp+0h] [ebp-14h]

	twp = tp->twp;
	wtimer = twp->wtimer;
	twp->wtimer = wtimer + 1;
	if (dsCheckViewV(&twp->pos, 5.0f))
	{
		// TODO: Figure out how to do this better
		if (ulGlobalTimer % 2 == 0)
		{
			if (wtimer <= 0xFu)
			{
				twp->scl.x = twp->scl.x * 0.975f;
				twp->scl.y = twp->scl.y * 0.975f - 0.085f;
				twp->scl.z = twp->scl.z * 0.975f;
				twp->pos.x = twp->pos.x + twp->scl.x;
				twp->pos.y = twp->pos.y + twp->scl.y;
				twp->pos.z = twp->pos.z + twp->scl.z;
				goto draw;
			}
			else
			{
				FreeTask(tp);
			}
		}
		else
			goto draw;
	draw:
		dist = twp->scl.z * twp->scl.z + twp->scl.y * twp->scl.y + twp->scl.x * twp->scl.x;
		sqr = 1.0f / squareroot(dist);
		addx = sqr * twp->scl.x * 0.3f;
		addy = sqr * twp->scl.y * 0.3f;
		addz = sqr * twp->scl.z * 0.3f;
		l_point[0].x = twp->pos.x + addx;
		l_point[0].y = twp->pos.y + addy;
		l_point[0].z = twp->pos.z + addz;
		l_point[1].x = twp->pos.x - addx;
		l_point[1].y = twp->pos.y - addy;
		l_point[1].z = twp->pos.z - addz;
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njDrawLine3D(&spark_point3col, 1, 0);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
	}
	else
		FreeTask(tp);
}

// Bomb fuse effect trampoline
static Trampoline* EffectSpark_t = nullptr;
static void __cdecl EffectSpark_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(EffectSpark);
	if (ModConfig::EnableSpeedFixes)
		EffectSpark_hack(tp);
	else
		original(tp);
}

// Trampoline for Amy's hammer effect
static Trampoline* AmyEffectPutSpdDwnHeart_t = nullptr;
static void __cdecl AmyEffectPutSpdDwnHeart_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(AmyEffectPutSpdDwnHeart);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
}

// Trampoline for Emerald Coast fishes
static Trampoline* Object_beach_osakana_Exec_t = nullptr;
static void __cdecl Object_beach_osakana_Exec_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(Object_beach_osakana_Exec);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
		Object_beach_osakana_Draw(tp);
}

// Trampoline for Casino flies
static Trampoline* ObjectCasinoHaeExec_t = nullptr;
static void __cdecl ObjectCasinoHaeExec_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectCasinoHaeExec);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
		ObjectCasinoHaeDraw(tp);
}

// Trampoline for Casino smoke
static Trampoline* Exec_87_Wsr2_t = nullptr;
static void __cdecl Exec_87_Wsr2_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(Exec_87_Wsr2);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
}

// Trampoline for Casino jackpot sprite
static Trampoline* Normal_32_Yaku_t = nullptr;
static void __cdecl Normal_32_Yaku_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(Normal_32_Yaku);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
	{
		LoopTaskC(tp);
		Disp_113_PinballJackpot(tp);
	}
}

// Trampoline for Lost World fire effect
static Trampoline* ObjectRuinFire_t = nullptr;
static void __cdecl ObjectRuinFire_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(ObjectRuinFire);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
		DrawFire(tp);
}

// Trampoline for upgrade sparks
static Trampoline* effectKiran2_t = nullptr;
static void __cdecl effectKiran2_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(effectKiran2);
	taskwk* twp = tp->twp;
	float scale = twp->scl.z;
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else if (scale < 7.0f)
		drawEffectKiran2(tp);
	else
		FreeTask(tp);
}

// Hack to draw sparks with depth
void EffectKiran2DrawSpriteHack(NJS_SPRITE *sp, int n, NJD_SPRITE attr, LATE zfunc_type)
{
	if (!IsChaoGarden)
	{
		late_z_ofs___ = 2000.0f;
		SetMaterial(1.0f, 1.0f, 1.0f, 1.0f);
		late_DrawSprite3D(sp, n, attr, LATE_NO);
		late_z_ofs___ = 0;
	}
	else
	{
		late_DrawSprite3D(sp, n, attr, zfunc_type);
	}
}

// Trampoline for Chaos 0 rain effect
static Trampoline* RainEffect_t = nullptr;
static void __cdecl RainEffect_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(RainEffect);
	taskwk* twp = tp->twp;
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
	{
		float cnt = twp->counter.f - 0.033333335f;
		twp->counter.f = cnt;
		if (cnt >= 0.0f)
		{
			DisplayDrop(tp);
		}
	}
}

// Hook for Egg Hornet jets
FastcallFunctionHook<int, Egm1JetWk*> PtnChgEgm1Jet_h(PtnChgEgm1Jet);
static int __fastcall PtnChgEgm1Jet_r(Egm1JetWk* wkp)
{
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		return PtnChgEgm1Jet_h.Original(wkp);
	else
		return 0;
}

// Unidus hack
void CreateFireHack(NJS_VECTOR* pos, NJS_VECTOR* vel, float scl)
{
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		CreateFire(pos, vel, scl);
}

// Bubbles display function (exact copy of the original at 0x007A7E00)
void bubble_small_draw(task* tp)
{
	bbl_small_taskwk* twp; // edi
	bbl_small_work* smallwk; // esi
	int wtimer; // edi
	float rad; // st7
	float pz; // eax
	float range; // [esp+0h] [ebp-8h]

	twp = (bbl_small_taskwk*)tp->twp;
	if (!loop_count)
	{
		range = (twp->radius + 410.0f) * (twp->radius + 410.0f);
		if (!CheckRangeOutWithR(tp, range))
		{
			smallwk = twp->top_list_p;
			njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
			njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
			njSetConstantMaterial(&default_argb);
			wtimer = twp->bbls_count;
			if (wtimer > 0)
			{
				do
				{
					rad = smallwk->radius * 0.03125f;
					sprite_o_bubble_0.p.x = smallwk->pos.x;
					sprite_o_bubble_0.p.y = smallwk->pos.y;
					sprite_o_bubble_0.p.z = smallwk->pos.z;
					sprite_o_bubble_0.sx = rad;
					sprite_o_bubble_0.sy = rad;
					njDrawSprite3D(&sprite_o_bubble_0, 0, NJD_SPRITE_ALPHA | NJD_SPRITE_SCALE | NJD_SPRITE_COLOR);
					smallwk = smallwk->next;
					--wtimer;
				} while (wtimer);
			}
			ResetMaterial();
			njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
			njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		}
	}
}

// Trampoline for bubbles
static Trampoline* bubble_small_exe_t = nullptr;
static void __cdecl bubble_small_exe_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(bubble_small_exe);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	bubble_small_draw(tp);
}

// Hack for Big's HUD
void BigHudFix(_SC_NUMBERS* pscn)
{
	if (ssNumRing != 0)
		pscn->color = 0xFFFFFFFF;
	DrawSNumbers(pscn);
}

// Trampoline for Red Mountain lamps
static Trampoline* Disp_121_Lamp_t = nullptr;
static void __cdecl Disp_121_Lamp_r(task* tp)
{
	taskwk* twp = tp->twp;
	const auto original = TARGET_DYNAMIC(Disp_121_Lamp);
	if (g_CurrentFrame >= 2 || ulGlobalTimer % 2 == 0)
		original(tp);
	else
	{
		__int16 wtimer = twp->wtimer;
		float ftimer = twp->timer.f - 0.05f;
		twp->wtimer = wtimer - 1;
		twp->timer.f = ftimer;
		original(tp);
	}
}

// Trampoline for Zero sparks
static Trampoline* exec_Emission_t = nullptr;
static void __cdecl exec_Emission_r(task* tp)
{
	const auto original = TARGET_DYNAMIC(exec_Emission);
	if (g_CurrentFrame > 2 || (g_CurrentFrame < 2 && ulGlobalTimer % 2 == 0))
		original(tp);
	else
		disp_Emission(tp);
}

// Trampoline for Zero's FVF shit
static Trampoline* exec_ChainElec_t = nullptr;
static void __cdecl exec_ChainElec_r(task *tp)
{
	const auto original = TARGET_DYNAMIC(exec_ChainElec);
	if (g_CurrentFrame > 2 || (g_CurrentFrame < 2 && ulGlobalTimer % 2 == 0))
		tp->twp->wtimer++;
	original(tp);
}

// Final Egg fan hack
void OFunAnimationOverride(NJS_ACTION* action, float frame, float scale)
{
	DrawAction(action, OFunFrame, LATE_MAT, scale, (void(__cdecl*)(NJS_MODEL_SADX*, int, int))__DrawModel);
}

// Hack for OTankH
void OTankHAnimationOverride(NJS_OBJECT* model, NJS_MOTION* motion, float frame, LATE flags, float scale)
{
	late_DrawMotionClipMesh(model, motion, OFunFrame, flags, scale);
}

// Initialize animal fixes
void AnimalFixes_Init()
{
	// sub_4D72B0 (Bubble)
	WriteData((float**)0x004D73FB, &BubbleMovementSpeed);
	WriteData((float**)0x004D7405, &BubbleMovementSpeed);
	WriteData((float**)0x004D740F, &BubbleMovementSpeed);
	WriteData((float**)0x004D73C4, &BubbleMovementSpeed2);
	WriteData((float**)0x004D73CE, &BubbleMovementSpeed2);
	WriteData((float**)0x004D73D8, &BubbleMovementSpeed2);
	WriteData((float**)0x004D7395, &BubbleMovementSpeed3);
	WriteData((float**)0x004D72F3, &BubbleRotationSpeed);
	// sub_4D8A10 (Distance calculation)
	WriteData((float**)0x004D8B01, &DistanceMultiplier_3);
	WriteData((float**)0x004D8AE0, &DistanceMultiplier_3);
	WriteData((float**)0x004D8B07, &DistanceMultiplier_1);
	WriteData((float**)0x004D8AE6, &DistanceMultiplier_4);
	WriteData((float**)0x004D8B63, &DistanceMultiplier_3);
	WriteData((float**)0x004D8B69, &DistanceMultiplier_1);
	WriteData((float**)0x004D8B36, &DistanceMultiplier_3);
	WriteData((float**)0x004D8B3C, &DistanceMultiplier_4);
	WriteData((float**)0x004D8C4A, &DistanceMultiplier_8);
	WriteData((float**)0x004D8C58, &DistanceMultiplier_8);
	WriteData((float**)0x004D8C66, &DistanceMultiplier_8);
	// sub_4D7A40 (Goma, Rako, Gori, Zou, Mogu, Koar)
	WriteData((float**)0x004D7ACB, &BubbleRotationSpeed);
	WriteData((float**)0x004D7B61, &AnimalGravity);
	WriteData((float**)0x004D7B44, &AnimalPositionMultiplier);
	WriteData((float**)0x004D7B54, &AnimalPositionMultiplier);
	// sub_4D7C30 (Pen, Suka)
	WriteData((float**)0x004D7C56, &BubbleMovementSpeed);
	WriteData((float**)0x004D7C69, &BubbleMovementSpeed);
	WriteData((float**)0x004D7C78, &AnimalGravity);
	// sub_4D7B70 (Called by functions used by Pen and Usa)
	WriteData((float**)0x004D7BB2, &BubbleRotationSpeed);
	// sub_4D7C90 (Banb, Lion)
	WriteData((float**)0x004D7D69, &AnimalMultiplier2);
	WriteData((float**)0x004D7D75, &AnimalMultiplier2);
	WriteData((float**)0x004D7D81, &AnimalMultiplier2);
	// sub_4D7D90 (hopping)
	WriteData((float**)0x004D7DBC, &AnimalGravity);
	WriteData((float**)0x004D7E56, &BubbleMovementSpeed);
	WriteData((float**)0x004D7EF6, &AnimalMultiplier2);
	WriteData((float**)0x004D7F02, &AnimalMultiplier2);
	WriteData((float**)0x004D7F0E, &AnimalMultiplier2);
}

// Apply non-pointer variables
void SpeedFixes_ApplyValues()
{
	// Ring count when red (Big)
	WriteData((short*)0x0046CE6B, RingCountFlashSpeed);
	// Ring count when red
	WriteData((short*)0x00425DBB, RingCountFlashSpeed);
	// General
	WriteData((short*)0x004AFB90, SpinnerYAnimationSpeedOverride);
	WriteData((short*)0x004AFB8A, SpinnerXAnimationSpeedOverride);
	WriteData((short*)0x004AFB85, SpinnerZAnimationSpeedOverride);
	WriteData((short*)0x004AFD67, SpinnerBladesAnimationSpeedOverride);
	WriteData((short*)0x007A9BFF, HintMonitorAnimationSpeedOverride);
	WriteData<1>((char*)0x004A6B8C, LeonTimer1); // Leon timer 1
	WriteData<1>((char*)0x004A81C1, LeonTimer2); // Leon timer 2
	WriteData((short*)0x004A2CA6, EmeraldPieceAnimationSpeedOverride);
	// Tails
	WriteData((short*)0x0045DA2C, Tails_1536); // 1 
	WriteData((short*)0x0045DA0C, Tails_1280); // 2
	WriteData((short*)0x0045DA14, Tails_1792); // 3
	WriteData((short*)0x0045DA1C, Tails_2048); // 4
	WriteData((short*)0x0045DA7D, Tails_20480); // 5
	WriteData((short*)0x004612DA, Tails_819); // 6
	WriteData((short*)0x004612ED, Tails_819); // 7
	// Emerald Coast
	if (ModConfig::EnabledLevels[LevelIDs_EmeraldCoast])
		WriteData((char*)0x004FB8BE, TakiSpeed);
	// Windy Valley
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley])
		WriteData((short*)0x004DD8C8, TornadoSpeed);
	// Lost World
	if (ModConfig::EnabledLevels[LevelIDs_LostWorld])
	{
		WriteData((char*)0x005E8BBD, OTPanelTimer);
		WriteData((char*)0x005E7C3A, LostWorldDoorFix);
		WriteData((char*)0x005E78E1, LostWorldDoorFix);
		WriteData((char*)0x005E7C63, LostWorldDoorFix1);
		WriteData((char*)0x005E7841, LostWorldDoorFix1);
	}
}

void SpeedFixes_Init()
{
	// General
	AmyEffectPutSpdDwnHeart_t = new Trampoline(0x004C5BC0, 0x004C5BC9, AmyEffectPutSpdDwnHeart_r);
	EffectSpark_t = new Trampoline(0x004CE830, 0x004CE837, EffectSpark_r);
	effectKiran2_t = new Trampoline(0x004BAF10, 0x004BAF15, effectKiran2_r);
	bubble_small_exe_t = new Trampoline(0x007A88B0, 0x007A88B5, bubble_small_exe_r);
	WriteCall((void*)0x004ADEF9, CreateFireHack); // Unidus
	WriteData((float**)0x004BA4A7, &InvincibilitySpeed);
	WriteData((int**)0x004A26B3, &loop_count_half); // Character bubbles
	AnimalFixes_Init();
	// Character upgrades
	WriteCall((void*)0x004BAEED, EffectKiran2DrawSpriteHack);
	WriteCall((void*)0x004BECBD, GetulGlobalTimer_half);
	WriteCall((void*)0x004BEBD4, GetulGlobalTimer_half);
	WriteCall((void*)0x004BEBC3, GetulGlobalTimer_half);
	WriteCall((void*)0x004BF088, GetulGlobalTimer_half);
	// Tails
	WriteData((float**)0x00461284, &TailsWiggleSpeed_Run);
	WriteData((float**)0x00461276, &TailsWiggleSpeed_Run);
	WriteData((float**)0x004613C0, &TailsWiggleSpeed_Rotation);
	WriteData((int**)0x0045AC4C, &FrameIncrement_Tails);
	// Big ring flashing HUD
	WriteData((float**)0x0046CE9B, &BigHudFix_float);
	WriteCall((void*)0x0046CECD, BigHudFix);
	// Emerald Coast
	if (ModConfig::EnabledLevels[LevelIDs_EmeraldCoast])
	{
		Object_beach_osakana_Exec_t = new Trampoline(0x004FC9C0, 0x004FC9C8, Object_beach_osakana_Exec_r);
	}
	// Windy Valley
	if (ModConfig::EnabledLevels[LevelIDs_WindyValley])
	{
		WriteData((float**)0x004DD8E7, &TornadoCenterLayer);
		WriteData((double**)0x004DF63E, &OHaneaSpeedOverride); // O Setiff Debris inside tornado
		WriteData((double**)0x004DF6C6, &OHaneaSpeedOverride); // O Setiff Debris inside tornado
		WriteData((double**)0x004DF6A2, &OSetiffNegativeSpeedOverride); // O Setiff Debris inside tornado
		WriteData((double**)0x004E1169, &OHaneaSpeedOverride); // Fans
		WriteData((double**)0x004E113E, &OHaneaSpeedOverride); // Fans
		WriteData((double**)0x004E1194, &OHaneaSpeedOverride); // Fans
		// Windpath Position 0.899
		WriteData((float**)0x004E4714, &LeafPathPositionOverride);
		WriteData((float**)0x004E471C, &LeafPathPositionOverride);
		WriteData((float**)0x004E4736, &LeafPathPositionOverride);
		WriteData((float**)0x004E473E, &LeafPathPositionOverride);
		WriteData((float**)0x004E4758, &LeafPathPositionOverride);
		WriteData((float**)0x004E4760, &LeafPathPositionOverride);
		WriteData((float**)0x004E44C3, &LeafPathPositionOverride);
		WriteData((float**)0x004E44BB, &LeafPathPositionOverride);
		WriteData((float**)0x004E44DD, &LeafPathPositionOverride);
		WriteData((float**)0x004E44E5, &LeafPathPositionOverride);
		WriteData((float**)0x004E44FF, &LeafPathPositionOverride);
		WriteData((float**)0x004E4507, &LeafPathPositionOverride);
		// Rotation 1024
		WriteData((float**)0x004E478D, &LeafPathRotxyOverride); // Windpath Rx 
		WriteData((float**)0x004E47A6, &LeafPathRotxyOverride); // Windpath Ry
		WriteData((float**)0x004E422B, &WCWindSpeedOverride);
	}
	// Twinkle Park
	if (ModConfig::EnabledLevels[LevelIDs_TwinklePark])
	{
		WriteData((float**)0x006201FD, &Flag1AnimationSpeedOverride); // Flag1
		WriteData((float**)0x0062028D, &Flag1AnimationSpeedOverride); // Flag2
		WriteData((float**)0x0062059D, &Flag1AnimationSpeedOverride); // OFlagWLamp
		Disp_121_Lamp_t = new Trampoline(0x00606610, 0x00606617, Disp_121_Lamp_r);
	}
	// Red Mountain
	if (ModConfig::EnabledLevels[LevelIDs_RedMountain])
	{
		WriteData((float**)0x0060C885, &OGLSpeedOverride); // OGL Speed Tweak
		WriteData((float**)0x0060B361, &OGLSpeedOverride); // O Gear Speed Tweak
	}
	// Sky Deck
	if (ModConfig::EnabledLevels[LevelIDs_SkyDeck])
	{
		WriteCall((void*)0x005EE29D, OTankHAnimationOverride); // OTankH
		WriteData((float**)0x005F4146, &OMekaSpeedOverride); // OMeka OTutu	
	}
	// Casinopolis
	if (ModConfig::EnabledLevels[LevelIDs_Casinopolis])
	{
		ObjectCasinoHaeExec_t = new Trampoline(0x005C8B40, 0x005C8B48, ObjectCasinoHaeExec_r);
		Exec_87_Wsr2_t = new Trampoline(0x005C93F0, 0x005C93F9, Exec_87_Wsr2_r);
		Normal_32_Yaku_t = new Trampoline(0x005E12C0, 0x005E12C7, Normal_32_Yaku_r); // Pinball jackpot sprite
		WriteData((float**)0x005D70F6, &NightsOverlayTransparencySubtract);
		WriteData((double**)0x005C802C, &OKaizAnimationSpeedOverride); // OKaiza Animation Speed Tweak
		WriteData((double**)0x005C747C, &OKaizAnimationSpeedOverride); // OKaizb Animation Speed Tweak
		WriteData((double**)0x005C698C, &OKaizAnimationSpeedOverride); // Green Pirate / KaizC Animation Speed Tweak
		WriteData((double**)0x005C5E9C, &OKaizAnimationSpeedOverride); // Captain Pirate / KaizS Animation Speed Tweak
		WriteData((int**)0x005D3D68, &OCrystalAnimationSpeedOverride); // Spinning golden emerald Animation Speed Tweak	
	}
	// Lost World
	if (ModConfig::EnabledLevels[LevelIDs_LostWorld])
	{
		ObjectRuinFire_t = new Trampoline(0x005E82F0, 0x005E82F5, ObjectRuinFire_r);
		WriteData((float**)0x005E8F37, &OTPanel1SpeedOverride); // Multiplier
	}
	// Ice Cap
	if (ModConfig::EnabledLevels[LevelIDs_IceCap])
	{
		WriteData((float**)0x004EB391, &AvalancheMultiplier);
		WriteData((float**)0x004EB3B6, &AvalancheMultiplier);
	}
	// Final Egg
	if (ModConfig::EnabledLevels[LevelIDs_FinalEgg])
	{
		WriteCall((void*)0x005B75B6, OFunAnimationOverride);
	}
	// Chaos 0
	if (ModConfig::EnabledLevels[LevelIDs_Chaos0])
	{
		RainEffect_t = new Trampoline(0x00546140, 0x00546146, RainEffect_r);
	}
	// Chaos 4
	if (ModConfig::EnabledLevels[LevelIDs_Chaos4])
	{
		WriteData((int**)0x005533CB, &loop_count_half);
	}
	// Egg Hornet
	if (ModConfig::EnabledLevels[LevelIDs_EggHornet])
	{
		PtnChgEgm1Jet_h.Hook(PtnChgEgm1Jet_r);
	}
	// Station Square
	if (ModConfig::EnabledLevels[LevelIDs_StationSquare])
	{
		WriteData((float**)0x00637E7F, &IceKeyRotationSpeed);
	}
	// Zero
	if (ModConfig::EnabledLevels[LevelIDs_Zero])
	{
		exec_Emission_t = new Trampoline(0x0058B640, 0x0058B645, exec_Emission_r);
		exec_ChainElec_t = new Trampoline(0x0058C590, 0x0058C597, exec_ChainElec_r);
		WriteData((int**)0x0058F448, &loop_count_half);
		WriteData((int**)0x0058F413, &loop_count_half);
	}
	SpeedFixes_ApplyValues();
}

void SpeedFixes_OnFrame()
{
	// Half frame counters
	loop_count_half = (!loop_count && (g_CurrentFrame > 2 || (g_CurrentFrame < 2 && ulGlobalTimer % 2 == 0))) ? 0 : 1;
	if (ulGlobalTimer % 2 == 0)
		ulGlobalTimer_half++;
	// Increase other counters
	if (!ChkPause())
	{
		// 60 FPS
		if (g_CurrentFrame < 2)
		{
			FrameIncrement_Tails = (ulGlobalMode == MD_TITLE2) ? 2: FrameIncrement; // Tails' speed on the character select screen
			OFunFrame -= 0.25f;
			if (OFunFrame < 0)
				OFunFrame = 19;
		}
		// 30 FPS
		else
		{
			FrameIncrement_Tails = FrameIncrement; // Tails' speed on the character select screen
			OFunFrame += 1.0f;
			if (OFunFrame > 19)
				OFunFrame = 0;
		}
	}
	// Reapply fixes if the framerate setting has been changed
	if (g_CurrentFrame_old != g_CurrentFrame)
	{
		// Original values for 30 FPS
		if (g_CurrentFrame >= 2)
		{
			// Ice Key
			IceKeyRotationSpeed = 0.5f;
			// Ring count
			RingCountFlashSpeed = 1024;
			// Invincibility
			InvincibilitySpeed = 0.7f;
			// Ice Cap avalanche
			AvalancheMultiplier = 25.0f;
			// Animals
			BubbleMovementSpeed = 0.0099999998f;
			BubbleMovementSpeed2 = 0.04999999f;
			BubbleMovementSpeed3 = 0.079999998f;
			BubbleRotationSpeed = 182.04445f;
			AnimalMultiplier2 = 0.89999998f;
			AnimalGravity = 0.28095001f;
			AnimalPositionMultiplier = 0.25f;
			DistanceMultiplier_3 = 1.5f;
			DistanceMultiplier_1 = 0.5f;
			DistanceMultiplier_4 = 2.0f;
			DistanceMultiplier_8 = 4.0f;
			// General
			SpinnerYAnimationSpeedOverride = 768;
			SpinnerXAnimationSpeedOverride = 576;
			SpinnerZAnimationSpeedOverride = 832;
			SpinnerBladesAnimationSpeedOverride = 12288;
			HintMonitorAnimationSpeedOverride = 728;
			LeonTimer1 = 20;
			LeonTimer2 = 60;
			EmeraldPieceAnimationSpeedOverride = 256;
			// Tails
			TailsWiggleSpeed_Run = 0.01f;
			TailsWiggleSpeed_Rotation = 4096.0f;
			Tails_1536 = 1536;
			Tails_1280 = 1280;
			Tails_1792 = 1792;
			Tails_2048 = 2048;
			Tails_20480 = 20480;
			Tails_819 = 819;
			// Emerald Coast
			TakiSpeed = 8;
			// Windy Valley
			TornadoSpeed = 345;
			TornadoCenterLayer = 3.0f;
			OHaneaSpeedOverride = 0.002777777f;
			OSetiffNegativeSpeedOverride = -0.002777777f;
			LeafPathRotxyOverride = 1024.0f;
			LeafPathPositionOverride = 0.88999999f;
			WCWindSpeedOverride = 0.05f;
			// Twinkle Park
			Flag1AnimationSpeedOverride = 0.4f;
			// Sky Deck
			OMekaSpeedOverride = 1.0f;
			// Red Mountain
			OGLSpeedOverride = 0.5f;
			// Casinopolis
			OKaizAnimationSpeedOverride = 0.00277777f;
			OCrystalAnimationSpeedOverride = 336;
			NightsOverlayTransparencySubtract = 0.1f;
			// LostWorld
			OTPanel1SpeedOverride = 0.016949153f;
			OTPanelTimer = 60;
			LostWorldDoorFix = 17;
			LostWorldDoorFix1 = 3;
			// Perfect Chaos
			PerfectChaosTornadoSpeed1 = 3276;
			PerfectChaosTornadoSpeed2 = 0;
			PerfectChaosTornadoSpeed3 = 1638;
			PerfectChaosTornadoSpeed4 = 1638;
		}
		// 60 FPS values
		else
		{
			// Ice Key
			IceKeyRotationSpeed = 0.25f;
			// Ring count
			RingCountFlashSpeed = 512;
			// Invincibility
			InvincibilitySpeed = 0.84f;
			// Ice Cap avalanche
			AvalancheMultiplier = 12.5f;
			// Animals
			BubbleMovementSpeed = 0.0049999999f;
			BubbleMovementSpeed2 = 0.0249999985f;
			BubbleMovementSpeed3 = 0.039999999f;
			BubbleRotationSpeed = 91.022225f;
			AnimalMultiplier2 = 0.44999999f;
			AnimalGravity = 0.140475005f;
			AnimalPositionMultiplier = 0.125f;
			DistanceMultiplier_3 = 3.0f;
			DistanceMultiplier_1 = 1.0f;
			DistanceMultiplier_4 = 4.0f;
			DistanceMultiplier_8 = 8.0f;
			// General
			SpinnerYAnimationSpeedOverride = 384;
			SpinnerXAnimationSpeedOverride = 288;
			SpinnerZAnimationSpeedOverride = 416;
			SpinnerBladesAnimationSpeedOverride = 6144;
			HintMonitorAnimationSpeedOverride = 364;
			LeonTimer1 = 10;
			LeonTimer2 = 30;
			EmeraldPieceAnimationSpeedOverride = 128;
			// Tails
			TailsWiggleSpeed_Run = 0.005f;
			TailsWiggleSpeed_Rotation = 2048.0f;
			Tails_1536 = 768;
			Tails_1280 = 640;
			Tails_1792 = 896;
			Tails_2048 = 1024;
			Tails_20480 = 10240;
			Tails_819 = 409;
			// Emerald Coast
			TakiSpeed = 0;
			// Windy Valley
			TornadoSpeed = 172;
			TornadoCenterLayer = 1.5f;
			OHaneaSpeedOverride = 0.0013888885f;
			OSetiffNegativeSpeedOverride = -0.0013888885f; // Debris
			LeafPathRotxyOverride = 512.0f;
			LeafPathPositionOverride = 0.444999995f;
			WCWindSpeedOverride = 0.05f;
			// Twinkle Park
			Flag1AnimationSpeedOverride = 0.2f;
			// Red Mountain
			OGLSpeedOverride = 0.25f;
			// Sky Deck
			OMekaSpeedOverride = 0.5f;
			// Casinopolis
			OKaizAnimationSpeedOverride = 0.001388885f;
			OCrystalAnimationSpeedOverride = 168;
			NightsOverlayTransparencySubtract = 0.05f;
			// Lost World
			OTPanel1SpeedOverride = 0.0084745765f;
			OTPanelTimer = 120;
			LostWorldDoorFix = 34;
			LostWorldDoorFix1 = 6;
			// Perfect Chaos
			PerfectChaosTornadoSpeed1 = 6552;
			PerfectChaosTornadoSpeed2 = 0;
			PerfectChaosTornadoSpeed3 = 3276;
			PerfectChaosTornadoSpeed4 = 3276;
		}
		SpeedFixes_ApplyValues();
		g_CurrentFrame_old = g_CurrentFrame;
	}
}