#include "stdafx.h"

static bool ModelsLoaded_ShareObj;

DataArray(NJS_MATERIAL, matlist_eisei_eiseioya_ita, 0x038AE590, 2); // Satellite ship materials for white diffuse

// OPole
void __cdecl OPole_RenderTransparentPartCallback(task* tsk)
{
	taskwk* twp;
	NJS_OBJECT* model;
	Angle x, y, z;

	twp = tsk->twp;
	model = (NJS_OBJECT*)(twp->counter.ptr);
	if (!loop_count)
	{
		twp->wtimer += twp->btimer;
		if (twp->wtimer >= 256u)
			twp->wtimer = 0;
		for (int i = 0; i < 12; i++)
			model->sibling->basicdxmodel->meshsets[1].vertuv[i].v += twp->wtimer;
		njSetTexture(&texlist_obj_shareobj);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		z = twp->ang.z;
		if (z)
			njRotateZ(0, z);
		x = twp->ang.x;
		if (x)
			njRotateX(0, x);
		y = twp->ang.y;
		if (y)
			njRotateY(0, y);
		njTranslateV(0, (NJS_VECTOR*)model->pos);
		DrawModelEx(model->sibling->basicdxmodel, LATE_MAT);
		njPopMatrix(1u);
		for (int i = 0; i < 12; i++)
			model->sibling->basicdxmodel->meshsets[1].vertuv[i].v -= twp->wtimer;
	}
}

void __cdecl OPole_Render(task* tsk)
{
	taskwk* twp;
	Angle x, y, z, x_beam;
	NJS_OBJECT* model;

	twp = tsk->twp;
	model = (NJS_OBJECT*)(twp->counter.ptr);
	if (!loop_count)
	{
		njSetTexture(&texlist_obj_shareobj);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		z = twp->ang.z;
		if (z)
			njRotateZ(0, z);
		x = twp->ang.x;
		if (x)
			njRotateX(0, x);
		y = twp->ang.y;
		if (y)
			njRotateY(0, y);
		njTranslateV(0, (NJS_VECTOR*)model->pos);
		dsDrawModel(model->basicdxmodel);
		late_DrawModelEx(model->sibling->sibling->basicdxmodel, LATE_WZ);
		njPushMatrix(0);
		njTranslateV(0, (NJS_VECTOR*)model->child->pos);
		x_beam = twp->value.w[1];
		if (x_beam)
			njRotateX(0, x_beam);
		late_DrawModel(model->child->basicdxmodel, LATE_MAT);
		njPopMatrix(1u);
		njPopMatrix(1u);
		// The transparent part of OPole has UV animation unique to each instance of the object.
		// Using the regular "late" draw functions wouldn't work in this case, so a callback is used instead.
		late_SetFunc((void(__cdecl*)(void*))OPole_RenderTransparentPartCallback, (void*)tsk, 0, 0);
	}
}

static void __declspec(naked) OPole_Display_asm()
{
	__asm
	{
		push eax
		call OPole_Render
		//add esp, 4
		pop eax
		retn
	}
}

// Arch1
void __cdecl RenderArch1(task* tsk)
{
	taskwk* twp;
	Angle x, y, z;
	NJS_OBJECT* njsobj;
	twp = tsk->twp;
	njsobj = (NJS_OBJECT*)(twp->counter.ptr);
	if (!loop_count)
	{
		twp->wtimer += twp->btimer;
		if (twp->wtimer >= 0x100u)
		{
			twp->wtimer = 0;
		}
		for (int i = 0; i < 56; i++)
			njsobj->child->basicdxmodel->meshsets[0].vertuv[i].v -= twp->wtimer;
		njSetTexture(&texlist_obj_shareobj);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		z = twp->ang.z;
		if (z)
			njRotateZ(0, z);
		x = twp->ang.x;
		if (x)
			njRotateX(0, x);
		y = twp->ang.y;
			njRotateY(0, y);
			DrawModelEx(njsobj->child->basicdxmodel, LATE_MAT);
		njPopMatrix(1u);
		for (int i = 0; i < 56; i++)
			njsobj->child->basicdxmodel->meshsets[0].vertuv[i].v += twp->wtimer;
	}
}

void __cdecl Arch1_Render(task* tsk)
{
	taskwk* twp;
	Angle x, y, z;
	NJS_OBJECT* njsobj;
	twp = tsk->twp;
	njsobj = (NJS_OBJECT*)(twp->counter.ptr);
	if (!loop_count)
	{
		njSetTexture(&texlist_obj_shareobj);
		njPushMatrix(0);
		njTranslateV(0, &twp->pos);
		z = twp->ang.z;
		if (z)
			njRotateZ(0, z);
		x = twp->ang.x;
		if (x)
			njRotateX(0, x);
		y = twp->ang.y;
		njRotateY(0, y);
		dsDrawModel(njsobj->basicdxmodel);
		njPopMatrix(1u);
	}
	// Same as OPole
	late_SetFunc((void(__cdecl*)(void*))RenderArch1, (void*)tsk, 0, 0);
}

static void __declspec(naked) Arch1_asm()
{
	__asm
	{
		push eax
		call Arch1_Render
		pop eax
		retn
	}
}

// ArchLight
void __cdecl FixArchLight(NJS_MODEL_SADX* model)
{
	late_DrawModel(model, LATE_MAT);
}

void __cdecl FixShittyLightObjects_Pause(NJS_OBJECT* object)
{
	// ArchLight
	if (object == &object_tpobj_arch02a_arch02a || object == &object_tpobj_arch02b_arch02b)
		late_DrawObjectClipEx(object, LATE_MAT, 1.0f);
	else if (object == &object_tpobj_arch01_arch01)
		late_DrawObject(object, LATE_WZ);
	// Pole
	else if (object == &object_tpobj_pole_pole)
	{
		dsDrawModel(object_tpobj_pole_pole.basicdxmodel);
		DrawModelMS(object_tpobj_pole_pole.sibling->basicdxmodel, LATE_MAT);
		DrawModelMS(object_tpobj_pole_pole.sibling->sibling->basicdxmodel, LATE_WZ);
		DrawObjectMS(object_tpobj_pole_pole.child, LATE_MAT, 1.0f);
	}
	else
		dsDrawObject(object);
}

void __cdecl RenderOPanel_Main(NJS_OBJECT* a1, LATE a2)
{
	dsDrawModel(a1->basicdxmodel);
}

void RenderOPanel_Transparent(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawModel(a1->child->basicdxmodel, LATE_MAT);
	late_DrawModel(a1->child->sibling->sibling->basicdxmodel, LATE_MAT);
	late_DrawModel(a1->child->sibling->basicdxmodel, LATE_MAT);
}

void ORocketFix(NJS_MODEL_SADX* a1)
{
	late_DrawModel(a1, LATE_MAT);
}

void ShareObj_Load()
{
	if (!ModelsLoaded_ShareObj)
	{
		ReplacePVM("BG_SHAREOBJ");
		ReplacePVM("OBJ_SHAREOBJ");
		// Satellite
		AddWhiteDiffuseMaterial(&matlist_eisei_eiseioya_ita[0]);
		AddWhiteDiffuseMaterial(&matlist_eisei_eiseioya_ita[1]);
		// OLight1
		LoadModel_ReplaceMeshes(&object_tpobj_slight1_slight1, "system\\data\\shareobj\\common\\models\\tpobj_slight1.nja.sa1mdl"); // Hierarchy edited to match DX
		AddWhiteDiffuseMaterial(&object_tpobj_slight1_slight1.basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&object_tpobj_slight1_slight1.basicdxmodel->mats[1]);
		// ORocket (just queue the model)
		WriteCall((void*)0x0079BEBA, ORocketFix);
		WriteCall((void*)0x0079BEF8, ORocketFix);
		WriteCall((void*)0x0079BF5D, ORocketFix);
		WriteCall((void*)0x0079BF97, ORocketFix);
		// Cart fixes
		NJS_OBJECT* CartSonic = LoadModel("system\\data\\shareobj\\common\\models\\sarucart_sarucart.nja.sa1mdl");
		// Since there's other code referencing model parts directly, meshes are replaced instead of overwriting the whole object.
		SwapModel(&object_sarucart_sarucart_sarucart, CartSonic); // Regular cart (edited hierarchy to make the glass sibling)
		object_sarucart_sarucart_sarucart.child->sibling = CartSonic->child->sibling;
		object_sarucart_sarucart_sarucart.child->sibling->evalflags &= ~NJD_EVAL_BREAK;
		// Arch1
		object_tpobj_arch01_arch01 = *LoadModel("system\\data\\shareobj\\common\\models\\tpobj_arch01.nja.sa1mdl"); // Edited hierarchy
		WriteJump((void*)0x0079C3A0, Arch1_asm);
		// ArchLight
		WriteCall((void*)0x0079C5FD, FixArchLight);
		WriteCall((void*)0x0079C36A, FixShittyLightObjects_Pause);
		// OCartStopper
		AddWhiteDiffuseMaterial(&object_tpobj_stop_stop.basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_tpobj_stop_stop.basicdxmodel->mats[8]);
		// OPanel fix
		object_tpobj_paneru_paneru = *LoadModel("system\\data\\shareobj\\common\\models\\tpobj_paneru.nja.sa1mdl"); // OPanel (edited)
		WriteCall((void*)0x0079DB94, RenderOPanel_Main);
		WriteCall((void*)0x0079DBA4, RenderOPanel_Transparent);
		AddWhiteDiffuseMaterial(&object_tpobj_paneru_paneru.basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_tpobj_paneru_paneru.basicdxmodel->mats[2]);
		// OPole fix
		WriteJump((void*)0x0079CCB0, OPole_Display_asm); // OPole display is usercall
		object_tpobj_pole_pole = *LoadModel("system\\data\\shareobj\\common\\models\\tpobj_pole.nja.sa1mdl"); // OPole (edited)
		AddWhiteDiffuseMaterial(&object_tpobj_pole_pole.basicdxmodel->mats[1]);
		ModelsLoaded_ShareObj = true;
	}
}