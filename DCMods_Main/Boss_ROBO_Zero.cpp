#include "stdafx.h"
#include "Boss_ROBO_Zero.h"

static bool ModelsLoaded_B_ROBO;

bool ZeroBarriers_FadeOut; // Fade out the electricity if true, fade in if false

// Trampoline to init fadeout values
static Trampoline* SetRobo_t = nullptr;
static void __cdecl SetRobo_r()
{
	const auto original = TARGET_DYNAMIC(SetRobo);
	original();
	// Set electricity colors to 0 for fadeout
	wline_thunder[0].pColor[0].argb.r = 0;
	wline_thunder[0].pColor[1].argb.r = 0;
	wline_thunder[1].pColor[0].argb.r = 0;
	wline_thunder[0].pColor[1].argb.r = 0;
	ZeroBarriers_FadeOut = false;
}

void LoadZeroOceanPVM(const char* filename, NJS_TEXLIST* texlist)
{
	texLoadTexturePvmFile(filename, texlist);
	texLoadTexturePvmFile("EC_SEA", &texlist_ec_sea);
}

void Zero_ChainAttackDisplayHook(task *a1)
{
	late_SetFunc(disp_ChainElec, a1, 22048.0f, LATE_LIG);
}

void Zero_BarrierDisplayHook(task *a1)
{
	late_SetFunc(dispPole, a1, -1000.0f, LATE_MAT);
}

void Zero_BarrierOnFire(NJS_VECTOR *a1, float a2)
{
	ZeroBarriers_FadeOut = true;
	CreateBomb(a1, a2);
}

void Zero_DrawSparks(NJS_OBJECT *a1, LATE a2, float a3)
{
	njScale(0, 0.85f, 0.85f, 0.85f);
	late_DrawObjectClipEx(a1, LATE_LIG, a3);
}

void Zero_DrawElectricBarrier(FVFStruct_H_B *a1, signed int count, int a3)
{
	int barrier_alpha = ZeroBarriers_FadeOut ? 
		barrier_alpha = max(0, wline_thunder[0].pColor[0].argb.r - 1) : 
		barrier_alpha = min(255, wline_thunder[0].pColor[0].argb.r + 1);
	unsigned int color = (barrier_alpha & 0x0ff) << 8 | (barrier_alpha & 0x0ff) << 16 | (barrier_alpha & 0x0ff) | 0xFF000000;
	SetOceanAlphaModeAndFVF(a3);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_ONE);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
	wline_thunder[0].pColor[0].color = color;
	wline_thunder[0].pColor[1].color = color;
	wline_thunder[1].pColor[0].color = color;
	wline_thunder[1].pColor[1].color = color;
	Direct3D_DrawFVF_H(a1, count);
	njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
	njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
}

void Zero_OceanHook(void(__cdecl* function)(void*), void* data, float depth, LATE queueflags)
{
	if (!loop_count)
	{
		___njFogDisable();
		njSetTexture(&texlist_ec_sea);
		njPushMatrix(0);
		njTranslate(0, camera_twp->pos.x, 0, camera_twp->pos.z);
		late_z_ofs___ = -40000.0f;
		late_DrawObjectClip(&object_ecsc_s_hare_umi_robo, LATE_WZ, 1.0f);
		njPopMatrix(1u);
		late_z_ofs___ = 0.0f;
	}
}

void DrawShadow_ERobo_Fix(NJS_OBJECT *a1)
{
	late_DrawObjectClip(a1, LATE_LIG, 1.0f);
}

void B_ROBO_Init()
{
	ReplaceSET("SETZEROA");
	ReplaceSET("SETZEROS");
	ReplacePVM("EROBO");
	ReplaceGeneric("EROBO_GC.NB", "EROBO_DC.NB");
	ReplacePVM("E101R_TIKEI");
	for (int i = 0; i < 3; i++)
	{
		pClipMap_Robo[0][i].f32Far = -9500.0f;
		pFogTable_Robo[0][i].f32StartZ = -10000.0f;
		pFogTable_Robo[0][i].f32EndZ = -10000.0f;
	}
}

void B_ROBO_Load()
{
	LevelLoader(LevelAndActIDs_Zero, "SYSTEM\\data\\bossrobo\\landtablerobo.c.sa1lvl", &texlist_e101r_tikei);
	if (!ModelsLoaded_B_ROBO)
	{
		SetRobo_t = new Trampoline(0x00587D40, 0x00587D47, SetRobo_r);
		WriteCall((void*)0x0058C7E4, DrawShadow_ERobo_Fix);
		WriteData<1>((char*)0x005850F0, 0xC3u); // Disable SetClip_ZERO
		WriteData((float*)0x0058752C, 0.8f); // Zero constant material alpha
		// Effect fixes
		WriteCall((void*)0x0058B580, Zero_BarrierOnFire); // Hook to tell when to fade out barrier effect
		WriteCall((void*)0x0058B61D, Zero_DrawSparks);
		WriteCall((void*)0x0058BC56, Zero_DrawElectricBarrier);
		WriteJump((void*)0x0058C721, Zero_ChainAttackDisplayHook);
		WriteJump((void*)0x0058F798, Zero_BarrierDisplayHook);
		// Ocean model
		if (!CheckSADXWater(LevelIDs_Zero))
		{
			WriteCall((void*)0x00585448, LoadZeroOceanPVM);
			WriteCall((void*)0x00585493, Zero_OceanHook); // Normal
			WriteCall((void*)0x005853EA, Zero_OceanHook); // Pause
			object_ecsc_s_hare_umi_robo.basicdxmodel->mats[0].attr_texId = 4;
			object_ecsc_s_hare_umi_robo.basicdxmodel->mats[0].attrflags |= NJD_FLAG_IGNORE_LIGHT;
			object_ecsc_s_hare_umi_robo.basicdxmodel->mats[0].diffuse.color = 0x7FFFFFFF;
			AddTextureAnimation(LevelIDs_Zero, 0, &object_ecsc_s_hare_umi_robo.basicdxmodel->mats[0], false, 4, 4, 13);
		}
		ModelsLoaded_B_ROBO = true;
	}
}