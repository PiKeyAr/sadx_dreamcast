#include "stdafx.h"

// Erases material colors, adds alpha rejected textures, adjusts specular flags
void ProcessMaterials_Object(NJS_OBJECT* obj)
{
	//PrintDebug("Processing materials...\n");
	Uint32 materialflags;
	bool ignorespecular;
	if (obj->basicdxmodel)
	{
		// Check meshsets and remove vertex colors, if any
		for (int k = 0; k < obj->basicdxmodel->nbMeshset; ++k)
		{
			if (obj->basicdxmodel->meshsets[k].vertcolor != nullptr)
			{
				PrintDebug("Vertex colors found! %X\n", obj);
				obj->basicdxmodel->meshsets[k].vertcolor = nullptr;
			}
		}

		// Check the first material for NJD_FLAG_IGNORE_SPECULAR
		ignorespecular = obj->basicdxmodel->mats[0].attrflags & NJD_FLAG_IGNORE_SPECULAR;

		// Loop through materials
		for (int k = 0; k < obj->basicdxmodel->nbMat; ++k)
		{
			materialflags = obj->basicdxmodel->mats[k].attrflags;

			// Adjust the rest of the materials according to the first material's ignore specular flag
			if (k != 0)
			{
				if (ignorespecular)
				{
					if (!(materialflags & NJD_FLAG_IGNORE_SPECULAR))
						obj->basicdxmodel->mats[k].attrflags |= NJD_FLAG_IGNORE_SPECULAR;
				}
				else
				{
					if ((materialflags & NJD_FLAG_IGNORE_SPECULAR))
						obj->basicdxmodel->mats[k].attrflags &= ~NJD_FLAG_IGNORE_SPECULAR;
				}
			}

			// Remove material colors
			obj->basicdxmodel->mats[k].diffuse.argb.r = 0xFF;
			obj->basicdxmodel->mats[k].diffuse.argb.g = 0xFF;
			obj->basicdxmodel->mats[k].diffuse.argb.b = 0xFF;

			// Check for alpha rejection flag
			if (materialflags & NJD_CUSTOMFLAG_NO_REJECT)
				AddAlphaRejectMaterial((NJS_MATERIAL*)&obj->basicdxmodel->mats[obj->basicdxmodel->meshsets[k].type_matId & ~0xC000]);
		}
	}

	// Process materials of child and sibling models as well
	if (obj->child != nullptr)
		ProcessMaterials_Object(obj->child);
	if (obj->sibling != nullptr)
		ProcessMaterials_Object(obj->sibling);
}

// Replaces MODEL structs in the whole hierarchy without touching OBJECT structs
void LoadModel_ReplaceMeshes(NJS_OBJECT* object, const char* ModelName)
{
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("Loading model (r): %s: ", helperFunctionsGlobal->GetReplaceablePath(ModelName));
	ModelInfo* info = new ModelInfo(helperFunctionsGlobal->GetReplaceablePath(ModelName));
	NJS_OBJECT* replacement = info->getmodel();
	SwapModel(object, replacement);
	ProcessMaterials_Object(object);
	if (ModConfig::DebugVerbose >= DebugVerboseConfig::Models)
		PrintDebug("OK\n");
}

// Replaces MODEL structs in one object with MODEL structs from another object
void SwapModel(NJS_OBJECT* source, NJS_OBJECT* replacement)
{
	if (source->basicdxmodel)
		source->basicdxmodel = replacement->basicdxmodel;
	if (source->child)
		SwapModel(source->child, replacement->child);
	if (source->sibling)
		SwapModel(source->sibling, replacement->sibling);
}

void SwapMeshsets(NJS_OBJECT* object, int mesh1, int mesh2)
{
	NJS_MESHSET_SADX TempMeshset;
	memcpy(&TempMeshset, &object->basicdxmodel->meshsets[mesh1], sizeof(NJS_MESHSET_SADX));
	memcpy(&object->basicdxmodel->meshsets[mesh1], &object->basicdxmodel->meshsets[mesh2], sizeof(NJS_MESHSET_SADX));
	memcpy(&object->basicdxmodel->meshsets[mesh2], &TempMeshset, sizeof(NJS_MESHSET_SADX));
}

NJS_MODEL_SADX* CloneAttach(NJS_MODEL_SADX* att)
{
	NJS_MODEL_SADX* newatt = new NJS_MODEL_SADX;
	newatt->buffer = nullptr;
	newatt->center = att->center;
	newatt->r = att->r;
	newatt->nbMat = att->nbMat;
	newatt->nbPoint = att->nbPoint;
	newatt->nbMeshset = att->nbMeshset;

	// Vertices and normals
	if (att->points != NULL)
	{
		newatt->points = new NJS_VECTOR[att->nbPoint];
		memcpy(newatt->points, att->points, att->nbPoint * sizeof(NJS_VECTOR));
	}
	else
		newatt->points = NULL;
	if (att->normals != NULL)
	{
		newatt->normals = new NJS_VECTOR[att->nbPoint];
		memcpy(newatt->normals, att->normals, att->nbPoint * sizeof(NJS_VECTOR));
	}
	else
		newatt->normals = NULL;

	// Meshsets
	if (att->nbMeshset > 0)
	{
		newatt->meshsets = new NJS_MESHSET_SADX[att->nbMeshset];
		memcpy(newatt->meshsets, att->meshsets, att->nbMeshset * sizeof(NJS_MESHSET_SADX));
	}
	else
		newatt->meshsets = NULL;

	// Materials
	if (att->nbMat > 0)
	{
		newatt->mats = new NJS_MATERIAL[att->nbMat];
		memcpy(newatt->mats, att->mats, att->nbMat * sizeof(NJS_MATERIAL));
	}
	else
		newatt->mats = NULL;
	return newatt;
}

NJS_OBJECT* CloneObject(NJS_OBJECT* obj)
{
	NJS_OBJECT* newobj = new NJS_OBJECT;
	newobj->ang[0] = obj->ang[0];
	newobj->ang[1] = obj->ang[1];
	newobj->ang[2] = obj->ang[2];
	newobj->evalflags = obj->evalflags;
	newobj->pos[0] = obj->pos[0];
	newobj->pos[1] = obj->pos[1];
	newobj->pos[2] = obj->pos[2];
	newobj->scl[0] = obj->scl[0];
	newobj->scl[1] = obj->scl[1];
	newobj->scl[2] = obj->scl[2];

	// Model
	if (obj->basicdxmodel)
	{
		newobj->basicdxmodel = CloneAttach(obj->basicdxmodel);
	}
	else
		newobj->basicdxmodel = NULL;

	// Child
	if (obj->child)
		newobj->child = CloneObject(obj->child);
	else
		newobj->child = NULL;

	// Sibling
	if (obj->sibling)
		newobj->sibling = CloneObject(obj->sibling);
	else
		newobj->sibling = NULL;

	return newobj;
}

void HideMesh_Model_Real(NJS_MODEL_SADX* att, std::vector<int> hidemeshlist)
{
	if (!att) return;

	int currmesh = 0;
	for (int m = 0; m < att->nbMeshset; m++)
	{
		bool add = true;
		for (int i : hidemeshlist)
		{
			if (m == i) add = false;
		}
		if (add)
		{
			att->meshsets[currmesh] = att->meshsets[m];
			currmesh++;
		}
	}
	att->nbMeshset -= (Uint16)hidemeshlist.size();
}

void HideMesh_Model_Wrapper(NJS_MODEL_SADX* att, int arg, ...)
{
	va_list arguments;
	std::vector<int> hidemeshlist;
	for (va_start(arguments, arg); arg != -1; arg = va_arg(arguments, int))
	{
		hidemeshlist.push_back(arg);
	}
	va_end(arguments);
	HideMesh_Model_Real(att, hidemeshlist);
}

void HideMesh_Object_Wrapper(NJS_OBJECT* object, int arg, ...)
{
	va_list arguments;
	std::vector<int> hidemeshlist;
	// Add list of meshset IDs to hide
	for (va_start(arguments, arg); arg != -1; arg = va_arg(arguments, int))
	{
		hidemeshlist.push_back(arg);
	}
	va_end(arguments);
	HideMesh_Model_Real(object->basicdxmodel, hidemeshlist);
}