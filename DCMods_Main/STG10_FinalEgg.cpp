#include "stdafx.h"
#include "STG10_FinalEgg.h"

static bool ModelsLoaded_STG10;

NJS_TEXNAME textures_cylinder[256];
NJS_TEXLIST texlist_cylinder = { arrayptrandlength(textures_cylinder) };

TEX_PVMTABLE CylinderPVMList = { "CYLINDER", &texlist_cylinder };

void DrawOSpinTubeModels(NJS_MODEL_SADX* model, float scale)
{
	//late_z_ofs___ = 2000.0f;
	DrawModelMesh(model, LATE_WZ);
	//late_z_ofs___ = 0;
}

void RenderBlueLight(task* a2)
{
	taskwk* v1; // esi
	unsigned __int16 v2; // ax
	unsigned __int16 v3; // si

	v1 = a2->twp;
	if (!loop_count)
	{
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		ds_DrawModelClip(object_lig_blue_lig_bou_lig_bou.basicdxmodel, 1.0f); // Pole
		v2 = v1->timer.w[0];
		if (v2)
		{
			njRotateY(0, v2);
		}
		v3 = v1->timer.w[1];
		if (v3)
		{
			njRotateX(0, v3);
		}
		ds_DrawModelClip(object_lig_blue_lig_bou_lig_bou.child->basicdxmodel, 1.0f); // Camera
		late_DrawModelClipMS(object_lig_blue_lig_bou_lig_bou.child->child->sibling->basicdxmodel, LATE_WZ, 1.0f);  // Camera transparent 2
		late_DrawModelClipMS(object_lig_blue_lig_bou_lig_bou.child->child->basicdxmodel, LATE_LIG, 1.0f); // Camera transparent 1
		late_DrawModelClipMS(object_lig_blue_lig_bou_lig_bou.child->child->sibling->sibling->basicdxmodel, LATE_LIG, 1.0f);  // Camera light
		njPopMatrix(1u);
	}
}

void DrawOUkishima(NJS_OBJECT* obj, float scale)
{
	DrawObjectClipMesh(obj, LATE_WZ, scale);
}

void OTatekan_Display(task *a1)
{
	taskwk *v1; // esi@1
	int v2; // eax@2
	int v3; // eax@4
	float YDist; // ST04_4@6
	int v5; // eax@6
	float scale; // [sp+10h] [bp+4h]@9
	v1 = a1->twp;
	if (!loop_count)
	{
		object_houden_tate_fractal_fractal.basicdxmodel->mats->attr_texId = (Uint8)v1->btimer % 256;
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		v2 = v1->ang.y;
		if (v2)
		{
			njRotateY(0, (unsigned __int16)v2);
		}
		njPushMatrix(0);
		v3 = v1->ang.x;
		if (v3)
		{
			njRotateY(0, (unsigned __int16)v3);
		}
		ds_DrawObjectClip(&object_houden_tate_under_under, 1.0f); // Bottom
		njPopMatrix(1u);
		njPushMatrix(0);
		YDist = v1->scl.y * 22.0f;
		njTranslate(0, 0.0, YDist, 0.0);
		v5 = v1->ang.z;
		if (v5)
		{
			njRotateY(0, (unsigned __int16)v5);
		}
		ds_DrawObjectClip(&object_houden_tate_top_top, 1.0f); // Top
		njPopMatrix(1u);
		njPushMatrix(0);
		njTranslate(0, 0.0f, 4.0f, 0.0f);
		njScale(0, 1.0f, v1->scl.y, 1.0f);
		late_z_ofs___ = -20000.0f;
		late_DrawObjectClip(&object_houden_tate_sin_sin, LATE_MAT, v1->scl.y); // Pivot
		if (v1->scl.y >= 1.0f)
		{
			scale = v1->scl.y;
		}
		else
		{
			scale = 1.0f;
		}
		njSetTexture(&texlist_cylinder);
		late_z_ofs___ = -18000.0f;
		late_DrawObjectClip(&object_houden_tate_fractal_fractal, LATE_MAT, scale); // Glass
		late_z_ofs___ = 0.0f;
		njPopMatrix(1u);
		njPopMatrix(1u);
		if (!ChkPause())
			v1->btimer += (ulGlobalTimer % 2 == 0 || g_CurrentFrame >= 2);
	}
}

void OTexture_Display(task *a1)
{
	taskwk *v1; // esi@1
	NJS_VECTOR *v2; // esi@2
	float a3; // ST24_4@2
	v1 = a1->twp;
	if (!loop_count)
	{
		object_face4_face4.basicdxmodel->mats->attr_texId = (Uint8)v1->btimer % 256;
		SaveConstantAttr();
		OnConstantAttr(0, NJD_FLAG_USE_ALPHA);
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_ONE);
		njPushMatrix(0);
		njSetTexture(&texlist_cylinder);
		njTranslateV(0, &v1->pos);
		njRotateXYZ(0, v1->ang.x, v1->ang.y, v1->ang.z);
		v2 = &v1->scl;
		njScaleV(0, v2);
		late_z_ofs___ = -47952.0f;
		a3 = VectorMaxAbs(v2);
		late_DrawObjectClip(&object_face4_face4, LATE_MAT, a3);
		late_z_ofs___ = 0;
		njPopMatrix(1u);
		ResetMaterial();
		njColorBlendingMode(NJD_SOURCE_COLOR, NJD_COLOR_BLENDING_SRCALPHA);
		njColorBlendingMode(NJD_DESTINATION_COLOR, NJD_COLOR_BLENDING_INVSRCALPHA);
		LoadConstantAttr();
		if (!ChkPause())
			v1->btimer += (ulGlobalTimer % 2 == 0 || g_CurrentFrame >= 2);
	}
}

void SetGachaponEnvMaps1(NJS_MODEL_SADX* model, float scale)
{
	ScaleEnvironmentMap(1.0f, 1.0f, 0.5f, 0.5f);
	ds_DrawModelClip(model, scale);
	RestoreEnvironmentMap();
}

void SetGachaponEnvMaps2(NJS_ACTION* action, float frame, float scale)
{
	ScaleEnvironmentMap(1.0f, 1.0f, 0.5f, 0.5f);
	ds_ActionClip(action, frame, scale);
	RestoreEnvironmentMap();
}

void OLight1Fix(NJS_OBJECT* a1, LATE a2, float a3)
{
	late_DrawModelClip(a1->basicdxmodel, LATE_WZ, a3);
	late_DrawModelClipEx(a1->child->basicdxmodel, LATE_LIG, a3);
	late_DrawModelClipEx(a1->child->child->basicdxmodel, LATE_LIG, a3);
}

void OLight2Fix(NJS_OBJECT* a1, int a2, float a3)
{
	njTranslateV(0, (NJS_VECTOR*)a1->pos);
	ds_DrawModelClip(a1->basicdxmodel, a3);
	late_z_ofs___ = 4000.0f;
	late_DrawModelClip(a1->child->basicdxmodel, LATE_WZ, a3);
	late_DrawModelClip(a1->child->sibling->basicdxmodel, LATE_LIG, a3);
	late_z_ofs___ = 0.0f;
}

void _0Light_Camera_DisplayFix(task* a1)
{
	taskwk* v1 = a1->twp;
	if (!loop_count)
	{
		ResetMaterial();
		SetObjectTexture();
		njPushMatrix(0);
		njTranslateV(0, &v1->pos);
		// Pole
		dsDrawModel(object_cam_lig_bo_cl_bo_cl.basicdxmodel);
		njTranslate(0, object_cam_lig_bo_cl_bo_cl.child->pos[0], object_cam_lig_bo_cl_bo_cl.child->pos[1], object_cam_lig_bo_cl_bo_cl.child->pos[2]);
		unsigned __int16 rot_y = v1->timer.w[0];
		if (rot_y)
		{
			njRotateY(0, rot_y);
		}
		unsigned __int16 rot_x = v1->timer.w[1];
		if (rot_x)
		{
			njRotateX(0, rot_x);
		}
		// Camera body
		ds_DrawModelClip(object_cam_lig_bo_cl_bo_cl.child->basicdxmodel, 1.0f);
		// Camera second
		ds_DrawModelClip(object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel, 1.0f);
		// Camera second transparent
		late_DrawModelClipMS(object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->basicdxmodel, LATE_LIG, 1.0f);
		late_DrawModelClipMS(object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->sibling->basicdxmodel, LATE_LIG, 1.0f);
		// Small light
		njPushMatrix(0);
		njTranslateV(0, (NJS_VECTOR*)&object_cam_lig_bo_cl_bo_cl.child->child->pos);
		late_DrawModelClipMS(object_cam_lig_bo_cl_bo_cl.child->child->basicdxmodel, LATE_LIG, 1.0f);
		njPopMatrix(1u);
		// Big light
		njTranslateV(0, (NJS_VECTOR*)&object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->sibling->sibling->pos);
		late_DrawModelClipMS(object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->sibling->sibling->basicdxmodel, LATE_LIG, 1.0f);
		njPopMatrix(1u);
	}
}

void Elevator2Hook(NJS_OBJECT *obj, float scale)
{
	late_z_ofs___ = -10000.0f;
	late_DrawObjectClip(obj, LATE_WZ, scale);
	late_z_ofs___ = 0.0f;
}

void GachaponExplosionFix(NJS_MODEL_SADX *a1)
{
	late_z_ofs___ = 10000.0f;
	late_DrawModel(a1, LATE_LIG);
	late_z_ofs___ = 0;
}

void FinalEgg_Init()
{
	ReplaceCAM("CAM1000A");
	ReplaceCAM("CAM1000S");
	ReplaceCAM("CAM1001S");
	ReplaceCAM("CAM1002E");
	ReplaceCAM("CAM1002S");
	ReplaceCAM("CAM1003S");
	ReplaceSET("SET1000A");
	ReplaceSET("SET1000S");
	ReplaceSET("SET1001S");
	ReplaceSET("SET1002E");
	ReplaceSET("SET1002S");
	ReplaceSET("SET1003S");
	ReplacePVM("EFF_FINALEGG_POM");
	ReplacePVM("FINALEGG1");
	ReplacePVM("FINALEGG2");
	ReplacePVM("FINALEGG3");
	ReplacePVM("FINALEGG4");
	ReplacePVM("OBJ_FINALEGG");
	for (int i = 0; i < 3; i++)
	{
		pFogTable_Stg10[0][i].Col = 0xFF000000;
		pFogTable_Stg10[0][i].f32StartZ = 1200.0f;
		pFogTable_Stg10[0][i].f32EndZ = 3000.0f;
		pFogTable_Stg10[1][i].Col = 0xFF000000;
		pFogTable_Stg10[1][i].f32StartZ = 650.0f;
		pFogTable_Stg10[1][i].f32EndZ = 2000.0f;
		pFogTable_Stg10[1][i].u8Enable = 1;
		pFogTable_Stg10[2][i].Col = 0xFF000000;
		pFogTable_Stg10[2][i].f32StartZ = 650.0f;
		pFogTable_Stg10[2][i].f32EndZ = 2000.0f;
		pClipMap_Stg10[1][i].f32Far = -2400.0f;
	}
}

void FinalEgg_Load()
{
	LevelLoader(LevelAndActIDs_FinalEgg1, "SYSTEM\\data\\stg10_finalegg\\landtable1001.c.sa1lvl", &texlist_FinalEgg1);
	LevelLoader(LevelAndActIDs_FinalEgg2, "SYSTEM\\data\\stg10_finalegg\\landtable1002.c.sa1lvl", &texlist_FinalEgg2);
	LevelLoader(LevelAndActIDs_FinalEgg3, "SYSTEM\\data\\stg10_finalegg\\landtable1003.c.sa1lvl", &texlist_FinalEgg3);
	if (!ModelsLoaded_STG10)
	{
		ListofPvmList[10] = ExpandPVMList(PvmListFinalEgg, CylinderPVMList);
		WriteData<1>((char*)0x005ADC40, 0xC3u); // Disable SetClip_FEgg2
		WriteCall((void*)0x005AEF29, GachaponExplosionFix);
		// Always draw the most detailed Gachapon model
		WriteData((NJS_OBJECT**)0x005AEC07, &object_minimekam_obj2_obj2);
		WriteData((NJS_OBJECT**)0x005AEC0E, &object_minimekam_obj2_obj2);
		// Environment maps thing
		WriteCall((void*)0x005B3785, SetGachaponEnvMaps1);
		WriteCall((void*)0x005B3744, SetGachaponEnvMaps2);
		// Queue OSpinTube models
		WriteCall((void*)0x005BD048, DrawOSpinTubeModels);
		WriteCall((void*)0x005BD06D, DrawOSpinTubeModels);
		WriteCall((void*)0x005BD07C, DrawOSpinTubeModels);
		WriteCall((void*)0x005BCD18, DrawOSpinTubeModels);
		WriteCall((void*)0x005BCD3D, DrawOSpinTubeModels);
		WriteCall((void*)0x005BCD4C, DrawOSpinTubeModels);
		WriteCall((void*)0x005BC9E8, DrawOSpinTubeModels);
		WriteCall((void*)0x005BCA0D, DrawOSpinTubeModels);
		WriteCall((void*)0x005BCA1C, DrawOSpinTubeModels);
		AddWhiteDiffuseMaterial(&object_kanban_eggkanban_eggkanban.basicdxmodel->mats[1]); // OEggKanban (SL OBJECT)
		AddWhiteDiffuseMaterial(&object_kanban_eggkanban_eggkanban.basicdxmodel->mats[2]); // OEggKanban (SL OBJECT)
		// OTatekan
		ForceObjectSpecular_Object(&object_kanban_eggkanban_eggkanban_omote, false); // OEggKanban sibling
		// _0Light_Camera
		object_cam_lig_bo_cl_bo_cl = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\cam_lig_bo_cl.nja.sa1mdl"); // Edited hierarchy
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel->mats[1]);
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel->mats[2]);
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel->mats[3]);
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->basicdxmodel->mats[4]);
		AddWhiteDiffuseMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->sibling->basicdxmodel->mats[0]);
		AddAlphaRejectMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->basicdxmodel->mats[0]); // Small light
		AddAlphaRejectMaterial(&object_cam_lig_bo_cl_bo_cl.child->child->sibling->child->sibling->sibling->basicdxmodel->mats[0]); // Big light
		WriteJump((void*)0x005BBFE0, _0Light_Camera_DisplayFix);
		// _0BlueLight
		object_lig_blue_lig_bou_lig_bou = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\lig_blue_lig_bou.nja.sa1mdl"); // Edited hierarchy
		AddAlphaRejectMaterial(&object_lig_blue_lig_bou_lig_bou.child->child->sibling->sibling->basicdxmodel->mats[0]);
		WriteJump((void*)0x005BBD10, RenderBlueLight);
		// OPurs_Camera
		AddAlphaRejectMaterial(&model_cam_bo_c_redlgt.mats[0]);
		// OLight1
		object_ligpfr_light01_light01 = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\ligpfr_light01.nja.sa1mdl");
		AddWhiteDiffuseMaterial(&object_ligpfr_light01_light01.basicdxmodel->mats[0]);
		AddWhiteDiffuseMaterial(&object_ligpfr_light01_light01.basicdxmodel->mats[1]);
		WriteCall((void*)0x005B2796, OLight1Fix);
		// OLight2
		object_bl_nc_bllight_nc_bllight = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\bl_nc_bllight.nja.sa1mdl");
		AddWhiteDiffuseMaterial(&object_bl_nc_bllight_nc_bllight.basicdxmodel->mats[2]);
		WriteCall((void*)0x005B2636, OLight2Fix);
		// Other models
		model_setasiba_lv1_lv1 = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\setasiba_lv1.nja.sa1mdl")->basicdxmodel; // OSetStep
		model_contena_box_box = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\contena_box.nja.sa1mdl")->basicdxmodel; // OContainer
		object_ugokuhari_dodai_dodai = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\ugokuhari_dodai.nja.sa1mdl");
		model_ugokuhari_dodai_dodai = *object_ugokuhari_dodai_dodai.basicdxmodel; // OMova_thorn 1
		model_ugokuhari_dodai_waku = *object_ugokuhari_dodai_dodai.child->basicdxmodel; // OMova_thorn 2
		model_ugokuhari_dodai_hari = *object_ugokuhari_dodai_dodai.child->child->basicdxmodel; // OMova_thorn 3
		object_kaitenasiba_big_big = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\kaitenasiba_big.nja.sa1mdl");
		model_kaitenasiba_big_big = *object_kaitenasiba_big_big.basicdxmodel; // OSpin_TubeB 1
		model_kaitenasiba_big_kaiten_ami1a = *object_kaitenasiba_big_big.child->basicdxmodel; // OSpin_TubeB 2
		model_kaitenasiba_big_kaiten_waku1a = *object_kaitenasiba_big_big.child->child->basicdxmodel; // OSpin_TubeB 3
		object_kaitenasiba_midium_midium = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\kaitenasiba_midium.nja.sa1mdl"); // OSpin_TubeM
		model_kaitenasiba_midium_midium = *object_kaitenasiba_midium_midium.basicdxmodel; // OSpin_TubeM 1
		model_kaitenasiba_midium_kaiten_ami2a = *object_kaitenasiba_midium_midium.child->basicdxmodel; // OSpin_TubeM 2
		model_kaitenasiba_midium_kaiten_waku2a = *object_kaitenasiba_midium_midium.child->child->basicdxmodel; // OSpin_TubeM 3
		object_kaitenasiba_small_small = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\kaitenasiba_small.nja.sa1mdl");
		model_kaitenasiba_small_small = *object_kaitenasiba_small_small.basicdxmodel; // OSpin_TubeS 1
		model_kaitenasiba_small_kaiten_ami3a = *object_kaitenasiba_small_small.child->basicdxmodel; // OSpin_TubeS 2
		model_kaitenasiba_small_kaiten_waku3a = *object_kaitenasiba_small_small.child->child->basicdxmodel; // OSpin_TubeS 3
		object_fun_funflot_funflot = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\fun_funflot.nja.sa1mdl"); // OFun
		SwapMeshsets(object_houden1a_part_beem_part_beem.child, 0, 1); // Laser
		LoadModel_ReplaceMeshes(&object_aamu05_cyl40_cyl40, "SYSTEM\\data\\stg10_finalegg\\common\\models\\aamu05_cyl40.nja.sa1mdl"); // OSide_Arm
		SwapModel(&object_aamu05_cyl40_cyl51, object_aamu05_cyl40_cyl40.child->child->child->child->child->child); // OSide_Arm broken 1
		SwapModel(&object_aamu05_cyl40_cube50, object_aamu05_cyl40_cyl40.child->child->child->child->child->child->child->sibling->sibling); // OSide_Arm broken 2
		SwapModel(&object_aamu05_cyl40_cube51, object_aamu05_cyl40_cyl40.child->child->child->child->child->child->child->sibling); // OSide_Arm broken 3
		SwapModel(&object_aamu05_cyl40_cube52, object_aamu05_cyl40_cyl40.child->child->child->child->child->child->child); // OSide_Arm broken 4
		object_uki_uki_uki = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\uki_uki.nja.sa1mdl"); // OUkishima
		object_biglights_dai_dai = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\biglights_dai.nja.sa1mdl"); // OBigLight (unused)
		object_kowarekabe_koware_koware = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\kowarekabe_koware.nja.sa1mdl"); // Breakable walls near Amy's elevator
		object_kowarekabe_mae_mae = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\kowarekabe_mae.nja.sa1mdl"); // Breakable walls near Amy's elevator
		object_shatter_shatter1shape_shatter1shape = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\shatter_shatter1shape.nja.sa1mdl");  // Wall that Zero breaks (shape)
		object_shatter_breakparts_breakparts = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\shatter_breakparts.nja.sa1mdl"); // Wall that Zero breaks (fragments)
		WriteCall((void*)0x005B7AEA, DrawOUkishima); // Good idea not to queue a model with transparency, huh?
		LoadModel_ReplaceMeshes(&object_b_con_harikabe_harikabe, "SYSTEM\\data\\stg10_finalegg\\common\\models\\b_con_harikabe.nja.sa1mdl"); // OConvStop
		object_pinlamp_daimaru_daimaru = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\pinlamp_daimaru.nja.sa1mdl");
		object_pinlamp_daimaru_daimaru.child->basicdxmodel = object_pinlamp_daimaru_daimaru.child->basicdxmodel; // OPinLamp
		object_elevator_hontai_hontai = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\elevator_hontai.nja.sa1mdl"); // OElevator1 1 (the climbing thing)
		object_elevator_tobira_tobira = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\elevator_tobira.nja.sa1mdl"); // OElevator1 2 (the climbing thing) door
		object_mdlelv1_cl_elvtr_cl_elvtr = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\mdlelv1_cl_elvtr.nja.sa1mdl"); // OElevator2 1 (inside glass tube)
		object_mdlelv_nc_elvtr_nc_elvtr = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\mdlelv_nc_elvtr.nja.sa1mdl"); // OElevator2 2 (inside glass tube) - edited model
		AddWhiteDiffuseMaterial(&object_mdlelv_nc_elvtr_nc_elvtr.basicdxmodel->mats[6]);
		AddWhiteDiffuseMaterial(&object_mdlelv_nc_elvtr_nc_elvtr.basicdxmodel->mats[7]);
		AddWhiteDiffuseMaterial(&object_mdlelv_nc_elvtr_nc_elvtr.basicdxmodel->mats[8]);
		AddWhiteDiffuseMaterial(&object_mdlelv_nc_elvtr_nc_elvtr.basicdxmodel->mats[9]);
		WriteCall((void*)0x005B49C2, Elevator2Hook); // It has transparency so queue it
		object_boyon_saku_saku = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\boyon_saku.nja.sa1mdl"); // OFSaku 1
		object_boyon_henkeigo_saku_gomug = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\boyon_henkeigo_saku.nja.sa1mdl"); // OFSaku 2
		object_hsgo_hasigo_hasigo = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\hsgo_hasigo.nja.sa1mdl"); // OHasiGo
		AddWhiteDiffuseMaterial(&object_hsgo_hasigo_hasigo.basicdxmodel->mats[2]);
		object_hsgo_hasigo_hasigo.basicdxmodel->mats[2].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
		object_enemyout_hokoz_hokoz = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\enemyout_hokoz.nja.sa1mdl"); // EGacha 4 (thrower thing)
		object_enemyout2_enmout2_enmout2 = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\enemyout2_enmout2.nja.sa1mdl"); // OGShooter
		object_ue_aamu_ue_ue = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\ue_aamu_ue.nja.sa1mdl"); // OUp_Arm
		object_ue_aamu_ue_cube50a = *object_ue_aamu_ue_ue.child->child->child->child->child->sibling->sibling; // OUp_Arm broken 2
		object_ue_aamu_ue_cyl51a = *object_ue_aamu_ue_ue.child->child->child->child; // OUp_Arm broken 1
		object_ue_aamu_ue_cube51a = *object_ue_aamu_ue_ue.child->child->child->child->child->sibling; // OUp_Arm broken 3
		object_ue_aamu_ue_cube52a = *object_ue_aamu_ue_ue.child->child->child->child->child; // OUp_Arm broken 4
		object_hammer_bmerge2_bmerge2 = *LoadModel("SYSTEM\\data\\stg10_finalegg\\common\\models\\hammer_bmerge2.nja.sa1mdl"); // OHammer 1
		object_hammer_bmerge2_bmergex = *object_hammer_bmerge2_bmerge2.child->child; // OHammer 1
		model_hammer_bmerge2_hammer = *object_hammer_bmerge2_bmerge2.child->sibling->child->basicdxmodel; // OHammer 2
		objItemTable10.pObjItemEntry[59].ssAttribute = 1; // OSuikomi 
		objItemTable10.pObjItemEntry[59].fRange = 1600000.0f; // O Suikomi
		objItemTable10.pObjItemEntry[76].ssAttribute = 1; // OTatekan 
		objItemTable10.pObjItemEntry[76].fRange = 160000.0f; // OTatekan
		objItemTable10.pObjItemEntry[65].ssAttribute = 1; // OTexture 
		objItemTable10.pObjItemEntry[65].fRange = 720000.0f; // OTexture
		OStandLight_Init();
		WriteJump((void*)0x005AE330, OTexture_Display); // O Texture function
		WriteJump((void*)0x005B4690, OTatekan_Display); // Cylinder function
		ModelsLoaded_STG10 = true;
	}
}

void FinalEgg_OnFrame()
{
	if (ssStageNumber != LevelIDs_FinalEgg)
		return;

	auto entity = playertwp[0];
	if (!ChkPause() && entity != nullptr)
	{
		switch (ssActNumber)
		{
		case 0:
			if (entity->pos.z >= 0.0f)
			{
				gFog.f32StartZ = min(1400.0f, gFog.f32StartZ + 8.0f);
				gFog.f32EndZ = min(3400.0f, gFog.f32EndZ + 16.0f);
			}
			else
			{
				gFog.f32StartZ = max(pFogTable_Stg10[0][0].f32StartZ, gFog.f32StartZ - 16.0f);
				gFog.f32EndZ = max(pFogTable_Stg10[0][0].f32EndZ, gFog.f32EndZ - 8.0f);
			}
			break;
		case 1:
			if (entity->pos.x >= 900.0f && entity->pos.x <= 1250.0f && entity->pos.z >= -1050.0f && entity->pos.z <= -605.0f && entity->pos.y > -700.0f)
			{
				gFog.f32StartZ = max(500.0f, gFog.f32StartZ - 8.0f);
				gFog.f32EndZ = max(1500.0f, gFog.f32EndZ - 16.0f);
			}
			else if (entity->flag & Status_Ground)
			{
				gFog.f32StartZ = min(pFogTable_Stg10[1][0].f32StartZ, gFog.f32StartZ + 8.0f);
				gFog.f32EndZ = min(pFogTable_Stg10[1][0].f32EndZ, gFog.f32EndZ + 16.0f);
			}
			break;
		default:
			break;
		}
	}
}