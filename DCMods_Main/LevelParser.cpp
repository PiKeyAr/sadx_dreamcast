#include "stdafx.h"
#include "LevelParser.h"

void ParseWhiteDiffuseMaterial(const NJS_MATERIAL* material, bool unregister)
{
	if (ModConfig::DLLLoaded_Lantern && ModConfig::EnableWhiteDiffuse)
	{
		if (unregister)
			material_unregister(&material, 1, ForceWhiteDiffuse);
		else
			material_register(&material, 1, ForceWhiteDiffuse);
	}
}

void ParseWhiteDiffuseNightMaterial(const NJS_MATERIAL* material, bool unregister)
{
	if (ModConfig::DLLLoaded_Lantern && ModConfig::EnableWhiteDiffuse)
	{
		if (unregister)
			material_unregister(&material, 1, ForceWhiteDiffuse_Night);
		else
			material_register(&material, 1, ForceWhiteDiffuse_Night);
	}
}

void ParseAlphaRejectMaterial(const NJS_MATERIAL* material, bool unregister)
{
	if (ModConfig::DLLLoaded_Lantern)
	{
		if (unregister)
			material_unregister(&material, 1, DisableAlphaRejection);
		else
			material_register(&material, 1, DisableAlphaRejection);
	}
}

// Checks if the SADX Style Water mod has changed the water in this level
bool CheckSADXWater(LevelIDs level)
{
	switch (level)
	{	
	case LevelIDs_EmeraldCoast:
		return *(float*)0x004F8D2F == -2153.0f;
	case LevelIDs_StationSquare:
		return *(float*)0x006313F1 == -1500.012f;
	case LevelIDs_MysticRuins:
	case LevelIDs_EggHornet:
		return *(float*)0x005326A2 == -4174.012f;
	case LevelIDs_Past:
		return *(float*)0x00542A38 == -2800.012f;
	case LevelIDs_EggCarrierOutside:
	case LevelIDs_Zero:
	case LevelIDs_E101R:
		return *(float*)0x0051C6AA == -4000.012f;
	default:
		return false;
	}
}

// Process COL flags and add texture/UV animations or offset objects
void ProcessColFlags(_OBJ_LANDENTRY* col, unsigned __int16 levelact)
{
	bool SADXWaterDetected = CheckSADXWater((LevelIDs)(levelact >> 8));
	Uint32 colflags = col->slAttribute;
	for (int k = 0; k < col->pObject->basicdxmodel->nbMeshset; ++k)
	{
		NJS_MESHSET_SADX* meshset = &col->pObject->basicdxmodel->meshsets[k];
		NJS_MATERIAL* material = &col->pObject->basicdxmodel->mats[meshset->type_matId & ~0xC000];
		Uint32 matflags = material != nullptr ? material->attrflags : 0;
		Uint32 texid = material != nullptr ? material->attr_texId : 0;
		// UV animations general
		if ((matflags & NJD_CUSTOMFLAG_UVANIM2) || (matflags & NJD_CUSTOMFLAG_UVANIM1))
		{
			if (!(col->slAttribute & SurfaceFlags_UvManipulation))
				col->slAttribute |= SurfaceFlags_UvManipulation;
		}
		// Level-specific things
		switch (levelact)
		{

		case LevelAndActIDs_EmeraldCoast1:
			if (texid >= 82 && texid <= 96)
			{
				AddTextureAnimation(1, 0, material, false, 4, 82, 96);
				if (SADXWaterDetected)
					col->slAttribute = 0x80000402;
			}
			// Texanim 2
			if (texid >= 67 && texid <= 81)
			{
				AddTextureAnimation(1, 0, material, false, 3, 67, 81);
			}
			break;

		case LevelAndActIDs_EmeraldCoast2:
			// Hide pool meshes if using SADX water
			if (SADXWaterDetected && (colflags == 0xA0000002 || colflags == 0x80000402))
				col->slAttribute = 0x20000002;
			// Texanim 1
			if (texid >= 71 && texid <= 85)
			{
				AddTextureAnimation(1, 1, material, false, 4, 71, 85);
				if (SADXWaterDetected)
					col->slAttribute = 0x80000402;
			}
			// Texanim 2
			if (texid >= 42 && texid <= 56)
			{
				AddTextureAnimation(1, 1, material, false, 3, 42, 56);
			}
			// Texanim 3
			if (texid >= 57 && texid <= 70)
			{
				if (texid != 61) AddTextureAnimation(1, 1, material, false, 4, 57, 70);
				else
				{
					// Retarded as usual, I see
					AddTextureAnimation(1, 1, material, false, 4, 71, 85);
					if (SADXWaterDetected)
						col->slAttribute = 0x80000402;
				}
			}
			break;

		case LevelAndActIDs_EmeraldCoast3:
			// Texanim 1
			if (texid >= 65 && texid <= 79)
			{
				AddTextureAnimation(1, 2, material, false, 3, 65, 79);
			}
			// Texanim 2
			if (texid >= 50 && texid <= 64)
			{
				AddTextureAnimation(1, 2, material, false, 4, 50, 64);
			}
			// Texanim 3
			if (texid >= 80 && texid <= 93)
			{
				AddTextureAnimation(1, 2, material, false, 4, 80, 93);
			}
			break;

		case LevelAndActIDs_TwinklePark2:
			// Water animation
			if (texid >= 74 && texid <= 87)
			{
				AddTextureAnimation(3, 1, material, false, 2, 74, 87);
			}
			break;

		case LevelAndActIDs_LostWorld1:
			if (texid >= 44 && texid <= 57)
			{
				AddTextureAnimation(7, 0, material, false, 1, 44, 57);
			}
			break;

		case LevelAndActIDs_LostWorld2:
			if (texid >= 81 && texid <= 94)
			{
				AddTextureAnimation(7, 1, material, false, 1, 81, 94);
			}
			else if (col->slAttribute & SurfaceFlags_Water && texid == 44)
				AddTextureAnimation(7, 1, material, false, 1, 81, 94);
			break;

		case LevelAndActIDs_IceCap4:
			if (texid == 16 || (texid >= 41 && texid <= 54))
			{
				AddTextureAnimation(8, 3, material, true, 4, 16, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, -1);
			}
			break;

		case LevelAndActIDs_Casinopolis1:
			if (texid >= 67 && texid <= 80)
			{
				AddTextureAnimation(9, 0, material, true, 3, 75, 68, 69, 70, 71, 72, 73, 74, 67, 76, 77, 78, 79, 80, -1);
			}
			break;

		case LevelAndActIDs_Casinopolis2:
			if (texid == 7 || (texid >= 9 && texid <= 21))
			{
				AddTextureAnimation(9, 1, material, true, 3, 7, 10, 11, 12, 13, 14, 9, 15, 16, 17, 18, 19, 20, 21, -1);
			}
			break;

		case LevelAndActIDs_HotShelter2:
			if (texid == 129 || texid == 136) // Fences outside the gears area
			{
				if (colflags & SurfaceFlags_Visible && col->yWidth == 0)
				{
					col->yWidth = -3000.0f;
				}
			}
			break;

		case LevelAndActIDs_EggHornet:
			// Disable SA1 ocean if SADX water is enabled
			if (SADXWaterDetected && colflags & SurfaceFlags_Water)
			{
				col->slAttribute &= ~SurfaceFlags_Visible;
			}
			break;

		case LevelAndActIDs_StationSquare3: // Sewers
			// Texanim 1
			if (texid >= 46 && texid <= 55)
			{
				AddTextureAnimation(26, 2, material, false, 4, 46, 55);
			}
			if (SADXWaterDetected)
			{
				// Make the water less transparent like in DX
				if ((colflags & SurfaceFlags_Visible) && (colflags & SurfaceFlags_Water))
					col->pObject->basicdxmodel->mats[0].diffuse.argb.a = 0xD2;
			}
			break;

		case LevelAndActIDs_StationSquare4: // Main area
			// Texanim 1
			if (texid == 183)
				AddTextureAnimation(26, 3, material, true, 4, 183, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255);
			// Texanim 2
			if (texid == 29)
			{
				AddTextureAnimation(26, 3, material, true, 4, 29, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241);
			}
			// Texanim 3
			if (texid == 142)
			{
				AddTextureAnimation(26, 3, material, true, 4, 142, 219, 220, 221, 222, 223, 224, 225, 226, 227);
			}
			if (SADXWaterDetected)
			{
				// Show SADX sea bottom
				if (colflags == 0)
					col->slAttribute |= SurfaceFlags_Visible;
				// Hide SA1 water
				if ((colflags & SurfaceFlags_Visible) && (colflags & SurfaceFlags_Water))
					col->slAttribute &= ~SurfaceFlags_Visible;
			}
			break;

		case LevelAndActIDs_StationSquare5: // Hotel/Pool
			// Texanim 1
			if (texid >= 101 && texid <= 114)
			{
				AddTextureAnimation(26, 4, material, false, 4, 101, 114);
			}
			// Texanim 2
			if (texid >= 87 && texid <= 100)
			{
				AddTextureAnimation(26, 4, material, false, 4, 87, 100);
			}
			// Texanim 3
			if (texid == 65)
			{
				AddTextureAnimation(26, 4, material, true, 4, 65, 78, 79, 80, 81, 82, 83, 84, 85, 86);
			}
			// Texanim 4
			if (texid == 59 || (texid > 86 && texid < 101))
			{
				AddTextureAnimation(26, 4, material, true, 4, 59, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100);
			}
			if (SADXWaterDetected)
			{
				// Show SADX sea bottom
				if (colflags == 0)
					col->slAttribute |= SurfaceFlags_Visible;
				// Hide SA1 water
				if ((colflags & SurfaceFlags_Visible) && (colflags & SurfaceFlags_Water))
					col->slAttribute &= ~SurfaceFlags_Visible;
			}
			break;

		case LevelAndActIDs_MysticRuins1: // Station
			// Texanim 1
			if (texid == 128 || (texid >= 130 && texid <= 139))
			{
				AddTextureAnimation(33, 0, material, false, 5, 130, 139);
			}
			// Texanim 2
			if (texid == 129 || (texid >= 140 && texid <= 154))
			{
				AddTextureAnimation(33, 0, material, false, 4, 140, 154);
			}
			// UVAnim 1
			if ((matflags & NJD_CUSTOMFLAG_UVANIM1) && !(matflags & NJD_CUSTOMFLAG_UVANIM2))
			{
				AddUVAnimation(33, 0, meshset, 1, 0, 1);
			}
			// UVAnim 2
			if ((matflags & NJD_CUSTOMFLAG_UVANIM2) && !(matflags & NJD_CUSTOMFLAG_UVANIM1))
			{
				AddUVAnimation(33, 0, meshset, 1, 0, -1);
			}
			break;

		case LevelAndActIDs_MysticRuins2: // Angel Island
			// Texanim 1
			if (texid >= 76 && texid <= 89)
			{
				AddTextureAnimation(33, 1, material, false, 2, 76, 89);
			}
			break;

		case LevelAndActIDs_MysticRuins3: // Jungle
			// UVAnim 1
			if ((matflags & NJD_CUSTOMFLAG_UVANIM1) && !(matflags & NJD_CUSTOMFLAG_UVANIM2))
			{
				AddUVAnimation(33, 2, meshset, 2, 0, 1);
				//PrintDebug("Added UVAnim1\n");
			}
			// UVAnim 2
			if ((matflags & NJD_CUSTOMFLAG_UVANIM2) && !(matflags & NJD_CUSTOMFLAG_UVANIM1))
			{
				AddUVAnimation(33, 2, meshset, 2, 0, 1);
				//PrintDebug("Added UVAnim2\n");
			}
			// UVAnim 3
			if ((matflags & NJD_CUSTOMFLAG_UVANIM2) && (matflags & NJD_CUSTOMFLAG_UVANIM1))
			{
				if (texid == 83)
				{
					AddUVAnimation(33, 2, meshset, 2, 0, -1);
					//PrintDebug("Added UVAnim4 - 126\n");
				}
				else
				{
					if (meshset->nbMesh == 3)
					{
						AddUVAnimation(33, 2, meshset, 2, 0, 1);
						//PrintDebug("Added UVAnim3 - 48\n");
					}
					else
					{
						AddUVAnimation(33, 2, meshset, 2, 0, 1);
						//PrintDebug("Added UVAnim3 - 46\n");
					}
				}
			}
			break;

		case LevelAndActIDs_Past2:
			// Texanim 1
			if (texid == 6 || (texid >= 73 && texid <= 82))
			{
				if (SADXWaterDetected)
					col->slAttribute &= ~SurfaceFlags_Visible;
				else
					AddTextureAnimation(34, 1, material, false, 4, 73, 82);
			}
			// Texanim 2
			if (texid == 57 || (texid >= 59 && texid <= 72))
			{
				AddTextureAnimation(34, 1, material, false, 4, 59, 72);
			}
			// UVAnim 1
			if ((matflags & NJD_CUSTOMFLAG_UVANIM1) && !(matflags & NJD_CUSTOMFLAG_UVANIM2))
			{
				AddUVAnimation(34, 1, meshset, 1, 0, -4);
			}
			break;

		case LevelAndActIDs_Past3:
			// Texanim 1
			if (texid == 0 || (texid >= 75 && texid <= 84))
			{
				if (SADXWaterDetected)
					col->slAttribute &= ~SurfaceFlags_Visible;
				else
					AddTextureAnimation(34, 2, material, false, 4, 75, 84);
			}
			// Texanim 2
			if (texid == 1 || (texid >= 61 && texid <= 74))
			{
				AddTextureAnimation(34, 2, material, false, 4, 61, 74);
			}
			// UVAnim 1
			if ((matflags & NJD_CUSTOMFLAG_UVANIM1) && !(matflags & NJD_CUSTOMFLAG_UVANIM2))
			{
				AddUVAnimation(34, 2, meshset, 1, 0, -4);
			}
			break;

		case LevelAndActIDs_SSGarden:
			if (texid >= 0 && texid <= 9)
				AddTextureAnimation(LevelIDs_SSGarden, 0, material, false, 4, 0, 9);
			break;

		case LevelAndActIDs_ECGarden:
			if (texid >= 64 && texid <= 78)
				AddTextureAnimation(LevelIDs_ECGarden, 0, material, false, 4, 64, 78);
			// Below isn't needed at the moment because the landtable pieces are made invisible in the current version
			//if (texid >= 54 && texid <= 63) AddTextureAnimation(LevelIDs_ECGarden, 0, material, false, 4, 54, 63);
			break;

		case LevelAndActIDs_MRGarden:
			if (texid == 23)
				AddUVAnimation(LevelIDs_MRGarden, 0, meshset, 0, 0, -1);
			if (texid == 24)
				AddUVAnimation(LevelIDs_MRGarden, 0, meshset, 0, 0, -6);
			if (texid >= 36 && texid <= 45)
				AddTextureAnimation(LevelIDs_MRGarden, 0, material, false, 4, 36, 45);
			break;

		case (LevelAndActIDs_ChaoRace + 1): // Entry is Act 0, Race is 1
			if (texid >= 55 && texid <= 68)
				AddTextureAnimation(LevelIDs_ChaoRace, 1, material, false, 4, 55, 68);
		}
	}
}

// Process a COL entry to add Lantern materials and animations
void ProcessColEntry(_OBJ_LANDENTRY* col, unsigned __int16 levelact, bool unregister, bool lantern)
{
	bool SADXWaterDetected = CheckSADXWater((LevelIDs)(levelact >> 8));
	Uint32 colflags = col->slAttribute;
	for (int k = 0; k < col->pObject->basicdxmodel->nbMeshset; ++k)
	{
		NJS_MESHSET_SADX* meshset = &col->pObject->basicdxmodel->meshsets[k];
		NJS_MATERIAL* material = &col->pObject->basicdxmodel->mats[meshset->type_matId & ~0xC000];
		Uint32 matflags = material != nullptr ? material->attrflags : 0;
		Uint32 texid = material != nullptr ? material->attr_texId : 0;
		// Add animations etc.
		if (!unregister)
			ProcessColFlags(col, levelact);
		if (!lantern)
			return;
		// White diffuse (Station Square night)
		if ((matflags & NJD_CUSTOMFLAG_NIGHT) && (matflags & NJD_CUSTOMFLAG_WHITE))
			ParseWhiteDiffuseNightMaterial(material, unregister);
		// White diffuse (regular)
		else if (matflags & NJD_CUSTOMFLAG_WHITE)
			ParseWhiteDiffuseMaterial(material, unregister);
		// Alpha rejection
		if (matflags & NJD_CUSTOMFLAG_NO_REJECT)
			ParseAlphaRejectMaterial(material, unregister);
		// Level-specific things
		switch (levelact)
		{

		case LevelAndActIDs_FinalEgg2:
			if (texid == 16 || texid == 79)
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_HotShelter1:
			if (texid == 46)
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_HotShelter2:

			if ((texid == 104 || texid == 98 || texid == 18))
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_HotShelter3:
			if (texid == 18)
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_Chaos0:
			if (texid == 5 || texid == 15)
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_Chaos4:
			if (texid == 4)
			{
				ParseWhiteDiffuseMaterial(material, unregister);
			}
			break;

		case LevelAndActIDs_EggViper:
			if (texid == 32)
			{
				ParseAlphaRejectMaterial(material, unregister);
			}
			break;
		}
	}

}

void ProcessGeoAnimEntry(_OBJ_MOTLANDENTRY* entry, unsigned __int16 levelact, bool unregister)
{
	for (int k = 0; k < entry->pObject->basicdxmodel->nbMeshset; ++k)
	{
		NJS_MESHSET_SADX* meshset = &entry->pObject->basicdxmodel->meshsets[k];
		NJS_MATERIAL* material = &entry->pObject->basicdxmodel->mats[meshset->type_matId & ~0xC000];
		Uint32 matflags = material != nullptr ? material->attrflags : 0;
		Uint32 texid = material != nullptr ? material->attr_texId : 0;
		// White diffuse
		if (matflags & NJD_CUSTOMFLAG_WHITE)
			ParseWhiteDiffuseNightMaterial(material, unregister);
	}
}

void ProcessLandtable(_OBJ_LANDTABLE* landtable, unsigned __int16 levelact, bool unregister, bool lantern)
{
	for (int j = 0; j < landtable->ssCount; j++)
		ProcessColEntry(&landtable->pLandEntry[j], levelact, unregister, lantern);
	// Since GeoAnim stuff only has Lantern materials and nothing else, it can be skipped if Lantern isn't needed
	if (!lantern)
		return;
	// GeoAnim materials (hardcoded for now, will be replaced with the function below)
	switch (levelact)
	{
	case LevelAndActIDs_FinalEgg1:
		ParseWhiteDiffuseMaterial(&landtable->pMotLandEntry[21].pObject->child->basicdxmodel->mats[1], unregister);
		break;
	case LevelAndActIDs_EggCarrierOutside4:
		for (int i = 1; i < 10; i++)
			ParseWhiteDiffuseMaterial(&landtable->pMotLandEntry[landtable->ssMotCount - 1].pObject->basicdxmodel->mats[i], unregister); // Computers
		break;
	case LevelAndActIDs_MysticRuins3:
		for (int i = 0; i < 6; i++)
			ParseWhiteDiffuseMaterial(&landtable->pMotLandEntry[0].pObject->child->basicdxmodel->mats[i], unregister); // Lantern in jungle
		break;
	case LevelAndActIDs_MysticRuins4:
		for (int i = 0; i < 5; i++)
			ParseWhiteDiffuseMaterial(&landtable->pMotLandEntry[1].pObject->basicdxmodel->mats[i], unregister); // Metal Sonic in tube
		break;
	}
	/*
	for (int j = 0; j < landtable->ssMotCount; j++)
		ProcessGeoAnimEntry(&landtable->pMotLandEntry[j], levelact, unregister);
	*/
}