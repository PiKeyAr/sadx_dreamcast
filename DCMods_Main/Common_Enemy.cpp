#include "stdafx.h"

DataPointer(NJS_MODEL_SADX, model_sorsar_karada_1_atama1, 0x0094BAA0); // Egg Keeper part

// Calls GetShadowPos and corrects the return value to make badniks stay when it fails to find the ground
float GetShadowPos_r(float x, float y, float z, Angle3* rotation)
{
	float result = GetShadowPos(x, y, z, rotation);
	if (result == -1000000.0f)
	{
		result = y;
	}
	return result;
}

// Trampoline to make badniks stay when it fails to find the ground - only used by the Sweep badnik
static Trampoline* GetShadowPosOnWater_t = nullptr;
static float __cdecl GetShadowPosOnWater_r(float x, float y, float z, Rotation3* rotation)
{
	const auto original = TARGET_DYNAMIC(GetShadowPosOnWater);
	float result = original(x, y, z, rotation);
	if (result == -1000000.0f)
	{
		result = y;
	}
	return result;
}

void DrawBombExplosionHook(NJS_MODEL_SADX* model, LATE blend)
{
	if (current_event == -1 && ssStageNumber != LevelIDs_EggViper)
		late_z_ofs___ = 2000.0f;
	late_DrawModel(model, blend);
	late_z_ofs___ = 0.0f;
}

void Enemies_Load()
{

}

void Enemies_Init()
{
	ReplacePVM("E_AMENBO");
	ReplacePVM("E_BOMB");
	ReplacePVM("E_BUYON");
	ReplacePVM("E_CART");
	ReplacePVM("E_LEON");
	ReplacePVM("E_ROBO");
	ReplacePVM("E_SAI");
	ReplacePVM("E_SARU");
	ReplacePVM("E_SNAKE");
	ReplacePVM("E_SNOWMAN");
	ReplacePVM("GACHAPON");
	ReplacePVM("NISEPAT");
	ReplacePVM("SUPI_SUPI");
	ReplacePVM("UNI_A_UNIBODY");
	ReplacePVM("UNI_C_UNIBODY");
	WriteCall((void*)0x004CE670, DrawBombExplosionHook); // Depth bias for explosion
	// Aircraft UV patches
	*(NJS_TEX*)0x00960FB8 = { 252, 253 };
	*(NJS_TEX*)0x00960FBC = { 252, -253 };
	*(NJS_TEX*)0x00960FC0 = { 0, 253 };
	*(NJS_TEX*)0x00960FC4 = { 0, -253 };
	*(NJS_TEX*)0x00960FC8 = { 252, -253 };
	*(NJS_TEX*)0x00960FCC = { 252, 253 };
	*(NJS_TEX*)0x00960FD0 = { 0, -253 };
	*(NJS_TEX*)0x00960FD4 = { 0, 253 };
	// Aircraft base white diffuse
	AddWhiteDiffuseMaterial(&object_sky_signal_signal.basicdxmodel->mats[4]);
	AddWhiteDiffuseMaterial(&object_sky_signal_signal.basicdxmodel->mats[5]);
	AddWhiteDiffuseMaterial(&object_sky_signal_signal.basicdxmodel->mats[6]);
	AddWhiteDiffuseMaterial(&object_sky_signal_signal.basicdxmodel->mats[7]);
	AddWhiteDiffuseMaterial(&object_sky_signal_signal.basicdxmodel->mats[8]);
	// Fix badniks not spawning
	WriteCall((void*)0x0049EFE7, GetShadowPos_r); // Egg Keeper
	WriteCall((void*)0x007A05EF, GetShadowPos_r); // Rhinotank
	WriteCall((void*)0x004C9012, GetShadowPos_r); // Snowman
	WriteCall((void*)0x006F7F3B, GetShadowPos_r); // Station Square cars (Egg Walker cutscene)
	GetShadowPosOnWater_t = new Trampoline(0x0049EAD0, 0x0049EAD7, GetShadowPosOnWater_r); // Amenbo
	// Model tweaks
	object_uni_c_fireball_fireball.basicdxmodel->mats[1].attrflags &= ~NJD_FLAG_IGNORE_LIGHT;
	AddWhiteDiffuseMaterial(&object_reon2_kihon_atama01_atama01.child->basicdxmodel->mats[1]);
	AddWhiteDiffuseMaterial(&object_reon2_kihon_atama01_atama01.child->basicdxmodel->mats[2]);
	AddWhiteDiffuseMaterial(&object_reon2_kihon_atama01_atama01.child->sibling->sibling->sibling->sibling->basicdxmodel->mats[1]);
	AddWhiteDiffuseMaterial(&object_reon2_kihon_atama01_atama01.child->sibling->sibling->sibling->sibling->basicdxmodel->mats[2]);
	// Leon fixes
	WriteData((float**)0x004CD75A, &_nj_screen_.w); // From SADXFE
	WriteData((float**)0x004CD77C, &_nj_screen_.h); // From SADXFE
	// Various other stuff
	AddWhiteDiffuseMaterial(&model_sorsar_karada_1_atama1.mats[4]); // Egg Keeper
}