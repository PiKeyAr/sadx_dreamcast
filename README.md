# SADX Dreamcast Conversion mod by PkR

Dreamcast Conversion is a mod for Sonic Adventure DX PC that brings back Dreamcast levels, textures, object models, special effects and branding.

**Homepage:** http://www.moddb.com/mods/sadx-dreamcast-conversion

To install Dreamcast Conversion you can use SADX Mod Installer that sets up SADX Mod Loader and downloads the latest versions of mods automatically. The installer can also convert the Steam version of SADX to the 2004 version, which supports the Mod Loader. 

**SADX Mod Installer:** https://sadxmodinstaller.unreliable.network

Alternatively you can download Dreamcast Conversion for manual installation [here](https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/DreamcastConversion.7z).

Changelog: https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Changelog.txt

**Related mods:**

Dreamcast DLCs: https://gitlab.com/PiKeyAr/sadx-dreamcast-dlc

HD GUI 2: https://gitlab.com/PiKeyAr/sadx-hd-gui

Sound Overhaul 2: https://gitlab.com/PiKeyAr/sadx-sound-overhaul

Time of Day: https://gitlab.com/PiKeyAr/sadx-timeofday-mod
